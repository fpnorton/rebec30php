﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $VD = $VD ?? []; ?>
<section class="content-header">
<div class="row ct-steps-content">
	<div class="col-sm-12 content ct-steps">
		<div class="row">
			<div class="col-md-2 col-sm-2 col-xs-2 ct-sumary-link">
				<ul class="">
					<li>
						<a href="<?php echo base_url( @($page['base_url_form-sumario']).$VD['subData']->trial_id); ?>" >
						<img src="<?php echo base_url('assets/images/summary-icon-circle.png'); ?>">
                            <?php echo $genericlabel->get_or_new('step_summary'); ?>
						</a>
					</li>
				</ul>
			</div>
			<div class="col-md-10 col-sm-8 col-xs-8" id="steps-list">
				<ul class="nav">
					<?php foreach(array_keys(A_STEPS) as $step): ?>
                        <?php $step = $step - 1; ?>
						<li>
                            <p><span><?php echo $genericlabel->get_or_new('step_'.$step); ?></span></p>
                            <a  style="padding: 10px !important;"
                                href="<?php echo base_url( @($page['base_url_form-corrigir']).$step.'/'.$VD['subData']->trial_id); ?>">
							<?php if ($step < $VD['currentStep']): ?>
                                <span data-html="true"
                                      data-toggle="tooltip"
                                      data-placement="left"
                                      style="height: 30px; width: 30px; background-color: #0D6D67; color: #FFFFFF; border-radius: 50%; display: inline-block;">
                                    <h3><b><?php echo $step; ?></b></h3>
                                </span>
                            <?php elseif ($step == $VD['currentStep']): ?>
                                <span data-html="true"
                                      data-toggle="tooltip"
                                      data-placement="left"
                                      style="height: 30px; width: 30px; background-color: #0D6D67; color: #FFFFFF; border-radius: 50%; display: inline-block;">
                                    <h3><b><?php echo $step; ?></b></h3>
                                </span>
							<?php else: ?>
                                <span data-html="true"
                                      data-toggle="tooltip"
                                      data-placement="left"
                                      style="height: 30px; width: 30px; background-color: #d5d5d5; color: #0D6D67; border-radius: 50%; display: inline-block; border-width: 3px; border-color: #0D6D67;">
                                    <h3><b><?php echo $step; ?></b></h3>
                                </span>
							<?php endif; ?>
                            </a>
                            <?php if ($step > $VD['currentStep']): ?>
                                <hr style="background-color: #d5d5d5; height: 5px; border: 0;" />
                            <?php else : ?>
                                <hr style="background-color: #0D6D67; height: 5px; border: 0;" />
                            <?php endif; ?>
						</li>
					<?php endforeach; ?>
                    <?php if ($VD['subData']->isPodeSerEnviado() and ($usuario->isCommon())): ?>
                    <li>
                        <p><span><?php echo $genericlabel->get_or_new('step_'.$step); ?></span></p>
                        <a  style="padding: 10px !important;"
                            href="<?php echo base_url( @($controller_method)."/enviar/".$VD['subData']->trial_id); ?>">
                            <?php if ($step < $VD['currentStep']): ?>
                                <span data-html="true"
                                      data-toggle="tooltip"
                                      data-placement="left"
                                      style="height: 30px; width: 30px; background-color: #0D6D67; color: #FFFFFF; border-radius: 50%; display: inline-block;">
                                    <h3><b><i class="fa fa-mail-forward" data-original-title="<?php echo $genericlabel->get_or_new('step_send_to_remark'); ?>"></i></b></h3>
                                </span>
                            <?php elseif ($step == $VD['currentStep']): ?>
                                <span data-html="true"
                                      data-toggle="tooltip"
                                      data-placement="left"
                                      style="height: 30px; width: 30px; background-color: #0D6D67; color: #FFFFFF; border-radius: 50%; display: inline-block;">
                                    <h3><b><i class="fa fa-mail-forward" data-original-title="<?php echo $genericlabel->get_or_new('step_send_to_remark'); ?>"></i></b></h3>
                                </span>
                            <?php else: ?>
                                <span data-html="true"
                                      data-toggle="tooltip"
                                      data-placement="left"
                                      style="height: 30px; width: 30px; background-color: #d5d5d5; color: #0D6D67; border-radius: 50%; display: inline-block; border-width: 3px; border-color: #0D6D67;">
                                    <h3><b><i class="fa fa-mail-forward" data-original-title="<?php echo $genericlabel->get_or_new('step_send_to_remark'); ?>"></i></b></h3>
                                </span>
                            <?php endif; ?>
                        </a>
                        <?php if ($step > $VD['currentStep']): ?>
                            <hr style="background-color: #d5d5d5; height: 5px; border: 0;" />
                        <?php else : ?>
                            <hr style="background-color: #0D6D67; height: 5px; border: 0;" />
                        <?php endif; ?>
                    </li>
                    <?php endif; ?>
                    <?php if ($VD['subData']->isPodeSerRevisado() and ($usuario->isRevisor() or $usuario->isSuper())) : ?>
                    <li style="padding: 10px !important;">
                        <a  style="padding: 10px !important;"
                            href="<?php echo base_url( @($controller_method)."/revisar/".$VD['subData']->trial_id); ?>">
                            <span data-html="true"
                                  data-toggle="tooltip"
                                  data-placement="left"
                                  style="height: 30px; width: 30px; background-color: #0D6D67; color: #FFFFFF; border-radius: 50%; display: inline-block;">
                                <h3><b><i class="fa fa-mail-forward" data-original-title="<?php echo $genericlabel->get_or_new('step_approve_or_resubmit'); ?>"></i></b></h3>
                            </span>
                        </a>
                        <hr style="background-color: #d5d5d5; height: 5px; border: 0;" />
                    </li>
                    <?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
</div>
</section>
<?php $genericlabel->disableUpdate(); ?>