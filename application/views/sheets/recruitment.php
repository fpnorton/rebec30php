﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('RecruitmentForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldHelpLanguage('RecruitmentForm'); $fieldhelp->enableUpdate(); ?>
<script>
    {
        $('window').ready(function(){
            $("input[name^='date_']").attr('placeholder', 'dd/mm/yyyy');
        });
    }
</script>
<pre><?
// echo var_export([
//     '$VD[genData][countrycode]' => $VD['genData']['countrycode']->toArray()
// ], TRUE);
 ?></pre>
<div class="row">
    <div class="col-sm-12 content">
        <h1 class="title_line">
            <div class="row">
                <div class="number-rounded"><? echo PASSO_RECRUTAMENTO ?></div>
                <span>&nbsp;<?php echo $fieldlabel->get_or_new('main_title'); ?></span>
            </div>
        </h1>
        <div class="row">
            <form role="form"
                  class="col-sm-12"
                  action="<?php echo $_form_action ?>"
                  method="post"
                  accept-charset="utf-8">
                <!-- -->
                <? $languages = $languages ?? set_value('languages[]'); ?>
                <? foreach ($languages as $language) : ?>
                    <input type="hidden" name="languages[]" value="<? echo $language ?>">
                <? endforeach; ?>
                <!-- -->
                <?
                // ############################################################################################################### 
                $piece['passo'] = $passo_6;
                $piece['has_review_remark'] = $has_review_remark;
                $piece['reviewer'] = $reviewer;
                $piece['revisor_field'] = $revisor_field;
                include_once 'piece/remark.php'; 
                // ############################################################################################################### 
                ?>
                <input type="hidden" name="id" value="<?php echo $passo_6['id']; ?>">
                <!-- -->
                <div class="fieldWrapper">
                    <? $study_status = $passo_6['study_status'] ?? set_value('study_status') ?>
                    <div class="row-input label-line">
                        <div class="no-margin-left">
                            <label for="id_study_status"><?php echo $fieldlabel->get_or_new('study_status'); ?>:</label><span class="fielct-required "></span>
                        </div>
                        <div class="help-input-text">
                            <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                  data-original-title="<?php echo $fieldhelp->get_or_new('status'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                        </div>
                        <div class="revision-icons-wrapper"></div>
                    </div>
                    <div class="row-input input">
                        <div class="field-input">
                            <select id="id_study_status"
                                <?php echo @$common_field ?? '' ?>
                                    name="study_status">
                                <option value="0" selected="selected"></option>
                                <?php foreach ($VD['genData']['recruitment_status'] as $recruitment_status): ?>
                                    <?php if (linguagem_selecionada() == 'en') : ?>
                                        <?php $translation = $recruitment_status; ?>
                                    <?php else : ?>
                                        <?php $translation = $recruitment_status->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                    <?php endif; ?>
                                    <?php if ($recruitment_status->id == $study_status) : ?>
                                        <option selected="selected"
                                                value="<?php echo $recruitment_status->id;  ?>"><?php echo $translation->label; ?></option>
                                    <?php else: ?>
                                        <option
                                                value="<?php echo $recruitment_status->id;  ?>"><?php echo $translation->label; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="text-center">
                            <?php if(!empty($study_status)): ?>
                                <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <!-- -->
                <div class="fieldWrapper">
                    <? $recruitment_countries = $passo_6['recruitment_countries'] ?? set_value('recruitment_countries[]') ?>
                    <div class="row-input label-line">
                        <div class="no-margin-left">
                            <label for="id_recruitment_countries"><?php echo $fieldlabel->get_or_new('country'); ?>:</label><span class="fielct-required"></span>
                        </div>
                        <div class="help-input-text">
                            <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                  data-original-title="<?php echo $fieldhelp->get_or_new('recruitment_country') ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                        </div>
                    </div>
                    <?php echo form_error('recruitment_countries[]', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
<!-- -->
                    <div class="input row-input  ">
                        <div class="field-input">
                            <? $field_ok = $recruitment_countries; ?>
                            <select id="id_recruitment_countries"
                                    class="ui fluid search dropdown"
                                    name="recruitment_countries[]"
                                <?php echo @$common_field ?? '' ?>
                                    multiple="multiple">
                                <option id=""></option>
                                <?php foreach ($VD['genData']['countrycode'] as $cc): ?>
                                    <?php if (linguagem_selecionada() == 'en') : ?>
                                        <?php $translation = $cc; ?>
                                    <?php else : ?>
                                        <?php $translation = $cc->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                    <?php endif; ?>
                                    <?php $selected = in_array($cc->id, $recruitment_countries); ?>
                                    <option value="<?php echo $cc->id; ?>" <?php echo ($selected ? 'selected="selected"' : ''); ?>><?php echo $translation->description; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="text-right">
                            <?php if($field_ok): ?>
                                <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                <?php $field_ok = false;
                            endif; ?>
                        </div>
                    </div>
<!-- -->
                </div>
                <div class="fieldWrapper">
                    <? $date_first_enrollment = $passo_6['date_first_enrollment'] ?? set_value('date_first_enrollment') ?>
                    <? $date_first_enrollment_d = $passo_6['date_first_enrollment-d'] ?? set_value('date_first_enrollment-d') ?>
                    <? $date_first_enrollment_m = $passo_6['date_first_enrollment-m'] ?? set_value('date_first_enrollment-m') ?>
                    <? $date_first_enrollment_y = $passo_6['date_first_enrollment-y'] ?? set_value('date_first_enrollment-y') ?>
                    <div class="row-input label-line">
                        <div>
                            <label for="id_date_first_enrollment"><?php echo $fieldlabel->get_or_new('date_first_enrollment'); ?>:</label><span class="fielct-required"></span>
                        </div>
                        <div class="help-input-text">
                            <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                  data-original-title="<?php echo $fieldhelp->get_or_new('start_planned'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                        </div>
                        <div class="revision-icons-wrapper"></div>
                    </div>
                    <?php echo form_error('date_first_enrollment', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                    <div class="row-input input error-date_first_enrollment" id="date_first_enrollment">
                        <div class="field-input">
                            <!--
                            <input id="id_date_first_enrollment"
                                   name="date_first_enrollment"
                                <?php echo @$common_field ?? '' ?>
                                   type="date" value="<?php echo $date_first_enrollment; ?>" />
                            -->
                            <select id="id_date_first_enrollment-d"
                                style="width: 70px !important;"
                                <?php echo @$common_field ?? '' ?>
                                <?php echo @$readonly ?>
                                name="date_first_enrollment-d">
                                <option value="" selected></option>
                                <?php for ($c = 1; $c <= 31; $c++) : ?>
                                <option value="<?php echo $c; ?>" <?php if(@$date_first_enrollment_d == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                <?php endfor; ?>
                            </select>
                            <select id="id_date_first_enrollment-m"
                                style="width: 70px !important;"
                                <?php echo @$common_field ?? '' ?>
                                <?php echo @$readonly ?>
                                name="date_first_enrollment-m">
                                <option value="" selected></option>
                                <?php for ($c = 1; $c <= 12; $c++) : ?>
                                <option value="<?php echo $c; ?>" <?php if(@$date_first_enrollment_m == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                <?php endfor; ?>
                            </select>
                            <select id="id_date_first_enrollment-y"
                                style="width: 70px !important;"
                                <?php echo @$common_field ?? '' ?>
                                <?php echo @$readonly ?>
                                name="date_first_enrollment-y">
                                <option value="" selected></option>
                                <?php for ($c = 2001; $c <= 2040; $c++) : ?>
                                <option value="<?php echo $c; ?>" <?php if(@$date_first_enrollment_y == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                <?php endfor; ?>
                            </select>
                            dd-mm-yyyy
                        </div>
                        <div class="text-center">
                            <?php if(!empty($date_first_enrollment)): ?>
                                <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                <?php $field_ok = false;
                            endif; ?>
                        </div>
                    </div>
                </div>
                <div class="fieldWrapper">
                    <? $date_last_enrollment = $passo_6['date_last_enrollment'] ?? set_value('date_last_enrollment') ?>
                    <? $date_last_enrollment_d = $passo_6['date_last_enrollment-d'] ?? set_value('date_last_enrollment-d') ?>
                    <? $date_last_enrollment_m = $passo_6['date_last_enrollment-m'] ?? set_value('date_last_enrollment-m') ?>
                    <? $date_last_enrollment_y = $passo_6['date_last_enrollment-y'] ?? set_value('date_last_enrollment-y') ?>
                    <div class="row-input label-line">
                        <div>
                            <label for="id_date_last_enrollment"><?php echo $fieldlabel->get_or_new('date_last_enrollment'); ?>:</label><span class="fielct-required"></span>
                        </div>
                        <div class="help-input-text">
                            <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                  data-original-title="<?php echo $fieldhelp->get_or_new('end_planned'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                        </div>
                        <div class="revision-icons-wrapper"></div>
                    </div>
                    <?php echo form_error('date_last_enrollment', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                    <div class="row-input input " id="date_last_enrollment">
                        <div class="field-input">
                            <!--
                            <input id="id_date_last_enrollment"
                                   name="date_last_enrollment"
                                <?php echo @$common_field ?? '' ?>
                                   type="date" value="<?php echo $date_last_enrollment; ?>">
                            -->
                            <select id="id_date_last_enrollment-d"
                                style="width: 70px !important;"
                                <?php echo @$common_field ?? '' ?>
                                <?php echo @$readonly ?>
                                name="date_last_enrollment-d">
                                <option value="" selected></option>
                                <?php for ($c = 1; $c <= 31; $c++) : ?>
                                <option value="<?php echo $c; ?>" <?php if(@$date_last_enrollment_d == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                <?php endfor; ?>
                            </select>
                            <select id="id_date_last_enrollment-m"
                                style="width: 70px !important;"
                                <?php echo @$common_field ?? '' ?>
                                <?php echo @$readonly ?>
                                name="date_last_enrollment-m">
                                <option value="" selected></option>
                                <?php for ($c = 1; $c <= 12; $c++) : ?>
                                <option value="<?php echo $c; ?>" <?php if(@$date_last_enrollment_m == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                <?php endfor; ?>
                            </select>
                            <select id="id_date_last_enrollment-y"
                                style="width: 70px !important;"
                                <?php echo @$common_field ?? '' ?>
                                <?php echo @$readonly ?>
                                name="date_last_enrollment-y">
                                <option value="" selected></option>
                                <?php for ($c = 2001; $c <= 2040; $c++) : ?>
                                <option value="<?php echo $c; ?>" <?php if(@$date_last_enrollment_y == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                <?php endfor; ?>
                            </select>
                            dd-mm-yyyy
                        </div>
                        <div class="text-center">
                            <?php if(!empty($date_last_enrollment)): ?>
                                <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                <?php $field_ok = false;
                            endif; ?>
                        </div>
                    </div>
                </div>
                <div class="fieldWrapper">
                    <? $gender = $passo_6['gender'] ?? set_value('gender') ?>
                    <div class="row-input label-line">
                        <div>
                            <label for="id_gender"><?php echo $fieldlabel->get_or_new('gender'); ?>:</label><span class="fielct-required"></span>
                        </div>
                        <div class="help-input-text">
                            <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                  data-original-title="<?php echo $fieldhelp->get_or_new('gender'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                        </div>
                        <div class="revision-icons-wrapper"></div>
                    </div>
                    <?php echo form_error('gender', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                    <div class="row-input input " id="gender">
                        <div class="field-input">
                            <select <?php echo @$common_field ?? '' ?>
                                    name="gender">
                                    <option value=""></option>
                                    <option <?php echo ($gender == 'M') ? 'selected="selected"' : ''; ?>
                                            value="M"><?php echo $fieldlabel->get_or_new('gender_masculine'); ?></option>
                                    <option <?php echo ($gender == 'F') ? 'selected="selected"' : ''; ?>
                                            value="F"><?php echo $fieldlabel->get_or_new('gender_feminine'); ?></option>
                                    <option <?php echo ($gender == 'B') ? 'selected="selected"' : ''; ?>
                                            value="B"><?php echo $fieldlabel->get_or_new('gender_both'); ?></option>
                            </select>
                        </div>
                        <div class="text-center">
                            <?php if(!empty($gender)): ?>
                                <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                <?php $field_ok = false;
                            endif; ?>
                        </div>
                    </div>
                </div>
                <div class="div-unique-background">
                    <div class="fieldWrapper">
                        <? $minimum_age_no_limit = $passo_6['minimum_age_no_limit'] ?? set_value('minimum_age_no_limit') ?>
                        <div class="row-input label-line">
                            <div>
                                <label for="id_minimum_age_no_limit"><?php echo $fieldlabel->get_or_new('min_age_no_limit'); ?>?</label><span class="fielct-required"></span>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('agemin_limit'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('minimum_age_no_limit', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="row-input input " id="minimum_age_no_limit">
                            <div class="field-input">
                                <select id="id_minimum_age_no_limit"
                                    <?php echo @$common_field ?? '' ?>
                                        name="minimum_age_no_limit">
                                    <?php if ($minimum_age_no_limit == 'False') : ?>
                                        <option value="True"><?php echo $fieldlabel->get_or_new('min_age_no_limit_yes'); ?></option>
                                        <option value="False" selected="selected"><?php echo $fieldlabel->get_or_new('min_age_no_limit_no'); ?></option>
                                    <?php else : ?>
                                        <option value="True" selected="selected"><?php echo $fieldlabel->get_or_new('min_age_no_limit_yes'); ?></option>
                                        <option value="False"><?php echo $fieldlabel->get_or_new('min_age_no_limit_no'); ?></option>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="text-center">
                                <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="fieldWrapper" style="display: none;">
                        <? $inclusion_minimum_age = $passo_6['inclusion_minimum_age'] ?? set_value('inclusion_minimum_age') ?>
                        <div class="row-input label-line">
                            <div>
                                <label for="id_inclusion_minimum_age"><?php echo $fieldlabel->get_or_new('min_age'); ?>:</label>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('agemin_value'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('inclusion_minimum_age', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="row-input input " id="inclusion_minimum_age">
                            <div class="field-input">
                                <input id="id_inclusion_minimum_age"
                                       name="inclusion_minimum_age"
                                       type="number"
                                    <?php echo @$common_field ?? '' ?>
                                       value="<?php echo $inclusion_minimum_age; ?>">
                            </div>
                            <div class="text-center">
                                <?php if(!empty($inclusion_minimum_age)): ?>
                                    <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                    <?php $field_ok = false;
                                endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="fieldWrapper" style="display: none;">
                        <? $minimum_age_unit = $passo_6['minimum_age_unit'] ?? set_value('minimum_age_unit') ?>
                        <div class="row-input label-line">
                            <div>
                                <label for="id_minimum_age_unit"><?php echo $fieldlabel->get_or_new('min_age_unit'); ?>:</label>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('min_age_unit'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('minimum_age_unit', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="row-input input " id="minimum_age_unit">
                            <div class="field-input">
                                <select id="id_minimum_age_unit"
                                    <?php echo @$common_field ?? '' ?>
                                        name="minimum_age_unit">
                                    <option value="" selected="selected"></option>
                                    <option value="Y" <?php echo ($minimum_age_unit == 'Y' ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('min_age_unit_years'); ?></option>
                                    <option value="M" <?php echo ($minimum_age_unit == 'M' ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('min_age_unit_months'); ?></option>
                                    <option value="W" <?php echo ($minimum_age_unit == 'W' ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('min_age_unit_weeks'); ?></option>
                                    <option value="D" <?php echo ($minimum_age_unit == 'D' ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('min_age_unit_days'); ?></option>
                                    <option value="H" <?php echo ($minimum_age_unit == 'H' ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('min_age_unit_hours'); ?></option>
                                </select>
                            </div>
                            <div class="text-center">
                                <?php if(!empty($minimum_age_unit)): ?>
                                    <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                    <?php $field_ok = false;
                                endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="div-unique-background">
                    <div class="fieldWrapper">
                        <? $maximum_age_no_limit = $passo_6['maximum_age_no_limit'] ?? set_value('maximum_age_no_limit') ?>
                        <div class="row-input label-line">
                            <div>
                                <label for="id_maximum_age_no_limit"><?php echo $fieldlabel->get_or_new('max_age_no_limit'); ?>?</label><span class="fielct-required"></span>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('agemax_limit'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('maximum_age_no_limit', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="row-input input " id="maximum_age_no_limit">
                            <div class="field-input">
                                <select id="id_maximum_age_no_limit"
                                    <?php echo @$common_field ?? '' ?>
                                        name="maximum_age_no_limit">
                                    <?php if ($maximum_age_no_limit == 'False') : ?>
                                        <option value="True"><?php echo $fieldlabel->get_or_new('max_age_no_limit_yes'); ?></option>
                                        <option value="False" selected="selected"><?php echo $fieldlabel->get_or_new('max_age_no_limit_no'); ?></option>
                                    <?php else : ?>
                                        <option value="True" selected="selected"><?php echo $fieldlabel->get_or_new('max_age_no_limit_yes'); ?></option>
                                        <option value="False"><?php echo $fieldlabel->get_or_new('max_age_no_limit_no'); ?></option>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="text-center">
                                <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="fieldWrapper" style="display: none;">
                        <? $inclusion_maximum_age = $passo_6['inclusion_maximum_age'] ?? set_value('inclusion_maximum_age') ?>
                        <div class="row-input label-line">
                            <div>
                                <label for="id_inclusion_maximum_age"><?php echo $fieldlabel->get_or_new('max_age'); ?>:</label>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('agemax_value'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('inclusion_maximum_age', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="row-input input " id="inclusion_maximum_age">
                            <div class="field-input">
                                <input id="id_inclusion_maximum_age"
                                       name="inclusion_maximum_age"
                                       type="number"
                                    <?php echo @$common_field ?? '' ?>
                                       value="<?php echo $inclusion_maximum_age; ?>">
                            </div>
                            <div class="text-center">
                                <?php if(!empty($inclusion_maximum_age)): ?>
                                    <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                    <?php $field_ok = false;
                                endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="fieldWrapper" style="display: none;">
                        <? $maximum_age_unit = $passo_6['maximum_age_unit'] ?? set_value('maximum_age_unit') ?>
                        <div class="row-input label-line">
                            <div>
                                <label for="id_maximum_age_unit"><?php echo $fieldlabel->get_or_new('max_age_unit'); ?>:</label>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('agemax_unit'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('maximum_age_unit', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="row-input input " id="maximum_age_unit">
                            <div class="field-input">
                                <select id="id_maximum_age_unit"
                                    <?php echo @$common_field ?? '' ?>
                                        name="maximum_age_unit">
                                    <option value="" selected="selected"></option>
                                    <option value="Y" <?php echo ($maximum_age_unit == 'Y' ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('max_age_unit_years'); ?></option>
                                    <option value="M" <?php echo ($maximum_age_unit == 'M' ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('max_age_unit_months'); ?></option>
                                    <option value="W" <?php echo ($maximum_age_unit == 'W' ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('max_age_unit_weeks'); ?></option>
                                    <option value="D" <?php echo ($maximum_age_unit == 'D' ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('max_age_unit_days'); ?></option>
                                    <option value="H" <?php echo ($maximum_age_unit == 'H' ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('max_age_unit_hours'); ?></option>
                                </select>
                            </div>
                            <div class="text-center">
                                <?php if(!empty($maximum_age_unit)): ?>
                                    <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                    <?php $field_ok = false;
                                endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fieldWrapper">
                    <? $target_size = $passo_6['target_size'] ?? set_value('target_size') ?>
                    <div class="row-input label-line">
                        <div>
                            <label for="id_date_last_enrollment"><?php echo $fieldlabel->get_or_new('target_sample_size'); ?>:</label><span class="fielct-required"></span>
                        </div>
                        <div class="help-input-text">
                            <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                  data-original-title="<?php echo $fieldhelp->get_or_new('target_sample_size'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                        </div>
                        <div class="revision-icons-wrapper"></div>
                    </div>
                    <?php echo form_error('target_size', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                    <div class="row-input input " id="world_target_size">
                        <div class="field-input">
                            <input id="id_target_size"
                                   name="target_size"
                                   type="number"
                                <?php echo @$common_field ?? '' ?>
                                   value="<?php echo $target_size; ?>">
                        </div>
                        <div class="text-center">
                            <?php if(!empty($target_size)): ?>
                                <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                <?php $field_ok = false;
                            endif; ?>
                        </div>
                    </div>
                </div>
                <p class="form-label"><label for="s2id_autogen1"><?php echo $fieldlabel->get_or_new('inclusion_criteria'); ?>:</label></p>
                <div class="help-input-text">
                    <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                          data-original-title="<?php echo $fieldhelp->get_or_new('inclusion_criteria'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                </div>
                <div class="fields-wrapper">
                    <!-- Línguas obrigatórias -->
                    <?php $field_ok = false; ?>
                    <?php foreach ($languages as $language):?>
                        <? $fieldname = 'inclusion_criteria_'.$language; ?>
                        <? $inclusion_criteria = $passo_6[$fieldname] ?? set_value($fieldname); ?>
                        <div class="fieldWrapper">
                            <div class="row-input label-line">
                                <div class="icon_caret">
                                    <span class="glyphicon caret"></span><?php echo $genericlabel->get_or_new('language_'.$language); ?>
                                </div>
                                <div class="revision-icons-wrapper"></div>
                            </div>
                            <?php echo form_error('inclusion_criteria_' . $language, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                            <div class="row-input">
                                <div class="field-input ">
                                    <textarea cols="40"
                                              id="id_inclusion_criteria_<?php echo $language; ?>"
                                              name="<?php echo $fieldname; ?>"
                                              <?php echo @$common_field ?? '' ?>
                                              rows="30"><?php echo $inclusion_criteria; ?></textarea>
                                </div>
                                <div class="text-center">
                                    <?php if($field_ok): ?>
                                        <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                        <?php $field_ok = false;
                                    endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <p class="form-label"><label for="s2id_autogen3"><?php echo $fieldlabel->get_or_new('exclusion_criteria'); ?>:</label></p>
                <div class="help-input-text">
                    <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                          data-original-title="<?php echo $fieldhelp->get_or_new('exclusion_criteria'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                </div>
                <div class="fields-wrapper">
                    <!-- Línguas obrigatórias -->
                    <?php $field_ok = false; ?>
                    <?php foreach ($languages as $language):?>
                        <? $fieldname = 'exclusion_criteria_'.$language; ?>
                        <? $exclusion_criteria = $passo_6[$fieldname] ?? set_value($fieldname); ?>
                        <div class="fieldWrapper">
                            <div class="row-input label-line">
                                <div class="icon_caret">
                                    <span class="glyphicon caret"></span><?php echo $genericlabel->get_or_new('language_'.$language); ?>
                                </div>
                                <div class="revision-icons-wrapper"></div>
                            </div>
                            <?php echo form_error('exclusion_criteria_' . $language, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                            <div class="row-input">
                                <div class="field-input ">
                                                                        <textarea cols="40"
                                                                                  id="id_exclusion_criteria_<?php echo $language; ?>"
                                                                                  name="exclusion_criteria_<?php echo $language; ?>"
                                                                                  <?php echo @$common_field ?? '' ?>
                                                                                  rows="30"><?php echo $exclusion_criteria; ?></textarea>
                                </div>
                                <div class="text-center">
                                    <?php if($field_ok): ?>
                                        <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                        <?php $field_ok = false;
                                    endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <script type="text/javascript">
                    function check_minimun_age() {
                        if($('#id_minimum_age_no_limit').val() == 'True') {
                            $('#id_inclusion_minimum_age').val('').parents('.fieldWrapper').hide();
                            $('#id_minimum_age_unit').val('').parents('.fieldWrapper').hide();
                        }else {
                            $('#id_inclusion_minimum_age').parents('.fieldWrapper').show();
                            $('#id_minimum_age_unit').parents('.fieldWrapper').show();
                        }
                    }
                    function check_maximum_age() {
                        if($('#id_maximum_age_no_limit').val() == 'True') {
                            $('#id_inclusion_maximum_age').val('').parents('.fieldWrapper').hide();
                            $('#id_maximum_age_unit').val('').parents('.fieldWrapper').hide();
                        }else {
                            $('#id_inclusion_maximum_age').parents('.fieldWrapper').show();
                            $('#id_maximum_age_unit').parents('.fieldWrapper').show();
                        }
                    }
                    $('#id_minimum_age_no_limit').on('change', function() {
                        check_minimun_age();
                    });
                    $('#id_maximum_age_no_limit').on('change', function() {
                        check_maximum_age();
                    });
                    check_minimun_age();
                    check_maximum_age();
                </script>
                <div class="submit-line">
                    <?php if (($can_save ?? FALSE) && !($reviewer ?? FALSE)) : ?>
                        <input type="submit"
                               value="<?php echo $genericlabel->get_or_new('button_save'); ?>"
                               class="btn btn-primary pull-right">
                    <?php endif; ?>
                </div>
            </form>
        </div>
        <script type="text/javascript">
            function replaceAll(find, replace, str) {
                return str.replace(new RegExp(find, 'g'), replace);
            }
            // $('[id*=inclusion_criteria], [id*=exclusion_criteria]').select2({
            //         tags:true,
            //         tokenSeparators: [";"],
            //         initSelection : function (element, callback) {
            //                 var data = [];
            //                 var previous_value = element.val();
            //                 element.val("");
            //                 previous_value = replaceAll(',', ';', previous_value);
            //                 $(previous_value.split(";")).each(function () {
            //                         data.push({id: this, text: this});
            //                 });
            //                 callback(data);
            //         }
            // }).on("change", function(e) {
            //         if(e.removed != undefined) {
            //                 $(e.currentTarget).val(replaceAll(e.removed.id.toString()+';', "", $(e.currentTarget).val()));
            //                 $(e.currentTarget).val(replaceAll(e.removed.id.toString()+',', "", $(e.currentTarget).val()));
            //                 $(e.currentTarget).val(replaceAll(e.removed.id.toString(), "", $(e.currentTarget).val()));
            //         }
            // });
            // $('[id*=recruitment_countries]').select2();
            $('.dropdown').dropdown();
        </script>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="field-revision-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="field-registrant-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<?php $genericlabel->disableUpdate(); ?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>