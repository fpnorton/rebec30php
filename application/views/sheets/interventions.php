﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('InterventionForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldHelpLanguage('InterventionForm'); $fieldhelp->enableUpdate(); ?>
<div class="row">
    <div class="col-sm-12 content">
        <h1 class="title_line">
            <div class="row">
                <div class="number-rounded"><? echo PASSO_INTERVENCAO ?></div>
                <span>&nbsp;<?php echo $fieldlabel->get_or_new('main_title'); ?></span>
            </div>
        </h1>
        <div class="row">
            <form role="form"
                  class="col-sm-12"
                  action="<?php echo $_form_action ?>"
                  method="post"
                  accept-charset="utf-8">
                <!-- -->
                <? $languages = $languages ?? set_value('languages[]'); ?>
                <? foreach ($languages as $language) : ?>
                    <input type="hidden" name="languages[]" value="<? echo $language ?>">
                <? endforeach; ?>
                <!-- -->
                <?
                // ############################################################################################################### 
                $piece['passo'] = $passo_5;
                $piece['has_review_remark'] = $has_review_remark;
                $piece['reviewer'] = $reviewer;
                $piece['revisor_field'] = $revisor_field;
                include_once 'piece/remark.php'; 
                // ############################################################################################################### 
                ?>
                <input type="hidden" name="id" value="<?php echo $passo_5['id']; ?>">
                <div class="help-input-text">
                    <span class="field-help"
                          data-html="true" data-toggle="tooltip" data-placement="left"
                          data-original-title="<?php echo $fieldhelp->get_or_new('i_freetext'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                </div>
                <h2 class="subtitle-formset"><?php echo $fieldlabel->get_or_new('intervention'); ?></h2>
                <div class="fields-wrapper">
                    <?php $field_ok = false; ?>
                    <?php foreach ($languages as $language):?>
                        <div class="fieldWrapper">
                            <? $fieldname = 'intervention_'.$language; ?>
                            <? $intervention = $passo_5['intervention_'.$language] ?? set_value($fieldname); ?>
                            <div class="row-input label-line">
                                <div class="icon_caret">
                                    <span class="glyphicon caret"></span><?php echo $genericlabel->get_or_new('language_'.$language); ?>
                                </div>
                                <div class="revision-icons-wrapper"></div>
                            </div>
                            <?php echo form_error('intervention_' . $language, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                            <div class="row-input">
                                <div class="field-input ">
									<textarea cols="40" id="id_intervention_<?php echo $language; ?>"
                                              name="<?php echo $fieldname; ?>"
                                              <?php echo @$common_field ?? '' ?>
                                              rows="10"><?php echo $intervention; ?></textarea>
                                </div>
                                <div class="text-center">
                                    <?php if($field_ok): ?>
                                        <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                        <?php $field_ok = false; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="fieldWrapper">
                    <div class="row-input label-line">
                        <div class="no-margin-left">
                            <label for="intervention_codes"><?php echo $fieldlabel->get_or_new('intervention_categories'); ?>:</label>
                        </div>
                        <div class="help-input-text intervention_codes">
                            <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                  data-original-title="<?php echo $fieldhelp->get_or_new('i_code'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                        </div>
                        <div class="revision-icons-wrapper"></div>
                    </div>
                    <div class="row-input input error-intervention_codes" style="height: 445px;">
                        <div class="field-input">
                            <ul id="id_intervention_codes">
                                <?php foreach($VD['genData']['interventioncode'] as $ic) : ?>
                                    <? $fieldname = 'intervention_codes_'.$ic->id; ?>
                                    <? $intervention_code_checked = $passo_5[$fieldname] ?? set_value($fieldname); ?>
                                    <?php if (linguagem_selecionada() == 'en') :?>
                                        <?php $translation = $ic; ?>
                                    <?php else : ?>
                                        <?php $translation = $ic->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                    <?php endif; ?>
                                    <li><label for="id_intervention_codes_<?php echo $ic->id; ?>">
                                            <?php if (empty(@$intervention_code_checked)) : ?>
                                                <input id="id_intervention_codes_<?php echo $ic->id; ?>"
                                                       name="<?php echo $fieldname; ?>"
                                                       value="<?php echo $ic->id; ?>"
                                                    <?php echo @$common_field ?? '' ?>
                                                       type="checkbox" /><?php echo $translation->label; ?>
                                            <?php else : ?>
                                                <input id="id_intervention_codes_<?php echo $ic->id; ?>"
                                                       name="<?php echo $fieldname; ?>"
                                                       value="<?php echo $ic->id; ?>"
                                                       checked="checked"
                                                    <?php echo @$common_field ?? '' ?>
                                                       type="checkbox" /><?php echo $translation->label; ?>
                                            <?php endif; ?>
                                        </label>
                                        <input type="hidden"
                                               name="intervention_codes_<?php echo $ic->id; ?>_id"
                                            <?php echo @$common_field ?? '' ?>
                                               value="<?php echo @$intervention_code_checked; ?>"/>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="text-center">
                        </div>
                    </div>
                </div>
                <h2 class="subtitle-formset" for="descriptor_set"><?php echo lang('descriptors'); ?></h2>
                <table style="border-collapse: collapse; border: none; width: 100%;"
                       id="dynamic_field_interventions">
                    <?php
                    $count_ic = $passo_5['count_ic'] ?? set_value('count_ic');
                    $interventions_descriptor_matrix = [
                        0 => [],
                    ];
                    for ($count = 1; $count <= $count_ic; $count++) {
                        $matrix = [
                            'id' => $passo_5['descriptor_set-'.$count.'-id'] ?? set_value('descriptor_set-'.$count.'-id'),
                            'type' => $passo_5['descriptor_set-'.$count.'-descriptor_type'] ?? set_value('descriptor_set-'.$count.'-descriptor_type'),
                            'parent_vocabulary' => $passo_5['descriptor_set-'.$count.'-parent_vocabulary'] ?? set_value('descriptor_set-'.$count.'-parent_vocabulary'),
                            'code' => $passo_5['descriptor_set-'.$count.'-vocabulary_code'] ?? set_value('descriptor_set-'.$count.'-vocabulary_code'),
                        ];
                        foreach ($languages as $language) {
                            $matrix['vocabulary_id_'.$language] = @$passo_5['descriptor_set-'.$count.'-vocabulary_id_'.$language] ?? set_value('descriptor_set-'.$count.'-vocabulary_id_'.$language);
                            $matrix['vocabulary_item_'.$language] = @$passo_5['descriptor_set-'.$count.'-vocabulary_item_'.$language] ?? set_value('descriptor_set-'.$count.'-vocabulary_item_'.$language);
                        }
                        $interventions_descriptor_matrix[] = $matrix;
                    }
                    if ($count_ic == 0) $interventions_descriptor_matrix[1] = [];
//                    echo '<pre>'.var_export([
//                            __FILE__ => __LINE__,
//                            '$interventions_descriptor_matrix' => $interventions_descriptor_matrix,
//                            '$passo_5' => $passo_5,
//                        ], TRUE).'</pre>';
                    $index = 0;
                    ?>
                    <?php foreach($interventions_descriptor_matrix as $key_matrix => $matrix) : ?>
                        <?php $index = ($key_matrix == 0) ? '__prefix__' : $key_matrix; ?>
                        <tr id="row<?php echo $index; ?>">
                            <td>
                                <div class="inline descriptor_set dynamic-form"
                                    <?php if ($key_matrix == 0) : ?>
                                        style="display:none;"
                                    <?php endif; ?>
                                     id="dynamic_interventions_<?php echo $index; ?>">
                                    <input id="id_descriptor_set-<?php echo $index; ?>-id" name="descriptor_set-<?php echo $index; ?>-id" type="hidden" value="<?php echo $matrix['id']; ?>"/>
                                    <div class="fieldWrapper">
                                        <div class="row-input label-line <?php if ($key_matrix != 0) echo 'error-descriptor_set'; ?>">
                                            <div class="no-margin-left">
                                                <label for="id_descriptor_set-<?php echo $index; ?>-descriptor_type"><?php echo $fieldlabel->get_or_new('type_descriptor'); ?>:</label><span class="fielct-required"></span>
                                            </div>
                                            <div class="help-input-text">
                                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                                      data-original-title="<?php echo $fieldhelp->get_or_new('type'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                                            </div>
                                        </div>
                                        <?php echo form_error('descriptor_set-' . $index . '-descriptor_type', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                        <div class="row-input input <?php if ($key_matrix != 0) echo 'error-descriptor_set'; ?>">
                                            <div class="field-input">
                                                <select id="id_descriptor_set-<?php echo $index; ?>-descriptor_type"
                                                    <?php echo @$common_field ?? '' ?>
                                                        name="descriptor_set-<?php echo $index; ?>-descriptor_type">
                                                    <?php if ($key_matrix == 0) : ?>
                                                        <option value="" selected="selected"></option>
                                                        <option value="G"><?php echo $fieldlabel->get_or_new('descriptor_type_general'); ?></option>
                                                        <option value="S"><?php echo $fieldlabel->get_or_new('descriptor_type_specific'); ?></option>
                                                    <?php else : ?>
                                                        <option value=""></option>
                                                        <?php if ($matrix['type'] == 'G') : ?>
                                                            <option value="G" selected="selected"><?php echo $fieldlabel->get_or_new('descriptor_type_general'); ?></option>
                                                        <?php else : ?>
                                                            <option value="G"><?php echo $fieldlabel->get_or_new('descriptor_type_general'); ?></option>
                                                        <?php endif; ?>
                                                        <?php if ($matrix['type'] == 'S') : ?>
                                                            <option value="S" selected="selected"><?php echo $fieldlabel->get_or_new('descriptor_type_specific'); ?></option>
                                                        <?php else : ?>
                                                            <option value="S"><?php echo $fieldlabel->get_or_new('descriptor_type_specific'); ?></option>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fieldWrapper">
                                        <div class="row-input label-line <?php if ($key_matrix != 0) echo 'error-descriptor_set'; ?>">
                                            <div class="no-margin-left">
                                                <label for="id_descriptor_set-<?php echo $index; ?>-parent_vocabulary"><?php echo $fieldlabel->get_or_new('parent_vocabulary'); ?>:</label><span class="fielct-required"></span>
                                            </div>
                                            <div class="help-input-text">
                                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                                      data-original-title="<?php echo $fieldhelp->get_or_new('vocabulary'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                                            </div>
                                        </div>
                                        <?php echo form_error('descriptor_set-' . $index . '-parent_vocabulary', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                        <div class="row-input input <?php if ($key_matrix != 0) echo 'error-descriptor_set'; ?>">
                                            <div class="field-input">
                                                <select id="id_descriptor_set-<?php echo $index; ?>-parent_vocabulary"
                                                    <?php echo @$common_field ?? '' ?>
                                                        name="descriptor_set-<?php echo $index; ?>-parent_vocabulary">
                                                    <?php if ($key_matrix == 0) : ?>
                                                        <option value="" selected="selected"></option>
                                                        <option value="CID-10">CID-10</option>
                                                        <option value="DeCS">DeCS</option>
                                                    <?php else : ?>
                                                        <option value=""></option>
                                                        <?php if ($matrix['parent_vocabulary'] == 'CID-10') : ?>
                                                            <option value="CID-10" selected="selected">CID-10</option>
                                                        <?php else : ?>
                                                            <option value="CID-10">CID-10</option>
                                                        <?php endif; ?>
                                                        <?php if ($matrix['parent_vocabulary'] == 'DeCS') : ?>
                                                            <option value="DeCS" selected="selected">DeCS</option>
                                                        <?php else : ?>
                                                            <option value="DeCS">DeCS</option>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div    style="<? if (empty($matrix['parent_vocabulary'])) echo 'display: none; '; ?>"
                                            class="fieldWrapper vocabulary_item_wrapper vocabulary_item_wrapper-vocabulary_code" >
                                        <div class="row-input label-line <?php if ($key_matrix != 0) echo 'error-descriptor_set'; ?>">
                                            <div class="no-margin-left">
                                                <label for="id_descriptor_set-<?php echo $index; ?>-vocabulary_code"><?php echo $fieldlabel->get_or_new('vocabulary_code'); ?>:</label><span class="fielct-required"></span>
                                            </div>
                                            <div class="help-input-text">
                                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                                      data-original-title="<?php echo $fieldhelp->get_or_new('vocabulary_code'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>">
                                                </span>
                                            </div>
                                        </div>
                                        <?php echo form_error('descriptor_set-' . $index . '-vocabulary_code', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                        <div class="row-input input <?php if ($key_matrix != 0) echo 'error-descriptor_set'; ?>">
                                            <div class="field-input">
                                                <input id="id_descriptor_set-<?php echo $index; ?>-vocabulary_code"
                                                       maxlength="40"
                                                       name="descriptor_set-<?php echo $index; ?>-vocabulary_code"
                                                    <?php echo @$common_field ?? '' ?>
                                                       value="<?php echo @$matrix['code']; ?>"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <?php foreach ($languages as $language):?>
                                        <div
                                            style="<? if (empty($matrix['parent_vocabulary'])) echo 'display: none; '; ?>"
                                            class="fieldWrapper vocabulary_item_wrapper vocabulary_item_wrapper-vocabulary_item_<?php echo $language; ?>">
                                            <div class="row-input label-line <?php if ($key_matrix != 0) echo 'error-descriptor_set'; ?>">
                                                <div class="no-margin-left">
                                                    <label for="id_descriptor_set-<?php echo $index; ?>-vocabulary_item_<?php echo $language; ?>"><?php echo $fieldlabel->get_or_new('item_vocabulary_' . $language); ?>:</label>
                                                    <span class="fielct-required"></span>
                                                    <input id="id_descriptor_set-<?php echo $index; ?>-vocabulary_id_<?php echo $language; ?>"
                                                           name="descriptor_set-<?php echo $index; ?>-vocabulary_id_<?php echo $language; ?>"
                                                           type="hidden"
                                                        <?php echo @$common_field ?? '' ?>
                                                           value="<?php echo @$matrix['vocabulary_id_'.$language]; ?>"/>
                                                </div>
                                                <div class="help-input-text"></div>
                                            </div>
                                            <?php echo form_error('descriptor_set-' . $index . '-vocabulary_item_' . $language, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                            <div class="row-input input <?php if ($key_matrix != 0) echo 'error-descriptor_set'; ?>">
                                                <div class="field-input">
                                                    <input id="id_descriptor_set-<?php echo $index; ?>-vocabulary_item_<?php echo $language; ?>"
                                                           maxlength="4000"
                                                           name="descriptor_set-<?php echo $index; ?>-vocabulary_item_<?php echo $language; ?>"
                                                           type="text"
                                                        <?php echo @$common_field ?? '' ?>
                                                           value="<?php echo @$matrix['vocabulary_item_'.$language]; ?>">
                                                    <!-- a class="search-modal-button" data-toggle="modal"><?php echo lang("search"); ?></a -->
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                    <?php if (empty($common_field)) : ?>
                                    <div class="fieldWrapper">
                                        <div class="row-input label-line">
                                            <div class="no-margin-left"></div>
                                            <div class="help-input-text"></div>
                                        </div>
                                        <div class="row-input input">
                                            <div class="field-input">
                                                <input id="id_descriptor_set-<?php echo $index; ?>-delete"
                                                       name="descriptor_set-<?php echo $index; ?>-delete"
                                                    <?php echo @$common_field ?? '' ?>
                                                       type="checkbox">
                                                <label for="id_descriptor_set-<?php echo $index; ?>-delete"><?php echo $genericlabel->get_or_new('remove'); ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>

                <input id="id_count_ic" name="count_ic" type="hidden" value="<?php echo $index; ?>" />
                <input id="id_deleted_ic" name="deleted_ic" value="" type="hidden" />
                <?php if (empty($common_field)) : ?>
                <a class="add-row" id="add_interventions" name="add"><?php echo $genericlabel->get_or_new('button_add_another'); ?></a>
                <?php endif; ?>
                <div class="submit-line">
                    <?php if (($can_save ?? FALSE) && !($reviewer ?? FALSE)) : ?>
                        <input type="submit"
                               value="<?php echo $genericlabel->get_or_new('button_save'); ?>"
                               class="btn btn-primary pull-right">
                    <?php endif; ?>
                </div>
            </form>
        </div>
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="vocabulary-search">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h1 onclick="$(&#39;#cid10_search_input&#39;)[0].focus();"><?php echo lang('search_vocabulary'); ?></h1>
                    </div>
                    <div id="cid10-table">
                        <label for="cid10_search_input"><?php echo lang('type_press_enter'); ?></label>
                        <input type="text" id="cid10_search_input">
                        <h2><?php echo lang('search_results'); ?></h2>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><?php echo lang('id_tree'); ?></th>
                                <th><?php echo lang('en'); ?></th>
                                <th><?php echo lang('es'); ?></th>
                                <th><?php echo lang('pt-br'); ?></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div id="decs-table">
                        <label for="decs_search_input"><?php echo lang('type_press_enter'); ?></label>
                        <input type="text" id="decs_search_input">
                        <h2><?php echo lang('search_results'); ?></h2>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><?php echo lang('id_tree'); ?></th>
                                <th><?php echo lang('en'); ?></th>
                                <th><?php echo lang('es'); ?></th>
                                <th><?php echo lang('pt-br'); ?></th>
                                <th>Definição</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    $(document).on("click", "#add_interventions", function(){
                        var count = $('#id_count_ic').val();
                        $('#dynamic_field_interventions').append('<tr id="row'+count+'"><td><div class="inline descriptor_set dynamic-form" id="dynamic_interventions_'+count+'">'+ document.getElementById('dynamic_interventions___prefix__').innerHTML.split('__prefix__').join(count) +'</div></td></tr>');
                        count++;
                        $('#id_count_ic').val(count);
                    });
                    // $(document).on("change", "input[id*=-delete]", function(){
                    //     var button_id = $(this).attr("id").split("-")[1];
                    //     var attach_id = $('#id_descriptor_set-'+button_id+'-id').value;
                    //     if(attach_id != 0)
                    //     {
                    //         if($('#id_deleted_ic').val() == "")
                    //         {
                    //             $('#id_deleted_ic').val(attach_id);
                    //         }
                    //         else
                    //         {
                    //             $('#id_deleted_ic').val($('#id_deleted_hc').val() + ';' + attach_id);
                    //         }
                    //     }
                    //     $('#row'+button_id+'').remove();
                    // });
                });
            </script>

            <script type="text/javascript">
                //Trigger event to choose an item
                (function() {
                    $('#decs-table, #cid10-table').delegate('.choose-button', 'click', function() {
                        var value = $(this).parents('tr:eq(0)').find('td:eq(0)').html() + ' - ';
                        var code = value;
                        var english_value = code + $(this).parents('tr:eq(0)').find('td.en-lang').html();
                        var spanish_value = code + $(this).parents('tr:eq(0)').find('td.es-lang').html();
                        var portuguese_value = code + $(this).parents('tr:eq(0)').find('td.pt-br-lang').html();
                        current_input_modal_en.val(english_value);
                        current_input_modal_es.val(spanish_value);
                        current_input_modal_pt.val(portuguese_value);
                        $('#vocabulary-search').modal('hide');
                        current_input_modal_en = null;
                        current_input_modal_es = null;
                        current_input_modal_pt = null;
                    });
                })();
                //DECS javascript procedures
                (function() {
                    var decs_url = "<?php echo base_url('index.php/clinical_trial/get_decs/'); ?>";
                    var decs_table_body = $('#decs-table tbody');
                    var search_decs = function(term) {
                        decs_table_body.html("<tr><td colspan=\"5\"><?php echo lang('searching'); ?>...</td></tr>");
                        $('#decs-table').show();
                        $('#cid10-table').hide();

                        $.ajax({
                            type: 'POST',
                            url: decs_url+term,
                            contentType: 'application/json',
                            success: function(data){
                                var content = JSON.parse(data);
                                decs_table_body.html('');
                                var current;
                                for(var i=0;i<content.length;i++) {
                                    current = content[i];
                                    decs_table_body.append('<tr><td>'+(current.tree_id||'')+'</td><td class="en-lang">'+(current.descriptor_en||'')+'</td><td class="es-lang">'+(current.descriptor_es||'')+'</td><td class="pt-br-lang">'+(current.descriptor_pt||'')+'</td><td>'+(current.definition||'')+'</td><td><button class="button choose-button">Escolha</buton></td></tr>');
                                }
                                if(content.length == 0) {
                                    decs_table_body.html("<tr><td colspan=\"5\"><?php echo lang('no_results'); ?>...</td></tr>");
                                }
                            },
                            error: function() {
                                decs_table_body.html("<tr><td colspan=\"3\"><b style=\"color:red;\"><?php echo lang('not_available'); ?></b></td></tr>");
                            }
                        });
                    }
                    $("#decs_search_input").keyup(function (e) {
                        if (e.keyCode == 13) {
                            search_decs($("#decs_search_input").val());
                        }
                    });
                })();
                //CID-10 javascript procedures
                (function() {
                    var cid10_url = "<?php echo base_url('index.php/clinical_trial/get_cid10/'); ?>";
                    var cid10_table_body = $('#cid10-table tbody');
                    var search_cid10 = function(term) {
                        cid10_table_body.html("<tr><td colspan=\"5\"><?php echo lang('searching'); ?>...</td></tr>");
                        $('#decs-table').hide();
                        $('#cid10-table').show();

                        $.ajax({
                            type: 'POST',
                            url: cid10_url+term,
                            contentType: 'application/json',
                            success: function(data){
                                var content = JSON.parse(data);
                                cid10_table_body.html('');
                                var current;
                                for(var i=0;i<content.length;i++) {
                                    current = content[i];
                                    cid10_table_body.append('<tr><td>'+(current.tree_id||'')+'</td><td class="en-lang">'+(current.descriptor_en||'')+'</td><td class="es-lang">'+(current.descriptor_es||'')+'</td><td class="pt-br-lang">'+(current.descriptor_pt||'')+'</td><td><button class="button choose-button">Escolha</buton></td></tr>');
                                }
                                if(content.length == 0) {
                                    cid10_table_body.html("<tr><td colspan=\"5\"><?php echo lang('no_results'); ?>...</td></tr>");
                                }
                            },
                            error: function() {
                                cid10_table_body.html("<tr><td colspan=\"3\"><b style=\"color:red;\"><?php echo lang('not_available'); ?></b></td></tr>");
                            }
                        });
                    }
                    $("#cid10_search_input").keyup(function (e) {
                        if (e.keyCode == 13) {
                            search_cid10($("#cid10_search_input").val());
                        }
                    });
                })();
            </script>
        </div>
        <style>
            /*.vocabulary_item_wrapper {*/
            /*    display: none;*/
            /*}*/
        </style>
        <script type="text/javascript">
            var current_input_modal;
            // $('body').delegate('.search-modal-button', 'click', function() {
            //     current_input_modal_en = $(this).parents('.form-row:eq(0)').find('[id*=vocabulary_item_en]');
            //     current_input_modal_es = $(this).parents('.form-row:eq(0)').find('[id*=vocabulary_item_es]');
            //     current_input_modal_pt = $(this).parents('.form-row:eq(0)').find('[id*=vocabulary_item_pt-br]');
            //     $('#cid10-table').hide().find('tbody').html('');
            //     $('#decs-table').hide().find('tbody').html('');
            //     $('#decs_search_input').val('');
            //     $('#cid10_search_input').val('');
            //     if($(this).parents('.form-row:eq(0)').find('select[id*=parent_vocabulary]:eq(0)').val() == 'CID-10') {
            //         $('#cid10-table').show();
            //         $('#vocabulary-search').on('shown.bs.modal', function (e) {
            //             $('#cid10_search_input').focus();
            //         });
            //     }
            //     else {
            //         $('#decs-table').show();
            //         $('#vocabulary-search').on('shown.bs.modal', function (e) {
            //             $('#decs_search_input').focus();
            //         });
            //     }
            //     $('#vocabulary-search').modal('show');
            // });
            // $('[id*=-parent_vocabulary]').each(function(){
            //     if($(this).val()) {
            //         $(this).parents('.dynamic-form').find('.vocabulary_item_wrapper').hide();
            //         $(this).parents('.dynamic-form').find('.vocabulary_item_wrapper-vocabulary_item_pt-br').show();
            //     }
            // });
            $('body').delegate('[id*=-parent_vocabulary]', 'change', function(){
                if($(this).val()) {
                    $(this).parents('.dynamic-form').find('.vocabulary_item_wrapper').show();
                }
                else {
                    $(this).parents('.dynamic-form').find('.vocabulary_item_wrapper').hide().find('input').val('');
                }
            });
        </script>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="field-revision-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="field-registrant-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>