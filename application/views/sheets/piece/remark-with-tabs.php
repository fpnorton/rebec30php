<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<!-- ############################################################################################################### -->
<?php $review_remarks = $piece['passo']['review_remarks']; ?>
<?php $review_remark = $piece['passo']['review_remark']; ?>
<?php $review_remark_text = $review_remark['text']; ?>
<?php if ($piece['reviewer'] or (($piece['has_review_remark'] ?? FALSE) && !empty($review_remark_text))) : ?>
    <p class="form-label"><label><?php echo $genericlabel->get_or_new('remark_notes'); ?>:</label></p>
    <div class="fields-wrapper">
        <div class="fieldWrapper">
            <div class="row-input label-line">
                <div class="icon_caret">
                    <span class="glyphicon caret"></span>
                </div>
                <div class="revision-icons-wrapper"></div>
            </div>
            <div class="row-input">
                <div class="field-input">
                    <? if ($piece['reviewer']) : ?>
                    <div class="tab">
                        <button type='button' class="tablinks btn btn-light active" onclick="showRemark(event, 'remark_0'); return false;"><?php echo $genericlabel->get_or_new('remark_recent'); ?></button>
                        <?php foreach($review_remarks as $remark) : ?>
                        <button type='button' class="tablinks btn btn-light" onclick="showRemark(event, 'remark_<?php echo $remark['id']; ?>'); return false;"><?php echo str_replace(' ', '<br/>', $remark['created']); ?></button>
                        <?php endforeach; ?>
                    </div>
                    <? endif; ?>
                    <div>
                        <textarea cols="40"
                                id="remark_0" 
                                class="tabcontent"
                                style="height: 100px;"
                                <?php echo @$piece['revisor_field'] ?? '' ?>
                                name="review_remark"
                                rows="40"><?php echo $review_remark; ?></textarea>
                    <? if ($piece['reviewer']) : ?>
                    <?php foreach($review_remarks as $remark) : ?>
                        <textarea cols="40"
                                id="remark_<?php echo $remark['id']; ?>" 
                                class="tabcontent" 
                                style="height: 100px; display: none;"
                                <?php echo @$piece['revisor_field'] ?? '' ?>
                                disabled='disabled' 
                                readonly='readonly'
                                rows="40"><?php echo $remark['text']; ?></textarea>
                    <?php endforeach; ?>
                    <? endif; ?>
                    </div>
                </div>
                <div class="text-center">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10">&nbsp;
            </div>
            <div class="col-sm-2">
                <?php if ($piece['reviewer'] ?? FALSE) : ?>
                <input type="submit"
                        value="<?php echo $genericlabel->get_or_new('button_save'); ?>"
                        class="btn btn-primary">
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<!-- ############################################################################################################### -->
