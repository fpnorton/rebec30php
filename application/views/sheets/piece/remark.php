<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php
// echo '<pre>'.var_export([
//    __FILE__ => __LINE__,
//    '$piece[passo]' => $piece['passo'],
// ], TRUE).'</pre>';
?>
<!-- ############################################################################################################### -->
<?php $review_remarks = $piece['passo']['review_remarks']; ?>
<?php $review_remark = $piece['passo']['review_remark']; ?>
<?php $review_remark_last = $piece['passo']['review_remark_last']; ?>
<?php $review_remark_text = $review_remark['text']; ?>
<?php $review_remark_lastest = $piece['passo']['review_remark_lastest']; ?>
<input type="hidden" name="info_submission_id" value="<?php echo $piece['passo']['submission_id'] ?>"/>
<?php if 
        ($piece['reviewer'] or (
            ($piece['has_review_remark'] ?? FALSE) and 
            !is_null($review_remark_lastest) and 
            !empty($review_remark_lastest->text)
        )) : ?>
    <p class="form-label"><label><?php echo $genericlabel->get_or_new('remark_notes'); ?>:</label></p>
    <div class="fields-wrapper">
        <div class="fieldWrapper">
            <div class="row-input label-line">
                <div class="icon_caret">
                    <span class="glyphicon caret"></span>
                </div>
                <div class="revision-icons-wrapper"></div>
            </div>
            <div class="row-input">
                <div class="field-input">
                    <div>
                    <? if (is_null($review_remark_lastest) or (!is_null($review_remark_lastest) and ($review_remark_lastest->status == 'open'))) : ?>
                        <textarea cols="40"
                                class="tabcontent"
                                style="height: 200px;"
                                <?php echo @$piece['revisor_field'] ?? '' ?>
                                name="review_remark"
                                rows="40"><?php echo (is_null($review_remark_lastest) ? '' : $review_remark_lastest->text); ?></textarea>
                    <? else : ?>
                        <input type="hidden" name="review_remark" value="<?php echo $review_remark_last; ?>"/>
                        <textarea cols="40"
                                class="tabcontent"
                                style="height: 200px;"
                                <?php echo @$piece['revisor_field'] ?? '' ?>
                                disabled='disabled' 
                                readonly='readonly'
                                rows="40"><?php echo $review_remark_lastest->text; ?></textarea>
                    <? endif; ?>
                    </div>
                </div>
                <div class="text-center">
                </div>
            </div>
        </div>
        <?php if ($piece['reviewer'] ?? FALSE) : ?>
        <div class="row">
            <? if (is_null($review_remark_lastest) or ($review_remark_lastest->status == 'open')) : ?>
                <div class="col-sm-10">&nbsp;
                </div>
                <div class="col-sm-2">
                    <input type="submit"
                            name="button_action_save"
                            value="<?php echo $genericlabel->get_or_new('button_update'); ?>"
                            class="btn btn-primary">
                </div>
            <? elseif ($review_remark_lastest->status != 'open') : ?>
                <div class="col-sm-2">&nbsp;
                    <? if ($review_remark_lastest->status == 'acknowledged') : ?>
                    <input type="submit"
                            name="button_action_approve"
                            value="<?php echo $genericlabel->get_or_new('button_approve'); ?>"
                            class="btn btn-success">
                    <?php endif; ?>
                    <? if ($review_remark_lastest->status == 'closed') : ?>
                    <input type="submit"
                            name="button_action_none"
                            value="<?php echo $genericlabel->get_or_new('button_approve'); ?>"
                            disabled='disabled' 
                            readonly='readonly'
                            style="background-color: white !important;"
                            class="btn btn-default">
                    <?php endif; ?>
                </div>
                <div class="col-sm-2">&nbsp;
                    <input type="submit"
                            name="button_action_reopen"
                            value="<?php echo $genericlabel->get_or_new('button_reopen'); ?>"
                            class="btn btn-warning">
                </div>
                <div class="col-sm-8">
                </div>
            <? endif; ?>
        </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
<!-- ############################################################################################################### -->
