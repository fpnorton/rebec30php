﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('TermForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldHelpLanguage('TermForm'); $fieldhelp->enableUpdate(); ?>
<?php $terms_of_use = new Helper\FlatPageAssist('/terms-of-use/'); ?>
<div class="row " >
    <div class="col-sm-12 content" >
        <div class="box">
            <div class="box-header">
                <h3 class="col-xs-11 box-title"><?php echo $fieldlabel->get_or_new('main_title'); ?></h3>
            </div>
            <hr/>
            <div style="padding: 30px !important;">
                <form role="form"
                      class="col-sm-12"
                      action="<?php echo base_url(__FORM_ACTION__ ?? '') ?>"
                      method="post"
                      accept-charset="utf-8">
                    <!-- -->
                    <div class="row-input h2">
                        <label for="id_study_type"><?php echo $fieldlabel->get_or_new('languages'); ?>:</label><span class="fielct-required "></span>
                        <div class="help-input-text"></div>
                    </div>
                    <div class="row h3">
                        <?php foreach (\Helper\Language::$available as $language_key => $language_value): ?>
                            <div class="fieldWrapper"
                                 id="language_<?php echo $language_key; ?>">
                                <div class="input row-input  ">
                                    <div class="field-input">
                                        <input <?php if ($language_value['default']) : ?>
                                               checked="checked"
                                               disabled
                                               <?php else : ?>
                                               name="language_<?php echo $language_key; ?>"
                                               <?php endif; ?>
                                               value="on"
                                               type="checkbox" />
                                        <?php if ($language_value['default']) : ?>
                                        <input name="language_<?php echo $language_key; ?>"
                                               value="on"
                                               type="hidden" />
                                        <?php endif; ?>
                                        <label for="id_language_<?php echo $language_key; ?>"><?php echo $genericlabel->get_or_new('language_'.$language_key); ?></label>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- -->
                    <div class="row" style="background-color: #EBEBEA !important; padding-top: 30px !important;">
                        <input type="hidden" name="id" value="<?php echo $clinical_trial['id']; ?>">
                        <input id="id_accept_modal" name="accept_modal" type="hidden">
                        <h2><?php echo $terms_of_use->_title; ?></h2>
                    </div>
                    <div class="row" style="background-color: #EBEBEA !important; padding-top: 30px !important;">
                        <div class="col-md-10 h3 text-justify bg-gray"
                             style="padding: 30px !important;"
                             id="terms_of_use"><?php echo $terms_of_use->_content; ?></div>
                    </div>
                    <div class="row h3" style="background-color: #EBEBEA !important; padding-top: 30px !important;">
                        <div class="help-input-text col-sm-6">
                            <?php echo form_error('accept_terms', '<div class="input row-input with-error">', '<img src="' . base_url('assets/images/field-error.png') . '" align="right"></div>'); ?>
                        </div>
                        <div class="input col-sm-6 pull-right">
                            <input id="id_accept_terms" name="accept_terms" type="checkbox" <?php echo set_checkbox('accept_terms'); ?>>
                            <label for="id_accept_terms"><?php echo $fieldlabel->get_or_new('accept'); ?></label>
                            <div class="text-center"></div>
                        </div>
                        <div class="submit-line pull-right col-sm-12">
                            <input id="save_ct"
                                   class="btn btn-secondary btn-lg"
                                   type="submit"
                                   value="<?php echo $genericlabel->get_or_new('button_confirm'); ?>" />
                            <!-- <button type="button" id="save_ct" class="button" onclick='$("#modal-confirm-ct").show()'>Salvar</button>  -->
                        </div>
                    </div>
                    <div class="modal fade" id="modal-confirm-ct">
                        <div class="modal-dialog modal-sm" style="max-width: 65%;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title"><?php echo $fieldhelp->get_or_new('title_terms_of_use'); ?></h4>
                                </div>
                                <div class="modal-body">
                                    <p><?php echo $fieldlabel->get_or_new('accept_text'); ?></p>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit"
                                           name="button_accept_modal"
                                           value="<?php echo $genericlabel->get_or_new('button_save'); ?>"
                                           class="button"
                                           style="margin-bottom:0;">
                                    <button type="button"
                                            class="button"
                                            data-dismiss="modal"
                                            style="margin-bottom:0;"><?php echo $genericlabel->get_or_new('button_close'); ?>
                                    </button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function showstuff(boxid){
        // alert('entrei');
        $('body').modal-open;
    }
</script>
<?php $genericlabel->disableUpdate(); ?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>