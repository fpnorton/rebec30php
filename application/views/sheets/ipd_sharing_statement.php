﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('IPDSharingStatementForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldHelpLanguage('IPDSharingStatementForm'); $fieldhelp->enableUpdate(); ?>
    <div class="row">
        <div class="col-sm-12 content">
            <h1 class="title_line">
                <div class="row">
                    <div class="number-rounded"><? echo PASSO_TERMO_COMPARTILHAMENTO_DADOS_INDIVIDUAIS ?></div>
                    <span>&nbsp;<?php echo $fieldlabel->get_or_new('main_title'); ?></span>
                </div>
            </h1>
            <div class="row">
                <form role="form"
                      class="col-sm-12"
                      action="<?php echo $_form_action ?>"
                      method="post"
                      accept-charset="utf-8">
                    <!-- -->
                    <? $languages = $languages ?? set_value('languages[]'); ?>
                    <? foreach ($languages as $language) : ?>
                        <input type="hidden" name="languages[]" value="<? echo $language ?>">
                    <? endforeach; ?>
                    <!-- -->
                    <?
                    // ############################################################################################################### 
                    $piece['passo'] = $passo_11;
                    $piece['has_review_remark'] = $has_review_remark;
                    $piece['reviewer'] = $reviewer;
                    $piece['revisor_field'] = $revisor_field;
                    include_once 'piece/remark.php'; 
                    // ############################################################################################################### 
                    ?>
                    <div class="fieldWrapper" id="results_ipd_plan">
                        <? $fieldname = 'results_ipd_plan_id'; ?>
                        <? $results_ipd_plan_id = $passo_11[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input">
                            <label for="id_results_ipd_plan"><?php echo $fieldlabel->get_or_new('results_ipd_plan'); ?>:</label><span class="fielct-required "></span>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('results_ipd_plan'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                        </div>
                        <?php echo form_error('results_ipd_plan', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="input row-input error-id_results_ipd_plan">
                            <div class="field-input">
                                <select id="id_results_ipd_plan"
                                    <?php echo @$common_field ?? '' ?>
                                        name="results_ipd_plan_id">
                                    <option value=""></option>
                                    <?php foreach($VD['genData']['ipd_plan'] as $type): ?>
                                        <?php if (linguagem_selecionada() == 'en') : ?>
                                            <?php $translation = $type; ?>
                                        <?php else : ?>
                                            <?php $translation = $type->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                        <?php endif; ?>
                                        <option value="<?php echo $type->id; ?>" <?php echo ($type->id == $results_ipd_plan_id ? 'selected="selected"' : ''); ?>><?php echo $translation->label; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="text-right"></div>
                        </div>
                    </div>
                    <!-- ############################################################################################################### -->
                    <p class="form-label results_ipd_description"><label for="id_form-0-results_ipd_description"><?php echo $fieldlabel->get_or_new('results_ipd_description'); ?>:</label></p><span class="fielct-required "></span>
                    <div class="help-input-text">
                        <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                              data-original-title="<?php echo $fieldhelp->get_or_new('results_ipd_description'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                    </div>
                    <div class="fields-wrapper">
                        <!-- Línguas obrigatórias -->
                        <?php $field_ok = false; ?>
                        <?php foreach ($passo_11['selected_languages'] as $language):?>

                            <div class="fieldWrapper">
                                <div class="row-input label-line">
                                    <div class="icon_caret">
                                        <span class="glyphicon caret"></span><?php echo $genericlabel->get_or_new('language_'.$language); ?>
                                    </div>
                                    <div class="revision-icons-wrapper"></div>
                                </div>
                                <?php echo form_error('results_ipd_description_' . $language, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                <div class="row-input">
                                    <div class="field-input ">
                                    <textarea cols="40"
                                              id="id_results_ipd_description_<?php echo $language; ?>"
                                              name="results_ipd_description_<?php echo $language; ?>"
                                              <?php echo @$common_field ?? '' ?>
                                              rows="10"><?php echo $passo_11['results_ipd_description_'.$language]; ?></textarea>
                                    </div>
                                    <div class="text-center">
                                        <?php if($field_ok): ?>
                                            <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                            <?php $field_ok = false;
                                        endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- ############################################################################################################### -->
                    <div class="submit-line">
                        <?php if (($can_save ?? FALSE) && !($reviewer ?? FALSE)) : ?>
                            <input type="submit"
                                   value="<?php echo $genericlabel->get_or_new('button_save'); ?>"
                                   class="btn btn-primary pull-right">
                        <?php endif; ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $genericlabel->disableUpdate(); ?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>