﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('IdentificationForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldHelpLanguage('IdentificationForm'); $fieldhelp->enableUpdate(); ?>
<?php
// echo '<pre>'.var_export(array(
//     __FILE__ => __LINE__,
//     '$has_review_remark' => $has_review_remark,
//     '$reviewer' => $reviewer,
// ), true).'</pre>';
?>
<div class="row">
    <div class="col-sm-12 content">
        <h1 class="title_line">
            <div class="row">
                <div class="number-rounded"><? echo PASSO_IDENTIFICACAO ?></div>
                <span>&nbsp;<?php echo $fieldlabel->get_or_new('main_title'); ?></span>
            </div>
        </h1>
        <div class="row">
            <form role="form"
                  class="col-sm-12"
                  action="<?php echo $_form_action ?>"
                  method="post"
                  accept-charset="utf-8">
                <!-- -->
                <? $languages = $languages ?? set_value('languages[]'); ?>
                <? foreach ($languages as $language) : ?>
                    <input type="hidden" name="languages[]" value="<? echo $language ?>">
                <? endforeach; ?>
                <!-- -->
                <?
                // ############################################################################################################### 
                $piece['passo'] = $passo_1;
                $piece['has_review_remark'] = $has_review_remark;
                $piece['reviewer'] = $reviewer;
                $piece['revisor_field'] = $revisor_field;
                include_once 'piece/remark.php'; 
                // ############################################################################################################### 
                ?>
                <?php foreach ([
                 'scientific_title',
                 'public_title',
                 'scientific_acronym',
                 'scientific_acronym_expansion',
                 'acronym',
                 'acronym_expansion',
                 ] as $_field) : ?>
                    <p class="form-label">
                        <label for="id_<?php echo $_field;?>"><?php echo $fieldlabel->get_or_new($_field); ?>:</label>
                    </p>
                    <span class="fielct-required "></span>
                    <div class="help-input-text">
                        <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                              data-original-title="<?php echo $fieldhelp->get_or_new($_field); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                    </div>
                    <?php foreach ($languages as $language):?>
                        <?php
                        $field_ok = false;
                        $k_l = "{$_field}_{$language}";
                        ?>
                        <div class="fields-wrapper">
                            <? $text = $passo_1[$k_l] ?? set_value($k_l); ?>
                            <div class="fieldWrapper">
                                <div class="row-input label-line">
                                    <div class="icon_caret">
                                        <span class="glyphicon caret"></span><?php echo $genericlabel->get_or_new('language_'.$language); ?>
                                    </div>
                                    <div class="revision-icons-wrapper"></div>
                                </div>
                                <?php echo form_error("$k_l", '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                <div class="row-input">
                                    <div class="field-input ">
                                        <?php
//echo '<pre>'.var_export(array(
//        '$k_l' => $k_l,
//    ), true).'</pre>';
//                                        $text = $passo_1[$k_l];
                                        if (!empty($text))
                                            $field_ok = true;
                                        ?>
                                        <textarea cols="40"
                                                  <?php echo $common_field ?? '' ?>
                                                  id="id_<?php echo "$k_l"; ?>"
                                                  name="<?php echo "$k_l"; ?>"
                                                  rows="10"><?php echo $text; ?></textarea>
                                    </div>
                                    <div class="text-center">
                                        <?php if($field_ok): ?>
                                            <img src="<?php echo base_url('assets/images/field-ok.png'); ?>" />
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endforeach; ?>

                <div class="clear"></div>

<!-- Identificadores Secundários -->

                <div class="help-input-text" style="margin-top:14px">
                    <span class="field-help"
                          data-toggle="tooltip"
                          data-html="true"
                          data-placement="left"
                          data-original-title="<?php echo $fieldhelp->get_or_new('secondary_identifier'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                </div>
                <h2 class="subtitle-formset" for="identifier_set-0-secid_id"><?php echo $fieldlabel->get_or_new('secondaries_identifiers_title'); ?></h2>

                <table style="border-collapse: collapse; border: none; width: 100%;" id="dynamic_field_identification">
                    <? $secondaryidentifier = $VD['genData']['secondaryidentifier']; ?>
                    <? $identifier_set_count = $passo_1['identifier_set-count'] ?? set_value('count_secid'); ?>
                    <? //$index_count = $identifier_set_count > 4 ? $identifier_set_count : 4; ?>
                    <? $index_count = $identifier_set_count ?? 0; ?>
                    <?
//echo '<pre>'.var_export(array(
//        __FILE__ => __LINE__,
////        '$count' => $count,
////        '$passo_1' => $passo_1,
////        '$secondaryidentifier' => $secondaryidentifier->toArray(),
//        '$index_count' => $index_count,
//    ), true).'</pre>';
                    ?>
                    <? $count = 0; ?>
                    <? for($index = 0; $index <= $index_count; $index++) : ?>
                        <? $identifier_set_identifier =
                                $passo_1['identifier_set-'.($index).'-identifier'] ?? set_value('identifier_set-'.($index).'-identifier'); ?>
                        <? $identifier_set_issuer_id =
                                $passo_1['identifier_set-'.($index).'-issuer_id'] ?? set_value('identifier_set-'.($index).'-issuer_id'); ?>
                        <? $identifier_set_description =
                            $passo_1['identifier_set-'.($index).'-description'] ?? set_value('identifier_set-'.($index).'-description'); ?>
                        <? $identifier_set_code =
                            $passo_1['identifier_set-'.($index).'-code'] ?? set_value('identifier_set-'.($index).'-code'); ?>
<?
//echo '<pre>'.var_export(array(
//        __FILE__ => __LINE__,
//        '$identifier_set_identifier' => $identifier_set_identifier,
//        '$identifier_set_issuer_id' => $identifier_set_issuer_id,
//        '$identifier_set_description' => $identifier_set_description,
//        '$identifier_set_code' => $identifier_set_code,
//    ), true).'</pre>';
 ?>
                        <?
                        $idx = ($index === 0) ? '__prefix__' : $index;
                        if ($index === 0) :
                            $identifier = $secondaryidentifier[3];
                            $trial_identifier['id'] = $idx;
                            $trial_identifier['trial_id'] = $VD['subData']['trial_id'];
                            $trial_identifier['issuing_authority'] = '';
                            $trial_identifier['id_number'] = '';
                            $trial_identifier['id_identifier'] = $identifier['id'];
                        else :
                            $count++;
                            $i =  $identifier_set_identifier;
                            $identifier = $secondaryidentifier[$i - 1];
//echo '<pre>'.var_export(array(
//        __FILE__ => __LINE__,
//        '$index' => $index,
//        '$i' => $i,
//    ), true).'</pre>';
                            $trial_identifier['id']                     = $identifier_set_issuer_id;
                            $trial_identifier['trial_id']               = $VD['subData']['trial_id'];
                            $trial_identifier['issuing_authority']      = $identifier_set_description;
                            $trial_identifier['id_number']              = $identifier_set_code;
                            $trial_identifier['id_identifier']          = $i;
                        endif;

                        $id_identifier = $identifier['id'];
//                        $qtd_identifier = $passo_1['identifier_set-'.($id_identifier).'-count'];
//echo '<pre>'.var_export([
//        __FILE__ => __LINE__,
//        'array_keys' => array_keys($passo_1),
//        '_' => 'identifier_set-'.($index).'-identifier',
////        '$index' => $index,
//        '$identifier' => $identifier,
//        '$trial_identifier' => $trial_identifier,
//        '$qtd_identifier' => $qtd_identifier,
//    ], true).'</pre>';
                        ?>
                        <tr id="row<?php echo $idx; ?>">
                            <!-- <td style="border-style: dotted solid;"><?php echo $idx; ?><?php echo var_export($trial_identifier, TRUE); ?>
                            </td> -->
                            <td <?php if ($idx != '__prefix__') : ?>
                                style="border: 3px; border-style: solid; padding: 10px; border-color: #CBcBcB;"
                                <?php endif; ?>
                                >
                                <div class="inline identifier_set dynamic-form"
                                    <?php if ($idx == '__prefix__') echo 'style="display:none;"'; ?>
                                     id="dynamic_identification_<?php echo $trial_identifier['id']; ?>">
                                    <input id="id_identifier_set-<?php echo $idx; ?>-secid_id"
                                           name="identifier_set-<?php echo $idx; ?>-secid_id"
                                           type="hidden"
                                           value="<?php echo @$index; ?>"/>
                                    <!-- -->
                                    <div class="fieldWrapper">
                                        <div class="row-input label-line">
                                            <div class="no-margin-left">
                                                <label for="id_identifier"><?php echo $fieldlabel->get_or_new('identifier'); ?>:</label>
                                            </div>
                                            <div class="help-input-text">
                                                <?php if ($trial_identifier['id_identifier'] < 4) : ?>
                                            <span class="field-help"
                                                  data-html="true"
                                                  data-toggle="tooltip"
                                                  data-placement="left"
                                                  title=""
                                                  data-original-title="<?php echo $fieldhelp->get_or_new('sec_id_' . $trial_identifier['id_identifier'] . '_identifier'); ?>">
                                                <?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>">
                                            </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <input id="id_identifier_set-<?php echo $idx; ?>-issuer_id"
                                               name="identifier_set-<?php echo $idx; ?>-issuer_id"
                                            <?php if ($idx != '__prefix__') : ?>
                                                value="<?php echo $trial_identifier['id']; ?>"
                                            <?php else :?>
                                                value=""
                                            <?php endif; ?>
                                               type="hidden">
                                        <div class="row-input input" id="setup_identifier">
                                            <div class="field-input">
                                                <select id="id_identifier_set-<?php echo $idx; ?>-identifier"
                                                    <?php if (
                                                        empty($common_field) and 
                                                        ($trial_identifier['id'] < 0) and 
                                                        !empty(@$trial_identifier['issuing_authority'])) : ?>
                                                        readonly disabled
                                                    <?php endif; ?>
                                                    <?php echo @$common_field ?? '' ?>
                                                        name="identifier_set-<?php echo $idx; ?>-identifier">
                                                    <option value="" selected="selected"></option>
                                                    <?php foreach ($VD['genData']['secondaryidentifier'] as $p): ?>
                                                        <option value="<?php echo $p->id; ?>" <?php echo ($p->id == $trial_identifier['id_identifier'] ? 'selected="selected"' : ''); ?>><?php echo $p->issuingidentifier; ?></option>
                                                    <?php endforeach; ?>
                                                </select>

                                            </div>
                                            <div class="text-center">
                                                <?php if(!empty(@$trial_identifier['id_number']) && !empty(@$trial_identifier['issuing_authority'])): ?>
                                                    <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fieldWrapper">
                                        <div class="row-input label-line">
                                            <div class="no-margin-left">
                                                <label for="id_identifier"><?php echo $fieldlabel->get_or_new('code'); ?>:</label>
                                            </div>
                                            <div class="help-input-text">
                                                <?php if ($trial_identifier['id_identifier'] < 4) : ?>
                                                <span class="field-help"
                                                          data-html="true"
                                                          data-toggle="tooltip"
                                                          data-placement="left"
                                                          title=""
                                                          data-original-title="<?php echo $fieldhelp->get_or_new('sec_id_' . $trial_identifier['id_identifier'] . '_code'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                                                    <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <?php echo form_error('identifier_set-' . $idx . '-code', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '"/></div>'); ?>
                                        <div class="row-input input error-identifier_set-0-secid_id" id="code">
                                            <div class="field-input">
                                                <input id="id_identifier_set-<?php echo $idx; ?>-code"
                                                       name="identifier_set-<?php echo $idx; ?>-code"
                                                       maxlength="100"
                                                       type="text"
                                                    <?php echo $common_field ?? '' ?>
                                                       value="<?php echo @$trial_identifier['id_number']; ?>">
                                            </div>
                                            <div class="text-center">
                                                <?php if(!empty(@$trial_identifier['id_number'])): ?>
                                                    <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fieldWrapper">
                                        <div class="row-input label-line">
                                            <div class="no-margin-left">
                                                <label for="id_identifier_set-<?php echo $idx; ?>-description"><?php echo $fieldlabel->get_or_new('description'); ?>:</label>
                                            </div>
                                            <div class="help-input-text">
                                                <?php if ($trial_identifier['id_identifier'] < 4) : ?>
                                        <span class="field-help"
                                              data-html="true"
                                              data-toggle="tooltip"
                                              data-placement="left"
                                              title=""
                                              data-original-title="<?php echo $fieldhelp->get_or_new('sec_id_' . $trial_identifier['id_identifier'] . '_description'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                                            <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
                                        </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <?php echo form_error('identifier_set-' . $idx . '-description', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '"/></div>'); ?>
                                        <div class="row-input input error-identifier_set-0-secid_id" id="description">
                                            <div class="field-input">
                                                <input id="id_identifier_set-<?php echo $idx; ?>-description"
                                                       name="identifier_set-<?php echo $idx; ?>-description"
                                                       maxlength="2000"
                                                       type="text"
                                                    <?php echo $common_field ?? '' ?>
                                                       value="<?php echo @$trial_identifier['issuing_authority']; ?>">
                                            </div>
                                            <div class="text-center">
                                                <?php if(!empty(@$trial_identifier['issuing_authority'])): ?>
                                                    <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fieldWrapper">
                                        <div class="row-input label-line">
                                            <div class="no-margin-left"></div>
                                            <div class="help-input-text"></div>
                                        </div>
                                        <? if (empty($common_field)) : ?>
                                        <div class="row-input input" id="DELETE">
                                            <div class="field-input">
                                                <input id="id_identifier_set-<?php echo $idx; ?>-delete"
                                                        name="identifier_set-<?php echo $idx; ?>-delete"
                                                    <?php echo $common_field ?? '' ?>
                                                        type="checkbox"/>
                                                <label for="id_delete-<?php echo $idx; ?>"><?php echo $genericlabel->get_or_new('remove'); ?></label>
                                            </div>
                                            <div class="text-center"></div>
                                        </div>
                                        <? endif; ?>
                                        <div class="clear"></div>
                                    </div>

                                    <!-- -->
                                </div>
                            </td>
                        </tr>
                    <?php endfor; ?>
                    <!-- --------------------------------------------------------------------------------------------------------------- -->
                </table>
<!-- ############################################################################################################### -->
                <!-- --------------------------------------------------------------------------------------------------------------- -->
                <input type="hidden" id="id_deleted_secid_identification" name="deleted_secid" value=""/>
                <input type="hidden" id="id_count_secid_identification" name="count_secid" value="<?php echo $count; ?>"/>
                <?php if (empty($common_field)) : ?>
                <a class="add-row"
                   id="add_identification"
                   name="add"><?php echo $genericlabel->get_or_new('button_add_another'); ?></a>
                <?php endif; ?>
                <div class="submit-line">
                    <?php if (($can_save ?? FALSE) && !($reviewer ?? FALSE)) : ?>
                        <input type="submit"
                               value="<?php echo $genericlabel->get_or_new('button_save'); ?>"
                               class="btn btn-primary pull-right">
                    <?php endif; ?>
                </div>
                <div class="modal fade" id="modal-confirm-study-type-change">
                    <div class="modal-dialog modal-sm" style="max-width: 65%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <h4 class="modal-title"><?php echo lang('int_modal_title_alt_tipo_estudo'); ?></h4>
                            </div>
                            <div class="modal-body">
                                <p><?php echo lang('int_modal_conf_alt_tipo_estudo'); ?></p>
                            </div>
                            <div class="modal-footer">
                                <input type="submit"
                                       name="accept_modal"
                                       value="<?php echo $genericlabel->get_or_new('button_accept'); ?>"
                                       class="btn btn-primary btn-sm"
                                       style="margin-bottom:0;">
                                <button type="button"
                                        class="btn btn-primary btn-sm"
                                        data-dismiss="modal"
                                        style="margin-bottom:0;"><?php echo $genericlabel->get_or_new('button_close'); ?></button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $(document).on("click", "#add_identification", function(){
                    var count = $('#id_count_secid_identification').val();
                    count++;
                    var replaced = document.getElementById('dynamic_identification___prefix__').innerHTML.split('__prefix__').join(count);
                    $('#dynamic_field_identification').append('<tr id="row'+count+'"><td style="border: 3px; border-style: solid; padding: 10px; border-color: #CBcBcB;"><div class="inline identifier_set empty-form dynamic-form" id="dynamic_identification_'+count+'">' + replaced +'</div></td></tr>');
                    $('#id_count_secid_identification').val(count);
                });
                // $(document).on("change", "input[id*=-delete]", function(){
                //     var button_id = $(this).attr("id").split("-")[1];
                //     var attach_id = $('#id_identifier_set-'+button_id+'-secid_id').val();
                //     if(attach_id != 0)
                //     {
                //         if(this.checked){
                //             $('#id_deleted_secid').val($('#id_deleted_secid').val() + attach_id + ';' );
                //         }else{
                //             $('#id_deleted_secid').val($('#id_deleted_secid').val().replace(attach_id + ';', ""));
                //         }
                //     }
                //     else {
                //         if(!$('#id_identifier_set-'+button_id+'-description').val() && !$('#id_identifier_set-'+button_id+'-code').val()){
                //             $('#row'+button_id+'').remove(); //Remove imediatamente o identificador da pagina
                //         }
                //     }
                // });
            });
        </script>
    </div>
</div>
<?php $genericlabel->disableUpdate(); ?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>