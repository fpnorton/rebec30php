﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('AttachmentForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldHelpLanguage('AttachmentForm'); $fieldhelp->enableUpdate(); ?>
<div class="row">
    <div class="col-sm-12 content">
        <h1 class="title_line">
            <div class="row">
                <div class="number-rounded"><? echo PASSO_ANEXOS ?></div>
                <span>&nbsp;<?php echo $fieldlabel->get_or_new('main_title'); ?></span>
            </div>
        </h1>
        <div class="row">
            <form role="form"
                  class="col-sm-12"
                  action="<?php echo $_form_action ?>"
                  method="post"
                  enctype="multipart/form-data"
                  accept-charset="utf-8">
                <?
                // ############################################################################################################### 
                $piece['passo'] = $passo_2;
                $piece['has_review_remark'] = $has_review_remark;
                $piece['reviewer'] = $reviewer;
                $piece['revisor_field'] = $revisor_field;
                include_once 'piece/remark.php'; 
                // ############################################################################################################### 
                ?>
                <?php $index = 0; ?>
                <table style="border-collapse: collapse; border: none; width: 100%;" id="dynamic_field_attachments">
                    <?php
                    $attachments_descriptor_matrix = [
                        0 => [],
                    ];
                    for ($count = 1; $count <= $passo_2['count_attach']; $count++) {
                        $matrix = [
                            'id' => $passo_2['ctattachment_set-'.$count.'-id'],
                            'link' => $passo_2['ctattachment_set-'.$count.'-link'],
                            'attachment' => $passo_2['ctattachment_set-'.$count.'-attachment'],
                            'file' => $passo_2['ctattachment_set-'.$count.'-file'],
                            'description' => $passo_2['ctattachment_set-'.$count.'-description'],
                            'public' => $passo_2['ctattachment_set-'.$count.'-public'],
                        ];
                        if (empty($passo_2['ctattachment_set-'.$count.'-info']) == false) {
                            $matrix['info'] = $passo_2['ctattachment_set-'.$count.'-info'];
                        }
//                    foreach ($passo_5['selected_languages'] as $language) {
//                        $matrix['vocabulary_id_'.$language] = @$passo_5['descriptor_set-'.$count.'-vocabulary_id_'.$language];
//                        $matrix['vocabulary_item_'.$language] = @$passo_5['descriptor_set-'.$count.'-vocabulary_item_'.$language];
//                    }
                        $attachments_descriptor_matrix[] = $matrix;
                    }
                    if ($passo_2['count_attach'] == 0) $attachments_descriptor_matrix[1] = [];
//echo '<pre>'.var_export([
//        __FILE__ => __LINE__,
//        '@$matrix' => @$matrix,
//        '$interventions_descriptor_matrix' => $interventions_descriptor_matrix,
//        '$passo_5' => $passo_5,
//    ], TRUE).'</pre>';
                    $count = 0;
                    ?>
                    <?php foreach($attachments_descriptor_matrix as $key_matrix => $matrix) : ?>
                    <?php $count = ($key_matrix == 0) ? '__prefix__' : $key_matrix; ?>
                    <?php $readonly = (@$matrix['file'] != '' || @$matrix['link'] != '') ? 'readonly="readonly" tabindex="-1" aria-disabled="true" disabled="disabled"' : ''; ?>
                    <tr id="row<?php echo $count; ?>">
                        <td>
                            <div class="inline ctattachment_set dynamic-form"
                                <?php if ($key_matrix == 0) : ?>
                                    style="display:none;"
                                <?php endif; ?>
                                 id="dynamic_attachments_<?php echo $count; ?>">
                                <input id="id_ctattachment_set-<?php echo $count; ?>-id"
                                       name="ctattachment_set-<?php echo $count; ?>-id"
                                       type="hidden"
                                       value="<?php echo @$matrix['id']; ?>">
                                <div class="fieldWrapper">
                                    <div class="row-input label-line">
                                        <div class="no-margin-left">
                                            <label for="id_attachment"><?php echo $fieldlabel->get_or_new('attachment'); ?>:</label>
                                        </div>
                                        <div class="help-input-text">
                                            <span class="field-help"
                                                  data-html="true"
                                                  data-toggle="tooltip"
                                                  data-placement="left"
                                                  data-original-title="<?php echo $fieldhelp->get_or_new('attach_type'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                                                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row-input input" id="attachment">
                                        <div class="field-input">
                                            <select id="id_ctattachment_set-<?php echo $count; ?>-attachment"
                                                <?php echo @$common_field ?? '' ?>
                                                <?php echo @$readonly ?>
                                                    name="ctattachment_set-<?php echo $count; ?>-attachment">
                                                <option value="" selected></option>
                                                <option value="1"<?php if(@$matrix['attachment']==1) echo 'selected="selected"'; ?>><?php echo $fieldlabel->get_or_new('type_file'); ?></option>
                                                <option value="2"<?php if(@$matrix['attachment']==2) echo 'selected="selected"'; ?>><?php echo $fieldlabel->get_or_new('type_link'); ?></option>
                                            </select>
                                        </div>
                                        <div class="text-center"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="fieldWrapper" style="display: none;" id="divFile_<?php echo $count; ?>">
                                    <div class="row-input label-line">
                                        <div class="no-margin-left">
                                            <label for="id_ctattachment_set-<?php echo $count; ?>-file"><?php echo $fieldlabel->get_or_new('file'); ?>:</label>
                                        </div>
                                        <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip"
                                      data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('file'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                                    <img src="<?php echo base_url('assets/images/interrog.png'); ?>"/>
                                </span>
                                        </div>
                                    </div>
                                    <?php echo form_error('ctattachment_set-' . $count . '-file', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '"/></div>'); ?>
                                    <div class="row-input input" id="file">
                                        <div class="field-input">
                                            <?php if (@$matrix['info']) : ?>
                                                <?php   echo @$matrix['info']['name']; ?>
                                                &nbsp;-&nbsp;
                                                <?php   echo @$matrix['info']['size']; ?>&nbsp;bytes&nbsp;
                                                <a href="<?php echo $_form_action.'/download/'.$matrix['id'] ?>" target="_blank"><?php echo $fieldlabel->get_or_new('download'); ?></a>
                                            <?php elseif (@$matrix['id']) : ?>
                                                <?php   echo basename(@$matrix['file']); ?>
                                            <?php else : ?>
                                                <input id="id_ctattachment_set-<?php echo $count; ?>-file"
                                                       name="ctattachment_set-<?php echo $count; ?>-file"
                                                    <?php echo @$common_field ?? '' ?>
                                                       type="file">
                                            <?php endif; ?>
                                        </div>
                                        <div class="text-center"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="fieldWrapper" style="display: none;" id="divLink_<?php echo $count; ?>">
                                    <div class="row-input label-line">
                                        <div class="no-margin-left">
                                            <label for="id_ctattachment_set-<?php echo $count; ?>-link"><?php echo $fieldlabel->get_or_new('link'); ?>:</label>
                                        </div>
                                        <div class="help-input-text">
                                            <span class="field-help"
                                                  data-html="true"
                                                  data-toggle="tooltip"
                                                  data-placement="left"
                                                  data-original-title="<?php echo $fieldhelp->get_or_new('attach_url'); ?>">
                                                <?php echo $genericlabel->get_or_new('help'); ?>
                                                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
                                            </span>
                                        </div>
                                    </div>
                                    <?php echo form_error('ctattachment_set-' . $count . '-link', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '"/></div>'); ?>
                                    <div class="row-input input" id="link">
                                        <div class="field-input">
                                            <?php if (@$matrix['link']) : ?>
                                                <a target="_blank" href="<?php echo @$matrix['link']; ?>"><?php 
                                                    echo @$matrix['link']; 
                                                ?></a>
                                            <?php else : ?>
                                                <input id="id_ctattachment_set-<?php echo $count; ?>-link"
                                                       maxlength="5000"
                                                    <?php echo @$readonly ?>
                                                       name="ctattachment_set-<?php echo $count; ?>-link"
                                                    <?php echo @$common_field ?? '' ?>
                                                       type="url">
                                            <?php endif; ?>
                                        </div>
                                        <div class="text-center"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="fieldWrapper">
                                    <div class="row-input label-line">
                                        <div class="no-margin-left">
                                            <label for="id_ctattachment_set-0-description"><?php echo $fieldlabel->get_or_new('description'); ?>:</label>
                                        </div>
                                        <div class="help-input-text">
                                            <span class="field-help"
                                                  data-html="true"
                                                  data-toggle="tooltip"
                                                  data-placement="left"
                                                  data-original-title="<?php echo $fieldhelp->get_or_new('description'); ?>">
                                                <?php echo $genericlabel->get_or_new('help'); ?>
                                                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
                                            </span>
                                        </div>
                                    </div>
                                    <?php echo form_error('ctattachment_set-' . $count . '-description', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '"/></div>'); ?>
                                    <div class="row-input input" id="description">
                                        <div class="field-input">
                                            <input id="id_ctattachment_set-<?php echo $count; ?>-description"
                                                   maxlength="5000"
                                                   name="ctattachment_set-<?php echo $count; ?>-description"
                                                <?php echo @$readonly ?>
                                                   type="text"
                                                <?php echo @$common_field ?? '' ?>
                                                   value="<?php echo @$matrix['description']; ?>">
                                        </div>
                                        <div class="text-center"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="fieldWrapper">
                                    <div class="row-input label-line">
                                        <div class="no-margin-left"></div>
                                        <div class="help-input-text">
                                            <span class="field-help"
                                                data-html="true"
                                                data-toggle="tooltip"
                                                data-placement="left"
                                                data-original-title="<?php echo $fieldhelp->get_or_new('public'); ?>">
                                                <?php echo $genericlabel->get_or_new('help'); ?>
                                                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
                                            </span>
                                        </div>
                                    </div>
                                    <?php echo form_error('ctattachment_set-' . $count . '-public', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '"/></div>'); ?>
                                    <div class="row-input input" id="public">
                                        <div class="field-input">
                                            <input id="id_ctattachment_set-<?php echo $count; ?>-public"
                                                <?php echo @$readonly ?>
                                                   name="ctattachment_set-<?php echo $count; ?>-public"
                                                <?php echo @$common_field ?? '' ?>
                                                   type="checkbox" <?php if (@$matrix['public'] == 'on') echo 'checked="checked"'; ?> />
                                            <label for="id_ctattachment_set-<?php echo $count; ?>-public"><?php echo $fieldlabel->get_or_new('public'); ?></label>
                                        </div>
                                        <div class="text-center"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <?php if (empty($common_field)) : ?>
                                <div class="fieldWrapper">
                                    <div class="row-input label-line">
                                        <div class="no-margin-left"></div>
                                        <div class="help-input-text"></div>
                                    </div>
                                    <div class="row-input input" id="DELETE">
                                        <div class="field-input">
                                            <input id="id_ctattachment_set-<?php echo $count; ?>-delete"
                                                   name="ctattachment_set-<?php echo $count; ?>-delete"
                                                <?php echo @$common_field ?? '' ?>
                                                   type="checkbox"/>
                                            <label for="id_ctattachment_set-<?php echo $count; ?>-delete"><?php echo $genericlabel->get_or_new('remove'); ?></label>
                                        </div>
                                        <div class="text-center"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>

                <input type="hidden" id="id_deleted_attach" name="deleted_attach" value=""/>
                <input type="hidden" id="id_count_attach" name="count_attach" value="<?php echo $count; ?>"/>
                <?php if (empty($common_field)) : ?>
                <a class="add-row"
                   id="add_attachments"
                   name="add"><?php echo $genericlabel->get_or_new('button_add_another'); ?></a>
                <?php endif; ?>
                <div class="submit-line">
                    <?php if (($can_save ?? FALSE) && !($reviewer ?? FALSE)) : ?>
                        <input type="submit"
                               value="<?php echo $genericlabel->get_or_new('button_save'); ?>"
                               class="btn btn-primary pull-right">
                    <?php endif; ?>
                </div>
            </form>
            <script type="text/javascript">
                $(document).ready(function(){
                    $(document).on("click", "#add_attachments", function(){
                        var count = $('#id_count_attach').val();
                        count++;
                        $('#dynamic_field_attachments').append('<tr id="row'+count+'"><td><div class="inline ctattachment_set empty-form dynamic-form" id="dynamic_attachments_'+count+'">'+ document.getElementById('dynamic_attachments___prefix__').innerHTML.split('__prefix__').join(count) +'</div></td></tr>');
                        $('#id_count_attach').val(count);
                    });
                    // $(document).on("change", "input[id*=-delete]", function(){
                    //         var button_id = $(this).attr("id").split("-")[1];
                    //         //alert('button_id = ' + button_id);
                    //         var attach_id = $('#id_ctattachment_set-'+button_id+'-id').val();
                    //         //alert('attach_id = ' + attach_id);
                    //         if(attach_id > 0)
                    //         {
                    //                 if($('#id_deleted_attach').val() === "")
                    //                 {
                    //                         $('#id_deleted_attach').val(attach_id);
                    //                 }
                    //                 else
                    //                 {
                    //                         $('#id_deleted_attach').val($('#id_deleted_attach').val() + ';' + attach_id);
                    //                 }
                    //         }
                    //         //alert($('#id_deleted_attach').val());
                    //         $('#row'+button_id+'').remove();
                    // });
                    $('body').delegate('select[id*=-attachment]', 'change', function(){
                        check_attachment_type(this);
                    });

                    $('select[id*=-attachment]').each(function(){
                        check_attachment_type(this);
                    });
                });

                function check_attachment_type(element) {
                    if($(element).val() == "") {
                        $(element).parents('div.ctattachment_set').find('[id*=file]').parents('.fieldWrapper').hide();
                        $(element).parents('div.ctattachment_set').find('[id*=link]').parents('.fieldWrapper').hide();
                    }
                    else {
                        if($(element).val() == '1') {
                            $(element).parents('div.ctattachment_set').find('[id*=link]').parents('.fieldWrapper').hide();
                            $(element).parents('div.ctattachment_set').find('[id*=file]').parents('.fieldWrapper').show();
                        }
                        else {
                            $(element).parents('div.ctattachment_set').find('[id*=file]').parents('.fieldWrapper').hide();
                            $(element).parents('div.ctattachment_set').find('[id*=link]').parents('.fieldWrapper').show();
                        }
                    }
                }

            </script>
        </div>
    </div>
</div>
<?php $genericlabel->disableUpdate(); ?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>