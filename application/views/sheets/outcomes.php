﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('OutcomeForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldHelpLanguage('OutcomeForm'); $fieldhelp->enableUpdate(); ?>
<div class="row">
    <div class="col-sm-12 content">
        <?php
//echo '<pre>'.var_export([
//    __FILE__ => __LINE__,
//    '$passo_8' => $passo_8,
//], TRUE).'</pre>';
        ?>
        <h1 class="title_line">
            <div class="row">
                <div class="number-rounded"><? echo PASSO_DESFECHOS ?></div>
                <span>&nbsp;<?php echo $fieldlabel->get_or_new('main_title'); ?></span>
            </div>
        </h1>
        <div class="row">
            <form role="form"
                  class="col-sm-12"
                  action="<?php echo $_form_action ?>"
                  method="post"
                  accept-charset="utf-8">
                <!-- -->
                <? $languages = $languages ?? set_value('languages[]'); ?>
                <? foreach ($languages as $language) : ?>
                    <input type="hidden" name="languages[]" value="<? echo $language ?>">
                <? endforeach; ?>
                <!-- -->
                <?
                // ############################################################################################################### 
                $piece['passo'] = $passo_8;
                $piece['has_review_remark'] = $has_review_remark;
                $piece['reviewer'] = $reviewer;
                $piece['revisor_field'] = $revisor_field;
                include_once 'piece/remark.php'; 
                // ############################################################################################################### 
                ?>
                <input type="hidden" name="id" value="<?php echo $passo_8['id']; ?>">
                <!-- results_date_completed -->
                <div class="fieldWrapper">
                    <? $results_date_completed = $passo_8['results_date_completed'] ?? set_value('results_date_completed') ?>
                    <? $results_date_completed_d = $passo_8['results_date_completed-d'] ?? set_value('results_date_completed-d') ?>
                    <? $results_date_completed_m = $passo_8['results_date_completed-m'] ?? set_value('results_date_completed-m') ?>
                    <? $results_date_completed_y = $passo_8['results_date_completed-y'] ?? set_value('results_date_completed-y') ?>
                    <?
//echo '<pre>'.var_export([
//    __FILE__ => __LINE__,
//    '$results_date_completed' => $results_date_completed,
//    'empty($results_date_completed)' => empty($results_date_completed),
////    '$passo_8' => $passo_8,
//    ], TRUE).'</pre>';
                     ?>
                    <div class="row-input label-line">
                        <div>
                            <label for="id_results_date_completed"><?php echo $fieldlabel->get_or_new('results_date_completed'); ?>:</label><span class="fielct-required"></span>
                        </div>
                        <div class="help-input-text">
                            <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                  data-original-title="<?php echo $fieldhelp->get_or_new('results_date_completed'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                        </div>
                        <div class="revision-icons-wrapper"></div>
                    </div>
                    <?php echo form_error('results_date_completed', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                    <div class="row-input input error-results_date_completed" id="results_date_completed">
                        <div class="field-input">
                            <!--
                            <input id="id_results_date_completed"
                                name="results_date_completed"
                                <?php echo @$common_field ?? '' ?>
                                type="date" value="<?php echo $results_date_completed; ?>" />
                            -->
                            <select id="id_results_date_completed-d"
                                style="width: 70px !important;"
                                <?php echo @$common_field ?? '' ?>
                                <?php echo @$readonly ?>
                                name="results_date_completed-d">
                                <option value="" selected></option>
                                <?php for ($c = 1; $c <= 31; $c++) : ?>
                                <option value="<?php echo $c; ?>" <?php if(@$results_date_completed_d == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                <?php endfor; ?>
                            </select>
                            <select id="id_results_date_completed-m"
                                style="width: 70px !important;"
                                <?php echo @$common_field ?? '' ?>
                                <?php echo @$readonly ?>
                                name="results_date_completed-m">
                                <option value="" selected></option>
                                <?php for ($c = 1; $c <= 12; $c++) : ?>
                                <option value="<?php echo $c; ?>" <?php if(@$results_date_completed_m == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                <?php endfor; ?>
                            </select>
                            <select id="id_results_date_completed-y"
                                style="width: 70px !important;"
                                <?php echo @$common_field ?? '' ?>
                                <?php echo @$readonly ?>
                                name="results_date_completed-y">
                                <option value="" selected></option>
                                <?php for ($c = 2001; $c <= 2040; $c++) : ?>
                                <option value="<?php echo $c; ?>" <?php if(@$results_date_completed_y == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                <?php endfor; ?>
                            </select>
                            dd-mm-yyyy

                        </div>
                        <div class="text-center">
                            <?php if(!empty($results_date_completed)): ?>
                                <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                <?php $field_ok = false;
                            endif; ?>
                        </div>
                    </div>
                </div>
                <div class="fieldWrapper">
                    <div class="row-input label-line">
                        <h2 class="subtitle-formset"><?php echo $fieldlabel->get_or_new('outcomes'); ?></h2>
                    </div>
                    <table style="border-collapse: collapse; border: none; width: 100%;" id="dynamic_field_outcomers" >
                        <?php
                        $count_outcomes = $passo_8['count_outcomes'] ?? set_value('count_outcomes');
                        $outcomers_descriptor_matrix = [
                            0 => [],
                        ];
                        for ($count = 1; $count <= $count_outcomes; $count++) {
                            $matrix = [
                                'id' => $passo_8['outcome_set-'.$count.'-id'] ?? set_value('outcome_set-'.$count.'-id'),
                                'type' => $passo_8['outcome_set-'.$count.'-outcome_type'] ?? set_value('outcome_set-'.$count.'-outcome_type'),
                            ];
                            foreach ($languages as $language) {
                                $matrix['description_id_'.$language] = @$passo_8['outcome_set-'.$count.'-description_id_'.$language] ?? set_value('outcome_set-'.$count.'-description_id_'.$language);
                                $matrix['description_'.$language] = @$passo_8['outcome_set-'.$count.'-description_'.$language] ?? set_value('outcome_set-'.$count.'-description_'.$language);
                            }
                            $outcomers_descriptor_matrix[] = $matrix;
                        }
                        if ($count_outcomes == 0) $outcomers_descriptor_matrix[1] = [];
                        //                    echo '<pre>'.var_export([
                        //                            __FILE__ => __LINE__,
                        //                            '$interventions_descriptor_matrix' => $interventions_descriptor_matrix,
                        //                            '$passo_5' => $passo_5,
                        //                        ], TRUE).'</pre>';
                        $index = 0;
                        ?>
                        <?php foreach($outcomers_descriptor_matrix as $key_matrix => $matrix) : ?>
                        <?php $index = ($key_matrix == 0) ? '__prefix__' : $key_matrix; ?>
                        <tr id="row<?php echo $index; ?>">
                            <td>
                                <?php
                                //    echo '<pre>'.var_export(array(
                                //        array_keys($VD),
                                //        '$VD[stepData]' => $passo_8,
                                //    ), true).'</pre>';
                                ?>
                                <div class="inline outcome_set empty-form dynamic-form"
                                    <?php if ($key_matrix == 0) : ?>
                                        style="display:none;"
                                    <?php endif; ?>
                                     id="dynamic_outcomers_<?php echo $index; ?>">
                                    <input id="id_outcome_set-<?php echo $index; ?>-id"
                                           name="outcome_set-<?php echo $index; ?>-id"
                                           type="hidden"
                                           value="<?php echo $matrix['id']; ?>"/>
                                    <div class="fieldWrapper">
                                        <div class="row-input label-line">
                                            <div class="no-margin-left">
                                                <label for="id_outcome_set-<?php echo $index; ?>-outcome_type"><?php echo $fieldlabel->get_or_new('outcome_type'); ?>:</label>
                                            </div>
                                            <div class="help-input-text">
                                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                                      data-original-title="<?php echo $fieldhelp->get_or_new('interest'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                                            </div>
                                        </div>
                                        <div class="row-input input error-id_outcome_set-1-outcome_type error-id_outcome_set-2-outcome_type">
                                            <div class="field-input">
                                                <select id="id_outcome_set-<?php echo $index; ?>-outcome_type"
                                                    <?php echo @$common_field ?? '' ?>
                                                        name="outcome_set-<?php echo $index; ?>-outcome_type">
                                                    <?php if ($key_matrix == 0) : ?>
                                                        <option value="" selected="selected"></option>
                                                        <option value="P"><?php echo $fieldlabel->get_or_new('primary'); ?></option>
                                                        <option value="S"><?php echo $fieldlabel->get_or_new('secondary'); ?></option>
                                                    <?php else : ?>
                                                        <option value=""></option>
                                                        <option
                                                            <?php if ($matrix['type'] == 'P') : ?>
                                                                selected="selected"
                                                            <?php endif; ?>
                                                                value="P"><?php echo $fieldlabel->get_or_new('primary'); ?></option>
                                                        <option
                                                            <?php if ($matrix['type'] == 'S') : ?>
                                                                selected="selected"
                                                            <?php endif; ?>
                                                                value="S"><?php echo $fieldlabel->get_or_new('secondary'); ?></option>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Descrição -->
                                    <div class="fieldWrapper">
                                        <div class="row-input label-line">
                                            <div class="no-margin-left">
                                                <label for="id_description"><?php echo $fieldlabel->get_or_new('description'); ?>:</label>
                                            </div>
                                            <div class="help-input-text">
                                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                                      data-original-title="<?php echo $fieldhelp->get_or_new('description'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fields-wrapper">
                                        <?php $field_ok = false; ?>
                                        <?php foreach ($languages as $language): ?>
                                            <div class="fieldWrapper">
                                                <div class="row-input label-line">
                                                    <div class="icon_caret">
                                                        <span class="glyphicon caret"></span><?php echo $genericlabel->get_or_new('language_'.$language); ?>
                                                    </div>
                                                    <div class="revision-icons-wrapper"></div>
                                                    <input id="id_outcome_set-<?php echo $index; ?>-description_id_<?php echo $language; ?>"
                                                           name="outcome_set-<?php echo $index; ?>-description_id_<?php echo $language; ?>"
                                                           type="hidden"
                                                           value="<?php echo $matrix['description_id_'.$language]; ?>"/>
                                                </div>
                                                <?php echo form_error('outcome_set-' . $index  . '-description_'. $language, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                                <div class="row-input">
                                                    <div class="field-input ">
                                    <textarea cols="40"
                                              id="id_outcome_set-<?php echo $index; ?>-description_<?php echo $language; ?>"
                                              name="outcome_set-<?php echo $index; ?>-description_<?php echo $language; ?>"
                                              <?php echo @$common_field ?? '' ?>
                                              rows="10"><?php echo $matrix['description_'.$language]; ?></textarea>
                                                    </div>
                                                    <div class="text-center">
                                                        <?php if($field_ok): ?>
                                                            <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                                            <?php $field_ok = false;
                                                        endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <?php if (empty($common_field)) : ?>
                                    <div class="fieldWrapper">
                                        <div class="row-input label-line">
                                            <div class="no-margin-left"></div>
                                            <div class="help-input-text"></div>
                                        </div>
                                        <div class="row-input input  ">
                                            <div class="field-input">
                                                <input id="id_outcome_set-<?php echo $index; ?>-delete"
                                                       name="outcome_set-<?php echo $index; ?>-delete"
                                                    <?php echo @$common_field ?? '' ?>
                                                       type="checkbox"/><label><?php echo $genericlabel->get_or_new('remove'); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>

                <input type="hidden" id="id_deleted_outcomes" name="deleted_outcomes" value=""/>
                <input type="hidden"
                       id="id_count_outcomes"
                       name="count_outcomes"
                       value="<?php echo $index; ?>"/>
                <?php if (empty($common_field)) : ?>
                <a class="add-row" id="add_outcomes" name="add"><?php echo $genericlabel->get_or_new('button_add_another'); ?></a>
                <?php endif; ?>
                <div class="submit-line">
                    <?php if (($can_save ?? FALSE) && !($reviewer ?? FALSE)) : ?>
                        <input type="submit"
                               value="<?php echo $genericlabel->get_or_new('button_save'); ?>"
                               class="btn btn-primary pull-right">
                    <?php endif; ?>
                </div>
            </form>
            <script type="text/javascript">
                $(document).ready(function(){
                    $(document).on("click", "#add_outcomes", function(){
                        var count = $('#id_count_outcomes').val();
                        count++;
                        $('#dynamic_field_outcomers').append('<tr id="row'+count+'"><td><div class="inline outcome_set empty-form dynamic-form" id="dynamic_outcomers_'+count+'">'+ document.getElementById('dynamic_outcomers___prefix__').innerHTML.split('__prefix__').join(count) +'</div></td></tr>');
                        $('#id_count_outcomes').val(count);
                    });
                    // $(document).on("change", "input[id*=-delete]", function(){
                    // 	var button_id = $(this).attr("id").split("-")[1];
                    // 	var attach_id = $('#id_outcome_set-'+button_id+'-id').value;
                    // 	if(attach_id != 0)
                    // 	{
                    // 		if($('#id_deleted_outcomes').val() == "")
                    // 		{
                    // 			$('#id_deleted_outcomes').val(attach_id);
                    // 		}
                    // 		else
                    // 		{
                    // 			$('#id_deleted_outcomes').val($('#id_deleted_outcomes').val() + ';' + attach_id);
                    // 		}
                    // 	}
                    // 	$('#row'+button_id+'').remove();
                    // });
                });
            </script>
        </div>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="field-revision-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="field-registrant-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<?php $genericlabel->disableUpdate(); ?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>