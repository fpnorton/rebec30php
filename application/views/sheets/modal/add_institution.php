﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('InstitutionForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldLabelLanguage('InstitutionForm'); $fieldhelp->enableUpdate(); ?>
<success /><div class='hidden invisible'>
    <select data-update='institutions'>
        <option value="" selected="selected"></option>
        <?php foreach($VD['genData']['institutions'] as $inst): ?>
            <?php if ($inst == NULL) continue; ?>
            <option value="<?php echo $inst->id.':'.$inst->hashCode(); ?>"><?php echo $inst->name; ?></option>
        <?php endforeach; ?>
    </select>
</div>
<form role="form"
      action="<?php echo base_url() ?>prototype/instituicao"
      method="post"
      accept-charset="utf-8"
      onclick="return false;"
      role="form">

    <input type="hidden" name="clinicaltrial_id" value="<? echo defined('CLINICAL_TRIAL_ID') ? CLINICAL_TRIAL_ID : 0; ?>"/>

    <div class="update">
        <div class="fieldWrapper" id="name">
            <?php echo $fieldlabel->get_or_new('name'); ?>*
            <br>
            <?php echo form_error('name', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
            <input id="id_name" maxlength="300" name="name" type="text"/>
            <br>
        </div>
        <div class="fieldWrapper" id="postal_address">
            <?php echo $fieldlabel->get_or_new('address'); ?>
            <br>
            <input id="id_postal_address" maxlength="1000" name="postal_address" type="text">
            <br>
        </div>
        <div class="fieldWrapper" id="city">
            <?php echo $fieldlabel->get_or_new('city'); ?>
            <br>
            <input id="id_city" maxlength="100" name="city" type="text">
            <br>
        </div>
        <div class="fieldWrapper" id="state">
            <?php echo $fieldlabel->get_or_new('state'); ?>
            <br>
            <input id="id_state" maxlength="50" name="state" type="text">
            <br>
        </div>
        <div class="fieldWrapper" id="country">
            <?php echo $fieldlabel->get_or_new('country'); ?>*
            <br>
            <?php echo form_error('country', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>');
            $country_id = set_value('country'); ?>
            <select id="id_country" name="country">
                <option selected="selected" id=""></option>
                <?php foreach ($VD['genData']['countrycode'] as $cc): ?>
                    <?php if (linguagem_selecionada() == 'en') : ?>
                        <?php $translation = $cc; ?>
                    <?php else : ?>
                        <?php $translation = $cc->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                    <?php endif; ?>
                    <option value="<?php echo $cc->id; ?>" <?php echo (@$country_id == $cc->id ? 'selected="selected"' : ''); ?>><?php echo $translation->description; ?></option>
                <?php endforeach; ?>
            </select>
            <br>
        </div>
        <div class="fieldWrapper" id="institution_type">
            <?php echo $fieldlabel->get_or_new('type'); ?>*
            <br>
            <?php echo form_error('institution_type', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>');
            $inst_tp = set_value('institution_type'); ?>
            <select id="id_institution_type" name="institution_type">
                <option selected="selected" id=""></option>
                <?php foreach ($VD['genData']['institutiontype'] as $it): ?>
                    <?php if (linguagem_selecionada() == 'en') : ?>
                        <?php $translation = $it; ?>
                    <?php else : ?>
                        <?php $translation = $it->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                    <?php endif; ?>
                    <option value="<?php echo $it->id; ?>" <?php echo ($inst_tp== $it->id ? 'selected="selected"' : ''); ?>><?php echo $translation->description; ?></option>
                <?php endforeach; ?>
            </select>
            <br>
        </div>
        <div class="submit-line">
            <input type="button" value="<?php echo $genericlabel->get_or_new('button_save'); ?>" action="save" class="button" data-dismiss="modal" />
            <input type="button" value="<?php echo $genericlabel->get_or_new('button_close'); ?>" action="close" class="button" data-dismiss="modal"/>
        </div>
    </div>
</form>
<?php $genericlabel->disableUpdate();?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>