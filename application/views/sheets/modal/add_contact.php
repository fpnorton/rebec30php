﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('ContactForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldLabelLanguage('ContactForm'); $fieldhelp->enableUpdate(); ?>
<?php
if (function_exists('generate_visible_contact_name') == false) {
    function generate_visible_contact_name($contact) { 
        return implode(' ', [
            $contact->firstname ?? '', 
            $contact->middlename ?? '', 
            $contact->lastname ?? ''
        ]);
    }
}
if (function_exists('generate_visible_contact_email') == false) {
    function generate_visible_contact_email($contact) { 
        $data = [];
        foreach([$contact->email, $contact->telephone] as $value) {
            if (empty($value)) continue;
            $data[] = $value;
        }
        return implode(' / ', $data);
    }
}
if (function_exists('generate_visible_contact_address') == false) {
    function generate_visible_contact_address($contact) { 
        $data = [];
        foreach([$contact->address, $contact->city, $contact->state] as $value) {
            if (empty($value)) continue;
            $data[] = $value;
        }
        return implode(' - ', $data);
    }
}
?>
<success /><div class='hidden invisible'>
    <select data-update='contacts'>
        <option value="" selected="selected"></option>
        <?php $hash_control = []; ?>
        <?php foreach($VD['genData']['contacts'] as $contact): ?>
            <?php if ($contact == NULL) continue; ?>
            <?php if (in_array($contact->hashCode(), $hash_control)) continue; ?>
            <!-- hashCode = <?php echo $contact->hashCode() ?> -->
            <?php $hash_control[] = $contact->hashCode(); ?>
            <optgroup label="<?php echo generate_visible_contact_name($contact); ?>">
                <option
                    value="<?php echo $contact->hashCode() ?>+<?php echo $contact->id ?>">
                    <?php echo implode(' - ', [
                        generate_visible_contact_name($contact), generate_visible_contact_email($contact)
                    ]); ?>
                </option>
                <option readonly disabled><?php echo generate_visible_contact_address($contact); ?>
                </option>
            </optgroup>
        <?php endforeach; ?>
    </select>
</div>
<?php
//echo '<pre>'.var_export(array(
//        array_keys($VD),
////        '$VD[genData]' => $VD['genData'],
//        '$VD[subData]' => $VD['subData'],
//        '$VD[stepData]' => $VD['stepData'],
//    ), true).'</pre>';
?>
<form role="form"
      action="<?php echo base_url() ?>prototype/contato"
      method="post"
      accept-charset="utf-8"
      onclick="return false;"
      role="form">

<input type="hidden" name="clinicaltrial_id" value="<? echo defined('CLINICAL_TRIAL_ID') ? CLINICAL_TRIAL_ID : 0; ?>"/>
<input type="hidden" name="contact_id" value="<? echo $id ?>"/>
<input type="hidden" name="contact_type" value=""/>
<input type="hidden" name="deleted_site" value=""/>

<div class="update">
    <div class="fieldWrapper">
        <label for="id_first_name"><?php echo $fieldlabel->get_or_new('first_name'); ?></label>
        <div class="help-input-text">
            <span class="field-help"
                  data-html="true"
                  data-toggle="tooltip"
                  data-placement="left"
                  data-original-title="<?php echo $fieldhelp->get_or_new('firstname'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
            </span>
        </div>
            <br>
            <?php echo form_error('first_name', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
            <input id="id_first_name"
                   maxlength="100"
                   name="first_name"
                   type="text"
                   error-when-empty="Primeiro nome é obrigatorio"
                   value="<?php echo @$firstname; ?>">
            <br>
    </div>
    
    <div class="fieldWrapper">
        <label for="id_middle_name"><?php echo $fieldlabel->get_or_new('middle_name'); ?></label>
        <div class="help-input-text">
            <span class="field-help"
                  data-html="true"
                  data-toggle="tooltip"
                  data-placement="left"
                  data-original-title="<?php echo $fieldhelp->get_or_new('middlename'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
            </span>
        </div>
            <br>
            <?php echo form_error('middle_name', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
            <input id="id_middle_name" maxlength="100" name="middle_name" type="text" value="<?php echo @$middlename; ?>">
            <br>
    </div>
    
    <div class="fieldWrapper">
        <label for="id_last_name"><?php echo $fieldlabel->get_or_new('last_name'); ?></label>
        <div class="help-input-text">
            <span class="field-help"
                  data-html="true"
                  data-toggle="tooltip"
                  data-placement="left"
                  data-original-title="<?php echo $fieldhelp->get_or_new('lastname'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
            </span>
        </div>
            <br>
            <?php echo form_error('last_name', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
            <input id="id_last_name" maxlength="100" name="last_name" type="text" value="<?php echo @$lastname; ?>">
            <br>
    </div>
    
    <div class="fieldWrapper">
        <label for="id_email"><?php echo $fieldlabel->get_or_new('email'); ?></label>
        <div class="help-input-text">
            <span class="field-help"
                  data-html="true"
                  data-toggle="tooltip"
                  data-placement="left"
                  data-original-title="<?php echo $fieldhelp->get_or_new('email'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
            </span>
        </div>
            <br>
            <?php echo form_error('email', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
            <input id="id_email"
                   maxlength="100"
                   name="email"
                   type="email"
                   error-when-empty="E-mail é obrigatorio"
                   value="<?php echo @$email; ?>">
            <br>
    </div>

    <div class="fieldWrapper">
        <label for="id_affiliation"><?php echo $fieldlabel->get_or_new('affiliation'); ?></label>
        <div class="help-input-text">
            <span class="field-help"
                  data-html="true"
                  data-toggle="tooltip"
                  data-placement="left"
                  data-original-title="<?php echo $fieldhelp->get_or_new('affiliation'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
            </span>
        </div>
            <br>
            <?php echo form_error('affiliation', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
            <select id="id_affiliation" name="affiliation">
                <option selected="selected" id=""></option>
                <?php foreach ($VD['genData']['institutions'] as $o): ?>
                    <option value="<?php echo $o->id; ?>" <?php echo (@$affiliation_id == $o->id ? 'selected="selected"' : ''); ?>><?php echo $o->name; ?></option>
                <?php endforeach; ?>
            </select>
            <br>
    </div>

    <!--
        <div class="fieldWrapper">
            <label>Buscar endereço</label><br>
            <div class="input">
                <input type="text" id="address_search" placeholder="Digite um local" autocomplete="off">
            </div>
        </div>
    -->
    <div class="fieldWrapper">
        <label for="id_address"><?php echo $fieldlabel->get_or_new('address'); ?></label>
        <div class="help-input-text">
            <span class="field-help"
                  data-html="true"
                  data-toggle="tooltip"
                  data-placement="left"
                  data-original-title="<?php echo $fieldhelp->get_or_new('address'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
            </span>
        </div>
            <br>
            <?php echo form_error('address', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
            <input id="id_address" maxlength="500" name="address" type="text" value="<?php echo @$address; ?>">
            <br>
    </div>
    
    <div class="fieldWrapper">
        <label for="id_city"><?php echo $fieldlabel->get_or_new('city'); ?></label>
        <div class="help-input-text">
            <span class="field-help"
                  data-html="true"
                  data-toggle="tooltip"
                  data-placement="left"
                  data-original-title="<?php echo $fieldhelp->get_or_new('city'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
            </span>
        </div>
            <br>
            <?php echo form_error('city', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
            <input id="id_city" maxlength="500" name="city" type="text" value="<?php echo @$city; ?>">
            <br>
    </div>
    
    <div class="fieldWrapper">
            <label for="id_state"><?php echo $fieldlabel->get_or_new('state'); ?></label>
        <div class="help-input-text">
            <span class="field-help"
                  data-html="true"
                  data-toggle="tooltip"
                  data-placement="left"
                  data-original-title="<?php echo $fieldhelp->get_or_new('state'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
            </span>
        </div>
            <br>
            <?php echo form_error('state', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
            <input id="id_state" maxlength="500" name="state" type="text" value="<?php echo @$state; ?>">
            <br>
    </div>
    
    <div class="fieldWrapper">
        <label for="id_country"><?php echo $fieldlabel->get_or_new('country'); ?></label>
        <div class="help-input-text">
            <span class="field-help"
                  data-html="true"
                  data-toggle="tooltip"
                  data-placement="left"
                  data-original-title="<?php echo $fieldhelp->get_or_new('country'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
            </span>
        </div>
            <br>
            <?php echo form_error('country', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
            <select id="id_country" name="country">
                <option selected="selected" id=""></option>
                <?php foreach ($VD['genData']['countrycode'] as $cc): ?>
                    <?php if (linguagem_selecionada() == 'en') : ?>
                        <?php $translation = $cc; ?>
                    <?php else : ?>
                        <?php $translation = $cc->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                    <?php endif; ?>
                    <option value="<?php echo $cc->id; ?>" <?php echo (@$country_id == $cc->id ? 'selected="selected"' : ''); ?>><?php echo $translation->description; ?></option>
                <?php endforeach; ?>
            </select>
            <br>
    </div>
    
    <div class="fieldWrapper">
        <label for="id_postal_code"><?php echo $fieldlabel->get_or_new('zip'); ?></label>
        <div class="help-input-text">
            <span class="field-help"
                  data-html="true"
                  data-toggle="tooltip"
                  data-placement="left"
                  data-original-title="<?php echo $fieldhelp->get_or_new('zip'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
            </span>
        </div>
            <br>
            <?php echo form_error('postal_code', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
            <input id="id_postal_code" 
                   maxlength="50" 
                   name="postal_code" 
                   type="text"  
                   value="<?php echo @$zip; ?>"
                   class="mask-zip">
            <br>
    </div>
        
    <div class="fieldWrapper">
            <label for="id_phone"><?php echo $fieldlabel->get_or_new('phone'); ?></label>
        <div class="help-input-text">
            <span class="field-help"
                  data-html="true"
                  data-toggle="tooltip"
                  data-placement="left"
                  data-original-title="<?php echo $fieldhelp->get_or_new('telephone'); ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
            </span>
        </div>
            <br>
            <?php echo form_error('phone', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
            <input id="id_phone" 
                   maxlength="40" 
                   name="phone" 
                   type="text"
                   error-when-empty="Telefone é obrigatorio"
                   value="<?php echo @$telephone; ?>"
                   class="mask-phone">
            <br>
    </div>
    <div class="submit-line">
        <p><?php echo $fieldlabel->get_or_new('confirmation_message'); ?></p>
        <input type="button" value="<?php echo $genericlabel->get_or_new('button_save'); ?>" action="save" class="button" />
        <input type="button" value="<?php echo $genericlabel->get_or_new('button_close'); ?>" action="close" class="button" data-dismiss="modal"/>
    </div>
</div>
<script>
    // function validate() {
    //     var errors = [];
    //     ['first_name','email','phone'].forEach(function(_value, _index, _array) {
    //         var element = $('[name="'+_value+'"]');
    //         if ((element.getAttribute('error-when-empty') != null) && (element.val() == '')) {
    //             errors.push(element.getAttribute('error-when-empty'));
    //         }
    //     });
    //
    //     // 'firstname' => $this->input->post('first_name'),
    //     //     'middlename' => $this->input->post('middle_name'),
    //     //     'lastname' => $this->input->post('last_name'),
    //     //     'email' => $this->input->post('email'),
    //     //     'address' => $this->input->post('address'),
    //     //     'city' => $this->input->post('city'),
    //     //     'state' => $this->input->post('state'),
    //     //     'country_id' => $this->input->post('country'),
    //     //     'zip' => $this->input->post('postal_code'),
    //     //     'telephone' => $this->input->post('phone'),
    //     console.info(errors.join("\n"));
    //     alert(errors.join("\n"));
    // }
</script>
<?php echo '</form>'; ?>
<?php $genericlabel->disableUpdate();?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>