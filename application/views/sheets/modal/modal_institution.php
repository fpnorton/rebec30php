﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal fade bs-example-modal-lg" 
     tabindex="-1" 
     role="dialog" 
     aria-hidden="true" 
     data-dismiss="modal"
     id="add-institution-modal">
    <div class="modal-dialog modal-dialog-medium modal-lg">
        <div class="modal-content">
            <?php $fieldlabel = new Helper\FieldLabelLanguage('InstitutionForm'); $fieldlabel->enableUpdate(); ?>
            <div class="modal-header">
                <h1><?php echo $fieldlabel->get_or_new('main_title_new'); ?></h1>
            </div>
            <?php $fieldlabel->disableUpdate(); ?>
            <div class="modal-body">
                <?php include_once('add_institution.php'); ?>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
