﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('ContactForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldHelpLanguage('ContactForm'); $fieldhelp->enableUpdate(); ?>
<?php
function dummy() { }
if (function_exists('generate_visible_contact_name') == false) {
    function generate_visible_contact_name($contact) { 
        return implode(' ', [
            $contact->firstname ?? '', 
            $contact->middlename ?? '', 
            $contact->lastname ?? ''
        ]);
    }
}
if (function_exists('generate_visible_contact_email') == false) {
    function generate_visible_contact_email($contact) { 
        $data = [];
        foreach([$contact->email, $contact->telephone] as $value) {
            if (empty($value)) continue;
            $data[] = $value;
        }
        return implode(' / ', $data);
    }
}
if (function_exists('generate_visible_contact_address') == false) {
    function generate_visible_contact_address($contact) { 
        $data = [];
        foreach([$contact->address, $contact->city, $contact->state] as $value) {
            if (empty($value)) continue;
            $data[] = $value;
        }
        return implode(' - ', $data);
    }
}
?>
<div class="row">
    <div class="col-sm-12 content">
        <h1 class="title_line">
            <div class="row">
                <div class="number-rounded"><? echo PASSO_CONTATOS ?></div>
                <span>&nbsp;<?php echo $fieldlabel->get_or_new('main_title'); ?></span>
            </div>
        </h1>
        <div class="row">
            <form role="form"
                  class="col-sm-12"
                  action="<?php echo $_form_action ?>"
                  method="post"
                  accept-charset="utf-8">
                <?
                // ############################################################################################################### 
                $piece['passo'] = $passo_9;
                $piece['has_review_remark'] = $has_review_remark;
                $piece['reviewer'] = $reviewer;
                $piece['revisor_field'] = $revisor_field;
                include_once 'piece/remark.php'; 
                // ############################################################################################################### 
                ?>
                <input type="hidden" name="id" value="<?php echo $passo_9['id']; ?>">
                <div class="help-input-text" style="margin-top:14px">
                    <span class="field-help"
                          data-toggle="tooltip"
                          data-html="true"
                          data-placement="left"
                          data-original-title="<?php echo $fieldhelp->get_or_new('contacts'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                </div>
                <h2 class="subtitle-formset" for="contacts-0"><?php echo $fieldlabel->get_or_new('contacts_subtitle'); ?></h2>
                    <?php
                    $contacts_descriptor_matrix = [
                        0 => [],
                    ];
                    // echo '<pre>'.var_export([
                    //         __FILE__ => __LINE__,
                    //         '$passo_9 keys' => array_keys($passo_9),
                    //         '$passo_9' => $passo_9,
                    //     ], TRUE).'</pre>';

                    for ($count = 1; $count <= $passo_9['count_contact']; $count++) {
                        $matrix = [
                            'id' => $passo_9['ctcontact_set-'.$count.'-id'],
                            'hash' => $passo_9['ctcontact_set-'.$count.'-hash'],
                            'contact_type' => $passo_9['ctcontact_set-'.$count.'-contact_type'],
                            'contact_id' => $passo_9['ctcontact_set-'.$count.'-contact_id'],
                            'date_approval' => $passo_9['ctcontact_set-'.$count.'-date_approval'],
                            'status_id' => $passo_9['ctcontact_set-'.$count.'-status_id'],
                            'date_approval-d' => $passo_9['ctcontact_set-'.$count.'-date_approval-d'],
                            'date_approval-m' => $passo_9['ctcontact_set-'.$count.'-date_approval-m'],
                            'date_approval-y' => $passo_9['ctcontact_set-'.$count.'-date_approval-y'],
                            'repository_contact' => $passo_9['ctcontact_set-'.$count.'-object']->contact,
                        ];
//                        echo '<pre>'.var_export([
//                                __FILE__ => __LINE__,
//                                '$matrix' => $matrix,
//                            ], TRUE).'</pre>';
                        $contacts_descriptor_matrix[] = $matrix;
                    }
                    // echo '<pre>'.var_export([
                    //         __FILE__ => __LINE__,
                    //         '$contacts_descriptor_matrix' => $contacts_descriptor_matrix,
                    //     ], TRUE).'</pre>';
                    if ($passo_9['count_contact'] == 0) $contacts_descriptor_matrix[1] = [];
//                                        echo '<pre>'.var_export([
//                                                __FILE__ => __LINE__,
//                                                '$interventions_descriptor_matrix' => $interventions_descriptor_matrix,
//                                                '$passo_5' => $passo_5,
//                                            ], TRUE).'</pre>';
                    $index = 0;
                    ?>
                    <!-- -->
                    <?php 
                        $contact_types = [
                            'P' => $fieldlabel->get_or_new('type_public'),
                            'S' => $fieldlabel->get_or_new('type_scientific'),
                            'W' => $fieldlabel->get_or_new('type_site'),
                            'E' => $fieldlabel->get_or_new('type_ethic'),
                        ];
                    ?>
                    <?php if ($reviewer ?? FALSE) : ?>
                        <div>
                    <?php else : ?>
                        <table style="border-collapse: collapse; border: none; width: 100%;" id="dynamic_field_contacts">
                    <?php endif; ?>
                    <?php foreach($contacts_descriptor_matrix as $key_matrix => $matrix) : ?>
                    <?php if ($reviewer ?? FALSE) : ?>
                        <?php
                        // echo '<pre>'.var_export([
                        //     __FILE__ => __LINE__,
                        //     '$key_matrix' => $key_matrix,
                        //     '$matrix' => $matrix,
                        // ], TRUE).'</pre>';
                        ?>
                        <?php if ($key_matrix == 0) continue; ?>
                        <?php if ($key_matrix > $passo_9['count_contact']) continue; ?>
                        <?php $fieldlabel_contactFossil = new Helper\FieldLabelLanguage('ContactFossil'); ?>
                        <div style="background-color: #AABBCC; border-radius: 5px; padding: 15px; margin-top: 10px;">
                        <?php echo $contact_types[$matrix['contact_type']] ?>
                        <ul class="vcard">
                            <li>
                                <b><?php echo $fieldlabel_contactFossil->get_or_new('full_name'); ?>:</b>
                                <span class="fn"><?php echo implode(' ', [
                                    $matrix['repository_contact']->firstname,
                                    $matrix['repository_contact']->middlename,
                                    $matrix['repository_contact']->lastname,
                                ]) ?></span>
                            </li>
                            <li>
                                <ul class="adr">
                                    <li>
                                        <b><?php echo $fieldlabel_contactFossil->get_or_new('address'); ?>:</b>
                                        <span class="street-address"><?php echo $matrix['repository_contact']->address ?></span>
                                    </li>
                                    <li>
                                        <b><?php echo $fieldlabel_contactFossil->get_or_new('city'); ?>: </b>
                                        <span><span class="locality"><?php echo $matrix['repository_contact']->city ?></span> / <span class="country-name">
                                        <?php if ($matrix['repository_contact']->country) : ?>
                                            <?php $translation = $matrix['repository_contact']->country; ?>
                                            <?php if (linguagem_selecionada() != 'en') : ?>
                                                <?php $translation = $matrix['repository_contact']->country->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                            <?php endif; ?>
                                            <?php echo $translation->description ?>
                                        <?php endif; ?>
                                        </span></span>
                                    </li>
                                    <li>
                                        <b><?php echo $fieldlabel_contactFossil->get_or_new('zip'); ?>:</b>
                                        <span class="postal-code"><?php echo $matrix['repository_contact']->zip ?></span>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <b><?php echo $fieldlabel_contactFossil->get_or_new('phone'); ?>:</b>
                                <span class="tel"><?php echo $matrix['repository_contact']->telephone ?></span>
                            </li>
                            <li>
                                <b><?php echo $fieldlabel_contactFossil->get_or_new('email'); ?>:</b>
                                <span class="email"><?php echo $matrix['repository_contact']->email ?></span>
                            </li>
                            <li>
                                <b><?php echo $fieldlabel_contactFossil->get_or_new('affiliation'); ?>:</b>
                                <span class="org"><?php echo $matrix['repository_contact']->affiliation == NULL ? '' : $matrix['repository_contact']->affiliation->name ?></span>
                            </li>
                        </ul>
                        </div>
                        <?php continue; ?>
                    <?php endif; ?>
                    <?php $index = ($key_matrix == 0) ? '__prefix__' : $key_matrix; ?>
                        <tr id="row<?php echo $index; ?>">
                            <td>
                                <?
//echo '<pre>'.var_export([
//__FILE__ => __LINE__,
//'$matrix' => $matrix,
//], TRUE).'</pre>';
                                ?>
                                <div class="inline ctcontact_set empty-form dynamic-form"
                                    <?php if ($key_matrix == 0) : ?>
                                        style="display:none;"
                                    <?php endif; ?>
                                     id="dynamic_contacts_<?php echo $index; ?>">
                                    <input id="id_ctcontact_set-<?php echo $index; ?>-id"
                                           name="ctcontact_set-<?php echo $index; ?>-id"
                                           value="<?php echo $matrix['id']; ?>"
                                           type="hidden">
                                    <div class="fieldWrapper">
                                        <label for="id_ctcontact_set-<?php echo $index; ?>-contact_type"><?php echo $fieldlabel->get_or_new('type'); ?></label>
                                        <span class="fielct-required"></span>
                                        <div class="help-input-text"></div>
                                        <div class="row-input input">
                                            <div class="field-input">
                                                <select id="id_ctcontact_set-<?php echo $index; ?>-contact_type"
                                                        onchange="change_type_<?php echo $index; ?>(this)"
                                                    <?php echo @$common_field ?? '' ?>
                                                        name="ctcontact_set-<?php echo $index; ?>-contact_type">
                                                    <?php if ($key_matrix == 0) : ?>
                                                        <option value="" selected="selected"></option>
                                                        <option value="P"><?php echo $fieldlabel->get_or_new('type_public'); ?></option>
                                                        <option value="S"><?php echo $fieldlabel->get_or_new('type_scientific'); ?></option>
                                                        <option value="W"><?php echo $fieldlabel->get_or_new('type_site'); ?></option>
                                                        <option value="E"><?php echo $fieldlabel->get_or_new('type_ethic'); ?></option>
                                                    <?php else : ?>
                                                        <option value=""></option>
                                                        <option <?php echo ('P' == $matrix['contact_type']) ? 'selected="selected"' : ''; ?>
                                                                value="P"><?php echo $fieldlabel->get_or_new('type_public'); ?></option>
                                                        <option <?php echo ('S' == $matrix['contact_type']) ? 'selected="selected"' : ''; ?>
                                                                value="S"><?php echo $fieldlabel->get_or_new('type_scientific'); ?></option>
                                                        <option <?php echo ('W' == $matrix['contact_type']) ? 'selected="selected"' : ''; ?>
                                                                value="W"><?php echo $fieldlabel->get_or_new('type_site'); ?></option>
                                                        <option <?php echo ('E' == $matrix['contact_type']) ? 'selected="selected"' : ''; ?>
                                                                value="E"><?php echo $fieldlabel->get_or_new('type_ethic'); ?></option>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fieldWrapper">
                                        <label for="id_ctcontact_set-<?php echo $index; ?>-contact"><?php echo $fieldlabel->get_or_new('contact'); ?>:</label>
                                        <span class="fielct-required"></span>
                                        <div class="help-input-text"></div>
                                        <div class="row-input input  ">
                                            <div class="field-input">
                                                <input  id="id_ctcontact_set-<?php echo $index; ?>-contact_id"
                                                        name="ctcontact_set-<?php echo $index; ?>-contact_id"
                                                        value="<?php echo $matrix['contact_id']; ?>"
                                                        type="hidden">
                                                <select id="id_ctcontact_set-<?php echo $index; ?>-hash"
                                                        name="ctcontact_set-<?php echo $index; ?>-hash"
                                                    <?php echo @$common_field ?? '' ?>
                                                        update='contacts'>
                                                    <option
                                                            value=""></option>
                                                    <?php $hash_control = []; ?>
                                                    <?php foreach($VD['genData']['contacts'] as $contact): ?>
                                                        <?php if ($contact == NULL) continue; ?>
                                                        <?php if (in_array($contact->hashCode(), $hash_control)) continue; ?>
                                                        <!-- hashCode = <?php echo $contact->hashCode() ?> -->
                                                        <!-- matrix[hash] = <?php echo $matrix['hash'] ?> -->
                                                        <?php $hash_control[] = $contact->hashCode(); ?>
                                                        <optgroup label="<?php echo generate_visible_contact_name($contact); ?>">
                                                            <option
                                                                <?php
                                                                // HASH PARA MANTER AS DUPLICATAS PARA O ENSAIO
                                                                ?>
                                                                <?php if ($contact->hashCode() === $matrix['hash']) : ?>
                                                                    selected="selected"
                                                                <?php endif; ?>
                                                                    value="<?php echo $contact->hashCode() ?>+<?php echo $contact->id ?>">
                                                                <?php echo implode(' - ', [
                                                                    generate_visible_contact_name($contact), generate_visible_contact_email($contact)
                                                                ]); ?>
                                                            </option>
                                                            <option readonly disabled><?php echo generate_visible_contact_address($contact); ?>
                                                            </option>
                                                        </optgroup>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="field-input">
                                                <?php if (empty($common_field)) : ?>
                                                <?php botao_editar_contato($clinicaltrial_id, 'id_ctcontact_set-'.$index.'-hash'); ?>
                                                <?php botao_cadastro_contato($clinicaltrial_id); ?>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        function change_type_<?php echo $index; ?>(element) {
                                            var total = 2;
                                            for (i = 0; i <= total; i++) {
                                                var div = document.getElementById('id_ctcontact_set-<?php echo $index; ?>-' + i);
                                                if (div) div.style.display = (element.value == 'E') ? '' : 'none';
                                            }
                                        }
                                    </script>
<!-- -->
                                    <? if (($passo_9['count_contact'] == 0) || ($key_matrix == 0) || ('E' == $matrix['contact_type'])) : ?>
                                    <div
                                        <?php if (($passo_9['count_contact'] == 0) || ($key_matrix == 0)) : ?>
                                            style="display:none;"
                                        <?php endif; ?>
                                        id="id_ctcontact_set-<?php echo $index; ?>-1"
                                        class="fieldWrapper">
                                        <? $date_approval = $matrix['date_approval'] ?? set_value('ctcontact_set-'.$index.'-date_approval') ?>
                                        <div class="row-input label-line">
                                            <div>
                                                <label for="id_ctcontact_set-<?php echo $index; ?>-date_approval"><?php echo $fieldlabel->get_or_new('date_approval'); ?>:</label><span class="fielct-required"></span>
                                            </div>
                                            <div class="help-input-text">
                                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                                      data-original-title="<?php echo $fieldhelp->get_or_new('date_approval'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                                            </div>
                                            <div class="revision-icons-wrapper"></div>
                                        </div>
                                        <?php echo form_error('date_approval', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                                        <div class="row-input input error-date_approval" id="date_approval_<?php echo $index; ?>">
                                            <div class="field-input">
                                                <!-- 
                                                <input id="id_ctcontact_set-<?php echo $index; ?>-date_approval"
                                                       name="ctcontact_set-<?php echo $index; ?>-date_approval"
                                                    <?php echo @$common_field ?? '' ?>
                                                       type="date" value="<?php echo $matrix['date_approval']; ?>" />
                                                -->
                                                <select id="id_ctcontact_set-<?php echo $index; ?>-date_approval-d"
                                                    style="width: 70px !important;"
                                                    <?php echo @$common_field ?? '' ?>
                                                    <?php echo @$readonly ?>
                                                    name="ctcontact_set-<?php echo $index; ?>-date_approval-d">
                                                    <option value="" selected></option>
                                                    <?php for ($c = 1; $c <= 31; $c++) : ?>
                                                    <option value="<?php echo $c; ?>" <?php if(@$matrix['date_approval-d'] == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                                <select id="id_ctcontact_set-<?php echo $index; ?>-date_approval-m"
                                                    style="width: 70px !important;"
                                                    <?php echo @$common_field ?? '' ?>
                                                    <?php echo @$readonly ?>
                                                    name="ctcontact_set-<?php echo $index; ?>-date_approval-m">
                                                    <option value="" selected></option>
                                                    <?php for ($c = 1; $c <= 12; $c++) : ?>
                                                    <option value="<?php echo $c; ?>" <?php if(@$matrix['date_approval-m'] == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                                <select id="id_ctcontact_set-<?php echo $index; ?>-date_approval-y"
                                                    style="width: 70px !important;"
                                                    <?php echo @$common_field ?? '' ?>
                                                    <?php echo @$readonly ?>
                                                    name="ctcontact_set-<?php echo $index; ?>-date_approval-y">
                                                    <option value="" selected></option>
                                                    <?php for ($c = 2001; $c <= 2040; $c++) : ?>
                                                    <option value="<?php echo $c; ?>" <?php if(@$matrix['date_approval-y'] == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                                dd-mm-yyyy
                                                <!-- -->
                                            </div>
                                            <div class="text-center">
                                                <?php if(!empty($matrix['date_approval'])): ?>
                                                    <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                                    <?php $field_ok = false;
                                                endif; ?>
                                            </div>
                                        </div>
                                    </div>
<!-- -->
                                    <div
                                        id="id_ctcontact_set-<?php echo $index; ?>-2"
                                        <?php if (($passo_9['count_contact'] == 0) || ($key_matrix == 0)) : ?>
                                            style="display:none;"
                                        <?php endif; ?>
                                        class="fieldWrapper" id="ctcontact_set-<?php echo $index; ?>-status_id">
                                        <? $fieldname = 'status_id'; ?>
                                        <? $status_id = $matrix[$fieldname] ?? set_value($fieldname); ?>
                                        <div class="row-input">
                                            <label for="id_ctcontact_set-<?php echo $index; ?>-status_id"><?php echo $fieldlabel->get_or_new('ethics_review_contact_status'); ?>:</label><span class="fielct-required "></span>
                                            <div class="help-input-text">
                                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                                      data-original-title="<?php echo $fieldhelp->get_or_new('ethics_review_contact_status'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                                            </div>
                                        </div>
                                        <?php echo form_error('study_type_id', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                                        <div class="input row-input error-id_study_type">
                                            <div class="field-input">
                                                <select id="id_ctcontact_set-<?php echo $index; ?>-status_id"
                                                    <?php echo @$common_field ?? '' ?>
                                                        name="ctcontact_set-<?php echo $index; ?>-status_id">
                                                    <option value=""></option>
                                                    <?php foreach($VD['genData']['ethics_review_contact_statuses'] as $status): ?>
                                                        <?php if (linguagem_selecionada() == 'en') : ?>
                                                            <?php $translation = $status; ?>
                                                        <?php else : ?>
                                                            <?php $translation = $status->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                                        <?php endif; ?>
                                                        <option value="<?php echo $status->id; ?>" <?php echo ($status->id == $status_id ? 'selected="selected"' : ''); ?>><?php echo $translation->label; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="text-right"></div>
                                        </div>
                                    </div>
                                    <? endif; ?>
<!-- -->
                                    <?php if (empty($common_field)) : ?>
                                    <div class="fieldWrapper">
                                        <div class="help-input-text"></div>
                                        <div class="row-input input  ">
                                            <div class="field-input">
                                                <input id="id_ctcontact_set-<?php echo $index; ?>-delete"
                                                       name="ctcontact_set-<?php echo $index; ?>-delete"
                                                    <?php echo @$common_field ?? '' ?>
                                                       type="checkbox"/><label><?php echo $genericlabel->get_or_new('remove'); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <!-- -->
                <?php if ($reviewer ?? FALSE) : ?>
                    </div>
                <?php else : ?>
                    </table>
                <?php endif; ?>
                <input type="hidden" id="id_public_contact" name="public_contact" value="<?php echo $counters['public_contact']; ?>" />
                <input type="hidden" id="id_scientific_contact" name="scientific_contact" value="<?php echo $counters['scientific_contact']; ?>" />
                <input type="hidden" id="id_site_contact" name="site_contact" value="<?php echo $counters['site_contact']; ?>" />
                <input type="hidden" id="id_deleted_public" name="deleted_public" value=""/>
                <input type="hidden" id="id_deleted_scientific" name="deleted_scientific" value=""/>
                <input type="hidden" id="id_deleted_site" name="deleted_site" value=""/>
                <input type="hidden"
                       id="id_count_contact"
                       name="count_contact"
                       value="<?php echo $index; ?>"/>
                <?php if (empty($common_field)) : ?>
                <a class="add-row" id="add_contacts" name="add"><?php echo $genericlabel->get_or_new('button_add_another'); ?></a>
                <?php endif ?>
                <div class="submit-line">
                    <?php if (($can_save ?? FALSE) && !($reviewer ?? FALSE)) : ?>
                        <input type="submit"
                               value="<?php echo $genericlabel->get_or_new('button_save'); ?>"
                               class="btn btn-primary pull-right">
                    <?php endif; ?>
                </div>
                <?php echo '</form>'; ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#add_contacts").click(function(){
           var count = $('#id_count_contact').val();
           count++;
           $('#dynamic_field_contacts')
                .append(
                    '<tr id="row'+count+'" idx="'+count+'">'+
                    '<td>'+
                    '<div class="inline ctcontact_set empty-form dynamic-form" id="dynamic_contacts_'+count+'">'+
                    document.getElementById('dynamic_contacts___prefix__').innerHTML.split('__prefix__').join(count) +
                    '</div>'+
                    '</td>'+
                    '</tr>');
           $('#id_count_contact')
                .val(count);
        });
        // $('input[name="botao_cadastro_contato"]')
        //     .unbind('click');
        // $('input[name="botao_cadastro_contato"]').click(
        //     function(eventObject) {
        //         var button = $(eventObject.currentTarget);
        //         if (button.hasClass('btn-disabled')) return;
        //         var divContato =
        //             button.parents("div[id^=dynamic_contacts]")[0];
        //         var selectContactType =
        //             $(divContato).find('select[id^=id_ctcontact_set-]')[0];
        //         var contactType = $(selectContactType).val();
        //         if (contactType == '') return;
        //         //
        //         console.info($('#add-contact-modal').length);
        //         $('#add-contact-modal')
        //             .find('input[name=contact_type]')
        //             .val(contactType);
        //         //
        //         $('#add-contact-modal')
        //             .modal({backdrop: 'static'});
        //     });
        $('select[id^=id_ctcontact_set-]').on('change',
            function(eventObject){
                var selectContactType = $(eventObject.currentTarget);
                // console.info(selectContactType);
                var divContato =
                    selectContactType.parents("div[id^=dynamic_contacts]")[0];
                var button =
                    $(divContato).find('input[name=botao_cadastro_contato]');
                // console.info(button);
                var contactType = $(selectContactType).val();
                // console.info(contactType);
                if (contactType == '') {
                    $(button).removeClass('btn-primary');
                    $(button).addClass('btn-disabled');
                } else {
                    $(button).removeClass('btn-disabled');
                    $(button).addClass('btn-primary');
                }
            });

// <![CDATA[
//         $(".mask-zip")
//                .mask('00000-000');
//         $(".mask-phone")
//                .focusout(function (event) {
//                    var target, phone, element;
//                    target = (event.currentTarget) ? event.currentTarget : event.srcElement;
//                    phone = target.value.replace(/\D/g, '');
//                    element = $(target);
//                    element.unmask();
//                    if(phone.length <= 10) {
//                        element.mask("(99) 9999-99999");
//                    } else {
//                        element.mask("(99) 99999-9999");
//                    }
//                })
//                .mask("(99) 9999-99999")
//         ;
//         $("button[name=botao-editar-contato]").click(function(){
//         });
    });
    // ]]>
</script>

<?php $genericlabel->disableUpdate(); ?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>

<?php include_once VIEWPATH . 'sheets/modal/modal_contact.php'; ?>

<style type="text/css">
    div.contact-field select {
        width: 60%;
    }
</style>
<!-- -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="field-revision-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="field-registrant-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">	</div>
    </div>
</div>