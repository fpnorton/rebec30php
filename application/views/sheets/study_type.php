﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('StudyTypeForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldHelpLanguage('StudyTypeForm'); $fieldhelp->enableUpdate(); ?>
<div class="row">
    <div class="col-sm-12 content">
        <h1 class="title_line">
            <div class="row">
                <div class="number-rounded"><? echo PASSO_DESENHO_ESTUDO ?></div>
                <span>&nbsp;<?php echo $fieldlabel->get_or_new('main_title'); ?></span>
            </div>
        </h1>
        <div class="row">
            <form role="form"
                  class="col-sm-12"
                  action="<?php echo $_form_action ?>"
                  method="post"
                  accept-charset="utf-8">
                <!-- -->
                <? $languages = $languages ?? set_value('languages[]'); ?>
                <? foreach ($languages as $language) : ?>
                    <input type="hidden" name="languages[]" value="<? echo $language ?>">
                <? endforeach; ?>
                <!-- -->
                <?
                // ############################################################################################################### 
                $piece['passo'] = $passo_7;
                $piece['has_review_remark'] = $has_review_remark;
                $piece['reviewer'] = $reviewer;
                $piece['revisor_field'] = $revisor_field;
                include_once 'piece/remark.php'; 
                // ############################################################################################################### 
                ?>
                <input type="hidden" name="id" value="<?php echo $passo_7['id']; ?>">
                <!-- -->
                <? $study_type_id = $passo_7['study_type_id'] ?? set_value('study_type_id'); ?>
                <? $is_intervencional = $passo_7['is_interventional'] ?? set_value('is_interventional'); ?>
                <? $is_observational = $passo_7['is_observational'] ?? set_value('is_observational'); ?>
                <input type="hidden"
                       name="study_type_id"
                       value="<?php echo $study_type_id; ?>" />
                <input type="hidden"
                       name="is_interventional"
                       value="<?php echo $is_intervencional; ?>" />
                <input type="hidden"
                       name="is_observational"
                       value="<?php echo $is_observational; ?>" />
                <!-- -->
                <?php if ($is_intervencional) : ?>
                    <!-- -->
                    <div class="fieldWrapper" style="display: inline;">
                        <? $fieldname = 'expanded_access_program'; ?>
                        <? $expanded_access_program = $passo_7[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="id_expanded_access_program"><?php echo $fieldlabel->get_or_new('expanded_access_program'); ?>:</label><span class="fielct-required "></span>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('expanded_access_program'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('expanded_access_program', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                        <div class="row-input input">
                            <div class="field-input">
                                <select id="id_expanded_access_program"
                                    <?php echo @$common_field ?? '' ?>
                                        name="expanded_access_program">
                                    <option value="U" <?php echo (empty($expanded_access_program) ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('expanded_access_program_unknown'); ?></option>
                                    <option value="Y" <?php echo ($expanded_access_program == '1' ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('expanded_access_program_yes'); ?></option>
                                    <option value="N" <?php echo ($expanded_access_program == '2' ? 'selected="selected"' : ''); ?>><?php echo $fieldlabel->get_or_new('expanded_access_program_no'); ?></option>
                                </select>
                            </div>
                            <div class="text-center">
                            </div>
                        </div>
                    </div>
                    <div class="fieldWrapper" style="display: inline;">
                        <? $fieldname = 'study_purpose'; ?>
                        <? $study_purpose = $passo_7[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                            </div>
                            <label for="id_study_purpose"><?php echo $fieldlabel->get_or_new('purpose'); ?>:</label><span class="fielct-required "></span>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('purpose'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('study_purpose', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                        <div class="row-input input">
                            <div class="field-input">
                                <select id="id_study_purpose"
                                    <?php echo @$common_field ?? '' ?>
                                        name="study_purpose">
                                    <option value="" selected="selected"></option>
                                    <?php foreach ($VD['genData']['purpose'] as $p): ?>
                                        <?php if (linguagem_selecionada() == 'en') : ?>
                                            <?php $translation = $p; ?>
                                        <?php else : ?>
                                            <?php $translation = $p->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                        <?php endif; ?>
                                        <option value="<?php echo $p->id; ?>" <?php echo ($p->id == $study_purpose ? 'selected="selected"' : ''); ?>><?php echo $translation->label; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="text-center">
                            </div>
                        </div>
                    </div>
                    <div class="fieldWrapper" style="display: inline;">
                        <? $fieldname = 'intervention_assignment'; ?>
                        <? $intervention_assignment = $passo_7[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="id_intervention_assignment"><?php echo $fieldlabel->get_or_new('intervention_assignment'); ?>:</label><span class="fielct-required "></span>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('intervention_assignment'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('intervention_assignment', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                        <div class="row-input input">
                            <div class="field-input">
                                <select id="id_intervention_assignment"
                                    <?php echo @$common_field ?? '' ?>
                                        name="intervention_assignment">
                                    <option value=""></option>
                                    <?php foreach ($VD['genData']['intervention_assignment'] as $ia): ?>
                                        <?php if (linguagem_selecionada() == 'en') : ?>
                                            <?php $translation = $ia; ?>
                                        <?php else : ?>
                                            <?php $translation = $ia->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                        <?php endif; ?>
                                        <option value="<?php echo $ia->id; ?>" <?php echo ($ia->id == $intervention_assignment ? 'selected="selected"' : ''); ?>><?php echo $translation->label; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="text-center"></div>
                        </div>
                    </div>
                    <div class="fieldWrapper" style="display: inline;">
                        <? $fieldname = 'number_arms'; ?>
                        <? $number_arms = $passo_7[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="id_number_arms"><?php echo $fieldlabel->get_or_new('number_arms'); ?>:</label>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('number_of_arms'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <div class="row-input input error-id_number_arms">
                            <div class="field-input">
                                <input id="id_number_arms"
                                       name="number_arms"
                                       value="<?php echo $number_arms; ?>"
                                    <?php echo @$common_field ?? '' ?>
                                       type="number">
                            </div>
                            <div class="text-center"></div>
                        </div>
                    </div>
                    <div class="fieldWrapper" style="display: inline;">
                        <? $fieldname = 'masking_type'; ?>
                        <? $masking_type = $passo_7[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="id_masking_type"><?php echo $fieldlabel->get_or_new('masking_type'); ?>:</label><span class="fielct-required "></span>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('masking'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('masking_type', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                        <div class="row-input input">
                            <div class="field-input">
                                <select id="id_masking_type"
                                    <?php echo @$common_field ?? '' ?>
                                        name="masking_type">
                                    <option value=""></option>
                                    <?php foreach ($VD['genData']['masking'] as $m): ?>
                                        <?php if (linguagem_selecionada() == 'en') : ?>
                                            <?php $translation = $m; ?>
                                        <?php else : ?>
                                            <?php $translation = $m->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                        <?php endif; ?>
                                        <option value="<?php echo $m->id; ?>" <?php echo ($m->id == $masking_type ? 'selected="selected"' : ''); ?>><?php echo $translation->label; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="text-center">
                            </div>
                        </div>
                    </div>
                    <div class="fieldWrapper" style="display: inline;">
                        <? $fieldname = 'allocation_type'; ?>
                        <? $allocation_type = $passo_7[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="id_allocation_type"><?php echo $fieldlabel->get_or_new('allocation_type'); ?>:</label><span class="fielct-required "></span>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('allocation'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('allocation_type', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                        <div class="row-input input">
                            <div class="field-input">
                                <select id="id_allocation_type"
                                    <?php echo @$common_field ?? '' ?>
                                        name="allocation_type">
                                    <option value=""></option>
                                    <?php foreach ($VD['genData']['allocation'] as $a): ?>
                                        <?php if (linguagem_selecionada() == 'en') : ?>
                                            <?php $translation = $a; ?>
                                        <?php else : ?>
                                            <?php $translation = $a->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                        <?php endif; ?>
                                        <option value="<?php echo $a->id; ?>" <?php echo ($a->id == $allocation_type ? 'selected="selected"' : ''); ?>><?php echo $translation->label; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="text-center"></div>
                        </div>
                    </div>
                    <div class="fieldWrapper" style="display: inline;">
                        <? $fieldname = 'study_phase'; ?>
                        <? $study_phase = $passo_7[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="id_study_phase"><?php echo $fieldlabel->get_or_new('study_phase'); ?>:</label><span class="fielct-required "></span>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('phase'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('study_phase', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                        <div class="row-input input">
                            <div class="field-input">
                                <select id="id_study_phase"
                                    <?php echo @$common_field ?? '' ?>
                                        name="study_phase">
                                    <option value="" selected="selected"></option>
                                    <?php foreach ($VD['genData']['phase'] as $p): ?>
                                        <option value="<?php echo $p['id']; ?>" <?php echo ($p['id'] == $study_phase ? 'selected="selected"' : ''); ?>><?php echo $p['label']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="text-center"></div>
                        </div>
                    </div>
                    <!-- -->
                <?php elseif ($is_observational) : ?>
                    <!-- -->
                    <div class="fieldWrapper" style="display: inline;">
                        <? $fieldname = 'observational_study_design'; ?>
                        <? $observational_study_design = $passo_7[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="id_observational_study_design"><?php echo $fieldlabel->get_or_new('observational_study_design'); ?>:</label>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('observational_study_design'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('observational_study_design', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                        <div class="row-input input">
                            <div class="field-input">
                                <select id="id_observational_study_design"
                                    <?php echo @$common_field ?? '' ?>
                                        name="observational_study_design">
                                    <option value=""></option>
                                    <?php foreach ($VD['genData']['observational_study_design'] as $osd): ?>
                                        <?php if (linguagem_selecionada() == 'en') : ?>
                                            <?php $translation = $osd; ?>
                                        <?php else : ?>
                                            <?php $translation = $osd->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                        <?php endif; ?>
                                        <option value="<?php echo $osd->id; ?>" <?php echo ($osd->id == $observational_study_design ? 'selected="selected"' : ''); ?>><?php echo $translation->label; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="text-center"></div>
                        </div>
                    </div>
                    <div class="fieldWrapper" style="display: inline;">
                        <? $fieldname = 'time_perspective'; ?>
                        <? $time_perspective = $passo_7[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="id_time_perspective"><?php echo $fieldlabel->get_or_new('time_perspective'); ?>:</label>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('time_perspective'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error('time_perspective', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                        <div class="row-input input">
                            <div class="field-input">
                                <select id="id_time_perspective"
                                    <?php echo @$common_field ?? '' ?>
                                        name="time_perspective">
                                    <option value=""></option>
                                    <?php foreach ($VD['genData']['time_perspective'] as $tp): ?>
                                        <?php if (linguagem_selecionada() == 'en') : ?>
                                            <?php $translation = $tp; ?>
                                        <?php else : ?>
                                            <?php $translation = $tp->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                        <?php endif; ?>
                                        <option value="<?php echo $tp->id; ?>" <?php echo ($tp->id == $time_perspective ? 'selected="selected"' : ''); ?>><?php echo $translation->label; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="text-center"></div>
                        </div>
                    </div>
                    <!-- -->
                <?php else : ?>
                    <div class="fieldWrapper" id="study_type">
                        <? $fieldname = 'study_type_id'; ?>
                        <? $study_type_id = $passo_7[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input">
                            <label for="id_study_type"><?php echo $fieldlabel->get_or_new('create_study_type'); ?>:</label><span class="fielct-required "></span>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('recruitment_status'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                        </div>
                        <?php echo form_error('study_type_id', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="input row-input error-id_study_type">
                            <div class="field-input">
                                <select id="id_study_type"
                                    <?php echo @$common_field ?? '' ?>
                                        name="study_type_id">
                                    <option value=""></option>
                                    <?php foreach($VD['genData']['study_type'] as $type): ?>
                                        <?php if (linguagem_selecionada() == 'en') : ?>
                                            <?php $translation = $type; ?>
                                        <?php else : ?>
                                            <?php $translation = $type->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                        <?php endif; ?>
                                        <option value="<?php echo $type->id; ?>" <?php echo ($type->id == $study_type_id ? 'selected="selected"' : ''); ?>><?php echo $translation->label; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="text-right"></div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="submit-line">
                    <?php if (($can_save ?? FALSE) && !($reviewer ?? FALSE)) : ?>
                        <input type="submit"
                               value="<?php echo $genericlabel->get_or_new('button_save'); ?>"
                               class="btn btn-primary pull-right">
                    <?php endif; ?>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="field-revision-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="field-registrant-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<?php $genericlabel->disableUpdate(); ?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>