﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('SponsorForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldHelpLanguage('SponsorForm'); $fieldhelp->enableUpdate(); ?>
<?php
//echo '<pre>'.var_export($clinical_trial, TRUE).'</pre>';
//echo '<pre>'.var_export(array(
//        'institutions' => $institutions,
//        array_keys($VD),
//        '$VD[genData]' => array_keys($VD['genData']),
//        '$VD[subData]' => array_keys($VD['subData']),
//        '$VD[stepData]' => array_keys($passo_3),
//        '$VD[stepData]' => $passo_3,
//        '$VD[stepData][count_sponsor]' => $passo_3['count_sponsor'],
//        '$VD[genData][institutions]' => array_keys($VD['genData']['institutions']),
//    ), true).'</pre>';
$sponsor_matrix = [
    0 => [],
    1 => [],
    2 => [],
    3 => [],
];
$count_sponsor = $passo_3['count_sponsor'] ?? set_value('count_sponsor');
$sequence = 0;
foreach(['P', 'S', 'U'] as $key_type) 
foreach($passo_3['id_sponsor_map'][$key_type] as $index) {
// for ($index = 1; $index <= $count_sponsor; $index++) {
    $sponsor_set_type_name = 'id_sponsor_set-'.$index.'-sponsor_type';
    $sponsor_set_type = $passo_3[$sponsor_set_type_name];

    $sponsor_set_institution_name = 'id_sponsor_set-'.$index.'-institution';
    $sponsor_set_institution = $passo_3[$sponsor_set_institution_name];

    $sponsor_set_hash_name = 'id_sponsor_set-'.$index.'-hash';
    $sponsor_set_hash = $passo_3[$sponsor_set_hash_name];

    $sponsor_set_mix_name = 'id_sponsor_set-'.$index.'-mix';
    $sponsor_set_mix = $passo_3[$sponsor_set_mix_name];

    if (strpos($sponsor_set_mix, ':')) {
        $id_and_hash = explode(':', $sponsor_set_mix);
        $sponsor_set_institution = $id_and_hash[0];
        $sponsor_set_hash = $id_and_hash[1];
    }

    $sponsor_ids = explode(';', $sponsor_set_type);
    $sponsor_id = @$sponsor_ids[0];
    $sponsor_type = @$sponsor_ids[1] ?? $key_type;

    $matrix = [
        'id' => $sponsor_id,
        'type' => $sponsor_type,
        'institution_id' => $sponsor_set_institution,
        'hash' => $sponsor_set_hash,
    ];
    // 
    // echo '<pre>'.var_export([
    //     __FILE__ => __LINE__,
    //     '$key_type' => $key_type,
    //     '$index' => $index,
    //     '$matrix' => $matrix,
    // ], TRUE).'</pre>';
    // 
    // if ($sponsor_type == 'P' && count($sponsor_matrix[1]) == 0) {
    //     $sponsor_matrix[1] = $matrix;
    // } else if ($sponsor_type == 'S' && count($sponsor_matrix[2]) == 0) {
    //     $sponsor_matrix[2] = $matrix;
    // } else if ($sponsor_type == 'U' && count($sponsor_matrix[3]) == 0) {
    //     $sponsor_matrix[3] = $matrix;
    // } else {
    //     $sponsor_matrix[] = $matrix;
    // }
    $sponsor_matrix[++$sequence] = $matrix;
}
if (count($sponsor_matrix[1]) == 0) $sponsor_matrix[1]= ['type' => 'P'];
if (count($sponsor_matrix[2]) == 0) $sponsor_matrix[2]= ['type' => 'S'];
if (count($sponsor_matrix[3]) == 0) $sponsor_matrix[3]= ['type' => 'U'];

?>
<div class="row">
    <div class="col-sm-12 content">
        <h1 class="title_line">
            <div class="row">
                <div class="number-rounded"><? echo PASSO_PATROCINADORES ?></div>
                <span>&nbsp;<?php echo $fieldlabel->get_or_new('main_title'); ?></span>
            </div>
        </h1>
        <div class="row">
            <form role="form"
                  class="col-sm-12"
                  action="<?php echo $_form_action ?>"
                  method="post"
                  accept-charset="utf-8">
                <!-- -->
                <? $languages = $languages ?? set_value('languages[]'); ?>
                <? foreach ($languages as $language) : ?>
                    <input type="hidden" name="languages[]" value="<? echo $language ?>">
                <? endforeach; ?>
                <!-- -->
                <?
                // ############################################################################################################### 
                $piece['passo'] = $passo_3;
                $piece['has_review_remark'] = $has_review_remark;
                $piece['reviewer'] = $reviewer;
                $piece['revisor_field'] = $revisor_field;
                include_once 'piece/remark.php'; 
                // ############################################################################################################### 
                ?>
                <?
//    echo '<pre>'.var_export([
//            __FILE__ => __LINE__,
//            '$passo_3' => $passo_3,
//            '$_POST' => $_POST,
//            '$count_sponsor' => $count_sponsor,
//            '$sponsor_matrix' => $sponsor_matrix,
//        ], TRUE).'</pre>';
                ?>
                <table style="border-collapse: collapse; border: none; width: 100%;" id="dynamic_field_sponsors">
                    <?php $index = 0; ?>
                    <!-- ===================================================================== -->
                    <?php foreach ($sponsor_matrix as $key_matrix => $matrix) : ?>
                        <?php
                        $sponsor =  ($index == 0) ? '__prefix__' : $index;
                        ?>
                        <tr id="row<?php echo $sponsor; ?>" for="sponsor_set-0-institution">
                            <td>
                                <div class="inline sponsor_set empty-form dynamic-form"
                                    <?php if ($key_matrix == 0) : ?>
                                        style="display:none;"
                                    <?php endif; ?>
                                     id="dynamic_sponsors_<?php echo $sponsor; ?>"
                                     style="">
                                    <div class="fieldWrapper">
                                        <label for="id_sponsor_set-<?php echo $sponsor; ?>-sponsor_type"><?php echo $fieldlabel->get_or_new('type'); ?>:</label>
                                        <?php
                                        $sponsor_id = @$matrix['id'];
                                        $sponsor_type = @$matrix['type'];
                                        ?>
                                        <div class="help-input-text">
                                            <span class="field-help"
                                                  data-html="true"
                                                  data-toggle="tooltip"
                                                  data-placement="left"
                                                  data-original-title="<?php
if ($sponsor_type == 'P') { echo $fieldhelp->get_or_new('primary_sponsor'); }
elseif ($sponsor_type == 'S') { echo $fieldhelp->get_or_new('secondary_sponsor'); }
elseif ($sponsor_type == 'U') { echo $fieldhelp->get_or_new('supporting_source'); }
else { echo '<p>'.$fieldhelp->get_or_new('secondary_sponsor').'</p><p>'.$fieldhelp->get_or_new('supporting_source').'</p>'; }
                                                    ?>"><?php echo $genericlabel->get_or_new('help'); ?>
                                                <img src="<?php echo base_url('assets/images/interrog.png'); ?>">
                                            </span>
                                        </div>
                                        <div class="input row-input">
                                            <div class="field-input"><label for="id_sponsor_set-<?php echo $sponsor; ?>-sponsor_type">
                                            <?
//    echo '<pre>'.var_export([
//            __FILE__ => __LINE__,
//            '$key_matrix' => $key_matrix,
//            '$sponsor_type' => $sponsor_type,
//        ], TRUE).'</pre>';
                ?>
                                                    <?php if ($key_matrix == 0) : ?>
                                                        <select name='id_sponsor_set-<?php echo $sponsor; ?>-sponsor_type'
                                                            <?php echo @$common_field ?? '' ?>
                                                                id='id_sponsor_set-<?php echo $sponsor; ?>-sponsor_type'>
                                                            <option value="" selected="selected"></option>
                                                            <option value="0;S"><?php echo $fieldlabel->get_or_new('secondary_sponsor'); ?></option>
                                                            <option value="0;U"><?php echo $fieldlabel->get_or_new('supporting_source'); ?></option>
                                                        </select>
                                                    <?php else : ?>
                                                        <input name='id_sponsor_set-<?php echo $sponsor; ?>-sponsor_type'
                                                               value="<?php echo ($sponsor_id.';'.$sponsor_type); ?>"
                                                               type='hidden' />
                                                        <?php if ($sponsor_type == 'P') echo $fieldlabel->get_or_new('primary_sponsor'); ?>
                                                        <?php if ($sponsor_type == 'S') echo $fieldlabel->get_or_new('secondary_sponsor'); ?>
                                                        <?php if ($sponsor_type == 'U') echo $fieldlabel->get_or_new('supporting_source'); ?>
                                                    <?php endif; ?>
                                            </div>
                                            <div class="text-center">
                                                <?php if(!empty(@$matrix['institution_id'])): ?>
                                                    <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fieldWrapper" style="display: inline;">
                                        <label for="id_sponsor_set-<?php echo $sponsor; ?>-institution"><?php echo $fieldlabel->get_or_new('institution'); ?>:</label>
                                        <?php  $institution_id = $matrix['institution_id'];  ?>
                                        <div class="help-input-text">
                                            <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                                  data-original-title="<?php
if ($sponsor_type == 'P') { echo $fieldhelp->get_or_new('primary_sponsor'); }
elseif ($sponsor_type == 'S') { echo $fieldhelp->get_or_new('secondary_sponsor'); }
elseif ($sponsor_type == 'U') { echo $fieldhelp->get_or_new('supporting_source'); }
else { echo '<p>'.$fieldhelp->get_or_new('secondary_sponsor').'</p><p>'.$fieldhelp->get_or_new('supporting_source').'</p>'; }
                                                  ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>" /></span>
                                        </div>
                                        <?php echo form_error('sponsor_set-' . $sponsor . '-institution', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                        <div class="input row-input error-sponsor_set-0-institution">
                                            <div class="field-input">
                                                <?php $combo_institutions_readonly = ($index > 1 and !empty($matrix['hash'])); ?>
                                                <select update="institutions"
                                                        name="id_sponsor_set-<?php echo $sponsor; ?>-mix"
                                                    <?php echo @$common_field ?? '' ?>
                                                        id="id_sponsor_set-<?php echo $sponsor; ?>-institution">
                                                    <option value="" selected="selected"></option>
                                                    <?php if ($VD['genData']['institutions']) foreach($VD['genData']['institutions'] as $inst): ?>
                                                        <?php if ($inst == NULL) continue; ?>
                                                        <option
                                                            <?php if ($inst->hashCode() === $matrix['hash']) : ?>
                                                                selected="selected"
                                                            <?php endif; ?>
                                                                value="<?php echo $inst->id.':'.$inst->hashCode(); ?>"><?php echo $inst->name; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="field-input">
                                            <?php if (empty($common_field)) : ?>
                                                <?php botao_cadastro_instituicao(); ?>
                                            <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if ($index == 0  || $index > 1) : ?>
                                        <div class="fieldWrapper">
                                            <div class="help-input-text"></div>
                                            <div class="input row-input">
                                                <div class="field-input">
                                                    <input id="id_sponsor_set-<?php echo $sponsor; ?>-delete"
                                                           name="sponsor_set-<?php echo $sponsor; ?>-delete"
                                                        <?php echo @$common_field ?? '' ?>
                                                           type="checkbox" />
                                                    <label for="id_sponsor_set-<?php echo $sponsor; ?>-delete"><?php echo $genericlabel->get_or_new('remove'); ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php $index++ ?>
                        <!-- ===================================================================== -->
                    <?php endforeach; ?>
                    <tr><td>
                        <?php echo form_error('secondary_sponsor', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                    </td></tr>
                    <!-- ===================================================================== -->
                </table>
                <input type="hidden" id="id_secondary_sponsor" name="secondary_sponsor" value="<?php echo $count_sec; ?>" />
                <input type="hidden" id="id_support_sources" name="support_sources" value="<?php echo $sup_source; ?>" />
                <input type="hidden" id="id_deleted_secondary" name="deleted_secondary" value=""/>
                <input type="hidden" id="id_deleted_support" name="deleted_support" value=""/>
                <input type="hidden" id="id_count_sponsor" name="count_sponsor" value="<?php echo ($count_sponsor < 3) ? 3 : $count_sponsor; ?>"/>
                <?php if (empty($common_field)) : ?>
                <a class="add-row" id="add_sponsor" name="add"><?php echo $genericlabel->get_or_new('button_add_another'); ?></a>
                <?php endif; ?>
                <div class="submit-line">
                    <?php if (($can_save ?? FALSE) && !($reviewer ?? FALSE)) : ?>
                        <input type="submit"
                               value="<?php echo $genericlabel->get_or_new('button_save'); ?>"
                               class="btn btn-primary pull-right">
                    <?php endif; ?>
                </div>
            </form>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){

                $(document).on("click", "#add_sponsor", function(){
                    var count = $('#id_count_sponsor').val();
                    count++;
                    $('#dynamic_field_sponsors').append('<tr id="row'+count+'"><td><div class="inline sponsor_set empty-form dynamic-form" id="dynamic_sponsors_'+count+'">'+ document.getElementById('dynamic_sponsors___prefix__').innerHTML.split('__prefix__').join(count) +'</div></td></tr>');
                    $('#id_count_sponsor').val(count);
                    $('[data-toggle="tooltip"]').tooltip();
                });
                // $(document).on("change", "input[id*=-delete]", function(){
                //     var button_id = $(this).attr("id").split("-")[1];
                //     var attach_id = $('#id_sponsor_set-'+button_id+'-institution').val();
                //     var type = $('#id_sponsor_set-'+ button_id +'-sponsor_type').val();
                //     if(attach_id != "")
                //     {
                //         if(type != "")
                //         {
                //             if(type == "S")
                //             {
                //                 if($('#id_deleted_secondary').val() == "")
                //                 {
                //                     $('#id_deleted_secondary').val(attach_id);
                //                 }
                //                 else
                //                 {
                //                     $('#id_deleted_secondary').val($('#id_deleted_secondary').val() + ';' + attach_id);
                //                 }
                //             }
                //             else
                //             {
                //                 if($('#id_deleted_support').val() == "")
                //                 {
                //                     $('#id_deleted_support').val(attach_id);
                //                 }
                //                 else
                //                 {
                //                     $('#id_deleted_support').val($('#id_deleted_support').val() + ';' + attach_id);
                //                 }
                //             }
                //         }
                //
                //     }
                //     else if(type == "" && this.checked)
                //     {
                //         $('#row'+button_id+'').remove();
                //     }
                // });
            });
        </script>
    </div>
</div>
<?php $genericlabel->disableUpdate(); ?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>

<!-- add-institution-modal -->
<?php include_once VIEWPATH . 'sheets/modal/modal_institution.php'; ?>

<?php $trial_id = $clinical_trial['id']; ?>

<script>
</script>
