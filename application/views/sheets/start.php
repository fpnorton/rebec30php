﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('InitialTrialForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldHelpLanguage('InitialTrialForm'); $fieldhelp->enableUpdate(); ?>
<div class="row ">
    <div class="col-sm-12 content">
        <div class="box">
            <div class="row">
                <div class="box-header">
                    <h3 class="col-xs-11 box-title"><?php echo $fieldlabel->get_or_new('main_title'); ?></h3>
                </div>
                <span class="clear"></span>
            </div>
            <hr />
            <div class="row">
<!--                --><?php //echo @($controller_method) ?><!--/0/0-->
                <form role="form"
                      class="col-sm-12"
                      action="<?php echo base_url(__FORM_ACTION__ ?? '') ?>"
                      method="post"
                      accept-charset="utf-8">
                    <input type="hidden" name="id" value="<?php echo $VD['stepData']['trial_id']; ?>">
                    <!-- -->
                    <? $languages = $languages ?? set_value('languages[]'); ?>
                    <? $recruitment_countries = $recruitment_countries ?? set_value('recruitment_countries[]'); ?>
                    <? $study_type_id = $study_type_id ?? set_value('study_type_id'); ?>
                    <? foreach ($languages as $language) : ?>
                        <input type="hidden" name="languages[]" value="<? echo $language ?>">
                    <? endforeach; ?>
                    <!-- -->
                    <div class="clear"></div>
                    <div class="fieldWrapper" id="study_type">
                        <div class="row-input">
                            <label for="id_study_type"><?php echo $fieldlabel->get_or_new('study_type'); ?>:</label><span class="fielct-required "></span>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('study_type'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                        </div>
                        <?php echo form_error('study_type_id', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="input row-input error-id_study_type">
                            <div class="field-input">
                                <select id="id_study_type"
                                        name="study_type_id">
                                    <option value="" selected="selected"></option>
                                    <?php $s_tp_id = set_value('study_type_id');
                                    foreach($VD['genData']['study_type'] as $type): ?>
                                        <?php if (linguagem_selecionada() == 'en') :?>
                                            <?php $translation = $type; ?>
                                        <?php else : ?>
                                            <?php $translation = $type->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                        <?php endif; ?>
                                        <option value="<?php echo $type->id; ?>" <?php echo ($s_tp_id == $type->id ? 'selected="selected"' : ''); ?>><?php echo $translation->label; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="text-right"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="fieldWrapper">
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="scientific_title"><?php echo $fieldlabel->get_or_new('scientific_title'); ?>:</label><span class="fielct-required"></span>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('scientific_title'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                        </div>
                        <!-- Línguas -->
                        <? $field_ok = false; ?>
                        <? foreach ($languages as $language):?>
                        <div class="input row-input error-scientific_title">
                            <div class="field-input">
                                <input type='hidden'
                                       name='language_<?php echo $language; ?>'
                                       value="<?php echo $language; ?>" />
                                <div class="row-input label-line">
                                    <div class="icon_caret">
                                        <span class="glyphicon caret"></span><?php echo $genericlabel->get_or_new('language_'.$language); ?>
                                    </div>
                                    <div class="revision-icons-wrapper"></div>
                                </div>
                                <?php echo form_error('scientific_title_' . $language, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                                <div class="row-input">
                                    <div class="field-input ">
                                        <textarea cols="40"
                                                  id="id_scientific_title_<?php echo $language; ?>"
                                                  name="scientific_title_<?php echo $language; ?>"
                                                  rows="10"><?php echo set_value('scientific_title_' . $language); ?></textarea>
                                    </div>
                                    <div class="text-center"></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="clear"></div>
                    <div class="fieldWrapper">
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="id_recruitment_countries"><?php echo $fieldlabel->get_or_new('recruitment_country'); ?>:</label>
                                <span class="fielct-required"></span>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('recruitment_country') ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                        </div>
                        <?php echo form_error('recruitment_countries[]', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="input row-input error-id_recruitment_countries">
                            <div class="field-input">
                                <select id="id_recruitment_countries"
                                        class="ui fluid search dropdown"
                                        name="recruitment_countries[]"
                                        multiple="multiple">
                                    <option id=""></option>
                                    <?php foreach ($VD['genData']['countrycode'] as $cc): ?>
                                        <?php if (linguagem_selecionada() == 'en') :?>
                                            <?php $translation = $cc; ?>
                                        <?php else : ?>
                                            <?php $translation = $cc->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                                        <?php endif; ?>
                                        <option value="<?php echo $cc->id; ?>" <?php echo (in_array($cc->id, $recruitment_countries) ? 'selected="selected"' : ''); ?>><?php echo $translation->description; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="text-right"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="fieldWrapper">
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="utrn_number"><?php echo $fieldlabel->get_or_new('utn_code'); ?>:</label>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left" title=""
                                      data-original-title="<?php echo $fieldhelp->get_or_new('utrn_number'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                        </div>
                        <?php echo form_error('utrn_number', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="row-input input error-utrn_number" id="code">
                            <div class="field-input">
                                <input id="id_utn_code" maxlength="100" name="utrn_number" value="<?php echo set_value('utrn_number'); ?>" type="text">
                            </div>
                            <div class="text-right"></div>
                        </div>
                    </div>
                    <div class="clear"></div>

                    <div class="fieldWrapper">
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="id_sponsor_set-0-sponsor_type"><?php echo $fieldlabel->get_or_new('primary_sponsor'); ?>:</label>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new('primary_sponsor'); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                        </div>
                        <?php echo form_error('primary_sponsor', '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="row-input input error-id_sponsor_set-0-sponsor_type" id="code">
                            <div class="field-input">
                                <select update="institutions"
                                        id="id_primary_sponsor"
                                        name="primary_sponsor">
                                    <option value=""
                                            selected="selected"></option>
                                    <?
                                    $primary_sponsor = set_value('primary_sponsor');
                                    $primary_sponsor_id = NULL;
                                    $primary_sponsor_hash = NULL;
                                    if (strpos($primary_sponsor, ':')) :
                                        $mix = explode(':', $primary_sponsor);
                                        $primary_sponsor_id = $mix[0];
                                        $primary_sponsor_hash = $mix[1];
                                    else :
                                        $primary_sponsor_id = $primary_sponsor;
                                    endif;
                                    foreach($VD['genData']['institutions'] as $inst):
                                        if ($inst == NULL) continue;
                                        if ($primary_sponsor_hash != NULL and $primary_sponsor_hash == $inst->hashCode()) :
                                        ?>
                                            <option selected="selected"
                                                    value="<?php echo $inst->id.':'.$inst->hashCode(); ?>"><?php echo $inst->name; ?>
                                            </option>
                                        <?
                                        elseif ($primary_sponsor_id != NULL and $primary_sponsor_id == $inst->id) :
                                        ?>
                                            <option selected="selected"
                                                    value="<?php echo $inst->id.':'.$inst->hashCode(); ?>"><?php echo $inst->name; ?>
                                            </option>
                                        <?
                                        else :
                                        ?>
                                            <option value="<?php echo $inst->id.':'.$inst->hashCode(); ?>"><?php echo $inst->name; ?>
                                            </option>
                                        <?
                                        endif;
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                            <div class="field-input">
                            <?php botao_cadastro_instituicao(); ?>
                            </div>
                            <div class="text-right"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="submit-line">
                        <input type="submit"
                               class="btn btn-secondary btn-lg"
                               value="<?php echo $genericlabel->get_or_new('button_save'); ?>">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.dropdown').dropdown();
    </script>
</div>

<!-- add-institution-modal -->
<?php include_once VIEWPATH . 'sheets/modal/modal_institution.php'; ?>
