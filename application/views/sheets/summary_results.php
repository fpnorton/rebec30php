﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('SummaryResultsForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldHelpLanguage('SummaryResultsForm'); $fieldhelp->enableUpdate(); ?>
    <div class="row">
        <div class="col-sm-12 content">
            <h1 class="title_line">
                <div class="row">
                    <div class="number-rounded"><? echo PASSO_RESUMO_RESULTADOS ?></div>
                    <span>&nbsp;<?php echo $fieldlabel->get_or_new('main_title'); ?></span>
                </div>
            </h1>
            <div class="row">
                <form role="form"
                      class="col-sm-12"
                      action="<?php echo $_form_action ?>"
                      method="post"
                      accept-charset="utf-8">
                    <!-- -->
                    <? $languages = $languages ?? set_value('languages[]'); ?>
                    <? foreach ($languages as $language) : ?>
                        <input type="hidden" name="languages[]" value="<? echo $language ?>">
                    <? endforeach; ?>
                    <!-- -->
                    <?
                    // ############################################################################################################### 
                    $piece['passo'] = $passo_10;
                    $piece['has_review_remark'] = $has_review_remark;
                    $piece['reviewer'] = $reviewer;
                    $piece['revisor_field'] = $revisor_field;
                    include_once 'piece/remark.php'; 
                    // ############################################################################################################### 
                    ?>
                    <div class="fieldWrapper">
                        <? $fieldname = 'results_date_posted'; ?>
                        <? $fieldvalue = $passo_10[$fieldname] ?? set_value($fieldname) ?>
                        <? $fieldvalue_d = $passo_10[$fieldname.'-d'] ?? set_value($fieldname.'-d') ?>
                        <? $fieldvalue_m = $passo_10[$fieldname.'-m'] ?? set_value($fieldname.'-m') ?>
                        <? $fieldvalue_y = $passo_10[$fieldname.'-y'] ?? set_value($fieldname.'-y') ?>
                        <div class="row-input label-line">
                            <div>
                                <label for="id_<? echo $fieldname ?>"><?php echo $fieldlabel->get_or_new($fieldname); ?>:</label>
                            </div>
                            <div class="help-input-text">
                            <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                  data-original-title="<?php echo $fieldhelp->get_or_new($fieldname); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error($fieldname, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="row-input input error-<? echo $fieldname ?>" id="<? echo $fieldname ?>">
                            <div class="field-input">
                                <!--
                                <input id="id_<? echo $fieldname ?>"
                                       name="<? echo $fieldname ?>"
                                    <?php echo @$common_field ?? '' ?>
                                       type="date" value="<?php echo $fieldvalue; ?>" />
                                -->
                                <select id="id_<? echo $fieldname ?>-d"
                                    style="width: 70px !important;"
                                    <?php echo @$common_field ?? '' ?>
                                    <?php echo @$readonly ?>
                                    name="<? echo $fieldname ?>-d">
                                    <option value="" selected></option>
                                    <?php for ($c = 1; $c <= 31; $c++) : ?>
                                    <option value="<?php echo $c; ?>" <?php if(@$fieldvalue_d == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                    <?php endfor; ?>
                                </select>
                                <select id="id_<? echo $fieldname ?>-m"
                                    style="width: 70px !important;"
                                    <?php echo @$common_field ?? '' ?>
                                    <?php echo @$readonly ?>
                                    name="<? echo $fieldname ?>-m">
                                    <option value="" selected></option>
                                    <?php for ($c = 1; $c <= 12; $c++) : ?>
                                    <option value="<?php echo $c; ?>" <?php if(@$fieldvalue_m == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                    <?php endfor; ?>
                                </select>
                                <select id="id_<? echo $fieldname ?>-y"
                                    style="width: 70px !important;"
                                    <?php echo @$common_field ?? '' ?>
                                    <?php echo @$readonly ?>
                                    name="<? echo $fieldname ?>-y">
                                    <option value="" selected></option>
                                    <?php for ($c = 2001; $c <= 2040; $c++) : ?>
                                    <option value="<?php echo $c; ?>" <?php if(@$fieldvalue_y == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 4, '0', STR_PAD_LEFT); ?></option>
                                    <?php endfor; ?>
                                </select>
                                dd-mm-yyyy

                            </div>
                            <div class="text-center">
                                <?php if(!empty($fieldvalue)): ?>
                                    <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                    <?php $field_ok = false;
                                endif; ?>
                            </div>
                        </div>
                    </div>
                    <!-- ############################################################################################################### -->
                    <div class="fieldWrapper">
                        <? $fieldname = 'results_date_first_publication'; ?>
                        <? $fieldvalue_d = $passo_10[$fieldname.'-d'] ?? set_value($fieldname.'-d') ?>
                        <? $fieldvalue_m = $passo_10[$fieldname.'-m'] ?? set_value($fieldname.'-m') ?>
                        <? $fieldvalue_y = $passo_10[$fieldname.'-y'] ?? set_value($fieldname.'-y') ?>
                        <div class="row-input label-line">
                            <div>
                                <label for="id_<? echo $fieldname ?>"><?php echo $fieldlabel->get_or_new($fieldname); ?>:</label>
                            </div>
                            <div class="help-input-text">
                            <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                  data-original-title="<?php echo $fieldhelp->get_or_new($fieldname); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <?php echo form_error($fieldname, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url('assets/images/field-error.png') . '"/></div>'); ?>
                        <div class="row-input input error-<? echo $fieldname ?>" id="<? echo $fieldname ?>">
                            <div class="field-input">
                                <!--
                                <input id="id_<? echo $fieldname ?>"
                                       name="<? echo $fieldname ?>"
                                    <?php echo @$common_field ?? '' ?>
                                       type="date" value="<?php echo $fieldvalue; ?>" />
                                -->
                                <select id="id_<? echo $fieldname ?>-d"
                                    style="width: 70px !important;"
                                    <?php echo @$common_field ?? '' ?>
                                    <?php echo @$readonly ?>
                                    name="<? echo $fieldname ?>-d">
                                    <option value="" selected></option>
                                    <?php for ($c = 1; $c <= 31; $c++) : ?>
                                    <option value="<?php echo $c; ?>" <?php if(@$fieldvalue_d == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                    <?php endfor; ?>
                                </select>
                                <select id="id_<? echo $fieldname ?>-m"
                                    style="width: 70px !important;"
                                    <?php echo @$common_field ?? '' ?>
                                    <?php echo @$readonly ?>
                                    name="<? echo $fieldname ?>-m">
                                    <option value="" selected></option>
                                    <?php for ($c = 1; $c <= 12; $c++) : ?>
                                    <option value="<?php echo $c; ?>" <?php if(@$fieldvalue_m == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 2, '0', STR_PAD_LEFT); ?></option>
                                    <?php endfor; ?>
                                </select>
                                <select id="id_<? echo $fieldname ?>-y"
                                    style="width: 70px !important;"
                                    <?php echo @$common_field ?? '' ?>
                                    <?php echo @$readonly ?>
                                    name="<? echo $fieldname ?>-y">
                                    <option value="" selected></option>
                                    <?php for ($c = 2001; $c <= 2040; $c++) : ?>
                                    <option value="<?php echo $c; ?>" <?php if(@$fieldvalue_y == $c) echo 'selected="selected"'; ?>><?php echo str_pad($c, 4, '0', STR_PAD_LEFT); ?></option>
                                    <?php endfor; ?>
                                </select>
                                dd-mm-yyyy

                            </div>
                            <div class="text-center">
                                <?php if(!empty($fieldvalue)): ?>
                                    <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                    <?php $field_ok = false;
                                endif; ?>
                            </div>
                        </div>
                    </div>
                    <!-- ############################################################################################################### -->
                    <div class="fieldWrapper">
                        <? $fieldname = 'results_url_link'; ?>
                        <? $fieldvalue = $passo_10[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="id_<? echo $fieldname ?>"><?php echo $fieldlabel->get_or_new($fieldname); ?>:</label>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new($fieldname); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <div class="row-input input error-id_<? echo $fieldname ?>">
                            <div class="field-input">

                                <?php if (@$common_field) : ?>
                                    <a target="_blank" href="<?php echo @$fieldvalue; ?>"><?php 
                                        echo @$fieldvalue; 
                                    ?></a>
                                <?php else : ?>
                                    <input id="id_<? echo $fieldname ?>"
                                        name="<? echo $fieldname ?>"
                                        value="<?php echo $fieldvalue; ?>"
                                        type="text">
                                <?php endif; ?>


                            </div>
                            <div class="text-center"></div>
                        </div>
                    </div>
                    <!-- ############################################################################################################### -->
                    <? $fieldname = 'results_baseline_char'; ?>
                    <? $fieldvalue = $passo_10[$fieldname] ?? set_value($fieldname); ?>
                    <p class="form-label <? echo $fieldname ?>"><label for="id_form-0-<? echo $fieldname ?>"><?php echo $fieldlabel->get_or_new($fieldname); ?>:</label></p><span class="fielct-required "></span>
                    <div class="help-input-text">
                        <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                              data-original-title="<?php echo $fieldhelp->get_or_new($fieldname); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                    </div>
                    <div class="fields-wrapper">
                        <?php $field_ok = false; ?>
                        <?php foreach ($passo_10['selected_languages'] as $language):?>

                            <div class="fieldWrapper">
                                <div class="row-input label-line">
                                    <div class="icon_caret">
                                        <span class="glyphicon caret"></span><?php echo $genericlabel->get_or_new('language_'.$language); ?>
                                    </div>
                                    <div class="revision-icons-wrapper"></div>
                                </div>
                                <?php echo form_error($fieldname. '_' . $language, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                <div class="row-input">
                                    <div class="field-input ">
                                    <textarea cols="40"
                                              id="id_<? echo $fieldname ?>_<?php echo $language; ?>"
                                              name="<? echo $fieldname ?>_<?php echo $language; ?>"
                                              <?php echo @$common_field ?? '' ?>
                                              rows="10"><?php echo $passo_10[$fieldname.'_'.$language]; ?></textarea>
                                    </div>
                                    <div class="text-center">
                                        <?php if($field_ok): ?>
                                            <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                            <?php $field_ok = false;
                                        endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- ############################################################################################################### -->
                    <? $fieldname = 'results_participant_flow'; ?>
                    <? $fieldvalue = $passo_10[$fieldname] ?? set_value($fieldname); ?>
                    <p class="form-label <? echo $fieldname ?>"><label for="id_form-0-<? echo $fieldname ?>"><?php echo $fieldlabel->get_or_new($fieldname); ?>:</label></p><span class="fielct-required "></span>
                    <div class="help-input-text">
                        <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                              data-original-title="<?php echo $fieldhelp->get_or_new($fieldname); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                    </div>
                    <div class="fields-wrapper">
                        <?php $field_ok = false; ?>
                        <?php foreach ($passo_10['selected_languages'] as $language):?>

                            <div class="fieldWrapper">
                                <div class="row-input label-line">
                                    <div class="icon_caret">
                                        <span class="glyphicon caret"></span><?php echo $genericlabel->get_or_new('language_'.$language); ?>
                                    </div>
                                    <div class="revision-icons-wrapper"></div>
                                </div>
                                <?php echo form_error($fieldname. '_' . $language, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                <div class="row-input">
                                    <div class="field-input ">
                                    <textarea cols="40"
                                              id="id_<? echo $fieldname ?>_<?php echo $language; ?>"
                                              name="<? echo $fieldname ?>_<?php echo $language; ?>"
                                              <?php echo @$common_field ?? '' ?>
                                              rows="10"><?php echo $passo_10[$fieldname.'_'.$language]; ?></textarea>
                                    </div>
                                    <div class="text-center">
                                        <?php if($field_ok): ?>
                                            <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                            <?php $field_ok = false;
                                        endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- ############################################################################################################### -->
                    <? $fieldname = 'results_adverse_events'; ?>
                    <? $fieldvalue = $passo_10[$fieldname] ?? set_value($fieldname); ?>
                    <p class="form-label <? echo $fieldname ?>"><label for="id_form-0-<? echo $fieldname ?>"><?php echo $fieldlabel->get_or_new($fieldname); ?>:</label></p><span class="fielct-required "></span>
                    <div class="help-input-text">
                        <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                              data-original-title="<?php echo $fieldhelp->get_or_new($fieldname); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                    </div>
                    <div class="fields-wrapper">
                        <?php $field_ok = false; ?>
                        <?php foreach ($passo_10['selected_languages'] as $language):?>

                            <div class="fieldWrapper">
                                <div class="row-input label-line">
                                    <div class="icon_caret">
                                        <span class="glyphicon caret"></span><?php echo $genericlabel->get_or_new('language_'.$language); ?>
                                    </div>
                                    <div class="revision-icons-wrapper"></div>
                                </div>
                                <?php echo form_error($fieldname. '_' . $language, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                <div class="row-input">
                                    <div class="field-input ">
                                    <textarea cols="40"
                                              id="id_<? echo $fieldname ?>_<?php echo $language; ?>"
                                              name="<? echo $fieldname ?>_<?php echo $language; ?>"
                                              <?php echo @$common_field ?? '' ?>
                                              rows="10"><?php echo $passo_10[$fieldname.'_'.$language]; ?></textarea>
                                    </div>
                                    <div class="text-center">
                                        <?php if($field_ok): ?>
                                            <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                            <?php $field_ok = false;
                                        endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- ############################################################################################################### -->
                    <? $fieldname = 'results_outcome_measure'; ?>
                    <? $fieldvalue = $passo_10[$fieldname] ?? set_value($fieldname); ?>
                    <p class="form-label <? echo $fieldname ?>"><label for="id_form-0-<? echo $fieldname ?>"><?php echo $fieldlabel->get_or_new($fieldname); ?>:</label></p><span class="fielct-required "></span>
                    <div class="help-input-text">
                        <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                              data-original-title="<?php echo $fieldhelp->get_or_new($fieldname); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                    </div>
                    <div class="fields-wrapper">
                        <?php $field_ok = false; ?>
                        <?php foreach ($passo_10['selected_languages'] as $language):?>

                            <div class="fieldWrapper">
                                <div class="row-input label-line">
                                    <div class="icon_caret">
                                        <span class="glyphicon caret"></span><?php echo $genericlabel->get_or_new('language_'.$language); ?>
                                    </div>
                                    <div class="revision-icons-wrapper"></div>
                                </div>
                                <?php echo form_error($fieldname. '_' . $language, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                <div class="row-input">
                                    <div class="field-input ">
                                    <textarea cols="40"
                                              id="id_<? echo $fieldname ?>_<?php echo $language; ?>"
                                              name="<? echo $fieldname ?>_<?php echo $language; ?>"
                                              <?php echo @$common_field ?? '' ?>
                                              rows="10"><?php echo $passo_10[$fieldname.'_'.$language]; ?></textarea>
                                    </div>
                                    <div class="text-center">
                                        <?php if($field_ok): ?>
                                            <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                            <?php $field_ok = false;
                                        endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- ############################################################################################################### -->
                    <div class="fieldWrapper">
                        <? $fieldname = 'results_url_protocol'; ?>
                        <? $fieldvalue = $passo_10[$fieldname] ?? set_value($fieldname); ?>
                        <div class="row-input label-line">
                            <div class="no-margin-left">
                                <label for="id_<? echo $fieldname ?>"><?php echo $fieldlabel->get_or_new($fieldname); ?>:</label>
                            </div>
                            <div class="help-input-text">
                                <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                                      data-original-title="<?php echo $fieldhelp->get_or_new($fieldname); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                            </div>
                            <div class="revision-icons-wrapper"></div>
                        </div>
                        <div class="row-input input error-id_<? echo $fieldname ?>">
                            <div class="field-input">

                                <?php if (@$common_field) : ?>
                                    <a target="_blank" href="<?php echo @$fieldvalue; ?>"><?php 
                                        echo @$fieldvalue; 
                                    ?></a>
                                <?php else : ?>
                                    <input id="id_<? echo $fieldname ?>"
                                        name="<? echo $fieldname ?>"
                                        value="<?php echo $fieldvalue; ?>"
                                        type="text">
                                <?php endif; ?>

                            </div>
                            <div class="text-center"></div>
                        </div>
                    </div>
                    <!-- ############################################################################################################### -->
                    <? $fieldname = 'results_summary'; ?>
                    <? $fieldvalue = $passo_10[$fieldname] ?? set_value($fieldname); ?>
                    <p class="form-label <? echo $fieldname ?>"><label for="id_form-0-<? echo $fieldname ?>"><?php echo $fieldlabel->get_or_new($fieldname); ?>:</label></p><span class="fielct-required "></span>
                    <div class="help-input-text">
                        <span class="field-help" data-html="true" data-toggle="tooltip" data-placement="left"
                              data-original-title="<?php echo $fieldhelp->get_or_new($fieldname); ?>"><?php echo $genericlabel->get_or_new('help'); ?><img src="<?php echo base_url('assets/images/interrog.png'); ?>"></span>
                    </div>
                    <div class="fields-wrapper">
                        <?php $field_ok = false; ?>
                        <?php foreach ($passo_10['selected_languages'] as $language):?>

                            <div class="fieldWrapper">
                                <div class="row-input label-line">
                                    <div class="icon_caret">
                                        <span class="glyphicon caret"></span><?php echo $genericlabel->get_or_new('language_'.$language); ?>
                                    </div>
                                    <div class="revision-icons-wrapper"></div>
                                </div>
                                <?php echo form_error($fieldname. '_' . $language, '<div class="input row-input with-error">', '<img data-placement="left" class="field-error" src="'. base_url(IMAGE_FIELD_ERROR) . '" /></div>'); ?>
                                <div class="row-input">
                                    <div class="field-input ">
                                    <textarea cols="40"
                                              id="id_<? echo $fieldname ?>_<?php echo $language; ?>"
                                              name="<? echo $fieldname ?>_<?php echo $language; ?>"
                                              <?php echo @$common_field ?? '' ?>
                                              rows="10"><?php echo $passo_10[$fieldname.'_'.$language]; ?></textarea>
                                    </div>
                                    <div class="text-center">
                                        <?php if($field_ok): ?>
                                            <img src="<?php echo base_url('assets/images/field-ok.png'); ?>">
                                            <?php $field_ok = false;
                                        endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- ############################################################################################################### -->
                    <div class="submit-line">
                        <?php if (($can_save ?? FALSE) && !($reviewer ?? FALSE)) : ?>
                            <input type="submit"
                                   value="<?php echo $genericlabel->get_or_new('button_save'); ?>"
                                   class="btn btn-primary pull-right">
                        <?php endif; ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $genericlabel->disableUpdate(); ?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>