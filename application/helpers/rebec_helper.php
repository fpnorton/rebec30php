<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if(!function_exists('botao_cadastro_instituicao'))
{
    function botao_cadastro_instituicao()
    {
        $genericlabel = new Helper\FieldLabelLanguage('General');
        $genericlabel->enableUpdate();
        ?><input type="button"
                   name="botao_cadastro_instituicao"
                   value="<?php echo $genericlabel->get_or_new('new_institution'); ?>"
                   class="button btn-primary" 
                   data-backdrop="static"
                   data-toggle="modal"
                   data-target="#add-institution-modal"/><?php
        $genericlabel->disableUpdate();
    }
}
if(!function_exists('botao_cadastro_contato'))
{
    function botao_cadastro_contato($clinicaltrial_id = 0)
    {
        $genericlabel = new Helper\FieldLabelLanguage('General');
        $genericlabel->enableUpdate();
        ?><input type="button"
               name="botao_cadastro_contato"
               value="<?php echo $genericlabel->get_or_new('new_contact'); ?>"
               class="button btn-primary"
               data-backdrop="static"
               data-toggle="modal"
               data-clinicaltrial_id="<?php echo $clinicaltrial_id; ?>"
               data-combo=""
               data-target="#add-contact-modal" /><?php
        $genericlabel->disableUpdate();
    }
}
if(!function_exists('botao_editar_contato'))
{
    function botao_editar_contato($clinicaltrial_id, $combo_identifier = '')
    {
        $genericlabel = new Helper\FieldLabelLanguage('General');
        $genericlabel->enableUpdate();
        ?><input type="button" 
                   name="botao_editar_contato"
                   value="<?php echo $genericlabel->get_or_new('edit_contact'); ?>" 
                   class="button btn-primary" 
                   data-toggle="modal" 
                   data-backdrop="static"
                   data-clinicaltrial_id="<?php echo $clinicaltrial_id; ?>"
                   data-combo="<?php echo $combo_identifier; ?>"
                   data-target="#add-contact-modal"/><?php
        $genericlabel->disableUpdate();
    }
}
if(!function_exists('botao_seletor_linguagens')) {
    function botao_seletor_linguagens()
    { ?>
        <span class="dropdown">
            <button class="btn dropdown-toggle"
                    type="button" id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false">
                <? $cookie = get_cookie('LANGUAGE'); ?>
                <? if (in_array($cookie, array_keys(\Helper\Language::$available))) : ?>
                    <span class="flag-icon <?php echo \Helper\Language::$available[$cookie]['flag'] ?>"></span>
                    <span class="d-none d-lg-inline"> <?php echo \Helper\Language::$available[$cookie]['name']; ?></span>
                <? else : ?>
                    <span class="d-none d-lg-inline"><? echo 'Languages'; ?></span>
                <? endif; ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <?php foreach (\Helper\Language::$available as $language_key => $language_value) : ?>
                    <a class="dropdown-item select-language" data-value="<?php echo $language_key ?>"
                       href="#">
                        <span class="flag-icon <?php echo $language_value['flag'] ?>"></span>
                        <span> <?php echo $language_value['name'] ?></span>
                    </a>
                <?php endforeach; ?>
            </div>
            <script>
                $(window).ready(function () {
                    $('.select-language').on('click', function (event) {
                        var cname = "LANGUAGE";
                        var cvalue = $(this).attr('data-value');
                        $(this).parents(".dropdown").find('.btn').html($(this).html());
                        document.cookie = cname + "=" + cvalue + "; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
                        window.location.reload(false);
                    });
                });
            </script>
        </span>
        <?php
    }
}
if(!function_exists('linguagem_selecionada')) {
    function linguagem_selecionada()
    {
        $linguagem_selecionada = get_cookie('LANGUAGE');
        if (in_array($linguagem_selecionada, ['en','es','pt-br'])) {
            return $linguagem_selecionada;
        }
        return 'en';
    }
}

?>