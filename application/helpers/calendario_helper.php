<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This function used to get the CI instance
 */
if(!function_exists('get_data_por_extenso'))
{
    function get_data_por_extenso($date, $weekday = FALSE)
    {
        $cal_week = array(
            0 => 'sunday',
            1 => 'monday',
            2 => 'tuesday',
            3 => 'wednesday',
            4 => 'thursday',
            5 => 'friday',
            6 => 'saturday',
        );
        $cal_month = array(
            '01' => 'january',
            '02' => 'february',
            '03' => 'march',
            '04' => 'april',
            '05' => 'mayl',
            '06' => 'june',
            '07' => 'july',
            '08' => 'august',
            '09' => 'september',
            '10' => 'october',
            '11' => 'november',
            '12' => 'december',
        );
        $time = strtotime($date);
        $week = strftime('%w', $time);
        $month = strftime('%m', $time);
        if ($weekday) {
            return lang('cal_'.$cal_week[$week]).', '.strftime('%d', $time).' '.$cal_month[$month].' - '.strftime('%Y', $time);
        }
        return strftime('%d', $time).' '.lang('cal_'.$cal_month[$month]).' - '.strftime('%Y', $time);
    }
}

?>