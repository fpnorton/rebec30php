<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Class LanguageSwitcherHook
 */
class LanguageSwitcherHook {

    /**
     * Holds the instance
     * @var object
     */
    protected $instance;
    static public $languages = [];

    /**
     * Gets CI instance
     */
    private function setInstance() {
        $this->instance =& get_instance();
    }

    public function postControllerCaller()
    {
        (get_instance())->load->helper('cookie');

//        error_log(var_export([' ------- POS ------- ', self::$languages], true));
    }

    public function preControllerCaller()
    {
        (get_instance())->load->helper('cookie');

        self::$languages[0] = get_cookie('rebec_language');

//        error_log(var_export([' ------- PRE ------- ', self::$languages], true));

//        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
//
//        }

//        error_log(var_export(['pre', $_COOKIE, get_class($this->instance), get_class_vars($this->instance)], true));

//        $this->loadDatabase();
//
//        $config = $this->getDB();
//
//        $capsule = new Capsule;
//
//        $capsule->addConnection([
//            'driver'    => 'mysql',
//            'host'      => $config->hostname,
//            'database'  => $config->database,
//            'username'  => $config->username,
//            'password'  => $config->password,
//            'charset'   => $config->char_set,
//            'collation' => $config->dbcollat,
//            'prefix'    => $config->dbprefix,
//        ]);
//
//        $capsule->setAsGlobal();
//        $capsule->bootEloquent();
    }

}