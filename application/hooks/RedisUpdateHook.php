<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class LanguageSwitcherHook
 */
class RedisUpdateHook {

    /**
     * Holds the instance
     * @var object
     */
    protected $instance;
    static public $languages = [];

    private $redis = NULL;

    /**
     * Gets CI instance
     */
    private function setInstance() {
        $this->instance =& get_instance();
        $this->redis = new \Redis();
        if ($this->redis->connect('redis', 6379)) {
            $this->redis->auth('rebec');
        } else {
            $this->redis = false;
        }
    }

    public function postSystemCaller()
    {
        if ($this->redis) $this->redis->save();
    }

}