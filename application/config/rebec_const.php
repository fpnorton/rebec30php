<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This config contain the constants for REBEC
$config['system_name'] = 'REBEC';
$config['pt-br'] = 'portuguese-brazilian';
$config['en'] = 'english';
$config['es'] = 'spanish';
$config['available_languages'] = 'pt-br;en;es';
$config['mandatory_languages'] = 'pt-br;en';
$config['other_languages'] = 'es';


$config['dummy'] = 'dummy';

// constants
define('FIELD_STATUS_REMARK',   1);
define('FIELD_STATUS_MISSING',  2);
define('FIELD_STATUS_PARTIAL',  3);
define('FIELD_STATUS_COMPLETE', 4);

define('TRIAL_FORMS_IDENTIFICATION',	   'Trial Identification');
define('TRIAL_FORMS_PRIMARY_SPONSOR',	   'Sponsors');
define('TRIAL_FORMS_HEALTH_CONDITIONS',    'Health Conditions');
define('TRIAL_FORMS_INTERVENTIONS', 	   'Interventions');
define('TRIAL_FORMS_RECRUITMENT', 		   'Recruitment');
define('TRIAL_FORMS_STUDY_TYPE', 	       'Study Type');
define('TRIAL_FORMS_OUTCOMES', 			   'Outcomes');
define('TRIAL_FORMS_CONTACTS', 			   'Contacts');
define('TRIAL_FORMS_ATTACHMENTS', 	       'Attachments');
define('TRIAL_FORMS_SUMMARY_RESULTS',      'Summary Results');
define('TRIAL_FORMS_IPD_SHARING_STATEMENT','IPD Sharing Statement');

const PASSO_TERMO                                       = -2;
const PASSO_DADOS_INICIAIS                              = -1;
const PASSO_SUMARIO	                                    =  0;
const PASSO_IDENTIFICACAO                               =  1;
const PASSO_ANEXOS                                      =  2;
const PASSO_PATROCINADORES                              =  3;
const PASSO_CONDICOES_SAUDE                             =  4;
const PASSO_INTERVENCAO                                 =  5;
const PASSO_RECRUTAMENTO                                =  6;
const PASSO_DESENHO_ESTUDO                              =  7;
const PASSO_DESFECHOS 		                            =  8;
const PASSO_CONTATOS 		                            =  9;
const PASSO_RESUMO_RESULTADOS                           = 10;
const PASSO_TERMO_COMPARTILHAMENTO_DADOS_INDIVIDUAIS    = 11;
const PASSO_ENVIAR                                      = 99;

const STEP_TERMO			= -1;
const STEP_DADOS_INICIAIS	= 0;
const STEP_SUMARIO			= 1;
const STEP_IDENTIFICACAO	= 2;
const STEP_ANEXOS			= 3;
const STEP_PATROCINADORES	= 4;
const STEP_CONDICOES_SAUDE	= 5;
const STEP_INTERVENCAO		= 6;
const STEP_RECRUTAMENTO		= 7;
const STEP_DESENHO_ESTUDO	= 8;
const STEP_DESFECHOS		= 9;
const STEP_CONTATOS			= 10;
const STEP_ENVIAR			= 11;

const A_INITIAL_STEPS = array(
	STEP_TERMO			=> "",
	STEP_DADOS_INICIAIS	=> "",
	STEP_SUMARIO		=> "",
);

const A_STEPS = array(
    STEP_IDENTIFICACAO		=> "",
    STEP_ANEXOS				=> "",
    STEP_PATROCINADORES		=> "",
    STEP_CONDICOES_SAUDE	=> "",
    STEP_INTERVENCAO		=> "",
    STEP_RECRUTAMENTO		=> "",
    STEP_DESENHO_ESTUDO		=> "",
    STEP_DESFECHOS			=> "",
    STEP_CONTATOS			=> "",
);

const A_PASSOS = array(
    PASSO_SUMARIO		=> "",
    PASSO_IDENTIFICACAO		=> "",
    PASSO_ANEXOS			=> "",
    PASSO_PATROCINADORES	=> "",
    PASSO_CONDICOES_SAUDE	=> "",
    PASSO_INTERVENCAO		=> "",
    PASSO_RECRUTAMENTO		=> "",
    PASSO_DESENHO_ESTUDO	=> "",
    PASSO_DESFECHOS			=> "",
    PASSO_CONTATOS			=> "",
    PASSO_ENVIAR            => "",
);

const A_STEP_VIEW = array(
	STEP_TERMO				=> "termo",
	STEP_DADOS_INICIAIS		=> "dados_iniciais",
	STEP_SUMARIO			=> "summary",
	STEP_IDENTIFICACAO		=> "identification",
	STEP_ANEXOS				=> "attachments",
	STEP_PATROCINADORES		=> "sponsors",
	STEP_CONDICOES_SAUDE	=> "health_conditions",
	STEP_INTERVENCAO		=> "interventions",
	STEP_RECRUTAMENTO		=> "recruitment",
	STEP_DESENHO_ESTUDO		=> "study_type",
	STEP_DESFECHOS			=> "outcomers",
	STEP_CONTATOS			=> "contacts",
);     

const A_STEP_STATUS = array(
	STEP_IDENTIFICACAO		=> 'Trial Identification',
	STEP_ANEXOS				=> 'Attachments',
	STEP_PATROCINADORES		=> 'Sponsors',
	STEP_CONDICOES_SAUDE	=> 'Health Conditions',
	STEP_INTERVENCAO		=> 'Interventions',
	STEP_RECRUTAMENTO		=> 'Recruitment',
	STEP_DESENHO_ESTUDO		=> 'Study Type',
	STEP_DESFECHOS			=> 'Outcomes',
	STEP_CONTATOS			=> 'Contacts',
);  

const A_STEP_CLASS = array(
	STEP_TERMO				=> "Step1Termo",
	STEP_DADOS_INICIAIS		=> "Step0DadosIniciais",
	STEP_SUMARIO			=> "Step1Sumario",
	STEP_IDENTIFICACAO		=> "Step2Identificacao",
	STEP_ANEXOS				=> "Step3Anexos",
	STEP_PATROCINADORES		=> "Step4Patrocinadores",
	STEP_CONDICOES_SAUDE	=> "Step5CondicoesSaude",
	STEP_INTERVENCAO		=> "Step6Intervencao",
	STEP_RECRUTAMENTO		=> "Step7Recrutamento",
	STEP_DESENHO_ESTUDO		=> "Step8DesenhoEstudo",
	STEP_DESFECHOS			=> "Step9Desfecho",
	STEP_CONTATOS			=> "Step10Contatos",
);
$FIELD_ORDER = [
    'Trial Identification',
    'Attachments',
    'Sponsors',
    'Health Conditions',
    'Interventions',
    'Recruitment',
    'Study Type',
    'Outcomes',
    'Contacts',
    'Summary Results',
    'IPD Sharing Statement',
];

class ClinicalTrialSteps {

    const fieldsByName = [
        'Trial Identification',
        'Attachments',
        'Sponsors',
        'Health Conditions',
        'Interventions',
        'Recruitment',
        'Study Type',
        'Outcomes',
        'Contacts',
        'Summary Results',
        'IPD Sharing Statement',
    ];

    const remarksByName = [
        'trial-identification',
        'attachments',
        'sponsors',
        'health-conditions',
        'interventions',
        'recruitment',
        'study-type',
        'outcomes',
        'contacts',
        'summary-results',
        'ipd-sharing-statement',
    ];

    private $_step = null;
    private $_next = null;
    private $_prev = null;
    public function __construct($step = STEP_TERMO)
    {
        $this->_step = $step;
        $this->_update();
//        $arr_passo = explode('-', $step);
//        $passo = $arr_passo[0];
//        $passo_anterior = count($arr_passo) > 1 ? $arr_passo[1] : '';
//        parent::__construct();
    }

    static public function getFieldStatusCompletedByStep($fieldStatus, $step) {
        $field = self::fieldsByName[$step - 1];
        return self::getFieldStatusCompletedByName($fieldStatus, $field);
    }

    static public function getFieldStatusCompletedByName($fieldStatus, $field) {
        return $fieldStatus[$field];
    }

    private function _update() {
        $this->_next = $this->_step == null ? STEP_TERMO : $this->_step + 1;
        $this->_prev = ($this->_step != null && $this->_step > -1) ? ($this->_step - 1) : null;
    }

    public function reward() {
        $this->_step = $this->_prev;
        $this->_update();
    }

    public function forward() {
        $this->_step = $this->_next;
        $this->_update();
    }

    public function step() {
        return $this->_step;
    }

    public function prev() {
        return $this->_prev;
    }

    public function next() {
        return $this->_next;
    }

    public function view() {
        return A_STEP_VIEW[$this->_step];
    }

    public function nextView() {
        return A_STEP_VIEW[$this->_next];
    }

    public function prevView() {
        return A_STEP_VIEW[$this->_prev];
    }

    public function isNotTermo()
    {
        return $this->_step != null && $this->_step != STEP_TERMO;
    }

    public function isDadosIniciais()
    {
        return $this->_step != null && $this->_step == STEP_DADOS_INICIAIS;
    }

    public function isNotSumario()
    {
        return $this->_step != null &&
               $this->_step != STEP_TERMO &&
               $this->_step != STEP_DADOS_INICIAIS &&
               $this->_step != STEP_SUMARIO;
    }


}
