<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Vocabulary Config
| -------------------------------------------------------------------------
| This file map the vocabulary types for Rebec.
| This array contains the mappings for the vocabulary content types from django_content_types table
*/
$config['attachmenttype'] = '23';
$config['countrycode'] = '11';
$config['decsdisease'] = '21';
$config['icdchapter'] = '22';
$config['institutiontype'] = '61';
$config['interventionassigment'] = '16';
$config['interventioncode'] = '13';
$config['mailmessage'] = '64';
$config['observationalstudydesign'] = '63';
$config['recruitmentstatus'] = '20';
$config['studyallocation'] = '18';
$config['studymasking'] = '17';
$config['studyphase'] = '19';
$config['studypurpose'] = '15';
$config['studytype'] = '14';
$config['timeperspective'] = '62';
$config['trialnumberissuingauthority'] = '12';
$config['vocabularytranslation'] = '10';