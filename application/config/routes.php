<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = "welcome";
//$route['404_override'] = 'error';

$route['translate_uri_dashes'] = FALSE;

/*********** USER DEFINED ROUTES *******************/
// $route['information']['get']='zeus/information/researcher/teste';
$route['information/researcher']['get']='zeus/information/researcher_test';
$route['information/researcher']['post']='zeus/information/researcher';
$route['information/php']['get']='zeus/information/php';

//$route['signin']['get']='zeus/signin/form';
$route['signin']['post']='zeus/signin/login';

//$route['signout']['get']='zeus/signout/form';
$route['signout']['post']='zeus/signout/logout';

$route['signup']['get']='zeus/signup/form';
$route['signup']['post']='zeus/signup/register';
$route['signup/activate/(:any)']['get']='zeus/signup/activate/$1';

$route['reset']['post']='zeus/token/forgot_password';
$route['reset/(:any)']['get']='zeus/token/reset_password/$1';
$route['reset/(:any)']['post']='zeus/token/change_password/$1';

$route['profile']['get']='zeus/profile/edit';
$route['profile']['post']='zeus/profile/save';

$route['metis/(:any)/query']['post']='metis/$1/query';
$route['metis/(:any)/(:any)']['get']='metis/$1/$2';
$route['search/query/simple']='metis/search/simple_query';

$route['robots\.txt']['get']='eris/configuration/robots_read';

$route['eris/(:any)']['get']='eris/$1/index';
$route['eris/(:any)/paginate']['get']='eris/$1/paginate';
$route['eris/(:any)/edit/(:num)']['get']='eris/$1/edit/$2';
$route['eris/(:any)/edit/(:num)']['post']='eris/$1/save';
$route['eris/(:any)/create']['get']='eris/$1/create';
$route['eris/(:any)/create']['post']='eris/$1/save';

//$route['ares/(:any)']['get']='ares/$1/index';
$route['pesquisador']                                   = 'ares/main/index';
$route['pesquisador/(:any)/(:num)']['post']             = 'ares/main/$1/$2';
$route['pesquisador/submissao/nova']['get']             = 'ares/trial/nova_1';
$route['pesquisador/submissao/nova']['post']            = 'ares/trial/nova_2';
$route['pesquisador/submissao/start']['post']           = 'ares/trial/nova_3';
$route['pesquisador/submissao/sumario/(:num)']['get']   = 'ares/summary/view/$1';

$route['pesquisador/submissao/passo/1/(:num)']['get']   = 'ares/identification/edit/$1';
$route['pesquisador/submissao/passo/2/(:num)']['get']   = 'ares/attachments/edit/$1';
$route['pesquisador/submissao/passo/2/(:num)/download/(:num)']['get'] = 'ares/attachments/download/$1/$2';
$route['pesquisador/submissao/passo/3/(:num)']['get']   = 'ares/sponsors/edit/$1';
$route['pesquisador/submissao/passo/4/(:num)']['get']   = 'ares/healthConditions/edit/$1';
$route['pesquisador/submissao/passo/5/(:num)']['get']   = 'ares/interventions/edit/$1';
$route['pesquisador/submissao/passo/6/(:num)']['get']   = 'ares/recruitment/edit/$1';
$route['pesquisador/submissao/passo/7/(:num)']['get']   = 'ares/studyType/edit/$1';
$route['pesquisador/submissao/passo/8/(:num)']['get']   = 'ares/outcomes/edit/$1';
$route['pesquisador/submissao/passo/9/(:num)']['get']   = 'ares/contacts/edit/$1';
$route['pesquisador/submissao/passo/10/(:num)']['get']   = 'ares/summaryResults/edit/$1';
$route['pesquisador/submissao/passo/11/(:num)']['get']   = 'ares/IPDSharingStatement/edit/$1';

$route['pesquisador/submissao/passo/1/(:num)']['post']   = 'ares/identification/save/$1';
$route['pesquisador/submissao/passo/2/(:num)']['post']   = 'ares/attachments/save/$1';
$route['pesquisador/submissao/passo/3/(:num)']['post']   = 'ares/sponsors/save/$1';
$route['pesquisador/submissao/passo/4/(:num)']['post']   = 'ares/healthConditions/save/$1';
$route['pesquisador/submissao/passo/5/(:num)']['post']   = 'ares/interventions/save/$1';
$route['pesquisador/submissao/passo/6/(:num)']['post']   = 'ares/recruitment/save/$1';
$route['pesquisador/submissao/passo/7/(:num)']['post']   = 'ares/studyType/save/$1';
$route['pesquisador/submissao/passo/8/(:num)']['post']   = 'ares/outcomes/save/$1';
$route['pesquisador/submissao/passo/9/(:num)']['post']   = 'ares/contacts/save/$1';
$route['pesquisador/submissao/passo/10/(:num)']['post']  = 'ares/summaryResults/save/$1';
$route['pesquisador/submissao/passo/11/(:num)']['post']  = 'ares/IPDSharingStatement/save/$1';

$route['observador/submissao/sumario/(:num)']['get']   = 'iris/summary/view/$1';
$route['observador/submissao/passo/1/(:num)']['get']   = 'iris/identification/edit/$1';
$route['observador/submissao/passo/2/(:num)']['get']   = 'iris/attachments/edit/$1';
$route['observador/submissao/passo/2/(:num)/download/(:num)']['get'] = 'iris/attachments/download/$1/$2';
$route['observador/submissao/passo/3/(:num)']['get']   = 'iris/sponsors/edit/$1';
$route['observador/submissao/passo/4/(:num)']['get']   = 'iris/healthConditions/edit/$1';
$route['observador/submissao/passo/5/(:num)']['get']   = 'iris/interventions/edit/$1';
$route['observador/submissao/passo/6/(:num)']['get']   = 'iris/recruitment/edit/$1';
$route['observador/submissao/passo/7/(:num)']['get']   = 'iris/studyType/edit/$1';
$route['observador/submissao/passo/8/(:num)']['get']   = 'iris/outcomes/edit/$1';
$route['observador/submissao/passo/9/(:num)']['get']   = 'iris/contacts/edit/$1';
$route['observador/submissao/passo/10/(:num)']['get']  = 'iris/summaryResults/edit/$1';
$route['observador/submissao/passo/11/(:num)']['get']  = 'iris/IPDSharingStatement/edit/$1';

$route['audit/view/(:num)']['get']                     = 'iris/auditTrail/view/$1';
$route['audit/download/(:num)']['get']                 = 'iris/auditTrail/download/$1';

$route['revisor']['get']                            = 'enyo/main/blank';
$route['revisor/(:any)/(:num)']['post']             = 'enyo/main/$1/$2';
$route['revisor/createFossil/(:num)/(:num)']['get'] = 'enyo/main/createFossil/$1/$2';
$route['revisor/submissao/sumario/(:num)']['get']   = 'enyo/summary/view/$1';

$route['revisor/submissao/passo/1/(:num)']['get']   = 'enyo/identification/edit/$1';
$route['revisor/submissao/passo/2/(:num)']['get']   = 'enyo/attachments/edit/$1';
$route['revisor/submissao/passo/2/(:num)/download/(:num)']['get'] = 'enyo/attachments/download/$1/$2';
$route['revisor/submissao/passo/3/(:num)']['get']   = 'enyo/sponsors/edit/$1';
$route['revisor/submissao/passo/4/(:num)']['get']   = 'enyo/healthConditions/edit/$1';
$route['revisor/submissao/passo/5/(:num)']['get']   = 'enyo/interventions/edit/$1';
$route['revisor/submissao/passo/6/(:num)']['get']   = 'enyo/recruitment/edit/$1';
$route['revisor/submissao/passo/7/(:num)']['get']   = 'enyo/studyType/edit/$1';
$route['revisor/submissao/passo/8/(:num)']['get']   = 'enyo/outcomes/edit/$1';
$route['revisor/submissao/passo/9/(:num)']['get']   = 'enyo/contacts/edit/$1';
$route['revisor/submissao/passo/10/(:num)']['get']   = 'enyo/summaryResults/edit/$1';
$route['revisor/submissao/passo/11/(:num)']['get']   = 'enyo/IPDSharingStatement/edit/$1';

$route['revisor/submissao/passo/1/(:num)']['post']   = 'enyo/identification/save/$1';
$route['revisor/submissao/passo/2/(:num)']['post']   = 'enyo/attachments/save/$1';
$route['revisor/submissao/passo/3/(:num)']['post']   = 'enyo/sponsors/save/$1';
$route['revisor/submissao/passo/4/(:num)']['post']   = 'enyo/healthConditions/save/$1';
$route['revisor/submissao/passo/5/(:num)']['post']   = 'enyo/interventions/save/$1';
$route['revisor/submissao/passo/6/(:num)']['post']   = 'enyo/recruitment/save/$1';
$route['revisor/submissao/passo/7/(:num)']['post']   = 'enyo/studyType/save/$1';
$route['revisor/submissao/passo/8/(:num)']['post']   = 'enyo/outcomes/save/$1';
$route['revisor/submissao/passo/9/(:num)']['post']   = 'enyo/contacts/save/$1';
$route['revisor/submissao/passo/10/(:num)']['post']   = 'enyo/summaryResults/save/$1';
$route['revisor/submissao/passo/11/(:num)']['post']   = 'enyo/IPDSharingStatement/save/$1';

$route['page/(:any)']['get']='eris/flatPages/view/$1';

$route['news/(:num)']['get']='eris/news/view/$1';
$route['news']['get']='eris/news/list';

$route['trial/(:num)/(:num)']['get']='eris/fossil/view/FOSSIL/$1/$2';
$route['trial/(:num)']['get']='eris/fossil/view/FOSSIL/$1';
$route['trial']['get']='eris/fossil/list/FOSSIL';
$route['rg/(:any)/(:num)']['get']='eris/fossil/view/RBR/$1/$2';
$route['rg/(:any)']['get']='eris/fossil/view/RBR/$1';
$route['list']['get']='eris/fossil/list/RBR/1';
$route['list/(:num)']['get']='eris/fossil/list/RBR/$1';

$route['questions']['get']='eris/questions/list';

$route['faq']['get']='eris/questions/faq';

$route['ambrosia']='ambrosia/dashboard';
$route['api/web/retornaj/page/(:any)']= 'api/web/retornaj/$1';

$route['prototype/instituicao']['post'] = 'prototype/criarInstituicao';

$route['prototype/contato/(:num)/(:num)']['patch'] = 'prototype/editarContato/$1/$2';
$route['prototype/contato/(:num)']['patch'] = 'prototype/criarContato/$1';
$route['prototype/contato']['post'] = 'prototype/salvarContato';

$route['xml_ictrp']['get']                                          = 'hefesto/ictrp/index';
$route['xml_ictrp/downloadxmlictrp/all']['get']                     = 'hefesto/ictrp/downloadallxmlictrp';
$route['xml_ictrp/downloadxmlictrp/rbr/(:any)']['get']              = 'hefesto/ictrp/downloadtrialxmlictrp_by_rbr/$1/0';
$route['xml_ictrp/downloadxmlictrp/rbr/(:any)/(:num)']['get']       = 'hefesto/ictrp/downloadtrialxmlictrp_by_rbr/$1/$2';
$route['xml_ictrp/downloadxmlictrp/(:num)']['get']                  = 'hefesto/ictrp/downloadtrialxmlictrp_by_trial_id/$1';
$route['xml_ictrp/downloadxmlictrp/(:num)/(:num)']['get']           = 'hefesto/ictrp/downloadtrialxmlictrp_by_trial_id/$1/$2';
$route['xml_ictrp/downloadxmlictrp/(:any)/(:any)']['get']           = 'hefesto/ictrp/downloadtrialxmlictrp_by_periodo/$1/$2/0';
$route['xml_ictrp/downloadxmlictrp/(:any)/(:any)/(:num)']['get']    = 'hefesto/ictrp/downloadtrialxmlictrp_by_periodo/$1/$2/$3';
$route['xml_ictrp/downloadxmlictrp']['get']                         = 'hefesto/ictrp/downloadtrialxmlictrpfiltered';

$route['*'] = "welcome";