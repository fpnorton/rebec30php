<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/**** USER DEFINED CONSTANTS **********/

define('ROLE_ADMIN',                            '1');
define('ROLE_MANAGER',                         	'2');
define('ROLE_EMPLOYEE',                         '3');

define('SEGMENT',								2);

/************************** EMAIL CONSTANTS *****************************/

define('EMAIL_FROM',                            'Your from email');		// e.g. email@example.com
define('EMAIL_BCC',                            	'Your bcc email');		// e.g. email@example.com
define('FROM_NAME',                             'CIAS Admin System');	// Your system name
define('EMAIL_PASS',                            'Your email password');	// Your email password
define('PROTOCOL',                             	'smtp');				// mail, sendmail, smtp
define('SMTP_HOST',                             'Your smtp host');		// your smtp host e.g. smtp.gmail.com
define('SMTP_PORT',                             '25');					// your smtp port e.g. 25, 587
define('SMTP_USER',                             'Your smtp user');		// your smtp user
define('SMTP_PASS',                             'Your smtp password');	// your smtp password
define('MAIL_PATH',                             '/usr/sbin/sendmail');

//define('ROTA_PAGINA_INICIAL', 'prototype');
define('IMAGE_FIELD_ERROR', 'assets/images/field-error.png');
define('REBEC_TEMPLATE', 'default_0');

define('REBEC_PACK', 'default_0');

switch (REBEC_PACK) {
    case 'default_0':
/*
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/css/font-awesome.css'); ?>" rel="stylesheet" />
        <!-- Page-Level Plugin CSS - Dashboard -->
        <link href="<?php echo base_url('assets/css/morris-0.4.3.min.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/css/timeline.css'); ?>" rel="stylesheet" />
        <!-- SB Admin CSS - Include with every page -->
        <link href="<?php echo base_url(); ?>assets/css/sb-admin.css" rel="stylesheet" />
        <link href="<?php echo base_url('assets/css/960.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet" id="css" />
        <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet" type="text/css" media="print" />
        <link href="<?php echo base_url('assets/css/1.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/bootstrap-tour.min.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/css/bootstrap-tour-standalone.min.css'); ?>" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/clinical_trials.css'); ?>" />

 */
        define('CSS_VENDORS_FILES', serialize(array(
        )));
        define('CSS_FILES', serialize(array(
            array('href' => 'bootstrap.min.css'),

            array('href' => 'font-awesome.css'),

            array('href' => 'morris-0.4.3.min.css'),
            array('href' => 'timeline.css'),

            array('href' => 'sb-admin.css'),
            
            array('href' => '960.css'),
            array('href' => 'style.css', 'id' => 'css'),
            array('href' => 'print.css', 'media' => 'print'),
            array('href' => '1.css'),

            array('href' => 'bootstrap-tour.min.css'),
            array('href' => 'bootstrap-tour-standalone.min.css'),

            array('href' => 'clinical_trials.css'),
        )));
        define('JS_FILES', serialize(array(
            array('src' => 'jquery.js'),
            array('src' => 'bootstrap.min.js'),
            array('src' => 'portal.js'),
            array('src' => 'barra.js', 'defer' => 'defer', 'async' => 'async', 'onload' => 'link_barra_brasil()'),
        )));
        define('JS_VENDORS_FILES', serialize(array(
            // Google
            array('url' => 'http://maps.googleapis.com/maps/api/js?libraries=places&amp;sensor=false'),
        )));
/*
        <script type="text/javascript" src="<?php echo base_url('assets/scripts/jquery.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/scripts/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/scripts/portal.js'); ?>"></script>
        <script defer="defer" async="async" src="<?php echo base_url('assets/scripts/barra.js'); ?>" type="text/javascript" onload="link_barra_brasil()"></script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&amp;sensor=false"></script>
 */
        define('JS_FILES_FOOTER', serialize(array(
            array('src' => 'jquery.metisMenu.js'),
            array('src' => 'sb-admin.js'),
            array('src' => 'bootstrap-tour.js'),
            array('src' => 'bootstrap-tour.min.js'),
            array('src' => 'bootstrap-tour-standalone.min.js'),
            array('src' => 'bootstrap-tour-standalone.js'),
            array('src' => 'clinical_trials.js'),
        )));
/*
        <script src="<?php echo base_url(); ?>assets/scripts/jquery.metisMenu.js"></script>
        <script src="<?php echo base_url(); ?>assets/scripts/sb-admin.js"></script>
        <script src="<?php echo base_url(); ?>assets/scripts/bootstrap-tour.js"></script>
        <script src="<?php echo base_url(); ?>assets/scripts/bootstrap-tour.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/scripts/bootstrap-tour-standalone.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/scripts/bootstrap-tour-standalone.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/clinical_trials.js"></script>
 
 */
        break;
    case 'default_1':
        define('CSS_VENDORS_FILES', serialize(array(
            // bootstrap-v4
            array('href' => 'bootstrap-v4/css/bootstrap.css'),
            array('href' => 'bootstrap-v4/css/bootstrap-grid.css'),
            array('href' => 'bootstrap-v4/css/bootstrap-reboot.css'),
            // sb-admin-2
            array('href' => 'startbootstrap-sb-admin-2-gh-pages/css/sb-admin-2.css'),
            // bootstrap-tour-0.12
            array('href' => 'bootstrap-tour-0.12.0/css/bootstrap-tour-standalone.css'),
        )));
        define('CSS_FILES', serialize(array(
            array('href' => 'font-awesome.css'),
            // Page-Level Plugin CSS - Dashboard
            array('href' => 'morris-0.4.3.min.css'),
            array('href' => 'timeline.css'),
            // sb-admin
            array('href' => '960.css'),
            array('href' => 'style.css', 'id' => 'css'),
            array('href' => 'print.css', 'media' => 'print'),
            array('href' => '1.css'),
            array('href' => 'clinical_trials.css'),
        )));
        define('JS_VENDORS_FILES', serialize(array(
            // bootstrap-v4
            array('src' => 'bootstrap-v4/js/bootstrap.js'),
            // sb-admin-2
            array('src' => 'startbootstrap-sb-admin-2-gh-pages/js/sb-admin-2.js'),
            // bootstrap-tour-0.12
            array('src' => 'bootstrap-tour-0.12.0/js/bootstrap-tour.js'),
            array('src' => 'bootstrap-tour-0.12.0/js/bootstrap-tour-standalone.js'),
            // jquery 3.2.1
            array('src' => 'jquery/jquery-3.2.1.js'),
            // Google
            array('url' => 'http://maps.googleapis.com/maps/api/js?libraries=places&amp;sensor=false'),
        )));
        define('JS_FILES', serialize(array(
            array('src' => 'portal.js'),
            array('src' => 'barra.js', 'defer' => 'defer', 'async' => 'async', 'onload' => 'link_barra_brasil()'),
            array('src' => 'jquery.metisMenu.js'),
            array('src' => 'clinical_trials.js'),
        )));
        break;
}
/**
 * font-awesome-4.7.0/css/font-awesome.min.css
 * fontawesome-free-5.15.3-web/css/fontawesome.min.css
 */
define(FONT_AWESOME_CSS, 'font-awesome-4.7.0/css/font-awesome.min.css');
