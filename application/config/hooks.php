<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
/**
 * https://blog.sarav.co/integrate-laravel-eloquent-in-code-igniter/
 */
$hook['post_controller_constructor'][] = [
    'class'    => 'EloquentHook',
    'function' => 'bootEloquent',
    'filename' => 'EloquentHook.php',
    'filepath' => 'hooks'
];
//$hook['pre_controller'][] = [
//    'class'    => 'LanguageSwitcherHook',
//    'function' => 'preControllerCaller',
//    'filename' => 'LanguageSwitcherHook.php',
//    'filepath' => 'hooks'
//];
//$hook['post_controller'][] = [
//    'class'    => 'LanguageSwitcherHook',
//    'function' => 'postControllerCaller',
//    'filename' => 'LanguageSwitcherHook.php',
//    'filepath' => 'hooks'
//];
$hook['post_system'][] = [
    'class'    => 'RedisUpdateHook',
    'function' => 'postSystemCaller',
    'filename' => 'RedisUpdateHook.php',
    'filepath' => 'hooks'
];