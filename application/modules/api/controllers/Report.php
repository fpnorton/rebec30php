<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'modules/api/libraries/REST_Controller.php';
require APPPATH . 'modules/api/libraries/Format.php';

class Report extends REST_Controller   {

	protected $table="picolo_reports";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['ReportModel'=>'rModel','GenericModel'=>'post']);
		$this->load->helper(['string']);
	}

	function crud_post(){


		$post= $this->post();
		parse_str($post['form'], $form);
		if ($post['query']=='true') {
			$data['reports']= $this->rModel->getQuery(true,$form);
		}else{
			$data['reports']=$this->rModel->getReportCrud();
		}

	    $output['view']= $this->load->view('report',$data,true);
		
		$output['status']=  true;
		$output['post']= $form;
		$this->response($output, 200);
	}



	function chart_post(){

		$get= $this->post();

		$report= (isset($get['report'])) ? $get['report'] : null ;
		$this->post->setTable($this->table);
		$result=null;
		$chartInfo= null;

		$output= array();
		$output['status']= false;

		if ($report !=null) {
			$r= $this->post->as_object()->get($report);
			$output= array();
			//check if report has chart
			if ($r->is_chart =='T') {
			$chartInfo= $this->rModel->arrayunserialize($r->chart_info);

			//check report is prcedure or query
			// if procedure is true
			if ($r->is_procedure=='T') {
				$vars= $this->rModel->arrayunserialize($r->procedure_vars);

				if (is_array($vars)) {

					$count = count($vars); //count varables
					$data=[];

					//set up procedure call para
					$string = "?, ";
					$rep=  repeater($string, $count);
					$para= reduce_multiples($rep, ", ", TRUE);
					$parawithfn="(".$para.")";

					//map varables with the get value
					foreach ($vars as $key => $var) {
						$data[$var['name']]= $get[$var['name']];
					}
					//call of model function to get result
					$result= $this->rModel->callProcedure($r->procedure_name,$parawithfn,$data);
				}

			}
			// if query is true
				if ($r->is_query=='T') {
					$result= $this->rModel->getReport($get);
				}

		    //maping data with headers
		   $rHeader= $this->rModel->arrayunserialize($r->report_header);

		   $rkey= array_keys($result[0]);

		   //$newArray= array();

		   $newkey = array_combine($rkey, $rHeader);

		   $arraymap= array();

		   foreach ($result as $row) {
		   		$arraymap[]= $this->maparray( $row, $newkey);
		   }


		}

		// if ($chartInfo['type']=='line') {
		 	$nHeaderXasiz= $chartInfo['x_axis']-1;
		 	$nHeaderYasiz= $chartInfo['y_axis']-1;
		 	$nHeader= array();
		 	$h=0;
		 	foreach ($rHeader as $key => $value) {
		 		if ($key== $nHeaderXasiz) {
		 			$nHeader[$h++]= $value;
		 		}

		 		if ($key == $nHeaderYasiz) {
		 			$nHeader[$h++]= $value;
		 		}
		 	}

		 	$gNChart= $this->remapChartLineData($nHeader,$arraymap);

		// }

		  $gChart= $this->chartDate($this->rModel->arrayunserialize($r->report_header),$result);

		//var_dump($gChart);
			$output['status']= true;
			$output['report']['name']= $r->report_name ;
			$output['report']['header']= $rHeader;
			$output['report']['data']= $result;
			$output['report']['chart']= $chartInfo;
			$output['rkey']= $rkey;
			$output['newarray']= $newkey;
			$output['arraymap']= $arraymap;
			$output['gChart']= $gChart;
			$output['nHeader']= $nHeader;
			$output['chartInfo']= $chartInfo;
			$output['gNChart']= $gNChart;
			// pass value to view

		}

		$this->response($output, 200);
	}

	function maparray($array,$renameMap){
		$array = array_combine(array_map(function($el) use ($renameMap) {
				    return $renameMap[$el];
				}, array_keys($array)), array_values($array));
		return $array;
	}


	function chartDate($header,$data){

		$rows = array_chunk($header, count($header));
		$ab=array();

		if (is_array($data)) {
			foreach ($data as $key => $value) {
				$ab[]= $this->removeArrayKey($value);
			}
		}

		$merge = array_merge($rows, $ab);
		return $merge;
	}

	function removeArrayKey($value){
		$output= array();
		if (is_array($value)) {
			$i=0;
			foreach ($value as $key => $value) {
				$output[$i++]= $value;
			}
		}
		$new= array();
		foreach ($output as $z => $o) {
			//unset($o[0]);
			if ($z==0) {
				$new[$z] = $this->monthName($o);
			}else{
				$new[$z]=(int)$o;
			}
		}

		return $new;
	}

	function monthName($n){
		switch($n){
	    case "1":
	        return "Jan";
	        break;
	    case "2":
	        return "Feb";
	        break;
	    case "3":
	        return "March";
	        break;
	    case "4":
	        return "Apr";
	        break;
	    case "5":
	        return "May";
	        break;
	    case "6":
	        return "June";
	        break;
	    case "7":
	        return "July";
	        break;
	     case "8":
	        return "Aug";
	        break;
	    case "9":
	        return "Spet";
	        break;
	    case "10":
	        return "Oct";
	        break; 
	    case "11":
	        return "Nov";
	        break;
	    case "12":
	        return "Dec";
	        break;
	    default:
	        return $n;
	        break;
	    }

	}

	function remapChartLineData($info,$data){
		$rows = array_chunk($info, count($info));
		$iCount=count($info);

		$output=array();
		foreach ($data as $key => $value) {
				$output[]= $this->remapdatewithNheader($info,$value);
		}
		$merge = array_merge($rows, $output);
		return $merge;
		//return $output;
	}

	function remapdatewithNheader($info,$data){
		$output=array();
		$iCount=count($info);

		foreach ($data as $key => $value) {
			foreach ($info as $i => $iv) {
				if ($key== $iv) {
					$output[$iv]= $value;
				}
			}
		}

		$new = $this->removeArrayKey($output);

		return $new;
	}

}

/* End of file Report.php */
/* Location: ./application/modules/api/controllers/Report.php */