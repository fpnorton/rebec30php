<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'modules/api/libraries/REST_Controller.php';
require APPPATH . 'modules/api/libraries/Format.php';

class Web extends REST_Controller   {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['ApplicationModel'=>'appModel','ListModel'=>'listModel']);
	}

	function priority_get(){

		$get = $this->get();

		// $revisor_info = \Picolo\RevisorInfo::where('trial_id', '=', $get['trial_id'])->get()->first();

		\Picolo\RevisorInfo::where('trial_id', '=', $get['trial_id'])
			->where('priority', '=', ($get['direction'] == 1 ? 0 : 1))
			->update([
			'priority' => ($get['direction'] == 1 ? 1 : 0)
		]);

		$revisor_info = \Picolo\RevisorInfo::where('trial_id', '=', $get['trial_id'])->get()->first();

		$output['status']=true;
		$output['priority']=$revisor_info->priority;
		$this->response($output, 200);
	}

	function remark_get(){

		$get= $this->get();

		$output= array();
		$result= $this->appModel->getRemark($get['sub_id']);
		if (is_array($result)) {
			$output['status']=true;
			$output['data']= $result;
			$this->response($output, 200);
		}else{
			$output['status']=false;
			$this->response($output, 200);
		}
		
	}

	function next_reviewer_get(){

		$get = $this->get();

		$revisor_info = \Picolo\RevisorInfo::where('trial_id', '=', $get['trial_id'])->get()->first();

		$value = intval($revisor_info->reviewer);

		\Picolo\RevisorInfo::where('trial_id', '=', $get['trial_id'])
			->update([
				'reviewer' => (($value > 1) ? 0 : ($value + 1))
			]);

		$revisor_info = \Picolo\RevisorInfo::where('trial_id', '=', $get['trial_id'])->get()->first();

		$output['status']=true;
		$output['reviewer'] = $revisor_info->reviewer;
		$this->response($output, 200);
	}

	// function retornaj_get(){

	// 	$get= $this->get();

	// 	$output= array();
	// 	$result=null;
	// 	if ($get['tipo_de_relatorio']=='relatorio_de_pendings') {
	// 		$result= $this->appModel->relatorio_de_pendings();
	// 	}

	// 	//$result= $this->appModel->getRemark($get['tipo_de_relatorio']);
	// 	if (is_array($result)) {
	// 		$output['status']=true;
	// 		$output['data']= $result;
	// 		$output['estudos']= $this->appModel->getStaff();
	// 		$this->response($output, 200);
	// 	}else{
	// 		$output['status']=false;
	// 		$this->response($output, 401);
	// 	}
	// }

	function retornaj_post(){
		$post= $this->post();

        $page= $post['page'];

        //parse_str($post['form'], $filter);

        $this->load->library('pagination');

        $totalrecords= $this->listModel->getListRows($post);
        

        $config["base_url"] = base_url('api/retornaj/page/');
        $config['total_rows'] = $totalrecords;

        $config['page_query_string']=false;
        $config['reuse_query_string'] = TRUE;

        $config['num_links'] = 2;

        $config['per_page'] = 10;
        $config['use_page_numbers'] = TRUE;
        $choice = $config["total_rows"] / $config["per_page"];

        $config['num_tag_open'] = '<li class="page-item" >';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active page-item"><a href="javascript:;"  class="page-link" data-ci-pagination-page="'.$page.'" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['attributes'] = array('class' => 'page-link');

        $config['prev_link'] = '<i class="mdi mdi-chevron-left"></i>';
        $config['prev_tag_open'] = '<li class="page-item" >';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="mdi mdi-chevron-right"></i>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="page-item" >';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="page-item" >';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
 
        $data=$this->listModel->getList($config["per_page"], $page,$post);
       
        $str_links = $this->pagination->create_links($ajax=true);
        $links = explode('&nbsp;',$str_links );

        $output=array();
        if (is_array($data)) {
        	$output['status']=true;
	        $output['totalrecords']=$totalrecords;
	        $output['data']=$data;
	        $output['links']= $links;
	        $output['estudos']= $this->appModel->getStaff();
	        $this->response($output, 200);
        }else{
        	$output['status']=false;
        	$this->response($output, 200);	
        }
        
       // $output['url']=  $filter;
        
	}


	function gravarevisorname_get(){
		$output= array();

		$get= $this->get();
		$result= $this->appModel->updateRevisor_info($get);
		if (is_array($result)) {
			$output['persisted']=true;
			$output['revisor_name']= $result['user'];
			$this->response($output, 200);
		}else{
			$output['persisted']=false;
			$this->response($output, 400);
		}

	}

	function getEstudos_get(){
		$output['status']=true;
        $output['estudos']= $this->appModel->getStaff();
       // $output['url']=  $filter;
        $this->response($output, REST_Controller::HTTP_OK);
	}

}

/* End of file Web.php */
/* Location: ./application/modules/api/controllers/Web.php */

