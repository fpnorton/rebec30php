<div class="table-responsive">
             <table id="#reportTable" class="table table-bordered">
             	<thead>
             		<tr>
                  <th>Manage</th>
             			<th>Id</th>
             			<th>Report Name</th>
             			<th>Report Target</th>
             			<th>Report Header</th>
             			<th>Query</th>
             			<th>View	 </th>
             			<th>Procedure	</th>
             			<th>Chart</th>
                        <th>Table</th>
             			<th>Procedure Call</th>
             			<th>Procedure Vars</th>
             			<th>Chart Info</th>
             			<th>Active	</th>
             			
             		</tr>
             	</thead>
             	<tbody>
             		<?php if (is_array($reports)): ?>
             			<?php foreach ($reports as $report): ?>
             				<tr>

                        <td>
                                    
                        <div class="dropdown">
                          <button type="button" class="btn btn-dark btn-sm dropdown-toggle" id="dropdownMenuIconButton4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-menu"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton4" x-placement="bottom-start" >
                           
                            <a class="dropdown-item" href="<?= app_url('reportcrud/view/'.$report['id']) ?>">View</a>
                            <a class="dropdown-item" href="<?= app_url('reportcrud/edit/'.$report['id']) ?>">Edit</a>
                            <?php if ($report['isDelete']==0): ?>
                              <a class="dropdown-item" href="<?= app_url('reportcrud/delete/'.$report['id']) ?>">Delete</a>
                            <?php else: ?>
                               <a class="dropdown-item" href="<?= app_url('reportcrud/active/'.$report['id']) ?>">Active</a>
                            <?php endif ?>
                            
                          </div>
                        </div>


                                </td>

             					<td> <?= $report['id'] ?> </td>
             					<td style="word-wrap: break-word"> <?= ucfirst($report['report_name']) ?> </td>
             					<td> <?= $report['report_target'] ?> </td>
             					<td> 
                                    <?php if (is_array($report['report_header'])): ?>
                                        <?php foreach ($report['report_header'] as $head): ?>
                                            <div class="badge badge-info m-1">
                                                <?= ucfirst($head) ?>
                                            </div>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </td>
             					<td>
                        <?php if ($report['is_query']=='T'): ?>
                            <button type="button" class="btn btn-primary btn-rounded btn-icon">
                            <i class="mdi mdi-check"></i>
                          </button>
                          <?php else: ?>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon">
                            <i class="mdi mdi-close"></i>
                          </button>

                          <?php endif ?>
                                 
                                </td>
             					<td> 
                         <?php if ($report['is_view']=='T'): ?>
                            <button type="button" class="btn btn-primary btn-rounded btn-icon">
                            <i class="mdi mdi-check"></i>
                          </button>
                          <?php else: ?>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon">
                            <i class="mdi mdi-close"></i>
                          </button>

                          <?php endif ?>
                                   
                                </td>

                                <td> 
                         <?php if ($report['is_procedure']=='T'): ?>
                            <button type="button" class="btn btn-primary btn-rounded btn-icon">
                            <i class="mdi mdi-check"></i>
                          </button>
                          <?php else: ?>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon">
                            <i class="mdi mdi-close"></i>
                          </button>

                          <?php endif ?>
                                   
                                </td>


                                <td> 
                         <?php if ($report['is_chart']=='T'): ?>
                            <button type="button" class="btn btn-primary btn-rounded btn-icon">
                            <i class="mdi mdi-check"></i>
                          </button>
                          <?php else: ?>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon">
                            <i class="mdi mdi-close"></i>
                          </button>

                          <?php endif ?>
                                   
                                </td>

                                 <td> 
                         <?php if ($report['is_table']=='T'): ?>
                            <button type="button" class="btn btn-primary btn-rounded btn-icon">
                            <i class="mdi mdi-check"></i>
                          </button>
                          <?php else: ?>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon">
                            <i class="mdi mdi-close"></i>
                          </button>

                          <?php endif ?>
                                   
                                </td>

             					
             					<td> <?= $report['procedure_call'] ?> </td>
             					<td> 
                                    <?php if (is_array($report['procedure_vars'])): ?>
                                        <?php $i=1 ?>
                                        <?php foreach ($report['procedure_vars'] as $key=> $vars): ?>
                                        <div class="card card-inverse-success mb-1" id="context-menu-simple">
                                            <div class="card-body p-1">
                                              <p class="card-text">
                                                <?php foreach ($vars as $key=> $i): ?>
                                                <?= ucfirst($key) .":". ucfirst($i)  ?> <br>
                                            <?php endforeach ?>
                                               </p>
                                            </div>
                                          </div>
                                            
                                           
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </td>
             					<td> 

                                <?php if (is_array($report['chart_info'])): ?>
                                    <?php foreach ($report['chart_info'] as $key => $chart): ?>
                                        <div class="badge badge-info  m-1">
                                                <?= ucfirst($key) ." : ". ucfirst($chart) ?>
                                        </div>
                                    <?php endforeach ?>
                                <?php endif ?>

                                </td>

             					<td> 
                        <?php if ($report['isDelete']==1): ?>
                           <button type="button" class="btn btn-warning btn-rounded btn-icon">
                            <i class="mdi mdi-close"></i>
                          </button>
                          <?php else: ?>
                             <button type="button" class="btn btn-primary btn-rounded btn-icon">
                                <i class="mdi mdi-check"></i>
                             </button>
                          <?php endif ?>
                                   
                      </td>
             					
             					
             					
             				</tr>
             			<?php endforeach ?>
             		<?php endif ?>
             		
             	</tbody>
             </table>
            </div>