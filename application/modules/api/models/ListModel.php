<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListModel extends CI_Model {

	public function __construct(){
                parent::__construct();
                $this->config->load('python');
        }

	function _tablequery($query){


    $whereQuery=[
      'sub._deleted' => 0,
      'sub.status'=>'pending',
    ];
    $this->db->where($whereQuery);

    $search= trim($query['search']);
    if (!empty($search) && $search !=null) {
      $match= trim(strtolower($search));

      $likeArray = array(
        'user.username' => $match,
        'info.user_name'=> $match,
        'user.email' => $match,
        'sub.trial_id' => $match,
      );
      $this->db->or_like($likeArray);
    }

    if ($query['estudos'] !='null') {
      $this->db->where('info.user_id',$query['estudos']);
    }

		$q= $this->db->select('sub.id,sub.created,sub.trial_id,sub.status,sub.updated as updated_submission ,rep.trial_id as rbr,user.username as username ,user.email as email,info.user_name as revisor_name,info.priority,sub.title,info.reviewer')
			->from('reviewapp_submission as sub')
			->join('repository_clinicaltrial as rep','rep.id=sub.trial_id ','left')
			->join('auth_user as user','user.id=sub.creator_id','left')
			->join('picolo_revisor_info as info','info.trial_id=sub.trial_id','left')
			->Order_by('rep.id','asc');
	}



	function getListRows($post){
		$this->_tablequery($post);

		$q=$this->db->get();

		return $q->num_rows();
	}

	function getList($limit,$offset,$post){
		if ($offset > 0) {
            $offset = ($offset - 1) * $limit;
        }

    $this->_tablequery($post);
		//$this->db->limit($limit,$offset);
		$q=$this->db->get();

		if ($q->num_rows()>0) {
			$i=$offset+1;
			$staffList= $this->getStaff();
			foreach ($q->result_array() as $row) {
         		$id=$row['trial_id'];

         		$created_date= $row['created'];
                $daysAdd=  $this->config->item('days');

                $resubmission= '0';
                $dias_para_revisao ='null';

                $dias_para_ordenar = 0;

                $submited=  $this->subDate($id);

             		if (is_array($submited)) {
             			$subDate= $submited['date'];
                        $hoje= mdate('%Y-%m-%d'); //present date
                        // $date = "2019-05-01 ";
                        $expired= date('Y-m-d', strtotime($subDate. " + $daysAdd days"));

                        $date1 = new DateTime($hoje);
                        $date2 = new DateTime($expired);
                        $date3 = new DateTime($subDate);
                        $interval = $date1->diff($date2);
                        $_interval = $date1->diff($date3);

                        $resubmission= [
                            'resubmission_date'=> $subDate,
                             'limited_date'=>  $expired,
                        ];
                        $dias_para_revisao = (int)$interval->days * ($interval->invert ? -1 : 1);
                        $dias_para_ordenar = $_interval->days;
             			
             		}

                $revisor_name= $row['revisor_name'];

                if ($revisor_name ==null) {
                    $revisor_name = $staffList;
                }

           		$result[]= array(
           			'created'=> date('Y-m-d H:s:m', strtotime($created_date. " + $daysAdd days")),
           			'trial_id'=> $row['trial_id'],
           			'status'=> $row['status'],
           			'rbr'=> ($row['rbr']==null) ? 'None' : $row['rbr']  ,
           			'updated_submission'=> $row['updated_submission'],
           			'username'=> $row['username'],
           			'email'=> $row['email'],
           			'link_review'=> $this->config->item('link').$row['trial_id'],
           			'data_limite'=> $row['created'],
           			'submissionId'=> $row['id'],
           			'resubmission'=> $resubmission,
           			'dias_para_revisao'=> $dias_para_revisao,
                'dias_para_ordenar' => $dias_para_ordenar, // str_pad($dias_para_ordenar, 10, '0', STR_PAD_LEFT),
           			'revisor_name'=> $revisor_name,
                'priority'=> $row['priority'], // ($this->session->userdata('user')->username == $revisor_name ? $row['priority'] : -1),
                'title'=> base64_encode(htmlentities($row['title'])),
                'reviewer'=> $row['reviewer'],
           		);
         	}

			return $result;
		}

	}


	function getStaff(){
        $where=[
            'is_staff'=>1,
        ];
        $q= $this->db->select('id,username')->where($where)->get('auth_user');
        if ($q->num_rows()>0) {
            foreach ($q->result_array() as $row) {
                $result[]=array(
                    'name'=>$row['username'],'id'=>$row['id']
                );
            }

            return $result;
        }
    }

    function subDate($id){
    	//$sql="select submit.submit_date from picolo_submit as submit where  submit.trial_id=sub.trial_id limit 1";
    	$q= $this->db->where('trial_id',$id)->limit(1)->Order_by('submit_date','desc')->get('picolo_submit');
    	if ($q->num_rows()>0) {
    		foreach ($q->result_array() as $row) {
    			$result= array(
    				'date'=> $row['submit_date'],
    			);
    		}
    		return $result;
    	}else{
    		return null;
    	}
    }

}

/* End of file ListModel.php */
/* Location: ./application/modules/app/models/ListModel.php */