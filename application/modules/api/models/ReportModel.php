<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportModel extends CI_Model {

	protected $table="picolo_reports";

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['text']);
	}

	function getReportCrud(){
		$q= $this->db->get($this->table);
		if ($q->num_rows()>0) {
			foreach ($q->result_array() as $row) {

				$procedure_vars=null;
				$chart_info=null;
				if ($row['is_procedure']=='T') {
					$procedure_vars= $this->arrayunserialize($row['procedure_vars']);
				}

				if ($row['is_chart']=='T') {
					$chart_info= $this->arrayunserialize($row['chart_info']);
				}

				$result[]= array(
					'id'=> $row['id'],
					'report_name'=> $row['report_name'],
					'report_header'=> $this->arrayunserialize($row['report_header']) ,
					'is_query'=> $row['is_query'] ,
					'is_view'=> $row['is_view'],
					'is_procedure' => $row['is_procedure'],
					'report_target' => character_limiter($row['report_target'], 40),
					'procedure_vars' => $procedure_vars,
					'procedure_call'=> $row['procedure_call'] ,
					'is_table'=> $row['is_table'] ,
					'is_chart'=> $row['is_chart'] ,
					'chart_info'=> $chart_info ,
					'procedure_name'=> $row['procedure_name'],
					'isDelete'=>$row['isDelete']
				);
			}

			return $result;
		}
	}

	function getQuery($query=false,$form){
		$sql="SELECT * from $this->table";

		if ($query==true) {
			$sql= $sql." where ".$form['query'];
		}

		$q= $this->db->query($sql);
		if ($q->num_rows()>0) {
			foreach ($q->result_array() as $row) {

				$procedure_vars=null;
				$chart_info=null;
				if ($row['is_procedure']=='T') {
					$procedure_vars= $this->arrayunserialize($row['procedure_vars']);
				}

				if ($row['is_chart']=='T') {
					$chart_info= $this->arrayunserialize($row['chart_info']);
				}

				$result[]= array(
					'id'=> $row['id'],
					'report_name'=> $row['report_name'],
					'report_header'=> $this->arrayunserialize($row['report_header']) ,
					'is_query'=> $row['is_query'] ,
					'is_view'=> $row['is_view'],
					'is_procedure' => $row['is_procedure'],
					'report_target' => character_limiter($row['report_target'], 40),
					'procedure_vars' => $procedure_vars,
					'procedure_call'=> $row['procedure_call'] ,
					'is_table'=> $row['is_table'] ,
					'is_chart'=> $row['is_chart'] ,
					'chart_info'=> $chart_info ,
					'procedure_name'=> $row['procedure_name'],
					'isDelete'=>$row['isDelete']
				);
			}

			return $result;
		}
	}


	function getReport($get){
		$id= $get['report'];
		$q= $this->db->where('id',$id)->get($this->table);
		if ($q->num_rows()>0) {
			$result= $q->row();
			if ($result->is_query=='T') {
				$r= $this->db->query($result->report_target);
				if ($r->num_rows()>0) {
					$output= $r->result_array();
				}
			}
			return $output;
		}

	}


	function callProcedure($procedure,$para,$object){
		$call = "CALL ".$procedure.$para;
		$q = $this->db->query($call, $object);
		if ($q->num_rows()>0) {
			return $q->result_array();
		}

	}

	function arrayunserialize($string){
		return unserialize($string);
	}

	function arrayserialize($array){
		return serialize($array);
	}

}

/* End of file ReportModel.php */
/* Location: ./application/modules/api/models/ReportModel.php */