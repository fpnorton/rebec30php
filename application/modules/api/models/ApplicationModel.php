<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApplicationModel extends CI_Model {

        public function __construct()
        {
                parent::__construct();
                $this->config->load('python');
        }

	public function relatorio_de_pendings(){
		  $sql="SELECT sub.id,sub.created,sub.trial_id,sub.status,sub.updated as updated_submission ,rep.trial_id as rbr,user.username as username ,user.email as email,info.user_name as revisor_name,
                        (select submit.submit_date from picolo_submit as submit where  submit.trial_id=sub.trial_id limit 1 ) as submit_date 
                        from reviewapp_submission as sub 
                        left join repository_clinicaltrial as rep on rep.id=sub.trial_id 
                        left JOIN auth_user as user on user.id=sub.creator_id 
                        LEFT JOIN picolo_revisor_info as info on info.trial_id=sub.trial_id
                        where sub._deleted = 0 and sub.status = 'pending' order by rep.id asc";

                $q= $this->db->query($sql);
                if ($q->num_rows()>0) {
                
                $staffList= $this->getStaff();

                foreach ($q->result_array() as $row) {
         		     $id=$row['trial_id'];

         		    $created_date= $row['created'];
                $daysAdd=  $this->config->item('days');

                $resubmission= '0';
                $dias_para_revisao ='null';

                $submited= $row['submit_date'];

             		if ($submited !=null) {
                        $hoje= mdate('%Y-%m-%d'); //present date
                        // $date = "2019-05-01 ";
                        $expired= date('Y-m-d', strtotime($submited. " + $daysAdd days"));

                        $date1 = new DateTime($hoje);
                        $date2 = new DateTime($expired);
                        $interval = $date1->diff($date2);

                        $resubmission= [
                            'resubmission_date'=> $row['submit_date'],
                             'limited_date'=>  $expired,
                        ];
                        $dias_para_revisao = $interval->days * ($interval->invert ? -1 : 1);
             			
             		}

                $revisor_name= $row['revisor_name'];

                if ($revisor_name ==null) {
                    $revisor_name = $staffList;
                }

           		$result[]= array(
           			'created'=> date('Y-m-d H:s:m', strtotime($created_date. " + $daysAdd days")),
           			'trial_id'=> $row['trial_id'],
           			'status'=> $row['status'],
           			'rbr'=> ($row['rbr']==null) ? 'None' : $row['rbr']  ,
           			'updated_submission'=> $row['updated_submission'],
           			'username'=> $row['username'],
           			'email'=> $row['email'],
           			'link_review'=> $this->config->item('link').$row['trial_id'],
           			'data_limite'=> $row['created'],
           			'submissionId'=> $row['id'],
           			'resubmission'=> $resubmission,
           			'dias_para_revisao'=> $dias_para_revisao,
           			'revisor_name'=> $revisor_name,
           		);
         	}

         	return $result;

         }
	}


    function updateRevisor_info($get){

        $trial_id= $get['trial_id'];
        $revisor_id= $get['revisor'];
        $revisor_table=  $this->getUser($revisor_id);
        if (is_array($revisor_table)) {
          $q= $this->db->where('trial_id',$trial_id)->get('picolo_revisor_info');

            if ($q->num_rows()>0) {

                $dataupdate=[
                    'user_name'=>  $revisor_table['username'],
                    'user_id'=> $revisor_table['id'],
                ];
                $z= $this->db->where('trial_id',$trial_id)->update('picolo_revisor_info',$dataupdate);

                $output=array(
                    'status'=> $z,
                    'user'=> $revisor_table['username'],
                );

                if ($z) {
                    return $output;
                }else{
                    return false;
                }
                
            }else{
                $datainsert=[
                'user_name'=>  $revisor_table['username'],
                'user_id'=>$revisor_table['id'],
                'trial_id'=> $trial_id
                ];

                $z = $this->db->insert('picolo_revisor_info',$datainsert);
                $output=array(
                    'status'=> $z,
                    'user'=> $revisor_table['username'],
                );

                if ($z) {
                    return $output;
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }


    }


    function getUser($id){
        $q= $this->db->where(['id'=>$id,'is_staff'=>1])->get('auth_user');
        if ($q->num_rows()>0) {
            $x= $q->result_array();
            return array_pop($x);
        }
    }

    function getStaff(){
        $where=[
            'is_staff'=>1,
        ];
        $q= $this->db->select('id,username')->where($where)->get('auth_user');
        if ($q->num_rows()>0) {
            foreach ($q->result_array() as $row) {
                $result[]=array(
                    'name'=>$row['username'],'id'=>$row['id']
                );
            }

            return $result;
        }
    }

    function getRemark($id){
     
        $q= $this->db->select('rr.id,au.username,rr.created,rr.context,rr.text')
                ->from('reviewapp_remark as rr')
                ->join('auth_user as au','au.id=rr.creator_id','left')
                ->where('rr.submission_id',$id)->get();

        if ($q->num_rows()>0) {
             foreach ($q->result_array() as $row) {
                $result[]=array(
                    'revisor'=> $row['username'],
                    'date'=> nice_date($row['created'],'Y-m-d'),
                    'context'=> $row['context'],
                    'text'=>$row['text'],

                );
             }

             return $result;
        }

    }

}

/* End of file ApplicationModel.php */
/* Location: ./application/modules/app/models/ApplicationModel.php */