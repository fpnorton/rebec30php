<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sendemail {
	public $ci;
	public $data;
	
	public function __construct() {
		$this->ci =& get_instance();
		$this->ci->load->library('Email');
		$this->ci->config->load('email_sender');
	}
/*

	function property($post,$template){
		$to_email=$post['email'];
		$sender_email= $this->ci->config->item('smtp_user');
		$sender_name="Budget Homes In Dubai";
		$sender_subject="Budget Homes In Dubai";
		$config = array(
			'smtp_host' => $this->ci->config->item('smtp_host'),
			'smtp_port' => $this->ci->config->item('smtp_port'),
			'smtp_user' => $this->ci->config->item('smtp_user'),
			'smtp_pass' => $this->ci->config->item('smtp_pass'),
			'smtp_crypto'=> $this->ci->config->item('smtp_crypto'),
		);
		$this->ci->email->initialize($config);
		$this->ci->email->set_newline("\r\n");

		$data['post'] = $post;

		//$fullpath= APPPATH . 'modules/email/views/';
		//$c['content'] = $this->ci->load->view("emails/", $data, true);
		$data['title']=$sender_subject;
		$msg = $this->ci->load->view(trim($template), $data, true);

		$download= web_url('pdf/').$post['pdfName'];

		$this->ci->email->attach($download);

		$this->ci->email->message($msg);
		$this->ci->email->subject($sender_subject);
		$this->ci->email->to($to_email);
		$this->ci->email->from($sender_email, $sender_name);
		$this->ci->email->reply_to($sender_email, $sender_name);
		
		if($this->ci->email->send()){
			return true;
		}else{
			return false;
			//throw new Exception($this->ci->email->print_debugger());
		}
	}

	function propertyAdmin($post){

		$to_email=$this->ci->config->item('smtp_user');
		$sender_email= $this->ci->config->item('smtp_user');
		$sender_name="Budget Homes In Dubai";
		$sender_subject="Budget Homes In Dubai";
		$config = array(
			'smtp_host' => $this->ci->config->item('smtp_host'),
			'smtp_port' => $this->ci->config->item('smtp_port'),
			'smtp_user' => $this->ci->config->item('smtp_user'),
			'smtp_pass' => $this->ci->config->item('smtp_pass'),
			'smtp_crypto'=> $this->ci->config->item('smtp_crypto'),
		);
		$this->ci->email->initialize($config);
		$this->ci->email->set_newline("\r\n");

		
		//$this->ci->email->attach($download);
		$msg="Name : ".$post['name']." <br> Email: ".$post['email']." <br> Contact Number: ".$post['phone']."<br> City: ".$post['city']." <br> Comment: ".$post['comments']." <br> Property : ".$post['propertyName']."  ";

		$this->ci->email->message($msg);
		$this->ci->email->subject($sender_subject);
		$this->ci->email->to($to_email);
		$this->ci->email->from($sender_email, $sender_name);
		$this->ci->email->reply_to($post['email'], $post['name']);
		
		if($this->ci->email->send()){
			return true;
		}else{
			return false;
			//throw new Exception($this->ci->email->print_debugger());
		}
	}



// */
	
}