<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as DB;
class Search extends MetisController {

    private $redis;

	public function __construct()
	{
		parent::__construct();
        $this->load->library('RedisFunctions');
		$this->load->helper('date');
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
        $this->redis = new Redis();
        $this->redis->connect('redis', 6379);
        $this->redis->auth('rebec');
    }

    private function crop_query($query) 
    {
        $return = [];
        $crop = [
            'i' => 0,
            'e' => 0,
            'l' => 0,
        ];
        $level = 0;
        if (strpos($query , '(') > 0) {
            $return[] = substr($query, 0, strpos($query , '('));
        }
        for ($i = strpos($query , '('); $i < strlen($query); $i++) { 
            if ($query[$i] == '(') { $level++; $crop['i'] = $i + 1; }
            if ($query[$i] == ')') { $level--; $crop['e'] = $i - 1; }
            if ($level != 0) break;
        }
        $return[] = substr($query, 
            $crop['i'], 
            ($crop['e'] - $crop['i'] + 1)
        );
        $return['crop'] = $crop;
        return $return;
    }

    private function extract_instructions($query) 
    {
        // error_log(json_encode([
        //     'function' => 'metis->search->extract_instructions',
        //     '$query' => $query,
        //     'utf8_decode($query)' => utf8_decode($query),
        // ]));
        $instructions = [];
        $matches = NULL;
        preg_match_all('/"([^"]+)"|[\.|\w|\$|\-]+(\s+|\$+)?/u', $query, $matches);
        foreach($matches[0] as $key => $value) $instructions[] = trim($value);
        error_log(json_encode([
            'function' => 'metis->search->extract_instructions',
            '$instructions' => $instructions,
        ]));
        return $instructions;
    }

    public function simple_query()
    {
        $linguagem_selecionada = linguagem_selecionada();
        $word = "";
        // $words = [];
        $data = [];
        $search_results = [];
        $working_hour = [];
        $search_pack = [

        ];
        {
            if ($this->input->post('q')) :
                $word = $this->input->post('q');
            elseif ($this->input->post('terms')) :
                $word = $this->input->post('terms');
            endif;
            // $words = explode(' ', utf8_decode($word));
        }
        $set_number = 0;
        $instructions = $this->extract_instructions($word);
        $search_results = NULL;
        foreach($instructions as $instruction) {
            if (strtolower($instruction) == 'or') $set_number++;
            // if (in_array(strtolower($instruction), ['and', 'or', 'not', 'of', 'in'])) continue;
            if (strlen($instruction) < 4) continue;
            $clinical_trial_id = 0;
            $clinical_trial_key = NULL;
            $only_digits = preg_replace("/[^0-9]/", "", $instruction);
            if (strpos(strtolower($instruction), 'rbr-') !== FALSE) {
                $clinical_trial_id = \Repository\ClinicalTrial::where('trial_id', '=', $instruction)->first()->id; 
                $clinical_trial_key = 'rbr';
            } elseif (strlen($only_digits) == 12 and strpos(strtolower($instruction), 'u') === 0) {
                $utn = 'U'.implode('%', [
                    substr($only_digits,  0, 4),
                    substr($only_digits,  4, 4),
                    substr($only_digits,  8, 4),
                ]);
                $clinical_trial_id = \Repository\TrialNumber::where('id_number', 'like', $utn)->first()->trial_id; 
                $clinical_trial_key = 'utn';
            } elseif (strlen($only_digits) == 17) {
                $caae = '%'.implode('%', [
                    substr($only_digits,  0, 8),
                    substr($only_digits,  8, 1),
                    substr($only_digits,  9, 4),
                    substr($only_digits, 13),
                ]).'%';
                $clinical_trial_id = \Repository\TrialNumber::where('id_number', 'like', $caae)->first()->trial_id; 
                $clinical_trial_key = 'caae';
                // echo '<pre>'.var_export([
                //     '$instruction' => $instruction,
                //     '$only_digits' => ($only_digits),
                //     '$caae' => ($caae),
                //     '$clinical_trial_id' => ($clinical_trial_id),
                // ], TRUE).'</pre>';
            }
            //
            if ($clinical_trial_id == -1) continue;
            if ($clinical_trial_id > 0) {
                $fosseis = \Fossil\Fossil::select("id")->where('is_most_recent', '=', 1)->where('object_id', '=', $clinical_trial_id)->get();
                // echo '<pre>'.var_export([
                //     'function' => 'metis->search->simple_query',
                //     '$fosseis' => ($fosseis),
                // ], TRUE).'</pre>';
                foreach($fosseis as $object) {
                    $results = [
                        $object->id => [$clinical_trial_key]
                    ];
                    // echo '<pre>'.var_export([
                    //     'function' => 'metis->search->extract_instructions [1]',
                    //     '$instruction' => $instruction,
                    //     '$results' => ($results),
                    // ], TRUE).'</pre>';
                    $search_results[$set_number][$instruction] = array_merge(
                        $search_results[$set_number][$instruction] ?? [],
                        $results
                    );
                }
            } else {
                $search_results[$set_number][$instruction] = [];
                foreach(array_keys(\Helper\Language::$available) as $linguagem_selecionada) {
                    $results = $this->get_metaphone_results($linguagem_selecionada, $instruction);
                    // echo '<pre>'.var_export([
                    //     'function' => 'metis->search->extract_instructions [1]',
                    //     '$linguagem_selecionada' => $linguagem_selecionada,
                    //     '$instruction' => $instruction,
                    //     '$results' => ($results),
                    // ], TRUE).'</pre>';
                    $search_results[$set_number][$instruction] = array_merge(
                        $search_results[$set_number][$instruction] ?? [],
                        $results
                    );
                }
            }
            // echo '<pre>'.var_export([
            //     'function' => 'metis->search->extract_instructions [1]',
            //     '$set_number' => $set_number,
            //     '$instruction' => $instruction,
            //     '$search_results[$set_number][$instruction]' => ($search_results[$set_number][$instruction]),
            // ], TRUE).'</pre>';
        }
        // echo '<pre>'.var_export([
        //     'function' => 'metis->search->extract_instructions [2]',
        //     '$search_results' => ($search_results),
        // ], TRUE).'</pre>';
        $results = [];
        foreach($search_results as $set_number => $set_results) {
            $fossil_ids = [];
            $index = 0;
            foreach($set_results as $instruction => $instruction_results) {
                // echo '<pre>'.var_export([
                //     'function' => 'metis->search->extract_instructions [1]',
                //     '$instruction' => $instruction,
                //     '$instruction_results->keys' => array_keys($instruction_results),
                //     '$fossil_ids' => ($fossil_ids),
                // ], TRUE).'</pre>';
                if ($index > 0 and count($fossil_ids) == 0) {
                    break;
                } elseif ($index++ == 0 and count($fossil_ids) == 0) {
                    $fossil_ids = array_keys($instruction_results);
                    continue;
                }
                $fossil_ids = array_intersect($fossil_ids, array_keys($instruction_results));
            }
            foreach($set_results as $instruction => $instruction_results) {
                foreach($fossil_ids as $key => $fossil_id) {
                    if (array_key_exists($fossil_id, $instruction_results)) {
                        $results[$fossil_id] = array_merge($results[$fossil_id] ?? [], $instruction_results[$fossil_id]);
                    }
                }
            }
        }
        // echo '<pre>'.var_export([
        //     'function' => 'metis->search->extract_instructions [3]',
        //     '$results' => ($results),
        // ], TRUE).'</pre>';
        $data_results = [];
        foreach($results as $fossil_id => $search_result) {

            // echo '<pre>'.var_export([
            //     __FILE__=>__LINE__, 
            //     'count($search_results)' => count($search_results),
            //     '$fossil_id' => $fossil_id,
            //     '$search_result' => $search_result,
            //     // 'DB::getQueryLog()' => DB::getQueryLog(),
            // ], TRUE).'</pre>';

            $fossil = \Fossil\Fossil::where('id', '=', $fossil_id)->first();

            if (empty($fossil)) continue;

            foreach($instructions as $instruction) {
                // if (in_array(strtolower($instruction), ['and', 'or', 'not', 'of', 'in'])) continue;
                if (strlen($instruction) < 4) continue;
                if ($instruction) {
                    $unserialized = json_decode($fossil->serialized, TRUE);
                    foreach($search_result as $field) {
                        $conteudo_fossil_invalido = in_array($field, [
                            'rbr', 'caae', 'utn'
                        ]) ? FALSE : $this->conteudo_fossil_invalido($instruction, $field, $unserialized);
                        // echo '<pre>'.var_export([
                        //     __FILE__=>__LINE__, 
                        //     'params' => [$instruction, $field, '$unserialized'],
                        //     '$conteudo_fossil_invalido' => $conteudo_fossil_invalido,
                        // ], TRUE).'</pre>';
                        if ($conteudo_fossil_invalido) continue;
                        $data_results[$fossil_id]['is_most_recent'] = ($fossil->is_most_recent == 1);
                        if (@$data_results[$fossil_id]['language'] == NULL)
                            $data_results[$fossil_id]['language'] = $linguagem_selecionada;
                        if (array_key_exists($field, $data_results[$fossil_id]['words'][$instruction]) == false)
                            $data_results[$fossil_id]['words'][$instruction][] = $field;
                        if (@$data_results[$fossil_id]['unserialized'] == NULL)
                            $data_results[$fossil_id]['unserialized'] = $unserialized;
                        if (@$data_results[$fossil_id]['object_id'] == NULL)
                            $data_results[$fossil_id]['object_id'] = $fossil->object_id;
                    }
                } else {
                    foreach($search_result as $field) {
                        $data_results[$fossil_id]['is_most_recent'] = ($fossil->is_most_recent == 1);
                        if (@$data_results[$fossil_id]['language'] == NULL)
                            $data_results[$fossil_id]['language'] = $linguagem_selecionada;
                        if (@$data_results[$fossil_id]['unserialized'] == NULL)
                            $data_results[$fossil_id]['unserialized'] = $unserialized;
                        if (@$data_results[$fossil_id]['object_id'] == NULL)
                            $data_results[$fossil_id]['object_id'] = $fossil->object_id;
                    }
                }
            }
        }
        // echo '<pre>'.var_export([
        //     __FILE__=>__LINE__, 
        //     'count($search_results)' => count($data_results),
        //     '$data_results' => $data_results,
        // ], TRUE).'</pre>';
        {
            error_log(json_encode([
                '('.$_SERVER['REQUEST_URI'].')',
                (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
                'count($data_results)' => count($data_results),
                '$instructions' => $instructions,
            ]));
        }
        {
            $data['content'] = $this->load->view('search_list.php', [
                'results' => $data_results,
                'tag' => $this->tags(),
                'previous' => $this->input->post(),
            ], TRUE);
            $this->parser->parse('main', array_merge($data, [
                'usuario' => $this->session->userdata('user'),
            ]));
        }
    }

    private function conteudo_fossil($field, $fossil_unserialized, $linguagem_selecionada = 'en') {
        $keys = array_keys($fossil_unserialized);
        // echo '<pre>'.var_export([__FILE__=>__LINE__, '$keys' => $keys], TRUE).'</pre>';
        if (in_array($field, $keys) and is_array($fossil_unserialized[$field])) {
            $array_field = $fossil_unserialized[$field];
            // echo '<pre>'.var_export([__FILE__=>__LINE__,$array_field], TRUE).'</pre>';
            if (in_array('label', $array_field)) {
                // echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $array_field['label']], TRUE).'</pre>';
                if ($linguagem_selecionada == 'en') return $array_field['label'];
                foreach ($array_field['translations'] as $translation) {
                    // echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $translation['label']], TRUE).'</pre>';
                    if ($linguagem_selecionada == $translation['language']) return $translation['label'];
                }
            } elseif (in_array('text', $array_field)) {
                // echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $array_field['text']], TRUE).'</pre>';
                if ($linguagem_selecionada == 'en') return $array_field['text'];
                foreach ($array_field['translations'] as $translation) {
                    // echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $translation['text']], TRUE).'</pre>';
                    if ($linguagem_selecionada == $translation['language']) return $translation['text'];
                }
            } elseif (in_array('description', $array_field)) {
                // echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $array_field['description']], TRUE).'</pre>';
                if ($linguagem_selecionada == 'en') return $array_field['description'];
                foreach ($array_field['translations'] as $translation) {
                    // echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $translation['text']], TRUE).'</pre>';
                    if ($linguagem_selecionada == $translation['language']) return $translation['description'];
                }
            }
        } elseif (in_array($field, $keys)) {
            if ($linguagem_selecionada == 'en') return $fossil_unserialized[$field];
            foreach ($fossil_unserialized['translations'] as $translation) {
                // echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $translation[$field]], TRUE).'</pre>';
                if ($linguagem_selecionada == $translation['language']) return $translation[$field];
            }
        }
    }

    private function conteudo_fossil_invalido($word, $field, $fossil_unserialized) {
        $linguagens = array_keys(\Helper\Language::$available);
        $counter = 0;
        foreach($linguagens as $linguagem_selecionada) {
            $text = $this->conteudo_fossil($field, $fossil_unserialized, $linguagem_selecionada);
            if (stripos($text, $word) == false) $counter++;
        }
        // echo '<pre>'.var_export([__FILE__=>__LINE__, '$word' => $word, '$field' => $field, '$fossil_unserialized' => $fossil_unserialized], TRUE).'</pre>';
        // echo '<pre>'.var_export([__FILE__=>__LINE__, '$text' => $text], TRUE).'</pre>';
        return ($counter === count($linguagens)) ? TRUE : FALSE;
    }

    private function get_metaphone_results($language, $query) {
        $results = [];
        foreach ($this->redisfunctions->metaphone_words($query) as $word => $metaphone) {
            $metaphone_results = $this->redis->hKeys($language .":".$metaphone);
            foreach ($metaphone_results as $metaphone_result) {
                $parts = explode(':', $metaphone_result);
                $results[ $parts[0] ][] = $parts[1];
            }
        }
        return $results;
    }

    public function query()
    {
        DB::enableQueryLog();
        $linguagem_selecionada = linguagem_selecionada();
        $word = "";
        $words = [];
        $data = [];
        $search_results = [];
        {
            if ($this->input->post('q')) :
                $word = $this->input->post('q');
            elseif ($this->input->post('terms')) :
                $word = $this->input->post('terms');
            endif;
            $words = explode(' ', $word);
            if (!empty($word)) $search_results = $this->get_metaphone_results($linguagem_selecionada, $word);
        }
        {
            $key = 'rbr';
            $rbr = NULL;
            foreach($words as $single_word) {
                if (strpos(strtolower($single_word), 'rbr') === FALSE) continue;
                $clinicalTrial = \Repository\ClinicalTrial::where('trial_id', '=', $single_word)->first(); 
                $fosseis = \Fossil\Fossil::select("id")->where('is_most_recent', '=', 1)->where('object_id', '=', $clinicalTrial->id)->get();
                foreach($fosseis as $object) {
                    $search_results[$object->id][] = $key;
                }
            }
        }
        {
            if (!empty($this->input->post('country'))) {
                $label = strtolower($this->input->post('country'));
                $search_advanced_value = "json_contains(lower(serialized), '{\"label\": \"$label\"}', '$.recruitment_country') > 0";
                $fosseis = \Fossil\Fossil::select("id")->where('is_most_recent', '=', 1)->whereRaw($search_advanced_value)->get();
                {
                    $key = 'recruitment_country';
                    $search_results_before = $search_results;
                    $search_results = [];
                    foreach($fosseis as $object) {
                        if (empty($search_results_before) or $search_results_before[$object->id]) {
                            $search_results[$object->id]   = $search_results_before[$object->id];
                            $search_results[$object->id][] = $key;
                        }
                    }
    
                }
            }
            if (!empty($this->input->post('study-type'))) {
                $label = strtolower($this->input->post('study-type'));
                $search_advanced_value = "json_contains(lower(serialized), '{\"label\": \"$label\"}', '$.study_type') > 0";
                $fosseis = \Fossil\Fossil::select("id")->where('is_most_recent', '=', 1)->whereRaw($search_advanced_value)->get();
                {
                    $key = 'study_type';
                    $search_results_before = $search_results;
                    $search_results = [];
                    foreach($fosseis as $object) {
                        if (empty($search_results_before) or $search_results_before[$object->id]) {
                            $search_results[$object->id]   = $search_results_before[$object->id];
                            $search_results[$object->id][] = $key;
                        }
                    }
                }
            }
            if (!empty($this->input->post('gender'))) {
                $label = strtolower($this->input->post('gender'));
                $search_advanced_value = "json_contains(lower(serialized), '{\"gender\": \"$label\"}') > 0";
                $fosseis = \Fossil\Fossil::select("id")->where('is_most_recent', '=', 1)->whereRaw($search_advanced_value)->get();
                {
                    $key = 'gender';
                    $search_results_before = $search_results;
                    $search_results = [];
                    foreach($fosseis as $object) {
                        if (empty($search_results_before) or $search_results_before[$object->id]) {
                            $search_results[$object->id]   = $search_results_before[$object->id];
                            $search_results[$object->id][] = $key;
                        }
                    }
                }
            }
            if (!empty($this->input->post('recruitment-status'))) {
                $label = strtolower($this->input->post('recruitment-status'));
                $search_advanced_value = "json_contains(lower(serialized), '{\"label\": \"$label\"}', '$.recruitment_status') > 0";
                $fosseis = \Fossil\Fossil::select("id")->where('is_most_recent', '=', 1)->whereRaw($search_advanced_value)->get();
                {
                    $key = 'recruitment_status';
                    $search_results_before = $search_results;
                    $search_results = [];
                    foreach($fosseis as $object) {
                        if (empty($search_results_before) or $search_results_before[$object->id]) {
                            $search_results[$object->id]   = $search_results_before[$object->id];
                            $search_results[$object->id][] = $key;
                        }
                    }
                }
            }
            if (!empty($this->input->post('institution-type'))) {
                // $label = $this->input->post('institution-type');
                $label = strtolower($this->input->post('institution-type'));
                $search_advanced_value = implode(' OR ', [
                    "json_contains(lower(serialized), '{\"institution\" : {\"i_type\" : {\"label\": \"$label\"}}}', '$.support_sources') > 0",
                    "json_contains(lower(serialized), '{\"institution\" : {\"i_type\" : {\"label\": \"$label\"}}}', '$.secondary_sponsors') > 0",
                    // "json_contains(lower(serialized), '{\"institution\" : {\"i_type\" : {\"label\": \"$label\"}}}', '$.site_contact') > 0",
                ])                    
                ;
                $fosseis = \Fossil\Fossil::select("id")->where('is_most_recent', '=', 1)->whereRaw($search_advanced_value)->get();
                {
                    $key = 'institution_type';
                    $search_results_before = $search_results;
                    $search_results = [];
                    foreach($fosseis as $object) {
                        if (empty($search_results_before) or $search_results_before[$object->id]) {
                            $search_results[$object->id]   = $search_results_before[$object->id];
                            $search_results[$object->id][] = $key;
                        }
                    }
                }
            }
            $min_age_value = $this->input->post('min-age-value') ? 
                preg_replace("/[^0-9]/", "", $this->input->post('min-age-value')) : 
                NULL;
            $min_age_unit  = substr($this->input->post('min-age-unit'), 0, 1);
            if (!empty($min_age_value) and !empty($min_age_unit)) {
                $value = $min_age_value;
                $unit  = strtolower($min_age_unit);
                $search_advanced_value = 
                    "(json_contains(lower(serialized), '$value', '$.agemin_value') > 0)" .
                    " and ".
                    "(json_contains(lower(serialized), '\"$unit\"', '$.agemin_unit') > 0)"
                ;
                $fosseis = \Fossil\Fossil::select("id")->where('is_most_recent', '=', 1)->whereRaw($search_advanced_value)->get();
                {
                    $key = 'age-min';
                    $search_results_before = $search_results;
                    $search_results = [];
                    foreach($fosseis as $object) {
                        if (empty($search_results_before) or $search_results_before[$object->id]) {
                            $search_results[$object->id]   = $search_results_before[$object->id];
                            $search_results[$object->id][] = $key;
                        }
                    }
                }
            }
            $max_age_value = $this->input->post('max-age-value') ? 
                preg_replace("/[^0-9]/", "", $this->input->post('max-age-value')) : 
                NULL;
            $max_age_unit  = substr($this->input->post('max-age-unit'), 0, 1);
            if (!empty($max_age_value) and !empty($max_age_unit)) {
                $value = $max_age_value;
                $unit  = strtolower($max_age_unit);
                $search_advanced_value = 
                    "(json_contains(lower(serialized), '$value', '$.agemax_value') > 0)" .
                    " and ".
                    "(json_contains(lower(serialized), '\"$unit\"', '$.agemax_unit') > 0)"
                ;
                $fosseis = \Fossil\Fossil::select("id")->where('is_most_recent', '=', 1)->whereRaw($search_advanced_value)->get();
                // echo '<pre>'.var_export([
                //     __FILE__=>__LINE__, 
                //     'count($fosseis)' => count($fosseis),
                // ], TRUE).'</pre>';
                {
                    $key = 'age-max';
                    $search_results_before = $search_results;
                    $search_results = [];
                    foreach($fosseis as $object) {
                        if (empty($search_results_before) or $search_results_before[$object->id]) {
                            $search_results[$object->id]   = $search_results_before[$object->id];
                            $search_results[$object->id][] = $key;
                        }
                    }
                }
            }
        }
        // echo '<pre>'.var_export([
        //     __FILE__=>__LINE__, 
        //     'count($search_results)' => count($search_results),
        //     '$fossil_id' => $fossil_id,
        //     '$search_result' => $search_result,
        //     'DB::getQueryLog()' => DB::getQueryLog(),
        // ], TRUE).'</pre>';
        foreach($search_results as $fossil_id => $search_result) {

            // echo '<pre>'.var_export([
            //     __FILE__=>__LINE__, 
            //     'count($search_results)' => count($search_results),
            //     '$fossil_id' => $fossil_id,
            //     '$search_result' => $search_result,
            //     // 'DB::getQueryLog()' => DB::getQueryLog(),
            // ], TRUE).'</pre>';

            $fossil = \Fossil\Fossil::where('id', '=', $fossil_id)->first();

            if (empty($fossil)) continue;

            if ($word) {
                $unserialized = json_decode($fossil->serialized, TRUE);
                foreach($search_result as $field) {
                    $conteudo_fossil_invalido = $this->conteudo_fossil_invalido($word, $field, $unserialized);
                    if ($field == 'rbr') $conteudo_fossil_invalido = FALSE;
                    if ($field == 'caae') $conteudo_fossil_invalido = FALSE;
                    if ($field == 'utn') $conteudo_fossil_invalido = FALSE;
                    // echo '<pre>'.var_export([
                    //     __FILE__=>__LINE__, 
                    //     'params' => [$word, $field, '$unserialized'],
                    //     '$conteudo_fossil_invalido' => $conteudo_fossil_invalido,
                    // ], TRUE).'</pre>';
                    if ($conteudo_fossil_invalido) continue;
                    $results[$fossil_id]['is_most_recent'] = ($fossil->is_most_recent == 1);
                    if (@$results[$fossil_id]['language'] == NULL)
                        $results[$fossil_id]['language'] = $linguagem_selecionada;
                    if (array_key_exists($field, $results[$fossil_id]['words'][$word]) == false)
                        $results[$fossil_id]['words'][$word][] = $field;
                    if (@$results[$fossil_id]['unserialized'] == NULL)
                        $results[$fossil_id]['unserialized'] = $unserialized;
                    if (@$results[$fossil_id]['object_id'] == NULL)
                        $results[$fossil_id]['object_id'] = $fossil->object_id;
                }
            } else {
                foreach($search_result as $field) {
                    $results[$fossil_id]['is_most_recent'] = ($fossil->is_most_recent == 1);
                    if (@$results[$fossil_id]['language'] == NULL)
                        $results[$fossil_id]['language'] = $linguagem_selecionada;
                    if (@$results[$fossil_id]['unserialized'] == NULL)
                        $results[$fossil_id]['unserialized'] = $unserialized;
                    if (@$results[$fossil_id]['object_id'] == NULL)
                        $results[$fossil_id]['object_id'] = $fossil->object_id;
                }
            }
        }
        {
            $data['content'] = $this->load->view('search_list.php', [
                'results' => $results,
                'tag' => $this->tags(),
                'previous' => $this->input->post(),
            ], TRUE);
        }

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    private function tags() {
        $fieldlabel = new Helper\FieldLabelLanguage('RecruitmentForm'); 
        $fieldlabel->enableUpdate();
        $linguagem_selecionada = linguagem_selecionada();

        $data = [];
        {
            $key = 'countries';
            $data[$key][''] = '';
            foreach (\Vocabulary\CountryCode::orderBy('id')->get() as $o) {
                if ($linguagem_selecionada == 'en') {
                    $t = $o;
                } else {
                    $t = $o->translations()->where('language', '=', $linguagem_selecionada)->first();
                }
                // $data[$key][$t->id] = $t->description;
                $data[$key][$o->label] = $t->description;
            }
        }
        {
            $key = 'study_types';
            $data[$key][''] = '';
            foreach (\Vocabulary\StudyType::orderBy('id')->get() as $o) {
                if ($linguagem_selecionada == 'en') {
                    $t = $o;
                } else {
                    $t = $o->translations()->where('language', '=', $linguagem_selecionada)->first();
                }
                // $data[$key][$t->id] = $t->label;
                $data[$key][strtolower($o->label)] = $t->label;
            }
        }
        {
            $key = 'genders';
            $data[$key][''] = '';
            $data[$key]['-'] = $fieldlabel->get_or_new('gender_both');
            $data[$key]['m'] = $fieldlabel->get_or_new('gender_masculine');
            $data[$key]['f'] = $fieldlabel->get_or_new('gender_feminine');
        }
        {
            $key = 'recruitment_statuses';
            $data[$key][''] = '';
            foreach (\Vocabulary\RecruitmentStatus::orderBy('order')->get() as $o) {
                if ($linguagem_selecionada == 'en') {
                    $t = $o;
                } else {
                    $t = $o->translations()->where('language', '=', $linguagem_selecionada)->first();
                }
                // $data[$key][$t->id] = $t->label;
                $data[$key][strtolower($o->label)] = $t->label;
            }
        }
        {
            $key = 'institution_types';
            $data[$key][''] = '';
            foreach (\Vocabulary\InstitutionType::orderBy('order')->get() as $o) {
                if ($linguagem_selecionada == 'en') {
                    $t = $o;
                } else {
                    $t = $o->translations()->where('language', '=', $linguagem_selecionada)->first();
                }
                // $data[$key][$t->id] = $t->description;
                $data[$key][strtolower($o->label)] = $t->description;
            }
        }
        {
            $key = 'min_age_units';
            $data[$key][''] = '';
            $data[$key]['Y'] = $fieldlabel->get_or_new('min_age_unit_years');
            $data[$key]['M'] = $fieldlabel->get_or_new('min_age_unit_months');
            $data[$key]['W'] = $fieldlabel->get_or_new('min_age_unit_weeks');
            $data[$key]['D'] = $fieldlabel->get_or_new('min_age_unit_days');
            $data[$key]['H'] = $fieldlabel->get_or_new('min_age_unit_hours');
        }
        {
            $key = 'max_age_units';
            $data[$key][''] = '';
            $data[$key]['Y'] = $fieldlabel->get_or_new('min_age_unit_years');
            $data[$key]['M'] = $fieldlabel->get_or_new('min_age_unit_months');
            $data[$key]['W'] = $fieldlabel->get_or_new('min_age_unit_weeks');
            $data[$key]['D'] = $fieldlabel->get_or_new('min_age_unit_days');
            $data[$key]['H'] = $fieldlabel->get_or_new('min_age_unit_hours');
        }
        return $data;
    }

    public function advanced()
    {
        {
            $data = [
                'content' => $this->load->view('search_advanced.php', $this->tags(), TRUE),
            ];
        }
        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

}
