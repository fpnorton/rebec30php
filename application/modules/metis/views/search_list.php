<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('Search'); $fieldlabel->enableUpdate(); ?>
<?php $fossil_fieldlabel = new Helper\FieldLabelLanguage('Fossil'); $fossil_fieldlabel->enableUpdate(); ?>
<?php $fosseis = $fosseis ?? [$fossil]; ?>
<?
// echo '<pre>'.var_export([
//     __FILE__ => __LINE__,
//     'count($results)' => count($results),
// ], TRUE).'</pre>';
// foreach ($results as $fossil_id => $result) :
//     echo '<pre>'.var_export([
//         __FILE__ => __LINE__,
//         'array_keys($result)' => array_keys($result),
//         '$result[is_most_recent]' => $result['is_most_recent'],
//     ], TRUE).'</pre>';
// endforeach;
function stritr($text, $words_reference) {
    foreach ($words_reference as $word_in => $word_out) {
        $offset = NULL;
        $alarm = 10;
        while($alarm-- > 0) {
//            echo '<pre>'.var_export([
//                    __FILE__ => __LINE__,
//                    'text' => $text,
//                ], TRUE).'</pre>';
            $position = stripos($text, $word_in, $offset);
            if ($position === false) break;
            $word_out = "<b class='word-mark'>".substr($text, $position, strlen($word_in))."</b>";
            $text = substr_replace($text, $word_out, $position, strlen($word_in));
            $offset = $position + strlen($word_out);
//            echo '<pre>'.var_export([
//                    __FILE__ => __LINE__,
//                    'word_in' => $word_in,
//                    'position' => $position,
//                    'offset' => $offset,
//                    'text' => $text,
//                ], TRUE).'</pre>';
        }
    }
    return $text;
}
?>
<style><!--
    .word-mark {
        color: gray !important;
        background-color:yellow !important;
    }
--></style>
<style><!--
    .container {
    /*    margin: 0 auto;*/
    /*    width: 1004px;*/
        line-height: 1.7em;
    /*    background: #fff;*/
    }
    span.label, span.legend {
        font-weight: bold;
    }
    H2, H2 a {
        color: #006837;
        margin: 0px;
        text-decoration: none;
        font-size: 19px;
    }
    h3 {
        display: block;
        font-size: 1.17em;
        margin-block-start: 1em;
        margin-block-end: 1em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;
        font-weight: bold;
    }
    section {
        margin: 0;
        padding: 0;
        text-align: left;
        font-size: 95%;
        font-family: 'Lato', sans-serif;
        background: #f2f2f0;
    }
    .balloon {
        border: 6px solid #ffffff;
        border-radius: 16px;
        -moz-border-radius: 16px;
        -webkit-border-radius: 16px;
        background-color: #999999;
    }
    .subset {
        border: 1px solid #000000;
        border-radius: 5px;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        padding: 10px;
        margin: 10px;
    }
    table.dataTable TH {
        color: #0c4762;
        font-size: 90%;
        background: #dedede;
    }
    table.dataTable TH, table.dataTable TD {
        border-bottom: 1px solid #c9c9c9;
        font-size: 85%;
        padding: 6px 4px;
    }
--></style>
<section class="row">
    <div class="col-12">
        <div class="row" > <!-- style="max-height: 600px; overflow:auto; " -->
            <section class="container" style="min-height: 10px; height: 90%;">
                <span class="welcome_subtitle">
                    <h3><?php echo $fieldlabel->get_or_new('main_title') ?></h3>
                    <hr />
                </span>
                <span>
                    <pre><?php 
                    // echo var_export([
                    //     __FILE__ => __LINE__,
                    //     '$previous' => $previous,
                    //     '$tag' => $tag,
                    // ]); 
                    ?></pre>
                    <div class="card" style="margin-top: 5px;">
                    <?php if ($previous['advanced']) : ?>
                        <div class="card-header text-justify">
                            <?php echo $fieldlabel->get_or_new('advanced_search_title') ?>
                        </div>
                        <div class="card-body text-left">
                            <?php if ($previous['terms']) : ?>
                                <span class="label"><?php echo $fieldlabel->get_or_new('terms') ?>:</span>
                                <span class="value"><?php echo $previous['terms'] ?></span><br />
                            <?php endif; ?>
                            <?php if ($previous['country']) : ?>
                                <span class="label"><?php echo $fieldlabel->get_or_new('country') ?>:</span>
                                <span class="value"><?php echo $tag['countries'][ $previous['country'] ] ?></span><br />
                            <?php endif; ?>
                            <?php if ($previous['study-type']) : ?>
                                <span class="label"><?php echo $fieldlabel->get_or_new('study-type') ?>:</span>
                                <span class="value"><?php echo $tag['study_types'][ $previous['study-type'] ] ?></span><br />
                            <?php endif; ?>
                            <?php if ($previous['gender']) : ?>
                                <span class="label"><?php echo $fieldlabel->get_or_new('gender') ?>:</span>
                                <span class="value"><?php echo $tag['genders'][ $previous['gender'] ] ?></span><br />
                            <?php endif; ?>
                            <?php if ($previous['recruitment-status']) : ?>
                                <span class="label"><?php echo $fieldlabel->get_or_new('recruitment-status') ?>:</span>
                                <span class="value"><?php echo $tag['recruitment_statuses'][ $previous['recruitment-status'] ] ?></span><br />
                            <?php endif; ?>
                            <?php if ($previous['institution-type']) : ?>
                                <span class="label"><?php echo $fieldlabel->get_or_new('institution-type') ?>:</span>
                                <span class="value"><?php echo $tag['institution_types'][ $previous['institution-type'] ] ?></span><br />
                            <?php endif; ?>
                            <?php if ($previous['min-age-value']) : ?>
                                <span class="label"><?php echo $fieldlabel->get_or_new('age-limit-minimum') ?>:</span>
                                <span class="value"><?php echo $previous['min-age-value'] ?></span>
                                <span class="value"><?php echo $tag['min_age_units'][ $previous['min-age-unit'] ] ?></span><br />
                            <?php endif; ?>
                            <?php if ($previous['max-age-value']) : ?>
                                <span class="label"><?php echo $fieldlabel->get_or_new('age-limit-minimum') ?>:</span>
                                <span class="value"><?php echo $previous['max-age-value'] ?></span>
                                <span class="value"><?php echo $tag['max_age_units'][ $previous['max-age-unit'] ] ?></span><br />
                            <?php endif; ?>
                        </div>
                    <?php else : ?>
                        <div class="card-header text-justify">
                            <?php echo $fieldlabel->get_or_new('simple_search_title') ?>
                        </div>
                        <div class="card-body text-left">
                            <?php if ($previous['q']) : ?>
                                <span class="label"><?php echo $fieldlabel->get_or_new('terms') ?>:</span>
                                <span class="value"><?php echo $previous['q'] ?></span><br />
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                        <div class="card-body text-right">
                            <?php 
                            if (count($results) == 0) : 
                                echo $fieldlabel->get_or_new('no_results_found');
                            elseif (count($results) == 1) : 
                                echo '1 '.$fieldlabel->get_or_new('result_found');
                            else : 
                                echo count($results).' '.$fieldlabel->get_or_new('results_found');
                            endif; 
                            ?>
                        </div>
                    </div>
                </span>
                <? $counter = 0; ?>
                <? $collapse_index = 1; ?>
                <?php foreach ($results as $fossil_id => $result) : ?>
<!-- -->
                    <?php if ($result['is_most_recent'] == false) continue; ?>
                    <?php $fossil = \Fossil\Fossil::where('id', '=', $fossil_id)->first(); ?>
                    <?php 
                    $revision_1 = \Fossil\Fossil::where('content_type_id', '=', 24)
                        ->where('revision_sequential', '=', 1)
                        ->where('object_id', '=', $fossil->object_id)
                        ->first();
                    ?>
                    <?php $clinicalTrial = \Repository\ClinicalTrial::find($fossil->object_id); ?>
                    <?php $json = $result['unserialized'];  ?>
<!-- -->
                    <div class="card" style="border-color: black; margin-top: 5px;">
                        <?php if (count($result['words']) > 0) : ?>
                            <div class="card-header text-justify">
                                <?php echo $fieldlabel->get_or_new('words_found') ?>:&nbsp;
<!-- PALAVRAS ENCONTRADAS -->
                                <? for ($i = 0; $i < count($result['words']);  $i++) : ?>
                                <b><? echo (($i == 0) ? '' : '</b>,<b>') . array_keys($result['words'])[$i] ?></b>
                                <? endfor; ?>
                            </div>
<!-- -->
                        <?php endif; ?>
                        <div class="card-body text-justify">
<!-- CAMPOS E MARCAÇÃO DE PALAVRAS -->
                            <? $_words = []; ?>
                            <? $_fields = []; ?>
                            <? foreach ($result['words'] as $word => $fields) : ?>
                                <? $_words[$word] =  "<b style='background-color:yellow !important;'>".$word."</b>"; ?>
                                <? foreach ($fields as $field) : ?>
<!-- METAPHONE <? echo $word . ' =  '. metaphone($word) . ' ' . $field ?> -->
                                    <? $_fields[$field]++; ?>
                                <? endforeach; ?>
                            <? endforeach; ?>
                            <!-- -->
                            <?php if (linguagem_selecionada() == 'en') :?>
                                <?php $translation = $json['public_title']; ?>
                            <?php else : ?>
                                <?php foreach ($json['translations'] as $key => $value) : ?>
                                    <?php if ($value['language'] == linguagem_selecionada()) $translation = $value['public_title']; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <div class="">
                                <h5><?php echo stritr($translation, $_words) ?></h5>

                                <span class="label"><?php echo $fossil_fieldlabel->get_or_new('first_revision_approved_date'); ?>:</span>
                                <span class="value">
                                    <?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($revision_1->creation)) ?>
                                    <sup>(<?php echo @\Helper\Language::$available[linguagem_selecionada()]['date_sample'] ?>)</sup>
                                </span>
                                <br>
                                <span class="label"><?php echo $fossil_fieldlabel->get_or_new('revision_approved_date'); ?>:</span>
                                <span class="value">
                                    <?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($fossil->creation)) ?>
                                    <sup>(<?php echo @\Helper\Language::$available[linguagem_selecionada()]['date_sample'] ?>)</sup>
                                </span>
                                <br>

                                <span class="label"><?php echo $fossil_fieldlabel->get_or_new('last_revision'); ?>:</span>
                                <span class="value"><?php echo $fossil->revision_sequential ?></span><br />
                                <span class="label"><?php echo $fossil_fieldlabel->get_or_new('study_type'); ?>:</span>
                                <span class="value"><?php echo $json['is_observational'] ? $fossil_fieldlabel->get_or_new('is_observational') : $fossil_fieldlabel->get_or_new('is_interventional') ?></span>
                            </div>
                            <!-- -->
                            <? foreach ($_fields as $field => $count) : ?>
                            <?
                                // echo '<pre>'.var_export([
                                //     __FILE__ => __LINE__,
                                //     '$field' => $field,
                                //     '$count' => $count,
                                // ], TRUE).'</pre>';
                            ?>
<!-- -->
                                <? if ($field == 'scientific_title') : ?>
                                    <div class="card-text">
                                        <p><span data-toggle="collapse"
                                                 data-target="#collapse_<? echo $collapse_index ?>"
                                                 class="label"><?php echo $fossil_fieldlabel->get_or_new('scientific_title'); ?>:&nbsp;(<b><i class="fa fa-caret-down fa-lg"></i><? echo $count ?></b>)</span></p>
                                        <div id="collapse_<? echo $collapse_index++ ?>"
                                             class="row collapse">
                                            <?php $key = 'scientific_title' ?>
                                            <?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                $text = $json[$key];
                                                foreach ($json['translations'] as $translation) :
                                                    if ($translation['language'] == $language_key) :
                                                        $text = $translation[$key];
                                                        break;
                                                    endif;
                                                endforeach;
                                                if (empty($text)) continue;
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p><?php echo stritr($text, $_words) ?></p>
                                                </div><?
                                            endforeach;
                                            ?>
                                        </div>
                                    </div>
                                <?php endif?>
                                <? if ($field == 'exclusion_criteria') : ?>
                                <div class="card-text">
                                    <p><span data-toggle="collapse"
                                             data-target="#collapse_<? echo $collapse_index ?>"
                                             class="label"><?php echo $fossil_fieldlabel->get_or_new('exclusion_criteria'); ?>:&nbsp;(<b><i class="fa fa-caret-down fa-lg"></i><? echo $count ?></b>)</span></p>
                                    <div id="collapse_<? echo $collapse_index++ ?>"
                                         class="row collapse">
                                        <?php $key = 'exclusion_criteria' ?>
                                        <?php
                                        foreach (\Helper\Language::$available as $language_key => $language_value) :
                                            if ($language_key == 'en') {
                                                $text = $json[$key];
                                            } else {
                                                $text = '';
                                                foreach ($json['translations'] as $translation) :
                                                    if ($translation['language'] == $language_key) :
                                                        $text = $translation[$key];
                                                        break;
                                                    endif;
                                                endforeach;
                                                if (empty($text)) continue;
                                            }
                                            ?><div class="col-sm-3 balloon">
                                            <h2><?php echo $language_key ?></h2>
                                            <p><?php echo stritr($text, $_words) ?></p>
                                            </div><?
                                        endforeach;
                                        ?>
                                    </div>
                                </div>
                                <?php endif?>
                                <? if ($field == 'inclusion_criteria') : ?>
                                <div class="card-text">
                                    <p><span data-toggle="collapse"
                                             data-target="#collapse_<? echo $collapse_index ?>"
                                             class="label"><?php echo $fossil_fieldlabel->get_or_new('inclusion_criteria'); ?>:&nbsp;(<b><i class="fa fa-caret-down fa-lg"></i><? echo $count ?></b>)</span></p>
                                    <div id="collapse_<? echo $collapse_index++ ?>"
                                         class="row collapse">
                                        <?php $key = 'inclusion_criteria' ?>
                                        <?php
                                        foreach (\Helper\Language::$available as $language_key => $language_value) :
                                            if ($language_key == 'en') {
                                                $text = $json[$key];
                                            } else {
                                                $text = '';
                                                foreach ($json['translations'] as $translation) :
                                                    if ($translation['language'] == $language_key) :
                                                        $text = $translation[$key];
                                                        break;
                                                    endif;
                                                endforeach;
                                                if (empty($text)) continue;
                                            }
                                            ?><div class="col-sm-3 balloon">
                                            <h2><?php echo $language_key ?></h2>
                                            <p><?php echo stritr($text, $_words) ?></p>
                                            </div><?
                                        endforeach;
                                        ?>
                                    </div>
                                </div>
                                <?php endif?>
                                <? if ($field == 'hc_freetext') : ?>
                                <div class="card-text">
                                    <p><span data-toggle="collapse"
                                             data-target="#collapse_<? echo $collapse_index ?>"
                                             class="label"><?php echo $fossil_fieldlabel->get_or_new('health_conditions'); ?>:&nbsp;(<b><i class="fa fa-caret-down fa-lg"></i><? echo $count ?></b>)</span></p>
                                    <div id="collapse_<? echo $collapse_index++ ?>"
                                         class="row collapse">
                                        <?php $key = 'hc_freetext' ?>
                                        <?php
                                        foreach (\Helper\Language::$available as $language_key => $language_value) :
                                            if ($language_key == 'en') {
                                                $text = $json[$key];
                                            } else {
                                                $text = '';
                                                foreach ($json['translations'] as $translation) :
                                                    if ($translation['language'] == $language_key) :
                                                        $text = $translation[$key];
                                                        break;
                                                    endif;
                                                endforeach;
                                                if (empty($text)) continue;
                                            }
                                            ?><div class="col-sm-3 balloon">
                                            <h2><?php echo $language_key ?></h2>
                                            <p><?php echo stritr($text, $_words) ?></p>
                                            </div><?
                                        endforeach;
                                        ?>
                                    </div>
                                </div>
                                <?php endif?>
                                <? if ($field == 'i_freetext') : ?>
                                    <div class="card-text">
                                        <p><span data-toggle="collapse"
                                                 data-target="#collapse_<? echo $collapse_index ?>"
                                                 class="label"><?php echo $fossil_fieldlabel->get_or_new('intervention'); ?>:&nbsp;(<b><i class="fa fa-caret-down fa-lg"></i><? echo $count ?></b>)</span></p>
                                        <div id="collapse_<? echo $collapse_index++ ?>"
                                             class="row collapse">
                                            <?php $key = 'i_freetext' ?>
                                            <?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $json[$key];
                                                } else {
                                                    $text = '';
                                                    foreach ($json['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation[$key];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                    if (empty($text)) continue;
                                                }
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p><?php echo stritr($text, $_words) ?></p>
                                                </div><?
                                            endforeach;
                                            ?>
                                        </div>
                                    </div>
                                <?php endif?>
                            <? endforeach; ?>
                            <!-- -->
                            <a class="collapsed pull-right"
                               href="<?php echo base_url('/rg/'.$clinicalTrial->trial_id) ?>"
                               aria-expanded="false"
                               aria-controls="collapseSummary">
                                <button type="button" class="btn btn-light btn-sm"><?php echo $fieldlabel->get_or_new('read_open') ?></button>
                            </a>
                        </div>
                    </div>
                    <?php $counter++; ?>
                <?php endforeach; ?>
            </section>
        </div>
        <div>&nbsp;</div>
        <span><?php
//            echo '<pre>'.var_export(array_keys($json), TRUE).'</pre>';
//            echo '<pre>'.var_export($json, TRUE).'</pre>';
        ?></span>
    </div>
</section>
