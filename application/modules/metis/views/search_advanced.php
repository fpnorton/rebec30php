<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('Search'); $fieldlabel->enableUpdate(); ?>
<?php 
// echo '<pre>'.var_export([
//        __FILE__ => __LINE__,
//        '$countries' => $countries,
//    ],true).'</pre>';
$elements[] =
    form_open('metis/search/query' ?? 0, ['id' => 'advanced','autocomplete' => 'off',]);
{
    foreach (
        [
            form_hidden('advanced', 1),
            form_fieldset($fieldlabel->get_or_new('terms'), ['class' => 'form-group']),
            form_input('terms', '', ['class' => 'form-input']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('country'), ['class' => 'form-group']),
            form_dropdown('country', $countries, [], ['class' => 'form-input']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('study-type'), ['class' => 'form-group']),
            form_dropdown('study-type', $study_types, [], ['class' => 'form-input']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('gender'), ['class' => 'form-group']),
            form_dropdown('gender', $genders, [], ['class' => 'form-input']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('recruitment-status'), ['class' => 'form-group']),
            form_dropdown('recruitment-status', $recruitment_statuses, [], ['class' => 'form-input']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('institution-type'), ['class' => 'form-group']),
            form_dropdown('institution-type', $institution_types, [], ['class' => 'form-input']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('age-limit-minimum'), ['class' => 'form-group']),
            form_input('min-age-value', '', ['class' => 'form-input']),
            form_dropdown('min-age-unit', $min_age_units, [], ['class' => 'form-input']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('age-limit-maximum'), ['class' => 'form-group']),
            form_input('max-age-value', '', ['class' => 'form-input']),
            form_dropdown('max-age-unit', $max_age_units, [], ['class' => 'form-input']),
            form_fieldset_close(),
        ] as $form_item) $elements[] = $form_item;
}
{
//    array_unshift($elements,
//        form_open('flatPages/'.$page->id ?? 0, ['id' => 'edit','autocomplete' => 'off',])
//    );
    $elements[] =
        form_submit('submit', $genericlabel->get_or_new('button_send'));
    $elements[] =
        form_close();
}
?>
<div class="row">
    <!-- left column -->
    <div class="col-md-1"></div>
    <!-- center column -->
    <div class="col-md-10">
        <!-- general form elements -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php echo $fieldlabel->get_or_new('advanced_search_title')?></h3>
            </div>
            <br />
            <?php foreach ($elements as $element) echo $element; ?>
        </div>
    </div>
    <!-- right column -->
    <div class="col-md-1"></div>
</div>
