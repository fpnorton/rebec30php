<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends PantheonController {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
	}

    public function index()
    {
        $data = [];
//        $data['content'] = $this->load->view('portal.php', $data, TRUE);;
        {
            $data['content_left'] = 
                $this->load->view('warnings_preview.php', [
                    'warnings' => \Django\FlatPage::where('url', 'like', 'warning-%')->orderBy('url', 'desc')->get(),
                ], TRUE) .
                $this->load->view('news_preview.php', [
                    'newses' => \ReviewApp\News::orderBy('created', 'desc')->take(5)->get()
                ], TRUE);;
        }
        {
            $data['content_right'] = $this->load->view('fossil_preview.php', [
                'fosseis' => \Fossil\Fossil::where('content_type_id', '=', 24)
                    ->where('is_most_recent', '=', 1)
                    ->take(7)
                    ->orderBy('creation', 'desc')
                    ->get()
            ], TRUE);;
        }
        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
            'registro_habilitado' => ($this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_USER_REGISTER_ENABLE) == 'ON' ? TRUE : FALSE),
        ]));
    }

}

/* End of file Welcome.php */
/* Location: ./application/modules/welcome/controllers/Welcome.php */