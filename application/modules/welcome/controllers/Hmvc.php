<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Laravolt\Avatar\Avatar;

class Hmvc extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		// modules::run('welcome/iswelcome');
	}

	public function index()
	{
		echo "string";
		//$this->iswelcome();
		$config=[
				// Image shape: circle or square
			    'shape' => 'circle',

			    // Image width, in pixel
			    'width'    => 300,

			    // Image height, in pixel
			    'height'   => 300,
			    // Number of characters used as initials. If name consists of single word, the first N character will be used
			    'chars'    => 2,

			    // font size
			    'fontSize' => 170,

			    'backgrounds'   => [
			        '#f44336',
			        '#E91E63',
			        '#9C27B0',
			        '#673AB7',
			        '#3F51B5',
			        '#2196F3',
			        '#03A9F4',
			        '#00BCD4',
			        '#009688',
			        '#4CAF50',
			        '#8BC34A',
			        '#CDDC39',
			        '#FFC107',
			        '#FF9800',
			        '#FF5722',
			    ],
			    'border'    => [
			        'size'  => 0,
			        
			        // border color, available value are:
			        // 'foreground' (same as foreground color)
			        // 'background' (same as background color)
			        // or any valid hex ('#aabbcc')
			        'color' => 'foreground'
			    ]
		];


		$path='uploads/'.time().".png";
		$avatar = new Avatar($config);
		//$avatar->create('John Doe')->toBase64();
		$avatar->create('Manpreet')->save($path, $quality = 90);

	}

}

/* End of file Hmvc.php */
/* Location: ./application/modules/welcome/controllers/Hmvc.php */