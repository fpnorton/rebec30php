<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('WelcomeForm'); $fieldlabel->enableUpdate(); ?>
<section class="row">
    <div class="col-7">
        <div class="row">
            <section class="container">
                <span class="welcome_subtitle">
                    <h5><?php echo $fieldlabel->get_or_new('news_subtitle') ?></h5>
                    <hr />
                </span>
                <?php $Newses = \ReviewApp\News::take(5)->orderBy('created', 'desc')->get(); ?>
                <?php foreach ($Newses as $News) : ?>
                    <?php if (linguagem_selecionada() == 'en') :?>
                        <?php $translation = $News; ?>
                    <?php else : ?>
                        <?php $translation = $News->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                    <?php endif; ?>
                    <div class="card menu_dashboard"
                         id="summary_<?php echo $News->id ?>">
                        <div class="card-body">
                            <div class="card-title">
                                <small class="card-description pull-right"><?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($News->created)) ?></small>
                                <h4><a class="title_link" href="<?php echo base_url('/news/'.$News->id) ?>"><?php echo $translation->title ?></a></h4>
                            </div>
                            <div class="card-text text-justify"
                                 style="max-height: 150px; overflow: hidden; text-overflow: ellipsis;"
                                 id="text_more_<?php echo $News->id ?>"><?php
                               echo character_limiter($translation->text, 1000);
                               ?></div>
                            <script>
                                function show_less_<?php echo $News->id ?>() {
                                    $('#link_less_<?php echo $News->id ?>').show();
                                    $('#link_more_<?php echo $News->id ?>').hide();
                                    $('#text_more_<?php echo $News->id ?>').css('max-height', '');
                                }
                                function show_more_<?php echo $News->id ?>() {
                                    $('#link_more_<?php echo $News->id ?>').show();
                                    $('#link_less_<?php echo $News->id ?>').hide();
                                    $('#text_more_<?php echo $News->id ?>').css('max-height', '150px');
                                }
                            </script>
                            <a class="collapsed pull-right"
                               id="link_more_<?php echo $News->id ?>"
                               data-toggle="collapse"
                               href="#"
                               onclick="show_less_<?php echo $News->id ?>()"
                               aria-expanded="false"
                               aria-controls="collapseSummary">
                               <button type="button" class="btn btn-light btn-sm"><?php echo $fieldlabel->get_or_new('read_more') ?></button>
                            </a>
                            <a class="collapsed pull-right"
                               style="display: none;"
                               id="link_less_<?php echo $News->id ?>"
                               data-toggle="collapse"
                               href="#"
                               onclick="show_more_<?php echo $News->id ?>()"
                               aria-expanded="false"
                               aria-controls="collapseSummary">
                               <button type="button" class="btn btn-light btn-sm"><?php echo $fieldlabel->get_or_new('read_less') ?></button>
                            </a>
                            <a class="collapsed pull-right"
                               href="<?php echo base_url('/news/'.$News->id) ?>"
                               aria-expanded="false"
                               aria-controls="collapseSummary">
                                <button type="button" class="btn btn-light btn-sm"><?php echo $fieldlabel->get_or_new('read_open') ?></button>
                            </a>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                <?php endforeach; ?>
            </section>
        </div>
    </div>

    <div class="col-5">
        <div class="row">
            <section class="container">
                <span class="welcome_subtitle">
                    <h5><?php echo $fieldlabel->get_or_new('approved_subtitle') ?></h5>
                    <hr />
                </span>
                <?php $Fosseis = \Fossil\Fossil::where('content_type_id', '=', 24)->where('is_most_recent', '=', 1)->take(7)->orderBy('creation', 'desc')->get(); ?>
                <?php foreach ($Fosseis as $Fossil) : ?>
                    <?php 
                        $clinicaltrial = \Repository\ClinicalTrial::find($Fossil->object_id);
                        $json = json_decode($Fossil->serialized, true); 
                    ?>
                    <?php if (linguagem_selecionada() == 'en') :?>
                        <?php $translation = $json['public_title']; ?>
                    <?php else : ?>
                        <?php foreach ($json['translations'] as $key => $value) : ?>
                            <?php if ($value['language'] == linguagem_selecionada()) $translation = $value['public_title']; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <div class="card menu_dashboard">
                        <div class="card-body">
                            <div class="card-title">
                                <small class="card-description pull-right"><?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($Fossil->creation)) ?></small>
                                <h5><a class="title_link" href="<?php echo base_url('/rg/'.$clinicaltrial->trial_id) ?>"><?php echo $translation ?></a></h5>
                            </div>
                            <div class="card-text text-justify"><pre><?php
//                                                echo var_export(array_keys($json['translations'][0]), true);
                            ?></pre></div>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                <?php endforeach; ?>
                <a class="pull-right"
                   href="<?php echo base_url('/trial'); ?>"
                   aria-expanded="false"
                   aria-controls="collapseSummary">
                   <button type="button" class="btn btn-default btn-sm"><?php echo $fieldlabel->get_or_new('list_all') ?></button>
                </a>
            </section>
        </div>
    </div>
</section>
