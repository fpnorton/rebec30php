<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('WelcomeForm'); $fieldlabel->enableUpdate(); ?>
<?php $newses = $newses ?? [$news]; ?>
<div class="row">
    <section class="container">
                <span class="welcome_subtitle">
                    <h5><?php echo $fieldlabel->get_or_new('news_subtitle') ?></h5>
                    <hr />
                </span>
        <?php foreach ($newses as $News) : ?>
            <?php if (linguagem_selecionada() == 'en') :?>
                <?php $translation = $News; ?>
            <?php else : ?>
                <?php $translation = $News->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
            <?php endif; ?>
            <div class="card menu_dashboard"
                 id="summary_<?php echo $News->id ?>">
                <div class="card-body">
                    <div class="card-title">
                        <small class="card-description pull-right"><?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($News->created)) ?></small>
                        <h4><a class="title_link" href="<?php echo base_url('/news/'.$News->id) ?>"><?php echo (empty($translation->title) ? $News->title : $translation->title) ?></a></h4>
                    </div>
                    <div class="card-text text-justify"
                         style="max-height: 150px; overflow: hidden; text-overflow: ellipsis;"
                         id="text_more_<?php echo $News->id ?>"><?php
                        echo character_limiter(empty(trim($translation->text)) ? $News->text : $translation->text, 2000);
                        ?></div>
                    <script>
                        function show_less_<?php echo $News->id ?>() {
                            $('#link_less_<?php echo $News->id ?>').show();
                            $('#link_more_<?php echo $News->id ?>').hide();
                            $('#text_more_<?php echo $News->id ?>').css('max-height', '');
                        }
                        function show_more_<?php echo $News->id ?>() {
                            $('#link_more_<?php echo $News->id ?>').show();
                            $('#link_less_<?php echo $News->id ?>').hide();
                            $('#text_more_<?php echo $News->id ?>').css('max-height', '100px');
                        }
                    </script>
                    <a class="collapsed pull-right"
                       id="link_more_<?php echo $News->id ?>"
                       data-toggle="collapse"
                       href="#"
                       onclick="show_less_<?php echo $News->id ?>()"
                       aria-expanded="false"
                       aria-controls="collapseSummary">
                        <button type="button" class="btn btn-light btn-sm"><?php echo $fieldlabel->get_or_new('read_more') ?></button>
                    </a>
                    <a class="collapsed pull-right"
                       style="display: none;"
                       id="link_less_<?php echo $News->id ?>"
                       data-toggle="collapse"
                       href="#"
                       onclick="show_more_<?php echo $News->id ?>()"
                       aria-expanded="false"
                       aria-controls="collapseSummary">
                        <button type="button" class="btn btn-light btn-sm"><?php echo $fieldlabel->get_or_new('read_less') ?></button>
                    </a>
                    <?php if (empty(trim($translation->text)) == false) : ?>
                    <a class="collapsed pull-right"
                       href="<?php echo base_url('/news/'.$News->id) ?>"
                       aria-expanded="false"
                       aria-controls="collapseSummary">
                        <button type="button" class="btn btn-light btn-sm"><?php echo $fieldlabel->get_or_new('read_open') ?></button>
                    </a>
                    <?php endif; ?>
                </div>
            </div>
            <div>&nbsp;</div>
        <?php endforeach; ?>
    </section>
</div>
