<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('WelcomeForm'); $fieldlabel->enableUpdate(); ?>
<?php if ($warnings->count() > 0) : ?>
    <div class="row">
        <section class="container">
            <span class="welcome_subtitle">
            </span>
        <?php foreach ($warnings as $FlatPage) : ?>
            <?php if (linguagem_selecionada() == 'en') :?>
                <?php $translation = $FlatPage; ?>
            <?php else : ?>
                <?php $translation = $FlatPage->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
            <?php endif; ?>
            <div class="card menu_dashboard"
                 id="summary_<?php echo $FlatPage->id ?>">
                <div class="card-body">
                    <div class="card-title">
                    </div>
                    <div class="card-text text-justify"><?php
                        echo empty($translation->content) ? $FlatPage->content : $translation->content;
                    ?></div>
                </div>
            </div>
            <div>&nbsp;</div>
        <?php endforeach; ?>
        </section>
    </div>
<?php endif; ?>