<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('WelcomeForm'); ?>
<style>
    .items_menu{
        border-left:3px solid #0D6965;
        padding-left: 0px;
    }
    .items_menu li a{
        text-decoration: none;
        color: #6D6A69;
        font-family: Helvetica;
        font-size:14px;
        display: block;
    }
    .items_menu li{
        list-style: none;
        border-bottom:1px solid #e1e1e2;
        padding-top: 5px;
        padding-bottom: 5px;
        padding-left: 16px;
    }

    .items_menu li:hover{
        background-color:  #e7ecec;
    }
</style>
<div class="row">
    <section class="container">
        <div class="menu_dashboard">
            <ul class="items_menu">
                <li class="">
                    <a href="<?php echo base_url("pesquisador"); ?>">Painel Inicial</a>
                </li>
                <? if ($this->session->userdata('user')->isCommon()) : ?>
                <li class="">
                    <a href="<?php echo base_url("pesquisador/submissao/nova"); ?>">Nova submissão</a>
                </li>
                <? endif; ?>
            </ul>
        </div>
        <div>&nbsp;</div>
    </section>
    <section class="container">
    <? if (false) : ?>
        <div class="menu_dashboard">
            <ul class="items_menu">
                <li class="">
                    <a href="<?php echo base_url("pesquisador/ajuda"); ?>">Ajuda</a>
                </li>
            </ul>
        </div>
        <div>&nbsp;</div>
    <? endif; ?>
    </section>
</div>
