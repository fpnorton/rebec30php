<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('WelcomeForm'); ?>
<style>
    .number-rounded {
        width: 30px;
        line-height: 30px;
        border-radius: 50%;
        text-align: center;
        font-size: 18px;
        border: 1px solid #666;
        background-color: white;
        color: black;
        height: 30px;
        position: relative;
        align-self: center;
    }
    .number-rounded-ok {
        width: 30px;
        line-height: 30px;
        border-radius: 50%;
        text-align: center;
        font-size: 18px;
        font-weight: bold;
        border: 2px solid #00a65a;
        background-color: white;
        color: #00a65a;
        height: 30px;
        position: relative;
        align-self: center;
    }
    .number-rounded:hover {
        width: 30px;
        line-height: 30px;
        border-radius: 50%;
        text-align: center;
        font-size: 18px;
        border: 1px solid #666;
        background-color: gray;
        color: white;
        height: 30px;
        position: relative;
        align-self: center;
    }
    table {
        /*border:1px solid black;*/
    }
    table th {
        /*border-bottom:1px solid black;*/
        width: 30px;
    }
    table td {
        width: 30px;
    }
    th.rotate {
        height:1px;
        white-space: nowrap;
        position:relative;
    }
    th.rotate > div {
        position: absolute;
        top: 0;
        left: 5px;
        transform-origin: 0 0;
        transform: rotate(270deg);
        margin:auto;
    }
    .vcenter {
        display: inline-block;
        vertical-align: middle;
        float: none;
    }
    .vertical-align {
        display: flex;
        align-items: center;
    }
</style>
<div class="row">
    <section class="container menu_dashboard">
        <div class="row">
            <div class="col-sm-1 visible-sm-inline vertical-align" style="opacity: 0.04;">A R E S
            </div>
            <div class="col-sm-11">
                <div class="row">
                    <table style="width: 100%; table-layout:fixed;">
                        <tr>
                            <th style="height: 150px;"></th>
                            <? for ($i = 1; $i <= count(ClinicalTrialSteps::fieldsByName); $i++) : ?>
                                <th style="height: 150px;"></th>
                            <? endfor; ?>
                        </tr>
                        <tr>
                            <th class="rotate"><div><? echo (new Helper\FieldLabelLanguage('SummaryForm'))->enableUpdate()->get_or_new('main_title') ?></div></th>
                            <? for ($i = 1; $i <= count(ClinicalTrialSteps::fieldsByName); $i++) : ?>
                            <th class="rotate"><div><?
switch ($i) :
    case 1: echo (new Helper\FieldLabelLanguage('IdentificationForm'))->get_or_new('main_title');
        break;
    case 2: echo (new Helper\FieldLabelLanguage('AttachmentForm'))->get_or_new('main_title');
        break;
    case 3: echo (new Helper\FieldLabelLanguage('SponsorForm'))->get_or_new('main_title');
        break;
    case 4: echo (new Helper\FieldLabelLanguage('HealthConditionForm'))->get_or_new('main_title');
        break;
    case 5: echo (new Helper\FieldLabelLanguage('InterventionForm'))->get_or_new('main_title');
        break;
    case 6: echo (new Helper\FieldLabelLanguage('RecruitmentForm'))->get_or_new('main_title');
        break;
    case 7: echo (new Helper\FieldLabelLanguage('StudyTypeForm'))->get_or_new('main_title');
        break;
    case 8: echo (new Helper\FieldLabelLanguage('OutcomeForm'))->get_or_new('main_title');
        break;
    case 9: echo (new Helper\FieldLabelLanguage('ContactForm'))->get_or_new('main_title');
        break;
    case 10: echo (new Helper\FieldLabelLanguage('SummaryResultsForm'))->get_or_new('main_title');
        break;
    case 11: echo (new Helper\FieldLabelLanguage('IPDSharingStatementForm'))->get_or_new('main_title');
        break;
    default: echo ClinicalTrialSteps::fieldsByName[$i];
endswitch;
                            ?></div></th>
                            <? endfor; ?>
                        </tr>
                        <tr>
                            <td>
                                <a href="<? echo base_url('pesquisador/submissao/sumario/'.$submission->trial_id) ?>">
                                    <div class="number-rounded"><i class="fa fa-list"></i></div>
                                </a>
                            </td>
                            <? for ($i = 1; $i <= count(ClinicalTrialSteps::fieldsByName); $i++) : ?>
                            <td>
                                <a href="<? echo base_url('pesquisador/submissao/passo/'.$i.'/'.$submission->trial_id) ?>">
                                    <? $style = ['font-size: 13px !important;']; ?>
                                    <? if ($i == $current_step) { $style[] = 'background-color: gray !important;'; $style[] = 'color: white !important;'; } ?>
                                    <? $status = ClinicalTrialSteps::getFieldStatusCompletedByStep($fields_status ?? [], $i); ?>
                                    <? if ($status == 4) : ?>
                                    <div class="number-rounded-ok" style="<? echo implode(' ', $style)?>"><i class="fa fa-check"></i></div>
                                    <? else : ?>
                                    <div class="number-rounded" style="<? echo implode(' ', $style)?>"><? echo $i ?></div>
                                    <? endif; ?>
                                </a>
                            </td>
                            <? endfor; ?>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div>&nbsp;</div>
    </section>
</div>
