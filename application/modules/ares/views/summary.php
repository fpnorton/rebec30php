﻿﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('SummaryForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldLabelLanguage('SummaryForm'); $fieldhelp->enableUpdate(); ?>
<?php
?>
<div class="row">
    <div class="col-sm-12 content">
        <div class="row">
            <div class="col-2">
                <?php echo $fieldlabel->get_or_new('submission_title'); ?>
            </div>
            <div class="col-10">
                <? echo $VD['subData']->title ?>
            </div>
        </div>
        <div class="row">
            <div class="col-2">
                <?php echo $fieldlabel->get_or_new('submission_status'); ?>
            </div>
            <div class="col-10">
                <? echo $VD['subData']->status ?>
            </div>
        </div>
        <div class="row">
            <table class="table table-bordered table_list table_sumary">
                <thead>
                <tr class="table_title">
                    <th><?php echo $fieldlabel->get_or_new('step'); ?></th>
                    <th><?php echo $fieldlabel->get_or_new('name'); ?></th>
                    <th><?php echo $fieldlabel->get_or_new('status'); ?></th>
                    <th><?php echo $fieldlabel->get_or_new('updated'); ?></th>
                </tr>
                </thead>
                <tbody>
                <? $count = 0; ?>
                <?php foreach(ClinicalTrialSteps::fieldsByName as $passo):
                    $file_name = $VD['fields_status']['en'][$passo] == 4 ? 'ok' : 'error';
                    $tip = NULL;
                    if ($VD['subData']->remarks->count() > 0)
                    {
                        $context = ClinicalTrialSteps::remarksByName[$count];
                        $statuses = ['open'];
                        $collection = 
                            $VD['subData']->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                                if (in_array($remark->status, $statuses)) {
                                    return $remark;
                                }
                            });
                        if ($collection->count() > 0) $tip = $collection->sortByDesc('id')->first()->text;
                    }
                    else 
                    {
                        $tip = $fieldhelp->get_or_new('tip_'.($count+1));
                    }
                    ?>
                    <tr>
                        <!-- PASSO -->
                        <td><?php echo (++$count); ?></td>
                        <!-- Nome -->
                        <td>
                            <a href="<?php echo base_url( "/pesquisador/submissao/passo/".($count).'/'.$clinicaltrial_id); ?>">
                                <?php echo $genericlabel->get_or_new('step_'.($count)); ?>
                            </a>
                        </td>
                        <!-- SITUAÇÃO -->
                        <td>
                            <?php
                            $situacao = array(
                                1 => $genericlabel->get_or_new('situation_1_changed'),
                                2 => $genericlabel->get_or_new('situation_2_missing'),
                                3 => $genericlabel->get_or_new('situation_3_partial'),
                                4 => $genericlabel->get_or_new('situation_4_complete')
                            );
                            // $tip = $fieldhelp->get_or_new('tip_'.($count));
                            ?>
                            <img data-toggle="tooltip"
                                 data-placement="auto"
                                 src="<?php echo base_url('assets/images/field-' . $file_name . '.png')?>"
                                 data-original-title="<?php echo $tip; ?>">
                            <span class="table-inner-text"><?php echo $situacao[ClinicalTrialSteps::getFieldStatusCompletedByName($VD['fields_status']['en'], $passo)]; ?></span>
                        </td>
                        <!-- ALTERADO -->
                        <td>
                            <span><!-- ALTERADO? --></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-12"><hr /></div>
        </div>
        <?php if ($isPodeSerEnviado == false) : ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-8">&nbsp;</div>
                    <div class="col-2">
                        <button class="btn btn-secondary"><?php echo $fieldlabel->get_or_new('submission_button_send'); ?>
                        </button>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if ($VD['subData']->isDraft() and $isPodeSerEnviado and ($usuario->isCommon() or $usuario->isSuper())) : ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-8">&nbsp;</div>
                    <div class="col-2">
                        <button data-toggle="modal" data-target="#modal_send"
                                class="btn btn-primary"><?php echo $fieldlabel->get_or_new('submission_button_send'); ?>
                        </button>
                    </div>
                </div>
            </div>
            <style>
                /*.modal-dialog {*/
                /*    height: 50%;*/
                /*}*/
            </style>
            <!-- -->
            <? foreach ([
                    'send' => [
                        'modal-content-style' => 'border-width: 2px; border-color: blue;',
                    ],
                ] as $modal_key => $modal_pack) : ?>
                <div class="modal fade"
                     id="<?php echo 'modal_'.$modal_key ?>"
                     tabindex="-1"
                     role="dialog"
                     aria-labelledby="<?php echo 'modalLabel_'.$modal_key ?>"
                     aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered"
                         role="document">
                        <div class="modal-content" style="<? echo $modal_pack['modal-content-style'] ?>">
                            <div class="modal-header">
                                <h5 class="modal-title"
                                    id="<?php echo 'modalLabel_'.$modal_key ?>"><?php echo $fieldlabel->get_or_new('title_modal_'.$modal_key); ?>
                                </h5>
                                <button type="button"
                                        class="close"
                                        data-dismiss="modal"
                                        aria-label="<?php echo $genericlabel->get_or_new('button_close'); ?>">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body"><?php echo $fieldlabel->get_or_new('text_modal_'.$modal_key) ?>
                            </div>
                            <div class="modal-footer">
                                <form action="<?php echo base_url('pesquisador/'.$modal_key.'/'.$clinicaltrial_id).'?'.$postback ?>" method="post">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $genericlabel->get_or_new('button_close'); ?></button>
                                    <button type="submit" class="btn btn-primary"><?php echo $genericlabel->get_or_new('button_confirm'); ?></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
            <!-- -->
        <?php endif; ?>
    </div>
</div>
<?php $genericlabel->disableUpdate();?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>
