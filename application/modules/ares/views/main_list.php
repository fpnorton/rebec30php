<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('ClinicalTrialList'); $fieldlabel->enableUpdate(); ?>
<style>
</style>
<?php
$FIELD_ORDER = [
    '-',
    'Trial Identification',
    'Attachments',
    'Sponsors',
    'Health Conditions',
    'Interventions',
    'Recruitment',
    'Study Type',
    'Outcomes',
    'Contacts',
];
$FIELD_STATUS = [
    '-',
    'Changed',
    'Missing',
    'Partial',
    'Complete',
];
?>
<style>
    .number-rounded {
        width: 30px;
        line-height: 30px;
        border-radius: 50%;
        text-align: center;
        font-size: 13px;
        border: 1px solid #666;
    }
</style>
<div class="row">
    <section class="container">
        <div class="menu_dashboard">
<!-- -->
            <section>
                <span class="welcome_subtitle">
                    <h5><?php echo $fieldlabel->get_or_new('main_title') ?></h5>
                    <hr />
                </span>
                <?php foreach (['resubmit','draft','pending','approved'] as $status) : ?>
                    <h3><?php echo $genericlabel->get_or_new('status_'.$status) ?></h3>
                    <div class="overflow-auto" style="max-height: 300px;padding: 20px;">
                    <?php foreach ($submissions_by_status[$status] as $submission) : ?>
                        <?php if (linguagem_selecionada() == 'en') :?>
                            <?php $translation = $submission->trial()->first(); ?>
                        <?php else : ?>
                            <?php $translation = $submission->trial()->first()->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                        <?php endif; ?>
                        <div class="card shadow" style="border-color: black; margin-top: 5px; border-width: .3px;">
                            <div class="card-body container text-justify">
                                <span class="row">
                                    <div class="col-sm-3"><b><?php echo $fieldlabel->get_or_new('updated_at') ?></b>:</div>
                                    <div class="col-sm-9">
                                        <?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($submission->updated)) ?>
                                    </div>
                                </span>
                                <span class="row">
                                    <div class="col-sm-3"><b><?php echo $fieldlabel->get_or_new('title') ?></b>:</div>
                                    <div class="col-sm-9"><?php echo $translation->public_title ?></div>
                                </span>
                                <span class="row">
                                    <div class="col-sm-9">
                                        <? if ($status == 'pending') : ?>
                                        <? elseif ($status == 'draft') : ?>
                                            <a  class="btn btn-secondary btn-sm"
                                                href="pesquisador/submissao/sumario/<? echo $submission->trial_id ?>"><?php echo $fieldlabel->get_or_new('button_edit') ?></a>
                                        <? elseif ($status == 'resubmit') : ?>
                                            <a  class="btn btn-primary btn-sm"
                                                href="pesquisador/submissao/sumario/<? echo $submission->trial_id ?>"><?php echo $fieldlabel->get_or_new('button_review') ?></a>
                                        <? elseif ($status == 'approved') : ?>
                                            <a  class="btn btn-info btn-sm"
                                                href="observador/submissao/sumario/<? echo $submission->trial_id ?>"><?php echo $fieldlabel->get_or_new('button_view') ?></a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a  class="btn btn-danger btn-sm"
                                                href="pesquisador/submissao/sumario/<? echo $submission->trial_id ?>"><?php echo $fieldlabel->get_or_new('button_renew') ?></a>
                                        <? endif; ?>
                                    </div>
                                    <div class="col-sm-3">
                                    </div>
                                </span>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
                <div>&nbsp;</div>
                <span><?php
                    //            echo '<pre>'.var_export(array_keys($json), TRUE).'</pre>';
                    //            echo '<pre>'.var_export($json, TRUE).'</pre>';
                    ?></span>
            </section>
<!-- -->
        </div>
        <div>&nbsp;</div>
    </section>
</div>
