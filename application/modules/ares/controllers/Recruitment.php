<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment extends AresController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }


    public function edit($clinical_trial_id)
    {
        $this->only_registrant();

        $data_step = [
            'register' => true
        ];
        {
            $clinicaltrial = \Repository\ClinicalTrial::find($clinical_trial_id);

            $this->validate_owner($clinicaltrial);

            $data_step['clinicaltrial_id'] = $clinical_trial_id;
            $data_step['VD']['currentStep'] = 1;
            $data_step['controller_method'] = 'pesquisador/submissao';
            {
                $data_step['passo_'.PASSO_RECRUTAMENTO] = (new \Steps\Recrutamento())->GetStepData($clinical_trial_id);
                $context = 'recruitment';
            }
            {
//                if ($formData['messages'] != NULL) {
//                    $dataKey = 'passo_'.$passo;
//                    $data['VD']['validationData']['messages'] = $formData['messages'];
//                    foreach ($this->input->post() as $formDataKey => $formDataValue) {
//                        if (in_array($formDataKey, array_keys($data[$dataKey]))) {
//                            $data[$dataKey][$formDataKey] = $formDataValue;
//                        }
//                    }
//                }
            }

            $this->load($clinicaltrial, $data_step);
        }
    }

    private function load(\Repository\ClinicalTrial $clinicaltrial, $data_step = [])
    {
        {
            $submissao = $clinicaltrial->submission();
            {
                $data_step['VD']['genData'] = $this->auxfunctions->loadGenData($this->session->userdata('user')->id, $clinicaltrial->id);
                $data_step['VD']['subData'] = $submissao;
                $data_step['VD']['fields_status'] = ['en' => $submissao->field_status()]; // @json_decode($submissao->fields_status, TRUE);
            }
            {
                $data_step['can_save'] = TRUE;
                $data_step['revisor_field'] = ' readonly disabled ';
                if ($this->session->userdata('user')->isCommon()) {
                    $data_step['has_review_remark'] = $submissao->hasRemarks();
                } else {
                    $data_step['has_review_remark'] = FALSE;
                }
            }
            $data_step['selected_languages'] = $clinicaltrial->selectedLanguages(2);
            {
                $data_step['loggedUser'] = $this->name;
                $data_step['page'] = [
                    'base_url_form-corrigir' => ($data_step['controller_method']) .'/corrigir/',
                    'base_url_form-sumario'  => ($data_step['controller_method']) .'/sumario/',
                ];
            }
        }
        {
            {
                $data['content_left'] = $this->load->view('menu.php', [
                ], TRUE);
            }
            {
                $data_step['languages'] = $clinicaltrial->selectedLanguages(2);
                $data_step['_form_action'] = base_url('pesquisador/submissao/passo/6/'.$clinicaltrial->id);
                $this->set_postback($data_step);
                $submission = \ReviewApp\Submission::where('_deleted', '=', 0)
                    ->where('trial_id', '=', $clinicaltrial->id)
                    ->first();
                $data['content_right'] =
                    $this->load->view('steps.php', [
                        'submission' => $submission,
                        'current_step' => PASSO_RECRUTAMENTO,
                        'fields_status' => $submission->field_status(), // $data_step['VD']['fields_status']['en'],
                    ], TRUE) .
                    '<div class="row">'.
                    '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                    $this->load->view('sheets/recruitment.php', $data_step, TRUE).
                    '</section>'.
                    '</div>';
            }

            define('REBEC_REGISTER', true);

            $this->parser->parse('main', array_merge($data, [
                'usuario' => $this->session->userdata('user'),
            ]));
        }
    }

    public function save($clinicaltrial_id = NULL)
    {
        $this->only_registrant();

        {
            $toValidate = \Repository\ClinicalTrial::find($clinicaltrial_id);
            $this->validate_owner($toValidate);
        }

        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        $fieldlabel = new Helper\FieldLabelLanguage('RecruitmentForm'); $fieldlabel->enableUpdate();
//        echo '<pre>'.var_export(
//                [
//                    __FILE__ => __LINE__,
//                    '$this->input->post()' => $this->input->post(),
//                ],true).'</pre>';
        {
            $stepObject = (new \Steps\Recrutamento());
            $data = $stepObject->SetStepData($clinicaltrial_id, $this->input->post());
            if ($data['messages']) {
                {
                    $flash_errors = [];
                    foreach ($data['messages'] as $message_key => $message_value) {
                        $flash_errors[$message_key] = $fieldlabel->get_or_new('error_'.$message_value);
                    }
                    $this->session->set_flashdata('error', $flash_errors);
                }
//                redirect(base_url('pesquisador/submissao/passo/6/'.$clinicaltrial_id));
                $clinicaltrial = \Repository\ClinicalTrial::find($clinicaltrial_id);
                $this->load($clinicaltrial, []);
                return;
            }
            {
                $clinicaltrial = \Repository\ClinicalTrial::find($clinicaltrial_id);
                $submission = $clinicaltrial->submission();
                $context = 'recruitment';
                foreach($submission->remarks->where('context', $context)->where('status', 'open') as $review_remark) {
                    $review_remark->update([
                        'status' => 'acknowledged',
                    ]);
                }
            }
            $stepObject = (new \Steps\Sumario());
            $stepObject->SetStepData($clinicaltrial_id,
                array_merge(
                    [
                        'current_user_id' => $this->session->userdata('user')->id
                    ],
                    $this->input->post()
                )
            );

        }
        $this->session->set_flashdata('success', $fieldlabel->get_or_new("update_success"));
        redirect('/pesquisador/submissao/sumario/'.$clinicaltrial_id);
    }

}