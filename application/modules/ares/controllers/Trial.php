<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trial extends AresController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function nova_1()
    {
        $this->only_registrant();

        {
            $data['content_left'] = $this->load->view('menu.php', [
            ], TRUE);
        }
        {
            $submissions_by_status = [];
            $submissions = \ReviewApp\Submission::where('_deleted', '=', 0)
                ->where('creator_id', '=', $this->session->userdata('userId'))
                ->orderBy('created', 'desc')
                ->get();
            foreach ($submissions as $submission) {
                $submissions_by_status[$submission->status][$submission->id] = $submission;
            }

            define('__FORM_ACTION__', '/pesquisador/submissao/nova');

            $data['content_right'] =
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $this->load->view('sheets/terms.php', [
                    'controller_method' => 'pesquisador/submissao',
                ], TRUE).
                '</section>'.
                '</div>';
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    public function nova_2()
    {
        $this->only_registrant();

        $fieldlabel = (new Helper\FieldLabelLanguage('TermForm'))->enableUpdate();

        $data_step = [];
        $data_step['controller_method'] = 'pesquisador/submissao';
        if (@$this->input->post('accept_terms') == NULL)
        {
            $this->session->set_flashdata('error', $fieldlabel->get_or_new('accept_terms_is_required'));
            redirect(base_url('pesquisador/submissao/nova'));
            return;
        }
        {
            {
//                if (@$this->input->post('language_en') != NULL) $data_step['VD']['stepData']['selected_languages'][] = 'en';
//                if (@$this->input->post('language_pt-br') != NULL) $data_step['VD']['stepData']['selected_languages'][] = 'pt-br';
//                if (@$this->input->post('language_es') != NULL) $data_step['VD']['stepData']['selected_languages'][] = 'es';
                if (@$this->input->post('language_en') != NULL) $data_step['languages'][] = 'en';
                if (@$this->input->post('language_pt-br') != NULL) $data_step['languages'][] = 'pt-br';
                if (@$this->input->post('language_es') != NULL) $data_step['languages'][] = 'es';
            }
        }

        $this->load($data_step);
    }

    private function load($data_step = NULL)
    {
        {
            $data_step['VD']['genData'] = $this->auxfunctions->loadGenData($this->session->userdata('user')->id, NULL);
            $data_step['loggedUser'] = $this->name;
        }
        $data = [];
        {
            $data['content_left'] = $this->load->view('menu.php', [
            ], TRUE);
        }
        {

            define('__FORM_ACTION__', '/pesquisador/submissao/start');

            $data['content_right'] =
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $this->load->view('sheets/start.php', $data_step, TRUE).
                '</section>'.
                '</div>';
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    public function nova_3()
    {
        $this->only_registrant();

//        $data = [
//            'id' => $clinicaltrial_id,
//            'currentStep' => $passo,
//        ];
//        $data['controller_method'] = 'pesquisador/submissao';
        $fieldlabel = (new Helper\FieldLabelLanguage('InitialTrialForm'))->enableUpdate();

        $stepObject = (new \Steps\DadosIniciais());
        $stepObject->SetUserData(['userId' => $this->session->userdata('user')->id]);
        $data = $stepObject->SetStepData(0, $this->input->post());

        if ($data['messages']) {
//            echo '<pre>'.var_export(
//                    [
//                        __FILE__ => __LINE__,
//                        '$data_dadosIniciais[messages]' => $data_dadosIniciais['messages'],
//                    ],true).'</pre>';
            {
                $flash_errors = [];
                foreach ($data['messages'] as $message_key => $message_value) {
                    $flash_errors[$message_key] = $fieldlabel->get_or_new('error_'.$message_value);
                }
                $this->session->set_flashdata('error', $flash_errors);
            }
            $this->load();
            return;

        } else {

            $clinicaltrial_id = $data['id'];

//            echo '<pre>'.var_export([
//                    __FILE__ => __LINE__,
//                    '$data_dadosIniciais' => $data_dadosIniciais,
//                ],true).'</pre>';

            $stepObject = (new \Steps\Sumario());
            $errors = $stepObject->SetStepData($clinicaltrial_id,
                array_merge(
                    [
                        'current_user_id' => $this->session->userdata('user')->id
                    ],
                    $this->input->post()
                )
            );

//            echo '<pre>'.var_export(array(
//                    '$errors' => $errors,
//                ), true).'</pre>'; die();


            redirect('/pesquisador/submissao/sumario/'.$data['id']);
        }
    }

}