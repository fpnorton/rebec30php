<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Summary extends AresController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function view($clinical_trial_id)
    {
        $this->only_registrant();

        $data_step = [];
        {
            $clinicaltrial = \Repository\ClinicalTrial::find($clinical_trial_id);

            $this->validate_owner($clinicaltrial);

            $submissao = $clinicaltrial->submission();

            $data_step['clinicaltrial_id'] = $clinical_trial_id;
            $data_step['currentStep'] = STEP_SUMARIO;

            $data_step['controller_method'] = 'pesquisador/submissao';

            {
                $key_for_check = \Helper\RedisKeys::KEY_CONFIG_STATUS_CHECK_COMPLETE_TO_SEND;
                if ( ($this->REDIS_SERVER->exists($key_for_check) == 0) or 
                     ($this->REDIS_SERVER->get($key_for_check) == "ON") )
                {
                    $data_step['isPodeSerEnviado'] = $submissao->isPodeSerEnviado();
                }
                else 
                {
                    $data_step['isPodeSerEnviado'] = true;
                }
                $data_step['VD']['genData'] = $this->auxfunctions->loadGenData($this->session->userdata('user')->id, $clinical_trial_id);
                $data_step['VD']['subData'] = $submissao;
                $data_step['VD']['fields_status'] = ['en' => $submissao->field_status()]; // @json_decode($submissao->fields_status, TRUE);
            }
//            {
//                echo '<pre>' . var_export([
//                        '$data' => array_keys($data),
//                    ], true) . '</pre>';
//            }
        }
        $data_step['page'] = [
            'base_url_form-corrigir' => ($data_step['controller_method']) .'/corrigir/',
            'base_url_form-sumario'  => ($data_step['controller_method']) .'/sumario/',
        ];
        $data_step['loggedUser'] = $this->name;
        $data_step['usuario'] = $this->session->userdata('user');
        $data_step['postback'] = '_postback='.$this->get_postback();

        $data = [];
        {
            $data['content_left'] = $this->load->view('menu.php', [
            ], TRUE);
        }
        {
            $submission = \ReviewApp\Submission::where('_deleted', '=', 0)
                ->where('trial_id', '=', $clinical_trial_id)
                ->first();
            $data['content_right'] =
                $this->load->view('steps.php', [
                    'submission' => $submission,
                    'current_step' => PASSO_SUMARIO,
                    'fields_status' => $submission->field_status(), // $data_step['VD']['fields_status']['en'],
                ], TRUE) .
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $this->load->view('summary.php', $data_step, TRUE).
                '</section>'.
                '</div>';
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

}