<?php

use Chaos\Versioner;
use Picolo\Submit;

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends AresController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function index()
    {
        $this->only_registrant();

        $data = [];
        {
            $data['content_left'] = $this->load->view('menu.php', [
            ], TRUE);
        }
        {
            $submissions_by_status = [];
            $submissions = \ReviewApp\Submission::where('_deleted', '=', 0)
                ->where('creator_id', '=', $this->session->userdata('userId'))
                ->orderBy('created', 'desc')
                ->get();
            foreach ($submissions as $submission) {
                $submissions_by_status[$submission->status][$submission->id] = $submission;
            }
            $data['content_right'] = $this->load->view('main_list.php', [
                'submissions_by_status' => $submissions_by_status,
                'submissions_statuses' =>  array_keys($submissions_by_status),
            ], TRUE);
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    private function clonar_contato(\Repository\Contact $contact) {
        $contact_id = $contact->id;
        $contact = \Repository\Contact::where('id','=', $contact_id)->first();
        $data = $contact->withHidden(['creator_id'])->toArray();
        $data['contact_origin_id'] = $contact_id;
        $contact = new \Repository\Contact($data);
        $contact->save();
        return $contact->id;
    }

    public function send($clinicaltrial_id)
    {
        $this->only_registrant();

        {
            $toValidate = \Repository\ClinicalTrial::find($clinicaltrial_id);
            $this->validate_owner($toValidate);
        }

        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        $fieldlabel = new Helper\FieldLabelLanguage('SummaryForm'); $fieldlabel->enableUpdate();
        $submission = NULL;
        $clinicaltrial = NULL;
        {
            if ($this->session->userdata('user') == NULL) {
                // error_log(var_export([__FILE__ => __LINE__], TRUE));
                redirect("welcome");
                return;
            }
            if ($this->session->userdata('user')->isCommon() == false) {
                $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
                // error_log(var_export([__FILE__ => __LINE__], TRUE));
                redirect("/pesquisador");
                return;
            }
            $submission = \ReviewApp\Submission::where('_deleted', '=', 0)
                ->where('trial_id', '=', $clinicaltrial_id)
                ->first();
            $clinicaltrial = \Repository\ClinicalTrial::where('_deleted', '=', 0)
                ->where('id', '=', $clinicaltrial_id)
                ->first();

            $this->validate_owner($clinicaltrial);

            if ($submission->isDraft() == false) {
                $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
                // error_log(var_export([__FILE__ => __LINE__], TRUE));
                redirect("/pesquisador/submissao/sumario/".$clinicaltrial_id);
                return;
            }
        }
        {
            $key_for_check = \Helper\RedisKeys::KEY_CONFIG_STATUS_CHECK_COMPLETE_TO_SEND;
            if ( ($this->REDIS_SERVER->exists($key_for_check) == 0) or 
                 ($this->REDIS_SERVER->get($key_for_check) == "ON") )
            {
                if ($submission->isPodeSerEnviado() == false) return;
            }


            { // VERSIONER
                $version = [
                ];
                foreach (
                    [
                        '01-trial-identification' =>
                            (new \Steps\Identificacao())->GetStepData($clinicaltrial_id),
                        '02-attachment' =>
                            (new \Steps\Anexos())->GetStepData($clinicaltrial_id),
                        '03-sponsors' =>
                            (new \Steps\Patrocinadores())->GetStepData($clinicaltrial_id),
                        '04-health-condition' =>
                            (new \Steps\CondicoesSaude())->GetStepData($clinicaltrial_id),
                        '05-interventions' =>
                            (new \Steps\Intervencao())->GetStepData($clinicaltrial_id),
                        '06-recruitment' =>
                            (new \Steps\Recrutamento())->GetStepData($clinicaltrial_id),
                        '07-study-type' =>
                            (new \Steps\DesenhoEstudo())->GetStepData($clinicaltrial_id),
                        '08-outcomes' =>
                            (new \Steps\Desfecho())->GetStepData($clinicaltrial_id),
                        '09-contacts' =>
                            (new \Steps\Contatos())->GetStepData($clinicaltrial_id),
                        '10-summary-results' =>
                            (new \Steps\ResumoDeResultados())->GetStepData($clinicaltrial_id),
                        '11-ipd-sharing statement' =>
                            (new \Steps\TermoDeCompartilhamento())->GetStepData($clinicaltrial_id),
                    ] as $context => $data) {
                    foreach ($data as $key => $value) {
                        if ($key == 'selected_languages') continue;
                        if (is_array($value)) {
                            $version[$context.':'.$key] = implode(';', $value);
                        } else {
                            $version[$context.':'.$key] = $value;
                        }
                    }
                }
                $sequence_number =
                    (Versioner::where('trial_id', '=', $clinicaltrial_id)->count() == 0) ? 1 :
                        (Versioner::where('trial_id', '=', $clinicaltrial_id)->max('sequence_number') + 1);

                (new Versioner([
                    'trial_id' => $clinicaltrial_id,
                    'sequence_number' => $sequence_number,
                    'creator_id' => $this->session->userdata('user')->id,
                    'created' => date('Y-m-d H:i:s'),
                    'snapshot' => json_encode($version),
                ]))->save();
            }

            $submission->update([
                'updated' => date('Y-m-d H:i:s'),
                'updater_id' => $this->session->userdata('user')->id,
                'status' => 'pending',
            ]);
            $clinicaltrial->update([
                'updated' => date('Y-m-d H:i:s'),
            ]);

            {
                foreach ($clinicaltrial->publicContacts as $publicContact) {
                    if ($publicContact->contact == NULL) continue;
                    $publicContact->update([
                        'contact_id' => $this->clonar_contato($publicContact->contact)
                    ]);
                }
                foreach ($clinicaltrial->scientificContacts as $scientificContact) {
                    if ($scientificContact->contact == NULL) continue;
                    $scientificContact->update([
                        'contact_id' => $this->clonar_contato($scientificContact->contact)
                    ]);
                }
                foreach ($clinicaltrial->siteContacts as $siteContact) {
                    if ($siteContact->contact == NULL) continue;
                    $siteContact->update([
                        'contact_id' => $this->clonar_contato($siteContact->contact)
                    ]);
                }
                foreach ($clinicaltrial->ethicsReviewContacts as $ethicsReviewContact) {
                    if ($ethicsReviewContact->contact == NULL) continue;
                    $ethicsReviewContact->update([
                        'contact_id' => $this->clonar_contato($ethicsReviewContact->contact)
                    ]);
                }
            }
    
            // foreach($submission->remarks->where('status', 'open') as $review_remark) {
            //     $review_remark->update([
            //         'status' => 'acknowledged',
            //     ]);
            // }

            (new Submit([
                'submit_date' => date('Y-m-d H:i:s'),
                'trial_id' => $clinicaltrial_id,
            ]))->save();

            $this->session->set_flashdata('success', $genericlabel->get_or_new("sended_success"));
            // error_log(var_export([__FILE__ => __LINE__], TRUE));
            redirect("/pesquisador");
        }
    }

}