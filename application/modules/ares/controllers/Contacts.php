<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends AresController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }


    public function edit($clinical_trial_id)
    {
        $this->only_registrant();

        define('CLINICAL_TRIAL_ID', $clinical_trial_id);

        $data_step = [
            'register' => true
        ];
        {
            $clinicaltrial = \Repository\ClinicalTrial::find($clinical_trial_id);

            $this->validate_owner($clinicaltrial);

            $submissao = $clinicaltrial->submission();

            $data_step['clinicaltrial_id'] = $clinical_trial_id;
            $data_step['VD']['currentStep'] = PASSO_CONTATOS;
            $data_step['controller_method'] = 'pesquisador/submissao';
            {
                $data_step['VD']['genData'] = $this->auxfunctions->loadGenData($this->session->userdata('user')->id, $clinical_trial_id);
                $data_step['VD']['subData'] = $submissao;
                $data_step['VD']['fields_status'] = ['en' => $submissao->field_status()]; // @json_decode($submissao->fields_status, TRUE);
            }
            {
                $data_step['passo_'.PASSO_CONTATOS] = (new \Steps\Contatos())->GetStepData($clinical_trial_id);
                $context = 'contacts';
            }
            {
//                if ($formData['messages'] != NULL) {
//                    $dataKey = 'passo_'.$passo;
//                    $data['VD']['validationData']['messages'] = $formData['messages'];
//                    foreach ($this->input->post() as $formDataKey => $formDataValue) {
//                        if (in_array($formDataKey, array_keys($data[$dataKey]))) {
//                            $data[$dataKey][$formDataKey] = $formDataValue;
//                        }
//                    }
//                }
            }
            {
                $data_step['can_save'] = TRUE;
                $data_step['revisor_field'] = ' readonly disabled ';
                if ($this->session->userdata('user')->isCommon()) {
                    $data_step['has_review_remark'] = $submissao->hasRemarks();
                } else {
                    $data_step['has_review_remark'] = FALSE;
                }
            }
            $data_step['selected_languages'] = $clinicaltrial->selectedLanguages(2);
            {
                $data_step['loggedUser'] = $this->name;
                $data_step['page'] = [
                    'base_url_form-corrigir' => ($data_step['controller_method']) .'/corrigir/',
                    'base_url_form-sumario'  => ($data_step['controller_method']) .'/sumario/',
                ];
//                $this->parser->parse('main_common', [
//                    'breadcrumbs' => $this->load->view('includes/common/_breadcrumbs', $data, TRUE),
//                    'menu' => $this->load->view('includes/common/_menu', $data, TRUE),
//                    'passos' => $this->load->view('content/clinicaltrial/steps', $data, TRUE),
//                    'corpo' => $this->load->view('content/clinicaltrial/'.A_STEP_VIEW[$passo+1], $data, TRUE),
//                ]);
            }

        }
        {
            $data['content_left'] = $this->load->view('menu.php', [
            ], TRUE);
        }
        {
            $data_step['_form_action'] = base_url('pesquisador/submissao/passo/9/'.$clinicaltrial->id);
            $this->set_postback($data_step);
            $submission = \ReviewApp\Submission::where('_deleted', '=', 0)
                ->where('trial_id', '=', $clinical_trial_id)
                ->first();
            $data['content_right'] =
                $this->load->view('steps.php', [
                    'submission' => $submission,
                    'current_step' => PASSO_CONTATOS,
                    'fields_status' => $submission->field_status(), // $data_step['VD']['fields_status']['en'],
                ], TRUE) .
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $this->load->view('sheets/contacts.php', $data_step, TRUE).
                '</section>'.
                '</div>';
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    public function save($clinicaltrial_id = NULL)
    {
        $this->only_registrant();

        {
            $toValidate = \Repository\ClinicalTrial::find($clinicaltrial_id);
            $this->validate_owner($toValidate);
        }

        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        $fieldlabel = new Helper\FieldLabelLanguage('ContactForm'); $fieldlabel->enableUpdate();
//        echo '<pre>'.var_export(
//                [
//                    __FILE__ => __LINE__,
//                    '$this->input->post()' => $this->input->post(),
//                ],true).'</pre>';
        {
            $stepObject = (new \Steps\Contatos());
            $data = $stepObject->SetStepData($clinicaltrial_id, $this->input->post(), $this->session);
            if ($data['messages']) {
                {
                    $flash_errors = [];
                    foreach ($data['messages'] as $message_key => $message_value) {
                        $flash_errors[$message_key] = $fieldlabel->get_or_new('error_'.$message_value);
                    }
                    $this->session->set_flashdata('error', $flash_errors);
                }
                redirect(base_url('pesquisador/submissao/passo/9/'.$clinicaltrial_id));
                return;
            }
            {
                $clinicaltrial = \Repository\ClinicalTrial::find($clinicaltrial_id);
                $submission = $clinicaltrial->submission();
                $context = 'contacts';
                foreach($submission->remarks->where('context', $context)->where('status', 'open') as $review_remark) {
                    $review_remark->update([
                        'status' => 'acknowledged',
                    ]);
                }
            }

            $stepObject = (new \Steps\Sumario());
            $stepObject->SetStepData($clinicaltrial_id,
                array_merge(
                    [
                        'current_user_id' => $this->session->userdata('user')->id
                    ],
                    $this->input->post()
                )
            );

        }

//        echo '<pre>'.var_export([
//                __FILE__ => __LINE__,
//                '$data' => $data,
//                '$this->input->post()' => $this->input->post(),
//            ],true).'</pre>';
        $this->session->set_flashdata('success', $fieldlabel->get_or_new("update_success"));
        redirect('/pesquisador/submissao/sumario/'.$clinicaltrial_id);
    }

}