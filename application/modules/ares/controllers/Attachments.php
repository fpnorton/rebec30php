<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attachments extends AresController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function download($clinical_trial_id, $attachment_id)
    {
        $this->only_registrant(FALSE);

        {
            $data = (new \Steps\Anexos())->GetStepData($clinical_trial_id);
            for ($i = 1; $i <= $data['count_attach']; $i++) { 
                if ($data["ctattachment_set-${i}-id"] != $attachment_id) continue;
                $info = $data["ctattachment_set-${i}-info"];
                header( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
                header( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
                header( "Cache-Control: no-cache, must-revalidate" );
                header( "Pragma: no-cache" );
                header( "Content-type: ".$info['type'] );
                header( "Content-Transfer-Encoding: Binary" );
                header( "Content-Disposition: attachment; filename=".$info['name'] );
                readfile($info['home'].$info['file']);
                // echo '<pre>'.var_export(
                //     [
                //         __FILE__ => __LINE__,
                //         '$info' => $info,
                //     ],true).'</pre>';
            }
        }
    }

    public function edit($clinical_trial_id)
    {
        $this->only_registrant();

        $data_step = [
            'register' => true
        ];
        {
            $clinicaltrial = \Repository\ClinicalTrial::find($clinical_trial_id);

            $this->validate_owner($clinicaltrial);

            $submissao = $clinicaltrial->submission();

            $data_step['clinicaltrial_id'] = $clinical_trial_id;
            $data_step['VD']['currentStep'] = 1;
            $data_step['controller_method'] = 'pesquisador/submissao';
            {
                $data_step['VD']['genData'] = $this->auxfunctions->loadGenData($this->session->userdata('user')->id, $clinical_trial_id);
                $data_step['VD']['subData'] = $submissao;
                $data_step['VD']['fields_status'] = ['en' => $submissao->field_status()]; // @json_decode($submissao->fields_status, TRUE);
            }
            {
                $data_step['passo_'.PASSO_ANEXOS] = (new \Steps\Anexos())->GetStepData($clinical_trial_id);
                    $context = 'attachments';
            }
            {
//                if ($formData['messages'] != NULL) {
//                    $dataKey = 'passo_'.$passo;
//                    $data['VD']['validationData']['messages'] = $formData['messages'];
//                    foreach ($this->input->post() as $formDataKey => $formDataValue) {
//                        if (in_array($formDataKey, array_keys($data[$dataKey]))) {
//                            $data[$dataKey][$formDataKey] = $formDataValue;
//                        }
//                    }
//                }
            }
            {
                $data_step['can_save'] = TRUE;
                $data_step['revisor_field'] = ' readonly disabled ';
                if ($this->session->userdata('user')->isCommon()) {
                    $data_step['has_review_remark'] = $submissao->hasRemarks();
                } else {
                    $data_step['has_review_remark'] = FALSE;
                }
            }
            $data_step['selected_languages'] = $clinicaltrial->selectedLanguages(2);
            {
                $data_step['loggedUser'] = $this->name;
                $data_step['page'] = [
                    'base_url_form-corrigir' => ($data_step['controller_method']) .'/corrigir/',
                    'base_url_form-sumario'  => ($data_step['controller_method']) .'/sumario/',
                ];
//                $this->parser->parse('main_common', [
//                    'breadcrumbs' => $this->load->view('includes/common/_breadcrumbs', $data, TRUE),
//                    'menu' => $this->load->view('includes/common/_menu', $data, TRUE),
//                    'passos' => $this->load->view('content/clinicaltrial/steps', $data, TRUE),
//                    'corpo' => $this->load->view('content/clinicaltrial/'.A_STEP_VIEW[$passo+1], $data, TRUE),
//                ]);
            }

        }
        {
            $data['content_left'] = $this->load->view('menu.php', [
            ], TRUE);
        }
        {
            $data_step['_form_action'] = base_url('pesquisador/submissao/passo/2/'.$clinicaltrial->id);
            $this->set_postback($data_step);
            $submission = \ReviewApp\Submission::where('_deleted', '=', 0)
                ->where('trial_id', '=', $clinical_trial_id)
                ->first();
            $data['content_right'] =
                $this->load->view('steps.php', [
                    'submission' => $submission,
                    'current_step' => PASSO_ANEXOS,
                    'fields_status' => $submission->field_status(), // $data_step['VD']['fields_status']['en'],
                ], TRUE) .
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $this->load->view('sheets/attachments.php', $data_step, TRUE).
                '</section>'.
                '</div>';
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    public function save($clinicaltrial_id = NULL)
    {
        $this->only_registrant();

        {
            $toValidate = \Repository\ClinicalTrial::find($clinicaltrial_id);
            $this->validate_owner($toValidate);
        }

        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        $fieldlabel = new Helper\FieldLabelLanguage('AttachmentForm'); $fieldlabel->enableUpdate();
    //    echo '<pre>'.var_export(
    //            [
    //                __FILE__ => __LINE__,
    //                '$this->input->post()' => $this->input->post(),
    //            ],true).'</pre>';
        {
            $stepObject = (new \Steps\Anexos());
            // $config = [];
            // $config['upload_path'] = './uploads/';
            // $config['allowed_types'] = '*';
            // $this->load->library('upload', $config);
            // $this->upload->initialize($config);
            // if ($this->upload->do_upload('ctattachment_set-1-file') == false) {
            //     echo '<pre>'.var_export(
            //         [
            //             __FILE__ => __LINE__,
            //             '$_FILES' => $_FILES,
            //             '$this->upload->display_errors()' => $this->upload->display_errors(),
            //             '$this->upload->data()' => $this->upload->data(),
            //         ],true).'</pre>';
            // }
            {
                unset($_FILES['ctattachment_set-__prefix__-file']);
                $count_attach = $this->input->post('count_attach');
                for ($i = 1; $i <= $count_attach; $i++) { 

                    $attachment_info = [
                        'name' => $_FILES["ctattachment_set-{$i}-file"]['name'],
                        'type' => $_FILES["ctattachment_set-{$i}-file"]['type'],
                        'size' => $_FILES["ctattachment_set-{$i}-file"]['size'],
                        'file' => sha1(rand().microtime()),
                        'home' => ROOT_DIRECTORY.'/attachments/',
                    ];

                    if ($_FILES["ctattachment_set-{$i}-file"]['size'] == 0) {
                        error_log(implode(' ', [
                            'class' => get_class($this),
                            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
                            'problem' => 'size == zero',
                            '$attachment_info' => @json_encode($attachment_info),
                        ]));
                        continue;
                    }

                    move_uploaded_file(
                        $_FILES["ctattachment_set-{$i}-file"]['tmp_name'], 
                        $attachment_info['home'].$attachment_info['file']
                    );

                    $_POST["ctattachment_set-{$i}-info"] = json_encode($attachment_info);

                    file_put_contents(
                        $attachment_info['home'].$attachment_info['file'].'.nfo', 
                        json_encode($attachment_info)
                    );

                    // if ($this->upload->do_upload('ctattachment_set-'.$i.'-file') == false) {
                    //     echo '<pre>' . var_export([ __FILE__ => __LINE__, '$this->upload->data()' => $this->upload->data(), '$this->upload->display_errors()' => $this->upload->display_errors(), ], true) . '</pre>';
                    // }
                    // $stepObject->addFile();
                }
            }
            $data = $stepObject->SetStepData($clinicaltrial_id, $this->input->post());
            if ($data['messages']) {
                {
                    $flash_errors = [];
                    foreach ($data['messages'] as $message_key => $message_value) {
                        $flash_errors[$message_key] = $fieldlabel->get_or_new('error_'.$message_value);
                    }
                    $this->session->set_flashdata('error', $flash_errors);
                }
                redirect(base_url('pesquisador/submissao/passo/2/'.$clinicaltrial_id));
                return;
            }
            {
                $clinicaltrial = \Repository\ClinicalTrial::find($clinicaltrial_id);
                $submission = $clinicaltrial->submission();
                $context = 'attachments';
                foreach($submission->remarks->where('context', $context)->where('status', 'open') as $review_remark) {
                    $review_remark->update([
                        'status' => 'acknowledged',
                    ]);
                }
            }
            $stepObject = (new \Steps\Sumario());
            $stepObject->SetStepData($clinicaltrial_id,
                array_merge(
                    [
                        'current_user_id' => $this->session->userdata('user')->id
                    ],
                    $this->input->post()
                )
            );

        }

//        echo '<pre>'.var_export([
//                __FILE__ => __LINE__,
//                '$data' => $data,
//                '$this->input->post()' => $this->input->post(),
//            ],true).'</pre>';
        $this->session->set_flashdata('success', $fieldlabel->get_or_new("update_success"));
        redirect('/pesquisador/submissao/sumario/'.$clinicaltrial_id);
    }

}