<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Repository\ClinicalTrial;
use Illuminate\Database\Capsule\Manager as DB;

define('MY_HTMLSPECIALCHARS_DEFAULT_FLAG', (ENT_COMPAT | ENT_HTML401));

function extract_any_date($date) {
    $data = [
        'fail' => FALSE,
        'year' => NULL,
        'month' => NULL,
        'day' => NULL,
        'hour' => NULL,
        'minute' => NULL,
        'second' => NULL,
    ];
    $date = urldecode($date);
    $length = @strlen($date);

    $exploded_by_minus = @explode('-', $date);
    $exploded_by_space = @explode(' ', $date);
    $exploded_by_t = @explode('T', $date);
    $exploded_by_slash = @explode('/', $date);
    $exploded_by_twopoints = @explode(':', $date);

    if (count($exploded_by_t) > 1) {
        $exploded_by_minus = @explode('-', $exploded_by_t[0]);
        $exploded_by_slash = @explode('/', $exploded_by_t[0]);
        $exploded_by_twopoints = @explode(':', $exploded_by_t[1]);
    } elseif (count($exploded_by_space) > 1) {
        $exploded_by_minus = @explode('-', $exploded_by_space[0]);
        $exploded_by_slash = @explode('/', $exploded_by_space[0]);
        $exploded_by_twopoints = @explode(':', $exploded_by_space[1]);
    }

    $boolean_is_iso_date_yyyy_mm_dd = ($length == 10 and count($exploded_by_minus) == 3 and strlen($exploded_by_minus[0]) == 4 and intval($exploded_by_slash[1]) <= 12);
    $boolean_is_iso_date_dd_mm_yyyy = ($length == 10 and count($exploded_by_minus) == 3 and strlen($exploded_by_minus[2]) == 4 and intval($exploded_by_slash[1]) <= 12);
    $boolean_is_iso_date_mm_dd_yyyy = ($length == 10 and count($exploded_by_minus) == 3 and strlen($exploded_by_minus[2]) == 4 and intval($exploded_by_slash[0]) <= 12);

    $boolean_is_british_date = ($length == 10 and count($exploded_by_slash) == 3 and strlen($exploded_by_slash[2]) == 4 and intval($exploded_by_slash[1]) <= 12);
    $boolean_is_english_date = ($length == 10 and count($exploded_by_slash) == 3 and strlen($exploded_by_slash[2]) == 4 and intval($exploded_by_slash[0]) <= 12);
    
    $boolean_is_british_date_yy = ($length == 8 and count($exploded_by_slash) == 3 and strlen($exploded_by_slash[2]) == 2 and intval($exploded_by_slash[1]) <= 12);
    $boolean_is_english_date_yy = ($length == 8 and count($exploded_by_slash) == 3 and strlen($exploded_by_slash[2]) == 2 and intval($exploded_by_slash[0]) <= 12);

    $boolean_is_ddmmyyyy = ($length == 8 and intval(substr($date, 2, 2)) <= 12);
    $boolean_is_mmddyyyy = ($length == 8 and intval(substr($date, 0, 2)) <= 12);

    $boolean_is_mm_yyyy = ($length == 7 and count($exploded_by_minus) == 2);

    $boolean_is_yyyy = ($length == 4 and intval($date) > 0);

    if ($boolean_is_yyyy) {
        $data['year'] = $date;
    } elseif ($boolean_is_mm_yyyy) {
        $data['year'] = $exploded_by_minus[0];
        $data['month'] = $exploded_by_minus[1];
    } elseif ($boolean_is_iso_date_yyyy_mm_dd) {
        $data['year'] = $exploded_by_minus[0];
        $data['month'] = $exploded_by_minus[1];
        $data['day'] = $exploded_by_minus[2];
    } elseif ($boolean_is_iso_date_dd_mm_yyyy) {
        $data['year'] = $exploded_by_minus[2];
        $data['month'] = $exploded_by_minus[1];
        $data['day'] = $exploded_by_minus[0];
    } elseif ($boolean_is_iso_date_mm_dd_yyyy) {
        $data['year'] = $exploded_by_minus[2];
        $data['month'] = $exploded_by_minus[0];
        $data['day'] = $exploded_by_minus[1];
    } elseif ($boolean_is_british_date) {
        $data['year'] = $exploded_by_slash[2];
        $data['month'] = $exploded_by_slash[1];
        $data['day'] = $exploded_by_slash[0];
    } elseif ($boolean_is_british_date_yy) {
        $data['year'] = '20'.$exploded_by_slash[2];
        $data['month'] = $exploded_by_slash[1];
        $data['day'] = $exploded_by_slash[0];
    } elseif ($boolean_is_english_date) {
        $data['year'] = $exploded_by_slash[2];
        $data['month'] = $exploded_by_slash[0];
        $data['day'] = $exploded_by_slash[1];
    } elseif ($boolean_is_english_date_yy) {
        $data['year'] = '20'.$exploded_by_slash[2];
        $data['month'] = $exploded_by_slash[0];
        $data['day'] = $exploded_by_slash[1];
    } elseif ($boolean_is_ddmmyyyy) {
        $data['year'] = substr($date, 4, 4);
        $data['month'] = substr($date, 2, 2);
        $data['day'] = substr($date, 0, 2);
    } elseif ($boolean_is_mmddyyyy) {
        $data['year'] = substr($date, 4, 4);
        $data['month'] = substr($date, 0, 2);
        $data['day'] = substr($date, 2, 2);
    } elseif ($length == 16 and 
        count($exploded_by_space) == 2 and
        count($exploded_by_minus) == 3 and
        count($exploded_by_twopoints) == 2) {
        $data['year'] = $exploded_by_minus[0];
        $data['month'] = $exploded_by_minus[1];
        $data['day'] = $exploded_by_minus[2];
        $data['hour'] = $exploded_by_twopoints[0];
        $data['minute'] = $exploded_by_twopoints[1];
    } elseif ($length == 19 and 
        count($exploded_by_space) == 2 and
        count($exploded_by_minus) == 3 and
        count($exploded_by_twopoints) == 3 and
        strlen($exploded_by_minus[0]) == 4 and
        strlen($exploded_by_minus[1]) == 2 and
        strlen($exploded_by_minus[2]) == 2) {
        $data['year'] = $exploded_by_minus[0];
        $data['month'] = $exploded_by_minus[1];
        $data['day'] = $exploded_by_minus[2];
        $data['hour'] = $exploded_by_twopoints[0];
        $data['minute'] = $exploded_by_twopoints[1];
        $data['second'] = $exploded_by_twopoints[2];
    } elseif ($length == 19 and 
        count($exploded_by_space) == 2 and
        count($exploded_by_slash) == 3 and
        count($exploded_by_twopoints) == 3 and
        strlen($exploded_by_slash[0]) == 2 and
        strlen($exploded_by_slash[1]) == 2 and
        strlen($exploded_by_slash[2]) == 4) {
        $data['year'] = $exploded_by_slash[2];
        $data['month'] = $exploded_by_slash[1];
        $data['day'] = $exploded_by_slash[0];
        $data['hour'] = $exploded_by_twopoints[0];
        $data['minute'] = $exploded_by_twopoints[1];
        $data['second'] = $exploded_by_twopoints[2];
    } else {
        $data['fail'] = TRUE;
    }
    // $data['extra']['date'] = json_encode($date);
    // $data['extra']['strlen date'] = json_encode(@strlen($date));
    // $data['extra']['exploded_by_minus'] = json_encode($exploded_by_minus);
    // $data['extra']['exploded_by_slash'] = json_encode($exploded_by_slash);
    // $data['extra']['exploded_by_space'] = json_encode($exploded_by_space);
    // $data['extra']['exploded_by_t'] = json_encode($exploded_by_t);
    // $data['extra']['exploded_by_twopoints'] = json_encode($exploded_by_twopoints);

    // echo '>'.$date.'< '.($data['fail'] ? 'fail' : implode('_', [
    //     $data['year'],
    //     $data['month'],
    //     $data['day']
    // ]))."\n";
    return $data;
}

function MY_htmlspecialchars($string, $flags = MY_HTMLSPECIALCHARS_DEFAULT_FLAG, $encoding = 'UTF-8', $double_encode = true) {
    $_tmp = "";
    for ($i=0; $i < strlen($string); $i++) { 
        $to_end = strlen($string) - $i;
        if ($string[$i] == '<') {
            $_tmp .= '&amp;lt;';
        } elseif ($string[$i] == '>') {
            $_tmp .= '&amp;gt;';
        } elseif ($string[$i] == '"') {
            $_tmp .= '&amp;quot;';
        } elseif ($string[$i] == "'") {
            $_tmp .= '&amp;apos;';
        } elseif ($to_end > 4 and 
            $string[$i] == '&' and 
            (($string[$i+1].$string[$i+2].$string[$i+3].$string[$i+4]) != 'amp;')) {
            $_tmp .= '&amp;amp;';
        } else {
            $_tmp .= $string[$i];
        }
    }
    // $_tmp = strtr($string, [
    //     '<' => '&amp;lt;',
    //     '>' => '&amp;gt;',
    //     '&' => '&amp;amp;',
    // ]);
    return $_tmp;
}
function MY_transform_any_date_to_british_date($any_date) {
    $_tmp = extract_any_date($any_date);
    if ($_tmp['fail']) return $any_date;
    $_slashed_date = $_tmp['day'].'/'.$_tmp['month'].'/'.$_tmp['year'];
    // echo var_export($_slashed_date, TRUE)." <= ".$any_date."\n";
    return $_slashed_date;
}

class Ictrp extends HefestoController {
	
	public function __construct()
	{
        parent::__construct();
        if ($this->template) $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
	}
    
    public function test() 
    {   // 2020-10-10 55:88:23
        $date = urldecode($this->input->post('test'));
        $messages['date'] = json_encode($date);
        $messages['strlen date'] = json_encode(@strlen($date));

        $exploded_by_minus = @explode('-', $date);
        $exploded_by_space = @explode(' ', $date);
        $exploded_by_slash = @explode('/', $date);
        $exploded_by_t = @explode('T', $date);
        $exploded_by_twopoints = @explode(':', $date);

        if (count($exploded_by_t) > 1) {
            $exploded_by_minus = @explode('-', $exploded_by_t[0]);
            $exploded_by_slash = @explode('/', $exploded_by_space[0]);
            $exploded_by_twopoints = @explode(':', $exploded_by_t[1]);
        } elseif (count($exploded_by_space) > 1) {
            $exploded_by_minus = @explode('-', $exploded_by_space[0]);
            $exploded_by_slash = @explode('/', $exploded_by_space[0]);
            $exploded_by_twopoints = @explode(':', $exploded_by_space[1]);
        }

        $messages['exploded_by_minus'] = json_encode($exploded_by_minus);
        $messages['exploded_by_slash'] = json_encode($exploded_by_slash);
        $messages['exploded_by_space'] = json_encode($exploded_by_space);
        $messages['exploded_by_t'] = json_encode($exploded_by_t);
        $messages['exploded_by_twopoints'] = json_encode($exploded_by_twopoints);
    
        $messages['extract_any_date'] = json_encode(extract_any_date($date));

        $messages['MY_transform_any_date_to_british_date'] = json_encode(MY_transform_any_date_to_british_date($date));

        $this->session->set_flashdata('pre', $messages);
        redirect('xml_ictrp');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('xml_ictrp_view');
	}

    public function create_xml_ictrp_by_rbr() 
    {
        $rbr = $this->input->post('rbr');

        $fossil = \Fossil\Fossil::where('is_most_recent', '=', 1)->where('display_text', 'LIKE', $rbr.' %')->get()->first();

        $this->create_xml_ictrp_by_fossil($fossil);
    }

    public function create_xml_ictrp_by_trial_id() 
    {
        $trial_id = intval($this->input->post('trialId'));

        $fossil = \Fossil\Fossil::where('is_most_recent', '=', 1)->where('object_id','=',$trial_id)->get()->first();

        $this->create_xml_ictrp_by_fossil($fossil);
    }

    private function create_xml_ictrp_by_fossil(\Fossil\Fossil $fossil) 
    {
        {
            $xml = new SimpleXMLElement('<root/>');
            $trials = $xml->addChild('trials');
            $trial = $trials->addChild('trial');
            $messages['json'] = json_encode(json_decode($fossil->serialized, true, 512, JSON_UNESCAPED_UNICODE), JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
            $this->createTrialXml(
                $fossil->id, 
                $fossil->object_id, 
                json_decode($fossil->serialized, true, 512, JSON_UNESCAPED_UNICODE), 
                $trial
            );
            $dom = dom_import_simplexml($xml)->ownerDocument;
            $dom->formatOutput = true;
            $messages['xml'] = $dom->saveXML();
        }

        $this->session->set_flashdata('pre', $messages);
        redirect('xml_ictrp');
    }

    public function downloadallxmlictrp()
    {
        $files = DB::table('fossil_fossil')
            ->select('id', 'creation')
            ->where('is_most_recent', '=', 1)
            ->orderBy('creation')
            ->get();

        $SERVER_PROTOCOL_AND_NAME = implode('',[
            $_SERVER['REQUEST_SCHEME'],
            '://',
            $_SERVER['HTTP_HOST'],
            ($_SERVER['SERVER_PORT'] == '80' ? '' : ':'.$_SERVER['SERVER_PORT']),
        ]);

        $tmp_name = tempnam(sys_get_temp_dir(), 'fossil-');
        file_put_contents($tmp_name, "<?xml version=\"1.0\"?>\n<root>\n<trials>\n", FILE_APPEND);
        foreach ($files as $file) {
            $fossil_filename = ROOT_DIRECTORY.'/fosseis/'.$file->id;
            if (file_exists($fossil_filename) == false) continue;
            $xml = file_get_contents($fossil_filename);
            $xml = str_replace("<?xml version=\"1.0\"?>\n", "", $xml);
            $xml = str_replace("__SITE__", $SERVER_PROTOCOL_AND_NAME, $xml);
            file_put_contents($tmp_name, $xml, FILE_APPEND);
        }
        file_put_contents($tmp_name, "\n</trials>\n</root>", FILE_APPEND);
        force_download('RBR-ictrp-ALL.xml', file_get_contents($tmp_name), TRUE);
        redirect('xml_ictrp');
    }

    public function downloadtrialxmlictrp_by_periodo($p_from, $p_to, $format = 0) {
        $dates = [
            date_parse_from_format('Y-m-d', $p_from),
            date_parse_from_format('Y-m-d', $p_to),
        ];
        foreach ($dates as $date) {
            if (($date['warning_count'] > 0) || ($date['error_count'] > 0)) {
                echo 'DATE FORMAT ERROR yyyy-mmm-dd';
                return;
            }
        }

        $from = sprintf('%d-%0d-%0d', $dates[0]['year'], $dates[0]['month'], $dates[0]['day']);
        $to   = sprintf('%d-%0d-%0d', $dates[1]['year'], $dates[1]['month'], $dates[1]['day']);

        $data = \Fossil\Fossil::where('is_most_recent', '=', 1)->whereBetween('creation', [date($from), date($to)])->get();

        {
            $xml = new SimpleXMLElement('<root/>');
            $trials = $xml->addChild('trials');
//            $trials->addAttribute('total', count($data));
//            $trials->addAttribute('created_from', $p_from);
//            $trials->addAttribute('created_to', $p_to);
            foreach ($data as $fossil)
            {
                $trial = $trials->addChild('trial');
                $array = json_decode ($fossil->serialized, true);
                $this->createTrialXml($fossil->id, $fossil->object_id, $array, $trial);
            }
            $dom = dom_import_simplexml($xml)->ownerDocument;
            $dom->formatOutput = true;
            if ($format == 2) {
                ?><pre><? echo var_export([__FILE__ => __LINE__, htmlentities($dom->saveXML())], TRUE); ?></pre><?
            } else {
                force_download(sprintf('RBR-ictrp_%s_%s.xml', $p_from, $p_to), $dom->saveXML(), TRUE);
                redirect('xml_ictrp');
            }

        }
    }

    public function downloadtrialxmlictrp_by_rbr($rbr, $format = 0)
    {
        $fossil = \Fossil\Fossil::where('is_most_recent', '=', 1)->where('display_text', 'LIKE', $rbr.' %')->get()->first();
        if ($fossil != NULL) return $this->downloadtrialxmlictrp($fossil, $format);
        redirect('xml_ictrp');
    }

    public function downloadtrialxmlictrp_by_trial_id($trial_id, $format = 0)
    {
        $fossil = \Fossil\Fossil::where('is_most_recent', '=', 1)->where('object_id','=',$trial_id)->get()->first();
        if ($fossil != NULL) return $this->downloadtrialxmlictrp($fossil, $format);
        redirect('xml_ictrp');
    }

	private function downloadtrialxmlictrp(\Fossil\Fossil $fossil, $format = 0)
	{		
		$array = json_decode ($fossil->serialized, true);
		if ($format == 1) {
		    ?><pre><? echo var_export([__FILE__ => __LINE__, $array], TRUE); ?></pre><?
        }
        if ($format == 0 or $format == 2)
        {
            $xml = new SimpleXMLElement('<root/>');
            $trials = $xml->addChild('trials');
            $trial = $trials->addChild('trial');
            $this->createTrialXml($fossil->id, $fossil->object_id, $array, $trial);
            $dom = dom_import_simplexml($xml)->ownerDocument;
            $dom->formatOutput = true;
            if ($format == 2) {
                ?><pre><? echo var_export([__FILE__ => __LINE__, htmlentities($dom->saveXML())], TRUE); ?></pre><?
            } else {
                $clinicalTrial = ClinicalTrial::where('id', '=', $fossil->object_id)->first();
                force_download(sprintf('%s-ictrp.xml', $clinicalTrial->trial_id), html_entity_decode($dom->saveXML()), TRUE);
                redirect('xml_ictrp');
            }
        }
	}

    private function get_british_date_max($date) {
        $tmp = explode('-', $date);
        if (count($tmp) != 3) return NULL;
        return $tmp[0] . '-' . $tmp[1] . '-' . $tmp[2] . ' 23:59:59';
    }

	private function get_iso_date_min($date) {
	    $tmp = explode('-', $date);
	    if (count($tmp) != 3) return NULL;
	    return $tmp[0] . '-' . $tmp[1] . '-' . $tmp[2] . ' 00:00:00';
    }

    private function get_iso_date_max($date) {
        $tmp = explode('-', $date);
        if (count($tmp) != 3) return NULL;
        return $tmp[0] . '-' . $tmp[1] . '-' . $tmp[2] . ' 23:59:59';
    }

    public function downloadtrialxmlictrpfiltered()
    {
        $parameters = [];
        {
            $parameters = [
                'since' => $this->get_iso_date_min($this->input->get('since')),
                'min-date' => $this->get_iso_date_min($this->input->get('min-date')),
                'max-date' => $this->get_iso_date_max($this->input->get('max-date')),
            ];
//echo '<pre>'.var_export([ __FILE__ => __LINE__, $parameters ], true).'</pre>';
        }
        $data = \Fossil\Fossil::query();
        if ($parameters['since'] != NULL) {
            $data = $data->where('creation', '>=', $parameters['since']);
        }
        if ($parameters['date'] != NULL) {
            $data = $data->where('creation', '=', $parameters['date']);
        }
        if ($parameters['min-date'] != NULL and $parameters['max-date'] != NULL) {
            $data = $data->where('creation', '>=', $parameters['min-date'])->where('creation', '<=', $parameters['max-date']);
        }

        $collection = $data->get();

        $xml = new SimpleXMLElement('<root/>');
        $trials = $xml->addChild('trials');
        foreach ($collection as $fossil)
        {
            $trial = $trials->addChild('trial');
            $array = json_decode ($fossil->serialized, true);
            $this->createTrialXml($fossil->id, $fossil->object_id, $array, $trial);
        }
        $dom = dom_import_simplexml($xml)->ownerDocument;
        $dom->formatOutput = true;
        force_download('RBR-ictrp.xml', html_entity_decode($dom->saveXML()), TRUE);
        redirect('xml_ictrp');
    }


    protected function createTrialXml($id, $clinicaltrial_id, $array, SimpleXMLElement &$trial)
	{
		//main sector
        $main = $trial->addChild('main');
        
        if (is_null($array['trial_id'])) {
            $ClinicalTrial = ClinicalTrial::where('id', '=', $clinicaltrial_id)->first();
            $array['trial_id'] = $ClinicalTrial->trial_id;
        }
        if (is_null($array['date_registration'])) { // data de registro 
            $revision_1 = \Fossil\Fossil::where('content_type_id', '=', 24)
                ->where('revision_sequential', '=', 1)
                ->where('object_id', '=', $clinicaltrial_id)
                ->first();
            
            $array['date_registration'] = $revision_1->creation;
        }

		$main->addChild('trial_id',MY_htmlspecialchars($array['trial_id'], ENT_QUOTES, 'utf-8'));
		$main->addChild('utrn',MY_htmlspecialchars($array['utrn_number'], ENT_QUOTES, 'utf-8'));
		$main->addChild('reg_name',MY_htmlspecialchars($this->config->item('system_name'), ENT_QUOTES, 'utf-8'));
        $main->addChild('date_registration',MY_htmlspecialchars(
            MY_transform_any_date_to_british_date($array['date_registration']), ENT_QUOTES, 'utf-8')
        );
        
		$main->addChild('primary_sponsor',MY_htmlspecialchars($array['primary_sponsor']['name'], ENT_QUOTES, 'utf-8'));
		$main->addChild('public_title',MY_htmlspecialchars($array['public_title'], ENT_QUOTES, 'utf-8'));
		$main->addChild('acronym',MY_htmlspecialchars($array['acronym_display'], ENT_QUOTES, 'utf-8'));
		$main->addChild('scientific_title',MY_htmlspecialchars($array['scientific_title'], ENT_QUOTES, 'utf-8'));
		$main->addChild('scientific_acronym',MY_htmlspecialchars($array['scientific_acronym_display'], ENT_QUOTES, 'utf-8'));
		if (array_key_exists('date_enrollment_start', $array)) 
		{
			$main->addChild('date_enrolment', MY_htmlspecialchars(
                MY_transform_any_date_to_british_date($array['date_enrollment_start']), 
                ENT_QUOTES, 'utf-8'
            ));
		}
		else
		{
			$main->addChild('date_enrolment', '');
		}
        {
            $type_enrolment = 'anticipated';
            if($array['enrollment_start_actual'] != '')
            {
                $type_enrolment = 'actual';
            }
            $main->addChild('type_enrolment',MY_htmlspecialchars($type_enrolment, ENT_QUOTES, 'utf-8'));
        }
		$main->addChild('target_size',MY_htmlspecialchars($array['target_sample_size'], ENT_QUOTES, 'utf-8'));
		$main->addChild('recruitment_status',MY_htmlspecialchars($array['recruitment_status']['label'], ENT_QUOTES, 'utf-8'));

        if ($_SERVER['HTTP_HOST']) {
            $main->addChild('url',MY_htmlspecialchars(base_url('/rg/'. $array['trial_id']), ENT_QUOTES, 'utf-8'));
        } else {
            // $main->addChild('url',MY_htmlspecialchars('__SITE__/xml_ictrp/downloadxmlictrp/rbr/'. $array['trial_id'], ENT_QUOTES, 'utf-8'));
            // $main->addChild('url',MY_htmlspecialchars('__SITE__/xml_ictrp/downloadxmlictrp/'. $clinicaltrial_id, ENT_QUOTES, 'utf-8'));
           $main->addChild('url',MY_htmlspecialchars('__SITE__/rg/'. $array['trial_id'], ENT_QUOTES, 'utf-8'));
        }
        {
            $study_type = 'Intervention';
            if($array['is_observational'] == 1)
            {
                $study_type='Observational';
            }
            $main->addChild('study_type',MY_htmlspecialchars($study_type, ENT_QUOTES, 'utf-8'));
        }
		$main->addChild('study_design',MY_htmlspecialchars($array['study_design'], ENT_QUOTES, 'utf-8'));
		$main->addChild('phase',MY_htmlspecialchars($array['phase']['label'], ENT_QUOTES, 'utf-8'));
		$main->addChild('hc_freetext',MY_htmlspecialchars($array['hc_freetext'], ENT_QUOTES, 'utf-8'));
		$main->addChild('i_freetext',MY_htmlspecialchars($array['i_freetext'], ENT_QUOTES, 'utf-8'));

		//results_actual_enrolment
        $main->addChild('results_actual_enrolment',MY_htmlspecialchars($array['results_actual_enrolment'], ENT_QUOTES, 'utf-8'));
        //results_date_completed
        $main->addChild('results_date_completed',MY_htmlspecialchars(MY_transform_any_date_to_british_date($array['results_date_completed']), ENT_QUOTES, 'utf-8'));
        //results_url_link
        $main->addChild('results_url_link',MY_htmlspecialchars($array['results_url_link'], ENT_QUOTES, 'utf-8'));
        //results_summary
        $main->addChild('results_summary',MY_htmlspecialchars($array['results_summary'], ENT_QUOTES, 'utf-8'));
        //results_date_posted
        $main->addChild('results_date_posted',MY_htmlspecialchars(MY_transform_any_date_to_british_date($array['results_date_posted']), ENT_QUOTES, 'utf-8'));
        //results_date_first_publication
        $main->addChild('results_date_first_publication',MY_htmlspecialchars(MY_transform_any_date_to_british_date($array['results_date_first_publication']), ENT_QUOTES, 'utf-8'));
        //results_baseline_char
        $main->addChild('results_baseline_char',MY_htmlspecialchars($array['results_baseline_char'], ENT_QUOTES, 'utf-8'));
        //results_participant_flow
        $main->addChild('results_participant_flow',MY_htmlspecialchars($array['results_participant_flow'], ENT_QUOTES, 'utf-8'));
        //results_adverse_events
        $main->addChild('results_adverse_events',MY_htmlspecialchars($array['results_adverse_events'], ENT_QUOTES, 'utf-8'));
        //results_outcome_measures
        $main->addChild('results_outcome_measures',MY_htmlspecialchars($array['results_outcome_measure'], ENT_QUOTES, 'utf-8'));
        //results_url_protocol
        $main->addChild('results_url_protocol',MY_htmlspecialchars($array['results_url_protocol'], ENT_QUOTES, 'utf-8'));
        //results_IPD_plan
        $main->addChild('results_IPD_plan',MY_htmlspecialchars($array['results_IPD_plan'], ENT_QUOTES, 'utf-8'));
        //results_IPD_description
        $main->addChild('results_IPD_description',MY_htmlspecialchars($array['results_IPD_description'], ENT_QUOTES, 'utf-8'));

        {   //Contacts
            $contacts = $trial->addChild('contacts');
        }
        {   //Public Contacts
            foreach ($array['public_contact'] as $arrPublic)
            {
                $public_contact = $contacts->addChild('contact');
                $public_contact->addChild('type', 'public');
                $public_contact->addChild('firstname',MY_htmlspecialchars($arrPublic['firstname'], ENT_QUOTES, 'utf-8'));
                $public_contact->addChild('middlename',MY_htmlspecialchars($arrPublic['middlename'], ENT_QUOTES, 'utf-8'));
                $public_contact->addChild('lastname',MY_htmlspecialchars($arrPublic['lastname'], ENT_QUOTES, 'utf-8'));
                $public_contact->addChild('address',MY_htmlspecialchars($arrPublic['address'], ENT_QUOTES, 'utf-8'));
                $public_contact->addChild('city',MY_htmlspecialchars($arrPublic['city'], ENT_QUOTES, 'utf-8'));
                $public_contact->addChild('country1',MY_htmlspecialchars($arrPublic['country']['description'], ENT_QUOTES, 'utf-8'));
                $public_contact->addChild('zip',MY_htmlspecialchars($arrPublic['zip'], ENT_QUOTES, 'utf-8'));
                $public_contact->addChild('telephone',MY_htmlspecialchars($arrPublic['telephone'], ENT_QUOTES, 'utf-8'));
                $public_contact->addChild('email',MY_htmlspecialchars($arrPublic['email'], ENT_QUOTES, 'utf-8'));
                $public_contact->addChild('affiliation',MY_htmlspecialchars($arrPublic['affiliation']['name'], ENT_QUOTES, 'utf-8'));
            }
        }
        {   //Scientific Contact
            foreach ($array['scientific_contact'] as $arrScientific)
            {
                $scientific_contact = $contacts->addChild('contact');
                $scientific_contact->addChild('type', 'scientific');
                $scientific_contact->addChild('firstname',MY_htmlspecialchars($arrScientific['firstname'], ENT_QUOTES, 'utf-8'));
                $scientific_contact->addChild('middlename',MY_htmlspecialchars($arrScientific['middlename'], ENT_QUOTES, 'utf-8'));
                $scientific_contact->addChild('lastname',MY_htmlspecialchars($arrScientific['lastname'], ENT_QUOTES, 'utf-8'));
                $scientific_contact->addChild('address',MY_htmlspecialchars($arrScientific['address'], ENT_QUOTES, 'utf-8'));
                $scientific_contact->addChild('city',MY_htmlspecialchars($arrScientific['city'], ENT_QUOTES, 'utf-8'));
                $scientific_contact->addChild('country1',MY_htmlspecialchars($arrScientific['country']['description'], ENT_QUOTES, 'utf-8'));
                $scientific_contact->addChild('zip',MY_htmlspecialchars($arrScientific['zip'], ENT_QUOTES, 'utf-8'));
                $scientific_contact->addChild('telephone',MY_htmlspecialchars($arrScientific['telephone'], ENT_QUOTES, 'utf-8'));
                $scientific_contact->addChild('email',MY_htmlspecialchars($arrScientific['email'], ENT_QUOTES, 'utf-8'));
                $scientific_contact->addChild('affiliation',MY_htmlspecialchars($arrScientific['affiliation']['name'], ENT_QUOTES, 'utf-8'));
            }
        }
        {   //Countries
            $countries = $trial->addChild('countries');
            foreach ($array['recruitment_country'] as $arrCountry)
            {
                $countries->addChild('country2',MY_htmlspecialchars($arrCountry['description'], ENT_QUOTES, 'utf-8'));
            }
        }
        {   //Criteria
            $criteria = $trial->addChild('criteria');
            $criteria->addChild('inclusion_criteria',MY_htmlspecialchars($array['inclusion_criteria'], ENT_QUOTES, 'utf-8'));
            $agemin = $array['agemin_value'];
            if($array['agemin_unit'] != '-')
            {
                $agemin .= $array['agemin_unit'];
            }
            $criteria->addChild('agemin',MY_htmlspecialchars($agemin, ENT_QUOTES, 'utf-8'));
            $agemax = $array['agemax_value'];
            if($array['agemax_unit'] != '-')
            {
                $agemax .= $array['agemax_unit'];
            }
            $criteria->addChild('agemax',MY_htmlspecialchars($agemax, ENT_QUOTES, 'utf-8'));

            $criteria->addChild('gender',MY_htmlspecialchars($array['gender'], ENT_QUOTES, 'utf-8'));
            $criteria->addChild('exclusion_criteria',MY_htmlspecialchars($array['exclusion_criteria'], ENT_QUOTES, 'utf-8'));
        }
        {   //Health Condition Code
            $health_condition_codes = $trial->addChild('health_condition_code');
            foreach ($array['hc_keyword'] as $arrHc_Code)
            {
                $health_condition_codes->addChild('hc_code',MY_htmlspecialchars($arrHc_Code['code'], ENT_QUOTES, 'utf-8'));
            }
        }
        {   //Intervention Code
            $intervention_codes = $trial->addChild('intervention_code');
            foreach ($array['i_code'] as $arrI_Code)
            {
                $intervention_codes->addChild('i_code',MY_htmlspecialchars($arrI_Code['label'], ENT_QUOTES, 'utf-8'));
            }
        }
        {   //Intervention Keyword
            $intervention_keyword = $trial->addChild('intervention_keyword');
            foreach ($array['intervention_keyword'] as $arrIntKeyword)
            {
                $intervention_keyword->addChild('i_keyword',MY_htmlspecialchars($arrIntKeyword['code'], ENT_QUOTES, 'utf-8'));
            }
        }
        {   //Primary Outcome
            $primary_outcome = $trial->addChild('primary_outcome');
            foreach ($array['primary_outcomes'] as $arrPrimaryOutcome)
            {
                $primary_outcome->addChild('prim_outcome',MY_htmlspecialchars($arrPrimaryOutcome['description'], ENT_QUOTES, 'utf-8'));
            }
        }
        {   //Secondary Outcome
            $secondary_outcome = $trial->addChild('secondary_outcome');
            foreach ($array['secondary_outcomes'] as $arrSecondaryOutcome)
            {
                //echoMY_htmlspecialchars($arrSecondaryOutcome['description'], ENT_QUOTES, 'utf-8');
                $secondary_outcome->addChild('sec_outcome',MY_htmlspecialchars($arrSecondaryOutcome['description'], ENT_QUOTES, 'utf-8'));
            }
        }
        {   //Secondary Sponsor
            $secondary_sponsor = $trial->addChild('secondary_sponsor');
            foreach ($array['secondary_sponsors'] as $arrSecondarySponsor)
            {
                $secondary_sponsor->addChild('sponsor_name',MY_htmlspecialchars($arrSecondarySponsor['institution']['name'], ENT_QUOTES, 'utf-8'));
                $secondary_sponsor->addChild('sponsor_name', '');
            }
        }
        {   //Secondary Ids
            $secondary_ids = $trial->addChild('secondary_ids');
            foreach ($array['trial_number'] as $arrSecId)
            {
                $secondary_id = $trial->addChild('secondary_id');
                $secondary_id->addChild('sec_id',MY_htmlspecialchars($arrSecId['id_number'], ENT_QUOTES, 'utf-8'));
                $secondary_id->addChild('issuing_authority',MY_htmlspecialchars($arrSecId['issuing_authority'], ENT_QUOTES, 'utf-8'));

                $secondary_id = $trial->addChild('secondary_id');
                $secondary_id->addChild('sec_id', '');
                $secondary_id->addChild('issuing_authority', '');
            }
        }
        {   //Source Support
            $source_support = $trial->addChild('source_support');
            foreach ($array['support_sources'] as $arrSuppSource)
            {
                $source_support->addChild('source_name',MY_htmlspecialchars($arrSuppSource['institution']['name'], ENT_QUOTES, 'utf-8'));
                $source_support->addChild('source_name', '');
            }
        }
        {   //Ethics Review
            $ethics_reviews = $trial->addChild('ethics_reviews');
            foreach ($array['ethics_contact'] as $arrEthicsContact)
            {
                $ethics_review = $ethics_reviews->addChild('ethics_review');
                // status,approval_date,contact_name,contact_address,contact_phone,contact_email
                $ethics_review->addChild('status',MY_htmlspecialchars($arrEthicsContact['status'], ENT_QUOTES, 'utf-8'));
                $ethics_review->addChild('approval_date',MY_htmlspecialchars(MY_transform_any_date_to_british_date($arrEthicsContact['approval_date']), ENT_QUOTES, 'utf-8'));
                $ethics_review->addChild('contact_name', implode(' ', [
                   MY_htmlspecialchars($arrEthicsContact['firstname'], ENT_QUOTES, 'utf-8'),
                   MY_htmlspecialchars($arrEthicsContact['middlename'], ENT_QUOTES, 'utf-8'),
                   MY_htmlspecialchars($arrEthicsContact['lastname'], ENT_QUOTES, 'utf-8'),
                ]));
                $ethics_review->addChild('contact_address',MY_htmlspecialchars($arrEthicsContact['address'], ENT_QUOTES, 'utf-8'));
                $ethics_review->addChild('contact_phone',MY_htmlspecialchars($arrEthicsContact['telephone'], ENT_QUOTES, 'utf-8'));
                $ethics_review->addChild('contact_email',MY_htmlspecialchars($arrEthicsContact['email'], ENT_QUOTES, 'utf-8'));
            }
        }
	}
}
