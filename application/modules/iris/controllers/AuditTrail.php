<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Support\Collection as BaseCollection;

class AuditTrail extends IrisController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function download($clinical_trial_id)
    {
        $data = $this->produce($clinical_trial_id);
        force_download(sprintf('trial-%s-events.txt', $clinical_trial_id), html_entity_decode($data), TRUE);
    }

    public function view($clinical_trial_id)
    {
        $data = $this->produce($clinical_trial_id);
        header('Content-Type: text/plain');
        echo $data;
    }

    private function produce($clinical_trial_id)
    {
        $collection = new BaseCollection();

        $ensaioclinico = \Repository\ClinicalTrial::find($clinical_trial_id);
        
        if ($ensaioclinico == NULL) return '';

        $submissao = $ensaioclinico->submission();

        if ($submissao == NULL) return '';

        {
            $order++;
            $collection->push([
                'order' => $ensaioclinico->created, 
                'date' => $ensaioclinico->created,
                'content_type' => 'TRIAL',
                'content' => $ensaioclinico->toArray(),
            ]);
        }
        {
            $order = 0;
            foreach($ensaioclinico->fossils as $fossil) {
                $collection->push([
                    'order' => $fossil->creation, 
                    'date' => $fossil->creation,
                    'content_type' => 'FOSSIL',
                    'content' => $fossil->toArray(),
                ]);
            }
        }
        {
            // $order = 0;
            // foreach($submissao->remarks as $remark) {
            //     $collection->push([
            //         'order' => $remark->created, 
            //         'date' => $remark->created,
            //         'content_type' => 'REMARK',
            //         'content' => $remark->toArray(),
            //     ]);
            // }
        }
        {
            foreach($ensaioclinico->submits as $o) {
                $collection->push([
                    'order' => $o->submit_date,
                    'date' => $o->submit_date,
                    'content_type' => 'SUBMIT',
                ]);
            }
            foreach($ensaioclinico->resubmits as $o) {
                $collection->push([
                    'order' => $o->resubmit_date,
                    'date' => $o->resubmit_date,
                    'content_type' => 'RESUBMIT',
                ]);
            }
            foreach($ensaioclinico->approveds as $o) {
                $collection->push([
                    'order' => $o->approved_date,
                    'date' => $o->approved_date,
                    'content_type' => 'APPROVED',
                ]);
            }
        }
        $events = [];
        foreach($collection->sortBy('order') as $item) {
            $content_type = NULL;
            switch ($item['content_type']) {
                case 'TRIAL':
                    $content_type = 'ENSAIO CRIADO';
                    break;
                case 'FOSSIL':
                    $content_type = 'ESTUDO PUBLICADO '.$ensaioclinico->trial_id.' REVISAO #'.$item['content']['revision_sequential'];
                    break;
                case 'REMARK':
                    $content_type = 'OBSERVACOES DO REVISOR';
                    break;
                case 'SUBMIT':
                    $content_type = 'SUBMETIDO PELO REGISTRANTE';
                    break;
                case 'RESUBMIT':
                    $content_type = 'DEVOLVIDO PELO REVISOR';
                    break;
                case 'APPROVED':
                    $content_type = 'APROVADO PELO REVISOR';
                    break;
            }
            $events[$item['order']][] = $content_type;
        }
        $data = NULL;
        ob_start();
        foreach($events as $date => $day) {
            echo $date . ' ' . implode('; ', array_unique($day))."\n";
        }
        $data = ob_get_contents();
        ob_end_clean();
        return $data;
   }

    private function var($var) {
        echo "\n";
        echo var_export($var, TRUE);
    }

}