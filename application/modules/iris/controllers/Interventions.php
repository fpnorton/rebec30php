<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interventions extends IrisController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }


    public function edit($clinical_trial_id)
    {
		$this->only_reviewer_or_author($clinical_trial_id);

        $data_step = [
            'reviewer' => false
        ];
        {
            $clinicaltrial = \Repository\ClinicalTrial::find($clinical_trial_id);

            $submissao = $clinicaltrial->submission();

            $data_step['clinicaltrial_id'] = $clinical_trial_id;
            $data_step['VD']['currentStep'] = 1;
            $data_step['controller_method'] = 'observador/submissao';
            {
                $data_step['VD']['genData'] = $this->auxfunctions->loadGenData($submissao->creator_id, $clinical_trial_id);
                $data_step['VD']['subData'] = $submissao;
                $data_step['VD']['fields_status'] = @json_decode($submissao->fields_status, TRUE);
            }
            {
                $data_step['passo_'.PASSO_INTERVENCAO] = (new \Steps\Intervencao())->GetStepData($clinical_trial_id);
                $context = 'interventions';
            }
            {
//                if ($formData['messages'] != NULL) {
//                    $dataKey = 'passo_'.$passo;
//                    $data['VD']['validationData']['messages'] = $formData['messages'];
//                    foreach ($this->input->post() as $formDataKey => $formDataValue) {
//                        if (in_array($formDataKey, array_keys($data[$dataKey]))) {
//                            $data[$dataKey][$formDataKey] = $formDataValue;
//                        }
//                    }
//                }
            }
        }
        {
            $data_step['selected_languages'] = $clinicaltrial->selectedLanguages(2);
            $data_step['controller_method'] = 'prototype/revisao';
            $data_step['page'] = [
                'base_url_form-corrigir' => 'prototype/revisao/corrigir/',
                'base_url_form-sumario'  => 'prototype/submissao/0/',
            ];
            $data_step['has_review_remark'] = TRUE;
            $data_step['can_save'] = FALSE;
            $data_step['common_field'] = ' readonly disabled ';
        }
        {
            $data['content_left'] = $this->load->view('menu.php', [
            ], TRUE);
        }
        {
            $data_step['languages'] = $clinicaltrial->selectedLanguages(2);
            $data_step['_form_action'] = base_url('observador/submissao/passo/5/'.$clinicaltrial->id);
            $submission = \ReviewApp\Submission::where('_deleted', '=', 0)
                ->where('trial_id', '=', $clinical_trial_id)
                ->first();
            $data['content_right'] =
                $this->load->view('steps.php', [
                    'submission' => $submission,
                    'current_step' => PASSO_INTERVENCAO,
                    'fields_status' => $data_step['VD']['fields_status']['en'],
                ], TRUE) .
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $this->load->view('sheets/interventions.php', $data_step, TRUE).
                '</section>'.
                '</div>';
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }


    public function save($clinicaltrial_id = NULL)
    {
		$this->only_reviewer();

    }

}