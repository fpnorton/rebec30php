<?php

use Picolo\Approved;
use Picolo\Resubmit;
use Repository\ClinicalTrial;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends IrisController {

    public function show_message($_message) {
		$this->only_reviewer();
        {
            $data['content_left'] = $this->load->view('menu.php', [
            ], TRUE);
        }
        {
            $data['content_right'] =
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $this->load->view('main_message.php', ['message' => $_message], TRUE).
                '</section>'.
                '</div>';
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    public function blank()
    {
        $this->show_message('');
    }

    /*************************************/
    public function __construct()
    {
        parent::__construct();

        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");

        $this->load->library('parser');
        $this->load->library('upload');

        //Load Languages
        //$this->languageslib->loadLanguage();

        //Load Libraries
        $this->load->library('AuxFunctions');

    }

}