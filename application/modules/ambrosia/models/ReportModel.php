<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportModel extends CI_Model {

	protected $table="picolo_reports";

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['text']);
	}

	function getReportCrud(){
		$q= $this->db->get($this->table);
		if ($q->num_rows()>0) {
			foreach ($q->result_array() as $row) {

				$procedure_vars=null;
				$chart_info=null;
				if ($row['is_procedure']=='T') {
					$procedure_vars= $this->arrayunserialize($row['procedure_vars']);
				}

				if ($row['is_chart']=='T') {
					$chart_info= $this->arrayunserialize($row['chart_info']);
				}

				$result[]= array(
					'id'=> $row['id'],
					'report_name'=> $row['report_name'],
					'report_header'=> $this->arrayunserialize($row['report_header']) ,
					'is_query'=> $row['is_query'] ,
					'is_view'=> $row['is_view'],
					'is_procedure' => $row['is_procedure'],
					'report_target' => character_limiter($row['report_target'], 40),
					'procedure_vars' => $procedure_vars,
					'procedure_call'=> $row['procedure_call'] ,
					'is_table'=> $row['is_table'] ,
					'is_chart'=> $row['is_chart'] ,
					'chart_info'=> $chart_info ,
					'procedure_name'=> $row['procedure_name'],
					'users'=> $row['users'],
					'isDelete'=>$row['isDelete']
				);
			}

			return $result;
		}
	}

	function saveReportCrud($post){
		$report_header=null;
		$chart_info= null; 
		$procedure_vars=null;

		if (isset($post['header'])) {
			if (is_array($post['header'])) {
				$report_header= $this->arrayserialize($post['header']);
			}
		}

		if (isset($post['chart'])) {
			if (is_array($post['chart'])) {
				$chart_info= $this->arrayserialize($post['chart']);
			}
		}

		if (isset($post['procedurvars'])) {
			if (is_array($post['procedurvars'])) {
				$procedure_vars= $this->arrayserialize($post['procedurvars']);
			}
		}

		if (isset($post['isProcedure'])) {
			$this->db->query($post['reportTarget']);
		}

		$data=[
			'users' =>  $post['users'],
			'report_name' => $post['reportName'],
			'report_header' => $report_header,
			'is_query' =>(isset($post['isQuery'])) ? 'T' : 'F' ,
			'is_view' => (isset($post['isView'])) ? 'T' : 'F'  ,
			'is_procedure' =>(isset($post['isProcedure'])) ? 'T' : 'F' ,
			'is_table' => (isset($post['isTable'])) ? 'T' : 'F',
			'is_chart' =>(isset($post['isChart'])) ? 'T' : 'F' ,
			'chart_info' => $chart_info ,
			'report_target' =>  $post['reportTarget'] ,
			'procedure_name'=> trim($post['procedureName']),
			'procedure_vars' => $procedure_vars ,
			'procedure_call' => $post['procedureCall'] ,
		];

		$q= $this->db->insert($this->table,$data);

		return $q;
	}


	function getReportCrudbyId($id){
		$q= $this->db->where('id',$id)->get($this->table);
		if ($q->num_rows()>0) {
			foreach ($q->result_array() as $row) {

				$procedure_vars=null;
				$chart_info=null;
				if ($row['is_procedure']=='T') {
					$procedure_vars= $this->arrayunserialize($row['procedure_vars']);
				}

				if ($row['is_chart']=='T') {
					$chart_info= $this->arrayunserialize($row['chart_info']);
				}

				$result= array(
					'id'=> $row['id'],
					'report_name'=> $row['report_name'],
					'report_header'=> $this->arrayunserialize($row['report_header']) ,
					'report_target' => $row['report_target'],
					'procedure_name'=> $row['procedure_name'],
					'procedure_call'=> $row['procedure_call'] ,
					'procedure_vars' => $procedure_vars,
					'chart_info'=> $chart_info ,
					'is_query'=> $row['is_query'] ,
					'is_view'=> $row['is_view'],
					'is_procedure' => $row['is_procedure'],
					'is_table'=> $row['is_table'] ,
					'is_chart'=> $row['is_chart'] ,
					'users'=> $row['users'],
					'isDelete'=>$row['isDelete']
				);
			}

			return $result;
		}
	}

	function updateReportCrud($post){
		$id= $post['reportId'];

		$report_header=null;
		$chart_info= null; 
		$procedure_vars=null;

		if (isset($post['header'])) {
			if (is_array($post['header'])) {
				$report_header= $this->arrayserialize($post['header']);
			}
		}

		if (isset($post['chart'])) {
			if (is_array($post['chart'])) {
				$chart_info= $this->arrayserialize($post['chart']);
			}
		}

		if (isset($post['procedurvars'])) {
			if (is_array($post['procedurvars'])) {
				$procedure_vars= $this->arrayserialize($post['procedurvars']);
			}
		}

		if (isset($post['isProcedure'])) {
			$fname=$post['procedureName'];
			$drop='DROP PROCEDURE IF EXISTS '.$fname;
			$this->db->query($drop);
			$this->db->query($post['reportTarget']);
		}

		$data=[
			'users'=> $post['users'],
			'report_name' => $post['reportName'],
			'report_header' => $report_header,
			'is_query' =>(isset($post['isQuery'])) ? 'T' : 'F' ,
			'is_view' => (isset($post['isView'])) ? 'T' : 'F'  ,
			'is_procedure' =>(isset($post['isProcedure'])) ? 'T' : 'F' ,
			'is_table' => (isset($post['isTable'])) ? 'T' : 'F',
			'is_chart' =>(isset($post['isChart'])) ? 'T' : 'F' ,
			'chart_info' => $chart_info ,
			'report_target' =>  $post['reportTarget'] ,
			'procedure_vars' => $procedure_vars ,
			'procedure_call' => $post['procedureCall'] ,
			'procedure_name'=> trim($post['procedureName']),
		];

		$q= $this->db->where('id',$id)->update($this->table,$data);

		return $q;

	}

	function getReportTable(){
		$q= $this->db->select('id,report_name,procedure_vars,is_query,is_procedure,is_chart,users')->get($this->table);
		$username = $this->session->userdata('user')->username;
		if ($q->num_rows()>0) {
			foreach ($q->result_array() as $row) {
				if (is_null($row['users']) == FALSE and strlen(trim($row['users'])) > 0) {
					$users = explode(';', $row['users']);
					if (in_array('*', $users) == FALSE and in_array($username, $users) == FALSE) continue;
				}
		
				$procedure_vars=null;
				if ($row['is_procedure']=='T') {
					$procedure_vars= $this->arrayunserialize($row['procedure_vars']);
				}

				$result[]= array(
					'id'=> $row['id'],
					'report_name'=> $row['report_name'],
					'procedure_vars'=> $procedure_vars ,
					'is_query'=> $row['is_query'] ,
					'is_procedure'=> $row['is_procedure'],
					'is_chart'=> $row['is_chart'],
				);
			}

			return $result;
		}
	}

	function getReport($get){
		$id= $get['report'];
		$q= $this->db->where('id',$id)->get($this->table);
		if ($q->num_rows()>0) {
			$result= $q->row();
			if ($result->is_query=='T') {
				$r= $this->db->query($result->report_target);
				if ($r->num_rows()>0) {
					$output= $r->result_array();
				}
			}
			return $output;
		}

	}

	function callProcedure($procedure,$para,$object){
		$call = "CALL ".$procedure.$para;
		$q = $this->db->query($call, $object);
		if ($q->num_rows()>0) {
			return $q->result_array();
		}

	}

	function arrayunserialize($string){
		return unserialize($string);
	}

	function arrayserialize($array){
		return serialize($array);
	}

}

/* End of file ReportModel.php */
/* Location: ./application/modules/ambrosia/models/ReportModel.php */