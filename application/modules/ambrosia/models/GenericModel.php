<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GenericModel extends MY_Model {

    public $table; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
   // public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $protected = array('id');
    public $timestamps = FALSE;

    public function __construct()
    {
        parent::__construct();
        //$this->setTable();
        $this->return_as = 'array';
        $this->soft_deletes = FALSE;
    }

    public function setTable($table)
	 {
	    $this->table = $table;
	 }


}

/* End of file CategoryModel.php */
/* Location: ./application/modules/admin/models/CategoryModel.php */