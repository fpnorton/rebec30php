<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
            <style type="text/css">
                #dataTable tbody tr td{font-size: 9px;}
              /*  #dataTable{table-layout:fixed;}*/
                #dataTable thead tr th{font-size: 10px;}
                .modal-content { border: none; }
            </style>

<div class="row">
   <div class="col-xl-12">
      <div class="page-title-box">
         <h4 class="page-title float-left">Estudos Pendentes</h4>
         <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="<?= app_url()?>">Ambrosia</a></li>
            <li class="breadcrumb-item"><a href="<?= app_url('dashboard')?>">Estudos Pendentes</a></li>
         </ol>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<!-- end row -->



<div class="row">
   <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
         <div class="card-body">

            <h4 class="card-title">Estudos Pendentes</h4>
            <form>
            <div class="form-row d-flex">
                  <div class="form-group col-md-3">
                    <select id="usuario" class="form-control" >
                        <option value="null">Selecione o usuário</option>
                    </select>
                  </div>

                  <div class="form-group col-md-2 mr-auto">
                    <button type="button" class="btn btn-primary btn-rounded btn-fw filtrar-estudos">Filtrar Estudos</button>
                  </div>

             <!--    <div class="form-group col-md-3 ">
                    <input type="text" class="form-control" id="searchTable" placeholder="Search Table" aria-label="Username"> 
                </div>
                 <div class="form-group col-md-2 ">
                    <select class="form-control" name="field" id="field">
                      <option value="null">Table Field</option>
                      <option value="trial_id">Trial Id</option>
                      <option value="user_name">User Name</option>
                      <option value="email">Email</option>
                    </select>
                </div>

                 <div class="form-group col-md-2 ">
                    <button type="button" class="btn btn-primary btn-rounded btn-fw search-estudos">Search</button>
                  </div> -->

            </div>


            </form>



        
            <div class="table-responsive">
               <table id="dataTable" class="table-bordered text-center" >
                  <thead>
                     <tr>
                        <th></th>
                        <th></th>
                        <th>Created</th>
                        <th>Trial Id</th>
                        <th>Rbr</th>
                        <th>Updated Submission</th>
                        <th>User Name & Email</th>
                        <th style="width: 55px;">Link</th>
                        <th>Histórico de Revisões</th>
                        <th>Subm Id</th>
                        <th>Subm Date</th>
                        <th>Limite</th>
                        <th>Dias restantes</th>
                        <th>Nome do Revisor</th>
                        <th>Controles</th>
                     </tr>
                  </thead>
                  <tbody>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- <div class="row">
    <div class="col-md-6">
        <p>Total Records: 
            <span id="toatlRecords"></span>
        </p>
    </div>
    <div class="col-md-6 text-right">
        <nav>
            <ul id="pagination" class="pagination justify-content-end"></ul>
        </nav>
    </div>
</div>
 -->

<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal"  aria-modal="true">
    <div class="modal-dialog" role="document" style="max-width: 900px;">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="exampleModalLabel-2">Histórico de Revisões</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-white">×</span>
                </button>
            </div>
            <div class="modal-body" style="padding: 14px 10px;">
                 <div class="historico-de-revisoes"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Fechar</button>
               
            </div>
        </div>
    </div>
</div>


<?php 
$this->template->javascript->add('assets/ambrosia/dashboard.js');

?>
