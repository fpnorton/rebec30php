<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style type="text/css">
   .parsley-errors-list{
   list-style: none;
    position: absolute;
    top: 0;
    color: red;
    margin-bottom: 0px;
    right: 20px;
}
</style>

<div class="row">
   <div class="col-xl-12">
      <div class="page-title-box">
         <h4 class="page-title float-left">Report</h4>
         <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="<?= app_url()?>">Ambrosia</a></li>
            <li class="breadcrumb-item"><a href="<?= app_url('report')?>">Report</a></li>
         </ol>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<!-- end row -->



<div class="row">
   <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
         <div class="card-body">

            <h4 class="card-title">Estudos Pendentes</h4>
            <form id="reportForm" action="" method="GET" data-parsley-validate="">
            <div class="row">
               <div class="col-md-4 selectRow">
                  <label>Select Report</label>
                  <select class="form-control" name="report" required="required" id="reportSelect">
                     <option  value="">Select Report </option>
                     <?php if (is_array($reports)): ?>
                        <?php foreach ($reports as $report): ?>
                           <option data-id="html_<?= $report['id']?>" data-procedure="<?= ($report['is_procedure']=='T') ? 1 : 0; ?>" data-chart="<?= ($report['is_chart']=='T') ? 1 : 0; ?>"value="<?= $report['id'] ?>" ><?= ucfirst($report['report_name']) ?></option>
                        <?php endforeach ?>
                     <?php endif ?>
                  </select>
               </div>


               <div class="col-md-3">
                  <button type="submit" id="gReport" class="btn btn-primary btn-rounded btn-fw search-estudos mt-4">Report</button>
                   <button type="submit" style="display: none;" id="gChart" class="btn btn-warning btn-rounded btn-fw search-estudos mt-4">Chart</button>
               </div>
            </div>

            <div class="row">
              
            </div>

            </form>
         </div>
      </div>
   </div>
</div>



<script type="text/javascript">
   $(".hideVars").hide();

    <?php if (is_array($reports)): ?><?php foreach ($reports as $key => $pro): ?>
      <?php if ($pro['is_procedure']=='T'): ?>
         var var_html_<?= $pro['id'] ?> = '<div class="form-group col-md-4 hideVars" id="report-<?= $pro['id'] ?>"><?php if (is_array($pro['procedure_vars'])): ?>
      <?php foreach ($pro['procedure_vars'] as $key => $p): ?><label><?= ucfirst($p['name']) ?> - Example : <?= $p['example']?></label><?php  $type= $p['type'];$inputType= $p['type']; if ($type=='int') { $inputType="number";}?> <input required="required" type="<?= strtolower($inputType) ?>" name="<?= $p['name'] ?>" placeholder="Example : <?= $p['example']?>" class="form-control mb-3" ><?php endforeach ?><?php endif ?></div>' ;
      <?php endif ?>
   <?php endforeach ?>
   <?php endif ?>

   $("#reportSelect").change(function(event) {
      event.preventDefault();
       $(".hideVars").remove();
      var report= "var_"+$(this).find(':selected').attr('data-id');
      var pro= $(this).find(':selected').attr('data-procedure');
      var isChart= $(this).find(':selected').attr('data-chart');
         if (pro==1) {
            $(".selectRow").after(eval(report));
         }

         if (isChart==1) {
            $("#gChart").show();
         }else{
            $("#gChart").hide();
         }

      /* Act on the event */
   });


   $('#reportForm').parsley();
   $("#gReport").click(function(event) {
      event.preventDefault();
      var valid= $("#reportForm").parsley().validate();
      console.log(valid);
      if (valid==true) {
         $("#reportForm").attr({
            action: '<?= app_url('report/show') ?>',
         });

         $( "#reportForm" ).submit();
      }
   });


   $("#gChart").click(function(event) {
      event.preventDefault();
      var valid= $("#reportForm").parsley().validate();
      console.log(valid);
      if (valid==true) {
         $("#reportForm").attr({
            action: '<?= app_url('report/chart') ?>',
         });

         $( "#reportForm" ).submit();
      }
   });

</script>


