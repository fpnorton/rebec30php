<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
   <div class="col-xl-12">
      <div class="page-title-box">
         <h4 class="page-title float-left">Report</h4>
         <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="<?= app_url()?>">Ambrosia</a></li>
            <li class="breadcrumb-item"><a href="<?= app_url('report')?>">Report</a></li>
         </ol>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<!-- end row -->


<div class="row">
   <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
         <div class="table-responsive">
            <h4 class="card-title"> <?= $report['name'] ?> </h4>



            <?php if (is_array($report['data'])): ?>
            <table id="reportTable" class="table table-hover table-bordered">
               <thead>
                 
                     <tr>

                        <?php foreach($report['header'] as $header): ?>
                              <th><?= ucfirst($header) ?></th>  
                        <?php endforeach ?>
                     </tr>
                
                  <tr>
                  </tr>
               </thead>
               <tbody>

                  <?php foreach ($report['data'] as $key => $value): ?>
                     <tr>
                     <?php foreach ($value as $i): ?>
                       <td> <?= $i ?> </td>
                     <?php endforeach ?>
                      </tr>
                  <?php endforeach ?>

                
               </tbody>
            </table>
             <?php endif ?>
           
         </div>
      </div>
   </div>
</div>

<script type="text/javascript"> 
$(document).ready(function() {
	 setTimeout(function(){
		  
    $('#reportTable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
			//'copy', 'csv', 'excel', 'pdf', 'print' ,
              'copyHtml5',
              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
        ]
	 } );},700)
} ); 
    // $('#reportTable').DataTable();
    // console.log("hello");
</script>
