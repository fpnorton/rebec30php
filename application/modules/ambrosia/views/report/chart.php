<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- <script src="https://www.google.com/jsapi"></script> -->

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- <script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
</script> -->

<!--     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {
          title: 'Company Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
        //var chart = new google.visualization.PieChart(document.getElementById('curve_chart'));
        chart.draw(data, options);
      }
    </script> -->

<div class="row">
   <div class="col-xl-12">
      <div class="page-title-box">
         <h4 class="page-title float-left">Report</h4>
         <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="<?= app_url()?>">Ambrosia</a></li>
            <li class="breadcrumb-item"><a href="<?= app_url('report')?>">Report</a></li>
         </ol>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<!-- end row -->

<div class="row">
   <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
         <div class="card-body">
<div id="curve_chart" style="height: 500px"></div>
</div>
</div>
</div>
</div>





<script>
    // Visualization API with the 'corechart' package.
    google.charts.load('visualization', { packages: ['corechart'] });
    google.charts.setOnLoadCallback(drawLineChart);

    

    function drawLineChart() {

        var obj={
          <?php if (is_array($get)) :?>
            <?php foreach ($get as $key => $value): ?>
              <?= $key ?> : <?= $value ?> ,
            <?php endforeach ?>
          <?php endif; ?>
        };

        $.ajax({
            url: "<?= base_url('api/report/chart/') ?>",
            dataType: "json",
            type: "POST",
            data: obj,
            success: function (data) {
              var chart= data.chartInfo;
              console.log(data.gNChart);

                // var arrSales = [['Month', 'Sales Figure', 'Perc. (%)']];    // Define an array and assign columns for the chart.
                // //console.log(data.arraymap);
                // // Loop through each data and populate the array.
                // $.each(data.arraymap, function (index, value) {
                //     //console.log(value);
                //     arrSales.push([value.month, value.year, value.total]);
                // });

                //console.log(arrSales);

                // Set chart Options.
                var options = {
                    title: chart.title,
                    curveType: 'function',
                    //legend: { position: 'bottom', textStyle: { color: '#555', fontSize: 14} }  // You can position the legend on 'top' or at the 'bottom'.
                };

                // Create DataTable and add the array to it.
                var figures = google.visualization.arrayToDataTable(data.gNChart)
                <?php if($chartInfo['type']=='line') :?>
                  var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
                <?php elseif($chartInfo['type']=='pie_chart') : ?>
                  var chart = new google.visualization.PieChart(document.getElementById('curve_chart'));
                <?php elseif($chartInfo['type']=='column_chart'): ?>
                  var chart = new google.visualization.ColumnChart(document.getElementById('curve_chart'));
                <?php endif ;?>
                // Define the chart type (LineChart ,LineChart ,PieChart) and the container (a DIV in our case).
                
                chart.draw(figures, options);      // Draw the chart with Options.
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert('Got an Error');
            }
        });
    }
</script>

