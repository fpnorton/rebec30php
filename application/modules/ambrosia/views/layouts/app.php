<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('WelcomeForm'); $fieldlabel->enableUpdate(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Estudos pendentes - Sistema de controle interno </title>
  <!-- base:css -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/') ?>vendors/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/') ?>vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?= base_url('assets/') ?>images/favicon.png" />
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/vendors/busyload/busyload.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css') ?>">
  <style type="text/css">
    .horizontal-menu .bottom-navbar .page-navigation {
      -webkit-justify-content: unset;
      justify-content: unset;
    }
  </style>
  <?php echo $this->template->stylesheet; ?>

  <!-- base:js -->
  <script src="<?= base_url('assets/') ?>vendors/js/vendor.bundle.base.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/') ?>vendors/parsley/parsley.js" ></script>
  <!-- endinject -->

</head>

<body  data-baseurl="<?= base_url(); ?>" >
<div>
    <? if (($this->session->userdata('user') != null) and ($this->session->userdata('user')->id > 0)) : ?>
        <div class="modal fade"
             id="modal_signout"
             tabindex="-1"
             role="dialog"
             aria-labelledby="modalLabel_signout"
             aria-hidden="true">
            <div class="modal-dialog modal-sm"
                 role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="modalLabel_signout"><?php echo $genericlabel->get_or_new('title_signout'); ?>
                        </h5>
                        <button type="button"
                                class="close"
                                data-dismiss="modal"
                                aria-label="<?php echo $genericlabel->get_or_new('button_close'); ?>">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"><?php echo $fieldlabel->get_or_new('text_signout') ?>
                    </div>
                    <div class="modal-footer">
                        <form action="<?php echo base_url('/signout') ?>" method="post">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $genericlabel->get_or_new('button_close'); ?></button>
                            <button type="submit" class="btn btn-danger"><?php echo $genericlabel->get_or_new('button_confirm'); ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <? else : ?>
        <div class="modal fade"
             id="modal_signin"
             tabindex="-1"
             role="dialog"
             aria-labelledby="modalLabel_signin"
             aria-hidden="true">
            <div class="modal-dialog modal-sm"
                 role="document">
                <div class="modal-content">
                    <form action="<?php echo base_url('/signin') ?>" method="post">
                        <div class="modal-header">
                            <img src="<?php echo base_url('assets/images/rebec_logo.png'); ?>" width="320px;" />
                        </div>
                        <div class="modal-body">
                            <div class="form-group has-feedback">
                                <input type="text" class="form-control" placeholder="Email" name="email" required value="<?php set_value('email'); ?>"  />
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" class="form-control" placeholder="Password" name="password" required />
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">
                                </div><!-- /.col -->
                                <div class="col-sm-4">
                                    <input type="submit" class="btn btn-primary btn-block btn-flat" value="<?php echo $genericlabel->get_or_new('button_login') ?>" />
                                </div><!-- /.col -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <? endif; ?>
</div>
  <div class="container-scroller">
    <!-- partial:partials/_horizontal-navbar.html -->
    <div class="horizontal-menu">
      <nav class="navbar top-navbar col-lg-12 col-12 p-0">
        <div class="container">
          <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
            <a class="navbar-brand brand-logo text-white" href="index.html">Sistema de Controle Interno</a>
            <a class="navbar-brand brand-logo-mini text-white" href="index.html">Sistema de Controle Interno </a>
          </div>
          <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">

            <ul class="nav nav-pills">
              <li class="nav-item dropdown dropleft">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <? if (empty($this->session->userdata('user')->username )) : ?>
                          <i class="fa fa-user-o fa-4"></i>
                      <? else : ?>
                          <i class="fa fa-user fa-4"></i>
                          <span class="d-none d-lg-inline" style="color: black"><?php echo $this->session->userdata('user')->username ?></span>
                      <? endif; ?>
                  </a>
                  <div class="dropdown-menu">
                      <?php if (($this->session->userdata('user') != null) and ($this->session->userdata('user')->id > 0)) : ?>
                          <?php if ($this->session->userdata('user')->isCommon() or $this->session->userdata('user')->isSuper()) : ?>
                              <div class="dropdown-item">
                                  <a href="<?php echo base_url(); ?>pesquisador" class="btn btn-default btn-flat">
                                      <i class="fa fa-asterisk"></i> <?php echo $genericlabel->get_or_new('to_research_dashboard') ?>
                                  </a>
                              </div>
                          <?php endif; ?>
                          <?php if ($this->session->userdata('user')->isRevisor() or $this->session->userdata('user')->isSuper()) : ?>
                              <div class="dropdown-item">
                                  <a href="<?php echo base_url(); ?>ambrosia" class="btn btn-default btn-flat">
                                      <i class="fa fa-book"></i> <?php echo $genericlabel->get_or_new('to_revisor_dashboard') ?>
                                  </a>
                              </div>
                          <?php endif; ?>
                          <?php if ($this->session->userdata('user')->isAdmin() or $this->session->userdata('user')->isSuper()) : ?>
                              <div class="dropdown-item">
                                  <a href="<?php echo base_url(); ?>eris/welcome" class="btn btn-default btn-flat">
                                      <i class="fa fa-sign-language"></i> <?php echo $genericlabel->get_or_new('to_admin_dashboard') ?>
                                  </a>
                              </div>
                          <?php endif; ?>
                          <div class="dropdown-divider"></div>
                          <div class="dropdown-item">
                              <a href="#"
                                 data-toggle="modal" data-target="#modal_signout"
                                 class="btn btn-default btn-flat" >
                                  <i class="fa fa-sign-out"></i> <?php echo $genericlabel->get_or_new('sign_out') ?>
                              </a>
                          </div>
                      <?php else : ?>
                          <div class="dropdown-item">
                              <a href="#"
                                 data-toggle="modal" data-target="#modal_signin"
                                 class="btn btn-default btn-flat">
                                  <i class="fa fa-sign-in"></i> <?php echo $genericlabel->get_or_new('sign_in') ?>
                              </a>
                          </div>
                          <div class="dropdown-divider"></div>
                          <div class="dropdown-item">
                              <a href="<?php echo base_url(); ?>signup" class="btn btn-default btn-flat">
                                  <i class="fa fa-sign-in"></i> <?php echo $genericlabel->get_or_new('sign_up') ?>
                              </a>
                          </div>
                      <?php endif; ?>
                  </div>
              </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
              <span class="mdi mdi-menu"></span>
            </button>
          </div>
        </div>
      </nav>
      <nav class="bottom-navbar">
        <div class="container">
          <ul class="nav page-navigation">
            <li class="nav-item">
              <a class="nav-link" href="<?= app_url('dashboard') ?>">
                <i class="mdi mdi-black-mesa menu-icon"></i>
                <span class="menu-title">Estudos Pendentes</span>
              </a> 
            </li>

            <li class="nav-item">
              <a class="nav-link" href="<?= app_url('report') ?>">
                <i class="mdi mdi-poll menu-icon"></i>
                <span class="menu-title">Report</span>
              </a> 
            </li>

            <?php if ($this->session->userdata('user')->isSuper()) : ?>
            <li class="nav-item">
              <a class="nav-link" href="<?= app_url('reportcrud') ?>">
                <i class="mdi mdi-poll-box menu-icon"></i>
                <span class="menu-title">Report CRUD</span>
              </a> 
            </li>
            <?php endif; ?>
         
          </ul>
        </div>
      </nav>
    </div>

       <!-- partial -->
    <div class="container-fluid page-body-wrapper">
	      <div class="main-panel">
	        <div class="content-wrapper">
	        	 <?php echo $this->template->content;?>
	        </div>
	         <!-- partial:partials/_footer.html -->
	        <footer class="footer">
	          <div class="w-100 clearfix">
	            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 <a href="http://www.urbanui.com/" target="_blank">Urbanui</a>. All rights reserved.</span>
	            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart-outline text-danger"></i></span>
	          </div>
	        </footer>
	        <!-- partial -->
	    </div>
	</div>


 </div>
  <!-- container-scroller -->

  <!-- inject:js -->
  <script src="<?= base_url('assets/') ?>js/off-canvas.js"></script>
  <script src="<?= base_url('assets/') ?>js/hoverable-collapse.js"></script>
  <script src="<?= base_url('assets/') ?>js/template.js"></script>

  <script src="<?= base_url('assets/') ?>vendors/datatables.net/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js') ?>" ></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js" ></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js" ></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" ></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" ></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js" ></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js" ></script>

  <script type="text/javascript" src="<?= base_url('assets/vendors/busyload/busyload.min.js') ?>" ></script>
  <script src="<?= base_url('assets/') ?>js/app.js"></script>

  <?php echo $this->template->javascript; ?>

  

</body>
</html>