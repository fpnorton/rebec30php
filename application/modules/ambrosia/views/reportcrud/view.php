<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style type="text/css">
.btn.btn-icon {
    width: 26px;
    height: 26px;
    padding: 0;
}
.parsley-errors-list{
    list-style: none;
    position: absolute;
    top: 26px;
    color: red;
    margin-bottom: 0px;
}
</style>

<div class="row mb-2">
   <div class="col-xl-12">
      <div class="page-title-box">

         <h4 class="page-title float-left">Report CRUD</h4>

         <a href="<?= app_url('reportcrud/create') ?>" class="float-right btn btn-primary btn-rounded btn-fw filtrar-estudos">Add Report</a>
         <!-- <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="<?= app_url()?>">Ambrosia</a></li>
            <li class="breadcrumb-item"><a href="<?= app_url('reportcrud')?>">Report CRUD</a></li>
         </ol> -->
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<!-- end row -->

<?php $tableHeader= array(
    array('field'=>'picolo_reports.id','name'=>'id','type'=>'1'),
    array('field'=>'picolo_reports.report_name','name'=>'report name','type'=>1),
    array('field'=>'picolo_reports.procedure_name','name'=>'procedure name','type'=>1),
    array('field'=>'picolo_reports.is_query','name'=>'is query','type'=>0),
    array('field'=>'picolo_reports.is_view','name'=>'is view','type'=>0),
    array('field'=>'picolo_reports.is_procedure','name'=>'is procedure','type'=>0),
    array('field'=>'picolo_reports.is_table','name'=>'is table','type'=>0),
    array('field'=>'picolo_reports.is_chart','name'=>'is chart','type'=>0),
    array('field'=>'picolo_reports.isDelete','name'=>'is delete','type'=>2),
  );

  $options= array(
    array('type'=>1,'name'=>'='),
    array('type'=>1,'name'=>'!='),
    array('type'=>1,'name'=>'>'),
    array('type'=>1,'name'=>'<'),
    array('type'=>1,'name'=>'>='),
    array('type'=>1,'name'=>'<='),
    array('type'=>0,'name'=>'in'),
    array('type'=>0,'name'=>'not in'),
);

?>


<div class="row">
   <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
         <div class="card-body">
           <form id="queryForm" data-parsley-validate="">

            <div class="row">
            <div class="form-group col-md-3 ">
                <select class="form-control" id="queryHeader" required="required">
                  <option value="">Select Report Header</option>
                    <?php foreach ($tableHeader as $key => $table): ?>
                      <option data-type="<?= $table['type']?>" value="<?= $table['field'] ?>"><?= ucfirst($table['name']) ?></option>
                    <?php endforeach ?>
                </select>
              </div>

              <div class="form-group col-md-3 ">
                <select class="form-control" id="queryOptions" required="required">
                  <option value="">Select options</option>
                    <?php foreach ($options as $key => $option): ?>
                      <option data-type="<?= $option['type'] ?>"  value="<?= $option['name'] ?>"><?= ucfirst($option['name']) ?></option>
                    <?php endforeach ?>
                </select>
              </div>

              <div class="form-group col-md-3" id="searchText">
                <input type="text" name="" id="queryText" class="form-control" placeholder="Enter parameters" >
              </div>

              <div class="col-md-3 form-group" id="isTrue" style="display: none;" >
                <ul class="list-inline m-0 ">
                  <li class="list-inline-item">
                    <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="typeRadios" id="typeRadios1" value="T" checked="">
                                True
                              <i class="input-helper"></i></label>
                            </div>
                  </li>
                  <li class="list-inline-item">
                    <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="typeRadios" id="typeRadios2" value="F">
                                False
                              <i class="input-helper"></i></label>
                            </div>
                  </li>
                </ul>
              </div>

               <div class="col-md-3">
                  <button type="submit" id="buildQuery" class="btn btn-info btn-rounded btn-fw ">Build Query</button>
                  <button type="button" data-option="and" class="btn btn-dark btn-rounded btn-fw queryAndOr">And</button>
                  <button type="button" data-option="or" class="btn btn-dark btn-rounded btn-fw queryAndOr">Or</button>
                
               </div>

            </div>
          </form>
            <form id="queryPreviewForm" method="POST" data-parsley-validate="" >
              <div class="form-row">
                <div class="form-group col">
                  <textarea id="queryPreview" name="query" class="bg-dark text-light form-control" required="required" readonly="readonly" ></textarea>
                </div>
                <div class="col-3">
                   <button type="submit" class="btn btn-primary btn-rounded btn-fw ">Run Query</button>
                    <button type="reset" class="btn btn-danger btn-rounded btn-fw queryReset">Clean</button>
                </div>
              </div>
            </form>

         </div>
      </div>
    </div>
</div>

<div class="row">
   <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
         <div class="card-body">
            <h4 class="card-title">Report CRUD</h4>
            <div id="crudTable"> 
           
             </div>
          </div>
    	</div>
	</div>
</div>


<?php  

$this->template->javascript->add('assets/js/data-table.js');
$this->template->javascript->add('assets/ambrosia/reportcrud.js');
?>

