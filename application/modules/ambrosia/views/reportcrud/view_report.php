<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style type="text/css">
  .cardClose{
    position: absolute;
    right: 15px;
  }
  .btn.btn-icon {
    width: 26px;
    height: 26px;
    padding: 0;
}
</style>

<div class="row mb-2">
   <div class="col-xl-12">
      <div class="page-title-box">

         <h4 class="page-title float-left">Report CRUD</h4>

         <a href="<?= app_url('reportcrud') ?>" class="float-right btn btn-primary btn-rounded btn-fw filtrar-estudos">Back to Report CRUD</a>
         <a href="<?= app_url('reportcrud/edit/'.$report['id']) ?>" class="float-right btn btn-dark btn-rounded btn-fw filtrar-estudos mr-2">Edit Report CRUD</a>

        
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<!-- end row -->


<div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"> Report </h4>

                  <p class="display-5"><?= ucfirst($report['report_name']) ?> </p>
                  <p class="text-light bg-dark p-2 rounded">
                       <?= $report['report_target'] ?>
                    </p>
                  <p>
                  Users : 
                  </p>
                  <p class="text-light bg-dark p-2 rounded">
                      <?= $report['users'] ?>
                  </p>
                  <p>
                    Report Headers : 
                      <?php if (is_array($report['report_header'])): ?>
                                        <?php foreach ($report['report_header'] as $head): ?>
                                            <div class="badge badge-info m-1">
                                                <?= ucfirst($head) ?>
                                            </div>
                                        <?php endforeach ?>
                                    <?php endif ?>
                    </p>

                    <ul class="list-inline">
                      <li class="list-inline-item display-5">
                        Query:

                         <?php if ($report['is_query']=='T'): ?>
                            <button type="button" class="btn btn-primary btn-rounded btn-icon">
                            <i class="mdi mdi-check"></i>
                          </button>
                          <?php else: ?>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon">
                            <i class="mdi mdi-close"></i>
                          </button>

                          <?php endif ?>
                      </li>
                       <li class="list-inline-item display-5">
                        View:

                         <?php if ($report['is_view']=='T'): ?>
                            <button type="button" class="btn btn-primary btn-rounded btn-icon">
                            <i class="mdi mdi-check"></i>
                          </button>
                          <?php else: ?>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon">
                            <i class="mdi mdi-close"></i>
                          </button>

                          <?php endif ?>
                      </li>

                       <li class="list-inline-item display-5">
                        Procedure:

                        <?php if ($report['is_procedure']=='T'): ?>
                            <button type="button" class="btn btn-primary btn-rounded btn-icon">
                            <i class="mdi mdi-check"></i>
                          </button>
                          <?php else: ?>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon">
                            <i class="mdi mdi-close"></i>
                          </button>

                          <?php endif ?>
                      </li>

                      <li class="list-inline-item display-5">
                       Chart:

                        <?php if ($report['is_chart']=='T'): ?>
                            <button type="button" class="btn btn-primary btn-rounded btn-icon">
                            <i class="mdi mdi-check"></i>
                          </button>
                          <?php else: ?>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon">
                            <i class="mdi mdi-close"></i>
                          </button>

                          <?php endif ?>
                      </li>

                       <li class="list-inline-item display-5">
                       Table:

                         <?php if ($report['is_table']=='T'): ?>
                            <button type="button" class="btn btn-primary btn-rounded btn-icon">
                            <i class="mdi mdi-check"></i>
                          </button>
                          <?php else: ?>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon">
                            <i class="mdi mdi-close"></i>
                          </button>

                          <?php endif ?>
                      </li>

                    </ul>

                    <?php if ($report['is_procedure']=='T'): ?>
                    <p class="display-5"> <?= $report['procedure_name'] ?> </p>
                    <p class="bg-dark p-2 rounded text-light"> <?= $report['procedure_call'] ?> </p>
                   
                       <?php if (is_array($report['procedure_vars'])): ?>
                                        <?php $i=1 ?>
                                        <?php foreach ($report['procedure_vars'] as $key=> $vars): ?>
                                        <div class="bg-dark  mb-1 rounded  p-2 mb-2" >
                                           
                                              <p class="text-light p-0 m-0">
                                                <?php foreach ($vars as $key=> $i): ?>
                                                <?= ucfirst($key) ." : ". $i  ?> <br>
                                            <?php endforeach ?>
                                               </p>
                                            
                                          </div>
                                            
                                           
                                        <?php endforeach ?>
                                    <?php endif ?>

                       <?php endif ?>


                       <?php if ($report['is_chart']=='T'): ?>
                      <p class="mt-4">
                         Chart Info :
                         <?php if (is_array($report['chart_info'])): ?>
                                    <?php foreach ($report['chart_info'] as $key => $chart): ?>
                                        <div class="badge badge-info  m-1">
                                                <?= ucfirst($key) ." : ". ucfirst($chart) ?>
                                        </div>
                                    <?php endforeach ?>
                                <?php endif ?>

                      </p>
                    <?php endif ?>
                  </div>
                </div>
              </div>
            </div>