<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style type="text/css">
  .cardClose{
    position: absolute;
    right: 15px;
  }
</style>

<div class="row mb-2">
   <div class="col-xl-12">
      <div class="page-title-box">

         <h4 class="page-title float-left">Report CRUD</h4>

         <a href="<?= app_url('reportcrud') ?>" class="float-right btn btn-primary btn-rounded btn-fw filtrar-estudos">Back to Report CRUD</a>
        
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<!-- end row -->


<div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Create Report</h4>
                  
                  <form class="forms-sample" id="createReport" action="<?= app_url('reportcrud/update') ?>" method="post">
                    <input type="hidden" name="reportId" value="<?= $report['id'] ?>">
                    <div class="form-group">
                      <label for="reportName">Report Name</label>
                      <input type="text" class="form-control" id="reportName" name="reportName" placeholder="Enter Report Name" value="<?= $report['report_name'] ?>" >
                    </div>
                    <div class="form-group">
                      <label for="reportTarget">Report Target</label>
                     
                        <textarea class="form-control bg-dark text-light" id="reportTarget" name="reportTarget" rows="5" placeholder="Enter Report Target" ><?= $report['report_target']?></textarea>
                       
                      
                    </div>
                    <div class="form-group">
                      <label for="reportName">Users [; between]</label>
                      <input type="text" class="form-control" id="users" name="users" placeholder="Enter Users" value="<?= $report['users'] ?>" >
                    </div>
                    
                    <div class="form-group">
                      <label for="reportHeader">Report Header <button type="button" class="btn btn-primary btn-rounded btn-fw btn-sm ml-3 addHeader">Add Header</button> </label>
                      <div id="reportHeader" >
                        <ul class="headerList list-inline ">

                          <?php if (is_array($report['report_header'])): ?>
                            <?php $i=0; ?>
                            <?php foreach ($report['report_header'] as $head): ?>
                                <li class="list-inline-item" ><div class="badge badge-primary mr-1"><?= $head ?> <i class="mdi mdi-close ml-2 removeHeader"></i> <input type="hidden" name="header[<?= $i++?>]" value="<?= $head ?>" > </div></li>
                            <?php endforeach ?>
                          <?php endif ?>

                        
                        </ul>
                      </div>
                    </div>

                    <div class="form-group">

                      <label>Report Type</label>

                      <ul class="list-inline">
                          <li class="list-inline-item">
                            <div class="form-check form-check-flat form-check-primary">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="isQuery"  <?php if ($report['is_query']=='T'): ?>checked="checked"<?php endif ?> >
                               Is Query
                              <i class="input-helper"></i></label>
                            </div>
                          </li>

                          <li class="list-inline-item">
                            <div class="form-check form-check-flat form-check-primary">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="isView" <?php if ($report['is_view']=='T'): ?>checked="checked"<?php endif ?>>
                              Is View
                              <i class="input-helper"></i></label>
                            </div>
                          </li>

                          <li class="list-inline-item">
                            <div class="form-check form-check-flat form-check-primary">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="isProcedure" <?php if ($report['is_procedure']=='T'): ?>checked="checked"<?php endif ?>>
                               Is Procedure
                              <i class="input-helper"></i></label>
                            </div>
                          </li>


                          <li class="list-inline-item">
                            <div class="form-check form-check-flat form-check-primary">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="isChart" <?php if ($report['is_chart']=='T'): ?>checked="checked"<?php endif ?>>
                               Is Chart
                              <i class="input-helper"></i></label>
                            </div>
                          </li>


                          <li class="list-inline-item">
                            <div class="form-check form-check-flat form-check-primary">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="isTable" <?php if ($report['is_table']=='T'): ?>checked="checked"<?php endif ?>>
                               Is Table
                              <i class="input-helper"></i></label>
                            </div>
                          </li>

                      </ul>

                    </div>

                    <div class="form-group">
                      <label for="procedureName">Procedure Name</label>
                      <input type="text" class="form-control" id="procedureName" name="procedureName" placeholder="Enter Procedure Name"  value="<?= $report['procedure_name']?>" >
                    </div>

                    <div class="form-group">
                      <label for="procedureCall">Procedure Call</label>
                      <textarea class="form-control bg-dark text-light " id="procedureCall" name="procedureCall" rows="5" placeholder="Enter Procedure Calls" ><?= $report['procedure_call']?></textarea>
                    </div>

                    <div class="form-group">
                      <label for="procedureVars">Procedure Variables <button type="button" class="btn btn-primary btn-rounded btn-fw btn-sm ml-3 addProcedureVars">Add Procedure Variables</button> </label>
                      <div id="procedureVars" >
                        <ul class="varList list-unstyled">

                           <?php if (is_array($report['procedure_vars'])): ?>
                                        <?php $p=0 ?>
                                        <?php foreach ($report['procedure_vars'] as $key=> $vars): ?>
                                        

                                          <li>
                                            <div class="card card-inverse-success mb-1" id="context-menu-simple"><div class="card-body p-1"> 
                                            <p class="card-text">

                                            <?php foreach ($vars as $key=> $i): ?>
                                              <?= ucfirst($key) ." : ". $i  ?>,
                                               <input type="hidden" name="procedurvars[<?= $p ?>][<?= $key?>]" value="<?= $i ?>" >
                                            <?php endforeach ?>
                                            </p>
                                            </div> 
                                           <button type="button" class="close cardClose" aria-label="Close"><span aria-hidden="true">&times;</span></button> </div></li>
                                            <?php $p++; ?>
                                           
                                        <?php endforeach ?>
                                    <?php endif ?>

                        
                        </ul>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="chartInfo">Chart Info <button type="button" class="btn btn-primary btn-rounded btn-fw btn-sm ml-3 addChartInfo">Add Chart Info</button> </label>
                      <div id="chartInfo" >
                        <ul class="chartInfoList list-unstyled">

                          <?php if (is_array($report['chart_info'])): ?>
                            <li>
                                            <div class="card card-inverse-success mb-1" id="context-menu-simple">
                                              <div class="card-body p-1"> 
                                                <p class="card-text">
                                    <?php foreach ($report['chart_info'] as $key => $chart): ?>
                                       
                                         <?= ucfirst($key) ." : ". ucfirst($chart) ?>,
                                                  <input type="hidden" name="chart[<?= $key ?>]" value="<?= $chart ?>" >
                                                 <?php endforeach ?>
                                                </p>
                                              </div> 
                                                <button type="button" class="close cardClose" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                                              </div>
                                          </li>
                                  
                          <?php endif ?>

                         
                        </ul>
                      </div>
                    </div>

                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>
           

          </div>


<div class="modal fade" id="reportHeaderModel" tabindex="-1" role="dialog" aria-labelledby="reportHeaderModel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Report Header</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="reportHeaderForm" action="" method="POST">
        <div class="modal-body ">
          <div class="form-group">
            <label>Header Name</label>
            <input type="text" name="" id="header" class="form-control" placeholder="Enter Header Name">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>


<div class="modal fade" id="procedurevarsModel" tabindex="-1" role="dialog" aria-labelledby="procedurevarsModel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >Procedure Vars</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="procedurevarsForm" action="" method="POST">
        <div class="modal-body">
            <div class="form-group">
            <label>Name</label>
            <input type="text" name="varName" id="varName" class="form-control" placeholder="Enter Header Name">
            </div>

            <div class="form-group">
            <label>Type</label>
            <select class="form-control" id="varType" name="varType">
              <option>Select Type</option>
              <option value="int">Int</option>
              <option value="date">Date</option>
              <option value="text">String</option>
            </select>
            </div>

            <div class="form-group">
            <label>Example</label>
            <input type="text" name="varExample" id="varExample" class="form-control" placeholder="Enter Header Name">
            </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>

<div class="modal fade" id="chartInfoModel" tabindex="-1" role="dialog" aria-labelledby="chartInfoModel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"> Chart Info</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="chartInfoForm" action="" method="POST">
        <div class="modal-body">

            <div class="form-group">
            <label>Chart Title</label>
            <input type="text" name="c_title" id="c_title" class="form-control" placeholder="Enter Header Name">
            </div>


            <div class="form-group">
            <label>X AXIS</label>
            <input type="text" name="x_axis" id="x_axis" class="form-control" placeholder="Enter Header Name">
            </div>

            <div class="form-group">
            <label>Y AXIS</label>
            <input type="text" name="y_axis" id="y_axis" class="form-control" placeholder="Enter Header Name">
            </div>

            <div class="form-group">
            <label>Type</label>
            <select class="form-control" id="chartType" name="chartType">
              <option>Select Type</option>
              <option value="line" data-name="Line" >Line</option>
              <option value="pie_chart">Pie Chart</option>
              <option value="column_chart">Column Chart</option>
            </select>
            </div>

         

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>


<script type="text/javascript">
  $(".addHeader").click(function(event) {
    event.preventDefault();
    $("#reportHeaderModel").modal('show');
  });

  $(".addProcedureVars").click(function(event) {
    event.preventDefault();
    $("#procedurevarsModel").modal('show');
  });

  $(".addChartInfo").click(function(event) {
    event.preventDefault();
    $("#chartInfoModel").modal('show');
  });

  var i= <?= (is_array($report['report_header'])) ? count($report['report_header']) : 0; ?>; ;
  var p= <?= (is_array($report['procedure_vars'])) ? count($report['procedure_vars']) : 0; ?>; 
  var c=<?= (is_array($report['chart_info'])) ? 1 : 0; ?>;

  if (c > 0 ) {
    $(".addChartInfo").hide();
  }

  $("#reportHeaderForm").submit(function(event) {
    event.preventDefault();
    var header= $('#reportHeaderForm #header').val();
    var badge='<li class="list-inline-item" ><div class="badge badge-primary mr-1">'+ header +'<i class="mdi mdi-close ml-2 removeHeader"></i> <input type="hidden" name="header['+i+']" value="'+header+'" > </div></li>';
    $("#reportHeader .headerList").append(badge);
    $('#reportHeaderForm').trigger('reset');
    $("#reportHeaderModel").modal('hide');
    i++;
  });


  $("#procedurevarsForm").submit(function(event) {
    event.preventDefault();
    var form = "#procedurevarsForm";
    var name= $( form +' #varName').val();
    var type= $( form +' #varType').val();
    var example= $( form +' #varExample').val();
    
    var html='<li><div class="card card-inverse-success mb-1" id="context-menu-simple"><div class="card-body p-1"> <p class="card-text">Name : '+ name +' , Type : '+type+' , Example : '+example+' <input type="hidden" name="procedurvars['+p+'][name]" value="'+name+'" ><input type="hidden" name="procedurvars['+p+'][type]" value="'+type+'" ><input type="hidden" name="procedurvars['+p+'][example]" value="'+example+'" > </p></div> <button type="button" class="close cardClose" aria-label="Close"><span aria-hidden="true">&times;</span></button> </div></li>';

    $("#procedureVars .varList").append(html);
    $('#procedurevarsForm').trigger('reset');
    $("#procedurevarsModel").modal('hide');
    p++;
  });


  $("#chartInfoForm").submit(function(event) {
    event.preventDefault();
    var form = "#chartInfoForm";
    var title= $(form + ' #c_title').val();
    var x_axis= $( form +' #x_axis').val();
    var y_axis= $( form +' #y_axis').val();
    var type= $( form +' #chartType').val();


     var html='<li><div class="card card-inverse-success mb-1" id="context-menu-simple"><div class="card-body p-1"> <p class="card-text">Title:'+ title +' ,x_axis : '+ x_axis +' , y_axis : '+y_axis+' , Type : '+type+' <input type="hidden" name="chart[title]" value="'+title+'" > <input type="hidden" name="chart[x_axis]" value="'+x_axis+'" ><input type="hidden" name="chart[y_axis]" value="'+y_axis+'" ><input type="hidden" name="chart[type]" value="'+type+'" > </p></div> <button type="button" class="close cardClose" aria-label="Close"><span aria-hidden="true">&times;</span></button> </div></li>';
     $("#chartInfo .chartInfoList").append(html);
     $('#chartInfoForm').trigger('reset');
     $("#chartInfoModel").modal('hide');
     $(".addChartInfo").hide();
     c++;
  });

  $("#reportHeader").on('click', '.removeHeader', function(event) {
    event.preventDefault();
    $(this).closest('li').remove();
    i-1;
    /* Act on the event */
  });

  $("#procedureVars").on('click', '.cardClose', function(event) {
    event.preventDefault();
    $(this).closest('li').remove();
    p-1;
    /* Act on the event */
  });

  $("#chartInfo").on('click', '.cardClose', function(event) {
    event.preventDefault();
    $(this).closest('li').remove();
    $(".addChartInfo").show();
    c-1;
    /* Act on the event */
  });

</script>