<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="<?= base_url('panel/') ?>assets/images/favicon.ico">

        <!-- App title -->
        <title>Login - Property Dashboard</title>

        <!-- Bootstrap CSS -->
        <link href="<?= base_url('panel/') ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="<?= base_url('panel/') ?>assets/css/style.css" rel="stylesheet" type="text/css" />


    </head>


    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">

        	<div class="account-bg">
                <div class="card-box mb-0">
                    <div class="text-center m-t-20">
                        <a href="index.html" class="logo">
                            <i class="zmdi zmdi-group-work icon-c-logo"></i>
                            <span>Uplon</span>
                        </a>
                    </div>
                    <div class="m-t-10 p-20">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h6 class="text-muted text-uppercase m-b-0 m-t-0">Sign In</h6>
                            </div>
                        </div>
                        <form class="m-t-20" method="post" action="<?= base_url('login') ?> ">

                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control" name="email" type="text" required="" placeholder="Email-Id">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control" name="password" type="password" required="" placeholder="Password">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="checkbox checkbox-custom">
                                        <input id="checkbox-signup" type="checkbox">
                                        <label for="checkbox-signup">
                                            Remember me
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group text-center row m-t-10">
                                <div class="col-12">
                                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>

                        </form>

                        <?php if(validation_errors()) :?>
                          <?php echo validation_errors(); ?>
                        <?php endif; ?>


                    </div>

                   
                </div>
            </div>
            <!-- end card-box-->

            

        </div>
        <!-- end wrapper page -->




        <!-- jQuery  -->


    </body>
</html>