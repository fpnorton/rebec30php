<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportcrud extends AmbrosiaController {
	protected $table="picolo_reports";

	public function __construct()
	{
		parent::__construct();
		$this->template->set_template( $this->config->item('path', 'app') ."/layouts/app");
		$this->load->model(['ReportModel'=>'rModel','GenericModel'=>'post']);
		$this->load->helper(['string']);
	}

	public function index()
	{
		$this->only_super();
		
		$data['reports']=$this->rModel->getReportCrud();
		$this->template->content->view('reportcrud/view',$data);
		$this->template->publish();	

	}

	function create(){
		$this->only_super();

		$this->template->content->view('reportcrud/create');
		$this->template->publish();	
	}

	function store(){
		$this->only_super();

		$post= $this->input->post();
		if ( $this->rModel->saveReportCrud($post)) {
			redirect( app_url('reportcrud'),'refresh');
		}
		//var_dump($post);
	}

	function view($id=null){
		$this->only_super();

		if (!empty($id)) {
			$r= $this->rModel->getReportCrudbyId($id);
			if (is_array($r)) {
				$data['report']= $r;
				$this->template->content->view('reportcrud/view_report',$data);
				$this->template->publish();	
			}
		}else{
			redirect( app_url('reportcrud'),'refresh');
		}
	}

	function edit($id=null){
		$this->only_super();

		if (!empty($id)) {
			$r= $this->rModel->getReportCrudbyId($id);
			if (is_array($r)) {
				$data['report']= $r;
				$this->template->content->view('reportcrud/edit',$data);
				$this->template->publish();	
			}
		}else{
			redirect( app_url('reportcrud'),'refresh');
		}
	}

	function update(){
		$this->only_super();

		$post= $this->input->post();
		if ( $this->rModel->updateReportCrud($post)) {
			redirect( app_url('reportcrud'),'refresh');
		}
	}

	function delete($id=null){
		$this->only_super();

		if (!empty($id)) {
			$this->post->setTable($this->table);
			$data=['isDelete'=>1];
			$q= $this->post->where('id',$id)->update($data);
			if ($q) {
				
			}else{
				
			}
		}

		redirect( app_url('reportcrud'),'refresh');
	}

	function active($id=null){
		$this->only_super();

		if (!empty($id)) {
			$this->post->setTable($this->table);
			$data=['isDelete'=>0];
			$q= $this->post->where('id',$id)->update($data);
			if ($q) {
				
			}else{
				
			}
		}

		redirect( app_url('reportcrud'),'refresh');
	}




}

/* End of file Makereport.php */
/* Location: ./application/modules/ambrosia/controllers/Makereport.php */