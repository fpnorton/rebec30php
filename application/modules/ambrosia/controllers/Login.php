<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends AmbrosiaController {

	public function index()
	{
		$this->load->view('login');
	}

}

/* End of file Login.php */
/* Location: ./application/modules/webpanel/controllers/Login.php */