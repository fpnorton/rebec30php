<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends AmbrosiaController {

	protected $table="picolo_reports";

	public function __construct()
	{
		parent::__construct();
		$this->template->set_template( $this->config->item('path', 'app') ."/layouts/app");
		$this->load->model(['ReportModel'=>'rModel','GenericModel'=>'post']);
		$this->load->helper(['string']);
		//isLogin();
	}

	public function index()
	{
		$this->only_reviewer();

		//$this->post->setTable("picolo_reports");
		$data['reports']= $this->rModel->getReportTable();

		// $this->post->fields('id,report_name,procedure_vars,is_query,is_procedure')->get_all();
		//var_dump($q);
		 $this->template->content->view('report/view',$data);
		 $this->template->publish();	
	}

	function show(){
		$this->only_reviewer();

		// get url values
		$get= $this->input->get();

		$report= (isset($get['report'])) ? $get['report'] : null ;
		$this->post->setTable($this->table);
		$result=null;

		if ($report !=null) {
			$r= $this->post->as_object()->get($report);
			$output= array();
			//check report is prcedure or query
			// if procedure is true
			if ($r->is_procedure=='T') {
				$vars= $this->rModel->arrayunserialize($r->procedure_vars);

				if (is_array($vars)) {

					$count = count($vars); //count varables
					$data=[];

					//set up procedure call para
					$string = "?, ";
					$rep=  repeater($string, $count);
					$para= reduce_multiples($rep, ", ", TRUE);
					$parawithfn="(".$para.")";

					//map varables with the get value
					foreach ($vars as $key => $var) {
						$data[$var['name']]= $get[$var['name']];
					}
					//call of model function to get result
					$result= $this->rModel->callProcedure($r->procedure_name,$parawithfn,$data);
				}

			}
			// if query is true
			if ($r->is_query=='T') {
				$result= $this->rModel->getReport($get);
			}

			$output['report']['name']= $r->report_name ;
			$output['report']['header']= $this->rModel->arrayunserialize($r->report_header);;
			$output['report']['data']= $result;

			// pass value to view
			
				$data['report']= $result;
				$this->template->content->view('report/detail',$output);
		 		$this->template->publish();	
			

		}else{
			redirect( app_url('report') ,'refresh');
		}

	}


	function chart(){
		$this->only_reviewer();

		$get= $this->input->get();

		$report= (isset($get['report'])) ? $get['report'] : null ;
		$this->post->setTable($this->table);
		$result=null;
		$chartInfo= null;


		if ($report !=null) {
			$r= $this->post->as_object()->get($report);
			$output= array();
			//check if report has chart
			if ($r->is_chart =='T') {
			$chartInfo= $this->rModel->arrayunserialize($r->chart_info);

			//check report is prcedure or query
			// if procedure is true
			if ($r->is_procedure=='T') {
				$vars= $this->rModel->arrayunserialize($r->procedure_vars);

				if (is_array($vars)) {

					$count = count($vars); //count varables
					$data=[];

					//set up procedure call para
					$string = "?, ";
					$rep=  repeater($string, $count);
					$para= reduce_multiples($rep, ", ", TRUE);
					$parawithfn="(".$para.")";

					//map varables with the get value
					foreach ($vars as $key => $var) {
						$data[$var['name']]= $get[$var['name']];
					}
					//call of model function to get result
					$result= $this->rModel->callProcedure($r->procedure_name,$parawithfn,$data);
				}

			}
			// if query is true
				if ($r->is_query=='T') {
					$result= $this->rModel->getReport($get);
				}

		}

		$gChart= $this->chartDate($this->rModel->arrayunserialize($r->report_header),$result);

		//var_dump($gChart);

			$output['chart']['header']= $gChart;
			$output['report']['name']= $r->report_name ;
			$output['report']['header']= $this->rModel->arrayunserialize($r->report_header);;
			$output['report']['data']= $result;
			$output['chartInfo']= $chartInfo;
			// pass value to view
			$output['get']= $get;
				$data['report']= $result;
				$this->template->content->view('report/chart',$output);
		 		$this->template->publish();	
			

		}else{
			redirect( app_url('report') ,'refresh');
		}
	}


	function chartDate($header,$data){
		$this->only_reviewer();
		//var_dump($data);

		$str="";
		$output="";
		$dstr="";
		//$dat="";
		$doutput="";

		if (is_array($header)) {

			foreach ($header as $key => $value) {
			  $str= $str."'".$value."',";
			}
			$para= reduce_multiples($str, ",", TRUE);
			$output= "[". $para ."]";
		}

		if (is_array($data)) {
			foreach ($data as $key => $value) {
			  $z="";
			  foreach ($value as $i) {
			  	$z= $z."'".$i."',";
			  }
			  $zp= reduce_multiples($z, ",", TRUE);
			  $dstr= $dstr. "[".$zp ."],";
			}
			$doutput= reduce_multiples($dstr, ",", TRUE);
			//$doutput= "[". $para ."]";
		}

		$result= "[".$output. ",". $doutput."]";

		return $result;
	}



}

/* End of file Report.php */
/* Location: ./application/modules/ambrosia/controllers/Report.php */