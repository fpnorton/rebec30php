<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('app_url'))
{

  function app_url($url = null, $protocol = NULL)
  {
    $c="";
    $ci=& get_instance();
    $ci->config->load('app');
    $path= $ci->config->item('path');

    if ($url!=null) {
         $c="/".$url;
    }
    $web= $path.$c  ;
    return $ci->config->base_url($web, $protocol);
  }

}

if ( ! function_exists('isLogin'))
{

  function isLogin()
  {
    $ci=& get_instance();
    if (!$ci->session->userdata('userId') && !$ci->session->userdata('role') && !$ci->session->userdata('isLogin') ) {
      redirect( base_url('login') ,'refresh');
    }else{
      $ci->load->model('loginModel');
      return $ci->loginModel->login();
    }
  }

}

if ( ! function_exists('checkLogin'))
{

  function checkLogin()
  {
    $ci=& get_instance();
    if (!$ci->session->userdata('userId') && !$ci->session->userdata('role') && !$ci->session->userdata('isLogin') ) {
      return false;
    }else{
      $ci->load->model('loginModel');
      return $ci->loginModel->login();
    }
  }

}




