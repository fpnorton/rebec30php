<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('FlatPageForm'); $fieldlabel->enableUpdate(); ?>
<div class="row ">
    <div class="col-xs-1"></div>
    <div class="col-xs-10">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-square fa-1g" aria-hidden="true"></i> <?php echo $genericlabel->get_or_new('menu_flat_pages_manager') ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
<!-- ############################################################################################################### -->
            <table id="datatable" class="table table-hover table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th><?php echo $fieldlabel->get_or_new('head_url') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_title') ?></th>
                        <th class="text-center"><?php echo $fieldlabel->get_or_new('head_actions') ?></th>
                    </tr>
                </thead>
                <?php foreach(($pages ?? []) as $page) : ?>
                    <?php if (linguagem_selecionada() == 'en') : ?>
                        <?php $translation = $page ?>
                    <?php else : ?>
                        <?php $translation = $page->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                    <?php endif; ?>
                    <tr>
                        <td><?php echo $page->url ?></td>
                        <td><?php echo $translation->title ?></td>
                        <td class="text-center">
                            <a href="<?php echo base_url() ?>eris/flatPages/edit/<?php echo $page->id ?>">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <? $object_id = $page->id; ?>
                            <? $class_name = 'flatPages'; ?>
                            <a href="#" data-toggle="modal" data-target="#<?php echo 'modal_'.$object_id ?>">
                                <i class="fa fa-eraser red-text"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <?php foreach(($pages ?? []) as $page) : ?>
                <? $object_id = $page->id; ?>
                <? $class_name = 'flatPages'; ?>
                <div class="modal fade"
                     id="<?php echo 'modal_'.$object_id ?>"
                     tabindex="-1"
                     role="dialog"
                     aria-labelledby="<?php echo 'modalLabel_'.$object_id ?>"
                     aria-hidden="true">
                    <div class="modal-dialog modal-sm"
                         role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"
                                    id="<?php echo 'modalLabel_'.$object_id ?>"><?php echo $genericlabel->get_or_new('title_exclusion'); ?>
                                </h5>
                                <button type="button"
                                        class="close"
                                        data-dismiss="modal"
                                        aria-label="<?php echo $genericlabel->get_or_new('button_close'); ?>">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body"><?php echo $fieldlabel->get_or_new('text_exclusion_confirmation') ?>
                            </div>
                            <div class="modal-footer">
                                <form action="<?php echo base_url() ?>eris/<? echo $class_name ?>/delete/<?php echo $object_id ?>">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $genericlabel->get_or_new('button_close'); ?></button>
                                    <button type="submit" class="btn btn-danger"><?php echo $genericlabel->get_or_new('button_confirm'); ?></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
            <?php //echo @$this->pagination->create_links(); ?>
        </div>
      </div><!-- /.box -->
    </div>
    <div class="col-xs-1"></div>
    <script>
        jQuery(window).ready(function(){
            $('.datatable').on('init.dt', function() {
                $("div.datatable-toolbar")
                    .html('<a href="<?php echo base_url('/eris/flatPages/create') ?>"><button type="button" class="btn btn-primary"><?php echo $fieldlabel->get_or_new('create') ?></button></a>');
            });
        });
    </script>
</div>
