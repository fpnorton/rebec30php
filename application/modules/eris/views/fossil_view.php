<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('Fossil'); $fieldlabel->enableUpdate(); ?>
<?php $fosseis = $fosseis ?? [$fossil]; ?>
<style><!--
    .container {
    /*    margin: 0 auto;*/
    /*    width: 1004px;*/
        line-height: 1.7em;
    /*    background: #fff;*/
    }
    span.label, span.legend {
        font-weight: bold;
    }
    H2, H2 a {
        color: #006837;
        margin: 0px;
        text-decoration: none;
        font-size: 19px;
    }
    h3 {
        display: block;
        font-size: 1.17em;
        margin-block-start: 1em;
        margin-block-end: 1em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;
        font-weight: bold;
    }
    section {
        margin: 0;
        padding: 0;
        text-align: left;
        font-size: 95%;
        font-family: 'Lato', sans-serif;
        background: #f2f2f0;
    }
    .balloon {
        border: 6px solid #ffffff;
        border-radius: 16px;
        -moz-border-radius: 16px;
        -webkit-border-radius: 16px;
        background-color: #999999;
    }
    .subset {
        border: 1px solid #000000;
        border-radius: 5px;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        padding: 10px;
        margin: 10px;
    }
    table.dataTable TH {
        color: #0c4762;
        font-size: 90%;
        background: #dedede;
    }
    table.dataTable TH, table.dataTable TD {
        border-bottom: 1px solid #c9c9c9;
        font-size: 85%;
        padding: 6px 4px;
    }
--></style>
<section class="row">
    <div class="col-12">
        <div class="row" > <!-- style="max-height: 600px; overflow:auto; " -->
            <section class="container" >
                <span class="welcome_subtitle">
                    <h5><?php echo $fieldlabel->get_or_new('main_title') ?></h5>
                    <hr />
                </span>
                <?php foreach ($fosseis as $fossil) : ?>
                    <pre><?php 
                    $clinicaltrial = \Repository\ClinicalTrial::find($fossil->object_id);
                    $revision_1 = \Fossil\Fossil::where('content_type_id', '=', 24)
                        ->where('revision_sequential', '=', 1)
                        ->where('object_id', '=', $fossil->object_id)
                        ->first();
                    ?></pre>
                    <?php $json = json_decode($fossil->serialized, true); ?>
                    <pre><?php 
                    // echo var_export([
                    //     $json['date_enrollment_start'],
                    //     strtotime($json['date_enrollment_start']),
                    //     DateTime::createFromFormat('d/m/Y', $json['date_enrollment_start']),
                    //     $json
                    //     'layout' => $layout,
                    //     'first_revision_approved_date' => $first_revision_approved_date,
                    // ], TRUE);
                    ?></pre>
                    <div class="card menu_dashboard">
                        <div class="card-title">
                            <h4><?php echo $data['public_title'] ?></h4>
                            <hr />
                        </div>
                        <div class="card-body text-justify">
                            <div class="mainContent">
                                <h2><?php echo $fossil->display_text ?></h2>

                                <span class="label"><?php echo $fieldlabel->get_or_new('first_revision_approved_date'); ?>:</span>
                                <span class="value">
                                    <?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($revision_1->creation)) ?>
                                    <sup>(<?php echo @\Helper\Language::$available[linguagem_selecionada()]['date_sample'] ?>)</sup>
                                </span>
                                <br>
                                <span class="label"><?php echo $fieldlabel->get_or_new('revision_approved_date'); ?>:</span>
                                <span class="value">
                                    <?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($fossil->creation)) ?>
                                    <sup>(<?php echo @\Helper\Language::$available[linguagem_selecionada()]['date_sample'] ?>)</sup>
                                </span>
<? /* ?>
                                <br>
                                <span class="label"><?php echo $fieldlabel->get_or_new('revision_last_update'); ?>:</span>
                                <span class="value">
                                    <?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($json['updated'] ?? $fossil->creation)) ?>
                                    <sup>(<?php echo @\Helper\Language::$available[linguagem_selecionada()]['date_sample'] ?>)</sup>
                                </span>
<? // */ ?>
                                <h3><?php echo $fieldlabel->get_or_new('study_type'); ?>:</h3>

                                <p><?php echo $json['is_observational'] ? $fieldlabel->get_or_new('is_observational') : $fieldlabel->get_or_new('is_interventional') ?></p>

                                <div class="spacer"> </div>

                                <div class="spacer"> </div>

                                <h3><?php echo $fieldlabel->get_or_new('scientific_title'); ?>:</h3>

                                <div class="row">
                                    <?php $key = 'scientific_title' ?>
                                    <?php
                                    foreach (\Helper\Language::$available as $language_key => $language_value) :
                                        $text = $json[$key];
                                        foreach ($json['translations'] as $translation) :
                                            if ($translation['language'] == $language_key) :
                                                $text = $translation[$key];
                                                break;
                                            endif;
                                        endforeach;
                                        if (empty($text)) continue;
                                        ?><div class="col-sm-3 balloon">
                                        <h2><?php echo $language_key ?></h2>
                                        <p><?php echo $text ?></p>
                                        </div><?
                                    endforeach;
                                    ?>
                                </div>

                                <div class="spacer"> </div>

<?php $fieldlabel = new Helper\FieldLabelLanguage('IdentificationFossil'); $fieldlabel->enableUpdate(); ?>

                                <h3><?php echo $fieldlabel->get_or_new('main_title'); ?></h3>
                                <ul>
                                    <li>
                                        <span class="label"><?php echo $fieldlabel->get_or_new('utn_code'); ?>: </span>
                                        <span class="value"><?php echo $json['utrn_number'] ?></span>
                                    </li>
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('public_title'); ?>:</span></p>
                                        <div class="row">
                                            <?php $key = 'public_title' ?>
                                            <?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $json[$key];
                                                } else {
                                                    $text = '';
                                                    foreach ($json['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation[$key];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                    if (empty($text)) continue;
                                                }
                                                ?><div class="col-sm-3 balloon">
                                                    <h2><?php echo $language_key ?></h2>
                                                    <p><?php echo $text ?></p>
                                                </div><?
                                            endforeach;
                                            ?>
                                        </div>
                                    </li>
                                    <div class="spacer"> </div>
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('scientific_acronym'); ?>:</span></p>
                                        <div class="row">
                                            <?php $key = 'scientific_acronym' ?>
                                            <?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $json[$key];
                                                } else {
                                                    $text = '';
                                                    foreach ($json['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation[$key];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                }
                                                if (empty($text)) continue;
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p><?php echo $text ?></p>
                                                </div><?
                                            endforeach;
                                            ?>
                                        </div>
                                    </li>
                                    <div class="spacer"> </div>
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('acronym'); ?>:</span></p>
                                        <div class="row">
                                            <?php $key = 'acronym' ?>
                                            <?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $json[$key];
                                                } else {
                                                    $text = '';
                                                    foreach ($json['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation[$key];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                }
                                                if (empty($text)) continue;
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p><?php echo $text ?></p>
                                                </div><?
                                            endforeach;
                                            ?>
                                        </div>
                                    </li>
                                    <div class="spacer"> </div>
                                    <li class="subset">
                                        <span class="legend"><?php echo $fieldlabel->get_or_new('secondaries_identifiers_title'); ?>:</span>
                                        <? $odd = true; ?>
                                        <? foreach ($json['trial_number'] as $item) : ?>
                                            <ul class="<? echo $odd ? 'odd' : 'even' ?>">
                                                <li>
                                                    <span class="label"><?php echo $item['id_number'] ?></span><br>
                                                    <span class="value"><?php echo $fieldlabel->get_or_new('issuing_authority'); ?>:</span>
                                                    <span class="value"><?php echo $item['issuing_authority'] ?></span>
                                                </li>
                                            </ul>
                                            <? $odd = !$odd ?>
                                        <? endforeach; ?>
                                    </li>
                                </ul>

<?php  $fieldlabel = new Helper\FieldLabelLanguage('SponsorFossil'); $fieldlabel->enableUpdate(); ?>

                                <h3><?php echo $fieldlabel->get_or_new('main_title'); ?></h3>
                                <ul>
                                    <li>
                                        <span class="label"><?php echo $fieldlabel->get_or_new('primary_sponsor'); ?>:</span>
                                        <span class="value"><?php echo $json['primary_sponsor']['name'] ?></span>
                                    </li>
                                    <li class="subset">
                                        <span class="legend"><?php echo $fieldlabel->get_or_new('secondary_sponsor'); ?>:</span>
                                        <? $odd = true; ?>
                                        <? foreach ($json['secondary_sponsors'] as $item) : ?>
                                        <? $institution_name = $item['institution']['name']; ?>
                                        <? if (is_array($item['institution']['pk'])) $institution_name = $item['institution']['pk'][0]['name']; ?>
                                            <ul class="<? echo $odd ? 'odd' : 'even' ?>">
                                                <li>
                                                    <span class="label"><?php echo $fieldlabel->get_or_new('institution'); ?>:</span>
                                                    <span class="value"><?php echo $institution_name ?></span>
                                                </li>
                                            </ul>
                                            <? $odd = !$odd ?>
                                        <? endforeach; ?>
                                    </li>
                                    <li class="subset">
                                        <span class="legend"><?php echo $fieldlabel->get_or_new('supporting_source'); ?>:</span>
                                        <? $odd = true; ?>
                                        <? foreach ($json['support_sources'] as $item) : ?>
                                            <ul class="<? echo $odd ? 'odd' : 'even' ?>">
                                                <li>
                                                    <span class="label"><?php echo $fieldlabel->get_or_new('institution'); ?>:</span>
                                                    <span class="value"><?php echo $item['institution']['name'] ?></span>
                                                </li>
                                            </ul>
                                            <? $odd = !$odd ?>
                                        <? endforeach; ?>
                                    </li>
                                </ul>

<?php  $fieldlabel = new Helper\FieldLabelLanguage('HealthConditionFossil'); $fieldlabel->enableUpdate(); ?>

                                <h3><?php echo $fieldlabel->get_or_new('main_title'); ?></h3>
                                <ul>
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('health_conditions'); ?>:</span></p>
                                        <div class="row">
                                            <?php $key = 'hc_freetext' ?>
                                            <?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $json[$key];
                                                } else {
                                                    $text = '';
                                                    foreach ($json['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation[$key];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                    if (empty($text)) continue;
                                                }
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p><?php echo $text ?></p>
                                                </div><?
                                            endforeach;
                                            ?>
                                        </div>
                                    </li>
                                    <div class="spacer"> </div>
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('general_descriptors'); ?>:</span></p>
                                        <?php
                                        foreach ($json['hc_keyword'] as $hc_keyword) :
                                            if ($hc_keyword['level'] != 'general') continue;
                                            ?><div class="row"><?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $hc_keyword['text'];
                                                } else {
                                                    $text = '';
                                                    foreach ($hc_keyword['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation['text'];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                    if (empty($text)) continue;
                                                }
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p>
                                                    <span class="label"><?php echo $hc_keyword['code'] ?></span>
                                                    <span class="value"><?php echo $text; ?></span>
                                                </p>
                                                </div><?
                                            endforeach;
                                            ?></div><?php
                                        endforeach;
                                        ?>
                                        <?php
                                        foreach ($json['hc_code'] as $hc_keyword) :
                                            if ($hc_keyword['level'] != 'general') continue;
                                            ?><div class="row"><?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $hc_keyword['text'];
                                                } else {
                                                    $text = '';
                                                    foreach ($hc_keyword['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation['text'];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                    if (empty($text)) continue;
                                                }
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p>
                                                    <span class="label"><?php echo $hc_keyword['code'] ?></span>
                                                    <span class="value"><?php echo $text; ?></span>
                                                </p>
                                                </div><?
                                            endforeach;
                                            ?></div><?php
                                        endforeach;
                                        ?>
                                        <div class="spacer"> </div>
                                    </li>
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('specific_descriptors'); ?>:</span></p>
                                        <?php
                                        foreach ($json['hc_code'] as $hc_keyword) :
                                            if ($hc_keyword['level'] != 'specific') continue;
                                            ?><div class="row"><?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $hc_keyword['text'];
                                                } else {
                                                    $text = '';
                                                    foreach ($hc_keyword['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation['text'];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                    if (empty($text)) continue;
                                                }
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p>
                                                    <span class="label"><?php echo $hc_keyword['code'] ?></span>
                                                    <span class="value"><?php echo $text; ?></span>
                                                </p>
                                                </div><?
                                            endforeach;
                                            ?></div><?php
                                        endforeach;
                                        ?>
                                        <div class="spacer"> </div>
                                    </li>
                                </ul>

<?php  $fieldlabel = new Helper\FieldLabelLanguage('InterventionFossil'); $fieldlabel->enableUpdate(); ?>

                                <h3><?php echo $fieldlabel->get_or_new('main_title'); ?></h3>
                                <ul>
<!--                                    <li class="subset">-->
<!--                                        <span class="legend">--><?php //echo $fieldlabel->get_or_new('intervention_categories'); ?><!--</span>-->
<!--                                        <ul>-->
<!--                                            <li>-->
<!--                                                <span class="label">Other</span>-->
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </li>-->
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('intervention'); ?>:</span></p>
                                        <div class="row">
                                            <?php $key = 'i_freetext' ?>
                                            <?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $json[$key];
                                                } else {
                                                    $text = '';
                                                    foreach ($json['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation[$key];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                    if (empty($text)) continue;
                                                }
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p><?php echo $text ?></p>
                                                </div><?
                                            endforeach;
                                            ?>
                                        </div>
                                    </li>
                                    <div class="spacer"> </div>
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('descriptors'); ?>:</span></p>
                                        <?php
                                        foreach ($json['intervention_keyword'] as $i_keyword) :
                                            ?><div class="row"><?php
                                                foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                    if ($language_key == 'en') {
                                                        $text = $i_keyword['text'];
                                                    } else {
                                                        $text = '';
                                                        foreach ($i_keyword['translations'] as $translation) :
                                                            if ($translation['language'] == $language_key) :
                                                                $text = $translation['text'];
                                                                break;
                                                            endif;
                                                        endforeach;
                                                        if (empty($text)) continue;
                                                    }
                                                    ?><div class="col-sm-3 balloon">
                                                    <h2><?php echo $language_key ?></h2>
                                                    <p>
                                                        <span class="label"><?php echo $i_keyword['code'] ?></span>
                                                        <span class="value"><?php echo $text; ?></span>
                                                    </p>
                                                    </div><?
                                                endforeach;
                                            ?></div><?php
                                        endforeach;
                                        ?>
                                        <div class="spacer"> </div>
                                    </li>
                                </ul>

<?php  $fieldlabel = new Helper\FieldLabelLanguage('RecruitmentFossil'); $fieldlabel->enableUpdate(); ?>

                                <h3><?php echo $fieldlabel->get_or_new('main_title'); ?></h3>
                                <ul>
                                    <li>
                                        <span class="label"><?php echo $fieldlabel->get_or_new('study_status'); ?>:</span>
                                        <?php
                                        $text = '';
                                        $recruitment_status = $json['recruitment_status'];
                                        if (linguagem_selecionada() == 'en') {
                                            $text = $recruitment_status['label'];
                                        } else {
                                            foreach ($recruitment_status['translations'] as $translation) :
                                                if ($translation['language'] == linguagem_selecionada()) $text = $translation['label'];
                                            endforeach;
                                        }
                                        ?>
                                        <span class="value"><?php echo $text; ?></span>
                                    </li>
                                    <li class="subset">
                                        <span class="legend"><?php echo $fieldlabel->get_or_new('country'); ?></span>
                                        <ul>
                                            <?php
                                            $text = '';
                                            $recruitment_countries = $json['recruitment_country'];
                                            foreach ($recruitment_countries as $recruitment_country) :
                                                if (linguagem_selecionada() == 'en') {
                                                    $text = $recruitment_country['description'];
                                                } else {
                                                    foreach ($recruitment_country['translations'] as $translation) :
                                                        if ($translation['language'] == linguagem_selecionada()) $text = $translation['description'];
                                                    endforeach;
                                                }
                                                ?><li><?php echo $text; ?></li><?php
                                            endforeach;
                                            ?>
                                        </ul>
                                    </li>
                                    <li>
                                        <span class="label"><?php echo $fieldlabel->get_or_new('date_first_enrollment'); ?>:</span>
                                        <span class="value">
                                            <?
                                                $date_enrollment_start = strtotime($json['enrollment_start_actual']); // ISO
                                                if ($date_enrollment_start == FALSE) $date_enrollment_start = DateTime::createFromFormat('d/m/Y', $json['date_enrollment_start']);
                                                if ($date_enrollment_start == FALSE) $date_enrollment_start = DateTime::createFromFormat('Y-m-d', $json['date_enrollment_start']);
                                                if (is_object($date_enrollment_start)) $date_enrollment_start = $date_enrollment_start->getTimestamp();
                                            ?>
                                            <?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], $date_enrollment_start) ?>
                                            <sup>(<?php echo @\Helper\Language::$available[linguagem_selecionada()]['date_sample'] ?>)</sup>
                                        </span>

                                    </li>
                                    <? if (@$json['enrollment_end_planned'] != NULL) : ?>
                                    <li>
                                        <span class="label"><?php echo $fieldlabel->get_or_new('date_last_enrollment'); ?>:</span>
                                        <span class="value">
                                            <?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($json['enrollment_end_planned'])) ?>
                                            <sup>(<?php echo @\Helper\Language::$available[linguagem_selecionada()]['date_sample'] ?>)</sup>
                                        </span>
                                    </li>
                                    <? endif; ?>
                                    <li>
                                        <table class="dataTable">
                                            <tbody><tr>
                                                <th><?php echo $fieldlabel->get_or_new('target_sample_size'); ?>:</th>
                                                <th><?php echo $fieldlabel->get_or_new('gender'); ?>:</th>
                                                <th><?php echo $fieldlabel->get_or_new('min_age'); ?>:</th>
                                                <th><?php echo $fieldlabel->get_or_new('max_age'); ?>:</th>
                                            </tr>
                                            <tr>
                                                <td><? echo $json['target_sample_size'] ?></td>
                                                <td><? echo $json['gender'] ?></td>
                                                <td><? echo $json['agemin_value'].' '.$json['agemin_unit'] ?></td>
                                                <td><? echo $json['agemax_value'].' '.$json['agemax_unit'] ?></td>
                                            </tr>
                                            </tbody></table>
                                    </li>
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('inclusion_criteria'); ?>:</span></p>
                                        <div class="row">
                                            <?php $key = 'inclusion_criteria' ?>
                                            <?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $json[$key];
                                                } else {
                                                    $text = '';
                                                    foreach ($json['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation[$key];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                    if (empty($text)) continue;
                                                }
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p><?php echo $text ?></p>
                                                </div><?
                                            endforeach;
                                            ?>
                                        </div>
                                    </li>
                                    <div class="spacer"> </div>
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('exclusion_criteria'); ?>:</span></p>
                                        <div class="row">
                                            <?php $key = 'exclusion_criteria' ?>
                                            <?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $json[$key];
                                                } else {
                                                    $text = '';
                                                    foreach ($json['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation[$key];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                    if (empty($text)) continue;
                                                }
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p><?php echo $text ?></p>
                                                </div><?
                                            endforeach;
                                            ?>
                                        </div>
                                    </li>
                                    <div class="spacer"> </div>
                                </ul>

<?php  $fieldlabel = new Helper\FieldLabelLanguage('StudyTypeFossil'); $fieldlabel->enableUpdate(); ?>

                                <h3><?php echo $fieldlabel->get_or_new('main_title'); ?></h3>
                                <ul>
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('study_design'); ?>:</span></p>
                                        <div class="row">
                                            <?php $key = 'study_design' ?>
                                            <?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $json[$key];
                                                } else {
                                                    $text = '';
                                                    foreach ($json['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation[$key];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                    if (empty($text)) continue;
                                                }
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p><?php echo $text ?></p>
                                                </div><?
                                            endforeach;
                                            ?>
                                        </div>
                                    </li>
                                    <div class="spacer"> </div>
                                    <li>
                                        <?php
                                        $purpose = '';
                                        if (linguagem_selecionada() == 'en') {
                                            $purpose = $json['purpose']['label'];
                                        } else {
                                            foreach ($json['purpose']['translations'] as $translation) :
                                                if ($translation['language'] == linguagem_selecionada()) $purpose = $translation['label'];
                                            endforeach;
                                        }
                                        ?>
                                        <?php
                                        $intervention_assignment = '';
                                        if (linguagem_selecionada() == 'en') {
                                            $intervention_assignment = $json['intervention_assignment']['label'];
                                        } else {
                                            foreach ($json['intervention_assignment']['translations'] as $translation) :
                                                if ($translation['language'] == linguagem_selecionada()) $intervention_assignment = $translation['label'];
                                            endforeach;
                                        }
                                        ?>
                                        <?php
                                        $masking = '';
                                        if (linguagem_selecionada() == 'en') {
                                            $masking = $json['masking']['label'];
                                        } else {
                                            foreach ($json['masking']['translations'] as $translation) :
                                                if ($translation['language'] == linguagem_selecionada()) $masking = $translation['label'];
                                            endforeach;
                                        }
                                        ?>
                                        <?php
                                        $allocation = '';
                                        if (linguagem_selecionada() == 'en') {
                                            $allocation = $json['allocation']['label'];
                                        } else {
                                            foreach ($json['allocation']['translations'] as $translation) :
                                                if ($translation['language'] == linguagem_selecionada()) $allocation = $translation['label'];
                                            endforeach;
                                        }
                                        ?>
                                        <?php
                                        $study_phase = '';
                                        if (linguagem_selecionada() == 'en') {
                                            $study_phase = $json['phase']['label'];
                                        } else {
                                            foreach ($json['phase']['translations'] as $translation) :
                                                if ($translation['language'] == linguagem_selecionada()) $study_phase = $translation['label'];
                                            endforeach;
                                        }
                                        ?>
                                        <table class="dataTable">
                                            <thead>
                                                <tr>
                                                    <th><span class="label"><?php echo $fieldlabel->get_or_new('expanded_access_program'); ?></span></th>
                                                    <th><span class="label"><?php echo $fieldlabel->get_or_new('purpose'); ?></span></th>
                                                    <th><span class="label"><?php echo $fieldlabel->get_or_new('intervention_assignment'); ?></span></th>
                                                    <th><span class="label"><?php echo $fieldlabel->get_or_new('number_arms'); ?></span></th>
                                                    <th><span class="label"><?php echo $fieldlabel->get_or_new('masking_type'); ?></span></th>
                                                    <th><span class="label"><?php echo $fieldlabel->get_or_new('allocation'); ?></span></th>
                                                    <th><span class="label"><?php echo $fieldlabel->get_or_new('study_phase'); ?></span></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><span class="value"><?php echo $json['expanded_access_program'] ?? '' ?></span></td>
                                                <td><span class="value"><?php echo $purpose ?? '' ?></span></td>
                                                <td><span class="value"><?php echo $intervention_assignment ?? '' ?></span></td>
                                                <td><span class="value"><?php echo $json['number_of_arms'] ?? '' ?></span></td>
                                                <td><span class="value"><?php echo $masking ?? '' ?></span></td>
                                                <td><span class="value"><?php echo $allocation ?? '' ?></span></td>
                                                <td><span class="value"><?php echo $study_phase ?? '' ?></span></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                </ul>

<?php $fieldlabel = new Helper\FieldLabelLanguage('OutcomeFossil'); $fieldlabel->enableUpdate(); ?>

                                <h3><?php echo $fieldlabel->get_or_new('main_title'); ?></h3>
                                <ul>
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('primary_outcomes'); ?>:</span></p>
                                        <?php
                                        foreach ($json['primary_outcomes'] as $outcome) :
                                            ?><div class="row"><?php
                                                foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                    if ($language_key == 'en') {
                                                        $text = $outcome['description'];
                                                    } else {
                                                        $text = '';
                                                        foreach ($outcome['translations'] as $translation) :
                                                            if ($translation['language'] == $language_key) :
                                                                $text = $translation['description'];
                                                                break;
                                                            endif;
                                                        endforeach;
                                                        if (empty($text)) continue;
                                                    }
                                                    ?><div class="col-sm-3 balloon">
                                                    <h2><?php echo $language_key ?></h2>
                                                    <p>
                                                        <span class="value"><?php echo $text; ?></span>
                                                    </p>
                                                    </div><?
                                                endforeach;
                                            ?></div><?php
                                        endforeach;
                                        ?>
                                        <div class="spacer"> </div>
                                    </li>
                                    <li>
                                        <p><span class="label"><?php echo $fieldlabel->get_or_new('secondary_outcomes'); ?>:</span></p>
                                        <?php
                                        foreach ($json['secondary_outcomes'] as $outcome) :
                                            ?><div class="row"><?php
                                            foreach (\Helper\Language::$available as $language_key => $language_value) :
                                                if ($language_key == 'en') {
                                                    $text = $outcome['description'];
                                                } else {
                                                    $text = '';
                                                    foreach ($outcome['translations'] as $translation) :
                                                        if ($translation['language'] == $language_key) :
                                                            $text = $translation['description'];
                                                            break;
                                                        endif;
                                                    endforeach;
                                                    if (empty($text)) continue;
                                                }
                                                ?><div class="col-sm-3 balloon">
                                                <h2><?php echo $language_key ?></h2>
                                                <p>
                                                    <span class="value"><?php echo $text; ?></span>
                                                </p>
                                                </div><?
                                            endforeach;
                                            ?></div><?php
                                        endforeach;
                                            ?>
                                        <div class="spacer"> </div>
                                    </li>
                                </ul>

<?php $fieldlabel = new Helper\FieldLabelLanguage('ContactFossil'); $fieldlabel->enableUpdate(); ?>

                                <h3><?php echo $fieldlabel->get_or_new('main_title'); ?></h3>
                                <ul>

                                    <li class="subset">
                                        <span class="legend"><?php echo $fieldlabel->get_or_new('type_public'); ?></span>

                                        <ul class="vcard">
                                            <?php foreach ($json['public_contact'] as  $contact) : ?>
                                                <li>
                                                    <b><?php echo $fieldlabel->get_or_new('full_name'); ?>:</b>
                                                    <span class="fn"><?php echo implode(' ', [$contact['firstname'], $contact['middlename'], $contact['lastname']]) ?></span>
                                                </li>
                                                <li>
                                                    <ul class="adr">
                                                        <li>
                                                            <b><?php echo $fieldlabel->get_or_new('address'); ?>:</b>
                                                            <span class="street-address"><?php echo $contact['address'] ?></span>
                                                        </li>
                                                        <li>
                                                            <b><?php echo $fieldlabel->get_or_new('city'); ?>: </b>
                                                            <span><span class="locality"><?php echo $contact['city'] ?></span> / <span class="country-name"><?php echo $contact['country']['description'] ?></span></span>
                                                        </li>
                                                        <li>
                                                            <b><?php echo $fieldlabel->get_or_new('zip'); ?>:</b>
                                                            <span class="postal-code"><?php echo $contact['zip'] ?></span>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <b><?php echo $fieldlabel->get_or_new('phone'); ?>:</b>
                                                    <span class="tel"><?php echo $contact['telephone'] ?></span>
                                                </li>
                                                <li>
                                                    <b><?php echo $fieldlabel->get_or_new('email'); ?>:</b>
                                                    <span class="email"><?php echo $contact['email'] ?></span>
                                                </li>
                                                <li>
                                                    <b><?php echo $fieldlabel->get_or_new('affiliation'); ?>:</b>
                                                    <span class="org"><?php echo $contact['affiliation']['name'] ?></span>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <div class="spacer">&nbsp;</div>
                                    </li>
                                    <li class="subset">
                                        <span class="legend"><?php echo $fieldlabel->get_or_new('type_scientific'); ?></span>

                                        <ul class="vcard">
                                            <?php foreach ($json['scientific_contact'] as  $contact) : ?>
                                                <li>
                                                    <b><?php echo $fieldlabel->get_or_new('full_name'); ?>:</b>
                                                    <span class="fn"><?php echo implode(' ', [$contact['firstname'], $contact['middlename'], $contact['lastname']]) ?></span>
                                                </li>
                                                <li>
                                                    <ul class="adr">
                                                        <li>
                                                            <b><?php echo $fieldlabel->get_or_new('address'); ?>:</b>
                                                            <span class="street-address"><?php echo $contact['address'] ?></span>
                                                        </li>
                                                        <li>
                                                            <b><?php echo $fieldlabel->get_or_new('city'); ?>:</b>
                                                            <span><span class="locality"><?php echo $contact['city'] ?></span> / <span class="country-name"><?php echo $contact['country']['description'] ?></span></span>
                                                        </li>
                                                        <li>
                                                            <b><?php echo $fieldlabel->get_or_new('zip'); ?>:</b>
                                                            <span class="postal-code"><?php echo $contact['zip'] ?></span>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <b><?php echo $fieldlabel->get_or_new('phone'); ?>:</b>
                                                    <span class="tel"><?php echo $contact['telephone'] ?></span>
                                                </li>
                                                <li>
                                                    <b><?php echo $fieldlabel->get_or_new('email'); ?>:</b>
                                                    <span class="email"><?php echo $contact['email'] ?></span>
                                                </li>
                                                <li>
                                                    <b><?php echo $fieldlabel->get_or_new('affiliation'); ?>:</b>
                                                    <span class="org"><?php echo $contact['affiliation']['name'] ?></span>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <div class="spacer">&nbsp;</div>
                                    </li>
                                    <li class="subset">
                                        <span class="legend"><?php echo $fieldlabel->get_or_new('type_site'); ?></span>

                                        <ul class="vcard">
                                            <?php foreach ($json['site_contact'] as  $contact) : ?>
                                                <li>
                                                    <b><?php echo $fieldlabel->get_or_new('full_name'); ?>:</b>
                                                    <span class="fn"><?php echo implode(' ', [$contact['firstname'], $contact['middlename'], $contact['lastname']]) ?></span>
                                                </li>
                                                <li>
                                                    <ul class="adr">
                                                        <li>
                                                            <b><?php echo $fieldlabel->get_or_new('address'); ?>:</b>
                                                            <span class="street-address"><?php echo $contact['address'] ?></span>
                                                        </li>
                                                        <li>
                                                            <b><?php echo $fieldlabel->get_or_new('city'); ?>:</b>
                                                            <span><span class="locality"><?php echo $contact['city'] ?></span> / <span class="country-name"><?php echo $contact['country']['description'] ?></span></span>
                                                        </li>
                                                        <li>
                                                            <b><?php echo $fieldlabel->get_or_new('zip'); ?>:</b>
                                                            <span class="postal-code"><?php echo $contact['zip'] ?></span>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <b><?php echo $fieldlabel->get_or_new('phone'); ?>:</b>
                                                    <span class="tel"><?php echo $contact['telephone'] ?></span>
                                                </li>
                                                <li>
                                                    <b><?php echo $fieldlabel->get_or_new('email'); ?>:</b>
                                                    <span class="email"><?php echo $contact['email'] ?></span>
                                                </li>
                                                <li>
                                                    <b><?php echo $fieldlabel->get_or_new('affiliation'); ?>:</b>
                                                    <span class="org"><?php echo $contact['affiliation']['name'] ?></span>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <div class="spacer">&nbsp;</div>
                                    </li>
                                </ul>

<?php $fieldlabel = new Helper\FieldLabelLanguage('Fossil'); $fieldlabel->enableUpdate(); ?>

                                <h3><?php echo $fieldlabel->get_or_new('additional_links'); ?>:</h3>
                                <? if ($fossil->revision_sequential > 1) : ?>
                                    <ul>
                                        <li><a href="<?php echo base_url('/rg/'.$clinicaltrial->trial_id.'/'.(intval($fossil->revision_sequential) - 1)) ?>"><?php echo $fieldlabel->get_or_new('previous_revision'); ?></a></li>
                                    </ul>
                                <? endif; ?>
                                <ul>
                                    <li><a href="<?php echo base_url('/xml_ictrp/downloadxmlictrp/'.$fossil->object_id) ?>"><?php echo $fieldlabel->get_or_new('download_ictrp'); ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                <?php endforeach; ?>
            </section>
        </div>
        <div>&nbsp;</div>
        <span><?php
//            echo '<pre>'.var_export(array_keys($json), TRUE).'</pre>';
//            echo '<pre>'.var_export($fossil, TRUE).'</pre>';
        ?></span>
    </div>
</section>
