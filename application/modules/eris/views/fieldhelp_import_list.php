<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('FieldHelpForm'); $fieldlabel->enableUpdate(); ?>
<?php
    $redis = new \Redis();
    $redis->connect('redis', 6379);
    $redis->auth('rebec');
?>
<div class="row ">
    <div class="col-xs-1"></div>
    <div class="col-xs-10">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-bookmark-o fa-1g" aria-hidden="true"></i> <?php echo $genericlabel->get_or_new('import_field_manager') ?></h3>
        <?php /*
            <div class="box-tools">
                <form action="<?php echo base_url() ?>userListing" method="POST" id="searchList">
                    <div class="input-group">
                      <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                </form>
            </div>
*/ ?>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
<!-- ############################################################################################################### -->
            <table id="datatable" class="table table-hover table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th></th>
                        <th><?php echo $fieldlabel->get_or_new('head_form') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_field') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_text') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_calls') ?></th>
                    </tr>
                </thead>
                <?php foreach(($fields ?? []) as $field) : ?>
                    <?php if (linguagem_selecionada() == 'en') : ?>
                        <?php $translation = $field ?>
                    <?php else : ?>
                        <?php foreach(($field['translations'] ?? []) as $item) : ?>
                            <?php $translation = $item ?>
                            <?php if ($item['language'] == linguagem_selecionada()) break; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php $use_counter = $redis->hGet('fieldHelp:'.$field['form'], $field['field']); ?>
                    <tr>
                        <td><? form_checkbox('fieldHelps[]', json_encode($field)) ?></td>
                        <td><?php echo $field['form'] ?></td>
                        <td><?php echo $field['field'] ?></td>
                        <td><?php echo character_limiter($translation['text'], 50); ?></td>
                        <td><?php echo ($use_counter == '' ? 0 : $use_counter); ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div><!-- /.box-body -->

        <div class="box-footer clearfix">
        </div>
      </div><!-- /.box -->
    </div>
    <div class="col-xs-1"></div>
    <script>
//        jQuery(window).ready(function(){
//            $('.datatable').on('init.dt', function() {
//                $("div.datatable-toolbar")
//                    .html(
//'<a href="<?php //echo base_url('/eris/fieldHelps/import') ?>//"><button type="button" class="btn btn-primary"><?php //echo $fieldlabel->get_or_new('fieldHelps_import') ?>//</button></a>' +
//'<a href="<?php //echo base_url('/eris/fieldHelps/export') ?>//"><button type="button" class="btn btn-primary"><?php //echo $fieldlabel->get_or_new('fieldHelps_export') ?>//</button></a>'
//                    );
//            });
//        });
    </script>
</div>
