<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('WelcomeForm'); $fieldlabel->enableUpdate(); ?>
<?php if (linguagem_selecionada() == 'en') :?>
    <?php $translation = $page; ?>
<?php else : ?>
    <?php $translation = $page->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
<?php endif; ?>
<section class="row">
    <div class="col-7">
        <div class="row">
            <section class="container">
                <span class="welcome_subtitle">
                    <h5>&nbsp;</h5>
                    <hr />
                </span>
                <div class="card menu_dashboard">
                    <div class="card-title">
                        <h4><?php echo $translation->title ?></h4>
                        <hr />
                    </div>
                    <div class="card-body text-justify">
                        <?php echo $translation->content ?>
                    </div>
                </div>
            </section>
        </div>
        <div>&nbsp;</div>
    </div>
    <div class="col-5">
        <div class="row">
            <section class="container">
                <span class="welcome_subtitle">
                    <h5><?php echo $fieldlabel->get_or_new('approved_subtitle') ?></h5>
                    <hr />
                </span>
                <?php $Fosseis = \Fossil\Fossil::where('content_type_id', '=', 24)->where('is_most_recent', '=', 1)->take(7)->orderBy('creation', 'desc')->get(); ?>
                <?php foreach ($Fosseis as $Fossil) : ?>
                    <?php 
                        $clinicaltrial = \Repository\ClinicalTrial::find($Fossil->object_id);
                        $json = json_decode($Fossil->serialized, true); 
                    ?>
                    <?php if (linguagem_selecionada() == 'en') :?>
                        <?php $translation = $json['public_title']; ?>
                    <?php else : ?>
                        <?php foreach ($json['translations'] as $key => $value) : ?>
                            <?php if ($value['language'] == linguagem_selecionada()) $translation = $value['public_title']; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <div class="card menu_dashboard">
                        <div class="card-body">
                            <div class="card-title">
                                <small class="card-description pull-right"><?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($Fossil->creation)) ?></small>
                                <h5><a class="title_link" href="<?php echo base_url('/rg/'.$clinicaltrial->trial_id) ?>"><?php echo $translation ?></a></h5>
                            </div>
                            <div class="card-text text-justify"><pre><?php
                                    //                                                echo var_export(array_keys($json['translations'][0]), true);
                                    ?></pre></div>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                <?php endforeach; ?>
            </section>
        </div>
    </div>
</section>
