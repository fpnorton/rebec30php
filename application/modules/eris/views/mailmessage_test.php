<?php defined('BASEPATH') OR exit('No direct script access allowed');
$genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
$fieldlabel = new Helper\FieldLabelLanguage('MailMessageForm'); $fieldlabel->enableUpdate();
$elements[] =
    form_open('eris/mailMessages/send', ['id' => 'send','autocomplete' => 'off',]);
{
    foreach (
        [
            form_fieldset($fieldlabel->get_or_new('test_to'), ['class' => 'form-group']),
            form_input('to', $email, ['class' => 'form-input']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('test_subject'), ['class' => 'form-group']),
            form_input('subject', '', ['class' => 'form-input']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('test_body'), ['class' => 'form-group']),
            form_textarea('body', '', ['class' => 'ckeditor']),
            form_fieldset_close(),
        ] as $form_item) $elements[] = $form_item;
}
{
    $elements[] =
        form_submit('submit', $genericlabel->get_or_new('button_send'));
    $elements[] =
        form_close();
}
?>
<div class="row">
    <!-- left column -->
    <div class="col-md-1"></div>
    <!-- center column -->
    <div class="col-md-10">
        <!-- general form elements -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php echo $fieldlabel->get_or_new('test_title')?></h3>
            </div>
            <?php foreach ($elements as $element) echo $element; ?>
        </div>
    </div>
    <!-- right column -->
    <div class="col-md-1"></div>
</div>
