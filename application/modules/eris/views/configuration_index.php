<?php defined('BASEPATH') OR exit('No direct script access allowed');
$genericlabel = new Helper\FieldLabelLanguage('General');$genericlabel->enableUpdate();
$fieldlabel = new Helper\FieldLabelLanguage('ConfigurationForm'); $fieldlabel->enableUpdate();
{ 
    $elements = NULL;
    $elements[] = '<br/><hr/>';
    {   // ROBOTS
        $elements[] = '<h3>'.$fieldlabel->get_or_new('SEO').'</h3>';
        $elements[] = form_open('eris/configuration/robots_save', ['id' => 'edit','autocomplete' => 'off',]);
        foreach (
            [
                form_fieldset($fieldlabel->get_or_new('robots'), ['class' => 'form-group']),
                form_textarea('robots_txt', $robots_txt),
                form_fieldset_close(),
            ] as $form_item) $elements[] = $form_item;
        $elements[] =
            form_submit('submit', $genericlabel->get_or_new('button_save'), ['class' => 'pull-right']);
        $elements[] =
            form_close();
    }
    $elements[] = '<br/><hr/>';
    {   // EMAIL
        $elements[] = '<h3>'.$fieldlabel->get_or_new('email').'</h3>';
        $elements[] = form_open('eris/configuration/email_save', ['id' => 'edit','autocomplete' => 'off',]);
        foreach (
            [
                form_fieldset($fieldlabel->get_or_new('email_from_address'), ['class' => 'form-group']),
                form_input('email_from_address', $email_from_address, ['class' => 'form-input']),
                form_fieldset_close(),
                form_fieldset($fieldlabel->get_or_new('email_from_name'), ['class' => 'form-group']),
                form_input('email_from_name', $email_from_name, ['class' => 'form-input']),
                form_fieldset_close(),
                form_fieldset($fieldlabel->get_or_new('smtp_port'), ['class' => 'form-group']),
                form_dropdown('smtp_port', 
                    [25 => 'SMTP', 465 => 'SMTPS/SSL', 587 => 'SMTPS/TLS'],
                    $smtp_port, 
                    ['class' => 'form-input']
                ),
                form_fieldset_close(),
                form_fieldset($fieldlabel->get_or_new('smtp_host'), ['class' => 'form-group']),
                form_input('smtp_host', $smtp_host, ['class' => 'form-input']),
                form_fieldset_close(),
                form_fieldset($fieldlabel->get_or_new('smtp_user'), ['class' => 'form-group']),
                form_input('smtp_user', $smtp_user, ['class' => 'form-input']),
                form_fieldset_close(),
                form_fieldset($fieldlabel->get_or_new('smtp_pass'), ['class' => 'form-group']),
                form_input('smtp_pass', $smtp_pass, ['class' => 'form-input']),
                form_fieldset_close(),
            ] as $form_item) $elements[] = $form_item;
        {
            $elements[] = form_fieldset($fieldlabel->get_or_new('email_ignore_list'), ['class' => 'form-group']);
            $index = 0;
            foreach ($email_ignore_list as $email_ignore_value) {
                $index++;
                foreach (
                    [
                        '<p>',
                        form_input("email_ignore_value[$index]", $email_ignore_value, ['class' => 'form-input', 'size' => '40']),
                        form_checkbox("email_ignore_remove[$index]", 'ON', FALSE),
                        $fieldlabel->get_or_new('email_ignore_remove'),
                        '</p>',
                    ] as $form_item) $elements[] = $form_item;
            }
            $elements[] = form_fieldset_close();
        }
    
        $elements[] =
            form_submit('submit', $genericlabel->get_or_new('button_save'), ['class' => 'pull-right']);
        $elements[] =
            form_close();
    }
    $elements[] = '<br/><hr/>';
    {   // USER REGISTER
        $elements[] = '<h3>'.$fieldlabel->get_or_new('user_register').'</h3>';
        $elements[] = form_open('eris/configuration/user_register_save', ['id' => 'edit','autocomplete' => 'off',]);
        foreach (
            [
                form_checkbox('user_register_enabled', 'ON', $user_register_enabled),
                $fieldlabel->get_or_new('user_register_enabled'),
                '<br/>',
                form_checkbox('user_register_sends_email', 'ON', $user_register_sends_email),
                $fieldlabel->get_or_new('user_register_sends_email'),
                '<br/>',
            ] as $form_item) $elements[] = $form_item;
        $elements[] =
            form_submit('submit', $genericlabel->get_or_new('button_save'), ['class' => 'pull-right']);
        $elements[] =
            form_close();
    }
    $elements[] = '<br/><hr/>';
    {   // FLOW
        $elements[] = '<h3>'.$fieldlabel->get_or_new('flow').'</h3>';
        $elements[] = form_open('eris/configuration/flow_save', ['id' => 'edit','autocomplete' => 'off',]);
        foreach (
            [
                form_checkbox('status_check_complete_to_send_enabled', 'ON', $status_check_complete_to_send_enabled),
                $fieldlabel->get_or_new('status_check_complete_to_send_enabled'),
                '<br/>',
            ] as $form_item) $elements[] = $form_item;
        $elements[] =
            form_submit('submit', $genericlabel->get_or_new('button_save'), ['class' => 'pull-right']);
        $elements[] =
            form_close();
    }
    $elements[] = '<br/><hr/>';
    {   // API
        $elements[] = '<h3>'.$fieldlabel->get_or_new('keys').'</h3>';
        $elements[] = form_open('eris/configuration/keys_save', ['id' => 'edit','autocomplete' => 'off',]);
        foreach (
            [
                form_fieldset($fieldlabel->get_or_new('key_private'), ['class' => 'form-group']),
                form_textarea('key_private', $key_private),
                form_fieldset_close(),
                form_fieldset($fieldlabel->get_or_new('key_public'), ['class' => 'form-group']),
                form_textarea('key_public', $key_public),
                form_fieldset_close(),
            ] as $form_item) $elements[] = $form_item;
        {
            $elements[] = form_fieldset($fieldlabel->get_or_new('key_tokens'), ['class' => 'form-group']);
            $index = 0;
            foreach ($key_tokens as $token_key => $token_value) {
                $index++;
                foreach (
                    [
                        form_fieldset($fieldlabel->get_or_new('key_token_key'), ['class' => 'form-group']),
                        form_input("key_token_key[$index]", $token_key, ['class' => 'form-input']),
                        form_checkbox("key_token_remove[$index]", 'ON', FALSE),
                        $fieldlabel->get_or_new('key_token_remove'),
                        form_fieldset_close(),
                        form_fieldset($fieldlabel->get_or_new('key_token_value'), ['class' => 'form-group']),
                        form_textarea("key_token_value[$index]", $token_value),
                        form_fieldset_close(),
                    ] as $form_item) $elements[] = $form_item;
            }
            $elements[] = form_fieldset_close();
        }
        $elements[] =
            form_submit('submit', $genericlabel->get_or_new('button_save'), ['class' => 'pull-right']);
        $elements[] =
            form_close();
    }
    $elements[] = '<br/><hr/>';
}
?>
<div class="row ">
    <div class="col-xs-1"></div>
    <div class="col-xs-10">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-gears fa-1g" aria-hidden="true"></i> <?php echo $genericlabel->get_or_new('menu_configuration') ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <?php foreach ($elements as $element) echo $element; ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
