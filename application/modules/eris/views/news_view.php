<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('WelcomeForm'); $fieldlabel->enableUpdate(); ?>
<?php $newses = $newses ?? [$news]; ?>
<div class="row" > <!-- style="max-height: 600px; overflow:auto; " -->
    <section class="container" >
                <span class="welcome_subtitle">
                    <h5><?php echo $fieldlabel->get_or_new('news_subtitle') ?></h5>
                    <hr />
                </span>
        <?php foreach ($newses as $news) : ?>
            <div class="card menu_dashboard">
                <?php if (linguagem_selecionada() == 'en') :?>
                    <?php $translation = $news; ?>
                <?php else : ?>
                    <?php $translation = $news->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                <?php endif; ?>
                <div class="card-title">
                    <h4><?php echo $translation->title ?></h4>
                    <hr />
                </div>
                <div class="card-body text-justify">
                    <?php echo $translation->text ?>
                </div>
            </div>
            <div>&nbsp;</div>
        <?php endforeach; ?>
    </section>
</div>
