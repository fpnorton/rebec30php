<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('QuestionForm'); $fieldlabel->enableUpdate(); ?>
<?php $questions = $questions ?? []; ?>
<div class="row">
    <section class="container">
        <span class="welcome_subtitle">
            <h5><?php echo $fieldlabel->get_or_new('questions_subtitle') ?></h5>
            <hr />
        </span>
        <div style="height: 600px; overflow-y: scroll;">
            <?php foreach ($questions as $question) : ?>
                <?php if (linguagem_selecionada() == 'en') :?>
                    <?php $translation = $question; ?>
                <?php else : ?>
                    <?php $translation = $question->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                <?php endif; ?>
                <div class="card"
                     id="summary_<?php echo $question->id ?>">
                    <div class="card-body">
                        <div class="card-title"
                             data-toggle="collapse"
                             href="#question_<?php echo $question->id ?>"
                             aria-expanded="false"
                             aria-controls="question_<?php echo $question->id ?>">
                            <h6><?php echo $translation->title ?></h6>
                        </div>
                        <div class="card-text text-justify collapse in"
                             id="question_<?php echo $question->id ?>">
                            <hr />
                            <h6><?php echo $translation->answer; ?></h6>
                        </div>
                    </div>
                </div>
                <div>&nbsp;</div>
            <?php endforeach; ?>
        </div>
    </section>
</div>
