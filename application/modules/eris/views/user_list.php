<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('UserForm'); $fieldlabel->enableUpdate(); ?>
<div class="row ">
    <div class="col-xs-1"></div>
    <div class="col-xs-10">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users fa-1g" aria-hidden="true"></i> <?php echo $genericlabel->get_or_new('menu_users') ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
<!-- ############################################################################################################### -->
            <table id="datatable"
                   class="table table-hover table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th><?php echo $fieldlabel->get_or_new('head_id') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_active') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_username') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_email') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_first_name') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_last_name') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_last_login') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_date_joined') ?></th>
                        <th class="text-center"><?php echo $fieldlabel->get_or_new('head_actions') ?></th>
                    </tr>
                </thead>
                <tr>
                    <td></td>
                    <td></td>
                    <td class=""></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-center">
                    </td>
                </tr>
            </table>
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
            <?php //echo @$this->pagination->create_links(); ?>
        </div>
      </div><!-- /.box -->
    </div>
    <div id="output" class="col-xs-1"></div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            // jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            // jQuery("#searchList").submit();
        });
    });
    jQuery(window).ready(function(){
        // $('.datatable').on('init.dt', function() {
        //     $("div.datatable-toolbar")
        //         .html('<button type="button" class="btn btn-primary" id="any_button">Click Me!</button>');
        // });
    });
    $('ul.pagination li a').on('click',function(e){
        e.preventDefault();
        window.location.href = $(this).attr('href');
    });
</script>
<? define('DATATABLE_SERVER_SIDE', '/eris/users/paginate'); ?>
<?php $fieldlabel->disableUpdate(); ?>