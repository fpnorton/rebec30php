<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('Fossil'); $fieldlabel->enableUpdate(); ?>
<?php $fosseis = $fosseis ?? [$fossil]; ?>
<style><!--
    .container {
    /*    margin: 0 auto;*/
    /*    width: 1004px;*/
        line-height: 1.7em;
    /*    background: #fff;*/
    }
    span.label, span.legend {
        font-weight: bold;
    }
    H2, H2 a {
        color: #006837;
        margin: 0px;
        text-decoration: none;
        font-size: 19px;
    }
    h3 {
        display: block;
        font-size: 1.17em;
        margin-block-start: 1em;
        margin-block-end: 1em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;
        font-weight: bold;
    }
    section {
        margin: 0;
        padding: 0;
        text-align: left;
        font-size: 95%;
        font-family: 'Lato', sans-serif;
        background: #f2f2f0;
    }
    .balloon {
        border: 6px solid #ffffff;
        border-radius: 16px;
        -moz-border-radius: 16px;
        -webkit-border-radius: 16px;
        background-color: #999999;
    }
    .subset {
        border: 1px solid #000000;
        border-radius: 5px;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        padding: 10px;
        margin: 10px;
    }
    table.dataTable TH {
        color: #0c4762;
        font-size: 90%;
        background: #dedede;
    }
    table.dataTable TH, table.dataTable TD {
        border-bottom: 1px solid #c9c9c9;
        font-size: 85%;
        padding: 6px 4px;
    }
--></style>
<div class="row" > <!-- style="max-height: 600px; overflow:auto; " -->
    <section class="container" >
            <span class="welcome_subtitle">
                <h5><?php echo $fieldlabel->get_or_new('main_title') ?></h5>
                <hr />
            </span>
        <?php foreach ($fosseis as $fossil) : ?>
            <pre><?php 
            $clinicaltrial = \Repository\ClinicalTrial::find($fossil->object_id);
            $revision_1 = \Fossil\Fossil::where('content_type_id', '=', 24)
                ->where('revision_sequential', '=', 1)
                ->where('object_id', '=', $fossil->object_id)
                ->first();
            ?></pre>
            <?php $json = json_decode($fossil->serialized, true); ?>
            <?php if (linguagem_selecionada() == 'en') :?>
                <?php $translation = $json['public_title']; ?>
            <?php else : ?>
                <?php foreach ($json['translations'] as $key => $value) : ?>
                    <?php if ($value['language'] == linguagem_selecionada()) $translation = $value['public_title']; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="card" style="border-color: black; margin-top: 5px;">
                <div class="card-body text-justify">
                    <div class="">
                        <h5><a class="title_link" href="<?php echo base_url('/rg/'.$clinicaltrial->trial_id) ?>"><?php echo $translation ?></a></h5>

                        <span class="label"><?php echo $fieldlabel->get_or_new('first_revision_approved_date'); ?>:</span>
                        <span class="value">
                            <?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($revision_1->creation)) ?>
                            <sup>(<?php echo @\Helper\Language::$available[linguagem_selecionada()]['date_sample'] ?>)</sup>
                        </span>
                        <br>
                        <span class="label"><?php echo $fieldlabel->get_or_new('revision_approved_date'); ?>:</span>
                        <span class="value">
                            <?php echo date( @\Helper\Language::$available[linguagem_selecionada()]['date_fmt'], strtotime($fossil->creation)) ?>
                            <sup>(<?php echo @\Helper\Language::$available[linguagem_selecionada()]['date_sample'] ?>)</sup>
                        </span>
                        <br>

                        <span class="label"><?php echo $fieldlabel->get_or_new('last_revision'); ?>:</span>
                        <span class="value"><?php echo $fossil->revision_sequential ?></span><br />
                        <span class="label"><?php echo $fieldlabel->get_or_new('study_type'); ?>:</span>
                        <span class="value"><?php echo $json['is_observational'] ? $fieldlabel->get_or_new('is_observational') : $fieldlabel->get_or_new('is_interventional') ?></span>
                    </div>
                    <a class="collapsed pull-right"
                       href="<?php echo base_url('/rg/'.$clinicaltrial->trial_id) ?>"
                       aria-expanded="false"
                       aria-controls="collapseSummary">
                        <button type="button" class="btn btn-light btn-sm"><?php echo $fieldlabel->get_or_new('read_open') ?></button>
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
        <div>&nbsp;</div>
        <a class="pull-right"
           href="<?php echo base_url('/list'); ?>"
           aria-expanded="false"
           aria-controls="collapseSummary">
            <button type="button" class="btn btn-default btn-sm"><?php echo $fieldlabel->get_or_new('list_all') ?></button>
        </a>
        <span><?php
        //            echo '<pre>'.var_export(array_keys($json), TRUE).'</pre>';
        //            echo '<pre>'.var_export($json, TRUE).'</pre>';
        ?></span>
    </section>
</div>
