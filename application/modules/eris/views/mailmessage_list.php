<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('MailMessageForm'); $fieldlabel->enableUpdate(); ?>
<div class="row ">
    <div class="col-xs-1"></div>
    <div class="col-xs-10">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-envelope-o fa-1g" aria-hidden="true"></i> <?php echo $genericlabel->get_or_new('menu_mail_message_manager') ?></h3>
        <?php /*
            <div class="box-tools">
                <form action="<?php echo base_url() ?>userListing" method="POST" id="searchList">
                    <div class="input-group">
                      <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                </form>
            </div>
*/ ?>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
<!-- ############################################################################################################### -->
            <table id="datatable" class="table table-hover table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th><?php echo $fieldlabel->get_or_new('head_label') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_description') ?></th>
                        <th class="text-center"><?php echo $fieldlabel->get_or_new('head_actions') ?></th>
                    </tr>
                </thead>
                <?php foreach(($mailmessages ?? []) as $mailmessage) : ?>
                    <?php if (linguagem_selecionada() == 'en') : ?>
                        <?php $translation = $mailmessage ?>
                    <?php else : ?>
                        <?php $translation = $mailmessage->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                    <?php endif; ?>
                    <tr>
                        <td><b><?php echo $mailmessage->label ?></b></td>
                        <td><?php echo character_limiter($translation->description, 50); ?></td>
                        <td class="text-center">
                            <a href="<?php echo base_url() ?>eris/mailMessages/edit/<?php echo $mailmessage->id ?>">
                                <i class="fa fa-pencil"></i>
                            </a>
<? /*
                            <? $object_id = $mailmessage->id; ?>
                            <? $class_name = 'mailMessages'; ?>
                            <a href="#" data-toggle="modal" data-target="#<?php echo 'modal_'.$object_id ?>">
                                <i class="fa fa-eraser red-text"></i>
                            </a>
                            <div class="modal fade"
                                 id="<?php echo 'modal_'.$object_id ?>"
                                 tabindex="-1"
                                 role="dialog"
                                 aria-labelledby="<?php echo 'modalLabel_'.$object_id ?>"
                                 aria-hidden="true">
                                <div class="modal-dialog modal-sm"
                                     role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"
                                                id="<?php echo 'modalLabel_'.$object_id ?>"><?php echo $genericlabel->get_or_new('title_exclusion'); ?>
                                            </h5>
                                            <button type="button"
                                                    class="close"
                                                    data-dismiss="modal"
                                                    aria-label="<?php echo $genericlabel->get_or_new('button_close'); ?>">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body"><?php echo $fieldlabel->get_or_new('text_exclusion_confirmation') ?>
                                        </div>
                                        <div class="modal-footer">
                                            <form action="<?php echo base_url() ?>eris/<? echo $class_name ?>/delete/<?php echo $object_id ?>">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $genericlabel->get_or_new('button_close'); ?></button>
                                                <button type="submit" class="btn btn-danger"><?php echo $genericlabel->get_or_new('button_confirm'); ?></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
 */ ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
            <?php //echo @$this->pagination->create_links(); ?>
        </div>
      </div><!-- /.box -->
    </div>
    <div class="col-xs-1"></div>
</div>
