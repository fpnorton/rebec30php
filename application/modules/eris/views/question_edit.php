<?php defined('BASEPATH') OR exit('No direct script access allowed');
$genericlabel = new Helper\FieldLabelLanguage('General');
$fieldlabel = new Helper\FieldLabelLanguage('QuestionForm'); $fieldlabel->enableUpdate();
$elements[] =
    form_open('eris/questions/edit/'.$question->id ?? 0, ['id' => 'edit','autocomplete' => 'off',]);
{
    foreach (
        [
            form_hidden('id', $question->id),
            form_fieldset($fieldlabel->get_or_new('order'), ['class' => 'form-group']),
            form_input('order', $question->order, ['class' => 'form-input']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('title'), ['class' => 'form-group']),
            form_textarea('title-en', $question->title, ['class' => 'ckeditor']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('answer'), ['class' => 'form-group']),
            form_textarea('answer-en', $question->answer, ['class' => 'ckeditor']),
            form_fieldset_close(),
        ] as $form_item) $elements[] = $form_item;
}
{
    foreach ($this->extra_languages() as $language => $language_pack) {
        $translation = $question->translations->where('language', $language)->first();
//echo '<pre>'.var_export([
//        __FILE__ => __LINE__,
//        $translation,
//    ],true).'</pre>';
        foreach (
            [
                form_hidden('id-'.$language, $translation->id),
                form_fieldset($fieldlabel->get_or_new('title_'.$language), ['class' => 'form-group']),
                form_textarea('title-'.$language, $translation->title, ['class' => 'ckeditor']),
                form_fieldset_close(),
                form_fieldset($fieldlabel->get_or_new('answer_'.$language), ['class' => 'form-group']),
                form_textarea('answer-'.$language, $translation->answer, ['class' => 'ckeditor']),
                form_fieldset_close(),
            ] as $form_item) $elements[] = $form_item;
    }
//echo '<pre>'.var_export([
//        __FILE__ => __LINE__,
//        $page->toArray(),
//    ],true).'</pre>';
}
{
//    array_unshift($elements,
//        form_open('flatPages/'.$page->id ?? 0, ['id' => 'edit','autocomplete' => 'off',])
//    );
    $elements[] =
        form_submit('submit', $genericlabel->get_or_new('button_save'));
    $elements[] =
        form_close();
}
?>
<?php /*
<div class="row">
    <div class="col-xs-12 text-right">
        <div class="form-group">
            <a class="btn btn-primary" href="<?php echo base_url(); ?>addNew"><i class="fa fa-plus"></i> Add New</a>
        </div>
    </div>
</div>
*/ ?>
<div class="row">
    <!-- left column -->
    <div class="col-md-1"></div>
    <!-- center column -->
    <div class="col-md-10">
        <!-- general form elements -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php echo $fieldlabel->get_or_new('edit_title')?></h3>
            </div>
            <?php foreach ($elements as $element) echo $element; ?>
        </div>
    </div>
    <!-- right column -->
    <div class="col-md-1"></div>
</div>
