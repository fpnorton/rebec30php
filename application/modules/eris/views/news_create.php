<?php defined('BASEPATH') OR exit('No direct script access allowed');
$genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
$fieldlabel = new Helper\FieldLabelLanguage('NewsForm'); $fieldlabel->enableUpdate();
$elements[] =
    form_open('eris/news/create', ['id' => 'edit','autocomplete' => 'off',]);
{
    foreach (
        [
            form_fieldset($fieldlabel->get_or_new('title'), ['class' => 'form-group']),
            form_input('title-en', '', ['class' => 'form-input']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('text'), ['class' => 'form-group']),
            form_textarea('text-en', '', ['class' => 'ckeditor']),
            form_fieldset_close(),
        ] as $form_item) $elements[] = $form_item;
}
{
    foreach ($this->extra_languages() as $language => $language_pack) {
        foreach (
            [
                form_fieldset($fieldlabel->get_or_new('title_'.$language), ['class' => 'form-group']),
                form_input('title-'.$language, '', ['class' => 'form-input']),
                form_fieldset_close(),
                form_fieldset($fieldlabel->get_or_new('text_'.$language), ['class' => 'form-group']),
                form_textarea('text-'.$language, '', ['class' => 'ckeditor']),
                form_fieldset_close(),
            ] as $form_item) $elements[] = $form_item;
    }
}
{
    $elements[] =
        form_submit('submit', $genericlabel->get_or_new('button_save'));
    $elements[] =
        form_close();
}
?>
<?php /*
<div class="row">
    <div class="col-xs-12 text-right">
        <div class="form-group">
            <a class="btn btn-primary" href="<?php echo base_url(); ?>addNew"><i class="fa fa-plus"></i> Add New</a>
        </div>
    </div>
</div>
*/ ?>
<div class="row">
    <!-- left column -->
    <div class="col-md-1"></div>
    <!-- center column -->
    <div class="col-md-10">
        <!-- general form elements -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php echo $fieldlabel->get_or_new('create_title'); ?></h3>
            </div>
            <?php foreach ($elements as $element) echo $element; ?>
        </div>
    </div>
    <!-- right column -->
    <div class="col-md-1"></div>
</div>
