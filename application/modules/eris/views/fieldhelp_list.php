<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('FieldHelpForm'); $fieldlabel->enableUpdate(); ?>
<?php
    $redis = new \Redis();
    $redis->connect('redis', 6379);
    $redis->auth('rebec');
?>
<div class="row ">
    <div class="col-xs-1"></div>
    <div class="col-xs-10">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-bookmark-o fa-1g" aria-hidden="true"></i> <?php echo $genericlabel->get_or_new('menu_field_manager') ?></h3>
        <?php /*
            <div class="box-tools">
                <form action="<?php echo base_url() ?>userListing" method="POST" id="searchList">
                    <div class="input-group">
                      <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                </form>
            </div>
*/ ?>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
<!-- ############################################################################################################### -->
            <table id="datatable" class="table table-hover table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th><?php echo $fieldlabel->get_or_new('head_form') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_field') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_text') ?></th>
                        <th><?php echo $fieldlabel->get_or_new('head_calls') ?></th>
                        <th class="text-center"><?php echo $fieldlabel->get_or_new('head_actions') ?></th>
                    </tr>
                </thead>
                <?php foreach(($fields ?? []) as $field) : ?>
                    <?php if (linguagem_selecionada() == 'en') : ?>
                        <?php $translation = $field ?>
                    <?php else : ?>
                        <?php $translation = $field->translations()->where('language', '=', linguagem_selecionada())->first(); ?>
                    <?php endif; ?>
                    <?php $use_counter = $redis->hGet('fieldHelp:'.$field->form, $field->field); ?>
                    <tr>
                        <td><?php echo $field->form ?></td>
                        <td><?php echo $field->field ?></td>
                        <td><?php echo character_limiter($translation->text, 50); ?></td>
                        <td><?php echo ($use_counter == '' ? 0 : $use_counter); ?></td>
                        <td class="text-center">
                            <a href="<?php echo base_url() ?>eris/fieldHelps/edit/<?php echo $field->id ?>">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <? $object_id = $field->id; ?>
                            <a href="#" data-toggle="modal" data-target="#<?php echo 'modal_'.$object_id ?>">
                                <i class="fa fa-eraser red-text"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <?php foreach(($fields ?? []) as $field) : ?>
                <? $object_id = $field->id; ?>
                <? $class_name = 'fieldHelps'; ?>
                <div class="modal fade"
                     id="<?php echo 'modal_'.$object_id ?>"
                     tabindex="-1"
                     role="dialog"
                     aria-labelledby="<?php echo 'modalLabel_'.$object_id ?>"
                     aria-hidden="true">
                    <div class="modal-dialog modal-sm"
                         role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"
                                    id="<?php echo 'modalLabel_'.$object_id ?>"><?php echo $genericlabel->get_or_new('title_exclusion'); ?>
                                </h5>
                                <button type="button"
                                        class="close"
                                        data-dismiss="modal"
                                        aria-label="<?php echo $genericlabel->get_or_new('button_close'); ?>">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body"><?php echo $fieldlabel->get_or_new('text_exclusion_confirmation') ?>
                            </div>
                            <div class="modal-footer">
                                <form action="<?php echo base_url() ?>eris/<? echo $class_name ?>/delete/<?php echo $object_id ?>">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $genericlabel->get_or_new('button_close'); ?></button>
                                    <button type="submit" class="btn btn-danger"><?php echo $genericlabel->get_or_new('button_confirm'); ?></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div><!-- /.box-body -->

        <div class="box-footer clearfix">
        </div>
      </div><!-- /.box -->
    </div>
    <div class="col-xs-1"></div>
    <script>
        jQuery(window).ready(function(){
            $('.datatable').on('init.dt', function() {
                $("div.datatable-toolbar")
                    .html(
'<a href="<?php echo base_url('/eris/fieldHelps/import') ?>"><button type="button" class="btn btn-primary"><?php echo $fieldlabel->get_or_new('fieldHelps_import') ?></button></a>' +
'<a href="<?php echo base_url('/eris/fieldHelps/export_all') ?>"><button type="button" class="btn btn-primary"><?php echo $fieldlabel->get_or_new('fieldHelps_export') ?></button></a>' +
'<a href="<?php echo base_url('/eris/fieldHelps/export') ?>"><button type="button" class="btn btn-secondary"><?php echo $fieldlabel->get_or_new('fieldHelps_export_most_used') ?></button></a>'
                    );
            });
        });
    </script>
</div>
