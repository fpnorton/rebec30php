<?php defined('BASEPATH') OR exit('No direct script access allowed');
$genericlabel = new Helper\FieldLabelLanguage('General');  $genericlabel->enableUpdate();
$fieldlabel = new Helper\FieldLabelLanguage('FieldHelpForm'); $fieldlabel->enableUpdate();
$elements[] =
    form_open_multipart('eris/fieldHelps/import', ['id' => 'import','autocomplete' => 'off',]);
{
    foreach (
        [
            form_hidden('id', $field->id),
            form_fieldset($fieldlabel->get_or_new('upload'), ['class' => 'form-group']),
            form_upload([
                'name' => 'upload',
                'accept-charset' => 'utf-8',
                'accept' => '.json,.xml,application/json',
            ]),
            form_fieldset_close(),
        ] as $form_item) $elements[] = $form_item;
}
{
//    array_unshift($elements,
//        form_open('flatPages/'.$page->id ?? 0, ['id' => 'edit','autocomplete' => 'off',])
//    );
    $elements[] =
        form_submit('submit', $genericlabel->get_or_new('button_save'));
    $elements[] =
        form_close();
}
?>
<?php /*
<div class="row">
    <div class="col-xs-12 text-right">
        <div class="form-group">
            <a class="btn btn-primary" href="<?php echo base_url(); ?>addNew"><i class="fa fa-plus"></i> Add New</a>
        </div>
    </div>
</div>
*/ ?>
<div class="row">
    <!-- left column -->
    <div class="col-md-1"></div>
    <!-- center column -->
    <div class="col-md-10">
        <!-- general form elements -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php echo $fieldlabel->get_or_new('import_title')?></h3>
            </div>
            <?php foreach ($elements as $element) echo $element; ?>
        </div>
    </div>
    <!-- right column -->
    <div class="col-md-1"></div>
</div>