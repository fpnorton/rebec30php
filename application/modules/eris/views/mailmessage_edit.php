<?php defined('BASEPATH') OR exit('No direct script access allowed');
$genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
$fieldlabel = new Helper\FieldLabelLanguage('MailMessageForm'); $fieldlabel->enableUpdate();
$elements[] =
    form_open('eris/mailMessages/edit/'.$mailmessage->id ?? 0, ['id' => 'edit','autocomplete' => 'off',]);
{
    foreach (
        [
            form_hidden('id', $mailmessage->id),
            form_fieldset($fieldlabel->get_or_new('label'), ['class' => 'form-group']),
            form_input('label-en', $mailmessage->label, ['class' => 'form-input readonly', 'disabled' => 'disabled']),
            form_fieldset_close(),
            form_fieldset($fieldlabel->get_or_new('description'), ['class' => 'form-group']),
            form_textarea('description-en', $mailmessage->description, ['class' => 'ckeditor']),
            form_fieldset_close(),
        ] as $form_item) $elements[] = $form_item;
}
{
    foreach ($this->extra_languages() as $language => $language_pack) {
        $translation = $mailmessage->translations->where('language', $language)->first();
        foreach (
            [
                form_hidden('id-'.$language, $translation->id),
//                form_fieldset($fieldlabel->get_or_new('label_'.$language), ['class' => 'form-group']),
//                form_input('label-'.$language, $translation->title, ['class' => 'form-input readonly']),
//                form_fieldset_close(),
                form_fieldset($fieldlabel->get_or_new('description_'.$language), ['class' => 'form-group']),
                form_textarea('description-'.$language, $translation->description, ['class' => 'ckeditor']),
                form_fieldset_close(),
            ] as $form_item) $elements[] = $form_item;
    }
}
{
//    array_unshift($elements,
//        form_open('flatPages/'.$page->id ?? 0, ['id' => 'edit','autocomplete' => 'off',])
//    );
    $elements[] =
        form_submit('submit', $genericlabel->get_or_new('button_save'));
    $elements[] =
        form_close();
}
?>
<?php /*
<div class="row">
    <div class="col-xs-12 text-right">
        <div class="form-group">
            <a class="btn btn-primary" href="<?php echo base_url(); ?>addNew"><i class="fa fa-plus"></i> Add New</a>
        </div>
    </div>
</div>
*/ ?>
<div class="row">
    <!-- left column -->
    <div class="col-md-1"></div>
    <!-- center column -->
    <div class="col-md-10">
        <!-- general form elements -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php echo $fieldlabel->get_or_new('edit_title')?></h3>
            </div>
            <?php foreach ($elements as $element) echo $element; ?>
        </div>
    </div>
    <!-- right column -->
    <div class="col-md-1"></div>
</div>
