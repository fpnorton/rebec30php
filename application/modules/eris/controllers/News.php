<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends ErisController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function index()
    {
        $this->only_admin();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['newses'] = \ReviewApp\News::orderBy('created', 'desc')->get();
        $data['content'] = $this->load->view('news_list.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function edit($id)
    {
        $this->only_admin();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['news'] = \ReviewApp\News::where('id', '=', $id)->first();
        $data['content'] = $this->load->view('news_edit.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function create()
    {
        $this->only_admin();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['news'] = [];
        $data['content'] = $this->load->view('news_create.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    public function save()
    {
        $this->only_admin();

        $fieldlabel = (new Helper\FieldLabelLanguage('NewsForm'))->enableUpdate();

        if ($this->input->post('id') > 0) {
            $News = \ReviewApp\News::where('id', '=', $this->input->post('id'))->first();

            $News->title = trim($this->input->post('title-en'));
            $News->text = trim($this->input->post('text-en'));
            $News->save();

            foreach (\Helper\Language::$available as $language_key => $language_value) {
                if ($this->input->post('id-'.$language_key) > 0) {
                    $NewsTranslation = \ReviewApp\NewsTranslation::where('id', '=', $this->input->post('id-'.$language_key))->first();
                    $NewsTranslation->title = trim($this->input->post('title-'.$language_key));
                    $NewsTranslation->text = trim($this->input->post('text-'.$language_key));
                    $NewsTranslation->save();

                }
            }
            $this->session->set_flashdata('success', $fieldlabel->get_or_new("update_success"));
            redirect("/eris/news/edit/".$News->id);
        } else {
            $News = new \ReviewApp\News([
                'title' => trim($this->input->post('title-en')),
                'text' => trim($this->input->post('text-en')),
                'created' => date('Y-m-d H:i:s'),
                'creator_id' => $this->session->userdata('user')->id,
                'status' => 'published',
            ]);
            $News->save();

            foreach (\Helper\Language::$available as $language_key => $language_value) {
                if ($language_key == 'en') continue;
                $NewsTranslation = new \ReviewApp\NewsTranslation([
                    'title' => trim($this->input->post('title-'.$language_key)),
                    'text' => trim($this->input->post('text-'.$language_key)),
                    'object_id' => $News->id,
                    'content_type_id' => 43,
                    'language' => $language_key,
                ]);
                $NewsTranslation->save();
            }
            $this->session->set_flashdata('success', $fieldlabel->get_or_new("insert_success"));
            redirect("/eris/news/edit/".$News->id);
        }
    }

    public function view($id)
    {
        $data = [];
        {
            $data['content_left'] = $this->load->view('news_view.php', [
                'news' => \ReviewApp\News::where('id', '=', $id)->first()
            ], TRUE);
        }
        {
            $data['content_right'] = $this->load->view('fossil_preview.php', [
                'fosseis' => \Fossil\Fossil::where('content_type_id', '=', 24)
                    ->where('is_most_recent', '=', 1)
                    ->take(7)
                    ->orderBy('creation', 'desc')
                    ->get()
            ], TRUE);;
        }
        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function list()
    {
        $data = [];
        {
            $data['content_left'] = $this->load->view('news_view.php', [
                'newses' => \ReviewApp\News::orderBy('created', 'desc')->take(5)->get()
            ], TRUE);
        }
        {
            $data['content_right'] = $this->load->view('fossil_preview.php', [
                'fosseis' => \Fossil\Fossil::where('content_type_id', '=', 24)
                    ->where('is_most_recent', '=', 1)
                    ->take(7)
                    ->orderBy('creation', 'desc')
                    ->get()
            ], TRUE);;
        }
        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    public function delete($id)
    {
        $this->only_admin();

        $fieldlabel = (new Helper\FieldLabelLanguage('NewsForm'))->enableUpdate();

        if ($this->session->userdata('user')->isSuper() == false) {
            redirect( base_url("/") );
            return;
        }

        $object = \ReviewApp\News::where('id', '=', $id)->first();

        $object->delete();

        $this->session->set_flashdata('success', $fieldlabel->get_or_new("delete_success"));

        redirect("/eris/news");

    }

}