<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuration extends ErisController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
        $this->config_boot_load();
    }

    private function config_boot_load() 
    {
        foreach([
            \Helper\RedisKeys::KEY_CONFIG_SEO_ROBOTS_TXT =>
                implode("\n",[
                    "User-Agent: *",
                    "Disallow: ",
                    "Disallow: /xml_ictrp/",
                    "User-Agent: Googlebot",
                    "Allow: /",
                ]),
            \Helper\RedisKeys::KEY_CONFIG_USER_REGISTER_ENABLE => "ON",
            \Helper\RedisKeys::KEY_CONFIG_USER_REGISTER_SENDS_EMAIL => "ON",
            \Helper\RedisKeys::KEY_CONFIG_EMAIL_FROM_ADDRESS => 'rebec@icict.fiocruz.br',
            \Helper\RedisKeys::KEY_CONFIG_EMAIL_FROM_NAME => 'ReBEC',
            \Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_HOST => 'rebec-maildev',
            \Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_PORT => '25',
            \Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_USER => 'root@maildev.org',
            \Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_PASS => 'thurisaz',
            \Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_CRYPTO => '',
            \Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_PROTOCOL => 'smtp',
            \Helper\RedisKeys::KEY_CONFIG_EMAIL_IGNORE_LIST => json_encode([]),
            \Helper\RedisKeys::KEY_CONFIG_STATUS_CHECK_COMPLETE_TO_SEND => "ON",
            \Helper\RedisKeys::KEY_CONFIG_KEY_PRIVATE => "",
            \Helper\RedisKeys::KEY_CONFIG_KEY_PUBLIC => "",
            \Helper\RedisKeys::KEY_CONFIG_KEY_TOKENS => json_encode([]),
        ] as $key => $value) {
            if ($this->REDIS_SERVER->exists($key) == 0) 
                $this->REDIS_SERVER->set($key, $value);
        }
        $this->REDIS_SERVER->save();
    }

    public function index()
    {
        $this->only_super();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        {
            $data['robots_txt'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_SEO_ROBOTS_TXT);
            $data['user_register_enabled'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_USER_REGISTER_ENABLE) == 'ON' ? TRUE : FALSE;
            $data['user_register_sends_email'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_USER_REGISTER_SENDS_EMAIL) == 'ON' ? TRUE : FALSE;
            $data['email_from_address'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_FROM_ADDRESS);
            $data['email_from_name'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_FROM_NAME);
            $data['smtp_host'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_HOST);
            $data['smtp_port'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_PORT);
            $data['smtp_user'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_USER);
            $data['smtp_pass'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_PASS);
            $data['smtp_crypto'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_CRYPTO);
            $data['smtp_protocol'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_PROTOCOL);
            $data['email_ignore_list'] = array_merge(
                json_decode(
                    $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_IGNORE_LIST),
                    TRUE
                ) ?? [], ['' => '']
            );
    
            $data['status_check_complete_to_send'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_STATUS_CHECK_COMPLETE_TO_SEND) == 'ON' ? TRUE : FALSE;

            $data['key_private'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_KEY_PRIVATE);
            $data['key_public'] = 
                $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_KEY_PUBLIC);
            $data['key_tokens'] = array_merge(
                json_decode(
                    $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_KEY_TOKENS),
                    TRUE
                ) ?? [], ['' => '']
            );
        }

        $data['content'] = $this->load->view('configuration_index.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function robots_read()
    {
        header('Content-Type: text/plain');
        echo $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_SEO_ROBOTS_TXT);
    }

    public function robots_save()
    {
        $this->only_super();

        $fieldlabel = (new Helper\FieldLabelLanguage('ConfigurationForm'))->enableUpdate();

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_SEO_ROBOTS_TXT, $this->input->post('robots_txt'));

        $this->REDIS_SERVER->save();

        $this->session->set_flashdata('success', $fieldlabel->get_or_new("robots_update_success"));

        redirect('/eris/configuration');
    }

    public function email_save()
    {
        $this->only_super();

        $fieldlabel = (new Helper\FieldLabelLanguage('ConfigurationForm'))->enableUpdate();

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_EMAIL_FROM_ADDRESS, 
            $this->input->post('email_from_address')
        );

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_EMAIL_FROM_NAME, 
            $this->input->post('email_from_name')
        );

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_HOST, 
            $this->input->post('smtp_host')
        );

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_PORT, 
            $this->input->post('smtp_port')
        );

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_USER, 
            $this->input->post('smtp_user')
        );

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_PASS, 
            $this->input->post('smtp_pass')
        );

        $smtp_crypto = '';
        if ($this->input->post('smtp_port') == 465) $smtp_crypto = 'ssl';
        if ($this->input->post('smtp_port') == 587) $smtp_crypto = 'tls';
        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_CRYPTO, 
            $smtp_crypto
        );

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_PROTOCOL, 
            'smtp'
        );

        {
            $ignore = [];
            foreach($this->input->post('email_ignore_value') as $key => $value) {
                if ($this->input->post('email_ignore_remove')[$key] == 'ON') continue;
                $_val = $this->input->post('email_ignore_value')[$key];
                if (empty($_val)) continue;
                $ignore = array_merge([$_val], $ignore);
            }
            $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_EMAIL_IGNORE_LIST, json_encode($ignore));
        }

        $this->REDIS_SERVER->save();

        $this->session->set_flashdata('success', $fieldlabel->get_or_new("email_update_success"));

        redirect('/eris/configuration');
    }

    public function user_register_save()
    {
        $this->only_super();

        $fieldlabel = (new Helper\FieldLabelLanguage('ConfigurationForm'))->enableUpdate();

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_USER_REGISTER_ENABLE, 
            ($this->input->post('user_register_enabled') == 'ON') ? 'ON' : 'OFF'
        );

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_USER_REGISTER_SENDS_EMAIL, 
            ($this->input->post('user_register_sends_email') == 'ON') ? 'ON' : 'OFF'
        );

        $this->REDIS_SERVER->save();

        $this->session->set_flashdata('success', $fieldlabel->get_or_new("user_register_update_success"));

        redirect('/eris/configuration');
    }

    public function flow_save()
    {
        $this->only_super();

        $fieldlabel = (new Helper\FieldLabelLanguage('ConfigurationForm'))->enableUpdate();

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_STATUS_CHECK_COMPLETE_TO_SEND, 
            ($this->input->post('status_check_complete_to_send') == 'ON') ? 'ON' : 'OFF'
        );

        $this->REDIS_SERVER->save();

        $this->session->set_flashdata('success', $fieldlabel->get_or_new("flow_update_success"));

        redirect('/eris/configuration');
    }

    public function keys_save()
    {
        $this->only_super();
 
        $fieldlabel = (new Helper\FieldLabelLanguage('ConfigurationForm'))->enableUpdate();

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_KEY_PRIVATE, $this->input->post('key_private'));

        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_KEY_PUBLIC, $this->input->post('key_public'));

        $tokens = [];
        foreach($this->input->post('key_token_key') as $key => $value) {
            if ($this->input->post('key_token_remove')[$key] == 'ON') continue;
            $_key = $this->input->post('key_token_key')[$key];
            if (empty($_key)) continue;
            $_val = $this->input->post('key_token_value')[$key];
            if (empty($_val)) continue;
            $tokens = array_merge([$_key => $_val], $tokens);
        }
        $this->REDIS_SERVER->set(\Helper\RedisKeys::KEY_CONFIG_KEY_TOKENS, json_encode($tokens));

        $this->REDIS_SERVER->save();

        $this->session->set_flashdata('success', $fieldlabel->get_or_new("keys_update_success"));

        redirect('/eris/configuration');
    }

}