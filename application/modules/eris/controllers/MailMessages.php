<?php

use Vocabulary\VocabularyTranslation;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

defined('BASEPATH') OR exit('No direct script access allowed');

class MailMessages extends ErisController {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function index()
    {
        $this->only_admin();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['mailmessages'] = \Vocabulary\MailMessage::whereNotNull('label')->where('label', '<>', '')->get();
        $data['content'] = $this->load->view('mailmessage_list.php', $data, TRUE);

//echo '<pre>'.var_export([
//        __FILE__ => __LINE__,
//        '$data' => $data['mailmessages']->count(),
//    ],true).'</pre>';

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function edit($id)
    {
        $this->only_admin();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['mailmessage'] = \Vocabulary\MailMessage::where('id', '=', $id)->first();
        $data['content'] = $this->load->view('mailmessage_edit.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function save()
    {
        $this->only_admin();

        $fieldlabel = (new Helper\FieldLabelLanguage('MailMessageForm'))->enableUpdate();
        if ($this->input->post('id') > 0) {
            $MailMessage = \Vocabulary\MailMessage::where('id', '=', $this->input->post('id'))->first();

            $MailMessage->description = trim($this->input->post('description-en'));
            $MailMessage->save();
            foreach (\Helper\Language::$available as $language_key => $language_value) {
                if ($language_key == 'en') continue;
                $VocabularyTranslation = NULL;
                if ($this->input->post('id-'.$language_key) > 0) {
                    $VocabularyTranslation = \Vocabulary\VocabularyTranslation::where('id', '=', $this->input->post('id-'.$language_key))->first();
                } else {
                    $VocabularyTranslation = new \Vocabulary\VocabularyTranslation();
                    $VocabularyTranslation->object_id = $MailMessage->id;
                    $VocabularyTranslation->label = $MailMessage->label;
                    $VocabularyTranslation->content_type_id = 64;
                    $VocabularyTranslation->language = $language_key;
                }
                $VocabularyTranslation->description = trim($this->input->post('description-'.$language_key));
                $VocabularyTranslation->save();
            }
            $this->session->set_flashdata('success', $fieldlabel->get_or_new("update_success"));
            redirect("/eris/mailMessages/edit/".$MailMessage->id);
        } else {
//            $MailMessage = new \Vocabulary\MailMessage([
//                'label' => trim($this->input->post('label-en')),
//                'description' => trim($this->input->post('description-en')),
//                'order' => 0,
//            ]);
//            $MailMessage->save();
//            foreach (\Helper\Language::$available as $language_key => $language_value) {
//                    if ($language_key == 'en') continue;
//                $QuestionTranslation = new \Assistance\QuestionTranslation([
//                    'title' => trim($this->input->post('title-'.$language_key)),
//                    'answer' => trim($this->input->post('answer-'.$language_key)),
//                    'object_id' => $Question->id,
//                    'content_type_id' => 49,
//                    'language' => $language_key,
//                ]);
//                $QuestionTranslation->save();
//            }
//            $this->session->set_flashdata('success', "Informação atualizada com sucesso.");
//            redirect("/eris/mailMessages/edit/".$MailMessage->id);
            redirect("/eris/mailMessages");
        }
    }

    public function delete($id)
    {
        $this->only_admin();

        $fieldlabel = (new Helper\FieldLabelLanguage('MailMessageForm'))->enableUpdate();
        if ($this->session->userdata('user')->isSuper() == false) {
            redirect( base_url("/") );
            return;
        }

        $object = \Vocabulary\MailMessage::where('id', '=', $id)->first();

        $object->delete();

        $this->session->set_flashdata('success', $fieldlabel->get_or_new("delete_success"));

        redirect("/eris/mailMessages");

    }

    public function test()
    {
        $this->only_super();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['email'] = $User->email;
        $data['content'] = $this->load->view('mailmessage_test.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function send()
    {
        $this->only_super();

        $fieldlabel = (new Helper\FieldLabelLanguage('MailMessageForm'))->enableUpdate();
        if ($this->session->userdata('user')->isSuper() == false) {
            redirect( base_url("/") );
            return;
        }

        $success = $this->mailfunctions->send(
            (new \Helper\Email(NULL, 'ReBEC'))
                ->addTO($this->input->post('to'))
                ->addBCC($this->input->post('to'))
                ->addSUBJECT($this->input->post('subject'))
                ->addBODY($this->input->post('body'))
        );

        if ($success) {
            $this->session->set_flashdata('success', $fieldlabel->get_or_new("send_success"));
        } else {
            $this->session->set_flashdata('error', $fieldlabel->get_or_new("send_failed"));
        }
        redirect("/eris/mailMessages/test");
    }
}