<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends ErisController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function index()
    {
        $this->only_super();

        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];
        $_pages = [];
        $_results = [];
        // $_results = \Auth\User::select('is_active', 'username', 'email', 'first_name', 'last_name', 'id')
        //     ->orderBy('first_name')
        //     ->get();

        {
            $this->parser->parse('main_admin', array_merge($headerInfo, $footerInfo,[
                'content' => $this->load->view('user_list.php', [
                    'pages' => $_pages,
                    'usuarios' => $_results,
                    'usuario' => $this->session->userdata('user'),
                ], TRUE)
            ]));
        }

    }

    public function edit($id)
    {
        $this->only_super();

        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['content'] = $this->load->view('user_edit.php', [
            'usuario' => \Auth\User::find($id),
            'grupos' => \Auth\Group::all(),
        ], TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

//    public function create()
//    {
//        $User = $this->session->userdata ( 'user' );
//        $data['content'] = '';
//        $headerInfo = $this->auxfunctions->getHeaderInfo();
//        $footerInfo = [
//
//        ];
//
//        $data['news'] = [];
//        $data['content'] = $this->load->view('news_create.php', $data, TRUE);
//
//        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
//            'usuario' => $this->session->userdata('user'),
//        ]));
//    }

    public function save()
    {
        $this->only_super();

        $this->load->library('encryption');

        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];
        $id = $this->input->post('id');
        $usuario = \Auth\User::find($id);
        {
            $usuario->first_name = $this->input->post('first_name');
            $usuario->last_name = $this->input->post('last_name');
            $usuario->is_active = (($this->input->post('is_active') ?? "off") == "on");
            $usuario->email = $this->input->post('email');
            $usuario->is_staff = 0;
            $usuario->is_superuser = 0;

            $usuario->groups()->detach();
            foreach ($this->input->post('groups') as $group_id) {
                if ($group_id == 1) $usuario->is_staff = 1;
                if ($group_id == 3) $usuario->is_superuser = 1;
                $usuario->groups()->attach(\Auth\Group::find($group_id));
            }

        }
        $pre = [];
        while (true)
        {
            {
                if ($this->input->post('password') && $this->input->post('cpassword')) {
                    $salt = substr(bin2hex(random_bytes(3)), 0, 5);
                    $hash = $this->encrypt->hash($salt . $this->input->post('password'));
                    $usuario->password = join('$', ['sha1', $salt, $hash]);
                }
            }
            {
                if ($this->input->post('password') && ($this->input->post('password') != $this->input->post('cpassword'))) {
                    $headerInfo['_alerts'][] = [
                        'error' => true,
                        'message' => "A senha não confere, tente novamente.",
                    ];
                    break;
                }
            }
            {
                if ($usuario->id) {
                    $usuario->update();
                    $headerInfo['_alerts'][] = [
                        'success' => true,
                        'message' => "Usuário atualizado com sucesso.",
                    ];
                } else {
                    $usuario->save();
                }
            }
            {
                if ($this->input->post('password') && $this->input->post('cpassword')) {
                    $headerInfo['_alerts'][] = [
                        'success' => true,
                        'message' => "Senha alterada com sucesso.",
                    ];
                }
            }
            break;
        }
        {
            $this->parser->parse('main_admin', array_merge($headerInfo, $footerInfo,[
                'content' => $this->load->view('user_edit.php', [
                    'pre' => $pre,
                    'usuario' => $usuario,
                    'grupos' => \Auth\Group::all(),
                ], TRUE)
            ]));
        }
    }

    public function paginate()
    {
        $this->only_super();

        $parameter = [
            'draw' => $this->input->get('draw'),
            'search_value' => $this->input->get('search')['value'],
            'limit' => $this->input->get('length'),
            'offset' => $this->input->get('start'),
            'column_num' => intval($this->input->get('order')[0]['column']),
            'column_dir' => $this->input->get('order')[0]['dir'] == 'desc' ? 'desc' : 'asc',
        ];

        $q = \Auth\User::select('is_active', 'username', 'email', 'first_name', 'last_name', 'last_login', 'date_joined', 'id');
        switch ($parameter['column_num']) {
            case 0: $q = $q->orderBy('id', $parameter['column_dir']); break;
            case 1: $q = $q->orderBy('is_active', $parameter['column_dir']); break;
            case 2: $q = $q->orderBy('username', $parameter['column_dir']); break;
            case 3: $q = $q->orderBy('email', $parameter['column_dir']); break;
            case 4: $q = $q->orderBy('first_name', $parameter['column_dir']); break;
            case 5: $q = $q->orderBy('last_name', $parameter['column_dir']); break;
            case 6: $q = $q->orderBy('last_login', $parameter['column_dir']); break;
            case 7: $q = $q->orderBy('date_joined', $parameter['column_dir']); break;
        }
        if ($parameter['search_value']) {
            $q = $q
                ->whereRaw("lower(username) like '%{$parameter['search_value']}%'")
                ->orWhereRaw("lower(email) like '%{$parameter['search_value']}%'")
                ->orWhereRaw("lower(first_name) like '%{$parameter['search_value']}%'")
                ->orWhereRaw("lower(last_name) like '%{$parameter['search_value']}%'")
            ;
            $recordsFiltered = $q->count();
        } else {
            $recordsFiltered = $recordsTotal;
        }
        {
            $q = $q
                ->limit($parameter['limit'])
                ->offset($parameter['offset'])
            ;
        }
        $objects = $q->get();
        $data = [];
        $fieldlabel = new Helper\FieldLabelLanguage('UserForm'); $fieldlabel->enableUpdate();
        foreach($objects as $object) 
        {
            $data[] = [
                $object->id,
                (
                    $object->is_active ? 
                        $fieldlabel->get_or_new('head_active_yes') :
                        $fieldlabel->get_or_new('head_active_no')
                ),
                $object->username,
                $object->email,
                $object->first_name,
                $object->last_name,
                $object->last_login,
                $object->date_joined,
                (
                    '<a href="'.base_url().'eris/users/edit/'.$object->id.'">'.
                    '<i class="fa fa-pencil"></i>'.
                    '</a>'
                ),
            ];
        }
        $paginate_data = [
            'draw' => $parameter['draw'],
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data,
        ];
        header('Content-Type: application/json');
        echo json_encode($paginate_data);
    }

}