<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class FieldHelps extends ErisController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function index($data = [])
    {
        $this->only_admin();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['fields'] = \Assistance\FieldHelp::orderBy('form','ASC')->orderBy('field', 'ASC')->get();
        $data['content'] = $this->load->view('fieldhelp_list.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function edit($id)
    {
        $this->only_admin();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['field'] = \Assistance\FieldHelp::where('id', '=', $id)->first();
        $data['content'] = $this->load->view('fieldhelp_edit.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function save()
    {
        $this->only_admin();

        $fieldlabel = (new Helper\FieldLabelLanguage('FieldHelpForm'))->enableUpdate();
//        error_log([__FILE__ => __LINE__,'save',var_export($this->input->post(), true)]);
        if ($this->input->post('id') > 0) {
            $FieldHelp = \Assistance\FieldHelp::where('id', '=', $this->input->post('id'))->first();

            $FieldHelp->text = trim($this->input->post('text-en'));
            $FieldHelp->example = trim($this->input->post('example-en'));
            $FieldHelp->save();

            foreach (\Helper\Language::$available as $language_key => $language_value) {
                if ($language_key == 'en') continue;
                error_log(json_encode([
                    'id-'.$language_key => $this->input->post('id-'.$language_key)
                ]));
                $FieldHelpTranslation = NULL;
                if ($this->input->post('id-'.$language_key) > 0) {
                    $FieldHelpTranslation = \Assistance\FieldHelpTranslation::where('id', '=', $this->input->post('id-'.$language_key))->first();
                } else {
                    $FieldHelpTranslation = new \Assistance\FieldHelpTranslation();
                    $FieldHelpTranslation->object_id = $FieldHelp->id;
                    $FieldHelpTranslation->content_type_id = 51;
                    $FieldHelpTranslation->language = $language_key;
                }
                $FieldHelpTranslation->text = trim($this->input->post('text-'.$language_key));
                $FieldHelpTranslation->example = trim($this->input->post('example-'.$language_key));
                $FieldHelpTranslation->save();
            }
            $this->session->set_flashdata('success', $fieldlabel->get_or_new("update_success"));
            redirect("/eris/fieldHelps/edit/".$FieldHelp->id);
        } else {
            redirect("/eris/fieldHelps");
        }
    }

    public function delete($id)
    {
        $this->only_admin();

        $fieldlabel = (new Helper\FieldLabelLanguage('FieldHelpForm'))->enableUpdate();
        if ($this->session->userdata('user')->isSuper() == false) {
            redirect( base_url("/") );
            return;
        }

        $User = $this->session->userdata ( 'user' );

        $object = \Assistance\FieldHelp::where('id', '=', $id)->first();

        $object->delete();

        $this->session->set_flashdata('success', $fieldlabel->get_or_new("delete_success"));

        redirect("/eris/fieldHelps");
    }

    public function export()
    {
        $this->only_admin();

        $redis = new Redis();
        $redis->connect('redis', 6379);
        $redis->auth('rebec');

        $fieldHelp_hset_keys = $redis->keys('fieldHelp:*');

        $data = [];

        foreach ($fieldHelp_hset_keys as $hset_key) :

            $parts = explode(':', $hset_key);

            foreach ($redis->hKeys($hset_key) as $key) {

                $FieldHelp = \Assistance\FieldHelp::where('form', '=', $parts[1])
                    ->where('field', '=', $key)
                    ->with('translations')
                    ->first();

                if ($FieldHelp != null) {
                    $FieldHelp->setHidden(['id']);
                    foreach ($FieldHelp->translations as $translation) {
                        $translation->setHidden(['id', 'object_id']);
                    }
                    $FieldHelp_Array = $FieldHelp->toArray();
                    $FieldHelp_Array['class'] = 'FieldHelp';
                    $data[] = $FieldHelp_Array;
                }

            }

        endforeach;

        $filename = "fieldHelps-json-data-export.xml";
        $json = json_encode($data, TRUE);

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . strlen($json));

        header("Content-type: application/json");

        echo $json;
    }

    public function export_all()
    {
        $this->only_admin();

//        $redis = new Redis();
//        $redis->connect('redis', 6379);
//        $redis->auth('rebec');

//        $fieldHelp_hset_keys = $redis->keys('fieldHelp:*');

        $data = [];

        $FieldHelps = \Assistance\FieldHelp::with('translations')->get();
        foreach ($FieldHelps as $FieldHelp) {
            $FieldHelp->setHidden(['id']);
            foreach ($FieldHelp->translations as $translation) {
                $translation->setHidden(['id', 'object_id']);
            }
            $FieldHelp_Array = $FieldHelp->toArray();
            $FieldHelp_Array['class'] = 'FieldHelp';
            $data[] = $FieldHelp_Array;
        }

        $filename = "fieldHelps-json-data-export-all.xml";
        $json = json_encode($data, TRUE);

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . strlen($json));

        header("Content-type: application/json");

        echo $json;
    }

    public function import()
    {
        $this->only_admin();

        $fieldlabel = (new Helper\FieldLabelLanguage('FieldHelpForm'))->enableUpdate();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

//        $redis = new Redis();
//        $redis->connect('redis', 6379);
//        $redis->auth('rebec');

//echo '<pre>'.var_export([
//        __FILE__ => __LINE__,
//        '$this->input->method' => $this->input->method(),
//    ],true).'</pre>';

        if ($this->input->method() == 'get') {

            $data['content'] = $this->load->view('fieldhelp_import.php', $data, TRUE);

            $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
                'usuario' => $this->session->userdata('user'),
            ]));

        } elseif ($this->input->method() == 'post') {

            $config['upload_path']          = '/tmp/';
            $config['allowed_types']        = 'xml|json';
//            $config['max_size']             = 100;
//            $config['max_width']            = 1024;
//            $config['max_height']           = 768;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload')) {

                $json = file_get_contents($this->upload->data()['full_path']);

                $data = json_decode($json, TRUE);

                $count = 0;

                foreach ($data as $datum) {
                    $FieldHelp = \Assistance\FieldHelp::where('form', '=', $datum['form'])
                        ->where('field', '=', $datum['field'])
                        ->first();
                    if ($FieldHelp == NULL) {
                        try {
                            $FieldHelp = new \Assistance\FieldHelp([
                                'form' => $datum['form'],
                                'field' => $datum['field'],
                                'text' => utf8_decode($datum['text']),
                                'example' => utf8_decode($datum['example']),
                            ]);
                            error_log(
                                json_encode([__FILE__ => __LINE__, '$FieldHelp' => $FieldHelp])
                            );
                            $FieldHelp->save();
                            foreach ($datum['translations'] as $translation) {
                                $FieldHelpTranslation = new  \Assistance\FieldHelpTranslation([
                                    'language' => $translation['language'],
                                    'content_type_id' => $translation['content_type_id'],
                                    'object_id' => $FieldHelp->id,
                                    'text' => utf8_decode($translation['text']),
                                    'example' => utf8_decode($translation['example']),
                                ]);
                                error_log(
                                    json_encode([__FILE__ => __LINE__, '$FieldHelpTranslation' => $FieldHelpTranslation])
                                );
                                $FieldHelpTranslation->save();
                            }
                            $count++;
                        } catch(Exception $e) {
                            error_log(
                                json_encode([__FILE__ => __LINE__, '$e' => $e])
                            );
                        }
                    }
                }
                $this->session->set_flashdata('success', $fieldlabel->get_or_new("import_success").' '.($count));
            } else {
                $this->session->set_flashdata('error', $fieldlabel->get_or_new("import_fail"));
            }
            redirect("/eris/fieldHelps");
        }
    }

}