<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fossil extends ErisController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function view($layout = 'FOSSIL', $id, $revision = NULL)
    {
        $fossil = NULL;
        if ($layout == 'RBR') {
            if ($revision == NULL) {
                $fossil = \Fossil\Fossil::where('content_type_id', '=', 24)
                    ->where('is_most_recent', '=', 1)
                    ->where('display_text', 'LIKE', ($id. ' %'))
                    ->first();
            } else {
                $fossil = \Fossil\Fossil::where('content_type_id', '=', 24)
                    ->where('revision_sequential', '=', $revision)
                    ->where('display_text', 'LIKE', ($id. ' %'))
                    ->first();
            }
        } else if ($layout == 'FOSSIL') {
            if ($revision == NULL) {
                $fossil = \Fossil\Fossil::where('content_type_id', '=', 24)
                    ->where('is_most_recent', '=', 1)
                    ->where('object_id', '=', $id)
                    ->first();
            } else {
                $fossil = \Fossil\Fossil::where('content_type_id', '=', 24)
                    ->where('revision_sequential', '=', $revision)
                    ->where('object_id', '=', $id)
                    ->first();
            }
        }
        if (is_null($fossil)) {
            redirect("/");
            return;
        }
        {
            $data['content'] = $this->load->view('fossil_view.php', [
                'layout' => $layout,
                'fossil' => $fossil,
            ], TRUE);
        }
        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    public function list($layout = "FOSSIL", $page = 1)
    {
        $data = [];
        {
            $per_page = 20;
            $total = \Fossil\Fossil::where('content_type_id', '=', 24)
                ->where('is_most_recent', '=', 1)
                ->count();
            $pages = $total / $per_page;
            $pages = ($pages > intval($pages)) ? intval($pages) + 1 : intval($pages);
            $data['content_left'] = $this->load->view('fossil_list.php', [
                'fosseis' => \Fossil\Fossil::where('content_type_id', '=', 24)
                    ->where('is_most_recent', '=', 1)
                    ->limit($per_page)
                    ->offset($per_page * ($page - 1))
                    ->orderBy('creation', 'desc')
                    ->get(),
                'total' => $total,
                'page' => $page,
                'pages' => $pages,
            ], TRUE);
        }
        {
            $data['content_right'] = $this->load->view('news_preview.php', [
                'newses' => \ReviewApp\News::take(5)->orderBy('created', 'desc')->get()
            ], TRUE);;
        }

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

}