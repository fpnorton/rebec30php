<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends ErisController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function index()
    {
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];
        $_pages = [];

        {
            $this->parser->parse('main_admin', array_merge($headerInfo, $footerInfo,[
                'content' => ''
            ]));
        }
    }

}