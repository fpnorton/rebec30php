<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FlatPages extends ErisController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function index()
    {
        $this->only_admin();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['pages'] = \Django\FlatPage::all();
        $data['content'] = $this->load->view('flatpage_list.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function create()
    {
        $this->only_admin();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['flatpage'] = [];
        $data['content'] = $this->load->view('flatpage_create.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    public function edit($id)
    {
        $this->only_admin();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['page'] = \Django\FlatPage::where('id', '=', $id)->first();
        $data['content'] = $this->load->view('flatpage_edit.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    /**
     * ALTER TABLE xpto CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci
     */
    public function save()
    {
        $this->only_admin();

        $fieldlabel = (new Helper\FieldLabelLanguage('FlatPageForm'))->enableUpdate();

        if ($this->input->post('id') > 0) {

            $url = trim($this->input->post('url'));

            $FlatPage = \Django\FlatPage::where('id', '=', $this->input->post('id'))->first();

            $FlatPage->url = $url;
            $FlatPage->title = trim($this->input->post('title-en'));
            $FlatPage->content = trim($this->input->post('content-en'));
            $FlatPage->save();

            foreach (\Helper\Language::$available as $language_key => $language_value) {
                if ($this->input->post('id-'.$language_key) > 0) {
                    $FlatPageTranslation = \Django\FlatPageTranslation::where('id', '=', $this->input->post('id-'.$language_key))->first();
                    $FlatPageTranslation->title = trim($this->input->post('title-'.$language_key));
                    $FlatPageTranslation->content = trim($this->input->post('content-'.$language_key));
                    $FlatPageTranslation->save();
                }
            }
            $this->session->set_flashdata('success', $fieldlabel->get_or_new("update_success"));
            redirect("/eris/flatPages/edit/".$FlatPage->id);
        } else {
            $url = trim($this->input->post('url'));
            $FlatPage = \Django\FlatPage::where('url', '=', $url)->orWhere('url', '=', '/'.$url.'/')->first();
            if ($FlatPage == NULL) {
                $FlatPage = new \Django\FlatPage([
                    'url' => $url,
                    'title' => trim($this->input->post('title-en')),
                    'content' => trim($this->input->post('content-en')),
                    'enable_comments' => '1',
                    'template_name' => '',
                    'registration_required' => '0',
                ]);
                $FlatPage->save();
                foreach (\Helper\Language::$available as $language_key => $language_value) {
                    if ($language_key == 'en') continue;
                    $FlatPageTranslation = new \Django\FlatPageTranslation([
                        'title' => trim($this->input->post('title-'.$language_key)),
                        'content' => trim($this->input->post('content-'.$language_key)),
                        'object_id' => $FlatPage->id,
                        'content_type_id' => 9,
                        'language' => $language_key,
                    ]);
                    $FlatPageTranslation->save();
                }
                $this->session->set_flashdata('success', $fieldlabel->get_or_new("insert_success"));
            } else {
                $this->session->set_flashdata('error', $fieldlabel->get_or_new("insert_exists"));
            }

            redirect("/eris/flatPages/edit/".$FlatPage->id);
        }
    }

    public function view($url)
    {
        $data['page'] = \Django\FlatPage::where('url', '=', $url)->orWhere('url', '=', '/'.$url.'/')->first();
        if ($data['page'] == NULL) {
            redirect(base_url("welcome"));
            return;
        }

        $data['content'] = $this->load->view('flatpage_view.php', $data, TRUE);

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function delete($id)
    {
        $this->only_admin();

        $fieldlabel = (new Helper\FieldLabelLanguage('FlatPageForm'))->enableUpdate();

        if ($this->session->userdata('user')->isSuper() == false) {
            redirect( base_url("/") );
            return;
        }

        $object = \Django\FlatPage::where('id', '=', $id)->first();

        $object->delete();

        $this->session->set_flashdata('success', $fieldlabel->get_or_new("delete_success"));

        redirect("/eris/flatPages");
    }

}