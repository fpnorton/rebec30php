<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questions extends ErisController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function index()
    {
        $this->only_admin();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['questions'] = \Assistance\Question::all();
        $data['content'] = $this->load->view('question_list.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function edit($id)
    {
        $this->only_admin();

        $User = $this->session->userdata ( 'user' );
        $data['content'] = '';
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];

        $data['question'] = \Assistance\Question::where('id', '=', $id)->first();
        $data['content'] = $this->load->view('question_edit.php', $data, TRUE);

        $this->parser->parse('main_admin', array_merge($data, $headerInfo, $footerInfo, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function save()
    {
        $this->only_admin();

        $fieldlabel = (new Helper\FieldLabelLanguage('QuestionForm'))->enableUpdate();
        if ($this->input->post('id') > 0) {
            $Question = \Assistance\Question::where('id', '=', $this->input->post('id'))->first();

            $Question->order = trim($this->input->post('order'));
            $Question->title = trim($this->input->post('title-en'));
            $Question->answer = trim($this->input->post('answer-en'));
            $Question->save();

            foreach (\Helper\Language::$available as $language_key => $language_value) {
                if ($this->input->post('id-'.$language_key) > 0) {
                    $QuestionTranslation = \Assistance\QuestionTranslation::where('id', '=', $this->input->post('id-'.$language_key))->first();
                    $QuestionTranslation->title = trim($this->input->post('title-'.$language_key));
                    $QuestionTranslation->answer = trim($this->input->post('answer-'.$language_key));
                    $QuestionTranslation->save();

                }
            }
            $this->session->set_flashdata('success', $fieldlabel->get_or_new("update_success"));
            redirect("/eris/questions/edit/".$Question->id);
        } else {
            $Question = new \Assistance\Question([
                'order' => trim($this->input->post('order')),
                'title' => trim($this->input->post('title-en')),
                'answer' => trim($this->input->post('text-en')),
                'created' => date('Y-m-d H:i:s'),
            ]);
            $Question->save();
            foreach (\Helper\Language::$available as $language_key => $language_value) {
                if ($language_key == 'en') continue;
                $QuestionTranslation = new \Assistance\QuestionTranslation([
                    'title' => trim($this->input->post('title-'.$language_key)),
                    'answer' => trim($this->input->post('answer-'.$language_key)),
                    'object_id' => $Question->id,
                    'content_type_id' => 49,
                    'language' => $language_key,
                ]);
                $QuestionTranslation->save();
            }
            $this->session->set_flashdata('success', $fieldlabel->get_or_new("insert_success"));
            redirect("/eris/questions/edit/".$Question->id);
        }
    }

    public function faq()
    {
        $data = [];
        {
            $data['content_left'] = $this->load->view('question_faq.php', [
                'questions' => \Assistance\Question::orderBy('order', 'asc')->get()
            ], TRUE);
        }
        {
            $data['content_right'] = $this->load->view('fossil_preview.php', [
                'fosseis' => \Fossil\Fossil::where('content_type_id', '=', 24)
                    ->where('is_most_recent', '=', 1)
                    ->take(7)
                    ->orderBy('creation', 'desc')
                    ->get()
            ], TRUE);;
        }
        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));

    }

    public function delete($id)
    {
        $this->only_admin();

        $fieldlabel = (new Helper\FieldLabelLanguage('QuestionForm'))->enableUpdate();

        if ($this->session->userdata('user')->isSuper() == false) {
            redirect( base_url("/") );
            return;
        }

        $object = \Assistance\Question::where('id', '=', $id)->first();

        $object->delete();

        $this->session->set_flashdata('success', $fieldlabel->get_or_new("delete_success"));

        redirect("/eris/questions");

    }

}