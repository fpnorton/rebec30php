﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();?>
<?php $fieldlabel = new Helper\FieldLabelLanguage('SummaryForm'); $fieldlabel->enableUpdate(); ?>
<?php $fieldhelp = new Helper\FieldLabelLanguage('SummaryForm'); $fieldhelp->enableUpdate(); ?>
<div class="row">
    <div class="col-sm-12 content">
        <div class="row">
            <div class="col-2">
                <?php echo $fieldlabel->get_or_new('submission_title'); ?>
            </div>
            <div class="col-10">
                <? echo $VD['subData']->title ?>
            </div>
        </div>
        <div class="row">
            <div class="col-2">
                <?php echo $fieldlabel->get_or_new('submission_status'); ?>
            </div>
            <div class="col-10">
                <? echo $VD['subData']->status ?>
            </div>
        </div>
        <? if (@$version != NULL) : ?>
            <div class="row">
                <div class="col-12"><hr /></div>
            </div>
            <div class="accordion-group accordion-caret">
                <div class="row accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" href="#diff">
                        <?php echo $fieldlabel->get_or_new('subtitle_version_changes'); ?>
                    </a>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="row">
                    <style>
                        .Differences {
                            width: 100%;
                            border-collapse: collapse;
                            border-spacing: 0;
                            empty-cells: show;
                        }

                        .Differences thead th {
                            text-align: left;
                            border-bottom: 1px solid #000;
                            background: #aaa;
                            color: #000;
                            padding: 4px;
                        }
                        .Differences tbody th {
                            text-align: right;
                            background: #ccc;
                            width: 4em;
                            padding: 1px 2px;
                            border-right: 1px solid #000;
                            vertical-align: top;
                            font-size: 13px;
                        }

                        .Differences td {
                            padding: 1px 2px;
                            font-family: Consolas, monospace;
                            font-size: 13px;
                        }

                        .DifferencesSideBySide .ChangeInsert td.Left {
                            background: #dfd;
                        }

                        .DifferencesSideBySide .ChangeInsert td.Right {
                            background: #cfc;
                        }

                        .DifferencesSideBySide .ChangeDelete td.Left {
                            background: #f88;
                        }

                        .DifferencesSideBySide .ChangeDelete td.Right {
                            background: #faa;
                        }

                        .DifferencesSideBySide .ChangeReplace .Left {
                            background: #fe9;
                        }

                        .DifferencesSideBySide .ChangeReplace .Right {
                            background: #fd8;
                        }

                        .Differences ins, .Differences del {
                            text-decoration: none;
                        }

                        .DifferencesSideBySide .ChangeReplace ins, .DifferencesSideBySide .ChangeReplace del {
                            background: #fc0;
                        }

                        .Differences .Skipped {
                            background: #f7f7f7;
                        }

                        .DifferencesInline .ChangeReplace .Left,
                        .DifferencesInline .ChangeDelete .Left {
                            background: #fdd;
                        }

                        .DifferencesInline .ChangeReplace .Right,
                        .DifferencesInline .ChangeInsert .Right {
                            background: #dfd;
                        }

                        .DifferencesInline .ChangeReplace ins {
                            background: #9e9;
                        }

                        .DifferencesInline .ChangeReplace del {
                            background: #e99;
                        }

                        pre {
                            width: 100%;
                            overflow: auto;
                        }
                    </style>
                    <div id="diff" class="col-12 collapse in">
                        <? $php_diff = new Diff($version['left'], $version['right']); ?>
                        <? echo $php_diff->render(new  Diff_Renderer_Html_SideBySide()); ?>
                    </div>
                </div>
            </div>
        <? endif; ?>
        <div class="row">
            <table class="table table-bordered table_list table_sumary">
                <thead>
                <tr class="table_title">
                    <th><?php echo $fieldlabel->get_or_new('step'); ?></th>
                    <th><?php echo $fieldlabel->get_or_new('name'); ?></th>
                    <th><?php echo $fieldlabel->get_or_new('status'); ?></th>
                    <th><?php echo $fieldlabel->get_or_new('updated'); ?></th>
                </tr>
                </thead>
                <tbody>
                <? $count = 0; ?>
                <?php foreach(ClinicalTrialSteps::fieldsByName as $passo):
                    $file_name = $VD['fields_status']['en'][$passo] == 4 ? 'ok' : 'error';
                    $tip = NULL;
                    if ($VD['subData']->remarks->count() > 0)
                    {
                        $context = ClinicalTrialSteps::remarksByName[$count];
                        $statuses = ['open'];
                        $collection = 
                            $VD['subData']->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                                if (in_array($remark->status, $statuses)) {
                                    return $remark;
                                }
                            });
                        if ($collection->count() > 0) $tip = $collection->sortByDesc('id')->first()->text;
                    }
                    else 
                    {
                        $tip = $fieldhelp->get_or_new('tip_'.($count+1));
                    }
                    ?>
                    <tr>
                        <!-- PASSO -->
                        <td><?php echo (++$count); ?></td>
                        <!-- Nome -->
                        <td>
                            <a href="<?php echo base_url( "/revisor/submissao/passo/".($count).'/'.$clinicaltrial_id); ?>">
                                <?php echo $genericlabel->get_or_new('step_'.($count)); ?>
                            </a>
                        </td>
                        <!-- SITUAÇÃO -->
                        <td>
                            <?php
                            $situacao = array(
                                1 => $genericlabel->get_or_new('situation_1_changed'),
                                2 => $genericlabel->get_or_new('situation_2_missing'),
                                3 => $genericlabel->get_or_new('situation_3_partial'),
                                4 => $genericlabel->get_or_new('situation_4_complete')
                            );
                            ?>
                            <img data-toggle="tooltip"
                                 data-placement="auto"
                                 src="<?php echo base_url('assets/images/field-' . $file_name . '.png')?>"
                                 data-original-title="<?php echo $tip; ?>">
                            <span class="table-inner-text"><?php echo $situacao[ClinicalTrialSteps::getFieldStatusCompletedByName($VD['fields_status']['en'], $passo)]; ?></span>
                        </td>
                        <!-- ALTERADO -->
                        <td>
                            <span><!-- ALTERADO? --></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-12"><hr /></div>
        </div>
        <?php if ($VD['subData']->isPodeSerRevisado() and ($usuario->isRevisor() or $usuario->isSuper())) : ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-2">
                    <button data-toggle="modal" data-target="#modal_resubmit"
                            class="btn btn-danger"><?php echo $fieldlabel->get_or_new('submission_button_resubmit'); ?>
                    </button>
                </div>
                <div class="col-8">&nbsp;</div>
                <div class="col-2">
                    <button data-toggle="modal" data-target="#modal_approve"
                            class="btn btn-success"><?php echo $fieldlabel->get_or_new('submission_button_approve'); ?>
                    </button>
                </div>
            </div>
        </div>
        <style>
            /*.modal-dialog {*/
            /*    height: 50%;*/
            /*}*/
        </style>
        <!-- -->
        <? foreach ([
                'approve' => [
                    'modal-content-style' => 'border-width: 2px; border-color: green;',
                ],
                'resubmit' => [
                    'modal-content-style' => 'border-width: 2px; border-color: red;',
                ],
            ] as $modal_key => $modal_pack) : ?>
            <div class="modal fade"
                 id="<?php echo 'modal_'.$modal_key ?>"
                 tabindex="-1"
                 role="dialog"
                 aria-labelledby="<?php echo 'modalLabel_'.$modal_key ?>"
                 aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered"
                     role="document">
                    <div class="modal-content" style="<? echo $modal_pack['modal-content-style'] ?>">
                        <div class="modal-header">
                            <h5 class="modal-title"
                                id="<?php echo 'modalLabel_'.$modal_key ?>"><?php echo $fieldlabel->get_or_new('title_modal_'.$modal_key); ?>
                            </h5>
                            <button type="button"
                                    class="close"
                                    data-dismiss="modal"
                                    aria-label="<?php echo $genericlabel->get_or_new('button_close'); ?>">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body"><?php echo $fieldlabel->get_or_new('text_modal_'.$modal_key) ?>
                        </div>
                        <div class="modal-footer">
                            <form action="<?php echo base_url('revisor/'.$modal_key.'/'.$clinicaltrial_id).'?'.$postback ?>" method="post">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $genericlabel->get_or_new('button_close'); ?></button>
                                <button type="submit" class="btn btn-primary"><?php echo $genericlabel->get_or_new('button_confirm'); ?></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
        <!-- -->
        <?php endif; ?>
    </div>
</div>
<?php $genericlabel->disableUpdate();?>
<?php $fieldlabel->disableUpdate(); ?>
<?php $fieldhelp->disableUpdate(); ?>
