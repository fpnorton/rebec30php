﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $genericlabel = new Helper\FieldLabelLanguage('General'); ?>
<div class="row">
    <div class="col-sm-12 content">
        <div class="container">
            <div class="row"><?php if (empty($message) == false) echo $message; ?></div>
        </div>
    </div>
</div>
