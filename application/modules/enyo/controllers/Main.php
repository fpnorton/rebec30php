<?php

use Picolo\Approved;
use Picolo\Resubmit;
use Repository\ClinicalTrial;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends EnyoController {

    public function show_message($_message) {
		$this->only_reviewer();
        {
            $data['content_left'] = $this->load->view('menu.php', [
            ], TRUE);
        }
        {
            $data['content_right'] =
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $this->load->view('main_message.php', ['message' => $_message], TRUE).
                '</section>'.
                '</div>';
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    public function blank()
    {
        $this->show_message('');
    }

    private function desclonar_contato(\Repository\Contact $contact) {
        $contact = \Repository\Contact::where('id','=', $contact->id)->first();
        if ($contact->contact_origin_id > 0) {
            if ($contact->_deleted == 0) return $contact->contact_origin_id;
            // anterior removido - ressucitando
            $data = $contact->withHidden(['creator_id'])->toArray();
            $contact = new \Repository\Contact($data);
            $contact->save();
        }
        return $contact->id;
    }

    public function resubmit($clinicaltrial_id)
    {
		$this->only_reviewer();

        $submission = NULL;
        $clinicaltrial = NULL;
        {
            $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
            if ($this->session->userdata('user') == NULL) {
                redirect("welcome");
                return;
            }
            if ($this->session->userdata('user')->isRevisor() == false) {
                $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
                redirect("/revisor");
                return;
            }
            $submission = \ReviewApp\Submission::where('_deleted', '=', 0)
                ->where('trial_id', '=', $clinicaltrial_id)
                ->first();
            if ($submission->isPending() == false) {
                $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
                redirect("/revisor");
                return;
            }
        }
        $clinicaltrial = \Repository\ClinicalTrial::where('_deleted', '=', 0)
            ->where('id', '=', $clinicaltrial_id)
            ->first();
        {
            // ficarão abertos
            foreach($submission->remarks->where('status', 'acknowledged') as $review_remark) {
                $review_remark->update([
                    'status' => 'closed',
                ]);
            }

            $submission->update([
                'updated' => date('Y-m-d H:i:s'),
                'updater_id' => $this->session->userdata('user')->id,
                'status' => 'resubmit',
            ]);
            
            {
                foreach ($clinicaltrial->publicContacts as $publicContact) {
                    if ($publicContact->contact == NULL) continue;
                    $publicContact->update([
                        'contact_id' => $this->desclonar_contato($publicContact->contact)
                    ]);
                }
                foreach ($clinicaltrial->scientificContacts as $scientificContact) {
                    if ($scientificContact->contact == NULL) continue;
                    $scientificContact->update([
                        'contact_id' => $this->desclonar_contato($scientificContact->contact)
                    ]);
                }
                foreach ($clinicaltrial->siteContacts as $siteContact) {
                    if ($siteContact->contact == NULL) continue;
                    $siteContact->update([
                        'contact_id' => $this->desclonar_contato($siteContact->contact)
                    ]);
                }
                foreach ($clinicaltrial->ethicsReviewContacts as $ethicsReviewContact) {
                    if ($ethicsReviewContact->contact == NULL) continue;
                    $ethicsReviewContact->update([
                        'contact_id' => $this->desclonar_contato($ethicsReviewContact->contact)
                    ]);
                }
            }

            $_subject = '';
            $_message = '';
            {
                $fieldlabel = new Helper\FieldLabelLanguage('MailMessageForm'); $fieldlabel->enableUpdate();
                $_subject = $fieldlabel->get_or_new('resubmit_mail_subject');
            }
            {
                $MailMessage = Vocabulary\MailMessage::where('label', '=', 'resubmitted')->first();
                if (linguagem_selecionada() != 'en') $MailMessage = $MailMessage->translations()->where('language', '=', linguagem_selecionada())->first();
                $_message = $MailMessage->description;
            }
            {
                $dictionary = [
                    '__RBR__' => $clinicaltrial->trial_id,
                    '__TRIAL_ID__' => $submission->trial_id,
                    '__TITLE__' => $submission->title,
                ];
                $_subject = strtr($_subject, $dictionary);
                $_message = strtr($_message, $dictionary);
            }
            {
                $to = $submission->author->email;
                $bcc = NULL;
                if (!empty($this->session->userdata('user')->email)) $bcc = $this->session->userdata('user')->email;
                //
                $this->mailfunctions->send(
                    (new \Helper\Email(NULL, 'ReBEC'))
                        ->addTO($to)
                        ->addBCC($bcc)
                        ->addSUBJECT($_subject)
                        ->addBODY($_message),
                    $submission->trial_id
                );
            }

            (new Resubmit([
                'resubmit_date' => date('Y-m-d H:i:s'),
                'trial_id' => $clinicaltrial_id,
            ]))->save();

            \Picolo\RevisorInfo::where('trial_id', '=', $clinicaltrial_id)
                ->update([
                    'reviewer' => 0
                ]);

            $this->show_message($_message);
        }
    }

    public function approve($clinicaltrial_id)
    {
		$this->only_reviewer();

        $this->load->library('RedisFunctions');
        $submission = NULL;
        $clinicaltrial = NULL;
        {
            $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
            if ($this->session->userdata('user') == NULL) {
                redirect("welcome");
                return;
            }
            if ($this->session->userdata('user')->isRevisor() == false) {
                $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
                redirect("/revisor");
                return;
            }
            $submission = \ReviewApp\Submission::where('_deleted', '=', 0)
                ->where('trial_id', '=', $clinicaltrial_id)
                ->first();
            $clinicaltrial = \Repository\ClinicalTrial::where('_deleted', '=', 0)
                ->where('id', '=', $clinicaltrial_id)
                ->first();
            if ($submission->isPending() == false) {
                $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
                redirect("/revisor");
                return;
            }
        }
        {
            foreach($submission->remarks->where('status', 'open') as $review_remark) {
                $review_remark->update([
                    'status' => 'closed',
                ]);
            }
            foreach($submission->remarks->where('status', 'acknowledged') as $review_remark) {
                $review_remark->update([
                    'status' => 'closed',
                ]);
            }

            $submission->update([
                'updated' => date('Y-m-d H:i:s'),
                'updater_id' => $this->session->userdata('user')->id,
                'status' => 'approved',
            ]);

            $this->createFossil($clinicaltrial_id);

            $fossil = \Fossil\Fossil::where('content_type_id', '=', 24)
                ->where('is_most_recent', '=', 1)
                ->where("object_id", '=', $clinicaltrial_id)
                ->first();

            $this->redisfunctions->update_redis_metaphone($fossil);

            $clinicaltrial = \Repository\ClinicalTrial::where('_deleted', '=', 0)
                ->where('id', '=', $clinicaltrial_id)
                ->first();
            
            $_subject = '';
            $_message = '';
            {
                $fieldlabel = new Helper\FieldLabelLanguage('MailMessageForm'); $fieldlabel->enableUpdate();
                $_subject = $fieldlabel->get_or_new('approved_mail_subject');
            }
            {
                $MailMessage = Vocabulary\MailMessage::where('label', '=', 'approved')->first();
                if (linguagem_selecionada() != 'en') $MailMessage = $MailMessage->translations()->where('language', '=', linguagem_selecionada())->first();
                $_message = $MailMessage->description;
            }
            {
                $dictionary = [
                    '__RBR__' => $clinicaltrial->trial_id,
                    '__TRIAL_ID__' => $submission->trial_id,
                    '__TITLE__' => $submission->title,
                ];
                $_subject = strtr($_subject, $dictionary);
                $_message = strtr($_message, $dictionary);
            }
            {
                $to = $submission->author->email;
                $bcc = NULL;
                if (!empty($this->session->userdata('user')->email)) $bcc = $this->session->userdata('user')->email;
                //
                $this->mailfunctions->send(
                    (new \Helper\Email(NULL, 'ReBEC'))
                        ->addTO($to)
                        ->addBCC($bcc)
                        ->addSUBJECT($_subject)
                        ->addBODY($_message),
                    $submission->trial_id
                );
            }

            (new Approved([
                'approved_date' => date('Y-m-d H:i:s'),
                'trial_id' => $clinicaltrial_id,
            ]))->save();

            \Picolo\RevisorInfo::where('trial_id', '=', $clinicaltrial_id)
            ->update([
                'reviewer' => 0
            ]);

            $this->show_message($_message);
        }
    }

    /*************************************/
    public function __construct()
    {
        parent::__construct();

        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");

        $this->load->library('parser');
        $this->load->library('upload');

        //Load Languages
        //$this->languageslib->loadLanguage();

        //Load Libraries
        $this->load->library('AuxFunctions');

        //Load Helpers

        //Load models
//        $this->load->model('Fossil_model', 'fossil_model', TRUE);
//        $this->load->model('ClinicalTrial_model', 'ct_model', TRUE);
//        $this->load->model('SupportSource_model', 'ss_model', TRUE);
//        $this->load->model('InterventionCode_model', 'ic_model', TRUE);
//        $this->load->model('RecruitmentCountry_model', 'rc_model', TRUE);
//        $this->load->model('Institution_model', 'inst_model', TRUE);
//        $this->load->model('Vocabulary_model', 'voc_model', TRUE);
//        $this->load->model('VocabularyTranslation_model', 'vt_model', TRUE);
//        $this->load->model('ClinicalTrialContact_model', 'ctc_model', TRUE);
//        $this->load->model('Contact_model', 'cnt_model', TRUE);
//        $this->load->model('Outcomes_model', 'out_model', TRUE);
//        $this->load->model('OutcomesTranslation_model', 'otrans_model', TRUE);
//        $this->load->model('TrialNumber_model', 'tn_model', TRUE);
//        $this->load->model('Descriptor_model', 'des_model', TRUE);
//        $this->load->model('DescriptorTranslation_model', 'dt_model', TRUE);
//        $this->load->model('SecondarySponsors_model', 'secs_model', TRUE);
//        $this->load->model('ClinicalTrialTranslations_model', 'ctt_model', TRUE);

        //Update vocabulary config array
//        SELF::updateVocabulary();
    }

    /* PYTHON
        BASE36 = string.digits+string.ascii_lowercase
        BASE28 = ''.join(d for d in BASE36 if d not in '1l0aeiou')

        def generate_trial_id(prefix, num_digits):
        s = str(randrange(2,10)) # start with a numeric digit 2...9
        s += ''.join(choice(BASE28) for i in range(1, num_digits))
        return '-'.join([prefix, s])
    */

    private function generate_trial_id($prefix, $num_digits) {
        $_BASE28 = str_split("23456789bcdfghjkmnpqrstvwxyz");
        $_return = mt_rand(2, 10);
        for ($i = 0; $i < $num_digits; $i++) {
            $_position = array_rand($_BASE28);
            $_return .= $_BASE28[$_position];
        }
        return $prefix.'-'.$_return;
    }

    /**
     * @param $obj_id = clinicaltrial.id
     */
    public function create_fossil_from_clinicatrial()
    {
        $trial_id = intval($this->input->post('trial_id'));
        $fossilized = $this->createFossil($trial_id, TRUE);
        $this->session->set_flashdata('pre', ['fossil' => json_encode($fossilized, JSON_PRETTY_PRINT)]);
        redirect('xml_ictrp');
    }

    /**
     * @param $obj_id = clinicaltrial.id
     */
    private function createFossil($obj_id, $debug = FALSE)
    {
        $ClinicalTrial = ClinicalTrial::where('id', '=', $obj_id)->first();

        //Verificar se j� existe algum fossil desse estudo na tabela
        $most_recent_fossil = \Fossil\Fossil::most_recent($obj_id);

        //Verificar qual � o maior sequencial (revision_sequential) e o seu id
        $revision_sequential = ($most_recent_fossil ? $most_recent_fossil->revision_sequential+1 : 1);
        $previous_revision_id = ($most_recent_fossil ? $most_recent_fossil->id : NULL);
        $content_type_id = 24;

        error_log(json_encode([
            __FILE__ => __LINE__,
            '$revision_sequential' => $revision_sequential,
        ]));
        $rbr_code = NULL;
        if ($revision_sequential > 1) {
            $rbr_code = $ClinicalTrial->trial_id;
        } elseif ($revision_sequential == 1) {
            $rbr_code = $this->generate_trial_id('RBR', 6);
        }

        error_log(json_encode([
            __FILE__ => __LINE__,
            'RBR-CODE' => ($rbr_code ?? 'nulo'),
        ]));

        if (is_null($rbr_code)) return;
        if (strpos($rbr_code, 'RBR-') == -1) return;

        // update RBR
        if ($revision_sequential == 1) {
            $ClinicalTrial->update([
                'trial_id' => $rbr_code,
                'date_registration' => date('Y-m-d H:i:s'),
            ]);
        }
        
        //Inserir o novo fossil com is_most_recent=true, previous_sequential_id=id_anterior, revision_sequential=revision_sequential+1
        $id = $this->auxfunctions->getGUID();
        $serialized = SELF::createSerializedFossil($obj_id);
        $json_serialized = json_encode($serialized);
        $creation = unix_to_human(time(), TRUE, 'eu');
        $display_text =
            $rbr_code .
            " " .
            substr($serialized['public_title'], 0, 120) .
            (strlen($serialized['public_title']) > 120 ? "..." : "")
        ;

        if ($debug == FALSE) {
            // create most recent fossil
            $new_fossil_data =
                [
                    'id' => $id,
                    'serialized' => $json_serialized,
                    'display_text' => $display_text,
                    'creation' => $creation,
                    'content_type_id' => $content_type_id,
                    'object_id' => $obj_id,
                    'is_most_recent' => TRUE,
                    'previous_revision_id' => $previous_revision_id,
                    'revision_sequential' => $revision_sequential,
                ];
            //
            $new_fossil = \Fossil\Fossil::insert($new_fossil_data);
            // update most recent TO not most recent
            if ($most_recent_fossil) {
                $most_recent_fossil->update([
                    'is_most_recent' => FALSE,
                ]);
            }
        } else {
            return $serialized;
        }
    }

    private function createSerializedFossil($obj_id)
    {
        {
            $ClinicalTrial = ClinicalTrial::where('id', '=', $obj_id)->first();
            if (is_null($ClinicalTrial->study_type_id)) {
                $study_type_id = ($ClinicalTrial->is_observational == 1) ? 2 : 1;
            } else  {
                $study_type_id = $ClinicalTrial->study_type_id;
            }
            $serialized = array(
                '__model__'						=> 'ClinicalTrial',
                '__unicode__'					=> $ClinicalTrial->trial_id . ' ' . $ClinicalTrial->public_title,
                'id'							=> $ClinicalTrial->id,
                'scientific_title'				=> $ClinicalTrial->scientific_title,
                'support_sources'				=> SELF::serializeSupportSources($ClinicalTrial->supportSources()->get()),
                'time_perspective'				=> SELF::serializeVocabulary($ClinicalTrial->time_perspective_id, "timeperspective"),
                'acronym_display'				=> $ClinicalTrial->acronym . ' ' . $ClinicalTrial-> acronym_expansion,
                'public_title'					=> $ClinicalTrial->public_title,
                'enrollment_end_actual'			=> $ClinicalTrial->enrollment_end_actual,
                'trial_id'						=> $ClinicalTrial->trial_id,
                'scientific_acronym_display'	=> $ClinicalTrial->scientific_acronym . ' ' . $ClinicalTrial->scientific_acronym_expansion,
                'inclusion_criteria'			=> $ClinicalTrial->inclusion_criteria,
                'recruitment_country'			=> SELF::serializeRecruitmentCountry($ClinicalTrial->recruitmentCountries()->get()),
                'i_code'						=> SELF::serializeInterventionCode($ClinicalTrial->interventionCodes()->get()),
                'agemax_value'					=> $ClinicalTrial->agemax_value,
                'agemax_unit'					=> $ClinicalTrial->agemax_unit,
                'expanded_access_program'		=> boolval($ClinicalTrial->expanded_access_program),
                'agemin_unit'					=> $ClinicalTrial->agemin_unit,
                'scientific_acronym'			=> $ClinicalTrial->scientific_acronym,
                'acronym'						=> $ClinicalTrial->acronym,
                'hc_freetext'					=> $ClinicalTrial->hc_freetext,
                'masking'						=> SELF::serializeVocabulary($ClinicalTrial->masking_id, "studymasking"),
                'is_observational'				=> $ClinicalTrial->is_observational,
                'pk'							=> $ClinicalTrial->id,
                'observational_study_design'	=> SELF::serializeVocabulary($ClinicalTrial->observational_study_design_id, "observationalstudydesign"),
                'number_of_arms'				=> $ClinicalTrial->number_of_arms,
                'primary_outcomes'				=> SELF::serializeOutcome($ClinicalTrial->outcomes()->where('interest', '=', 'primary')->get()),
                'secondary_outcomes'			=> SELF::serializeOutcome($ClinicalTrial->outcomes()->where('interest', '=', 'secondary')->get()),
                'trial_number'					=> SELF::serializeTrialNumber($ClinicalTrial->numbers()->get()),
                'updated'						=> $ClinicalTrial->updated,
                'target_sample_size'			=> $ClinicalTrial->target_sample_size,
                'study_type'					=> SELF::serializeVocabulary($study_type_id, "studytype"),
                'i_freetext'					=> $ClinicalTrial->i_freetext,
                'acronym_expansion'				=> $ClinicalTrial->acronym_expansion,
                'enrollment_start_actual'		=> $ClinicalTrial->enrollment_start_actual,
                'enrollment_end_planned'		=> $ClinicalTrial->enrollment_end_planned,
                'allocation'					=> SELF::serializeVocabulary($ClinicalTrial->allocation_id, "studyallocation"),
                'utrn_number'					=> $ClinicalTrial->utrn_number,
                'scientific_acronym_expansion'	=> $ClinicalTrial->scientific_acronym_expansion,
                'exported'						=> $ClinicalTrial->exported,
                'purpose'						=> SELF::serializeVocabulary($ClinicalTrial->purpose_id, "studypurpose"),
                'phase'							=> SELF::serializeVocabulary($ClinicalTrial->phase_id, "studyphase"),
                'public_contact'				=> SELF::serializeClinicalTrialContact($ClinicalTrial->publicContacts()->get()),
                'site_contact'					=> SELF::serializeClinicalTrialContact($ClinicalTrial->siteContacts()->get()),
                'scientific_contact'			=> SELF::serializeClinicalTrialContact($ClinicalTrial->scientificContacts()->get()),
                'ethics_contact'				=> SELF::serializeClinicalTrialContact($ClinicalTrial->ethicsReviewContacts()->get()),
                'date_registration'				=> $ClinicalTrial->date_registration,
                'agemin_value'					=> $ClinicalTrial->agemin_value,
                'enrollment_start_planned'		=> $ClinicalTrial->enrollment_start_planned,
                'staff_note'					=> $ClinicalTrial->staff_note,
                'study_design'					=> $ClinicalTrial->study_design,
                'language'						=> $ClinicalTrial->language,
                'created'						=> $ClinicalTrial->created,
                'gender'						=> $ClinicalTrial->gender,
                'primary_sponsor'				=> SELF::serializeInstitution($ClinicalTrial->primary_sponsor_id),
                'secondary_sponsors'			=> SELF::serializeSecondarySponsors($ClinicalTrial->secondarySponsors()->get()),
                'translations'					=> SELF::serializeClinicalTrialTranslations($ClinicalTrial->translations()->get()),
                'hc_keyword'					=> SELF::serializeDescriptor($ClinicalTrial->descriptors()->where('aspect', '=', 'HealthCondition')->where('level', '=', 'general')->get()),
                'hc_code'				    	=> SELF::serializeDescriptor($ClinicalTrial->descriptors()->where('aspect', '=', 'HealthCondition')->where('level', '=', 'specific')->get()),
                'intervention_keyword'			=> array_merge(
                    SELF::serializeDescriptor($ClinicalTrial->descriptors()->where('aspect', '=', 'Intervention')->where('level', '=', 'specific')->get()),
                    SELF::serializeDescriptor($ClinicalTrial->descriptors()->where('aspect', '=', 'Intervention')->where('level', '=', 'general')->get())
                ),
                'intervention_assignment'		=> SELF::serializeVocabulary($ClinicalTrial->intervention_assignment_id, "interventionassigment"),
                'date_enrollment_start'			=> ($ClinicalTrial->enrollment_start_actual ? : $ClinicalTrial->enrollment_start_planned),
                'exclusion_criteria'			=> $ClinicalTrial->exclusion_criteria,
                'recruitment_status'			=> SELF::serializeVocabulary($ClinicalTrial->recruitment_status_id, "recruitmentstatus"),
                //
                'results_date_completed'            => $ClinicalTrial->results_date_completed,
                'results_date_posted'               => $ClinicalTrial->results_date_posted,
                'results_date_first_publication'    => $ClinicalTrial->results_date_first_publication,
                'results_url_link'                  => $ClinicalTrial->results_url_link,
                'results_baseline_char'             => $ClinicalTrial->results_baseline_char,
                'results_participant_flow'          => $ClinicalTrial->results_participant_flow,
                'results_adverse_events'            => $ClinicalTrial->results_adverse_events,
                'results_outcome_measure'           => $ClinicalTrial->results_outcome_measure,
                'results_url_protocol'              => $ClinicalTrial->results_url_protocol,
                'results_summary'                   => $ClinicalTrial->results_summary,
                'results_IPD_plan'                  => $ClinicalTrial->ipdSharingPlan()->first() ? $ClinicalTrial->ipdSharingPlan()->first()->label : '',
                'results_IPD_description'           => $ClinicalTrial->results_IPD_description,
            );
        }

        return $serialized;
    }

    private function serializeSupportSources($support_sources)
    {
        $data = array();
        $count = 0;
        foreach ($support_sources->toArray() as $institution) {
            $data[$count++] =[
                'institution' => SELF::serializeInstitution($institution['institution_id']),
            ];
        }
        return $data;
    }

    private function serializeSecondarySponsors($secondary_sponsors)
    {
        $serialize_secondary_sponsors = array();
        $count = 0;
        foreach($secondary_sponsors as $secondary_sponsor) {
            $serialize_secondary_sponsors[$count++] = array(
                'institution' => SELF::serializeInstitution($secondary_sponsor->institution->id)
            );
        }
        return $serialize_secondary_sponsors;
    }

    private function serializeRecruitmentCountry($recruitment_countries)
    {
        $serialized_recruitment_country = array();
        $count = 0;
        foreach($recruitment_countries as $recruitment_country) {
            $serialized_recruitment_country[$count] = [
                'description'	=> $recruitment_country->description,
                'label'			=> $recruitment_country->label
            ];
            foreach ($recruitment_country->translations()->get() as $translation) {
                $serialized_recruitment_country[$count]['translations'][] = [
                    'description'	=> $translation->description,
                    'language'		=> $translation->language,
                    'label'			=> $translation->label
                ];
            }
            $count++;
        }
        return $serialized_recruitment_country;
    }

    private function serializeInterventionCode($intervention_codes)
    {
        $i_code = array();
        $count = 0;
        foreach($intervention_codes as $intervention_code) {
            $i_code[$count++] = SELF::serializeVocabulary($intervention_code->interventioncode_id, "interventioncode");
        }
        return $i_code;
    }

    private function serializeInstitution($id)
    {
        $data = \Repository\Institution::where('id', '=', $id)->first();
        $institution = array(
            'city'		=> $data->city,
            'name'		=> $data->name,
            'creator'	=> array(),
            'country'	=> SELF::serializeVocabulary($data->country_id, "countrycode"),
            'i_type'	=> SELF::serializeVocabulary($data->i_type_id, "institutiontype"),
            'state'		=> $data->state,
            'address'	=> $data->address,
            'pk'		=> $id
        );
        return $institution;
    }

    private function serializeVocabulary($id, $type)
    {
        $data = NULL;
        switch ($type) {
            case 'countrycode':
                $data = \Vocabulary\CountryCode::where('id', '=', $id)->first();
                break;
            case 'institutiontype':
                $data = \Vocabulary\InstitutionType::where('id', '=', $id)->first();
                break;
            case 'timeperspective':
                $data = \Vocabulary\TimePerspective::where('id', '=', $id)->first();
                break;
            case 'studymasking':
                $data = \Vocabulary\StudyMasking::where('id', '=', $id)->first();
                break;
            case 'observationalstudydesign':
                $data = \Vocabulary\ObservationalStudyDesign::where('id', '=', $id)->first();
                break;
            case 'studytype':
                $data = \Vocabulary\StudyType::where('id', '=', $id)->first();
                break;
            case 'studyallocation':
                $data = \Vocabulary\StudyAllocation::where('id', '=', $id)->first();
                break;
            case 'studypurpose':
                $data = \Vocabulary\StudyPurpose::where('id', '=', $id)->first();
                break;
            case 'studyphase':
                $data = \Vocabulary\StudyPhase::where('id', '=', $id)->first();
                break;
            case 'interventionassigment':
                $data = \Vocabulary\InterventionAssignment::where('id', '=', $id)->first();
                break;
            case 'recruitmentstatus':
                $data = \Vocabulary\RecruitmentStatus::where('id', '=', $id)->first();
                break;
            case 'interventioncode':
                $data = \Vocabulary\InterventionCode::where('id', '=', $id)->first();
                break;
        }
        if (is_null($data)) return [];
        $vocabulary = array(
            'description'	=> $data->description,
            'label'			=> $data->label
        );
        foreach ($data->translations()->get() as $translation) {
            $vocabulary['translations'][] = [
                'description'	=> $translation->description,
                'language'		=> $translation->language,
                'label'			=> $translation->label
            ];
        }
        return $vocabulary;
    }

//    private function updateVocabulary()
//    {
//        $data = $this->voc_model->get_vocabularyContentType();
//
//        foreach($data as $row) {
//            $this->config->set_item($row->model, $row->id);
//        }
//    }

    private function serializeClinicalTrialContact($contacts)
    {
        $serialized_contact = array();
        $count = 0;
        foreach($contacts as $obj) {
            $contact = $obj->contact;
            $status = $obj->status;
            $serialized_contact[$count] = [
                'firstname' 	=> $contact->firstname,
                'creator'		=> array(),
                'middlename' 	=> $contact->middlename,
                'lastname'		=> $contact->lastname,
                'telephone'		=> $contact->telephone,
                'affiliation'	=> SELF::serializeInstitution($contact->affiliation_id),
                'address'		=> $contact->address,
                'city'			=> $contact->city,
                'zip'			=> $contact->zip,
                'country'		=> SELF::serializeVocabulary($contact->country_id, "countrycode"),
                'pk'			=> $contact->id,
                'email'			=> $contact->email,
            ];
            if ($status) $serialized_contact[$count]['status'] = $status->label;
            if ($obj->date_approval) $serialized_contact[$count]['approval_date'] = $obj->date_approval;
            $count++;
        }
        return $serialized_contact;
    }

    private function serializeOutcome($outcomes)
    {
        $serialized_outcome = array();
        $count = 0;
        foreach($outcomes as $outcome) {
            $serialized_outcome[$count] = array(
                'description'	=> $outcome->description,
                'interest'		=> $outcome->interest
            );
            foreach ($outcome->translations()->get() as $translation) {
                $serialized_outcome[$count]['translations'][] = [
                    'description'	=> $translation->description,
                    'language'		=> $translation->language,
                    'label'			=> $translation->label
                ];
            }
            $count++;
        }
        return $serialized_outcome;
    }

    private function serializeTrialNumber($trial_numbers)
    {
        $trialnumber = array();
        $count = 0;
        foreach($trial_numbers as $trial_number) {
            $trialnumber[$count++] = array(
                'id_number'			=> $trial_number->id_number,
                'issuing_authority'	=> $trial_number->issuing_authority
            );
        }
        return $trialnumber;
    }
    
    private function serializeDescriptor($descriptors)
    {
        $serialized_descriptors = array();
        $count = 0;
        foreach($descriptors as $descriptor) {
            $serialized_descriptors[$count] = array(
                'code'			=> $descriptor->code,
                'vocabulary'	=> $descriptor->vocabulary,
                'level'			=> $descriptor->level,
                'text'			=> $descriptor->text,
                'version'		=> $descriptor->version,
                'aspect'		=> $descriptor->aspect
            );
            foreach ($descriptor->translations()->get() as $translation) {
                $serialized_descriptors[$count]['translations'][] = [
                    'language'		=> $translation->language,
                    'text'			=> $translation->text,
                ];
            }
            $count++;
        }
        return $serialized_descriptors;
    }

    private function serializeClinicalTrialTranslations($translations)
    {
        $serialize_translations = array();
        $count = 0;
        foreach($translations as $translation) {
            $serialize_translations[$count++] = array(
                '__model__' => "ClinicalTrialTranslation",
                '__unicode__' => $translation->language,
                'scientific_title' => $translation->scientific_title,
                'study_design' => $translation->study_design,
                'language' => $translation->language,
                'public_title' => $translation->public_title,
                'i_freetext' => $translation->i_freetext,
                'acronym_expansion' => $translation->acronym_expansion,
                'scientific_acronym' => $translation->scientific_acronym,
                'acronym' => $translation->acronym,
                'scientific_acronym_expansion' => $translation->scientific_acronym_expansion,
                'inclusion_criteria' => $translation->inclusion_criteria,
                'hc_freetext' => $translation->hc_freetext,
                'pk' => $translation->id,
                'exclusion_criteria' => $translation->exclusion_criteria,
                'id' => $translation->id,
            );
        }
        return $serialize_translations;
    }
    /*************************************/

}