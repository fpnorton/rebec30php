<?php

use Chaos\Versioner;

defined('BASEPATH') OR exit('No direct script access allowed');

class Summary extends EnyoController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function view($clinical_trial_id)
    {
		$this->only_reviewer();

        $data_step = [];
        {
            $clinicaltrial = \Repository\ClinicalTrial::find($clinical_trial_id);

            $submissao = $clinicaltrial->submission();

            if ($submissao->isPending() == false) {
                $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
                $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
                redirect("/revisor");
                return;
            }

            $data_step['clinicaltrial_id'] = $clinical_trial_id;
            $data_step['currentStep'] = STEP_SUMARIO;

            $data_step['controller_method'] = 'pesquisador/submissao';

            {
//                $data['VD']['genData'] = $this->loadGenData($submissao->author()->first()->id, $clinicaltrial_id);
                $data_step['VD']['genData'] = $this->auxfunctions->loadGenData($submissao->creator_id, $clinical_trial_id);
                $data_step['VD']['subData'] = $submissao;
                $data_step['VD']['fields_status'] = ['en' => $submissao->field_status()]; // @json_decode($submissao->fields_status, TRUE);
            }
//            {
//                echo '<pre>' . var_export([
//                        '$data' => array_keys($data),
//                    ], true) . '</pre>';
//            }
        }
        $data_step['page'] = [
            'base_url_form-corrigir' => ($data_step['controller_method']) .'/corrigir/',
            'base_url_form-sumario'  => ($data_step['controller_method']) .'/sumario/',
        ];
        $data_step['loggedUser'] = $this->name;
        $data_step['usuario'] = $this->session->userdata('user');
        $data_step['postback'] = '_postback='.$this->get_postback();

        { // VERSIONER
            $data_step['version'] = NULL;
            $last_sequence_number = Versioner::where('trial_id', '=', $clinical_trial_id)->max('sequence_number');
            if ($last_sequence_number > 1) {
                $data_step['version']['left'] = Versioner::where('trial_id', '=', $clinical_trial_id)
                    ->where('sequence_number', '=', ($last_sequence_number - 1))
                    ->first()
                    ->data();
                $data_step['version']['right'] = Versioner::where('trial_id', '=', $clinical_trial_id)
                    ->where('sequence_number', '=', ($last_sequence_number))
                    ->first()
                    ->data();
            }
        }

        $data = [];
        {
            $data['content_left'] = $this->load->view('menu.php', [
            ], TRUE);
        }
        {
            $submission = \ReviewApp\Submission::where('_deleted', '=', 0)
                ->where('trial_id', '=', $clinical_trial_id)
                ->first();
            $data['content_right'] =
                $this->load->view('steps.php', [
                    'submission' => $submission,
                    'current_step' => PASSO_SUMARIO,
                    'fields_status' => $submissao->field_status(), // $data_step['VD']['fields_status']['en'],
                ], TRUE) .
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $this->load->view('summary.php', $data_step, TRUE).
                '</section>'.
                '</div>';
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

}