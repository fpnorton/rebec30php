<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attachments extends EnyoController {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function download($clinical_trial_id, $attachment_id)
    {
		$this->only_reviewer(FALSE);

        {
            $data = (new \Steps\Anexos())->GetStepData($clinical_trial_id);
            for ($i = 1; $i <= $data['count_attach']; $i++) { 
                if ($data["ctattachment_set-${i}-id"] != $attachment_id) continue;
                $info = $data["ctattachment_set-${i}-info"];
                header( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
                header( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
                header( "Cache-Control: no-cache, must-revalidate" );
                header( "Pragma: no-cache" );
                header( "Content-type: ".$info['type'] );
                header( "Content-Transfer-Encoding: Binary" );
                header( "Content-Disposition: attachment; filename=".$info['name'] );
                readfile($info['home'].$info['file']);
                // echo '<pre>'.var_export(
                //     [
                //         __FILE__ => __LINE__,
                //         '$info' => $info,
                //     ],true).'</pre>';
            }
        }
    }

    public function edit($clinical_trial_id)
    {
		$this->only_reviewer();

        $data_step = [
            'reviewer' => true
        ];
        {
            $clinicaltrial = \Repository\ClinicalTrial::find($clinical_trial_id);

            $submissao = $clinicaltrial->submission();

            if ($submissao->isPending() == false) {
                $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
                $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
                redirect("/revisor");
                return;
            }

            $data_step['clinicaltrial_id'] = $clinical_trial_id;
            $data_step['VD']['currentStep'] = 1;
            $data_step['controller_method'] = 'pesquisador/submissao';
            {
                $data_step['VD']['genData'] = $this->auxfunctions->loadGenData($submissao->creator_id, $clinical_trial_id);
                $data_step['VD']['subData'] = $submissao;
                $data_step['VD']['fields_status'] = ['en' => $submissao->field_status()]; // @json_decode($submissao->fields_status, TRUE);
            }
            {
                $data_step['passo_'.PASSO_ANEXOS] = (new \Steps\Anexos())->GetStepData($clinical_trial_id);
                    $context = 'attachments';
            }
            {
//                if ($formData['messages'] != NULL) {
//                    $dataKey = 'passo_'.$passo;
//                    $data['VD']['validationData']['messages'] = $formData['messages'];
//                    foreach ($this->input->post() as $formDataKey => $formDataValue) {
//                        if (in_array($formDataKey, array_keys($data[$dataKey]))) {
//                            $data[$dataKey][$formDataKey] = $formDataValue;
//                        }
//                    }
//                }
            }
        }
        {
            $data_step['selected_languages'] = $clinicaltrial->selectedLanguages(2);
            $data_step['controller_method'] = 'prototype/revisao';
            $data_step['page'] = [
                'base_url_form-corrigir' => 'prototype/revisao/corrigir/',
                'base_url_form-sumario'  => 'prototype/submissao/0/',
            ];
            $data_step['has_review_remark'] = TRUE;
            $data_step['can_save'] = TRUE;
            $data_step['common_field'] = ' readonly disabled ';
        }
        {
            $data['content_left'] = $this->load->view('menu.php', [
            ], TRUE);
        }
        {
            $data_step['languages'] = $clinicaltrial->selectedLanguages(2);
            $data_step['_form_action'] = base_url('revisor/submissao/passo/2/'.$clinicaltrial->id);
            $this->set_postback($data_step);
            $submissao = \ReviewApp\Submission::where('_deleted', '=', 0)
                ->where('trial_id', '=', $clinical_trial_id)
                ->first();
            $data['content_right'] =
                $this->load->view('steps.php', [
                    'submission' => $submissao,
                    'current_step' => PASSO_ANEXOS,
                    'fields_status' => $submissao->field_status(), // $data_step['VD']['fields_status']['en'],
                ], TRUE) .
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $this->load->view('sheets/attachments.php', $data_step, TRUE).
                '</section>'.
                '</div>';
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, [
            'usuario' => $this->session->userdata('user'),
        ]));
    }

    public function save($clinicaltrial_id = NULL)
    {
		$this->only_reviewer();

        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        $fieldlabel = new Helper\FieldLabelLanguage('AttachmentForm'); $fieldlabel->enableUpdate();
        {
            $submission = \ReviewApp\Submission::where('_deleted', '=', 0)
                ->where('trial_id', '=', $clinicaltrial_id)
                ->first();

            if ($this->update_review_remark('attachments', $submission)) {
                $this->session->set_flashdata('success', $fieldlabel->get_or_new("update_success"));
                redirect('/revisor/submissao/sumario/'.$clinicaltrial_id);
            } else {
                $this->session->set_flashdata('success', $fieldlabel->get_or_new("update_success"));
                redirect('/revisor/submissao/passo/2/'.$clinicaltrial_id);
            }
        }
    }

}