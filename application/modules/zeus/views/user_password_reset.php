<? defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<? $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<? $fieldlabel = new Helper\FieldLabelLanguage('UserForm'); $fieldlabel->enableUpdate(); ?>
<? $usuario = $this->session->flashdata('form') ?? new \Auth\User(); ?>
<div class="row">
    <!-- left column -->
    <div class="col-md-1"></div>
    <!-- center column -->
    <div class="col-md-10">
        <!-- general form elements -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php echo $fieldlabel->get_or_new('main_title'); ?></h3>
            </div>
            <hr />
            <!-- form start -->
            <form role="form"
                  action="<?php echo base_url().'reset/'.$token ?>"
                  method="post"
                  id="editUser"
                  autocomplete="off">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="username"><?php echo $fieldlabel->get_or_new('username'); ?></label>
                                <input type="text"
                                       class="form-control"
                                       id="username"
                                       placeholder="<?php echo $fieldlabel->get_or_new('username_placeholder'); ?>"
                                       name="username"
                                       value="<?php echo ($usuario->username ?? ''); ?>"
                                       maxlength="30"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password"><?php echo $fieldlabel->get_or_new('password'); ?></label>
                                <input type="password"
                                       class="form-control"
                                       id="password"
                                       placeholder="<?php echo $fieldlabel->get_or_new('password_placeholder'); ?>"
                                       name="password"
                                       autocomplete="new-password">
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cpassword"><?php echo $fieldlabel->get_or_new('password_confirm'); ?></label>
                                <input type="password"
                                       class="form-control"
                                       id="cpassword"
                                       placeholder="<?php echo $fieldlabel->get_or_new('password_confirm_placeholder'); ?>"
                                       name="cpassword"
                                       autocomplete="new-password">
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <input type="submit" class="btn btn-primary" value="<?php echo $genericlabel->get_or_new('button_save'); ?>" />
                    <input type="reset" class="btn btn-default" value="<?php echo $genericlabel->get_or_new('button_reset'); ?>" />
                </div>
            </form>
        </div>
    </div>
    <!-- right column -->
    <div class="col-md-1"></div>
</div>
<?php $genericlabel->disableUpdate(); ?>
<?php $fieldlabel->disableUpdate(); ?>
