<? defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<? $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate(); ?>
<? $fieldlabel = new Helper\FieldLabelLanguage('UserForm'); $fieldlabel->enableUpdate(); ?>
<div class="row">
    <!-- left column -->
    <div class="col-md-1"></div>
    <!-- center column -->
    <div class="col-md-10">
        <!-- general form elements -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php echo $fieldlabel->get_or_new('main_title'); ?></h3>
            </div>
            <hr />
            <!-- form start -->
            <form role="form"
                  action="<?php echo base_url(); ?>profile"
                  method="post"
                  id="editUser"
                  autocomplete="off">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="username"><?php echo $fieldlabel->get_or_new('username'); ?></label>
                                <input type="text"
                                       class="form-control"
                                       id="username"
                                       placeholder="<?php echo $fieldlabel->get_or_new('username_placeholder'); ?>"
                                       value="<?php echo ($usuario->username ?? ''); ?>"
                                       readonly="readonly"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email"><?php echo $fieldlabel->get_or_new('email'); ?></label>
                                <input type="email"
                                       class="form-control"
                                       id="email"
                                       placeholder="<?php echo $fieldlabel->get_or_new('email_placeholder'); ?>"
                                       name="email"
                                       value="<?php echo ($usuario->email ?? ''); ?>"
                                       maxlength="75">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email"><?php echo $fieldlabel->get_or_new('first_name'); ?></label>
                                <input type="text"
                                       class="form-control"
                                       id="first_name"
                                       placeholder="<?php echo $fieldlabel->get_or_new('first_name_placeholder'); ?>"
                                       name="first_name"
                                       value="<?php echo ($usuario->first_name ?? ''); ?>"
                                       maxlength="30">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email"><?php echo $fieldlabel->get_or_new('last_name'); ?></label>
                                <input type="text"
                                       class="form-control"
                                       id="last_name"
                                       placeholder="<?php echo $fieldlabel->get_or_new('last_name_placeholder'); ?>"
                                       name="last_name"
                                       value="<?php echo ($usuario->last_name ?? ''); ?>"
                                       maxlength="30">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password"><?php echo $fieldlabel->get_or_new('password'); ?></label>
                                <input type="password"
                                       class="form-control"
                                       id="password"
                                       placeholder="<?php echo $fieldlabel->get_or_new('password_placeholder'); ?>"
                                       name="password"
                                       autocomplete="new-password">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cpassword"><?php echo $fieldlabel->get_or_new('password_confirm'); ?></label>
                                <input type="password"
                                       class="form-control"
                                       id="cpassword"
                                       placeholder="<?php echo $fieldlabel->get_or_new('password_confirm_placeholder'); ?>"
                                       name="cpassword"
                                       autocomplete="new-password">
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <input type="submit" class="btn btn-primary" value="<?php echo $genericlabel->get_or_new('button_save'); ?>" />
                    <input type="reset" class="btn btn-default" value="<?php echo $genericlabel->get_or_new('button_reset'); ?>" />
                </div>
            </form>
        </div>
    </div>
    <!-- right column -->
    <div class="col-md-1"></div>
</div>
<?php $genericlabel->disableUpdate(); ?>
<?php $fieldlabel->disableUpdate(); ?>
