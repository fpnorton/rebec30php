<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Token extends ZeusController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    private function forgot_password_key($token) 
    {
        return 'FORGOT-PASSWORD-'.$token;
    }

    private function forgot_password_verify_pack(\Auth\User $User) 
    {
        $pack = json_encode([
            'username' => $User->username, 
            'email' => $User->email
        ]);
        $md5 = md5($pack);
        return $md5;
    }

    public function forgot_password()
    {
        $messages = $this->load_messages(new Helper\FieldLabelLanguage('General'), [
            'message_success_for_email_sended_to_reset_password',
            'message_error_user_not_found',
            'reset_password_mail_subject',
            'message_error_token_not_found'
        ]);

        $email = $this->input->post('email');

        $User = \Auth\User::find_by_username_or_email($email);
        
        if ($User == NULL or $User->email == NULL) {
            $this->session->set_flashdata('error', 
                $messages['message_error_user_not_found']
            );
            redirect('/');
            return;
        }

        $better_token = md5(uniqid(rand(), true));

        $this->REDIS_SERVER->setEx(
            $this->forgot_password_key($better_token),
            (60*60)*30, 
            $this->forgot_password_verify_pack($User)
        );

        $_message = NULL;

        {
            $_subject = $messages['reset_password_mail_subject'];
        }
        {
            $MailMessage = Vocabulary\MailMessage::where('label', '=', 'password-reset-with-token')->first();
            if ($MailMessage) $_message = $MailMessage->description;
            $language = linguagem_selecionada();
            if ($language != 'en') {
                $MailMessageTranslation = $MailMessage->translations()->where('language', '=', $language)->first();
                if ($MailMessageTranslation != NULL and !empty($MailMessageTranslation->description)) { 
                    $_message = $MailMessageTranslation->description;
                };
            }
        }
        {
            $dictionary = [
                '__TOKEN__' => $better_token,
                '__NAME__' => $User->first_name,
                '__FULL_NAME__' => implode(' ', [
                    $User->first_name,
                    $User->last_name,
                ]),
                '__USERNAME__' => $User->username,
                '__EMAIL__' => $User->email,
                '__SITE__' => base_url("/"),
                '__LINK__' => base_url().'reset/'.$better_token,
            ];
            $_subject = strtr($_subject, $dictionary);
            $_message = strtr($_message, $dictionary);;
        }

        if ($_message == NULL) { redirect('/'); return; }

        $success = $this->mailfunctions->send(
            (new \Helper\Email(NULL, 'ReBEC'))
                ->addTO($User->email)
                ->addSUBJECT($_subject)
                ->addBODY($_message)
        );

        $anonymous_email = NULL;
        {
            $email_exploded = explode("@", $User->email);
            $l_len = strlen($email_exploded[0]);
            $r_len = strlen($email_exploded[1]);
            $anonymous[0] = '';
            if ($l_len < 2) {
                $anonymous[0] = str_repeat("*", $l_len);
            } else {
                $anonymous[0] = substr_replace($email_exploded[0], str_repeat("*", $l_len - 2), 2, $l_len - 2);
            }
            if ($r_len < 4) {
                $anonymous[1] = $email_exploded[1];
            } elseif ($r_len < 6) {
                $anonymous[1] = substr_replace($email_exploded[1], str_repeat("*", $r_len - 4), 2, $r_len - 2).
                    substr($email_exploded[1], $r_len - 2)
                ;
            } else {
                $anonymous[1] = substr_replace($email_exploded[1], str_repeat("*", $r_len - 6), 3, $r_len - 3).
                    substr($email_exploded[1], $r_len - 3)
                ;
            }
            $anonymous_email = implode("@", $anonymous);
        }
        {
            $dictionary = [
                '__MAIL__' => $anonymous_email,
                '__EMAIL__' => $anonymous_email,
            ];
            $_warning = strtr(
                $messages['message_success_for_email_sended_to_reset_password'],
                $dictionary
            );
            $this->session->set_flashdata('success', 
                strtr($_warning, $dictionary)
            );
        }
        redirect('/');
        return;
    }

    public function reset_password($token) 
    {
        $messages = $this->load_messages(new Helper\FieldLabelLanguage('General'), [
            'message_error_token_not_found'
        ]);

        if ($this->REDIS_SERVER->exists($this->forgot_password_key($token)) == FALSE) {
            $this->session->set_flashdata('error', 
                $messages['message_error_token_not_found']
            );
            redirect('/');
            return;
        }
        {
            {
                $data['content_left'] = $this->load->view('menu.php', [], TRUE);
            }
            {
                $data['content_right'] =
                    '<div class="row">'.
                    '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                    $this->load->view('user_password_reset.php', ['token' => $token], TRUE).
                    '</section>'.
                    '</div>';
            }
    
            define('REBEC_REGISTER', true);
    
            $this->parser->parse('main', array_merge($data, []));
        }
    }

    public function change_password($token) 
    {
        $messages = $this->load_messages(new Helper\FieldLabelLanguage('General'), [
            'message_error_all_fields_required',
            'message_success_password_changed',
            'message_error_password_confirmation',
            'message_error_token_not_found'
        ]);

        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $confirm  = $this->input->post('cpassword');

        if (empty($username) or empty($password) or empty($confirm)) 
        {
            $this->session->set_flashdata('error', 
                $messages['message_error_all_fields_required']
            );
            redirect(base_url($_SERVER['REQUEST_URI']));
            return;
        }

        if ($this->REDIS_SERVER->exists($this->forgot_password_key($token)) == FALSE) {
            $this->session->set_flashdata('error', 
                $messages['message_error_token_not_found']
            );
            redirect('/');
            return;
        }

        $User = \Auth\User::find_by_username($username);

        $check_pack_1 = $User ? $this->forgot_password_verify_pack($User) : '';

        $check_pack_2 = $this->REDIS_SERVER->get($this->forgot_password_key($token));

        $fail = TRUE;

        if (!empty($check_pack_1) and ($check_pack_1 == $check_pack_2)) {
            
            if ($password == $confirm) {
                $salt = substr(bin2hex(random_bytes(3)), 0, 5);
                $hash = $this->encrypt->hash($salt . $this->input->post('password'));
                $User->password = join('$', ['sha1', $salt, $hash]);
                $User->is_active = TRUE;
                $User->save();
                
                $this->session->set_flashdata('success', 
                    $messages['message_success_password_changed']
                );
                
                $this->REDIS_SERVER->del($this->forgot_password_key($token));

                $fail = FALSE;

                // envia email
                {
                    $to = NULL;
                    $bcc = NULL;
                    $_subject = "Nota: [".$User->username . "] - Ativação e Troca de senha executadas ";
                    $_message = "<pre>\nDados informados: \n\n".
                        "\tId: ". $User->id ."\n".
                        "\tLogin: ". $User->username ."\n".
                        "\tNome: " . $User->first_name . " " . $User->last_name . "\n".
                        "\tEmail: ". $User->email ."\n".
                        "\tIP: (". $_SERVER['HTTP_X_REAL_IP'] .")\n</pre>".
                    "";
                    $this->mailfunctions->send(
                        (new \Helper\Email(NULL, 'ReBEC'))
                            ->addTO($to)
                            ->addBCC($bcc)
                            ->addSUBJECT($_subject)
                            ->addBODY($_message)
                    );
                }

            } else {
                $this->session->set_flashdata('error', 
                    $messages['message_error_password_confirmation']
                );
            }
    
        } else {
            $this->session->set_flashdata('error', 
                $messages['message_error_password_not_changed']
            );
        }
        if ($fail) {
            redirect(base_url($_SERVER['REQUEST_URI']));
        } else {
            redirect('/');
        }
    }
}