<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Information extends ZeusController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    private function get_private_key() {
        return implode("\n", [
            "-----BEGIN RSA PRIVATE KEY-----",
            $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_KEY_PRIVATE),
            "-----END RSA PRIVATE KEY-----",
        ]);
    }

    private function get_public_key() {
        return implode("\n", [
            "-----BEGIN PUBLIC KEY-----",
            $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_KEY_PUBLIC),
            "-----END PUBLIC KEY-----",
        ]);
    }

    private function get_token($key) {
        $tokens = json_decode(
            $this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_KEY_TOKENS),
            TRUE
        ) ?? [];        
        return array_key_exists($key, $tokens) ? $tokens[$key] : NULL;
    }

    private function get_ip() {
        if (isset($_SERVER['HTTP_X_REAL_IP'])) return $_SERVER['HTTP_X_REAL_IP'];
        return $_SERVER['REMOTE_ADDR'];
    }

    private function get_authorization_header() {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;        
    }

    private function get_bearer_token() {
        $headers = $this->get_authorization_header();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;        
    }

    public function php() 
    {
        $this->only_super();

        phpinfo();
    }

    /**
     * @see https://www.devglan.com/online-tools/rsa-encryption-decryption
     * pvt MIIJQgIBADANBgkqhkiG9w0BAQEFAASCCSwwggkoAgEAAoICAQCbjNYxaXGRr5pyllydYivTOvB9yXvRijXpWVPs6+ci1CVHqOfnHviZ9MI180jD1Qm85DyXNHUQcEIao0Tr/ja2CpJVWEbze0MVNc0fQi8LlvwFLVb2LK8ZxtPjb93++kB37qBQSiGqbpxQ30tlDVUX2E1oV1TCKd98xCD7ntbANIz1zthSx+3FKq0kRKYZHqu92OYVoUJBota+yca516D35J+eSEJ31taJbVknUIRAiij1yIhGIuLv5zPR2UYIkbdQXWupbET8+ujK0SpurVk/QYQNbeerjm7ThkA3s1aJebnA8VjnbvDmJ6EMSi/Q04vpv7nI6otAom6AHmb/MzK0b76FW+tdi6tgXCDSbPHzCKiQSrtwh8eBzuVALBajh0tZ3uIcZX66kWUi8TAMxB5siRVx3NWZ1K7GNY8H6MkQ8Jfmkwq9W+3y2T7sUIX6LVBwR4RJXCPZR3S7gFEE94gr9QOAqT4F5W6ORCLca7fYh+Re4p/RQb5uHfwvU/f1caH79WOAwtHLOaR2nJkvl9fbdTvhgxNKjDH0xl+c03JYSiIipFggTscZE7+HWeX1H6EehP0Htd25gFhsuicaipRUNwWaPmB5ECU3YjN7ihzgetAAwunmSxHRMCdOjyr+IkvuxoIvaIcQyXDwMwU5qg9VTubnHLbAA+nTrb4kK/rvHwIDAQABAoICADbO5Rn8AVxH96+wc2mxEagX+G0Z9VrbzCswonW0mXiYL5qUVUIGk6oLp/LCij0Wb5vy/7Z3cVgdde1U4hAVPsnd57nSpQ1dvUFX97Kv60u2I47fbujGzdvTRw24eQxCuTyJBEvGEx6uYr3MMBCnJfiE6qF9RAw2HN0lbkm6qgiHe7OQ8oAYmhsC8ob4+V2YhQMbzdaJP4NJEUmgZzlMVReDowpug50eQfGiaeuEX1UuWUhhJEMyP6wwVv32BMs4LNNQV4MW9yf8cNIl6UGeKvgy6Yih1SOJyFsXzsDRnAhMr7rgT2ccnB08kW53JWn7YC04tQmqOu7HTRAvpjL9ozuL8HUu0AH8f2jFbuOwdKNiMEiASRsamCdVLl85XiAdw8ebHH3rzZx7dGh1oEPEC/Yc+fKwqkxyIPIuA2oZJt+6+MFqnoz6iAuWS8XwQK2rKKAruzQgg7/xQ+lqoTsQWvsQjIv8t/mcIbMOg1vgA2ICNcLJh5tG0gb4y+WGZDaXbHu5XFA3lfVLUi+B8P+uv+ZbS2q1AzFUFcFyqkUb77BKom2f+XK3n9u11KZfYZ2AyWynmb9s1mDqv3ceV2nYBFbHd5sQAMJSR0ardxX4aVq2+8ggohY6ASSMaNSGAjO0BAVo7uPNuvB0PU4GeOAuF72ucCTVRirbILava2/cDDQBAoIBAQDQlsjVxAAU3FA3LOab5KICkNMxdMg/dvJ4+F+tGwAk5ZRE/rSMry7taR9WtdwvrtAfGKnj31SsPqpHyKce4pmKCL2n4q5g3bjYlKLjN0DKCRnMPlUN3SD3oRvmUrgZ9xU121nnG2A6lSgZ2+vzYWsRGr3l3UDOrVObDJ26Lldj0VDSWcpN6BlY0CIImVSMhQlvPqgp6qMFjwUMfIsUZ9sWKKYpxzoiI6kr8EKa27uuUGukbhYAEWex0LtSU9XY+UL2ylmWvX13uPVFEaYayT5EXoYm6cRuBY9owd4dDt8LNfqs5RpiUOrtvMDPac3ZG3F4x7m6uh7JywkFR+8auzafAoIBAQC+59473R+v0fnJEzdA7eDNHSnHzC9b1Z2wPGHDQiEaErMfL7X6eSjUWrCzhhpcHyV415R1sRSj90RtP03Nke+vuNbYFrltrqU+4y2UFG8zlUIfntJuBza58b90+xa0FQUO+yLMM7GjoPUsYPzCqr+HG+BoW3RlnbMdC4Y5JFXG1H3XQMVbhhZv490oSAe/AvGmXPD5OIxOdlp1VW+ZS4orkDW5Drf0pbTA6mewc2Y/h0VKeG2UYwbO1hK4rmqGnKH+OHmkRLmqf2eN7LzUTmkjQkMV7EdVXHKVtxx4a760jj50eMzefi7p3+IwIq+4CDPD4EK9s7npmH8utoO9b/eBAoIBAFvPYoqkgJyt9Ci0/LP1SFH0yOBl1JYKluMj/4jD76ZN3psXuQSLPw6ETKTv2wCp4zUst+pyrGQ8GNsLuHNhrc8R1diFb9d3ABtkIOX9tei3ZMF/VhSKFi6onQ0dr6bOGGniKIoOhKaEbGglYo93e5bFvHvmjNXreHcwcGQ4bYKj6P0NRHxSAoYS5kf+pTZMgECUxZPdkO+rab97OgQ7SjVQowKUjEPmilmqoMZknRE2mIqBYfGZt5iOlUTBXcjovc6+dfxhX05I/Lvv7rqvhVa2fDm4fxsmG18ypb8Igqf+lF1xYdGQaDdH813X5iD/cUyGLHvB3DdZxJisdtO/P2UCggEBAJF9cFa7LNz375+Yb3GiWMK5SRAlwqoAYM7hFbiTe1q9iZlo6Q1crQs5mpUWzJwMeKhh05jmPc7AMXv41LioUbzf54yDtQmsIe+NfzztXdo4FE644bJM+R446Sqj+SeBJXvHVTu+4dDMaE1MFRXev1yemCWwHUMcLlciY0bFNEH1y3nyuPYJeKwJQpDiZFTkT3n0NwoYWIWrmUD2vbhcY47Ttr20Q+nFJ74mZqOJLxGXa+1XXlvKJHQZneb1teWlFagDLmFMpxsTQb5d/4LlxybB5xKcIbkiXYlQJfh4VLyR83McZTXjgKlooJ8CsFxWk+zhyrt8b2FhgPv+yOPkowECggEAc1fvznFGhcaTCxxFq9C0/nlhAuPtWKHZHa3Y3vn3XAl6vp9phcXaJpdqrGLA27kfscdsUmr0S9ULHe19u7IRwTOME7VP0+NEvuB596wonNNSyO/S/I/tmP0F4SPiMzZ2M314zc3DFbZOVsntC4R+hzXrqmilCg1pz4R8hb6mSDij4jt0Br45jdD4JsZFJm3/zgRXMBjWN95XL1PfmzwnFOfK6w1POdQ6JqusvoW8T8TsCwYgb5AHmDIBFTSY/xFdiQjBobTdztGL+6z9l5Qf7B3UXAlFF2kyBRY3N8aQKuiPw1ojtL5gFogTMq04OAZG95x0Z34ZJwzAhwPaHXKgDA==
     * pub MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAm4zWMWlxka+acpZcnWIr0zrwfcl70Yo16VlT7OvnItQlR6jn5x74mfTCNfNIw9UJvOQ8lzR1EHBCGqNE6/42tgqSVVhG83tDFTXNH0IvC5b8BS1W9iyvGcbT42/d/vpAd+6gUEohqm6cUN9LZQ1VF9hNaFdUwinffMQg+57WwDSM9c7YUsftxSqtJESmGR6rvdjmFaFCQaLWvsnGudeg9+SfnkhCd9bWiW1ZJ1CEQIoo9ciIRiLi7+cz0dlGCJG3UF1rqWxE/ProytEqbq1ZP0GEDW3nq45u04ZAN7NWiXm5wPFY527w5iehDEov0NOL6b+5yOqLQKJugB5m/zMytG++hVvrXYurYFwg0mzx8wiokEq7cIfHgc7lQCwWo4dLWd7iHGV+upFlIvEwDMQebIkVcdzVmdSuxjWPB+jJEPCX5pMKvVvt8tk+7FCF+i1QcEeESVwj2Ud0u4BRBPeIK/UDgKk+BeVujkQi3Gu32IfkXuKf0UG+bh38L1P39XGh+/VjgMLRyzmkdpyZL5fX23U74YMTSowx9MZfnNNyWEoiIqRYIE7HGRO/h1nl9R+hHoT9B7XduYBYbLonGoqUVDcFmj5geRAlN2Ize4oc4HrQAMLp5ksR0TAnTo8q/iJL7saCL2iHEMlw8DMFOaoPVU7m5xy2wAPp062+JCv67x8CAwEAAQ==
     */
    public function researcher_test() 
    {
        $this->only_super();

        $privateKey = implode("\n", [
            "-----BEGIN RSA PRIVATE KEY-----",
            "MIIJQgIBADANBgkqhkiG9w0BAQEFAASCCSwwggkoAgEAAoICAQCbjNYxaXGRr5pyllydYivTOvB9yXvRijXpWVPs6+ci1CVHqOfnHviZ9MI180jD1Qm85DyXNHUQcEIao0Tr/ja2CpJVWEbze0MVNc0fQi8LlvwFLVb2LK8ZxtPjb93++kB37qBQSiGqbpxQ30tlDVUX2E1oV1TCKd98xCD7ntbANIz1zthSx+3FKq0kRKYZHqu92OYVoUJBota+yca516D35J+eSEJ31taJbVknUIRAiij1yIhGIuLv5zPR2UYIkbdQXWupbET8+ujK0SpurVk/QYQNbeerjm7ThkA3s1aJebnA8VjnbvDmJ6EMSi/Q04vpv7nI6otAom6AHmb/MzK0b76FW+tdi6tgXCDSbPHzCKiQSrtwh8eBzuVALBajh0tZ3uIcZX66kWUi8TAMxB5siRVx3NWZ1K7GNY8H6MkQ8Jfmkwq9W+3y2T7sUIX6LVBwR4RJXCPZR3S7gFEE94gr9QOAqT4F5W6ORCLca7fYh+Re4p/RQb5uHfwvU/f1caH79WOAwtHLOaR2nJkvl9fbdTvhgxNKjDH0xl+c03JYSiIipFggTscZE7+HWeX1H6EehP0Htd25gFhsuicaipRUNwWaPmB5ECU3YjN7ihzgetAAwunmSxHRMCdOjyr+IkvuxoIvaIcQyXDwMwU5qg9VTubnHLbAA+nTrb4kK/rvHwIDAQABAoICADbO5Rn8AVxH96+wc2mxEagX+G0Z9VrbzCswonW0mXiYL5qUVUIGk6oLp/LCij0Wb5vy/7Z3cVgdde1U4hAVPsnd57nSpQ1dvUFX97Kv60u2I47fbujGzdvTRw24eQxCuTyJBEvGEx6uYr3MMBCnJfiE6qF9RAw2HN0lbkm6qgiHe7OQ8oAYmhsC8ob4+V2YhQMbzdaJP4NJEUmgZzlMVReDowpug50eQfGiaeuEX1UuWUhhJEMyP6wwVv32BMs4LNNQV4MW9yf8cNIl6UGeKvgy6Yih1SOJyFsXzsDRnAhMr7rgT2ccnB08kW53JWn7YC04tQmqOu7HTRAvpjL9ozuL8HUu0AH8f2jFbuOwdKNiMEiASRsamCdVLl85XiAdw8ebHH3rzZx7dGh1oEPEC/Yc+fKwqkxyIPIuA2oZJt+6+MFqnoz6iAuWS8XwQK2rKKAruzQgg7/xQ+lqoTsQWvsQjIv8t/mcIbMOg1vgA2ICNcLJh5tG0gb4y+WGZDaXbHu5XFA3lfVLUi+B8P+uv+ZbS2q1AzFUFcFyqkUb77BKom2f+XK3n9u11KZfYZ2AyWynmb9s1mDqv3ceV2nYBFbHd5sQAMJSR0ardxX4aVq2+8ggohY6ASSMaNSGAjO0BAVo7uPNuvB0PU4GeOAuF72ucCTVRirbILava2/cDDQBAoIBAQDQlsjVxAAU3FA3LOab5KICkNMxdMg/dvJ4+F+tGwAk5ZRE/rSMry7taR9WtdwvrtAfGKnj31SsPqpHyKce4pmKCL2n4q5g3bjYlKLjN0DKCRnMPlUN3SD3oRvmUrgZ9xU121nnG2A6lSgZ2+vzYWsRGr3l3UDOrVObDJ26Lldj0VDSWcpN6BlY0CIImVSMhQlvPqgp6qMFjwUMfIsUZ9sWKKYpxzoiI6kr8EKa27uuUGukbhYAEWex0LtSU9XY+UL2ylmWvX13uPVFEaYayT5EXoYm6cRuBY9owd4dDt8LNfqs5RpiUOrtvMDPac3ZG3F4x7m6uh7JywkFR+8auzafAoIBAQC+59473R+v0fnJEzdA7eDNHSnHzC9b1Z2wPGHDQiEaErMfL7X6eSjUWrCzhhpcHyV415R1sRSj90RtP03Nke+vuNbYFrltrqU+4y2UFG8zlUIfntJuBza58b90+xa0FQUO+yLMM7GjoPUsYPzCqr+HG+BoW3RlnbMdC4Y5JFXG1H3XQMVbhhZv490oSAe/AvGmXPD5OIxOdlp1VW+ZS4orkDW5Drf0pbTA6mewc2Y/h0VKeG2UYwbO1hK4rmqGnKH+OHmkRLmqf2eN7LzUTmkjQkMV7EdVXHKVtxx4a760jj50eMzefi7p3+IwIq+4CDPD4EK9s7npmH8utoO9b/eBAoIBAFvPYoqkgJyt9Ci0/LP1SFH0yOBl1JYKluMj/4jD76ZN3psXuQSLPw6ETKTv2wCp4zUst+pyrGQ8GNsLuHNhrc8R1diFb9d3ABtkIOX9tei3ZMF/VhSKFi6onQ0dr6bOGGniKIoOhKaEbGglYo93e5bFvHvmjNXreHcwcGQ4bYKj6P0NRHxSAoYS5kf+pTZMgECUxZPdkO+rab97OgQ7SjVQowKUjEPmilmqoMZknRE2mIqBYfGZt5iOlUTBXcjovc6+dfxhX05I/Lvv7rqvhVa2fDm4fxsmG18ypb8Igqf+lF1xYdGQaDdH813X5iD/cUyGLHvB3DdZxJisdtO/P2UCggEBAJF9cFa7LNz375+Yb3GiWMK5SRAlwqoAYM7hFbiTe1q9iZlo6Q1crQs5mpUWzJwMeKhh05jmPc7AMXv41LioUbzf54yDtQmsIe+NfzztXdo4FE644bJM+R446Sqj+SeBJXvHVTu+4dDMaE1MFRXev1yemCWwHUMcLlciY0bFNEH1y3nyuPYJeKwJQpDiZFTkT3n0NwoYWIWrmUD2vbhcY47Ttr20Q+nFJ74mZqOJLxGXa+1XXlvKJHQZneb1teWlFagDLmFMpxsTQb5d/4LlxybB5xKcIbkiXYlQJfh4VLyR83McZTXjgKlooJ8CsFxWk+zhyrt8b2FhgPv+yOPkowECggEAc1fvznFGhcaTCxxFq9C0/nlhAuPtWKHZHa3Y3vn3XAl6vp9phcXaJpdqrGLA27kfscdsUmr0S9ULHe19u7IRwTOME7VP0+NEvuB596wonNNSyO/S/I/tmP0F4SPiMzZ2M314zc3DFbZOVsntC4R+hzXrqmilCg1pz4R8hb6mSDij4jt0Br45jdD4JsZFJm3/zgRXMBjWN95XL1PfmzwnFOfK6w1POdQ6JqusvoW8T8TsCwYgb5AHmDIBFTSY/xFdiQjBobTdztGL+6z9l5Qf7B3UXAlFF2kyBRY3N8aQKuiPw1ojtL5gFogTMq04OAZG95x0Z34ZJwzAhwPaHXKgDA==",
            "-----END RSA PRIVATE KEY-----",
        ]);
        $payload = array(
            "iss" => "researcher_test iss",
            "aud" => "researcher_test aud",
            "iat" => 1356999524,
            "nbf" => 1357000000
        );
        $jwt = JWT::encode($payload, $privateKey, 'RS256');

        echo 'ip address: ' . $this->get_ip();
        echo '<script type="text/javascript" src="'.base_url('assets/vendors/jquery/jquery-3.4.1.min.js').'"></script>';
        echo form_open('information/researcher', ['id' => 'post','autocomplete' => 'off',]);
        echo form_fieldset("email", ['class' => 'form-group']);
        echo form_input('email', '', ['id' => 'email', 'class' => 'form-input']);
        echo form_fieldset_close();
        echo form_submit('submit', "Enviar", ['class' => 'pull-right']);
        echo form_close();
        echo '<script>
        $(window).ready(function () {
            $("form").submit(function ( e ) {
                $.ajax({
                    data: "email="+$("input#email").val(),
                    type: "POST",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader ("Authorization", "Bearer '.$jwt.'");
                    },
                    success: function ( data ) {
                        console.log( data );
                        if (data.found) {
                            alert( "Nome: " + data.completo + "\n" +
                                   "Estudos publicados: " + data.publics.length );
                        } else {
                            alert( "Não encontrado!");
                        }
                    }
                });
                e.preventDefault();
            });
        });
        </script>';

    }

    public function researcher() 
    {
        $remote_address = $this->get_ip();

        if ($this->get_token($remote_address) == NULL) { //<pre></pre>
            echo "No token for $remote_address";
            return;
        }
        $publicKey = implode("\n", [
            "-----BEGIN PUBLIC KEY-----",
            $this->get_token($remote_address),
            "-----END PUBLIC KEY-----",
        ]);

        $jwt = $this->get_bearer_token();

        $decoded = NULL;
        try {
            $decoded = JWT::decode($jwt, $publicKey, array('RS256'));
        } catch (\Throwable $th) {
            return;
        }

        $email = $this->input->post('email');

        $User = \Auth\User::find_by_email($email);

        // $fossil = \Fossil\Fossil::where('is_most_recent', '=', 1)->where('display_text', 'LIKE', $rbr.' %')->get()->first();
        $submissions = \ReviewApp\Submission::where('_deleted', '=', 0)
            ->where('creator_id', '=', $User->id)
            ->get();
        // $ClinicalTrial = \Repository\ClinicalTrial::where('_deleted', '=', 0)
        //     ->where('id', '=', $clinicaltrial_id)
        //     ->first();

        $payload = [
            'found' => $User->id > 0 ? TRUE : FALSE,
            // 'id' => $User->id,
            'nome' => $User->first_name ?? '',
            'sobrenome' => $User->last_name ?? '',
            'completo' => ($User->first_name . ($User->last_name == NULL ? '' : ' '.$User->last_name)) ?? '',
            'email' => $User->email ?? '',
            'publics' => [
            ],
            // '$submission' => [],
        ];
        
        foreach($submissions as $submission) {
            if ($submission->trial->trial_id !== NULL) {
                $payload['publics'][] = [
                    'rbr' => $submission->trial->trial_id,
                    'title' => $submission->trial->public_title,
                ];
            }
        }

        // $researcher = [
        //     var_export($payload, TRUE),           
        // ];
        // echo '<pre>'.implode('</pre><pre>', $researcher).'</pre>';
        header('Content-Type: application/json');
        echo json_encode($payload);
    }

}