<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signout extends ZeusController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function logout()
    {
        $genericlabel = new Helper\FieldLabelLanguage('General');$genericlabel->enableUpdate();
        $this->redisfunctions->log("logout");
        $this->session->unset_userdata([
            'userId',
            'role',
            'name',
            'isLoggedIn',
            'superuser',
            'user',
        ]);
        $this->session->set_flashdata('success', $genericlabel->get_or_new('sign_out_success'));
        redirect ( 'welcome' );
    }

}