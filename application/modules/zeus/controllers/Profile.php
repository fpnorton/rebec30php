<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends ZeusController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function edit()
    {
        if ($this->session->userdata('user') == NULL) {
            redirect("welcome");
            return;
        }
        $User = \Auth\User::where('id', '=', $this->session->userdata('user')->id)->first();
        {
            $data['content_left'] = $this->load->view('menu.php', [], TRUE);
        }
        {
            $data['content_right'] =
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $this->load->view('profile_edit.php', ['usuario' => $User], TRUE).
                '</section>'.
                '</div>';
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, []));
    }

    public function save()
    {
        if ($this->session->userdata('user') == NULL) {
            redirect("welcome");
            return;
        }
        $genericlabel = new Helper\FieldLabelLanguage('General');$genericlabel->enableUpdate();
        $this->load->library('encryption');
        while (true)
        {
            $usuario = NULL;
            $password = NULL;
            {
                if ($this->input->post('password') && $this->input->post('cpassword') &&
                    ($this->input->post('password') != $this->input->post('cpassword')) ) {
                    $password = NULL;
                    break;
                }
                error_log(json_encode([
                    __FILE__ => __LINE__,
                    'Profile.save()' => true,
                    'username' => $this->session->get_userdata()['user']->username,
                    'password_length' => strlen($this->input->post('password')),
                ]));
                if ($this->input->post('password') && $this->input->post('cpassword')) {
                    $salt = substr(bin2hex(random_bytes(3)), 0, 5);
                    $hash = $this->encrypt->hash($salt . $this->input->post('password'));
                    $password = join('$', ['sha1', $salt, $hash]);
                }
            }
            $usuario = \Auth\User::where('id', '=', $this->session->userdata('user')->id)->first();
            break;
        }
        if ($usuario) {
            if (filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL) == FALSE) {
                $this->session->set_flashdata('error', $genericlabel->get_or_new('user_profile_invalid_email'));
            } else {
                $usuario->first_name = $this->input->post('first_name');
                $usuario->last_name = $this->input->post('last_name');
                $usuario->email = $this->input->post('email');
                if ($password) $usuario->password = $password;
                $usuario->update();
                $this->session->set_flashdata('success', $genericlabel->get_or_new('user_profile_updated'));
            }
        } elseif ($password) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new('user_profile_update_failed'));
        } else {
            $this->session->set_flashdata('error', $genericlabel->get_or_new('user_profile_password_check_failed'));
        }
        redirect('/profile');
    }

}