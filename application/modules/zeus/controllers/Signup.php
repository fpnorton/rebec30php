<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends ZeusController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function form()
    {
        $messages = $this->load_messages(new Helper\FieldLabelLanguage('General'), [
            'message_signup_disabled',
        ]);
        {
            $data['content_left'] = $this->load->view('menu.php', [], TRUE);
        }
        if ($this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_USER_REGISTER_ENABLE) == "ON") 
        {
            $data['content_right'] =
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $this->load->view('user_create.php', [], TRUE).
                '</section>'.
                '</div>';
        } else {
            $data['content_right'] =
                '<div class="row">'.
                '<section class="container menu_dashboard" style="padding-top: 5px; margin-top: 5px;">'.
                $messages['message_signup_disabled'].
                '</section>'.
                '</div>';
        }

        define('REBEC_REGISTER', true);

        $this->parser->parse('main', array_merge($data, []));
    }

    private function is_invalid_username($string) 
    {
        $invalid = true;
        {
            $allowed = array(".", "-", "_"); // you can add here more value, you want to allow.
            if(ctype_alnum(str_replace($allowed, '', $string ))) $invalid = false;
        }
        if ($invalid) return true;
        {
            if (strlen($string) > 15)  $invalid = true;
            if (strlen($string) <  5)  $invalid = true;
        }
        if ($string != strtolower($string)) $invalid = true;
        return $invalid;
    }

    private function is_invalid_email($string) 
    {
        $invalid = (filter_var($string, FILTER_VALIDATE_EMAIL) == FALSE);
        if ($invalid == false) {
            $email_parts = explode("@", $string);
            $invalid = (checkdnsrr($email_parts[1], "MX") === FALSE);
        }
        return $invalid;
    }

    public function register()
    {
        if ($this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_USER_REGISTER_ENABLE) != "ON") return;

        $messages = $this->load_messages(new Helper\FieldLabelLanguage('General'), [
            'message_error_password_confirmation_not_same',
            'message_success_activation_email_sent',
            'message_success_user_created',
            'message_error_login_exists',
            'message_error_login_invalid',
            'message_error_email_invalid',
            'message_error_password_void'
        ]);

        $User = new \Auth\User([
            'username' => $this->input->post('username'),
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => trim($this->input->post('email')),
            'date_joined' => date('Y-m-d H:i:s'),
        ]);
        if ($this->is_invalid_email($User->email)) {
            $this->session->set_flashdata('error', 
                $messages['message_error_email_invalid']
            );
            $User->email = '';
            $this->session->set_flashdata('form', $User);
            redirect('/signup');
            return;
        }
        if ($this->is_invalid_username($User->username)) {
            $this->session->set_flashdata('error', 
                $messages['message_error_login_invalid']
            );
            $User->username = '';
            $this->session->set_flashdata('form', $User);
            redirect('/signup');
            return;
        }

        $User->username = strtolower($User->username);
        $User->email = strtolower($User->email);

        $found_user = \Auth\User::whereRaw('lower(username) like (?)', [$User->username])->get()->count();
        $found_email = \Auth\User::whereRaw('lower(email) like  (?)', [$User->email])->get()->count();

        if (($found_user + $found_email) > 0) {
            $this->session->set_flashdata('error', 
                $messages['message_error_login_exists']
            );
            if ($found_user > 0) $User->username = '';
            if ($found_email > 0) $User->email = '';
            $this->session->set_flashdata('form', $User);
            redirect('/signup');
            return;
        }
        if ($this->input->post('password') && ($this->input->post('password') != $this->input->post('cpassword'))) {
            $this->session->set_flashdata('error', 
                $messages['message_error_password_confirmation_not_same']
            );
            $User->username = '';
            $this->session->set_flashdata('form', $User);
            redirect('/signup');
            return;
        }
        if (empty($this->input->post('password'))) {
            $this->session->set_flashdata('error', 
                $messages['message_error_password_void']
            );
            $this->session->set_flashdata('form', $User);
            redirect('/signup');
            return;
        }

        $this->load->library('encryption');

        $User->is_active = FALSE;
        $User->is_staff = FALSE;
        $User->is_superuser = FALSE;
        $User->last_login = date('Y-m-d H:i:s');

        {
            if ($this->input->post('password') && $this->input->post('cpassword')) {
                $salt = substr(bin2hex(random_bytes(3)), 0, 5);
                $hash = $this->encrypt->hash($salt . $this->input->post('password'));
                $User->password = join('$', ['sha1', $salt, $hash]);
            }
        }
        {
            $to = NULL;
            $bcc = NULL;
            $_subject = "Nota: [".$User->username . "] - Novo registrante ";
            $_message = "<pre>\nDados informados: \n\n".
                "\tHabilita por token: ". (($this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_USER_REGISTER_SENDS_EMAIL) == "ON") ? 'Sim' : 'Não')."\n".
                "\tLogin: ". $User->username ."\n".
                "\tNome: " . $User->first_name . " " . $User->last_name . "\n".
                "\tEmail: ". $User->email ."\n".
                "\tIP: (". $_SERVER['HTTP_X_REAL_IP'] .")\n</pre>".
            "";
            $this->mailfunctions->send(
                (new \Helper\Email(NULL, 'ReBEC'))
                    ->addTO($to)
                    ->addBCC($bcc)
                    ->addSUBJECT($_subject)
                    ->addBODY($_message)
            );
        }
        if ($this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_USER_REGISTER_SENDS_EMAIL) == "ON") {
            $User->save();
            $this->send_email($User);
            $this->session->set_flashdata('success', 
                $messages['message_success_activation_email_sent']
            );
            $this->session->set_userdata([]);
            redirect('/');
        } else {
            $User->is_active = TRUE;
            $User->save();
            $this->session->set_flashdata('success', 
                $messages['message_success_user_created']
            );
            {
                $name = trim($User->first_name.' '.$User->last_name);
                if (strlen($name) == 0) $name = trim($User->email);
                if (strlen($name) == 0) $name = trim($User->username);
    
                $sessionArray = array(
                    'userId' => $User->id,
                    'role' => $User->is_staff,
                    'name' => $name,
                    'isLoggedIn' => TRUE,
                    'superuser' => $User->is_superuser,
                    'user' => $User,
                );
                $this->session->set_userdata($sessionArray);
            }
            redirect('/pesquisador');
        }
    }

    public function activate($token)
    {
        $token = $this->user_register_key($token);
        if ($this->REDIS_SERVER->exists($token) == FALSE) {
            $this->session->set_flashdata('error', 
                $messages['message_error_token_not_found']
            );
            redirect('/');
            return;
        }
        // pegar o token
        $user_pack = $this->REDIS_SERVER->get($token);
        if ($user_pack == false) {
            $this->session->set_flashdata('error', 
                $messages['message_error_token_not_found']
            );
            redirect('/');
            return;
        }
        $un_pack = json_decode($user_pack, TRUE);
        $User = \Auth\User::where('id', '=', $un_pack['id'])->first();
        {
            $User->is_active = TRUE;
            $User->last_login = date('Y-m-d H:i:s');
            $User->save();
        }
        // envia email
        {
            $to = NULL;
            $bcc = NULL;
            $_subject = "Nota: [".$User->username . "] - Ativando registrante ";
            $_message = "<pre>\nDados informados: \n\n".
                "\tHabilita por token: ". (($this->REDIS_SERVER->get(\Helper\RedisKeys::KEY_CONFIG_USER_REGISTER_SENDS_EMAIL) == "ON") ? 'Sim' : 'Não')."\n".
                "\tLogin: ". $User->username ."\n".
                "\tNome: " . $User->first_name . " " . $User->last_name . "\n".
                "\tEmail: ". $User->email ."\n".
                "\tIP: (". $_SERVER['HTTP_X_REAL_IP'] .")\n</pre>".
            "";
            $this->mailfunctions->send(
                (new \Helper\Email(NULL, 'ReBEC'))
                    ->addTO($to)
                    ->addBCC($bcc)
                    ->addSUBJECT($_subject)
                    ->addBODY($_message)
            );
        }
        // ativar o usuario
        $this->REDIS_SERVER->del($token);

        $this->session->set_flashdata('success', 
            $messages['message_success_user_created']
        );

        redirect('/');
    }

    private function user_register_key($token) 
    {
        return 'USER-REGISTER-'.$token;
    }

    private function user_register_pack(\Auth\User $User) 
    {
        $pack = json_encode([
            'id' => $User->id,
            'md5' => md5(json_encode([
                'username' => $User->username, 
                'email' => $User->email
            ]))
        ]);
        return $pack;
    }

    private function send_email(\Auth\User $User)
    {

        $messages = $this->load_messages(new Helper\FieldLabelLanguage('General'), [
            'user_activation_mail_subject',
        ]);

        $_message = NULL;

        {
            $_subject = $messages['user_activation_mail_subject'];
        }
        {
            // poderia usar => linguagem_selecionada()
            $MailMessage = Vocabulary\MailMessage::where('label', '=', 'user-activation-with-token')->first();
            if ($MailMessage) {
                $_message = $MailMessage->description;
                $language = linguagem_selecionada();
                if ($language != 'en') {
                    $MailMessageTranslation = $MailMessage->translations()->where('language', '=', $language)->first();
                    if ($MailMessageTranslation != NULL and !empty($MailMessageTranslation->description)) { 
                        $_message = $MailMessageTranslation->description;
                    };
                }
            } else {
                $_message = "Hello __FULL_NAME__ <br /><br />\nYour registration needs to be activated before proceed a login. <br />\n Please click this link __LINK__ to do this. <br /><br />\nBest regards!";
            }
        }
        $better_token = md5(uniqid(rand(), true));
        {
            $this->REDIS_SERVER->setEx(
                $this->user_register_key($better_token),
                ((60*60)*24)*8, // ~8 dias
                $this->user_register_pack($User)
            );
        }
        {
            $dictionary = [
                '__TOKEN__' => $better_token,
                '__NAME__' => $User->first_name,
                '__FULL_NAME__' => implode(' ', [
                    $User->first_name,
                    $User->last_name,
                ]),
                '__USERNAME__' => $User->username,
                '__EMAIL__' => $User->email,
                '__SITE__' => base_url("/"),
                '__LINK__' => base_url().'signup/activate/'.$better_token,
            ];
            $_subject = strtr($_subject, $dictionary);
            $_message = strtr($_message, $dictionary);;
        }

        if ($_message == NULL) { redirect('/'); return; }

        $success = $this->mailfunctions->send(
            (new \Helper\Email(NULL, 'ReBEC'))
                ->addTO($User->email)
                ->addSUBJECT($_subject)
                ->addBODY($_message)
        );
    }
}