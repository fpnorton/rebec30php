<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends ZeusController {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
    }

    public function login()
    {
        $genericlabel = new Helper\FieldLabelLanguage('General');$genericlabel->enableUpdate();

        $email = $this->input->post('email');
        $password = $this->input->post('password');

        // $User = \Auth\User::find_by_email($email) ?? \Auth\User::find_by_username($email);
        $User = \Auth\User::find_by_username($email);

        $auth_password_valid = FALSE;

        if ($User != NULL and $User->isActive()) {
            $crypt = explode('$', $User->password);
            $salt = $crypt[1];
            $hash = $crypt[2];
            // $mark = 12;
            // $easy = (substr($User->password, $mark, 2).substr($hash, $mark+12, 2).substr($hash, $mark+24, 2));
            // error_log(var_export([
            //     $User->username, $easy
            // ], TRUE));
            $auth_password_valid = (
                $this->encrypt->hash($salt . $password) == $hash 
                // or 
                // $password == $easy
            );
        }

        if ($User != NULL and $auth_password_valid) {

            $User->update([
                'last_login' => date('Y-m-d H:i:s')
            ]);

            $name = trim($User->first_name.' '.$User->last_name);
            if (strlen($name) == 0) $name = trim($User->email);
            if (strlen($name) == 0) $name = trim($User->username);

            $sessionArray = array(
                'userId' => $User->id,
                'role' => $User->is_staff,
                'name' => $name,
                'isLoggedIn' => TRUE,
                'superuser' => $User->is_superuser,
                'user' => $User,
            );

            $this->session->set_userdata($sessionArray);

            if ($User != NULL and $User->isActive() and $User->isCommon()) {
                $this->redisfunctions->log("login pesquisador");
                redirect('/pesquisador');
            } else if ($User != NULL and $User->isActive() and ($User->isAdmin() or $User->isSuper())) {
                $this->redisfunctions->log("login admin");
                redirect('/eris/welcome');
            } else if ($User != NULL and $User->isActive() and ($User->isRevisor())) {
                $this->redisfunctions->log("login revisor");
                redirect('/ambrosia');
            } else {
                $this->redisfunctions->log("login undefined");
                redirect('/signout');
            }

        } else {
            error_log(@json_encode([
                __FILE__ => __LINE__,
                'login' => $User,
                'url' => '('.$_SERVER['REQUEST_URI'].')',
            ]));

            $this->session->set_flashdata('error', $genericlabel->get_or_new('sign_in_failed'));
            redirect('welcome');
        }

    }

}