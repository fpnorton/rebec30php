<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

use Auth\User;
use Repository\ClinicalTrial;
use Repository\Contact;

/**
 */
class Queue extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->library('parser');
        // $this->load->library('upload');
        // $this->load->library('AuxFunctions');
    }

    public function walk() {
        sleep(10);
        $queue = \Chaos\Queue::where('status', '=', 'OPEN')
            ->take(1)
            ->orderBy('created', 'asc')
            ->first();
        if ($queue == NULL) return;
        echo 'OPEN id='.$queue->id.' -> '.$queue->tag."\n";
        if ($queue->tag == 'MAIL') {
            if ($this->mailfunctions->try($queue)) {
                echo 'SUCC id='.$queue->id.' -> '.$queue->tag;
            } else {
                echo 'FAIL id='.$queue->id.' -> '.$queue->tag;
            }
            echo "\n";
        }
    }

}

?>