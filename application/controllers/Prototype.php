<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

//require_once APPPATH . '/libraries/BaseController.php';

//require_once APPPATH . '/Steps/Step1Termo.php';
//require_once APPPATH . '/Steps/Step0DadosIniciais.php';
//require_once APPPATH . '/Steps/Step1Sumario.php';
//require_once APPPATH . '/Steps/Step2Identificacao.php';
//require_once APPPATH . '/Steps/Step3Anexos.php';
//require_once APPPATH . '/Steps/Step4Patrocinadores.php';
//require_once APPPATH . '/Steps/Step5CondicoesSaude.php';
//require_once APPPATH . '/Steps/Step6Intervencao.php';
//require_once APPPATH . '/Steps/Step7Recrutamento.php';
//require_once APPPATH . '/Steps/Step8DesenhoEstudo.php';
//require_once APPPATH . '/Steps/Step9Desfecho.php';
//require_once APPPATH . '/Steps/Step10Contatos.php';


use Auth\User;
use Repository\ClinicalTrial;
use Repository\Contact;

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Prototype extends MY_Controller
{
    private $_identifier;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('parser');
        $this->load->library('upload');
        $this->load->library('AuxFunctions');
        $this->_identifier = date("Y.m.d.H.i.s");
        error_log(json_encode([
            'Prototype __construct',
            $this->session->get_userdata()['user']->username,
            $this->_identifier,
            '('.$_SERVER['REQUEST_URI'].')',
            (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
        ]));
    }

    public function __destruct() {
        error_log(json_encode([
            'Prototype __destruct',
            $this->session->get_userdata()['user']->username,
            $this->_identifier,
            '('.$_SERVER['REQUEST_URI'].')',
            (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
        ]));
    }

    public function criarInstituicao()
    {
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];
        $clinicaltrial_id = $this->input->post('clinicaltrial_id');
        {
            $institution = new \Repository\Institution(
                [
                    'name'=>$this->input->post('name'),
                    'address' => $this->input->post('postal_address'),
                    'country_id' => $this->input->post('country'),
                    'creator_id' => $this->session->userdata('user')->id,
                    '_deleted' => 0,
                    'i_type_id' => $this->input->post('institution_type'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state')
                ]
            );
            $institution->save();
        }
        {
            if ($clinicaltrial_id) {
                define('CLINICAL_TRIAL_ID', $clinicaltrial_id);
                $data['VD']['genData'] = $this->auxfunctions->loadGenData($this->session->userdata('user')->id, $clinicaltrial_id);
            } else {
                $data['VD']['genData'] = $this->auxfunctions->loadGenData($this->session->userdata('user')->id);
            }
            // $data['VD']['genData'] = [
            //     'institutions' =>
            //         \Repository\Institution::where('creator_id', '=', $this->session->userdata('user')->id)
            //             ->where('_deleted', '=', 0)
            //             ->get(),
            //     'institutiontype' =>
            //         \Vocabulary\InstitutionType::orderBy('order')
            //             ->get(),
            // ];
            $this->load->view('sheets/modal/add_institution', $data);
        }
    }

    private function carregar($clinicaltrial_id, $contact_id = 0)
    {
        ob_start();
        $contacts =[];
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];
        $clinicaltrial_id = $this->input->post('clinicaltrial_id');
        if ($contact_id > 0) {
            $contato = \Repository\Contact::find($contact_id)->withHidden(['id']);
            if ($contato != NULL)  {
                $data = $contato->toArray();
            }
        }
        {
            if ($clinicaltrial_id) {
                define('CLINICAL_TRIAL_ID', $clinicaltrial_id);
                $data['VD']['genData'] = $this->auxfunctions->loadGenData($this->session->userdata('user')->id, $clinicaltrial_id);
            } else {
                $data['VD']['genData'] = $this->auxfunctions->loadGenData($this->session->userdata('user')->id);
            }
            $this->load->view('sheets/modal/add_contact', $data);
        }
        $html = ob_get_contents();
        ob_end_clean();
        return trim($html);
    }

    public function editarContato($clinicaltrial_id, $contact_id) 
    {
        $genericlabel = new Helper\FieldLabelLanguage('General');
        $genericlabel->enableUpdate();
        ?>
            <div class="modal-header">
                <h1><?php echo $genericlabel->get_or_new('edit_contact'); ?></h1>
            </div>
            <?php $genericlabel->disableUpdate(); ?>
            <div class="modal-body">
                <?php include_once('add_contact.php'); ?>
            <?php echo $this->carregar($clinicaltrial_id, $contact_id); ?>
            </div>
            <div class="modal-footer">
            </div>
        <?
    }

    public function criarContato($clinicaltrial_id) 
    {
        $genericlabel = new Helper\FieldLabelLanguage('General');
        $genericlabel->enableUpdate();
        ?>
            <div class="modal-header">
                <h1><?php echo $genericlabel->get_or_new('new_contact'); ?></h1>
            </div>
            <?php $genericlabel->disableUpdate(); ?>
            <div class="modal-body">
                <?php include_once('add_contact.php'); ?>
            <?php echo $this->carregar($clinicaltrial_id); ?>
            </div>
            <div class="modal-footer">
            </div>
        <?
    }

    public function salvarContato()
    {
        $contacts =[];
        $headerInfo = $this->auxfunctions->getHeaderInfo();
        $footerInfo = [

        ];
        $clinicaltrial_id = $this->input->post('clinicaltrial_id');
        $contact_id = $this->input->post('contact_id') ?? 0;
        $invalid = empty(trim(
            $this->input->post('first_name').
            $this->input->post('middle_name').
            $this->input->post('last_name')
        ));
        if ($invalid == FALSE) {
            if ($contact_id > 0) {
                $contato = \Repository\Contact::find($contact_id)->withHidden(['id']); // withHidden - otherwise is a insert
                if ($contato != NULL)  {
                    $contato->update([
                        'firstname'=> $this->input->post('first_name'),
                        'middlename' => $this->input->post('middle_name'),
                        'lastname' => $this->input->post('last_name'),
                        'email' => $this->input->post('email'),
                        'address' => $this->input->post('address'),
                        'city' => $this->input->post('city'),
                        'state' => $this->input->post('state'),
                        'zip' => $this->input->post('postal_code'),
                        'telephone' => $this->input->post('phone'),

                        'country_id' => empty($this->input->post('country')) ? NULL : $this->input->post('country'),
                        'affiliation_id' => empty($this->input->post('affiliation')) ? NULL : $this->input->post('affiliation'),
                    ]);
                }
        
            } else {
                $contato = new \Repository\Contact(
                    [
                        'firstname'=> $this->input->post('first_name'),
                        'middlename' => $this->input->post('middle_name'),
                        'lastname' => $this->input->post('last_name'),
                        'email' => $this->input->post('email'),
                        'address' => $this->input->post('address'),
                        'city' => $this->input->post('city'),
                        'state' => $this->input->post('state'),
                        'zip' => $this->input->post('postal_code'),
                        'telephone' => $this->input->post('phone'),

                        'country_id' => empty($this->input->post('country')) ? NULL : $this->input->post('country'),
                        'affiliation_id' => empty($this->input->post('affiliation')) ? NULL : $this->input->post('affiliation'),

                        'creator_id' => $this->session->userdata('user')->id,
                        '_deleted' => 0,
                    ]
                );
                $contato->save();
            }
        }
        {
            if ($clinicaltrial_id) {
                define('CLINICAL_TRIAL_ID', $clinicaltrial_id);
                $data['VD']['genData'] = $this->auxfunctions->loadGenData($this->session->userdata('user')->id, $clinicaltrial_id);
            } else {
                $data['VD']['genData'] = $this->auxfunctions->loadGenData($this->session->userdata('user')->id);
            }
            return $this->load->view('sheets/modal/add_contact', $data);
        }
    }

}

?>