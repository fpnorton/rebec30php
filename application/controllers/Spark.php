<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

use Auth\User;
use Repository\ClinicalTrial;
use Repository\Contact;

/**
 */
class Spark extends Ictrp
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->library('parser');
        // $this->load->library('upload');
        // $this->load->library('AuxFunctions');
    }

    // if (empty($_SERVER['SERVER_ADDR']) == false) return;

    public function fossil_search_update()
    {
        echo "fossil_search_update\n";
        sleep(2); // delay for supervisor
        $this->load->library('RedisFunctions');
		// $this->load->helper('date');
        // $this->template->set_template($this->config->item('path', 'app') ."/layouts/app");
        $this->redis = new Redis();
        $this->redis->connect('redis', 6379);
        $this->redis->auth('rebec');

        $data = [];
        // $data['last_fossil_id'] = $this->redis->get('last_fossil_id');
        // echo 'last_fossil_id: '.$data['last_fossil_id']."\n";
        $fosseis = [];
        $fosseis = \Fossil\Fossil::orderByRaw('CAST(object_id AS decimal) desc')
            ->where('content_type_id', '=', 24)
            ->where('search_generated', '=', 0)
            ->where('is_most_recent', '=', 1)
            ->take(100)
            ->get();

        echo 'total de fosseis: '.count($fosseis)."\n";

        if (count($fosseis) == 0) return;

        foreach ($fosseis as $fossil) {
            echo 'object_id = '.$fossil->object_id."\n";

            $metaphone_data = $this->redisfunctions->update_redis_metaphone($fossil);

            $fossil->update([
                'search_generated' => 1
            ]);
        }
    }

    public function fossil_xml_update()
    {
        echo "fossil_xml_update\n";
        sleep(2); // delay for supervisor
        if (empty($_SERVER['SERVER_ADDR']) == false) {
            echo "SERVER_ADDR: ".$_SERVER['SERVER_ADDR'];
            return;
        }
        $fossil = \Fossil\Fossil::whereNotNull('serialized')
            ->where('xml_generated', '=', 0)
            ->where('is_most_recent', '=', 1)
            ->orderBy('creation', 'asc')
            ->take(1)
            ->get()
            ->first();
        
        if (empty($fossil)) echo "empty \n";
        if (empty($fossil)) return;
        echo 'object_id = '.$fossil->object_id."\n";
        $trial = new SimpleXMLElement('<trial/>');
        {
            $this->createTrialXml(
                    $fossil->id,
                    $fossil->object_id,
                    json_decode($fossil->serialized, true),
                    $trial
            );
        }
        $dom = dom_import_simplexml($trial)->ownerDocument;
        $dom->formatOutput = true;
        $xml = html_entity_decode($dom->saveXML());
        file_put_contents($_SERVER['PWD'] . "/fosseis/{$fossil->id}", $xml);
//        {
//            echo var_export([$row->id, $row->serialized, $_SERVER, $_ENV], true)."\n";
//            echo "\n";
//        }
        $fossil->update([
            'xml_generated' => 1,
        ]);
        echo "updated \n";
    }

}

?>