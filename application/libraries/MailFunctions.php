<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class MailFunctions
{
    protected $CI;
    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    
    public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
    }

    /**
     * @return Redis
     */
    public function redis() {
        $redis = new Redis();
        $redis->connect('redis', 6379);
        $redis->auth('rebec');
        return $redis;
    }

    public function send(\Helper\Email $email, $trial_id = NULL) {
        if (empty($email->getTo()['email'])) {
            $redis = $this->redis();
            $from_mail   = 
                $redis->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_FROM_ADDRESS) ??
                'rebec@icict.fiocruz.br';
            $from_name   = 
                $redis->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_FROM_NAME) ??
                'ReBEC';
            $email->addTO($from_mail, $from_name);
        }
        error_log(var_export([
            __FILE__ => __LINE__,
            'MailFunctions SEND' => true,
            '\Helper\Email $email'=> $email,
        ], TRUE));
        \Chaos\Queue::create_for_email($email, $trial_id)->save();
    }

    public function try(\Chaos\Queue $queue) {

        $email = json_decode($queue->payload, TRUE);

        $redis = $this->redis();

        $ignore_list = array_merge(
            json_decode(
                $redis->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_IGNORE_LIST),
                TRUE
            ) ?? [], ['' => '']
        );

        $mail = new PHPMailer(true);
        try {
            $config['from_mail']   = 
                $redis->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_FROM_ADDRESS) ??
                'rebec@icict.fiocruz.br';
            $config['from_name']   = 
                $redis->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_FROM_NAME) ??
                'ReBEC';
            $config['smtp_host']   = 
                $redis->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_HOST) ??
                'rebec-maildev';
            $config['smtp_port']   = 
                $redis->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_PORT) ??
                25;
            $config['smtp_user']   = 
                $redis->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_USER) ??
                'root@maildev.org';
            $config['smtp_pass']   = 
                $redis->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_PASS) ??
                'thurisaz';
            $config['smtp_crypto'] = 
                $redis->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_CRYPTO) ?? 
                '';
            $config['protocol']    = 
                $redis->get(\Helper\RedisKeys::KEY_CONFIG_EMAIL_SMTP_PROTOCOL) ??
                'smtp';
            //
            $SMTPSecure = '';
            if ($config['smtp_crypto'] == 'ssl') $SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            if ($config['smtp_crypto'] == 'tls') $SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

            // error_log(json_encode([
            //     __FILE__ => __LINE__,
            // ]));
            //Server settings
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;      // Enable verbose debug output
            $mail->isSMTP();                            // Send using SMTP
            $mail->Host       = $config['smtp_host'];   // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                   // Enable SMTP authentication
            $mail->Username   = $config['smtp_user'];   // SMTP username
            $mail->Password   = $config['smtp_pass'];   // SMTP password
            $mail->SMTPSecure = $SMTPSecure;            // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port       = $config['smtp_port'];   // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
        
            //Recipients
            $mail->setFrom($config['from_mail'], $config['from_name']);

            // $mail->addReplyTo('info@example.com', 'Information');

            foreach($email['to'] as $to) {
                if (in_array(strtolower($to['email']), $ignore_list)) continue;
                $mail->addAddress($to['email'], $to['name']); // Add a recipient
            }

            foreach($email['cc'] as $cc) {
                if (in_array(strtolower($cc['email']), $ignore_list)) continue;
                $mail->addCC($cc['email'], $cc['name']); // Add a recipient
            }

            foreach($email['bcc'] as $bcc) {
                if (in_array(strtolower($bcc['email']), $ignore_list)) continue;
                $mail->addBCC($bcc['email'], $bcc['name']); // Add a recipient
            }

            // Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        
            if ($queue->identity) $mail->MessageID = '<'.$queue->identity.'>';

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $email['subject'];
            $mail->Body    = $email['body']['html'];
            $mail->AltBody = html_entity_decode($email['body']['text']);
        
            $mail->send();

            error_log(json_encode([
                __FILE__ => __LINE__,
                'MailFunctions' => true,
                '$email' => $email,
            ]));

            $queue->update([
                'status' => 'CLOSED',
                'closed' => date('Y-m-d H:i:s'),
            ]);

            return true;

        } catch (Exception $e) {
            $queue->update([
                'closed' => date('Y-m-d H:i:s'),
            ]);
            error_log(json_encode([
                __FILE__ => __LINE__,
                'MailFunctions' => true,
                '$email' => $email,
                '$mail->ErrorInfo' => $mail->ErrorInfo,
                '$e' => $e,
            ]));
        }
        return false;
    }

}