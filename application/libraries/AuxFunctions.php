<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuxFunctions 
{
    protected $CI;
    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
    }

	public function getGUID()
	{
		if (function_exists('com_create_guid'))
		{
			return com_create_guid();
		}
		else
		{
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = chr(123)// "{"
			.substr($charid, 0, 8).$hyphen
			.substr($charid, 8, 4).$hyphen
			.substr($charid,12, 4).$hyphen
			.substr($charid,16, 4).$hyphen
			.substr($charid,20,12)
			.chr(125);// "}"
			return $uuid;
		}
	}

    function getHeaderInfo()
    {
        $genericlabel = new Helper\FieldLabelLanguage('General');
        $genericlabel->enableUpdate();
        $sidebarGerente = [ // super user
            new \Sidebar\SidebarItem('fa fa-list',
                $genericlabel->get_or_new('menu_remark_manager'),
                'ambrosia'),
            new \Sidebar\SidebarItem('fa fa-users',
                $genericlabel->get_or_new('menu_users'),
                'eris/users'),
            new \Sidebar\SidebarItem('fa fa-square',
                $genericlabel->get_or_new('menu_flat_pages_manager'),
                'eris/flatPages'),
            new \Sidebar\SidebarItem('fa fa-bookmark-o',
                $genericlabel->get_or_new('menu_field_manager'),
                'eris/fieldHelps'),
            new \Sidebar\SidebarItem('fa fa-question',
                $genericlabel->get_or_new('menu_question_manager'),
                'eris/questions'),
            new \Sidebar\SidebarItem('fa fa-envelope-o',
                $genericlabel->get_or_new('menu_mail_message_manager'),
                'eris/mailMessages'),
            new \Sidebar\SidebarItem('fa fa-newspaper-o',
                $genericlabel->get_or_new('menu_news_manager'),
                'eris/news'),
            new \Sidebar\SidebarItem('fa fa-gears',
                $genericlabel->get_or_new('menu_configuration'),
                'eris/configuration'),
        ];
        $sidebarAdmin = [
            new \Sidebar\SidebarItem('fa fa-square',
                $genericlabel->get_or_new('menu_flat_pages_manager'),
                'eris/flatPages'),
            new \Sidebar\SidebarItem('fa fa-bookmark-o',
                $genericlabel->get_or_new('menu_field_manager'),
                'eris/fieldHelps'),
            new \Sidebar\SidebarItem('fa fa-question',
                $genericlabel->get_or_new('menu_question_manager'),
                'eris/questions'),
            new \Sidebar\SidebarItem('fa fa-envelope-o',
                $genericlabel->get_or_new('menu_mail_message_manager'),
                'eris/mailMessages'),
            new \Sidebar\SidebarItem('fa fa-newspaper-o',
                $genericlabel->get_or_new('menu_news_manager'),
                'eris/news'),
        ];
        $sidebarRevisor = [
            new \Sidebar\SidebarItem('fa fa-list',
                $genericlabel->get_or_new('menu_remark_manager'),
                'ambrosia'),
        ];
        $sidebarGeneric = [
        ];
        $genericlabel->disableUpdate();
        if ($this->CI->session->userdata('user') == NULL) {
            redirect('/');
        }
        {
            if ($this->CI->session->userdata('user')->isSuper()) {
//                $this->global['pageTitle'] = 'MENU DO GERENTE';
                return [
                    'sidebar' => [
//                        'title' => $this->global['pageTitle'],
                        'items' => $sidebarGerente,
                    ]
                ];
            } else if ($this->CI->session->userdata('user')->isAdmin()) {
//                $this->global['pageTitle'] = 'MENU DO ADMINISTRADOR';
                return [
                    'sidebar' => [
//                        'title' => $this->global['pageTitle'],
                        'items' => $sidebarAdmin,
                    ]
                ];
            } else if ($this->CI->session->userdata('user')->isRevisor()) {
//                $this->global['pageTitle'] = 'MENU DO REVISOR';
                return [
                    'sidebar' => [
//                        'title' => $this->global['pageTitle'],
                        'items' => $sidebarRevisor,
                    ]
                ];
            } else {
//                $this->global['pageTitle'] = 'MENU DO PESQUISADOR';
                return [
                    'sidebar' => [
//                        'title' => $this->global['pageTitle'],
                        'items' => $sidebarGeneric,
                    ]
                ];
            }
        }
//        return [];
    }

    public function loadGenData($user_id, $clinicaltrial_id = NULL) {

        $genData = [];

        {
            $genData[''] = ['', 'M' => 'Masculino', 'F' => 'Feminino', 'B' => 'Ambos'];

            $genData['ethics_review_contact_statuses'] = \Vocabulary\EthicsReviewContactStatus::orderBy('order')
                ->get()
            ;

            $genData['ipd_plan'] = \Vocabulary\DataSharingPlan::orderBy('order')
                ->get()
            ;

            $genData['genders'] = ['', 'M' => 'Masculino', 'F' => 'Feminino', 'B' => 'Ambos'];

            $genData['countrycode'] = \Vocabulary\CountryCode::orderBy('id')
                ->get()
            ;
            $genData['recruitment_status'] = \Vocabulary\RecruitmentStatus::orderBy('order')
                ->get()
            ;
            $genData['study_type'] = \Vocabulary\StudyType::orderBy('order')
                ->get()
            ;
            $genData['institutiontype'] = \Vocabulary\InstitutionType::orderBy('order')
                ->get()
            ;
            $genData['purpose'] = \Vocabulary\StudyPurpose::orderBy('order')
                ->get()
            ;
            $genData['intervention_assignment'] = \Vocabulary\InterventionAssignment::orderBy('order')
                ->get()
            ;
            $genData['masking'] = \Vocabulary\StudyMasking::orderBy('order')
                ->get()
            ;
            $genData['allocation'] = \Vocabulary\StudyAllocation::orderBy('order')
                ->get()
            ;
            $genData['phase'] = \Vocabulary\StudyPhase::orderBy('order')
                ->get()
            ;
            $genData['observational_study_design'] = \Vocabulary\StudyPurpose::orderBy('order')
                ->get()
            ;
            $genData['time_perspective'] = \Vocabulary\TimePerspective::orderBy('order')
                ->get()
            ;
            $genData['secondaryidentifier'] = TrialNumberIssuingAuthorityIdentifier::orderBy('id')
                ->get()
            ;
            $genData['interventioncode'] = \Vocabulary\InterventionCode::orderBy('order')
                ->get()
            ;
            {
                $genData['institutions'] = \Repository\Institution::whereNull('institution_origin_id')
                    ->where('creator_id', '=', $user_id)
                    ->where('_deleted', '=', 0)
                    ->get()
                ;
                if ($clinicaltrial_id > 0) {
                    // CARREGA O ENSAIO
                    $clinicaltrial = \Repository\ClinicalTrial::with(['primarySponsor', 'secondarySponsors', 'supportSources'])
                        ->find($clinicaltrial_id);
                    // PARA CADA TIPO DE CONTATO ADICIONA SE NAO EXISTIR
                    {
                        $institution = $clinicaltrial->primarySponsor()->first();
                        if ($genData['institutions']->isEmpty()) {
                            $genData['institutions']->push($institution);
                        } else if ($institution != NULL) {
                            $found = $genData['institutions']->first(function ($key, $value) use ($institution) {
                                return ($value->equals($institution));
                            });
                            if ($found === NULL) $genData['institutions']->push($institution);
                        }
                    }
                    foreach ($clinicaltrial->secondarySponsors()->where('_deleted', '=', 0)->get() as $inner_institution) {
                        $institution = @$inner_institution->institution()->first();
                        if ($genData['institutions']->isEmpty()) {
                            $genData['institutions']->push($institution);
                        } else if ($institution != NULL) {
                            $found = $genData['institutions']->first(function ($key, $value) use ($institution) {
                                return ($value->equals($institution));
                            });
                            if ($found === NULL) $genData['institutions']->push($institution);
                        }
                    }
                    foreach ($clinicaltrial->supportSources()->where('_deleted', '=', 0)->get() as $inner_institution) {
                        $institution = @$inner_institution->institution()->first();
                        if ($genData['institutions']->isEmpty()) {
                            $genData['institutions']->push($institution);
                        } else if ($institution != NULL) {
                            $found = $genData['institutions']->first(function ($key, $value) use ($institution) {
                                return ($value->equals($institution));
                            });
                            if ($found === NULL) $genData['institutions']->push($institution);
                        }
                    }
                }
            }
            { // TRECHO DE CODIGO QUE PERMITE (COM O ELOQUENT) MANTER OS CONTATOS DO USUARIO E DO ENSAIO EM SEGUIDA
                $hashCodes=[];
                $genData['contacts'] = collect([]);
                // echo '<pre>';
                {
                    foreach(\Repository\Contact::whereNull('contact_origin_id')
                        ->where('creator_id', '=', $user_id)
                        ->where('_deleted', '=', 0)
                        ->get() as $contact) {
                        $hashCodes[$contact->hashCode()] = $contact->id;
                    }
                }
                if ($clinicaltrial_id > 0) {
                    // CARREGA O ENSAIO
                    $clinicaltrial = \Repository\ClinicalTrial::with(['publicContacts','scientificContacts','siteContacts','ethicsReviewContacts'])
                        ->find($clinicaltrial_id);
                    $submission = $clinicaltrial->submission();
                    if ($submission->status === 'approved') {
                        
                        $genData['contacts'] = collect([]);
                        $hashCodes=[];

                        foreach ($clinicaltrial->publicContacts()->where('_deleted', '=', 0)->get() as $inner_contact) {
                            $contact = @$inner_contact->contact()->first();
                            if ($contact == NULL) continue;
                            $hashCodes[$contact->hashCode()] = $contact->id;
                        }
                        foreach ($clinicaltrial->scientificContacts()->where('_deleted', '=', 0)->get() as $inner_contact) {
                            $contact = @$inner_contact->contact()->first();
                            if ($contact == NULL) continue;
                            $hashCodes[$contact->hashCode()] = $contact->id;
                        }
                        foreach ($clinicaltrial->siteContacts()->where('_deleted', '=', 0)->get() as $inner_contact) {
                            $contact = @$inner_contact->contact()->first();
                            if ($contact == NULL) continue;
                            $hashCodes[$contact->hashCode()] = $contact->id;
                        }
                        foreach ($clinicaltrial->ethicsReviewContacts()->where('_deleted', '=', 0)->get() as $inner_contact) {
                            $contact = @$inner_contact->contact()->first();
                            if ($contact == NULL) continue;
                            $hashCodes[$contact->hashCode()] = $contact->id;
                        }
                    }
                }
                foreach($hashCodes as $hashCode => $contact_id) {
                    $genData['contacts']->push(
                        \Repository\Contact::find($contact_id)
                    );
                }
                // foreach($genData['contacts'] as $contact) echo "hashCode: ".$contact->hashCode()." id: ".$contact->id."\n";
                // echo '</pre>';
                /*
                if ($clinicaltrial_id > 0) {
                    // CARREGA O ENSAIO
                    $clinicaltrial = \Repository\ClinicalTrial::with(['publicContacts','scientificContacts','siteContacts','ethicsReviewContacts'])
                        ->find($clinicaltrial_id);
                    // PARA CADA TIPO DE CONTATO ADICIONA SE NAO EXISTIR
                    foreach ($clinicaltrial->publicContacts()->where('_deleted', '=', 0)->get() as $inner_contact) {
                        $contact = @$inner_contact->contact()->first();
                        if ($contact == NULL) continue;
                        if ($genData['contacts']->isEmpty()) {
                            $genData['contacts']->push($contact);
                        } else {
                            $found = $genData['contacts']->first(function ($key, $value) use ($contact) {
                                return ($value->equals($contact));
                            });
                            if ($found === NULL) $genData['contacts']->push($contact);
                        }
                    }
                    foreach ($clinicaltrial->scientificContacts()->where('_deleted', '=', 0)->get() as $inner_contact) {
                        $contact = @$inner_contact->contact()->first();
                        if ($contact == NULL) continue;
                        if ($genData['contacts']->isEmpty()) {
                            $genData['contacts']->push($contact);
                        } else {
                            $found = $genData['contacts']->first(function ($key, $value) use ($contact) {
                                return ($value->equals($contact));
                            });
                            if ($found === NULL) $genData['contacts']->push($contact);
                        }
                    }
                    foreach ($clinicaltrial->siteContacts()->where('_deleted', '=', 0)->get() as $inner_contact) {
                        $contact = @$inner_contact->contact()->first();
                        if ($contact == NULL) continue;
                        if ($genData['contacts']->isEmpty()) {
                            $genData['contacts']->push($contact);
                        } else {
                            $found = $genData['contacts']->first(function ($key, $value) use ($contact) {
                                return ($value->equals($contact));
                            });
                            if ($found === NULL) $genData['contacts']->push($contact);
                        }
                    }
                    foreach ($clinicaltrial->ethicsReviewContacts()->where('_deleted', '=', 0)->get() as $inner_contact) {
                        $contact = @$inner_contact->contact()->first();
                        if ($contact == NULL) continue;
                        if ($genData['contacts']->isEmpty()) {
                            $genData['contacts']->push($contact);
                        } else {
                            $found = $genData['contacts']->first(function ($key, $value) use ($contact) {
                                return ($value->equals($contact));
                            });
                            if ($found === NULL) $genData['contacts']->push($contact);
                        }
                    }
                }
                $genData['contacts'] = $genData['contacts']->unique();
                // */
            }
        }
        return $genData;
    }

}