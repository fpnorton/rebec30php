<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Languageslib {
	
	protected $CI;
	
	// We'll use a constructor, as you can't directly call a function
	// from a property definition.
	public function __construct()
	{
		// Assign the CodeIgniter super-object
		$this->CI =& get_instance();
	}

	public function changeLanguages($new_lang)
	{
		$this->CI->config->set_item('language_site', $new_lang);
                $this->CI->config->set_item('language', $new_lang);
		
		//Load Languages
		SELF::loadLanguage();
	}
	
	public function loadLanguage()
	{
		//Load Languages
		$lang = $this->CI->config->item('language_site');
                
                // Validation
                if ($lang == 'pt-br') {
                    $this->CI->config->set_item('language', 'portuguese-brazilian');
                } elseif ($lang = 'en') {
                    $this->CI->config->set_item('language', 'english');
                } elseif ($lang = 'es') {
                    $this->CI->config->set_item('language', 'spanish');
                } else {
                    $this->CI->config->set_item('language', $lang);
                }
//                if ("development" == ENVIRONMENT) {
//                    echo '<pre>'.__FILE__.':'.__LINE__.' $this->CI->config->item(language_site)'."\n".var_export(array($this->CI->config->item('language_site')), TRUE).'</pre>';
//                    echo '<pre>'.__FILE__.':'.__LINE__.' $this->CI->config->item(language)'."\n".var_export(array($this->CI->config->item('language')), TRUE).'</pre>';
//                }
                $this->CI->lang->load('calendar', $this->CI->config->item($lang));
                $this->CI->lang->load('page_text', $this->CI->config->item($lang));
		
		//Update tootips
		SELF::getTooltips($lang);
		
		//Update terms of use
		SELF::getTermsOfUse($lang);
	}
	
	protected function getTooltips($lang)
	{
		$this->CI->load->model('FieldHelp_model', 'fh_model', TRUE);
		$data = $this->CI->fh_model->get_fieldhelp_by_language($lang);
		
		foreach($data as $row)
		{
			$key = 'tooltip_' . $row->form . '_' . $row->field;
			$value = htmlspecialchars($row->text, ENT_QUOTES, 'utf-8') . '<br /><br />' . htmlspecialchars($row->example, ENT_QUOTES, 'utf-8');
			$this->CI->config->set_item($key, $value);
		}
	}
	
	protected function getTermsOfUse($lang)
	{
		$this->CI->load->model('FieldHelp_model', 'fh_model', TRUE);
		$data = $this->CI->fh_model->get_term_of_use_by_language($lang);
	
		if($data)
		{
			$this->CI->config->set_item('terms_of_use', $data[0]->content);//htmlspecialchars($data[0]->content, ENT_QUOTES, 'utf-8'));
			$this->CI->config->set_item('title_terms_of_use', $data[0]->title);//htmlspecialchars($data[0]->title, ENT_QUOTES, 'utf-8'));
		}
	}
}