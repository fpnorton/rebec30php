<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RedisFunctions
{
    protected $CI;
    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
    }

    public function metaphone_words($string) {
        $array = [];
        foreach (explode(' ', $string) as $value) {
            $value = trim($value);
            if (strlen($value) < 4) continue;
            $array[strtolower($value)] = metaphone($value);
        }
        return $array;
    }

    /**
     * @return Redis
     */
    public function redis() {
        $redis = new Redis();
        $redis->connect('redis', 6379);
        $redis->auth('rebec');
        return $redis;
    }

    public function update_redis_metaphone(\Fossil\Fossil $fossil) {
        $redis = $this->redis();

        $json = json_decode($fossil->serialized, TRUE);

        $fossil_id = $fossil->id;

        $data = [];

        foreach([
                    'public_title',
                    'scientific_title',
                    'study_design',
                    'acronym',
                    'acronym_expansion',
                    'scientific_acronym',
                    'scientific_acronym_expansion',
                    'hc_freetext',
                    'i_freetext',
                    'inclusion_criteria',
                    'exclusion_criteria',
                ] as $field_name) {
            foreach ($this->metaphone_words($json[$field_name]) as $metaphone) {
                $data['en:'.$metaphone][$fossil_id.':'.$field_name] = true;
                $redis->hSet(
                    'en:'.$metaphone,
                    $fossil_id.':'.$field_name,
                    true
                );
            }
            foreach ($json['translations'] as $translation) {
                $language = $translation['language'];
                foreach ($this->metaphone_words($translation[$field_name]) as $metaphone) {
                    $data[$language.':'.$metaphone][$fossil_id.':'.$field_name] = true;
                    $redis->hSet(
                        $language.':'.$metaphone,
                        $fossil_id.':'.$field_name,
                        true
                    );
                }
            }
        }
        // -------------------------------------------------------------------------------------------------------------
        {
            $field_name_1 = 'hc_code';
            $field_name_2 = 'text';
            foreach ($this->metaphone_words($json[$field_name_1][$field_name_2]) as $metaphone) {
                $data['en:'.$metaphone][$fossil_id.':'.$field_name_1.':'.$field_name_2] = true;
                $redis->hSet(
                    'en:'.$metaphone,
                    $fossil_id.':'.$field_name_1.':'.$field_name_2,
                    true
                );
            }
            foreach ($json[$field_name_1]['translations'] as $translation) {
                $language = $translation['language'];
                foreach ($this->metaphone_words($translation[$field_name_2]) as $metaphone) {
                    $data[$language.':'.$metaphone][$fossil_id.':'.$field_name_1.':'.$field_name_2] = true;
                    $redis->hSet(
                        $language.':'.$metaphone,
                        $fossil_id.':'.$field_name_1.':'.$field_name_2,
                        true
                    );
                }
            }
        }
        {
            $field_name_1 = 'hc_keyword';
            $field_name_2 = 'text';
            foreach ($this->metaphone_words($json[$field_name_1][$field_name_2]) as $metaphone) {
                $data['en:'.$metaphone][$fossil_id.':'.$field_name_1.':'.$field_name_2] = true;
                $redis->hSet(
                    'en:'.$metaphone,
                    $fossil_id.':'.$field_name_1.':'.$field_name_2,
                    true
                );
            }
            foreach ($json[$field_name_1]['translations'] as $translation) {
                $language = $translation['language'];
                foreach ($this->metaphone_words($translation[$field_name_2]) as $metaphone) {
                    $data[$language.':'.$metaphone][$fossil_id.':'.$field_name_1.':'.$field_name_2] = true;
                    $redis->hSet(
                        $language.':'.$metaphone,
                        $fossil_id.':'.$field_name_1.':'.$field_name_2,
                        true
                    );
                }
            }
        }
        {
            $field_name_1 = 'intervention_assignment';
            $field_name_2 = 'label';
            foreach ($this->metaphone_words($json[$field_name_1][$field_name_2]) as $metaphone) {
                $data['en:'.$metaphone][$fossil_id.':'.$field_name_1.':'.$field_name_2] = true;
                $redis->hSet(
                    'en:'.$metaphone,
                    $fossil_id.':'.$field_name_1.':'.$field_name_2,
                    true
                );
            }
            foreach ($json[$field_name_1]['translations'] as $translation) {
                $language = $translation['language'];
                foreach ($this->metaphone_words($translation[$field_name_2]) as $metaphone) {
                    $data[$language.':'.$metaphone][$fossil_id.':'.$field_name_1.':'.$field_name_2] = true;
                    $redis->hSet(
                        $language.':'.$metaphone,
                        $fossil_id.':'.$field_name_1.':'.$field_name_2,
                        true
                    );
                }
            }
        }
        {
            $field_name_1 = 'intervention_keyword';
            $field_name_2 = 'description';
            foreach ($this->metaphone_words($json[$field_name_1][$field_name_2]) as $metaphone) {
                $data['en:'.$metaphone][$fossil_id.':'.$field_name_1.':'.$field_name_2] = true;
                $redis->hSet(
                    'en:'.$metaphone,
                    $fossil_id.':'.$field_name_1.':'.$field_name_2,
                    true
                );
            }
            foreach ($json[$field_name_1]['translations'] as $translation) {
                $language = $translation['language'];
                foreach ($this->metaphone_words($translation[$field_name_2]) as $metaphone) {
                    $data[$language.':'.$metaphone][$fossil_id.':'.$field_name_1.':'.$field_name_2] = true;
                    $redis->hSet(
                        $language.':'.$metaphone,
                        $fossil_id.':'.$field_name_1.':'.$field_name_2,
                        true
                    );
                }
            }
        }
        {
            $field_name_1 = 'primary_outcomes';
            $field_name_2 = 'description';
            foreach ($this->metaphone_words($json[$field_name_1][$field_name_2]) as $metaphone) {
                $data['en:'.$metaphone][$fossil_id.':'.$field_name_1.':'.$field_name_2] = true;
                $redis->hSet(
                    'en:'.$metaphone,
                    $fossil_id.':'.$field_name_1.':'.$field_name_2,
                    true
                );
            }
            foreach ($json[$field_name_1]['translations'] as $translation) {
                $language = $translation['language'];
                foreach ($this->metaphone_words($translation[$field_name_2]) as $metaphone) {
                    $data[$language.':'.$metaphone][$fossil_id.':'.$field_name_1.':'.$field_name_2] = true;
                    $redis->hSet(
                        $language.':'.$metaphone,
                        $fossil_id.':'.$field_name_1.':'.$field_name_2,
                        true
                    );
                }
            }
        }
        {
            $field_name_1 = 'secondary_outcomes';
            $field_name_2 = 'description';
            foreach ($this->metaphone_words($json[$field_name_1][$field_name_2]) as $metaphone) {
                $data['en:'.$metaphone][$fossil_id.':'.$field_name_1.':'.$field_name_2] = true;
                $redis->hSet(
                    'en:'.$metaphone,
                    $fossil_id.':'.$field_name_1.':'.$field_name_2,
                    true
                );
            }
            foreach ($json[$field_name_1]['translations'] as $translation) {
                $language = $translation['language'];
                foreach ($this->metaphone_words($translation[$field_name_2]) as $metaphone) {
                    $data[$language.':'.$metaphone][$fossil_id.':'.$field_name_1.':'.$field_name_2] = true;
                    $redis->hSet(
                        $language.':'.$metaphone,
                        $fossil_id.':'.$field_name_1.':'.$field_name_2,
                        true
                    );
                }
            }
        }

        $redis->set(
            'last_fossil_id',
            $fossil->object_id
        );

       $redis->save();

        return $data;
    }

    public function get_fossil_metaphone_results($language, $query) {
        
        $redis = $this->redis();

        $results = [];

        foreach ($this->metaphone_words($query) as $word => $metaphone) {

            $metaphone_results = $redis->hKeys($language .":".$metaphone);

            foreach ($metaphone_results as $metaphone_result) {

                $parts = explode(':', $metaphone_result);

                $results[ $parts[0] ][] = $parts[1];

            }
        }

        return $results;
    }

    public function fossil_metaphone_results($language, $query, $advanced = []) {
        
        $references = get_fossil_metaphone_results($language, $query);

        foreach($references as $fossil_id => $fields) {

            $object = \Fossil\Fossil::where('id', '=', $fossil_id);

            $fossil = $object->first();

            if (empty($fossil)) continue;

            $unserialized = json_decode($fossil->serialized, TRUE);

            if ($this->conteudo_fossil_invalido($word, $field, $unserialized)) continue;

            {
                $results[$fossil_id]['is_most_recent'] = ($fossil->is_most_recent == 1);

                if (@$results[$fossil_id]['language'] == NULL)
                    $results[$fossil_id]['language'] = linguagem_selecionada();
    
                if (array_key_exists($field, $results[$fossil_id]['words'][$word]) == false)
                    $results[$fossil_id]['words'][$word][] = $field;
    
                if (@$results[$fossil_id]['unserialized'] == NULL)
                    $results[$fossil_id]['unserialized'] = $unserialized;
    
                if (@$results[$fossil_id]['object_id'] == NULL)
                    $results[$fossil_id]['object_id'] = $fossil->object_id;
            }
        }
        // DESCEND sort by quantity of words found
        uksort($results, function($left, $right) use ($results) {
            $l = sizeof($results[$left]['words']);
            $r = sizeof($results[$right]['words']);
            if ($l > $r) return -1;
            if ($l < $r) return 1;
            return 0;
        });
        return $results;
    }


//     public function fossil_metaphone_results($language, $query, $advanced = []) {

//         $redis = $this->redis();

//         $results = [];

//         foreach ($this->metaphone_words($query) as $word => $metaphone) {

//             $metaphone_results = $redis->hKeys($language .":".$metaphone);

//             foreach ($metaphone_results as $metaphone_result) {

//                 $parts = explode(':', $metaphone_result);

//                 $fossil_id = $parts[0];
//                 $field = $parts[1];

//                 $object = \Fossil\Fossil::where('id', '=', $fossil_id);

//                 $fossil = $object->first();

//                 if (empty($fossil)) continue;

//                 $unserialized = json_decode($fossil->serialized, TRUE);

//                 if ($this->conteudo_fossil_invalido($word, $field, $unserialized)) continue;

//                 $results[$fossil_id]['is_most_recent'] = ($fossil->is_most_recent == 1);

//                 if (@$results[$fossil_id]['language'] == NULL)
//                     $results[$fossil_id]['language'] = linguagem_selecionada();

//                 if (array_key_exists($field, $results[$fossil_id]['words'][$word]) == false)
//                     $results[$fossil_id]['words'][$word][] = $field;

//                 if (@$results[$fossil_id]['unserialized'] == NULL)
//                     $results[$fossil_id]['unserialized'] = $unserialized;

//                 if (@$results[$fossil_id]['object_id'] == NULL)
//                     $results[$fossil_id]['object_id'] = $fossil->object_id;
//             }
//         }

// //        foreach ($results as $fossil_id => $pack) {
// //            $unserialized = $pack['unserialized'];
// //            $language = $pack['language'];
// //            $words = $pack['words'];
// //            foreach ($words as $word => $fields) {
// //
// //            }
// //        }

//         // DESCEND sort by quantity of words found
//         uksort($results, function($left, $right) use ($results) {
//             $l = sizeof($results[$left]['words']);
//             $r = sizeof($results[$right]['words']);
//             if ($l > $r) return -1;
//             if ($l < $r) return 1;
//             return 0;
//         });

//         return $results;
//     }

//     function conteudo_fossil($field, $fossil_unserialized) {
//         $keys = array_keys($fossil_unserialized);
//         if (in_array($field, $keys) and is_array($fossil_unserialized[$field])) {
//             $array_field = $fossil_unserialized[$field];
// //            echo '<pre>'.var_export([__FILE__=>__LINE__,$array_field], TRUE).'</pre>';
//             if (in_array('label', $array_field)) {
// //                echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $array_field['label']], TRUE).'</pre>';
//                 if (linguagem_selecionada() == 'en') return $array_field['label'];
//                 foreach ($array_field['translations'] as $translation) {
// //                echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $translation['label']], TRUE).'</pre>';
//                     if (linguagem_selecionada() == $translation['language']) return $translation['label'];
//                 }
//             } elseif (in_array('text', $array_field)) {
// //                echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $array_field['text']], TRUE).'</pre>';
//                 if (linguagem_selecionada() == 'en') return $array_field['text'];
//                 foreach ($array_field['translations'] as $translation) {
// //                echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $translation['text']], TRUE).'</pre>';
//                     if (linguagem_selecionada() == $translation['language']) return $translation['text'];
//                 }
//             } elseif (in_array('description', $array_field)) {
// //                echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $array_field['description']], TRUE).'</pre>';
//                 if (linguagem_selecionada() == 'en') return $array_field['description'];
//                 foreach ($array_field['translations'] as $translation) {
// //                echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $translation['text']], TRUE).'</pre>';
//                     if (linguagem_selecionada() == $translation['language']) return $translation['description'];
//                 }
//             }
//         } elseif (in_array($field, $keys)) {
//             if (linguagem_selecionada() == 'en') return $fossil_unserialized[$field];
//             foreach ($fossil_unserialized['translations'] as $translation) {
// //                echo '<pre>'.var_export([__FILE__=>__LINE__, $field, $translation[$field]], TRUE).'</pre>';
//                 if (linguagem_selecionada() == $translation['language']) return $translation[$field];
//             }
//         }
//     }

//     function conteudo_fossil_invalido($word, $field, $fossil_unserialized) {
// //        echo '<pre>'.var_export([__FILE__=>__LINE__, $fossil_unserialized], TRUE).'</pre>';
//         $text = $this->conteudo_fossil($field, $fossil_unserialized);
//         return (stripos($text, $word) == false);
//     }

    public function log($action) {
        $redis = $this->redis();
        $userData = $this->CI->session->get_userdata();
        if ($userData['user']) {
            $User = $userData['user'];
            $redis->hSet('user:'.$User->username, date("Y-m-d H:i:s"), $action);
        }
    }

    public function warning($log) {
        $redis = $this->redis();
        $userData = $this->CI->session->get_userdata();
        if ($userData['user']) {
            $User = $userData['user'];
            $redis->hSet('warning:'.$User->username, date("Y-m-d H:i:s"), $log);
        }
    }
}