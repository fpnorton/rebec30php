<?php defined('BASEPATH') OR exit('No direct script access allowed');

abstract class AresController extends PantheonController {

    private $_identifier;

    public function __construct()
    {
        parent::__construct();
        $this->_identifier = date("Y.m.d.H.i.s");
        error_log(json_encode([
            'AresController __construct',
            $this->session->get_userdata()['user']->username,
            $this->_identifier,
            '('.$_SERVER['REQUEST_URI'].')',
            (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
        ]));
    }

    public function __destruct() 
    {
        parent::__destruct();
        error_log(json_encode([
            'AresController __destruct',
            $this->session->get_userdata()['user']->username,
            $this->_identifier,
            '('.$_SERVER['REQUEST_URI'].')',
            (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
        ]));
        {
            $event = new \Audit\Event(['tag' => 'ARES']);
            $event->save();
            $event->access()->save(new \Audit\Access([
                'origin' => $_SERVER['HTTP_X_REAL_IP'] ?? '',
                'access' => $_SERVER['REQUEST_URI'] ?? '',
                'request' => json_encode($_POST, TRUE),
            ]));
            $event->permissions()->save(new \Audit\Permission([
                'username' => $this->session->get_userdata()['user']->username ?? '',
                'profile' => 'REGISTRANTE',
            ]));
            $event->push();
        }
    }

    protected function validate_owner(\Repository\ClinicalTrial $clinicalTrial) {
        if (is_null($clinicalTrial)) {
            redirect('pesquisador');
            return;
        }
        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        $valid = ($this->session->userdata('user') != NULL);
        if ($valid) {
            $valid = ($clinicalTrial->submission()->creator_id == $this->session->userdata('user')->id);
        }
        
        if ($valid == FALSE) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect("welcome");
            throw new Exception('user-active');
        }
    }

}