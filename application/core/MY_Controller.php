<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH . "third_party/MX/Controller.php";

class MY_Controller extends MX_Controller
{	

	function __construct() 
	{
        parent::__construct();
		$this->_hmvc_fixes();
        error_log(implode(' ', [
			"USER", "(", (@$this->session->get_userdata()['user']->username ?? '<none>'), ")",
			"IP", "(", $_SERVER['HTTP_X_REAL_IP'], ")",
			"BROWSER", "(", $_SERVER['HTTP_USER_AGENT'], ")",
		]));
	}
	
	function _hmvc_fixes()
	{
		//fix callback form_validation
		//https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc
//        foreach ([
//            'form_validation',
//            'languageslib',
//            'database',
//            'session',
//            'encrypt',
//            'email',
//             ] as $library) {
//            $this->load->library($library);
//            $this->form_validation->CI =& $this;
//        }
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
