<?php defined('BASEPATH') OR exit('No direct script access allowed');

abstract class IrisController extends MY_Controller {

    private $_identifier;

    public function __construct()
    {
        parent::__construct();
        $this->_identifier = date("Y.m.d.H.i.s");
        error_log(json_encode([
            'IrisController __construct',
            $this->session->get_userdata()['user']->username,
            $this->_identifier,
            '('.$_SERVER['REQUEST_URI'].')',
            (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
        ]));
    }

    public function __destruct() {
        error_log(json_encode([
            'IrisController __destruct',
            $this->session->get_userdata()['user']->username,
            $this->_identifier,
            '('.$_SERVER['REQUEST_URI'].')',
            (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
        ]));
    }
    protected function only_reviewer_or_author($clinical_trial_id = 0) {
        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        if ($this->session->userdata('user') == NULL or $this->session->userdata('user')->isActive() == FALSE) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect("welcome");
            throw new Exception('user-active');
        }
        if ($this->session->userdata('user')->isAdmin() == false and
            $this->session->userdata('user')->isSuper() == false and
            $this->session->userdata('user')->isRevisor() == false) {
            $clinicaltrial = \Repository\ClinicalTrial::find($clinical_trial_id);
            $submission = $clinicaltrial->submission();

            if ($this->session->userdata('user')->id != $submission->creator_id) {
                $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
                redirect('/');
                throw new Exception('only-reviewer');
            }
        }
    }

    protected function only_reviewer() {
        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        if ($this->session->userdata('user') == NULL or $this->session->userdata('user')->isActive() == FALSE) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect("welcome");
            throw new Exception('user-active');
        }
        if ($this->session->userdata('user')->isAdmin() == false and
            $this->session->userdata('user')->isSuper() == false and
            $this->session->userdata('user')->isRevisor() == false) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect('/');
            throw new Exception('only-reviewer');
        }
    }

}
