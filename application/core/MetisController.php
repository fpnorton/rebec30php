<?php defined('BASEPATH') OR exit('No direct script access allowed');

abstract class MetisController extends PantheonController {
    private $_identifier;

    public function __construct()
    {
        parent::__construct();
        $this->_identifier = date("Y.m.d.H.i.s");
        error_log(json_encode([
            'MetisController __construct',
            $this->session->get_userdata()['user']->username,
            $this->_identifier,
            '('.$_SERVER['REQUEST_URI'].')',
            (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
        ]));
    }

    public function __destruct() 
    {
        parent::__destruct();
        error_log(json_encode([
            'MetisController __destruct',
            $this->session->get_userdata()['user']->username,
            $this->_identifier,
            '('.$_SERVER['REQUEST_URI'].')',
            (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
        ]));
    }

}
