<?php defined('BASEPATH') OR exit('No direct script access allowed');

abstract class EnyoController extends PantheonController {

    private $_identifier;

    public function __construct()
    {
        parent::__construct();
        $this->_identifier = date("Y.m.d.H.i.s");
        error_log(json_encode([
            'EnyoController __construct',
            $this->session->get_userdata()['user']->username,
            $this->_identifier,
            '('.$_SERVER['REQUEST_URI'].')',
            (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
        ]));
    }

    public function __destruct() 
    {
        parent::__destruct();
        error_log(json_encode([
            'EnyoController __destruct',
            $this->session->get_userdata()['user']->username,
            $this->_identifier,
            '('.$_SERVER['REQUEST_URI'].')',
            (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
        ]));
    }

    protected function update_review_remark($context, \ReviewApp\Submission $submission) {
         
        if ($this->input->post('button_action_approve')) {

            foreach($submission->remarks->where('context', $context)->where('status', 'acknowledged') as $review_remark) {
                $review_remark->update([
                    'status' => 'closed',
                ]);
            }

            return false;

        } else if ($this->input->post('button_action_reopen')) {

            $last_remark = $submission->remarks->where('context', $context)->sortByDesc('id')->first();
            
            foreach($submission->remarks->where('context', $context)->where('status', 'acknowledged') as $review_remark) {
                $review_remark->update([
                    'status' => 'closed',
                ]);
            }

            $reviewRemark = new \ReviewApp\Remark([
                'creator_id' => $this->session->userdata('user')->id,
                'created' => date('Y-m-d H:i:s'),
                'context' => $context,
                'status' => 'open',
                'text' => $last_remark->text,
            ]);

            $submission->remarks()->save($reviewRemark);

            return false;

        } else if ($this->input->post('button_action_save')) {

            if (empty(trim($this->input->post('review_remark')))) return false;

            $remarks = $submission->remarks->where('context', $context)->where('status', 'open')->sortByDesc('id')->all();

            if (empty($remarks)) {
    
                $reviewRemark = new \ReviewApp\Remark([
                    'creator_id' => $this->session->userdata('user')->id,
                    'created' => date('Y-m-d H:i:s'),
                    'context' => $context,
                    'status' => 'open',
                    'text' => $this->input->post('review_remark'),
                ]);
                
                $submission->remarks()->save($reviewRemark);

            } else {

                foreach($remarks as $review_remark) {

                    $review_remark->update([
                        'text' => $this->input->post('review_remark'),
                    ]);
                    break;
                }
            }

        }

        return true;
    }

}
