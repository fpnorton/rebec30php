<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ZeusController extends PantheonController {
    
    private $_identifier;

    public function __construct()
    {
        parent::__construct();
        $this->_identifier = date("Y.m.d.H.i.s");
        error_log(json_encode([
            'ZeusController __construct',
            $this->session->get_userdata()['user']->username,
            $this->_identifier,
            '('.$_SERVER['REQUEST_URI'].')',
            // (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
        ]));
    }

    public function __destruct() 
    {
        parent::__destruct();
        error_log(json_encode([
            'ZeusController __destruct',
            $this->session->get_userdata()['user']->username,
            $this->_identifier,
            '('.$_SERVER['REQUEST_URI'].')',
            // (count($_POST) == 0 ? '' : 'POST: '.@json_encode($_POST)),
        ]));
    }


    protected function only_super() {
        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        if ($this->session->userdata('user') == NULL or $this->session->userdata('user')->isActive() == FALSE) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect("welcome");
            throw new Exception('user-active');
        }
        if ($this->session->userdata('user')->isSuper() == false) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect('/');
            throw new Exception('only-super');
        }
    }

}