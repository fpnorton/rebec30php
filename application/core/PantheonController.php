<?php defined('BASEPATH') OR exit('No direct script access allowed');

abstract class PantheonController extends MY_Controller {

    private $_identifier;
    
    protected $REDIS_SERVER;

    private $_cookie_control;

    public function __construct() { 
        parent::__construct();
        $this->REDIS_SERVER = $this->redisfunctions->redis();
    }

    public function __destruct() {  }

    protected function load_messages(\Helper\FieldLabelLanguage $fieldLabelLanguage, $messages)
    {
        $fieldLabelLanguage->enableUpdate();
        $return = [];
        foreach($messages as $message) $return[$message] = $fieldLabelLanguage->get_or_new($message);
        return $return;
    }

    protected function only_super() {
        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        if ($this->session->userdata('user') == NULL or $this->session->userdata('user')->isActive() == FALSE) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect("welcome");
            throw new Exception('user-active');
        }
        if ($this->session->userdata('user')->isSuper() == false) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect('/');
            throw new Exception('only-super');
        }
    }

    protected function only_admin() {
        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        if ($this->session->userdata('user') == NULL or $this->session->userdata('user')->isActive() == FALSE) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect("welcome");
            throw new Exception('user-active');
        }
        if ($this->session->userdata('user')->isAdmin() == false and
            $this->session->userdata('user')->isSuper() == false) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect('/');
            throw new Exception('only-admin');
        }

    }

    protected function only_reviewer($verify = FALSE) {
        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        if ($this->session->userdata('user') == NULL or $this->session->userdata('user')->isActive() == FALSE) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect("welcome");
            throw new Exception('user-active');
        }
        if ($this->session->userdata('user')->isAdmin() == false and
            $this->session->userdata('user')->isSuper() == false and
            $this->session->userdata('user')->isRevisor() == false) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect('/');
            throw new Exception('only-reviewer');
        }
        if ($verify) $this->verify_postback();
    }
    
    protected function only_registrant($verify = FALSE) {
        $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
        if ($this->session->userdata('user') == NULL or $this->session->userdata('user')->isActive() == FALSE) {
            $this->session->set_flashdata('error', $genericlabel->get_or_new("access_denied"));
            redirect("welcome");
            throw new Exception('user-active');
        }
        if ($verify) $this->verify_postback();
    }

    protected function generate_name() {
        $name = [
            $this->session->userdata('user') == NULL ? 0 : $this->session->userdata('user')->id,
            $_SERVER['HTTP_X_REAL_IP'],
        ];
        return sha1(md5(implode('-', $name)));
    }

    protected function generate_value() {
        $name = [
            date('Y-m-d H:i:s:u'),
        ];
        return sha1(md5(implode('|', $name)));
    }

    protected function get_postback() {
        $_name  = 'postback-'.$this->generate_name();
        $_value = $this->generate_value();
        {
            $this->REDIS_SERVER->setEx(
                $_name,
                ((60*60)*3), // ~3 horas
                $_value
            );
        }
        return $_value;
    }

    protected function set_postback(&$_array) {
        /*
        $_value = $this->get_postback();
        if (in_array('_form_action', array_keys($_array))) {
            $_array['_form_action'] .= '?';
            $_array['_form_action'] .= '_postback';
            $_array['_form_action'] .= '=';
            $_array['_form_action'] .= $_value;
        }
        // */
        return $_array;
    }


    protected function verify_postback() {
        $_value_1 = $_GET['_postback'];

        if (empty($_value_1)) return;

        $_name  = 'postback-'.$this->generate_name();

        $_value_2 = $this->REDIS_SERVER->get($_name) ?? '';

        $this->REDIS_SERVER->del($_name);

        $_equal = strcmp($_value_1, $_value_2) === 0;

        if ($_equal == false) {
            $genericlabel = new Helper\FieldLabelLanguage('General'); $genericlabel->enableUpdate();
            $this->session->set_flashdata('error', [$genericlabel->get_or_new("invalid_postback")]);
            redirect(base_url(str_replace($_value_1, '', $_SERVER['REQUEST_URI'])));
        }
    }

}
