<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Security extends CI_Security {

    public function csrf_verify()
    {
        $uri = load_class('URI', 'core');
        error_log('--> csrf_verify --> parts '.var_export([
            '$uri->uri_string()' => $uri->uri_string(),
            '$_POST[$this->_csrf_token_name]' => $_POST[$this->_csrf_token_name],
            '$_COOKIE[$this->_csrf_cookie_name]' => $_COOKIE[$this->_csrf_cookie_name],
        ], TRUE));
        // Check if URI has been whitelisted from CSRF checks
//        if ($exclude_uris = config_item('csrf_exclude_uris'))
//        {
//            $uri = load_class('URI', 'core');
//            // assumes /controller/method in your url. adjust as needed.
//            $parts = explode("/",$uri->uri_string());
//            error_log('csrf_verify --> parts '.var_export([
//                '$uri->uri_string()' => $uri->uri_string(),
//                'parts' => $parts,
//            ], TRUE));
//            if (count($parts) >= 2) {
//                $class=$parts[0];
//                $method=$parts[1];
//
//                foreach($exclude_uris as $exclude_url_data) {
//
//                    if (@$exclude_url_data['controller'] == $class
//                        && @$exclude_url_data['method'] == $method) {
//                        return $this;
//                    }
//
//                }
//            }
//        }
        return parent::csrf_verify();
    }

}