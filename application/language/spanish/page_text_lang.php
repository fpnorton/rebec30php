<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['sim'] = 'Sim';
$lang['nao'] = 'N&atilde;o';

$lang['loading'] = 'Aguarde ...';

// Textos de navega��o
$lang['nav_step'] = 'Ensaio Cl&iacute;nico';
$lang['nav_step-1'] = 'Criar um novo Ensaio Cl&iacute;nico';
$lang['nav_step0'] = 'Novo Ensaio Cl&iacute;nico';
$lang['nav_step1'] = 'Resumo Dos Ensarios Cl&iacute;nicos';
$lang['nav_step2'] = 'Identifica&ccedil;&atilde;o';
$lang['nav_step3'] = 'Anexo';
$lang['nav_step4'] = 'Patrocinadores';
$lang['nav_step5'] = 'Condi&ccedil;&otilde;es De Sa&uacute;de';
$lang['nav_step6'] = 'Interven&ccedil;&atilde;o';
$lang['nav_step7'] = 'Recrutamento';
$lang['nav_step8'] = 'Desenho Do Dstudo';
$lang['nav_step9'] = 'Desfechos';
$lang['nav_step10'] = 'Contato';
$lang['nav_step11'] = 'Enviar';

//T�tulos
$lang['step-1'] = 'Criar um novo Ensaio Cl&iacute;nico';
$lang['step0'] = 'Novo Ensaio Cl&iacute;nico';
$lang['step1'] = 'Sum&aacute;rio';
$lang['step2'] = 'Identifica&ccedil;&atilde;o';
$lang['step3'] = 'Anexos';
$lang['step4'] = 'Patrocinadores';
$lang['step5'] = 'Condi&ccedil;&otilde;es de Sa&uacute;de';
$lang['step6'] = 'Interven&ccedil;&atilde;o';
$lang['step7'] = 'Recrutamento';
$lang['step8'] = 'Desenho do Estudo';
$lang['step9'] = 'Desfechos';
$lang['step10'] = 'Contato';
$lang['step11'] = 'Enviar';

// Menu de navega��o
$lang['menu_step_0'] = 'Sum&aacute;rio';
$lang['menu_step_1'] = 'Identifica&ccedil;&atilde;o';
$lang['menu_step_2'] = 'Anexos';
$lang['menu_step_3'] = 'Patrocinadores';
$lang['menu_step_4'] = 'Cond. Sa&uacute;de';
$lang['menu_step_5'] = 'Intervencional';
$lang['menu_step_6'] = 'Recrutamento';
$lang['menu_step_7'] = 'Desenho do Estudo';
$lang['menu_step_8'] = 'Desfechos';
$lang['menu_step_9'] = 'Contatos';
$lang['menu_step_10'] = 'Enviar';
$lang['menu_step_99'] = 'Enviar';

// Termo de uso - Passo -1
$lang['terms_of_use'] = <<<TER
Termo de Ades&atilde;o e Compromisso do Sistema/Plataforma de Registros de Estudos do Registro Brasileiro de Ensaios Cl&iacute;nicos &ndash; ReBEC<br />
<br />
1 - ACEITA&Ccedil;&Atilde;O DAS CONDI&Ccedil;&Otilde;ES<br />
<br />
1.1 - O presente Termo de Ades&atilde;o e Compromisso tem por finalidade normatizar o uso do servi&ccedil;o oferecido pelo Sistema/Plataforma de Registro Brasileiro de Ensaios Cl&iacute;nicos &ndash; ReBEC, sob a gest&atilde;o do ICICT &ndash; Instituto de Comunica&ccedil;&atilde;o e Informa&ccedil;&atilde;o Cient&iacute;fica e Tecnol&oacute;gica em Sa&uacute;de da FIOCRUZ. Ao utilizar o Sistema/Plataforma, o usu&aacute;rio est&aacute; ciente de que est&aacute; sujeito &agrave;s regras aplic&aacute;veis a ele. Ao submeter os dados dos estudos ao REBEC, o usu&aacute;rio assume que leu e concordou plenamente com a vers&atilde;o mais recente do Termo e vincula-se, autom&aacute;tica e irrevogavelmente, &agrave;s regras nele contidas.<br />
<br />
1.2 - O servi&ccedil;o oferecido no Sistema/Plataforma do ReBEC n&atilde;o envolver&aacute; quaisquer &ocirc;nus para o usu&aacute;rio. Da mesma forma, n&atilde;o haver&aacute; qualquer vantagem ou retribui&ccedil;&atilde;o ao usu&aacute;rio pelas informa&ccedil;&otilde;es e dados que cadastrar no Sistema/Plataforma e pela disponibilidade deles pelo REBEC a qualquer tempo.<br />
<br />
2 - DESCRI&Ccedil;&Atilde;O DO SERVI&Ccedil;O<br />
<br />
2.1 - O ReBEC, atrav&eacute;s do Sistema da &ldquo;Plataforma OpenTrials&rdquo;, coleta e armazena informa&ccedil;&otilde;es dos estudos cadastrados pelos registrantes respons&aacute;veis, necess&aacute;rias ao cumprimento de sua miss&atilde;o institucional: promover o acesso e divulgar as informa&ccedil;&otilde;es sobre estudos realizados por pesquisadores brasileiros e estrangeiros, no Brasil e em outros pa&iacute;ses. Tais informa&ccedil;&otilde;es s&atilde;o utilizadas na divulga&ccedil;&atilde;o cient&iacute;fica, na integra&ccedil;&atilde;o aos processos &eacute;ticos e regulat&oacute;rios, no subs&iacute;dio &agrave;s pol&iacute;ticas p&uacute;blicas de incentivo &agrave; pesquisa em sa&uacute;de e na constru&ccedil;&atilde;o de outras bases de dados que subsidiam a elabora&ccedil;&atilde;o de indicadores e estudos de interesse da Sa&uacute;de e da &aacute;rea de CT&amp;I (Ci&ecirc;ncia, Tecnologia &amp; Inova&ccedil;&atilde;o).<br />
<br />
2.2 - O ReBEC &eacute; um Sistema/Plataforma virtual de acesso livre para registro gratuito de estudos realizados em seres humanos por pesquisadores brasileiros e estrangeiros. Apresenta ensaios cl&iacute;nicos que avaliam interven&ccedil;&otilde;es em sa&uacute;de resultantes do uso de medicamentos, c&eacute;lulas e outros produtos biol&oacute;gicos, procedimentos cir&uacute;rgicos e radiol&oacute;gicos, dispositivos, terapias comportamentais e complementares, mudan&ccedil;as no processo de aten&ccedil;&atilde;o preventiva e outros estudos n&atilde;o-experimentais.<br />
<br />
3 - RESPONSABILIDADE DA SENHA E SEGURAN&Ccedil;A NO SITIO<br />
<br />
3.1 - Todo usu&aacute;rio registrado no s&iacute;tio do ReBEC, se obriga a tomar ci&ecirc;ncia que:<br />
<br />
a) A senha &eacute; pessoal e intransfer&iacute;vel;<br />
b) O usu&aacute;rio se responsabiliza pela guarda, sigilo e uso da senha;<br />
c) A senha, lan&ccedil;amento, registro de informa&ccedil;&otilde;es ou quaisquer outras atividades, s&atilde;o de inteira responsabilidade do usu&aacute;rio cadastrado &ndash; inclusive para efeito legal;<br />
d) O usu&aacute;rio se obriga a notificar imediatamente ao ReBEC sobre qualquer uso n&atilde;o autorizado da sua senha ou qualquer quebra de seguran&ccedil;a que tome conhecimento. A notifica&ccedil;&atilde;o dever&aacute; ser feita por e-mail e carta registrada.<br />
<br />
3.2 - Para proteger o sigilo de sua senha, o usu&aacute;rio se obriga:<br />
<br />
a) sair de sua conta ao final de cada sess&atilde;o e assegurar que a mesma n&atilde;o seja acessada por terceiros n&atilde;o autorizados; e,<br />
b) n&atilde;o informar sua senha a terceiros, nem mesmo ao ReBEC.<br />
<br />
4 - COMPARTILHAMENTO DAS INFORMA&Ccedil;&Otilde;ES<br />
<br />
4.1 - Todos os estudos em todas as &aacute;reas de sa&uacute;de, e com testes de todas as formas de interven&ccedil;&otilde;es, s&atilde;o aceitos para registro no ReBEC. A equipe t&eacute;cnica do ReBEC rev&ecirc; cada estudo registrado. Corre&ccedil;&otilde;es, inclus&otilde;es e/ou altera&ccedil;&otilde;es poder&atilde;o ser solicitadas aos usu&aacute;rios.<br />
<br />
4.2 - Durante o processo de submiss&atilde;o, todas as informa&ccedil;&otilde;es dos estudos enviadas ao ReBEC dever&atilde;o ser disponibilizadas, obrigatoriamente em Portugu&ecirc;s e Ingl&ecirc;s.<br />
<br />
4.3 - Durante o processo de revis&atilde;o, todas as informa&ccedil;&otilde;es dos estudos enviadas ao ReBEC poder&atilde;o ser por este disponibilizadas para acesso interno. Ap&oacute;s a publica&ccedil;&atilde;o dos estudos, os dados ser&atilde;o tamb&eacute;m divulgados para o p&uacute;blico externo, atrav&eacute;s da Internet ou de outros meios, exceto as informa&ccedil;&otilde;es relativas aos dados de identifica&ccedil;&atilde;o pessoal do registrante respons&aacute;vel pelo estudo, que continuar&atilde;o disponibilizadas somente para acesso interno.<br />
<br />
4.4 - O ReBEC poder&aacute; fornecer todas as informa&ccedil;&otilde;es dos estudos recebidas dos usu&aacute;rios para outros &oacute;rg&atilde;os governamentais federais, municipais e estaduais, resguardado o compromisso de n&atilde;o exibi&ccedil;&atilde;o p&uacute;blica das informa&ccedil;&otilde;es relativas aos dados de identifica&ccedil;&atilde;o relacionados acima.<br />
<br />
4.5 - O ReBEC fica expressamente autorizado a divulgar as informa&ccedil;&otilde;es dos estudos publicados na sua plataforma &ldquo;online&rdquo;, sob qualquer forma e registro, bem como compartilh&aacute;-las ou integr&aacute;-las a outras bases de dados pr&oacute;prias ou de terceiros de interesse para a elabora&ccedil;&atilde;o de indicadores e estudos sobre desenvolvimento e pol&iacute;tica cient&iacute;fica e tecnol&oacute;gica em sa&uacute;de, sem que da&iacute; decorra qualquer &ocirc;nus ou obriga&ccedil;&atilde;o para o ReBEC, exceto de preservar a integridade, a fidelidade, a exatid&atilde;o e a corre&ccedil;&atilde;o dos dados e informa&ccedil;&otilde;es dos estudos, conforme estejam estes originariamente lan&ccedil;ados na sua plataforma pelo usu&aacute;rio.<br />
<br />
5 - CONDUTA E OBRIGA&Ccedil;&Otilde;ES DO USU&Aacute;RIO<br />
<br />
5.1 - O usu&aacute;rio se obriga, em car&aacute;ter irrevog&aacute;vel e integralmente a:<br />
<br />
a) fornecer informa&ccedil;&otilde;es verdadeiras e exatas;<br />
b) aceitar que o usu&aacute;rio &eacute; o &uacute;nico respons&aacute;vel por toda e qualquer informa&ccedil;&atilde;o cadastrada em seu registro, estando sujeito &agrave;s conseq&uuml;&ecirc;ncias, administrativas e legais, decorrentes de declara&ccedil;&otilde;es falsas ou inexatas que vierem a causar preju&iacute;zos ao ReBEC, &agrave; Administra&ccedil;&atilde;o P&uacute;blica em geral ou a terceiros;<br />
c) n&atilde;o utilizar o servi&ccedil;o para fins il&iacute;citos ou proibidos;<br />
d) n&atilde;o utilizar o servi&ccedil;o para transmitir/divulgar material il&iacute;cito, proibido ou difamat&oacute;rio, que viole a privacidade de terceiros, ou que seja abusivo, amea&ccedil;ador, discriminat&oacute;rio, injurioso, ou calunioso;<br />
e) n&atilde;o utilizar o servi&ccedil;o para transmitir/divulgar material que incentive discrimina&ccedil;&atilde;o ou viol&ecirc;ncia;<br />
f) n&atilde;o transmitir e/ou divulgar qualquer material que viole direitos de terceiros, incluindo direitos de propriedade intelectual;<br />
g) n&atilde;o obter ou tentar obter acesso n&atilde;o-autorizado a outros sistemas ou redes de computadores conectados ao servi&ccedil;o (a&ccedil;&otilde;es de hacker);<br />
h) n&atilde;o interferir ou interromper o servi&ccedil;o, as redes ou os servidores conectados ao servi&ccedil;o;<br />
i) n&atilde;o criar falsa identidade ou utilizar-se de subterf&uacute;gios com a finalidade de enganar outras pessoas ou de obter benef&iacute;cios;<br />
j) solicitar autoriza&ccedil;&atilde;o para incluir links para outros sites e/ou bases de dados; e<br />
k) comunicar imediatamente qualquer discrep&acirc;ncia constatada pelo usu&aacute;rio nos dados e informa&ccedil;&otilde;es cadastrados e divulgados no e pelo ReBEC, concomitante &agrave; sua corre&ccedil;&atilde;o procedida por ele pr&oacute;prio.<br />
<br />
6 - CONDUTA E OBRIGA&Ccedil;&Otilde;ES DO ReBEC<br />
<br />
6.1 - O ReBEC reserva-se o direito de:<br />
<br />
a) compartilhar e/ou exibir os dados dos estudos, consoante descrito no item 4;<br />
b) realizar auditorias peri&oacute;dicas acerca das informa&ccedil;&otilde;es cadastradas pelo usu&aacute;rio;<br />
c) cancelar o acesso do usu&aacute;rio ao servi&ccedil;o, bem como suprimir o registro das informa&ccedil;&otilde;es do estudo da vis&atilde;o p&uacute;blica, apenas em circunst&acirc;ncias excepcionais, sempre que verificar a m&aacute;-utiliza&ccedil;&atilde;o pelo usu&aacute;rio do Sistema, ou a pr&aacute;tica de abusos na sua utiliza&ccedil;&atilde;o e no lan&ccedil;amento de informa&ccedil;&otilde;es dos estudos, como por exemplo, a comprova&ccedil;&atilde;o que o estudo &eacute; fraudulento ou falso ou quando o registro do estudo tiver sido inadvertidamente registrado duas vezes na mesma plataforma. Entende-se por abuso toda e qualquer atividade que ocasione preju&iacute;zo ou les&atilde;o de direitos de ou a terceiros.<br />
d) comunicar formalmente ao usu&aacute;rio, explicando o motivo do cancelamento.<br />
<br />
6.2 - O ReBEC n&atilde;o se responsabiliza pelas declara&ccedil;&otilde;es falsas ou inexatas prestadas pelo usu&aacute;rio que vierem a causar preju&iacute;zos a terceiros, &agrave; Administra&ccedil;&atilde;o P&uacute;blica em geral ou ao pr&oacute;prio servi&ccedil;o.<br />
<br />
7 - MODIFICA&Ccedil;&Otilde;ES DESTE TERMO DE ADES&Atilde;O E COMPROMISSO<br />
<br />
7.1 - O ReBEC reserva-se o direito de alterar o conte&uacute;do deste Termo, sendo responsabilidade do usu&aacute;rio consult&aacute;-lo regularmente.<br />
<br />
7.2 - O uso continuado do servi&ccedil;o implica na concord&acirc;ncia do usu&aacute;rio com todas as regras, condi&ccedil;&otilde;es e avisos emanados do ReBEC, enquanto ali constar o cadastramento das informa&ccedil;&otilde;es pessoais fornecidas por aquele.<br />
<br />
7.3 - Os casos omissos deste Termo ser&atilde;o dirimidos pelo Comit&ecirc; Executivo do ReBEC, no que lhe couber.<br />
<br />
8 - LEGISLA&Ccedil;&Atilde;O APLIC&Aacute;VEL<br />
<br />
8.1 - Aplica-se ao presente Termo, e &agrave;s responsabilidades nele contidas, toda a legisla&ccedil;&atilde;o federal que lhe for pertinente.<br />
<br />
8.2 - Fica eleito o Foro da Se&ccedil;&atilde;o Judici&aacute;ria da Justi&ccedil;a Federal do Rio de Janeiro, para dirimir quaisquer d&uacute;vidas ou quest&otilde;es oriundas da execu&ccedil;&atilde;o deste Termo de Ades&atilde;o e Compromisso.<br />
<br />
Ap&ecirc;ndice<br />
<br />
a) Seu ensaio deve ser registrado uma &uacute;nica vez no ReBEC. Por favor, verifique se o ensaio j&aacute; foi registrado no REBEC antes de prosseguir com o registro. Clique aqui para realizar a busca na base de dados do ReBEC;<br />
b) Apenas o patrocinador prim&aacute;rio, institui&ccedil;&otilde;es proponentes ou seus representantes autorizados devem registrar o ensaio cl&iacute;nico. Por favor, assegure que voc&ecirc; &eacute; o registrante respons&aacute;vel antes de registrar o ensaio. Se, por exemplo, voc&ecirc; &eacute; respons&aacute;vel por s&iacute;tio de estudo de uma pesquisa multic&ecirc;ntrica, voc&ecirc; n&atilde;o deveria registrar o ensaio. Contacte a pessoa respons&aacute;vel pela coordena&ccedil;&atilde;o geral do ensaio para verificar se voc&ecirc; est&aacute; autorizado a registrar o ensaio antes de prosseguir o registro. Clique aqui para mais informa&ccedil;&otilde;es sobre registrantes respons&aacute;veis do ensaio;<br />
c) Os dados do ensaio n&atilde;o ser&atilde;o removidos ap&oacute;s o registro do estudo;<br />
d) Os registros publicados estar&atilde;o dispon&iacute;veis para serem descarregados no formato XML;<br />
e) Eu autorizo a publica&ccedil;&atilde;o dos dados apresentados neste registro;<br />
f) Eu sou a pessoa designada pelo patrocinador prim&aacute;rio para registrar este ensaio.
TER;

$lang['text_accept'] = 'Prezado(a) registrante, voc&ecirc; est&aacute; prestes a criar um novo registro de estudo no ReBEC. Voc&ecirc; &eacute; o &uacute;nico respons&aacute;vel e habilitado para fornecer informa&ccedil;&otilde;es sobre o estudo. &Eacute; importante ressaltar que seu estudo passar&aacute; por um processo de revis&atilde;o com foco no correto e coerente preenchimento dos campos que implica, a cada nova submiss&atilde;o ou ressubmiss&atilde;o feita por voc&ecirc;, um prazo de at&eacute; 45 dias para avalia&ccedil;&atilde;o por parte do ReBEC. Em tempo algum ser&aacute; feita uma aprecia&ccedil;&atilde;o &eacute;tica ou metodol&oacute;gica &quot;per se&quot;. Assim, pedimos especial gentileza na observ&acirc;ncia das orienta&ccedil;&otilde;es feitas por nossa equipe de revisores, bem como daquelas que constam nos campos de ajuda. Elas possuem todas as informa&ccedil;&otilde;es que devem constar nos campos, incluindo exemplos. Isso garantir&aacute; mais agilidade para a aprova&ccedil;&atilde;o e obten&ccedil;&atilde;o do seu n&uacute;mero de registro.</p><p>Certifique-se, com base no protocolo, da correta informa&ccedil;&atilde;o sobre o tipo de estudo - observacional ou intervencional - pois a plataforma n&atilde;o permite altera&ccedil;&atilde;o posterior, o que implicaria a dele&ccedil;&atilde;o do registro atual e cria&ccedil;&atilde;o de um outro registro e novo preenchimento. Vale ressaltar que um estudo intervencional (ou ensaio cl&iacute;nico) envolve a realiza&ccedil;&atilde;o em pacientes volunt&aacute;rios de interven&ccedil;&otilde;es &ndash; uso de drogas, equipamentos, procedimentos, dietas... - e possui desenhos com especifica&ccedil;&atilde;o de enfoque do estudo, desenho da interven&ccedil;&atilde;o, n&uacute;mero de bra&ccedil;os, tipo de mascaramento, tipo de aloca&ccedil;&atilde;o e fase do estudo. J&aacute; no estudo observacional, os pesquisadores n&atilde;o realizam interven&ccedil;&otilde;es de acordo com o plano/protocolo de pesquisa, mas observam pacientes e desfechos de uma evolu&ccedil;&atilde;o na qual eles n&atilde;o intervieram. Os Estudos Observacionais aceitos pelo ReBEC s&atilde;o os anal&iacute;ticos (caso-controle, coorte e corte transversal). Em caso de estudos que possam ter uma etapa intervencional e outra observacional, o pesquisador deve definir qual &eacute; a etapa que melhor define o projeto - se a de interven&ccedil;&atilde;o ou de observa&ccedil;&atilde;o - e escolher em que formato (intervencional ou observacional) ir&aacute; registrar o estudo. As informa&ccedil;&otilde;es sobre a segunda etapa devem ser inseridas no campo &ldquo;Descri&ccedil;&atilde;o do estudo&rdquo;.';
$lang['label_accept_terms'] = 'Eu aceito os termos de uso';
$lang['btnSalvar'] = 'Salvar';
$lang['btnAccetp'] = 'Aceitar e Criar';
$lang['btnClose'] = 'Fechar';

//Criar um estudo - Passo 0
$lang['language'] = 'Escolha os idiomas do Ensaio Cl&iacute;nico';
$lang['portuguese'] = 'Portugu&ecirc;s (Brasil)';
$lang['english'] = 'Ingl&ecirc;s (Estados Unidos)';
$lang['spanish'] = 'Espanhol';

// Sum�rio - Passo 1
$lang['tblSum_Passo'] = 'PASSO';
$lang['tblSum_Nome'] = 'NOME';
$lang['tblSum_Situacao'] = 'SITUA&Ccedil;&Atilde;O';
$lang['tblSum_Alterado'] = 'ALTERADO';
$lang['tblSum_Faltando'] = 'FALTANDO';
$lang['tblSum_Parcial'] = 'PARCIAL';
$lang['tblSum_Completo'] = 'COMPLETO';
$lang['tblSum_Enviar'] = 'ENVIAR ESSE ENSAIO';
$lang['Sum_Obs'] = 'Observa&ccedil;&otilde;es';
$lang['Sum_Hist'] = 'Hist&oacute;rico do Ensaio Cl&iacute;nico';
$lang['sum_title_conf_pub']= 'Confirma publica��o';
$lant['sum_text_conf_pub'] = 'Tem certeza de que deseja publicar este ensaio?';
$lang['sum_tooltip_ident'] = 'Os seguintes campos devem ser preenchidos:T�tulo cient�fico, T�tulo P�blico';
$lang['sum_tooltip_att'] = 'Obrigat�rio pelo menos um anexo de qualquer tipo';
$lang['sum_tooltip_sponsor'] = 'Um patrocinador prim�rio deve ser informado.';
$lang['sum_tooltip_health_cond'] = 'Os seguintes campos devem ser preenchidos:Condi��es de sa�de';
$lang['sum_tooltip_interv'] = 'C�digos de interven��es s�o obrigat�rios.';
$lang['sum_tooltip_observ'] = 'Obrigat�rio pelo menos uma observa��o.';
$lang['sum_tooltip_recruit'] = 'Os seguintes campos devem ser preenchidos:Crit�rios de inclus�o, Crit�rios de exclus�o';
$lang['sum_tooltip_study_type'] = 'Os seguintes campos devem ser preenchidos:Fase de estudo, Tipo de mascaramento, Programa de acesso expandido, Tipo de aloca��o, Desenho da interven��o, Enfoque do estudo';
$lang['sum_tooltip_outcome'] = 'Deve existir pelo menos um desfecho prim�rio.';
$lang['sum_tooltip_contact'] = 'Deve existir pelo menos um de cada tipo.';

$lang['alert_outcomers'] = <<<TER
Favor adicionar os Desfechos encontrados.
TER;
