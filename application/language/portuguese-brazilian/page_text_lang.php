﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Geral
$lang['sim'] = 'Sim';
$lang['nao'] = 'N&atilde;o';
$lang['salvo'] = 'Salvo';
$lang['pt-br'] = 'Portugu&ecirc;s';
$lang['en'] = 'Ingl&ecirc;s';
$lang['es'] = 'Espanhol';
$lang['help'] = 'Ajuda';
$lang['btnSalvar'] = 'Salvar';
$lang['btnAccetp'] = 'Aceitar e Criar';
$lang['btnClose'] = 'Fechar';
$lang['btnAccetpUpdate'] = 'Aceitar e Alterar';
$lang['btnAddAnother'] = 'Adicionar outro';
$lang['lblRemove'] = 'Remover';
$lang['my_profile'] = 'Meu perfil';
$lang['logout'] = 'Sair';
$lang['btnBack'] = 'Voltar';
$lang['unknown'] = 'Desconhecido';
$lang['remark_notes'] = 'Observações de Revisão';

$lang['new'] = 'Novo';
$lang['edit'] = 'Editar';

$lang['loading'] = 'Aguarde ...';

//Header
$lang['acessibility'] = 'Acessibilidade';
$lang['high_contrast'] = 'Alto contraste';
$lang['lbl_linguagem'] = 'Linguagem';
$lang['new_submission'] = 'Nova Submiss&atilde;o';
$lang['complete_list'] = 'Lista Completa';

// Textos de navegação
$lang['nav_step'] = 'Ensaio Cl&iacute;nico';
$lang['nav_step-1'] = 'Criar um novo Ensaio Cl&iacute;nico';
$lang['nav_step0'] = 'Novo Ensaio Cl&iacute;nico';
$lang['nav_step1'] = 'Resumo Dos Ensarios Cl&iacute;nicos';
$lang['nav_step2'] = 'Identifica&ccedil;&atilde;o';
$lang['nav_step3'] = 'Anexo';
$lang['nav_step4'] = 'Patrocinadores';
$lang['nav_step5'] = 'Condi&ccedil;&otilde;es De Sa&uacute;de';
$lang['nav_step6'] = 'Interven&ccedil;&atilde;o';
$lang['nav_step7'] = 'Recrutamento';
$lang['nav_step8'] = 'Desenho Do Estudo';
$lang['nav_step9'] = 'Desfechos';
$lang['nav_step10'] = 'Contato';
$lang['nav_step11'] = 'Enviar';

//Títulos
$lang['step-1'] = 'Criar um novo Ensaio Cl&iacute;nico';
$lang['step0'] = 'Novo Ensaio Cl&iacute;nico';
$lang['step1'] = 'Sum&aacute;rio';
$lang['step2'] = 'Identifica&ccedil;&atilde;o';
$lang['step3'] = 'Anexos';
$lang['step4'] = 'Patrocinadores';
$lang['step5'] = 'Condi&ccedil;&otilde;es de Sa&uacute;de';
$lang['step6'] = 'Interven&ccedil;&atilde;o';
$lang['step7'] = 'Recrutamento';
$lang['step8'] = 'Desenho do Estudo';
$lang['step9'] = 'Desfechos';
$lang['step10'] = 'Contato';
$lang['step11'] = 'Enviar';

// Menu de navegação
$lang['menu_step_0'] = 'Sum&aacute;rio';
$lang['menu_step_1'] = 'Identifica&ccedil;&atilde;o';
$lang['menu_step_2'] = 'Anexos';
$lang['menu_step_3'] = 'Patrocinadores';
$lang['menu_step_4'] = 'Cond. Sa&uacute;de';
$lang['menu_step_5'] = 'Interven&ccedil;&atilde;o';
$lang['menu_step_6'] = 'Recrutamento';
$lang['menu_step_7'] = 'Desenho do Estudo';
$lang['menu_step_8'] = 'Desfechos';
$lang['menu_step_9'] = 'Contatos';
$lang['menu_step_10'] = 'Enviar';
$lang['menu_step_99'] = 'Enviar';

//Mensagens de erros
$lang['error_message_terms'] = 'Voc&ecirc; deve aceitar os termos de uso para prosseguir.';
$lang['error_message_primary_sponsor'] = 'Voc&ecirc; deve selecionar um patrocinador prim&aacute;rio.';
$lang['error_message_utrn_number'] = 'O n&uacute;mero UTN &eacute; obrigat&oacute;rio.';
$lang['error_message_recruitment_countries'] = 'Voc&ecirc; deve selecionar pelo menos um pa&iacute;s.';
$lang['error_message_scientific_title'] = 'O t&iacute;tulo cient&iacute;fico &eacute; obrigat&oacute;rio.';
$lang['error_message_study_type_id'] = 'O tipo de estudo &eacute; obrigat&oacute;rio.';
$lang['error_message_name'] = 'Voc&ecirc; deve preencher um nome para prosseguir.';
$lang['error_message_country'] = 'Voc&ecirc; deve escolher um país para prosseguir.';

// Termo de uso - Passo -1
$lang['text_accept'] = 'Prezado(a) registrante, voc&ecirc; est&aacute; prestes a criar um novo registro de estudo no ReBEC. Voc&ecirc; &eacute; o &uacute;nico respons&aacute;vel e habilitado para fornecer informa&ccedil;&otilde;es sobre o estudo. &Eacute; importante ressaltar que seu estudo passar&aacute; por um processo de revis&atilde;o com foco no correto e coerente preenchimento dos campos que implica, a cada nova submiss&atilde;o ou ressubmiss&atilde;o feita por voc&ecirc;, um prazo de at&eacute; 45 dias para avalia&ccedil;&atilde;o por parte do ReBEC. Em tempo algum ser&aacute; feita uma aprecia&ccedil;&atilde;o &eacute;tica ou metodol&oacute;gica &quot;per se&quot;. Assim, pedimos especial gentileza na observ&acirc;ncia das orienta&ccedil;&otilde;es feitas por nossa equipe de revisores, bem como daquelas que constam nos campos de ajuda. Elas possuem todas as informa&ccedil;&otilde;es que devem constar nos campos, incluindo exemplos. Isso garantir&aacute; mais agilidade para a aprova&ccedil;&atilde;o e obten&ccedil;&atilde;o do seu n&uacute;mero de registro.</p><p>Certifique-se, com base no protocolo, da correta informa&ccedil;&atilde;o sobre o tipo de estudo - observacional ou intervencional - pois a plataforma n&atilde;o permite altera&ccedil;&atilde;o posterior, o que implicaria a dele&ccedil;&atilde;o do registro atual e cria&ccedil;&atilde;o de um outro registro e novo preenchimento. Vale ressaltar que um estudo intervencional (ou ensaio cl&iacute;nico) envolve a realiza&ccedil;&atilde;o em pacientes volunt&aacute;rios de interven&ccedil;&otilde;es &ndash; uso de drogas, equipamentos, procedimentos, dietas... - e possui desenhos com especifica&ccedil;&atilde;o de enfoque do estudo, desenho da interven&ccedil;&atilde;o, n&uacute;mero de bra&ccedil;os, tipo de mascaramento, tipo de aloca&ccedil;&atilde;o e fase do estudo. J&aacute; no estudo observacional, os pesquisadores n&atilde;o realizam interven&ccedil;&otilde;es de acordo com o plano/protocolo de pesquisa, mas observam pacientes e desfechos de uma evolu&ccedil;&atilde;o na qual eles n&atilde;o intervieram. Os Estudos Observacionais aceitos pelo ReBEC s&atilde;o os anal&iacute;ticos (caso-controle, coorte e corte transversal). Em caso de estudos que possam ter uma etapa intervencional e outra observacional, o pesquisador deve definir qual &eacute; a etapa que melhor define o projeto - se a de interven&ccedil;&atilde;o ou de observa&ccedil;&atilde;o - e escolher em que formato (intervencional ou observacional) ir&aacute; registrar o estudo. As informa&ccedil;&otilde;es sobre a segunda etapa devem ser inseridas no campo &ldquo;Descri&ccedil;&atilde;o do estudo&rdquo;.';
$lang['label_accept_terms'] = 'Eu aceito os termos de uso';

//Criar um estudo - Passo 0
$lang['create_language'] = 'Escolha os idiomas do Ensaio Cl&iacute;nico';
$lang['create_study_type'] = 'Tipo de estudo';

// Sumário - Passo 1
$lang['tblSum_Passo'] = 'PASSO';
$lang['tblSum_Nome'] = 'NOME';
$lang['tblSum_Situacao'] = 'SITUA&Ccedil;&Atilde;O';
$lang['tblSum_Alterado'] = 'ALTERADO';
$lang['tblSum_Faltando'] = 'FALTANDO';
$lang['tblSum_Parcial'] = 'PARCIAL';
$lang['tblSum_Completo'] = 'COMPLETO';
$lang['tblSum_Enviar'] = 'ENVIAR ESSE ENSAIO';
$lang['Sum_Obs'] = 'Observa&ccedil;&otilde;es';
$lang['Sum_Hist'] = 'Hist&oacute;rico do Ensaio Cl&iacute;nico';
$lang['sum_title_conf_pub']= 'Confirma publicação';
$lant['sum_text_conf_pub'] = 'Tem certeza de que deseja publicar este ensaio?';
$lang['sum_tooltip_ident'] = 'Os seguintes campos devem ser preenchidos:T&iacute;tulo cient&iacute;fico, T&iacute;tulo P&uacute;blico';
$lang['sum_tooltip_att'] = 'Obrigat&oacute;rio pelo menos um anexo de qualquer tipo';
$lang['sum_tooltip_sponsor'] = 'Um patrocinador prim&aacute;rio deve ser informado.';
$lang['sum_tooltip_health_cond'] = 'Os seguintes campos devem ser preenchidos:Condi&ccedil;&otilde;es de sa&uacute;de';
$lang['sum_tooltip_interv'] = 'C&oacute;digos de interven&ccedil;&otilde;es são obrigat&oacute;rios.';
$lang['sum_tooltip_observ'] = 'Obrigat&oacute;rio pelo menos uma observa&ccedil;&atilde;o.';
$lang['sum_tooltip_recruit'] = 'Os seguintes campos devem ser preenchidos:Crit&eacute;rios de inclus&atilde;o, Crit&eacute;rios de exclus&atilde;o';
$lang['sum_tooltip_study_type'] = 'Os seguintes campos devem ser preenchidos:Fase de estudo, Tipo de mascaramento, Programa de acesso expandido, Tipo de aloca&ccedil;&atilde;o, Desenho da interven&ccedil;&atilde;o, Enfoque do estudo';
$lang['sum_tooltip_outcome'] = 'Deve existir pelo menos um desfecho prim&aacute;rio.';
$lang['sum_tooltip_contact'] = 'Deve existir pelo menos um de cada tipo.';

// Identificação - Passo 2
$lang['int_scientific_title'] = 'T&iacute;tulo cient&iacute;fico';
$lang['int_utn_code'] = 'N&uacute;mero do UTN';
$lang['int_public_title'] = 'T&iacute;tulo P&uacute;blico';
$lang['int_acronym'] = 'Acr&ocirc;nimo';
$lang['int_acronym_expansion'] = 'Expans&atilde;o do Acr&ocirc;nimo';
$lang['int_scientific_acronym'] = 'Acr&ocirc;nimo Cient&iacutefico';
$lang['int_scientific_acronym_expansion'] = 'Expans&atilde;o do Acr&ocirc;nimo Cient&iacutefico';
$lang['int_stydy_design'] = 'Descri&ccedil;&atilde;o do estudo';
$lang['int_study_final_date'] = 'Data final do estudo (dd/mm/aaaa)';
$lang['int_study_status'] = 'Situa&ccedil;&atilde;o do estudo';
$lang['int_modal_conf_alt_tipo_estudo'] = '<br/><br/>Voc&ecirc; est&aacute; trocando o tipo de estudo deste ensaio. Toda a sua informa&ccedil;&atilde;o no passo 5<br/>vai ser apagada.<br/><br/>Voc&ecirc; tem certeza?<br/><br/>';
$lang['int_modal_title_alt_tipo_estudo'] = 'Confirma&ccedil;&atilde;o da troca de tipo de estudo';
$lang['int_secondary_identifier'] = 'Identificadores Secund&aacute;rios';
$lang['int_identifier'] = 'Identificador';
$lang['int_study_type'] = 'Tipo de Estudo';
$lang['int_code'] = 'C&oacute;digo';
$lang['int_description'] = 'Descri&ccedil;&atilde;o';
$lang['int_date'] = 'Data';
$lang['rule_identification_less_one'] = 'Necessário haver ao menos um identificador';

// Anexo - Passo 3
$lang['att_attachment'] = 'Anexo';
$lang['att_file'] = 'Arquivo';
$lang['att_link'] = 'Link';
$lang['att_description'] = 'Descri&ccedil;&atilde;o';
$lang['att_public'] = 'P&uacuteblico';
$lang['att_tp_file'] = 'Arquivo';
$lang['att_tp_link'] = 'Link';

// Patrocinadores - Passo 4
$lang['sponsor_type'] = 'Tipo de patrocinador';
$lang['primary_sponsor'] = 'Patrocinador Prim&aacute;rio';
$lang['secondary_sponsor'] = 'Patrocinador Secund&aacute;rio';
$lang['supporting_source'] = 'Fonte de apoio monet&aacute;rio ou material';
$lang['institution'] = 'Institui&ccedil;&atilde;o';
$lang['primary_sponsor_tooltip'] = '<p>A INSTITUI&Ccedil;&Atilde;O deve ser escolhida dentre as j&aacute; cadastradas em nosso sistema. Caso sua institui&ccedil;&atilde;o n&atilde;o conste, voc&ecirc; deve formalmente solicitar a  inclus&atilde;o dela pelo e-mail rebec@icict.fiocruz.br informando<br />1) o nome completo da institui&ccedil;&atilde;o,<br />2) o CNPJ,<br />3) o endere&ccedil;o completo com CEP,<br />4) telefone e e-mail institucionais para checagem das informa&ccedil;&oti.</p>';
$lang['new_institutuion'] = 'Nova institui&ccedil;&atilde;o';
$lang['rule_sponsor_per_type'] = 'Somente deve haver um patrocinador por tipo';

// Condições de Saúde - Passo 5
$lang['health_conditions'] = 'Condi&ccedil;&otilde;es de sa&uacute;de';
$lang['searching'] = 'Buscando';
$lang['no_results'] = 'Resultados n&atilde;o encontrados';
$lang['not_available'] = 'Busca atualmente n&atilde;o dispon&iacute;vel, tente novamente mais tarde';
$lang['search_vocabulary'] = 'Busca de Vocabul&aacute;rios';
$lang['type_press_enter'] = 'Digite para procurar e pressione \'Enter\'';
$lang['search_results'] = 'Resultados da Busca';
$lang['id_tree'] = 'ID da &aacute;rvore';
$lang['definition'] = 'Defini&ccedil;&atilde;o';
$lang['descriptors'] = 'Descritores';
$lang['type_descriptor'] = 'Tipo de descritor';
$lang['general'] = 'Geral';
$lang['specific']= 'Espec&iacute;fico';
$lang['parent_vocabulary'] = 'Vocabul&aacute;rio';
$lang['item_vocabulary_en'] = 'Itens do vocabul&aacute;rio em ingl&ecirc;s';
$lang['item_vocabulary_es'] = 'Itens do vocabul&aacute;rio em espanhol';
$lang['item_vocabulary_pt-br'] = 'Itens do vocabul&aacute;rio em portugu&ecirc;s';
$lang['search'] = 'Pesquisar';
$lang['hc_code'] = 'C&oacute;digo';

// Intervenções - Passo 6
$lang['intervention'] = 'Interven&ccedil;&atilde;o';
$lang['intervention_categories'] = 'Categorias de Interven&ccedil;&otilde;es';
$lang['rule_intervention_minimal_category'] = 'Escolha ao menos uma categoria';

// Recrutamento - Passo 7
$lang['rec_recruit_country'] = 'Pa&iacute;ses para recrutamento';
$lang['exclusion_criteria'] = 'Crit&eacute;rios de exclus&atilde;o';
$lang['inclusion_criteria'] = 'Crit&eacute;rios de inclus&atilde;o';
$lang['target_sample_size'] = 'Tamanho da amostra alvo';
$lang['years'] = 'Anos';
$lang['months'] = 'Meses';
$lang['weeks'] = 'Semanas';
$lang['days'] = 'Dias';
$lang['hours'] = 'Horas';
$lang['maximum_age_unit'] = 'Unidade para idade m&aacute;xima';
$lang['max_age'] = 'Idade m&aacute;xima';
$lang['max_age_limit'] = 'Idade m&aacute;xima sem limite';
$lang['minimum_age_unit'] = 'Unidade para idade m&iacute;nima';
$lang['min_age'] = 'Idade m&iacute;nima';
$lang['min_age_limit'] = 'Idade m&iacute;nima sem limite';
$lang['male'] = 'Masculino';
$lang['female'] = 'Feminino';
$lang['both'] = 'Ambos';
$lang['rec_gender'] = 'G&ecirc;nero para inclus&atilde;o';
$lang['date_last_enrollment'] = 'Data fim recrutamento';
$lang['date_first_enrollment'] = 'Data in&iacute;cio recrutamento';
$lang['rule_date_first_enrollment_format'] = 'A data in&iacute;cio do recrutamento est&aacute; incorreta';
$lang['rule_date_first_enrollment_limit'] = 'A data in&iacute;cio do recrutamento est&aacute; incorreta';

// Desenho do Estudo - Passo 9
$lang['expanded_access_program'] = 'Programa de acesso expandido';
$lang['purpose'] = 'Enfoque do estudo';
$lang['intervention_assignment'] = 'Desenho da interven&ccedil;&atilde;o';
$lang['number_arms'] = 'Número de bra&ccedilos';
$lang['masking_type'] = 'Tipo de mascaramento';
$lang['allocation_type'] = 'Tipo de aloca&ccedil;&atildeo';
$lang['study_phase'] = 'Fase de estudo';
$lang['study_purpose'] = 'Enfoque do estudo';
$lang['observational_study_design'] = 'Desenho do estudo observacional';
$lang['time_perspective'] = 'Temporalidade';
$lang['rule_interventional_study_number_arms'] = 'Para um estudo intervencional é preciso preencher o número de braços';
$lang['rule_study_type_unknow'] = 'Defina o tipo de desenho de estudo entre intervencional e observacional';

// Desfechos - Passo 9
$lang['outcome_type'] = 'Tipo de desfecho';
$lang['primary'] = 'Prim&aacute;rio';
$lang['secondary'] = 'Secund&aacute;rio';
$lang['rule_outcome_primary_less_two'] = 'Necessário haver ao menos dois desfechos primários';
$lang['rule_outcome_secondary_less_two'] = 'Necessário haver ao menos dois desfechos secundários';

// Contatos - Passo 10
$lang['msg_novo_contato'] = 'Voc&ecirc; est&aacute; deixando esta p&aacute;gina para criar um novo contato. Se houverem dados n&atilde;o salvos eles ser&atilde;o perdidos. Continuar?';
$lang['type'] = 'Tipo';
$lang['cont_public'] = 'Consultas p&uacute;blicas';
$lang['cont_scientific'] = 'Consultas cient&iacute;ficas';
$lang['cont_site'] = 'Contatos para Informa&ccedil;&otilde;es sobre os centros de pesquisa';
$lang['contact'] = 'Contato';
$lang['new_contact'] = 'Novo contato';
$lang['edit_contact'] = 'Editar contato';

//Instituição
$lang['name'] = 'Nome';
$lang['search_address'] = 'Buscar Endere&ccedil;o';
$lang['address'] = 'Endere&ccedil;o';
$lang['zipcode'] = 'CEP';
$lang['city'] = 'Cidade';
$lang['state'] = 'Estado';
$lang['country'] = 'Pa&iacutes';
$lang['inst_type'] = 'Tipo de institui&ccedil;&atilde;o';
$lang['add_institution'] = 'Cadastro de Institui&ccedil;&atilde;o';

$lang['alert_outcomers'] = <<<TER
Favor adicionar os Desfechos encontrados.
TER;
