<?php

namespace Auth;

use OpenTrials\OpenTrialsModel;

class Group extends OpenTrialsModel {

    protected $table = "auth_group";

    public function permissions() {
        return $this->belongsToMany(Permission::class, 'auth_group_permissions', 'group_id', 'permission_id');
    }

}