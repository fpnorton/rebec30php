<?php

namespace Auth;

use OpenTrials\OpenTrialsModel;

class Permission extends OpenTrialsModel {

    protected $table = "auth_permission";

}