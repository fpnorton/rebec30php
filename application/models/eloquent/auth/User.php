<?php

namespace Auth;

use OpenTrials\OpenTrialsModel;
use Repository\Contact;

class User extends OpenTrialsModel {

    protected $table = "auth_user";
    protected $with = [
        'groups'
    ];

    public function messages() {
        return $this->hasMany(Message::class, 'user_id', 'id');
    }

    public function permissions() {
        return $this->belongsToMany(Permission::class, 'auth_user_user_permissions', 'user_id', 'permission_id');
    }

    public function groups() {
        return $this->belongsToMany(Group::class, 'auth_user_groups', 'user_id', 'group_id');
    }

    public function contacts() {
        return $this->hasMany(Contact::class, 'creator_id', 'id');
    }

    public function isRevisor() {
        return ($this->groups()->where('group_id', '=', 1)->count() > 0);
    }

    public function isAdmin() {
        return ($this->groups()->where('group_id', '=', 2)->count() > 0);
    }

    public function isSuper() {
        return ($this->groups()->where('group_id', '=', 3)->count() > 0);
    }

    public function isCommon() {
        return ($this->groups()->count() == 0);
    }

    public function isActive() {
        return ($this->is_active == 1);
    }

    static public function find_by_username($username = NULL) {
        return SELF::where('username', '=', $username)->orderBy('last_login', 'desc')->get()->first();
    }

    static public function find_by_email($email = NULL) {
        return SELF::where('email', '=', $email)->orderBy('last_login', 'desc')->get()->first();
    }

    static public function find_by_username_or_email($username_or_email = NULL) {
        $user = SELF::find_by_username($username_or_email);
        if ($user) return $user;
        $user = SELF::find_by_email($username_or_email);
        if ($user) return $user;
        return NULL;
    }

}