<?php

namespace Fossil;

use OpenTrials\OpenTrialsModel;

class Fossil extends OpenTrialsModel {

    protected $table = "fossil_fossil";
    protected $with = [
        'indexers',
    ];

    public function indexers() {
        return $this->hasMany(Indexer::class, 'fossil_id');
    }

    static public function most_recent($trial_id) {
        return Fossil::where('is_most_recent', '=', 1)->where('object_id', '=', $trial_id)->get()->first();
    }

    static public function createGUID()
    {
        if (function_exists('com_create_guid'))
        {
            return com_create_guid();
        }
        else
        {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12)
                .chr(125);// "}"
            return $uuid;
        }
    }

}