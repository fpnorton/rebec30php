<?php

namespace Fossil;

use OpenTrials\OpenTrialsModel;

class Indexer extends OpenTrialsModel {

    protected $table = "fossil_fossilindexer";

}