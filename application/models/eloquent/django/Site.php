<?php
/**
 * Created by PhpStorm.
 * User: josue
 * Date: 08/04/19
 * Time: 05:43
 */

namespace Django;


use OpenTrials\OpenTrialsModel;

class Site extends OpenTrialsModel
{

    protected $table = "django_site";

}