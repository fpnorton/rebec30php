<?php
/**
 * Created by PhpStorm.
 * User: josue
 * Date: 08/04/19
 * Time: 05:43
 */

namespace Django;


use OpenTrials\OpenTrialsModel;

class FlatPage extends OpenTrialsModel
{
    protected $table = "django_flatpage";
    protected $with = [
        'translations',
        'sites',
    ];

    public function translations() {
        return $this->hasMany(FlatPageTranslation::class, 'object_id');
    }

    public function sites() {
        return $this->belongsToMany(Site::class, 'django_flatpage_sites', 'flatpage_id', 'site_id');
    }

}