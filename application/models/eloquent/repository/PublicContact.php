<?php

namespace Repository;

use OpenTrials\FlaggedTrait;
use OpenTrials\OpenTrialsModel;

class PublicContact extends OpenTrialsModel {

    use FlaggedTrait;

    protected $table = "repository_publiccontact";
    protected $with = [
        'contact'
    ];

    public function trial() {
        return $this->belongsTo(ClinicalTrial::class, 'trial_id', 'id');
    }

    public function contact() {
        return $this->hasOne(Contact::class, 'id', 'contact_id');
    }

}