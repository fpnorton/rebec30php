<?php

namespace Repository;

use OpenTrials\FlaggedTrait;
use OpenTrials\OpenTrialsModel;

class Outcome extends OpenTrialsModel {

    protected $with = [
        'translations',
    ];

    use FlaggedTrait;

    protected $table = "repository_outcome";

    public function trial() {
        return $this->belongsTo(ClinicalTrial::class, 'trial_id');
    }

    public function translations() {
        return $this->hasMany(OutcomeTranslation::class, 'object_id', 'id');
    }

}