<?php

namespace Repository;

use Django\ContentType;
use OpenTrials\OpenTrialsModel;

class OutcomeTranslation extends OpenTrialsModel {

    protected $table = "repository_outcometranslation";

    public function outcome() {
        return $this->belongsTo(Outcome::class, 'id', 'object_id');
    }

    public function contentType() {
        return $this->hasOne(ContentType::class, 'id', 'content_type_id');
    }

}