<?php

namespace Repository;

use OpenTrials\FlaggedTrait;
use OpenTrials\OpenTrialsModel;

class Institution extends OpenTrialsModel {

    use FlaggedTrait;

    protected $hidden = ['id','creator_id'];
    protected $table = "repository_institution";

    public function secondarySponsors() {
        return $this->hasMany(SecondarySponsor::class);
    }

    public function hashCode() {
        return md5($this->toJson());
    }

    public function equals(Institution $institution) {
        return ($this->hashCode() === $institution->hashCode());
    }
}