<?php

namespace Repository;

use OpenTrials\FlaggedTrait;
use OpenTrials\OpenTrialsModel;

class SecondarySponsor extends OpenTrialsModel {

    use FlaggedTrait;

    protected $table = "repository_trialsecondarysponsor";

    public function trial() {
        return $this->belongsTo(ClinicalTrial::class, 'id', 'trial_id');
    }

    public function institution() {
        return $this->hasOne(Institution::class, 'id', 'institution_id');
    }

}