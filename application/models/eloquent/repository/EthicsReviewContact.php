<?php

namespace Repository;

use OpenTrials\FlaggedTrait;
use OpenTrials\OpenTrialsModel;
use Vocabulary\EthicsReviewContactStatus;

class EthicsReviewContact extends OpenTrialsModel {

    use FlaggedTrait;

    protected $table = "repository_ethicsreviewcontact";
    protected $with = [
        'contact', 'status'
    ];

    public function trial() {
        return $this->belongsTo(ClinicalTrial::class, 'trial_id', 'id');
    }

    public function contact() {
        return $this->hasOne(Contact::class, 'id', 'contact_id');
    }

    public function status() {
        return $this->hasOne(EthicsReviewContactStatus::class, 'id', 'ethicsreviewcontactstatus_id');
    }

}