<?php

namespace Repository;

use Django\ContentType;
use OpenTrials\OpenTrialsModel;

class DescriptorTranslation extends OpenTrialsModel {

    protected $table = "repository_descriptortranslation";

    public function trial() {
        return $this->belongsTo(Descriptor::class, 'id', 'object_id');
    }

    public function contentType() {
        return $this->hasOne(ContentType::class, 'id', 'content_type_id');
    }

}