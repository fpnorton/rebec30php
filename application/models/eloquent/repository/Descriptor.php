<?php

namespace Repository;

use OpenTrials\FlaggedTrait;
use OpenTrials\OpenTrialsModel;

class Descriptor extends OpenTrialsModel {

    protected $with = [
        'translations',
    ];

    use FlaggedTrait;

    protected $table = "repository_descriptor";

    public function trial() {
        return $this->belongsTo(ClinicalTrial::class, 'trial_id', 'id');
    }

    public function translations() {
        return $this->hasMany(DescriptorTranslation::class, 'object_id', 'id');
    }

}