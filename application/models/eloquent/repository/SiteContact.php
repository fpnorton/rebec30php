<?php

namespace Repository;

use OpenTrials\FlaggedTrait;
use OpenTrials\OpenTrialsModel;

class SiteContact extends OpenTrialsModel {

    use FlaggedTrait;

    protected $table = "repository_sitecontact";
    protected $with = [
        'contact'
    ];

    public function trial() {
        return $this->belongsTo(ClinicalTrial::class, 'trial_id', 'id');
    }

    public function contact() {
        return $this->hasOne(Contact::class, 'id', 'contact_id');
    }

}