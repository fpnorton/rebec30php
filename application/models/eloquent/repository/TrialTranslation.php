<?php

namespace Repository;

use Django\ContentType;
use OpenTrials\OpenTrialsModel;

class TrialTranslation extends OpenTrialsModel {

    protected $table = "repository_clinicaltrialtranslation";

    public function trial() {
        return $this->belongsTo(ClinicalTrial::class, 'object_id');
    }

    public function contentType() {
        return $this->hasOne(ContentType::class, 'id', 'content_type_id');
    }

}