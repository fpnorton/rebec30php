<?php

namespace Repository;

use OpenTrials\FlaggedTrait;
use OpenTrials\OpenTrialsModel;

class SupportSource extends OpenTrialsModel {

    use FlaggedTrait;

    protected $table = "repository_trialsupportsource";

    public function trial() {
        return $this->belongsTo(ClinicalTrial::class, 'trial_id', 'id');
    }

    public function institution() {
        return $this->hasOne(Institution::class, 'id', 'institution_id');
    }

}