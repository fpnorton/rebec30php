<?php

namespace Repository;

use Fossil\Fossil;
use OpenTrials\FlaggedTrait;
use OpenTrials\OpenTrialsModel;
use ReviewApp\Submission;
use Vocabulary\CountryCode;
use Vocabulary\DataSharingPlan;
use Vocabulary\InterventionCode;
use Picolo\Resubmit;
use Picolo\Submit;
use Picolo\Approved;

class ClinicalTrial extends OpenTrialsModel {

//    protected $with = [
//        'submission'
//        'translations',
//        'numbers',
//        'descriptors',
//        'publicContacts',
//        'scientificContacts',
//        'siteContacts',
//        'interventionCodes',
//        'outcomes',
//        'primarySponsor',
//        'secondarySponsors',
//        'supportSources',
//    ];

    use FlaggedTrait;

    protected $table = "repository_clinicaltrial";

    public function fossils() {
        return $this->hasMany(Fossil::class, 'object_id');
    }

    public function translations() {
        return $this->hasMany(TrialTranslation::class, 'object_id');
    }

    public function numbers() {
        return $this->hasMany(TrialNumber::class, 'trial_id')->where('_deleted', '=', 0);
    }

    public function descriptors() {
        return $this->hasMany(Descriptor::class, 'trial_id');
    }

    public function publicContacts() {
        return $this->hasMany(PublicContact::class, 'trial_id');
    }

    public function scientificContacts() {
        return $this->hasMany(ScientificContact::class, 'trial_id');
    }

    public function siteContacts() {
        return $this->hasMany(SiteContact::class, 'trial_id');
    }

    public function ethicsReviewContacts() {
        return $this->hasMany(EthicsReviewContact::class, 'trial_id');
    }

    public function interventionCodes() {
        return $this->belongsToMany(InterventionCode::class, 'repository_clinicaltrial_i_code', 'clinicaltrial_id', 'interventioncode_id')->withPivot('id');
    }

    public function outcomes() {
        return $this->hasMany(Outcome::class, 'trial_id');
    }

    public function primarySponsor() {
        return $this->hasOne(Institution::class, 'id', 'primary_sponsor_id');
    }

    public function secondarySponsors() {
        return $this->hasMany(SecondarySponsor::class, 'trial_id', 'id');
    }

    public function supportSources() {
        return $this->hasMany(SupportSource::class, 'trial_id', 'id');
    }
//
//    public function submissions() {
//        return $this->hasMany(Submission::class, 'trial_id', 'id');
//    }

    public function ipdSharingPlan() {
        return $this->hasOne(DataSharingPlan::class, 'id', 'results_IPD_plan_id');
    }

    public function submission() {
        return $this->submissions->first();
    }

    public function submissions() {
        return $this->hasMany(Submission::class, 'trial_id', 'id')->orderBy('id');
    }

    public function recruitmentCountries() {
        return $this->belongsToMany(CountryCode::class, 'repository_clinicaltrial_recruitment_country', 'clinicaltrial_id', 'countrycode_id')->withPivot('id');
    }

    public function isObservacional() {
        return ($this->study_type_id == 2 || $this->is_observational == 1);
    }

    public function isIntervencional() {
        return ($this->study_type_id == 1 || $this->is_observational == 0);
    }

    public function resubmits() {
        return $this->hasMany(Resubmit::class, 'trial_id', 'id');
    }

    public function submits() {
        return $this->hasMany(Submit::class, 'trial_id', 'id');
    }

    public function approveds() {
        return $this->hasMany(Approved::class, 'trial_id', 'id');
    }

    // -----------------------------------------------------------------------------------------------------

    public function selectedLanguages($version = 1)
    {
        $data = NULL;
        if ($version == 1) {
            $data[] = ['language' => 'en'];
            foreach ($this->translations as $translation) {
                $data[] = ['language' => $translation->language];
            }
        } else if ($version == 2) {
            $data[] = 'en';
            foreach ($this->translations as $translation) {
                $data[] = $translation->language;
            }
        }
        return $data;
    }

}