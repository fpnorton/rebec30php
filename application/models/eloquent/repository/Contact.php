<?php

namespace Repository;

use Auth\User;
use OpenTrials\FlaggedTrait;
use OpenTrials\OpenTrialsModel;
use Vocabulary\CountryCode;

class Contact extends OpenTrialsModel {

    use FlaggedTrait;

    protected $hidden = ['id','creator_id'];
    protected $table = "repository_contact";

    public function user() {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function affiliation() {
        return $this->hasOne(Institution::class, 'id', 'affiliation_id');
    }

    public function country() {
        return $this->hasOne(CountryCode::class, 'id', 'country_id');
    }

    public function publics() {
        return $this->hasMany( PublicContact::class, 'contact_id', 'id');
    }

    public function scientifics() {
        return $this->hasMany( ScientificContact::class, 'contact_id', 'id');
    }

    public function sites() {
        return $this->hasMany( SiteContact::class, 'contact_id', 'id');
    }

    public function ethicsReviews() {
        return $this->hasMany( EthicsReviewContact::class, 'contact_id', 'id');
    }

    public function toJsonExtra() {
        $data = [
            'firstname'         => NULL,
            'middlename'        => NULL,
            'lastname'          => NULL,
            'email'             => NULL,
            'affiliation_id'    => NULL,
            'address'           => NULL,
            'city'              => NULL,
            'country_id'        => NULL,
            'zip'               => NULL,
            'telephone'         => NULL,
            'state'             => NULL,
        ];
        foreach (array_keys($data) as $key) $data[$key] = $this->$key;
        return json_encode($data, TRUE);
    }

    public function hashCode() {
        return md5($this->toJsonExtra());
    }

    public function equals(Contact $contact) {
        return ($this->hashCode() === $contact->hashCode());
    }
}