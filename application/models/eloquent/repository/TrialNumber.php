<?php

namespace Repository;

use OpenTrials\FlaggedTrait;
use OpenTrials\OpenTrialsModel;

class TrialNumber extends OpenTrialsModel {

    use FlaggedTrait;

    protected $table = "repository_trialnumber";

    public function trial() {
        return $this->belongsTo(ClinicalTrial::class, 'trial_id', 'id');
    }

}