<?php

namespace Assistance;

use OpenTrials\OpenTrialsModel;

class FieldHelp extends OpenTrialsModel {

    protected $table = "assistance_fieldhelp";

    public function translations() {
        return $this->hasMany(FieldHelpTranslation::class, 'object_id');
    }
}