<?php

namespace Assistance;

use OpenTrials\OpenTrialsModel;

class Category extends OpenTrialsModel {

    protected $table = "assistance_category";

    public function translations() {
        return $this->hasMany(CategoryTranslation::class, 'object_id');
    }
}