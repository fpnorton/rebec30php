<?php

namespace Assistance;

use OpenTrials\OpenTrialsModel;

class Languages extends OpenTrialsModel {

    protected $table = "assistance_languages";

}