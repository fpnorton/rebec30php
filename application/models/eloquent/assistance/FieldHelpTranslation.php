<?php

namespace Assistance;

use Django\ContentType;
use OpenTrials\OpenTrialsModel;

class FieldHelpTranslation extends OpenTrialsModel {

    protected $table = "assistance_fieldhelptranslation";

    public function contentType() {
        return $this->hasOne(ContentType::class, 'id', 'content_type_id');
    }

}