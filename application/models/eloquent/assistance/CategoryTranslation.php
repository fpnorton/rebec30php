<?php

namespace Assistance;

use Django\ContentType;
use OpenTrials\OpenTrialsModel;

class CategoryTranslation extends OpenTrialsModel {

    protected $table = "assistance_categorytranslation";

    public function contentType() {
        return $this->hasOne(ContentType::class, 'id', 'content_type_id');
    }
}