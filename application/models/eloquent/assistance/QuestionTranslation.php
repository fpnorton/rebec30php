<?php

namespace Assistance;

use Django\ContentType;
use OpenTrials\OpenTrialsModel;

class QuestionTranslation extends OpenTrialsModel {

    protected $table = "assistance_questiontranslation";

    public function contentType() {
        return $this->hasOne(ContentType::class, 'id', 'content_type_id');
    }
}