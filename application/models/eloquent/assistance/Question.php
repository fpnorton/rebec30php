<?php

namespace Assistance;

use OpenTrials\OpenTrialsModel;

class Question extends OpenTrialsModel {

    protected $table = "assistance_question";

    public function translations() {
        return $this->hasMany(QuestionTranslation::class, 'object_id');
    }
}