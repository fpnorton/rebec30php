<?php

namespace Vocabulary;

use OpenTrials\OpenTrialsModel;

class TimePerspective extends OpenTrialsModel {

    protected $table = "vocabulary_timeperspective";
    protected $with = [
        'translations',
    ];

    public function translations() {
        return $this->hasMany(VocabularyTranslation::class, 'object_id')->where('content_type_id', '=', 62);
    }

}