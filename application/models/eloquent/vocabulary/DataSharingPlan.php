<?php

namespace Vocabulary;

use OpenTrials\OpenTrialsModel;

class DataSharingPlan extends OpenTrialsModel {

    protected $table = "vocabulary_ipd_plan";
    protected $with = [
        'translations',
    ];

    public function translations() {
        return $this->hasMany(VocabularyTranslation::class, 'object_id')->where('content_type_id', '=', 67);
    }
}