<?php

namespace Vocabulary;

use OpenTrials\OpenTrialsModel;

class RecruitmentStatus extends OpenTrialsModel {

    protected $table = "vocabulary_recruitmentstatus";
    protected $with = [
        'translations',
    ];

    public function translations() {
        return $this->hasMany(VocabularyTranslation::class, 'object_id')->where('content_type_id', '=', 20);
    }

}