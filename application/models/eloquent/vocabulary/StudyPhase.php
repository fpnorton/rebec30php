<?php

namespace Vocabulary;

use OpenTrials\OpenTrialsModel;

class StudyPhase extends OpenTrialsModel {

    protected $table = "vocabulary_studyphase";
    protected $with = [
        'translations',
    ];

    public function translations() {
        return $this->hasMany(VocabularyTranslation::class, 'object_id')->where('content_type_id', '=', 19);
    }

}