<?php

namespace Vocabulary;

use OpenTrials\OpenTrialsModel;

class InstitutionType extends OpenTrialsModel {

    protected $table = "vocabulary_institutiontype";
    protected $with = [
        'translations',
    ];

    public function translations() {
        return $this->hasMany(VocabularyTranslation::class, 'object_id')->where('content_type_id', '=', 61);
    }
}