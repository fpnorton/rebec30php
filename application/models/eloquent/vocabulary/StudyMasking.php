<?php

namespace Vocabulary;

use OpenTrials\OpenTrialsModel;

class StudyMasking extends OpenTrialsModel {

    protected $table = "vocabulary_studymasking";
    protected $with = [
        'translations',
    ];

    public function translations() {
        return $this->hasMany(VocabularyTranslation::class, 'object_id')->where('content_type_id', '=', 17);
    }

}