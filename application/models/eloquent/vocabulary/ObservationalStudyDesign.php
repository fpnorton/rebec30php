<?php

namespace Vocabulary;

use OpenTrials\OpenTrialsModel;

class ObservationalStudyDesign extends OpenTrialsModel {

    protected $table = "vocabulary_observationalstudydesign";
    protected $with = [
        'translations',
    ];

    public function translations() {
        return $this->hasMany(VocabularyTranslation::class, 'object_id')->where('content_type_id', '=', 63);
    }

}