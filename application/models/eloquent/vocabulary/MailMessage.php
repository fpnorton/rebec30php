<?php

namespace Vocabulary;

use OpenTrials\OpenTrialsModel;

class MailMessage extends OpenTrialsModel {

    protected $table = "vocabulary_mailmessage";
    protected $with = [
        'translations',
    ];

    public function translations() {
        return $this->hasMany(VocabularyTranslation::class, 'object_id')->where('content_type_id', '=', 64);
    }
}