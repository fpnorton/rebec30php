<?php

namespace Vocabulary;

use OpenTrials\OpenTrialsModel;

class StudyAllocation extends OpenTrialsModel {

    protected $table = "vocabulary_studyallocation";
    protected $with = [
        'translations',
    ];

    public function translations() {
        return $this->hasMany(VocabularyTranslation::class, 'object_id')->where('content_type_id', '=', 18);
    }

}