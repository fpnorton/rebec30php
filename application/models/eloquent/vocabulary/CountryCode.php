<?php

namespace Vocabulary;

use OpenTrials\OpenTrialsModel;

class CountryCode extends OpenTrialsModel {

    protected $table = "vocabulary_countrycode";
    protected $with = [
        'translations',
    ];

    public function translations() {
        return $this->hasMany(VocabularyTranslation::class, 'object_id')->where('content_type_id', '=', 11);
    }

}