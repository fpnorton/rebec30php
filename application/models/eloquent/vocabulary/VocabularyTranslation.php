<?php

namespace Vocabulary;

use OpenTrials\OpenTrialsModel;
use ReviewApp\Submission;

class VocabularyTranslation extends OpenTrialsModel {

    protected $table = "vocabulary_vocabularytranslation";

}