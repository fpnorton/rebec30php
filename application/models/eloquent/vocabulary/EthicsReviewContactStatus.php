<?php

namespace Vocabulary;

use OpenTrials\OpenTrialsModel;

class EthicsReviewContactStatus extends OpenTrialsModel {

    protected $table = "vocabulary_ethicsreviewcontactstatus";
    protected $with = [
        'translations',
    ];

    public function translations() {
        return $this->hasMany(VocabularyTranslation::class, 'object_id')->where('content_type_id', '=', 66);
    }
}