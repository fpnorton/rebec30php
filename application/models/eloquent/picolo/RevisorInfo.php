<?php

namespace Picolo;

use OpenTrials\OpenTrialsModel;

class RevisorInfo extends OpenTrialsModel {

    protected $table = "picolo_revisor_info";

}