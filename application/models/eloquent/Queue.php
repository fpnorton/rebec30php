<?php
/**
 * Created by PhpStorm.
 * User: josue
 * Date: 08/04/19
 * Time: 05:43
 */

namespace Chaos;


use Auth\User;
use OpenTrials\OpenTrialsModel;
use Repository\ClinicalTrial;

class Queue extends OpenTrialsModel
{
    protected $table = "queue";

    public function create_for_email(\Helper\Email $email, $trial_id = NULL) {
        $queue = new Queue();
        $queue->tag = 'MAIL';
        $queue->identity = 'REBEC-'.sha1(date('Y-m-d H:i:s')).($trial_id != NULL ? ('-'.$trial_id) : '').'@ensaiosclinicos.gov.br';
        $queue->status = 'OPEN';
        $queue->payload = json_encode([
            // 'from' => [
            //     'email' => $email->getFromEmail(),
            //     'name' => $email->getFromName(),
            // ],
            'to' => $email->getTo(),
            'cc' => $email->getCarbonCopy(),
            'bcc' => $email->getBlindedCarbonCopy(),
            'subject' => $email->getSubject(),
            'body' => [
                'text' => $email->getTextBody(),
                'html' => $email->getHtmlBody(),
            ],
        ]);
        $queue->created = date('Y-m-d H:i:s');
        return $queue;
    }
}