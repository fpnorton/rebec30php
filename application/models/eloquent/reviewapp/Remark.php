<?php

namespace ReviewApp;

use Auth\User;
use OpenTrials\OpenTrialsModel;

class Remark extends OpenTrialsModel {

    protected $table = "reviewapp_remark";
    protected $with = [
        'author',
    ];

    public function author() {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function submission() {
        return $this->belongsTo(Submission::class, 'submission_id', 'id');
    }

}