<?php

namespace ReviewApp;

use OpenTrials\OpenTrialsModel;

class News extends OpenTrialsModel {

    protected $table = "reviewapp_news";
    protected $with = [
        'translations',
    ];

    public function translations() {
        return $this->hasMany(NewsTranslation::class, 'object_id');
    }

}