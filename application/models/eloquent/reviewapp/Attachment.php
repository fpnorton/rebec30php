<?php

namespace ReviewApp;

use OpenTrials\OpenTrialsModel;

class Attachment extends OpenTrialsModel {

    protected $table = "reviewapp_attachment";

    public function trial() {
        return $this->belongsTo(Submission::class, 'submission_id', 'id');
    }

}