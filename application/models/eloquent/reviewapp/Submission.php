<?php

namespace ReviewApp;

use Auth\User;
use OpenTrials\FlaggedTrait;
use OpenTrials\OpenTrialsModel;
use Fossil\Fossil;
use Repository\ClinicalTrial;
use Vocabulary\CountryCode;

class Submission extends OpenTrialsModel {

    use FlaggedTrait;

    protected $table = "reviewapp_submission";
//    protected $with = [
//        'author',
//        'updater',
//        'remarks',
//        'trial',
//    ];

    public function fossils() {
        return $this->hasMany(Fossil::class, 'object_id', 'trial_id');
    }

    public function author() {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function updater() {
        return $this->belongsTo(User::class, 'updater_id', 'id');
    }

    public function remarks() {
        return $this->hasMany(Remark::class, 'submission_id', 'id');
    }

    public function trial() {
        return $this->belongsTo(ClinicalTrial::class, 'trial_id', 'id');
    }

    public function attachments() {
        return $this->hasMany(Attachment::class, 'submission_id', 'id');
    }

    public function recruitmentCountries() {
        return $this->belongsToMany(CountryCode::class, 'reviewapp_recruitmentcountry', 'submission_id', 'country_id');
    }

    public function getCreatedFormatted() {
        return $this->created;
    }

    public function isDraft() {
        return ($this->status == 'draft');
    }

    public function isPending() {
        return ($this->status == 'pending');
    }

    public function isApproved() {
        return ($this->status == 'approved');
    }

    public function isResubmit() {
        return ($this->status == 'resubmit');
    }

    public function revisor() {
        return ($this->remarks()->count() == 0 ? NULL : $this->remarks()->first()->author);
    }

    public function isObservacional() {
        return ($this->trial ? $this->trial->isObservacional() : false);
    }

    public function isIntervencional() {
        return ($this->trial ? $this->trial->isIntervencional() : false);
    }

    public function selectedLanguages() {
        if ($this->trial) return $this->trial->selectedLanguages();
        return [];
    }

    public function isPodeSerEnviado() {
        $fieldStatus = @json_decode($this->fields_status, TRUE);
        $fields = [
            ($fieldStatus['en'][TRIAL_FORMS_IDENTIFICATION] == FIELD_STATUS_COMPLETE),
            ($fieldStatus['en'][TRIAL_FORMS_ATTACHMENTS] == FIELD_STATUS_COMPLETE),
            ($fieldStatus['en'][TRIAL_FORMS_PRIMARY_SPONSOR] == FIELD_STATUS_COMPLETE),
            ($fieldStatus['en'][TRIAL_FORMS_HEALTH_CONDITIONS] == FIELD_STATUS_COMPLETE),
            ($fieldStatus['en'][TRIAL_FORMS_INTERVENTIONS] == FIELD_STATUS_COMPLETE),
            ($fieldStatus['en'][TRIAL_FORMS_RECRUITMENT] == FIELD_STATUS_COMPLETE),
            ($fieldStatus['en'][TRIAL_FORMS_STUDY_TYPE] == FIELD_STATUS_COMPLETE),
            ($fieldStatus['en'][TRIAL_FORMS_OUTCOMES] == FIELD_STATUS_COMPLETE),
            ($fieldStatus['en'][TRIAL_FORMS_CONTACTS] == FIELD_STATUS_COMPLETE),
            ($fieldStatus['en'][TRIAL_FORMS_SUMMARY_RESULTS] == FIELD_STATUS_COMPLETE),
            ($fieldStatus['en'][TRIAL_FORMS_IPD_SHARING_STATEMENT] == FIELD_STATUS_COMPLETE),
        ];
        error_log(
            @var_export([
                __FILE__ => __LINE__,
                'class Submission' => 'isPodeSerEnviado',
                '$fields' => $fields,
                '$field_status' => $field_status,
            ], TRUE)
        );

        $isPodeSerEnviado = TRUE;

        foreach ($fields as $field) $isPodeSerEnviado &= $field;

        return $isPodeSerEnviado;
    }

    public function isPodeSerRevisado() {
        $isPodeSerRevisado = $this->isPending();

        return $isPodeSerRevisado;
    }

    public function hasRemarks() {
        return ($this->remarks()->count() > 0);
    }

    public function field_status() {
        $field_status = [];
        if ($this->remarks->count() == 0) {
            $json_decoded = @json_decode($this->fields_status, TRUE);
            $field_status = $json_decoded['en'];
            error_log(
                @var_export([
                    __FILE__ => __LINE__,
                    'class Submission' => 'field_status',
                    '$this->remarks->count()' => $this->remarks->count(),
                    '$field_status' => $field_status,
                ], TRUE)
            );
        } else {
            $field_status = [
                'Trial Identification' => $this->remarks_as_status('trial-identification') ? 3 : 4,
                'Attachments' => $this->remarks_as_status('attachments') ? 3 : 4,
                'Sponsors' => $this->remarks_as_status('sponsors') ? 3 : 4,
                'Health Conditions' => $this->remarks_as_status('health-conditions') ? 3 : 4,
                'Interventions' => $this->remarks_as_status('interventions') ? 3 : 4,
                'Recruitment' => $this->remarks_as_status('recruitment') ? 3 : 4,
                'Study Type' => $this->remarks_as_status('study-type') ? 3 : 4,
                'Outcomes' => $this->remarks_as_status('outcomes') ? 3 : 4,
                'Contacts' => $this->remarks_as_status('contacts') ? 3 : 4,
                'Summary Results' => $this->remarks_as_status('summary-results') ? 3 : 4,
                'IPD Sharing Statement' => $this->remarks_as_status('ipd-sharing-statement') ? 3 : 4,
            ];
        }
        return $field_status;
    }

    public function remarks_as_status($context = NULL) {
        if ($context == NULL) return [];
        $statuses = ['open'];
        $collection = 
            $this->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                if (in_array($remark->status, $statuses)) {
                    return $remark;
                }
            });
        return $collection->sortByDesc('id')->count() > 0;
    }

}