<?php

namespace Audit;

use OpenTrials\OpenTrialsModel;

class Event extends OpenTrialsModel {

    protected $table = "audit_events";

    public function access() {
        return $this->hasMany(Access::class, 'event_id');
    }

    public function permissions() {
        return $this->hasMany(Permission::class, 'event_id');
    }

}