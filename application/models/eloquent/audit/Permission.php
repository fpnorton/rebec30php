<?php

namespace Audit;

use OpenTrials\OpenTrialsModel;

class Permission extends OpenTrialsModel {

    protected $table = "audit_permission";

}