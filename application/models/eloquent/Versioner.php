<?php
/**
 * Created by PhpStorm.
 * User: josue
 * Date: 08/04/19
 * Time: 05:43
 */

namespace Chaos;


use Auth\User;
use OpenTrials\OpenTrialsModel;
use Repository\ClinicalTrial;

class Versioner extends OpenTrialsModel
{
    protected $table = "versioner";

    public function author() {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function trial() {
        return $this->belongsTo(ClinicalTrial::class, 'trial_id', 'id');
    }

    public function data() {
        $json = json_decode($this->snapshot, TRUE);
        $data = [];
        foreach ($json as $key => $value) {
            $data[] = $key.'=>'.stripslashes($value);
        }
        return $data;
    }

}