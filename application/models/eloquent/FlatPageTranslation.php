<?php
/**
 * Created by PhpStorm.
 * User: josue
 * Date: 08/04/19
 * Time: 05:43
 */

namespace Django;


use OpenTrials\OpenTrialsModel;

class FlatPageTranslation extends OpenTrialsModel
{
    protected $table = "flatpages_polyglot_flatpagetranslation";

    public function contentType() {
        return $this->hasOne(ContentType::class, 'id', 'content_type_id');
    }

}