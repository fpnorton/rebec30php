<?php
namespace Steps;

defined('BASEPATH') OR exit('No direct script access allowed');

class ResumoDeResultados extends Base {

    private function web_date_to_iso($fieldvalue) {
        if (empty($fieldvalue)) return NULL;
        $date = explode('-', $fieldvalue);
        if (count($date) != 3) return NULL;
        if (checkdate($date[1], $date[2], $date[0]) == false) return NULL;
        return "{$date[0]}-{$date[1]}-{$date[2]} 12:00:00";
    }

    private function iso_to_web_date($fieldvalue) {
        if (empty($fieldvalue)) return '';
        $value = explode(' ', $fieldvalue);
        if (count($value) != 2) return '';
        $date = explode('-', $value[0]);
        if (count($date) != 3) return '';
        if (checkdate($date[1], $date[2], $date[0]) == false) return '';
        return "{$date[0]}-{$date[1]}-{$date[2]}";
    }

    public function SetStepData($id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $id,
            '$stepData' => $stepData,
        ]));
        $clinicalTrial = \Repository\ClinicalTrial::find($id);
        {
            $this->pack_date($stepData, 'results_date_posted');
            $this->pack_date($stepData, 'results_date_first_publication');
        }
        {
            $clinicalTrial = \Repository\ClinicalTrial::find($id);
            $clinicalTrial->update(
                [
                    'results_date_posted' => $stepData['results_date_posted'],
                    'results_date_first_publication' => $stepData['results_date_first_publication'],
                    'results_url_link' => $stepData['results_url_link'],
                    'results_url_protocol' => $stepData['results_url_protocol'],
                ]
            );
        }
        {
            $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);
            foreach($data['selected_languages'] as $language)
            {
                $data = [
                    'results_baseline_char' => $stepData['results_baseline_char'.'_'.$language],
                    'results_participant_flow' => $stepData['results_participant_flow'.'_'.$language],
                    'results_adverse_events' => $stepData['results_adverse_events'.'_'.$language],
                    'results_outcome_measure' => $stepData['results_outcome_measure'.'_'.$language],
                    'results_summary' => $stepData['results_summary'.'_'.$language],
                ];
                if ($language == 'en') {
                    $clinicalTrial->update($data);
                } else {
                    $translation = $clinicalTrial->translations->where('language', $language)->first();
                    $translation->update($data);
                }
            }
        }
        $clinicalTrial->push();
    }

    public function GetStepData($id)
    {
        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);

        {
            $data['results_date_posted'] = $this->iso_to_web_date($clinicalTrial->results_date_posted);
            $data['results_date_first_publication'] = $this->iso_to_web_date($clinicalTrial->results_date_first_publication);
            $data['results_url_link'] = $clinicalTrial->results_url_link;
            $data['results_url_protocol'] = $clinicalTrial->results_url_protocol;
        }
        {
            $extracted_results_date_posted = $this->extract_date($clinicalTrial->results_date_posted);
            // echo '<pre>'.var_export([
            //     $clinicalTrial->results_date_completed,
            //     $extracted_results_date_completed,
            // ], TRUE).'<pre>';
            $data['results_date_posted-d'] = $extracted_results_date_posted['day'];
            $data['results_date_posted-m'] = $extracted_results_date_posted['month'];
            $data['results_date_posted-y'] = $extracted_results_date_posted['year'];
        }
        {
            $extracted_results_date_first_publication = $this->extract_date($clinicalTrial->results_date_first_publication);
            // echo '<pre>'.var_export([
            //     $clinicalTrial->results_date_completed,
            //     $extracted_results_date_completed,
            // ], TRUE).'<pre>';
            $data['results_date_first_publication-d'] = $extracted_results_date_first_publication['day'];
            $data['results_date_first_publication-m'] = $extracted_results_date_first_publication['month'];
            $data['results_date_first_publication-y'] = $extracted_results_date_first_publication['year'];
        }
        { // idiomas
            $data['results_baseline_char_en'] = $clinicalTrial->results_baseline_char;
            $data['results_participant_flow_en'] = $clinicalTrial->results_participant_flow;
            $data['results_adverse_events_en'] = $clinicalTrial->results_adverse_events;
            $data['results_outcome_measure_en'] = $clinicalTrial->results_outcome_measure;
            $data['results_summary_en'] = $clinicalTrial->results_summary;
            foreach ($clinicalTrial->translations as $translation) {
                $data['results_baseline_char_'.$translation->language] = $translation->results_baseline_char;
                $data['results_participant_flow_'.$translation->language] = $translation->results_participant_flow;
                $data['results_adverse_events_'.$translation->language] = $translation->results_adverse_events;
                $data['results_outcome_measure_'.$translation->language] = $translation->results_outcome_measure;
                $data['results_summary_'.$translation->language] = $translation->results_summary;
            }
        }

        {
            $context = 'summary-results';
            $statuses = ['open', 'acknowledged'];
            if ($_SESSION['user']->isRevisor()) { $statuses[] = 'acknowledged'; $statuses[] = 'closed'; }
            $collection = 
                $clinicalTrial->submission()->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                    if (in_array($remark->status, $statuses)) {
                        return $remark;
                    }
                });
            $data['review_remark_lastest'] = 
                $collection->sortByDesc('id')->first();
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;
    }
}