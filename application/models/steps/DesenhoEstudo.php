<?php
namespace Steps;

defined('BASEPATH') OR exit('No direct script access allowed');

class DesenhoEstudo {


    public function SetStepData($id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $id,
            '$stepData' => $stepData,
        ]));

        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        if (@$clinicalTrial->study_type_id == NULL and $clinicalTrial->is_observational !== NULL) {
            // ajustando o campo de observacional
            $data = [
                'study_type_id' => ($clinicalTrial->is_observational == 1) ? 2 : 1,
            ];

            $clinicalTrial->update($data);

            $clinicalTrial = \Repository\ClinicalTrial::find($id);
        }

        $is_interventional = false;
        $is_observational  = false;

        {   // VALIDAÇÃO
            if (@$clinicalTrial->study_type_id == NULL) {
                $errors['messages'] = ['id_study_type' => 'rule_study_type_unknow'];
            } else {
                $is_interventional = ($clinicalTrial->study_type_id == 1);
                $is_observational  = ($clinicalTrial->study_type_id == 2);

                if ($is_interventional && empty($stepData['number_arms'])) {
                    $errors['messages'] = ['id_number_arms' => 'rule_interventional_study_number_arms'];
                }
            }
         
            if (count($errors) > 0) return $errors;
        }

        if ($is_interventional) {

            if ($stepData['expanded_access_program'] == 'U') {
                $stepData['expanded_access_program'] = 0;
            } else if ($stepData['expanded_access_program'] == 'Y') {
                $stepData['expanded_access_program'] = 1;
            } else if ($stepData['expanded_access_program'] == 'N') {
                $stepData['expanded_access_program'] = 2;
            }

            $data = [
                'expanded_access_program' => $stepData['expanded_access_program'],
                'purpose_id' => empty($stepData['study_purpose']) ? NULL : $stepData['study_purpose'],
                'number_of_arms' => $stepData['number_arms'],
                'masking_id' => empty($stepData['masking_type']) ? NULL : $stepData['masking_type'],
                'allocation_id' => empty($stepData['allocation_type']) ? NULL : $stepData['allocation_type'],
                'phase_id' => empty($stepData['study_phase']) ? NULL : $stepData['study_phase'],
                'intervention_assignment_id' => empty($stepData['intervention_assignment']) ? NULL : $stepData['intervention_assignment'],
            ];

            $clinicalTrial->update($data);
        }

        if ($is_observational) {

            $data = [
                'observational_study_design_id' => empty($stepData['observational_study_design']) ? NULL : $stepData['observational_study_design'],
                'time_perspective_id' => empty($stepData['time_perspective']) ? NULL : $stepData['time_perspective'],
            ];

            $clinicalTrial->update($data);
        }

    }

    public function GetStepData($id)
    {
        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);

        // na tela - 0 = U, 1 = Y, 2 = N
        $data['expanded_access_program'] = $clinicalTrial->expanded_access_program;

        $data['study_purpose'] = $clinicalTrial->purpose_id;
        $data['number_arms'] = $clinicalTrial->number_of_arms;
        $data['masking_type'] = $clinicalTrial->masking_id;
        $data['allocation_type'] = $clinicalTrial->allocation_id;
        $data['study_phase'] = $clinicalTrial->phase_id;
        $data['trial_id'] = $clinicalTrial->id;
        $data['observational_study_design'] = $clinicalTrial->observational_study_design_id;
        $data['time_perspective'] = $clinicalTrial->time_perspective_id;
        $data['intervention_assignment'] = $clinicalTrial->intervention_assignment_id;

        // ajustando o campo de observacional
        if (is_null($clinicalTrial->study_type_id)) {
            $data['study_type_id'] = ($clinicalTrial->is_observational == 1) ? 2 : 1;
        } else  {
            $data['study_type_id'] = $clinicalTrial->study_type_id;
        }

        $data['is_interventional'] = ($data['study_type_id'] == 1);
        $data['is_observational']  = ($data['study_type_id'] == 2);

        {
            $context = 'study-type';
            $statuses = ['open', 'acknowledged'];
            if ($_SESSION['user']->isRevisor()) { $statuses[] = 'acknowledged'; $statuses[] = 'closed'; }
            $collection = 
                $clinicalTrial->submission()->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                    if (in_array($remark->status, $statuses)) {
                        return $remark;
                    }
                });
            $data['review_remark_lastest'] = 
                $collection->sortByDesc('id')->first();
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;
    }

}