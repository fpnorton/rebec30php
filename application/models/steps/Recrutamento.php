<?php
namespace Steps;

defined('BASEPATH') OR exit('No direct script access allowed');

class Recrutamento extends Base {

    private function web_date_to_iso($fieldvalue) {
        if (empty($fieldvalue)) return NULL;
        $date = explode('-', $fieldvalue);
        if (count($date) != 3) {
            $date = explode('/', preg_replace("([^\/0-9])", '', $fieldvalue));
            if (count($date) == 3) {
                $_date = $date;
                if (strlen($_date[2]) == 4) $date[0] = $_date[2];
                if (strlen($_date[2]) == 4) $date[2] = $_date[0];
            }
        }
        if (count($date) != 3) return NULL;
        if (checkdate($date[1], $date[2], $date[0]) == false) return NULL;
        return "{$date[0]}-{$date[1]}-{$date[2]} 12:00:00";
    }

    private function iso_to_web_date($fieldvalue) {
        if (empty($fieldvalue)) return '';
        $value = explode(' ', $fieldvalue);
        if (count($value) != 2) return '';
        $date = explode('-', $value[0]);
        if (count($date) != 3) return '';
        if (checkdate($date[1], $date[2], $date[0]) == false) return '';
        return "{$date[0]}-{$date[1]}-{$date[2]}";
    }

    public function SetStepData($id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $id,
            '$stepData' => $stepData,
        ]));
        $clinicalTrial = \Repository\ClinicalTrial::find($id);
        {
            $this->pack_date($stepData, 'date_first_enrollment');
            $this->pack_date($stepData, 'date_last_enrollment');
        }
        {   // VALIDAÇÃO
            // a data do primeiro recrutamento não pode ser anterior a 02/01/2010
            $date = explode('-', $stepData['date_first_enrollment']);
            //if (count($date) != 3) return ['messages' => ['date_first_enrollment' => 'rule_date_first_enrollment_format']];
            if (intval($stepData['date_first_enrollment-y']) < 2010) return ['messages' => ['date_first_enrollment' => 'rule_date_first_enrollment_limit']];
        }

        {
            if ($stepData['minimum_age_no_limit'] == 'False') {
                $clinicalTrial->update([
                    'agemin_value' => empty($stepData['inclusion_minimum_age']) ? 0 : $stepData['inclusion_minimum_age'],
                    'agemin_unit' => $stepData['minimum_age_unit'],
                ]);
            } else {
                $clinicalTrial->update([
                    'agemin_value' => 0,
                    'agemin_unit' => '',
                ]);
            }
            if ($stepData['maximum_age_no_limit'] == 'False') {
                $clinicalTrial->update([
                    'agemax_value' => empty($stepData['inclusion_maximum_age']) ? 0 : $stepData['inclusion_maximum_age'],
                    'agemax_unit' => $stepData['maximum_age_unit'],
                ]);
            } else {
                $clinicalTrial->update([
                    'agemax_value' => 0,
                    'agemax_unit' => '',
                ]);
            }
        }
        {
            $countries = $clinicalTrial->recruitmentCountries()->get();
            foreach ($countries as $country) {
                if (in_array($country->id, $stepData['recruitment_countries'])) continue;
                 $country->pivot->delete();
            }
            foreach ($stepData['recruitment_countries'] as $recruitment_country_id) {
                $recruitment_country_id = intval($recruitment_country_id);
                $countryCode = \Vocabulary\CountryCode::find(intval($recruitment_country_id));

                $where = $clinicalTrial->recruitmentCountries()->where('countrycode_id', $recruitment_country_id);
                if ($where->count() == 0) {
                    $clinicalTrial->recruitmentCountries()->save($countryCode);
                }
            }
        }
        {
            $data = [
                'recruitment_status_id' => $stepData['study_status'],
                'enrollment_start_actual' => $stepData['date_first_enrollment'],
                'enrollment_end_actual' => $stepData['date_last_enrollment'],
                'enrollment_start_planned' => '',
                'enrollment_end_planned' => '',
                'gender' => $stepData['gender'] == 'B' ? '-' : $stepData['gender'],
                'inclusion_criteria' => $stepData['inclusion_criteria_en'],
                'exclusion_criteria' => $stepData['exclusion_criteria_en'],
            ];

            if ($stepData['target_size'] === 0 or empty($stepData['target_size']) == FALSE) {
                $data['target_sample_size'] = $stepData['target_size'];
            }

            $clinicalTrial->update($data);
        }
        {
            foreach($clinicalTrial->selectedLanguages(2) as $language)
            {
                if ($language != 'en') {
                    $translation = $clinicalTrial->translations->where('language', $language)->first();
                    $translation->update([
                        'inclusion_criteria' => $stepData['inclusion_criteria_'.$language],
                        'exclusion_criteria' => $stepData['exclusion_criteria_'.$language],
                    ]);
                }
            }
        }
    }

    public function GetStepData($id)
    {
        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        {
            $data['study_status'] = $clinicalTrial->recruitment_status_id;

            $data['date_first_enrollment'] = 
                !empty($clinicalTrial->enrollment_start_actual) ?
                $clinicalTrial->enrollment_start_actual :
                $clinicalTrial->enrollment_start_planned;

            {
                $extracted_date_first_enrollment = $this->extract_date($data['date_first_enrollment']);
                $data['date_first_enrollment-d'] = $extracted_date_first_enrollment['day'];
                $data['date_first_enrollment-m'] = $extracted_date_first_enrollment['month'];
                $data['date_first_enrollment-y'] = $extracted_date_first_enrollment['year'];
            }

            $data['date_last_enrollment'] = 
                !empty($clinicalTrial->enrollment_end_actual) ?
                $clinicalTrial->enrollment_end_actual :
                $clinicalTrial->enrollment_end_planned;
            
            {
                $extracted_date_last_enrollment = $this->extract_date($data['date_last_enrollment']);
                $data['date_last_enrollment-d'] = $extracted_date_last_enrollment['day'];
                $data['date_last_enrollment-m'] = $extracted_date_last_enrollment['month'];
                $data['date_last_enrollment-y'] = $extracted_date_last_enrollment['year'];
            }
    
            $data['gender'] = 
                ($clinicalTrial->gender == '-') ?
                    'B' :
                    $clinicalTrial->gender;

            $data['minimum_age_no_limit'] = $clinicalTrial->agemin_value == 0 ? 'True' : 'False';
            $data['inclusion_minimum_age'] = $clinicalTrial->agemin_value;
            $data['minimum_age_unit'] = $clinicalTrial->agemin_unit;

            $data['maximum_age_no_limit'] = $clinicalTrial->agemax_value == 0 ? 'True' : 'False';
            $data['inclusion_maximum_age'] = $clinicalTrial->agemax_value;
            $data['maximum_age_unit'] = $clinicalTrial->agemax_unit;

            $data['target_size'] = $clinicalTrial->target_sample_size;

            $data['inclusion_criteria_en'] = $clinicalTrial->inclusion_criteria;
            $data['exclusion_criteria_en'] = $clinicalTrial->exclusion_criteria;

            foreach ($clinicalTrial->recruitmentCountries()->get() as $recruitmentCountry) {
                $data['recruitment_countries'][] = $recruitmentCountry->id;
            }
            foreach ($clinicalTrial->translations()->get() as $translation) {
                $data['inclusion_criteria_'.$translation->language] = $translation->inclusion_criteria;
                $data['exclusion_criteria_'.$translation->language] = $translation->exclusion_criteria;
            }
        }
        {
            $context = 'recruitment';
            $statuses = ['open', 'acknowledged'];
            if ($_SESSION['user']->isRevisor()) { $statuses[] = 'acknowledged'; $statuses[] = 'closed'; }
            $collection = 
                $clinicalTrial->submission()->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                    if (in_array($remark->status, $statuses)) {
                        return $remark;
                    }
                });
            $data['review_remark_lastest'] = 
                $collection->sortByDesc('id')->first();
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;
    }

}