<?php
namespace Steps;

use Assistance\FieldHelp;
use Auth\User;
use Helper\FieldHelpLanguage;
use Repository\Contact;

defined('BASEPATH') OR exit('No direct script access allowed');

class Contatos extends Base {

    private function only_iso_date($input_date) {
        $date = urldecode($input_date);
        $exploded_by_minus = @explode('-', $date);
        if ( (count($exploded_by_minus) == 3) and (strlen($exploded_by_minus[0]) == 4) ) return $input_date;
        return NULL;
    }

    // private function clonar_contato($contact_id) {
    //     $contact = Contact::where('id','=', $contact_id)
    //         ->first();
    //     $data = $contact->withHidden(['creator_id'])->toArray();
    //     $contact = new Contact($data);
    //     $contact->save();
    //     return $contact->id;
    // }

    public function SetStepData($id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $id,
            '$stepData' => $stepData,
        ]));

        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);

        $contacts = [];

        for($index = 1; $index <= $stepData['count_contact']; $index++) {
            $contact_id = $stepData['ctcontact_set-' . $index . '-id'];
            $contact_contact_type = $stepData['ctcontact_set-' . $index . '-contact_type'];
            $contact_contact_id = $stepData['ctcontact_set-' . $index . '-contact_id'];
            $contact_delete = $stepData['ctcontact_set-' . $index . '-delete'];
            $contact_hash = $stepData['ctcontact_set-' . $index . '-hash'];
            $contact_hash_exploded = explode('+', $contact_hash);
            
            if (count($contact_hash_exploded) > 1) $contact_contact_id = $contact_hash_exploded[1];
            
            $this->pack_date($stepData, 'ctcontact_set-' . $index . '-date_approval');

            // $contact_extras = [
            //     'status_id' => $stepData['ctcontact_set-' . $index . '-status_id'],
            //     'date_approval' => $stepData['ctcontact_set-' . $index . '-date_approval'],
            // ];

            // echo '<pre>'.var_export([
            //     $contact_delete, $contact_id, $contact_contact_id, $contact_contact_type, $contact_extras
            // ], TRUE).'</pre>';die();
    
            // if (empty($contact_hash) == false) $this->contact_hash($clinicalTrial, $contact_delete, $contact_id, $contact_hash, $contact_contact_type, $contact_extras);
            // $this->handle_each_contact($clinicalTrial, $contact_delete, $contact_id, $contact_contact_id, $contact_contact_type, $contact_extras);

            if ($contact_delete === 'on') continue;

            $contacts[$contact_contact_type][$contact_contact_id] = [
                'id' => NULL,
                'status_id' => $stepData['ctcontact_set-' . $index . '-status_id'],
                'date_approval' => $stepData['ctcontact_set-' . $index . '-date_approval'],
            ];

        }
        // echo '<pre>'.var_export([
        //     '$stepData' => $stepData,
        //     '$contacts' => $contacts,
        // ], TRUE).'</pre>';
        // die();
        { // aqui ignoro o id de cada tipo e foco na combinacao de trial_id+contact_id
            $all = [
                'P' => (new \Repository\PublicContact())
                        ->newQueryWithoutScopes()
                        ->where('trial_id', '=', $id)
                        ->get(),
                'S' => (new \Repository\ScientificContact())
                        ->newQueryWithoutScopes()
                        ->where('trial_id', '=', $id)
                        ->get(),
                'W' => (new \Repository\SiteContact())
                        ->newQueryWithoutScopes()
                        ->where('trial_id', '=', $id)
                        ->get(),
                'E' => (new \Repository\EthicsReviewContact())
                        ->newQueryWithoutScopes()
                        ->where('trial_id', '=', $id)
                        ->get(),
            ];
            foreach(['P','S','W','E'] as $key_type) :
                $keys = array_keys($contacts[$key_type]);
                foreach($all[$key_type] as $value_object) {
                    $key_object = $value_object->contact->id;
                    $contacts[$key_type][$key_object]['id'] = $value_object->id;
                    if (in_array($key_object, $keys)) {
                        if ($key_type === 'E') {
                            $contact_extras = $contacts[$key_type][$key_object];
                            $value_object->update([
                                'ethicsreviewcontactstatus_id' => empty($contact_extras['status_id']) ? null : $contact_extras['status_id'],
                                'date_approval' => empty($contact_extras['date_approval']) ? null : $contact_extras['date_approval'],
                            ]);
                        }
                        if ($value_object->_deleted == 1) {
                            $value_object->update([
                                '_deleted' => 0,
                            ]);
                        }
                    } else {
                        if ($value_object->_deleted == 0) {
                            $value_object->update([
                                '_deleted' => 1,
                            ]);
                        }
                    }
                }
            endforeach;
            foreach(['P','S','W','E'] as $key_type) :
                foreach($contacts[$key_type] as $key_array => $value_array) {
                    if ($value_array['id'] != NULL) continue;
                    if ($key_type === 'P') {
                        $contact = new \Repository\PublicContact([
                            'contact_id' => $key_array, 
                            'status' => 'Active',
                        ]);
                        $clinicalTrial->publicContacts()->save($contact);
                    } else if ($key_type === 'S') {
                        $contact = new \Repository\ScientificContact([
                            'contact_id' => $key_array, 
                            'status' => 'Active',
                        ]);
                        $clinicalTrial->scientificContacts()->save($contact);
                    } else if ($key_type === 'W') {
                        $contact = new \Repository\SiteContact([
                            'contact_id' => $key_array, 
                            'status' => 'Active',
                        ]);
                        $clinicalTrial->siteContacts()->save($contact);
                    } else if ($key_type === 'E') {
                        $contact_extras = $value_array;
                        $contact = new \Repository\EthicsReviewContact([
                            'contact_id' => $key_array, 
                            'ethicsreviewcontactstatus_id' => empty($contact_extras['status_id']) ? null : $contact_extras['status_id'],
                            'date_approval' => empty($contact_extras['date_approval']) ? null : $contact_extras['date_approval'],
                        ]);
                        $clinicalTrial->ethicsReviewContacts()->save($contact);
                    }
                }
            endforeach;
        }
    }

    public function GetStepData($id)
    {
        $clinicalTrial = \Repository\ClinicalTrial::where('_deleted', '=', 0)
            ->where('id', '=', $id)
            ->first()
        ;

        $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);

        $index = 0;
        foreach ($clinicalTrial->publicContacts as $contact) {
            if ($contact->contact == NULL) continue;
            ++$index;
            $data['ctcontact_set-'.$index.'-object'] = $contact;
            $data['ctcontact_set-'.$index.'-id'] = $contact->id;
            $data['ctcontact_set-'.$index.'-contact_type'] = 'P';
            $data['ctcontact_set-'.$index.'-contact_id'] = $contact->contact->id;
            $data['ctcontact_set-'.$index.'-hash'] = $contact->contact->hashCode();
        }
        foreach ($clinicalTrial->scientificContacts as $contact) {
            if ($contact->contact == NULL) continue;
            ++$index;
            $data['ctcontact_set-'.$index.'-object'] = $contact;
            $data['ctcontact_set-'.$index.'-id'] = $contact->id;
            $data['ctcontact_set-'.$index.'-contact_type'] = 'S';
            $data['ctcontact_set-'.$index.'-contact_id'] = $contact->contact->id;
            $data['ctcontact_set-'.$index.'-hash'] = $contact->contact->hashCode();
        }
        foreach ($clinicalTrial->siteContacts as $contact) {
            if ($contact->contact == NULL) continue;
            ++$index;
            $data['ctcontact_set-'.$index.'-object'] = $contact;
            $data['ctcontact_set-'.$index.'-id'] = $contact->id;
            $data['ctcontact_set-'.$index.'-contact_type'] = 'W';
            $data['ctcontact_set-'.$index.'-contact_id'] = $contact->contact->id;
            $data['ctcontact_set-'.$index.'-hash'] = $contact->contact->hashCode();
        }
        foreach ($clinicalTrial->ethicsReviewContacts as $contact) {
            if ($contact->contact == NULL) continue;
            $extracted_date_approval = $this->extract_date($contact->date_approval);
            ++$index;
            $data['ctcontact_set-'.$index.'-object'] = $contact;
            $data['ctcontact_set-'.$index.'-id'] = $contact->id;
            $data['ctcontact_set-'.$index.'-contact_type'] = 'E';
            $data['ctcontact_set-'.$index.'-contact_id'] = $contact->contact->id;
            $data['ctcontact_set-'.$index.'-hash'] = $contact->contact->hashCode();
            $data['ctcontact_set-'.$index.'-status_id'] = $contact->status->id;
            $data['ctcontact_set-'.$index.'-date_approval'] = $contact->date_approval;
            $data['ctcontact_set-'.$index.'-date_approval-d'] = $extracted_date_approval['day'];
            $data['ctcontact_set-'.$index.'-date_approval-m'] = $extracted_date_approval['month'];
            $data['ctcontact_set-'.$index.'-date_approval-y'] = $extracted_date_approval['year'];
        }

        $data['count_contact'] = $index;

        {
            $context = 'contacts';
            $statuses = ['open', 'acknowledged'];
            if ($_SESSION['user']->isRevisor()) { $statuses[] = 'acknowledged'; $statuses[] = 'closed'; }
            $collection = 
                $clinicalTrial->submission()->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                    if (in_array($remark->status, $statuses)) {
                        return $remark;
                    }
                });
            $data['review_remark_lastest'] = 
                $collection->sortByDesc('id')->first();
        }
        // echo '<pre>'.var_export([
        //     '$data' => $data,
        // ], TRUE).'</pre>';
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;
    }

}