<?php
namespace Steps;

defined('BASEPATH') OR exit('No direct script access allowed');

class Identificacao {

    public function SetStepData($id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $id,
            '$stepData' => $stepData,
        ]));
        $clinicalTrial = \Repository\ClinicalTrial::find($id);
        {   // VALIDAÇÃO
            // $validation = [];
            // $errors = [];
            // // é obrigatorio ter a inserção de pelo menos um identificador
            // // o campo acrônimos não é obrigatório
            // $count = 0;
            // for($issuer_id = 1; $issuer_id < $stepData['count_secid']; $issuer_id++) {
            //     $secid = $stepData['identifier_set-' . $issuer_id . '-secid_id'];
            //     $values = [
            //         'id' => $issuer_id,
            //         'code' => $stepData['identifier_set-' . $issuer_id . '-code'],
            //         'description' => $stepData['identifier_set-' . $issuer_id . '-description'],
            //     ];
            //     if (empty($values['code']) || empty($values['description'])) continue;
            //     $count++;
            // }
            // if ($count == 0) return ['messages' => ['identifier_set-0-secid_id' => 'rule_identification_less_one']];
        }
        $clinicalTrial->update(
            [
                'scientific_title' => $stepData['scientific_title_en'] ?? '',
                'public_title' => $stepData['public_title_en'] ?? '',
                'scientific_acronym' => $stepData['scientific_acronym_en'] ?? '',
                'scientific_acronym_expansion' => $stepData['scientific_acronym_expansion_en'] ?? '',
                'acronym' => $stepData['acronym_en'] ?? '',
                'acronym_expansion' => $stepData['acronym_expansion_en'] ?? '',
            ]
        );
        foreach($clinicalTrial->selectedLanguages() as $language) {
            $eloquentCollection = $clinicalTrial->translations->where('language',  $language['language']);
            if ($eloquentCollection->count() == 0) continue;
            $trialTranslation = $eloquentCollection->shift();
            $trialTranslation->update(
                [
                    'scientific_title' => $stepData['scientific_title_'.$language['language']] ?? '',
                    'public_title' => $stepData['public_title_'.$language['language']] ?? '',
                    'scientific_acronym' => $stepData['scientific_acronym_'.$language['language']] ?? '',
                    'scientific_acronym_expansion' => $stepData['scientific_acronym_expansion_'.$language['language']] ?? '',
                    'acronym' => $stepData['acronym_'.$language['language']] ?? '',
                    'acronym_expansion' => $stepData['acronym_expansion_'.$language['language']] ?? '',
                ]
            );
        }
        for($secid = 1; $secid <= $stepData['count_secid']; $secid++) {
            $values = [
                'id' => $stepData['identifier_set-'.$secid.'-issuer_id'],
                'identifier' => $stepData['identifier_set-'.$secid.'-identifier'],
                'code' => $stepData['identifier_set-'.$secid.'-code'],
                'description' => $stepData['identifier_set-'.$secid.'-description'],
                'delete' => @$stepData['identifier_set-'.$secid.'-delete'],
            ];
            if (empty($values['identifier'])) $values['identifier'] = '4';
            if (empty($values['delete']) == false) { // delete
                if (empty($values['id'])) continue;
                if ($values['id'] > 0) {
                    $trialNumber = \Repository\TrialNumber::find($values['id']);
                    $trialNumber->update([
                        '_deleted' => 1
                    ]);
                } else {
                    $clinicalTrial->update(
                        [
                            'utrn_number' => '',
                        ]
                    );
                }
            } elseif (empty($values['id'])) { // insert
                if (empty($values['code'])) continue;
                // if (empty($values['description'])) continue;
                if ($values['identifier'] == 2) {
                    $clinicalTrial->update(
                        [
                            'utrn_number' => $values['code'],
                        ]
                    );
                } else {
                    $trialNumber = new \Repository\TrialNumber(
                        [
                            'id_number' => $values['code'],
                            'issuing_authority' => $values['description'],
                            'id_identifier' => $values['identifier'],
                        ]
                    );
                    $clinicalTrial->numbers()->save($trialNumber);
                }
            } elseif ($values['id'] > 0)  { //update
                $trialNumber = $clinicalTrial->numbers->find($values['id']);
                if ($values['identifier'] == 2) { // UTN como secundário?
                    $clinicalTrial->update(
                        [
                            'utrn_number' => $values['code'],
                        ]
                    );
                    $trialNumber->update([
                        '_deleted' => 1
                    ]);
                } else {
                    $trialNumber->update(
                        [
                            'id_number' => $values['code'],
                            'issuing_authority' => $values['description'],
                            'id_identifier' => $values['identifier'],
                        ]
                    );
                }
            } elseif ($values['identifier'] == 2)  { // clinicaltrial
                $clinicalTrial->update(
                    [
                        'utrn_number' => $values['code'],
                    ]
                );
            }
        }
        $clinicalTrial->push();
    }

	public function GetStepData($id)
    {
        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $data['selected_languages'] = $clinicalTrial->selectedLanguages();

        {
            foreach ([
                 'scientific_title',
                 'public_title',
                 'scientific_acronym',
                 'scientific_acronym_expansion',
                 'acronym',
                 'acronym_expansion',
                 ] as $key) {
                $data[$key.'_en'] = $clinicalTrial->$key;
                foreach ($clinicalTrial->translations as $translation) {
                    $data[$key.'_'.$translation->language] = $translation->$key;
                }
            }

            // for ($i = 1; $i < 4; $i++) {
            //     $data['identifier_set-count'] = $i;
            //     $data['identifier_set-'.$i.'-count'] = 0;
            //     $data['identifier_set-'.$i.'-secid_id'] = $i;
            //     $data['identifier_set-'.$i.'-identifier'] = $i;
            //     $data['identifier_set-'.$i.'-issuer_id'] = '';
            //     $data['identifier_set-'.$i.'-code'] = '';
            //     $data['identifier_set-'.$i.'-description'] = '';
            // }
            // {
            //     if ($clinicalTrial->utrn_number != NULL) { // UTN
            //         $data['identifier_set-2-count'] = 1;
            //         $data['identifier_set-2-code'] = $clinicalTrial->utrn_number;
            //         $data['identifier_set-2-description'] = 'Universal Trial Number';
            //     }
            // }
            {
                $i = 1;
                if ($clinicalTrial->utrn_number != NULL) { // UTN
                    $data['identifier_set-count'] = $i;
                    $data['identifier_set-'.$i.'-count'] = 1;
                    $data['identifier_set-'.$i.'-secid_id'] = $i;
                    $data['identifier_set-'.$i.'-identifier'] = 2;
                    $data['identifier_set-'.$i.'-issuer_id'] = -1;
                    $data['identifier_set-'.$i.'-code'] = $clinicalTrial->utrn_number;
                    $data['identifier_set-'.$i.'-description'] = 'Universal Trial Number';
                }
            }
            foreach ($clinicalTrial->numbers as $number) {
                // $count = $data['identifier_set-'.($number->id_identifier).'-count'];
                // if ($count == 0) {
                //     $index = $number->id_identifier;
                // } else {
                //     $i++;
                //     $index = $i;
                // }
                // $data['identifier_set-'.($number->id_identifier).'-count']++;
                // $data['identifier_set-count']++;
                $index = ++$data['identifier_set-count'];
                $data['identifier_set-'.$index.'-secid_id'] = $index;
                $data['identifier_set-'.$index.'-identifier'] = $number->id_identifier;
                $data['identifier_set-'.$index.'-issuer_id'] = $number->id;
                $data['identifier_set-'.$index.'-code'] = $number->id_number;
                $data['identifier_set-'.$index.'-description'] = $number->issuing_authority;
            }
        }
        {
            $context = 'trial-identification';
            $statuses = ['open', 'acknowledged'];
            if ($_SESSION['user']->isRevisor()) { $statuses[] = 'acknowledged'; $statuses[] = 'closed'; }
            $collection = 
                $clinicalTrial->submission()->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                    if (in_array($remark->status, $statuses)) {
                        return $remark;
                    }
                });
            $data['review_remark_lastest'] = 
                $collection->sortByDesc('id')->first();
        }

        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;
	}

}