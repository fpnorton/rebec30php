<?php
namespace Steps;

use Repository\ClinicalTrial;
use Repository\Descriptor;
use Repository\PublicContact;
use Repository\ScientificContact;
use Repository\SecondarySponsor;
use Repository\SiteContact;
use Repository\SupportSource;
use ReviewApp\Attachment;

defined('BASEPATH') OR exit('No direct script access allowed');

class Sumario {

    public function SetStepData($clinicaltrial_id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $clinicaltrial_id,
            '$stepData' => $stepData,
        ]));
        {
            $data['clinicaltrial'] =  \Repository\ClinicalTrial::find($clinicaltrial_id);
            $data['submission'] = $data['clinicaltrial']->submission();
        }
        {
            foreach($data['clinicaltrial']->selectedLanguages() as $language) {
                if ($language['language'] != 'pt-br') continue;
                $eloquentCollection = $data['clinicaltrial']->translations->where('language',  $language['language']);
                if ($eloquentCollection->count() == 0) continue;
                $trialTranslation = $eloquentCollection->shift();
                $data['submission']->update([
                    'title' => $trialTranslation->public_title,
                ]);
            }
        }
        { // fields status
            if (empty($data['submission']->fields_status)) {

                foreach ($data['clinicaltrial']->selectedLanguages() as $language) {
                    $data['fieldStatus'][$language['language']] = [
                        TRIAL_FORMS_IDENTIFICATION =>       FIELD_STATUS_PARTIAL,
                        TRIAL_FORMS_PRIMARY_SPONSOR =>      FIELD_STATUS_MISSING,
                        TRIAL_FORMS_HEALTH_CONDITIONS =>    FIELD_STATUS_MISSING,
                        TRIAL_FORMS_INTERVENTIONS =>        FIELD_STATUS_MISSING,
                        TRIAL_FORMS_RECRUITMENT =>          FIELD_STATUS_MISSING,
                        TRIAL_FORMS_STUDY_TYPE =>           FIELD_STATUS_MISSING,
                        TRIAL_FORMS_OUTCOMES =>             FIELD_STATUS_MISSING,
                        TRIAL_FORMS_CONTACTS =>             FIELD_STATUS_MISSING,
                        TRIAL_FORMS_ATTACHMENTS =>          FIELD_STATUS_MISSING,
                        TRIAL_FORMS_SUMMARY_RESULTS =>      FIELD_STATUS_COMPLETE,
                        TRIAL_FORMS_IPD_SHARING_STATEMENT => FIELD_STATUS_COMPLETE,
                    ];
                }

            } else {
                $data['fieldStatus'] = @json_decode($data['submission']->fields_status, TRUE);
            }
        }
        {   // IDENTIFICACAO
            foreach ($this->SetIdentificacao($data['clinicaltrial'], $data['submission']) as $language => $value) {
                $data['fieldStatus'][$language][TRIAL_FORMS_IDENTIFICATION] = $value;
            }
        }
        {   // ANEXOS
            foreach ($this->SetAnexos($data['clinicaltrial'], $data['submission']) as $language => $value) {
                $data['fieldStatus'][$language][TRIAL_FORMS_ATTACHMENTS] = $value;
            }
        }
        {   // PATROCINADORES
            foreach ($this->SetPatrocinadores($data['clinicaltrial'], $data['submission']) as $language => $value) {
                $data['fieldStatus'][$language][TRIAL_FORMS_PRIMARY_SPONSOR] = $value;
            }
        }
        {   // CONDICOES SAUDE
            foreach ($this->SetCondicoesDeSaude($data['clinicaltrial'], $data['submission']) as $language => $value) {
                $data['fieldStatus'][$language][TRIAL_FORMS_HEALTH_CONDITIONS] = $value;
            }
        }
        {   // INTERVENCIONAL
            foreach ($this->SetIntervencional($data['clinicaltrial'], $data['submission']) as $language => $value) {
                $data['fieldStatus'][$language][TRIAL_FORMS_INTERVENTIONS] = $value;
            }
        }
        {   // RECRUTAMENTO
            foreach ($this->SetRecrutamento($data['clinicaltrial'], $data['submission']) as $language => $value) {
                $data['fieldStatus'][$language][TRIAL_FORMS_RECRUITMENT] = $value;
            }
        }
        {   // DESENHO DO ESTUDO
            foreach ($this->SetDesenhoDeEstudo($data['clinicaltrial'], $data['submission']) as $language => $value) {
                $data['fieldStatus'][$language][TRIAL_FORMS_STUDY_TYPE] = $value;
            }
        }
        {   // DESFECHOS
            foreach ($this->SetDesfechos($data['clinicaltrial'], $data['submission']) as $language => $value) {
                $data['fieldStatus'][$language][TRIAL_FORMS_OUTCOMES] = $value;
            }
        }
        {   // CONTATOS
            foreach ($this->SetContatos($data['clinicaltrial'], $data['submission']) as $language => $value) {
                $data['fieldStatus'][$language][TRIAL_FORMS_CONTACTS] = $value;
            }
        }
        {
            foreach ($data['clinicaltrial']->selectedLanguages() as $language) {
                $data['fieldStatus'][$language][TRIAL_FORMS_SUMMARY_RESULTS] = FIELD_STATUS_COMPLETE;
            }
        }
        {
            foreach ($data['clinicaltrial']->selectedLanguages() as $language) {
                $data['fieldStatus'][$language][TRIAL_FORMS_IPD_SHARING_STATEMENT] = FIELD_STATUS_COMPLETE;
            }
        }

        $data['submission']->update([
            'fields_status' => @json_encode($data['fieldStatus']),
            'updated' => date('Y-m-d H:i:s'),
            'updater_id' => @$data['current_user_id'],
            'status' => 'draft',
        ]);
        $data['clinicaltrial']->update([
            'updated' => date('Y-m-d H:i:s'),
        ]);

//        unset($data['clinicaltrial']);
//        unset($data['submission']);
//        unset($data['healthcondition']);
//        unset($data['intervention']);
    }

    private function SetContatos($clinicaltrial, $submission)
    {   // CONTATOS

        $fieldStatus = [];
        $status =
            (PublicContact::where('trial_id', '=', $clinicaltrial->id)->get()->count() > 0) &&
            (ScientificContact::where('trial_id', '=', $clinicaltrial->id)->get()->count() > 0) &&
            (SiteContact::where('trial_id', '=', $clinicaltrial->id)->get()->count() > 0)
        ;
        error_log(json_encode([
            __FILE__ => __LINE__,
            'Sumario->SetContatos' => $clinicaltrial->id, 
            '$status' => $status, 
        ]));
        $fieldStatus['en'] = $status ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL;
        foreach ($clinicaltrial->translations as $translation) {
            $fieldStatus[$translation->language] = $status ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL;
        }
        return $fieldStatus;
    }


    private function SetDesenhoDeEstudo(ClinicalTrial $clinicalTrial, $submission)
    {   // DESENHO DO ESTUDO

        $fieldStatus = [];

        if ($clinicalTrial->isObservacional()) {
            $status['en'] =
                !empty($clinicalTrial->observational_study_design_id) &&
                !empty($clinicalTrial->time_perspective_id);
        } else {
            $status['en'] =
                !empty($clinicalTrial->phase_id) &&
                !empty($clinicalTrial->allocation_id) &&
                !empty($clinicalTrial->masking_id) &&
                !empty($clinicalTrial->number_of_arms) &&
                !empty($clinicalTrial->intervention_assignment_id) &&
                !empty($clinicalTrial->purpose_id)
            ;
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
            'Sumario->SetDesenhoDeEstudo' => $clinicaltrial->id, 
            '$clinicalTrial->phase_id' =>$clinicalTrial->phase_id,
            '$clinicalTrial->allocation_id' => $clinicalTrial->allocation_id,
            '$clinicalTrial->masking_id' => $clinicalTrial->masking_id,
            '$clinicalTrial->number_of_arms' => $clinicalTrial->number_of_arms,
            '$clinicalTrial->intervention_assignment_id' => $clinicalTrial->intervention_assignment_id,
            '$clinicalTrial->purpose_id' => $clinicalTrial->purpose_id,
            '$clinicalTrial->expanded_access_program' => $clinicalTrial->expanded_access_program,
            '$status[en]' => $status['en'],
        ]));
        {
            $fieldStatus['en'] =
                $status['en'] ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL
            ;
            foreach ($clinicalTrial->translations as $translation) {
                $fieldStatus[$translation->language] = $status['en'] ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL;
            }
        }
        return $fieldStatus;
    }

    private function SetDesfechos($clinicaltrial, $submission)
    {   // DESFECHOS

        $fieldStatus = [];

        // todos os campos precisam estar preenchidos
        $status['en'] = ($clinicaltrial->outcomes->count() >  0);
        foreach ($clinicaltrial->translations as $translation) {
            $status[$translation->language] = ($clinicaltrial->outcomes->count() >  0);
        }

        foreach ($clinicaltrial->outcomes as $outcome) {
            $status['en'] &= !empty($outcome->description);
            foreach ($outcome->translations as $translation) {
                $status[$translation->language] &= !empty($translation->description);
            }
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
            'Sumario->SetDesfechos' => $clinicaltrial->id, 
            '$status' => $status, 
        ]));
        foreach ($status  as $language => $status) {
            $fieldStatus[$language] = ($status ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL);
        }
        return $fieldStatus;
    }

    private function SetRecrutamento($clinicaltrial, $submission)
    {   // RECRUTAMENTO

        $fieldStatus = [];

        $status['en'] =
            !empty($clinicaltrial->inclusion_criteria) &&
            !empty($clinicaltrial->exclusion_criteria) &&
            !empty($clinicaltrial->enrollment_start_actual) &&
            !empty($clinicaltrial->enrollment_end_actual) &&
            !empty($clinicaltrial->gender) &&
            !empty($clinicaltrial->target_sample_size)
        ;
        foreach ($clinicaltrial->translations as $translation) {
            $status[$translation->language] =
                !empty($translation->inclusion_criteria) &&
                !empty($translation->exclusion_criteria)
            ;
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
            'Sumario->SetRecrutamento' => $clinicaltrial->id, 
            '$status' => $status, 
        ]));
        {
            $fieldStatus['en'] =
                $status['en'] ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL
            ;
            foreach ($status as $language => $statuses) {
                $fieldStatus[$language] = $statuses ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL;
            }
        }
        return $fieldStatus;
    }

    private function SetIntervencional($clinicaltrial, $submission)
    {   // INTERVENCIONAL

        $fieldStatus = [];
        $intervention = [];
        $descriptors = Descriptor::where('trial_id', '=', $clinicaltrial->id)->where('aspect', '=', 'Intervention')->get();
        foreach ($descriptors as $descriptor) {
            $intervention[$descriptor->level][] = $descriptor;
        }
        $status['en'] =
            !empty($clinicaltrial->i_freetext)
        ;
        foreach ($clinicaltrial->translations as $translation) {
            $status[$translation->language] =
                !empty($translation->i_freetext)
            ;
        }
        foreach ($intervention as $level => $descriptors) {

            foreach ($descriptors as $descriptor) {

//                NAO NECESSARIO
//                $status['en'] &=
//                    !empty($descriptor->vocabulary) &&
//                    !empty($descriptor->code)
//                ;

                foreach ($descriptor->translations as $translation) {
                    $status[$translation->language] &=
                        !empty($translation->text)
                    ;
                }

            }
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
            'Sumario->SetIntervencional' => $clinicaltrial->id, 
            '$status' => $status, 
        ]));
        {
            $fieldStatus['en'] =
                $status['en'] ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL;
            foreach ($status as $language => $statuses) {
                $fieldStatus[$language] = $statuses ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL;
            }
        }
        return $fieldStatus;
    }


    private function SetCondicoesDeSaude($clinicaltrial, $submission)
    {   // CONDICOES SAUDE hc_code: general; hc_keyword: specific

        $fieldStatus = [];
        $healthcondition = [];
        $descriptors = Descriptor::where('trial_id', '=', $clinicaltrial->id)->where('aspect', '=', 'HealthCondition')->get();
        foreach ($descriptors as $descriptor) {
            $healthcondition[$descriptor->level][] = $descriptor;
        }
        $status['en'] =
            !empty($clinicaltrial->hc_freetext)
        ;
        foreach ($clinicaltrial->translations as $translation) {
            $status[$translation->language] =
                !empty($translation->hc_freetext)
            ;
        }
        foreach ($healthcondition as $level => $descriptors) {

            foreach ($descriptors as $descriptor) {

//                $status['en'] &=
//                    !empty($descriptor->vocabulary) &&
//                    !empty($descriptor->code)
//                ;
                foreach ($descriptor->translations as $translation) {
                    $status[$translation->language] &=
                        !empty($translation->text)
                    ;
                }

            }
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
            'Sumario->SetCondicoesDeSaude' => $clinicaltrial->id, 
            '$status' => $status, 
        ]));
        {
            foreach ($status as $language => $statuses) {
                $fieldStatus[$language] = $statuses ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL;
            }
        }
        return $fieldStatus;
    }

    private function SetPatrocinadores($clinicaltrial, $submission)
    {   // PATROCINADORES

        $status =
            !empty($clinicaltrial->primary_sponsor_id) &&
            (SecondarySponsor::where('trial_id', '=', $clinicaltrial->id)->get()->count() > 0) &&
            (SupportSource::where('trial_id', '=', $clinicaltrial->id)->get()->count() > 0)
        ;
        error_log(json_encode([
            __FILE__ => __LINE__,
            'Sumario->SetPatrocinadores' => $clinicaltrial->id, 
            '$status' => $status, 
        ]));
        $fieldStatus['en'] =
            $status ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL
        ;
        foreach ($clinicaltrial->translations as $translation) {
            $fieldStatus[$translation->language] =
                $status ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL
            ;
        }
        return $fieldStatus;
    }

    private function SetAnexos($clinicaltrial, $submission)
    {   // ANEXOS
        $fieldStatus = [];
        $status =
            (Attachment::where('submission_id', '=', $submission->id)->get()->count() > 0);
        error_log(json_encode([
            __FILE__ => __LINE__,
            'Sumario->SetAnexos' => $clinicaltrial->id, 
            '$status' => $status, 
        ]));
        $fieldStatus['en'] =
            $status ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL
        ;
        foreach ($clinicaltrial->translations as $translation) {
            $fieldStatus[$translation->language] =
                $status ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL
            ;
        }
        return $fieldStatus;
    }

    private function SetIdentificacao($clinicaltrial, $submission)
    {   // IDENTIFICACAO
        $fieldStatus = [];
        $status['en'] =
            !empty($clinicaltrial->scientific_title) &&
            !empty($clinicaltrial->public_title)
        ;
        error_log(json_encode([
            __FILE__ => __LINE__,
            'Sumario->SetIdentificacao' => $clinicaltrial->id, 
            '$status' => $status, 
        ]));
        $fieldStatus['en'] =
            $status['en'] ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL;
        foreach ($clinicaltrial->translations as $translation) {
            $status[$translation->language] =
                !empty($translation->scientific_title) &&
                !empty($translation->public_title)
            ;
            $fieldStatus[$translation->language] =
                $status[$translation->language] ? FIELD_STATUS_COMPLETE : FIELD_STATUS_PARTIAL;
        }
        return $fieldStatus;
    }

    public function GetStepData($id)
    {
        $data = [];
        $clinicalTrial = \Repository\ClinicalTrial::find($id);
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;
    }

}