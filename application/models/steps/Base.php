<?php
namespace Steps;

defined('BASEPATH') OR exit('No direct script access allowed');

class Base {

    function pack_date(&$stepData, $key) {
        if (@checkdate($stepData[$key.'-m'], $stepData[$key.'-d'], $stepData[$key.'-y'])) {
            $stepData[$key] = implode('-', [
                $stepData[$key.'-y'],
                str_pad($stepData[$key.'-m'], 2, '0', STR_PAD_LEFT),
                str_pad($stepData[$key.'-d'], 2, '0', STR_PAD_LEFT),
            ]);
            if (strlen($stepData[$key]) == 10) return TRUE;
        }
        $stepData[$key] = NULL;
        return FALSE;
    }

    function extract_date($date) {
        if ($date != null or $date != '') {
            $extracted = $this->_extract_date($date);
            if (@checkdate($extracted['month'], $extracted['day'], $extracted['year'])) {
                return $extracted;
            }
        }
        return [
            'year' => '',
            'month' => '',
            'day' => '',
        ];
    }

    private function _extract_date($date) {
        if ($date != null or $date != '') {
            $date = urldecode($date);
            $length = @strlen($date);

            $exploded_by_space = @explode(' ', $date);
            $exploded_by_T = @explode('T', $date);

            if (count($exploded_by_space) > 1) {
                $date = $exploded_by_space[0];
                $length = @strlen($date);
            } elseif (count($exploded_by_T) > 1) {
                $date = $exploded_by_T[0];
                $length = @strlen($date);
            }

            $exploded_by_minus = @explode('-', $date);
            $exploded_by_slash = @explode('/', $date);
            // $exploded_by_space = @explode(' ', $date);
            // $exploded_by_t = @explode('T', $date);
            // $exploded_by_twopoints = @explode(':', $date);
            $boolean_is_iso_date_yyyy_mm_dd = ($length == 10 and count($exploded_by_minus) == 3 and strlen($exploded_by_minus[0]) == 4 and intval($exploded_by_slash[1]) <= 12);
            $boolean_is_iso_date_dd_mm_yyyy = ($length == 10 and count($exploded_by_minus) == 3 and strlen($exploded_by_minus[2]) == 4 and intval($exploded_by_slash[1]) <= 12);
            $boolean_is_iso_date_mm_dd_yyyy = ($length == 10 and count($exploded_by_minus) == 3 and strlen($exploded_by_minus[2]) == 4 and intval($exploded_by_slash[0]) <= 12);
            $boolean_is_british_date = ($length == 10 and count($exploded_by_slash) == 3 and strlen($exploded_by_slash[2]) == 4 and intval($exploded_by_slash[1]) <= 12);
            $boolean_is_english_date = ($length == 10 and count($exploded_by_slash) == 3 and strlen($exploded_by_slash[2]) == 4 and intval($exploded_by_slash[0]) <= 12);
            
            $boolean_is_british_date_yy = ($length == 8 and count($exploded_by_slash) == 3 and strlen($exploded_by_slash[2]) == 2 and intval($exploded_by_slash[1]) <= 12);
            $boolean_is_english_date_yy = ($length == 8 and count($exploded_by_slash) == 3 and strlen($exploded_by_slash[2]) == 2 and intval($exploded_by_slash[0]) <= 12);

            $boolean_is_ddmmyyyy = ($length == 8 and intval(substr($date, 2, 2)) <= 12);
            $boolean_is_mmddyyyy = ($length == 8 and intval(substr($date, 0, 2)) <= 12);

            // echo '<pre>'.var_export([
            //     '>'.$date.'<',
            //     substr($date, 0, 2),
            //     substr($date, 2, 2),
            //     substr($date, 4, 4),
            //     $boolean_is_iso_date,
            //     $boolean_is_british_date,
            //     $boolean_is_english_date
            // ], TRUE).'</pre>';
            if ($boolean_is_iso_date_yyyy_mm_dd) {
                return [
                    'year' => $exploded_by_minus[0],
                    'month' => $exploded_by_minus[1],
                    'day' => $exploded_by_minus[2],
                ];
            } else if ($boolean_is_iso_date_dd_mm_yyyy) {
                return [
                    'year' => $exploded_by_minus[2],
                    'month' => $exploded_by_minus[1],
                    'day' => $exploded_by_minus[0],
                ];
            } else if ($boolean_is_iso_date_mm_dd_yyyy) {
                return [
                    'year' => $exploded_by_minus[2],
                    'month' => $exploded_by_minus[0],
                    'day' => $exploded_by_minus[1],
                ];
            } else if ($boolean_is_british_date) {
                return [
                    'year' => $exploded_by_slash[2],
                    'month' => $exploded_by_slash[1],
                    'day' => $exploded_by_slash[0],
                ];
            } else if ($boolean_is_british_date_yy) {
                return [
                    'year' => '20'.$exploded_by_slash[2],
                    'month' => $exploded_by_slash[1],
                    'day' => $exploded_by_slash[0],
                ];
            } else if ($boolean_is_english_date) {
                return [
                    'year' => $exploded_by_slash[2],
                    'month' => $exploded_by_slash[0],
                    'day' => $exploded_by_slash[1],
                ];
            } else if ($boolean_is_english_date_yy) {
                return [
                    'year' => '20'.$exploded_by_slash[2],
                    'month' => $exploded_by_slash[0],
                    'day' => $exploded_by_slash[1],
                ];
            } else if ($boolean_is_ddmmyyyy) {
                return [
                    'year' => substr($date, 4, 4),
                    'month' => substr($date, 2, 2),
                    'day' => substr($date, 0, 2),
                ];
            } else if ($boolean_is_mmddyyyy) {
                return [
                    'year' => substr($date, 4, 4),
                    'month' => substr($date, 0, 2),
                    'day' => substr($date, 2, 2),
                ];
            }
        }
        return [
            'year' => '',
            'month' => '',
            'day' => '',
        ];
    }

}
