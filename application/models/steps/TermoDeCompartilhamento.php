<?php
namespace Steps;

defined('BASEPATH') OR exit('No direct script access allowed');

class TermoDeCompartilhamento {

    public function SetStepData($id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $id,
            '$stepData' => $stepData,
        ]));
        $clinicalTrial = \Repository\ClinicalTrial::find($id);
        if (!empty($stepData['results_ipd_plan_id'])) {
            $data = [
                'results_IPD_plan_id' => $stepData['results_ipd_plan_id'],
            ];
            $clinicalTrial->update($data);
        }
        {
            $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);
            foreach($data['selected_languages'] as $language)
            {
                if ($language == 'en') {
                    $clinicalTrial->update([
                        'results_IPD_description' => $stepData['results_ipd_description_'.$language],
                    ]);
                } else {
                    $translation = $clinicalTrial->translations->where('language', $language)->first();
                    $translation->update([
                        'results_IPD_description' => $stepData['results_ipd_description_'.$language],
                    ]);
                }
            }
        }
        $clinicalTrial->push();
    }

    public function GetStepData($id)
    {
        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);

        $data['results_ipd_plan_id'] = $clinicalTrial->results_IPD_plan_id;

        {
            $data['results_ipd_description_en'] = $clinicalTrial->results_IPD_description;
            foreach ($clinicalTrial->translations as $translation) {
                $data['results_ipd_description_'.$translation->language] = $translation->results_IPD_description;
            }
        }
        {
            $context = 'ipd-sharing-statement';
            $statuses = ['open', 'acknowledged'];
            if ($_SESSION['user']->isRevisor()) { $statuses[] = 'acknowledged'; $statuses[] = 'closed'; }
            $collection = 
                $clinicalTrial->submission()->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                    if (in_array($remark->status, $statuses)) {
                        return $remark;
                    }
                });
            $data['review_remark_lastest'] = 
                $collection->sortByDesc('id')->first();
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;
    }
}