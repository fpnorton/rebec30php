<?php
namespace Steps;

defined('BASEPATH') OR exit('No direct script access allowed');

class CondicoesSaude {

    public function SetStepData($id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $id,
            '$stepData' => $stepData,
        ]));

        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);

        foreach($data['selected_languages'] as $language)
        {
            if ($language == 'en') {
                $clinicalTrial->update([
                    'hc_freetext' => $stepData['health_condition_'.$language]
                ]);
            } else {
                $translation = $clinicalTrial->translations->where('language', $language)->first();
                $translation->update([
                    'hc_freetext' => $stepData['health_condition_'.$language]
                ]);
            }
        }
        for($count = 1; $count <= $stepData['count_hc']; $count++) {

            $descriptor_id = $stepData['descriptor_set-' . $count . '-id'];
            $descriptor_delete = $stepData['descriptor_set-' . $count . '-delete'];

            $level = '';
            if ($stepData['descriptor_set-' . $count . '-descriptor_type'] == 'G') $level = 'general';
            if ($stepData['descriptor_set-' . $count . '-descriptor_type'] == 'S') $level = 'specific';

            $parent_vocabulary = '';
            if ($stepData['descriptor_set-' . $count . '-parent_vocabulary'] == 'CID-10') $parent_vocabulary = 'ICD-10';
            if ($stepData['descriptor_set-' . $count . '-parent_vocabulary'] == 'DeCS') $parent_vocabulary = 'DeCS';

            $descriptor_code = $stepData['descriptor_set-' . $count . '-vocabulary_code'];
            $descriptor_description = $stepData['descriptor_set-' . $count . '-vocabulary_item_en'];

            if (empty($descriptor_id)) { // insert
                if (empty($level)) continue;
                if (empty($parent_vocabulary)) continue;
                $descriptor = new \Repository\Descriptor(
                    [
                        'aspect' => "HealthCondition",
                        'vocabulary' => $parent_vocabulary,
                        'level' => $level,
                        'version' => '',
                        'code' => $descriptor_code,
                        'text' => $descriptor_description,
                        '_order' => 0,
                    ]
                );
                $clinicalTrial->descriptors()->save($descriptor);
            } elseif ($descriptor_delete && $descriptor_id > 0) { // delete
                $descriptor = $clinicalTrial->descriptors()->where('id', $descriptor_id)->first();
//echo '<pre>'.var_export([
//        __FILE__ => __LINE__,
//        'action' => 'delete',
//    ],true).'</pre>';
                $descriptor->update(
                    [
                        '_deleted' => 1,
                    ]
                );
                continue;
            } else { // update
                $descriptor = $clinicalTrial->descriptors()->where('id', $descriptor_id)->first();
                $descriptor->update(
                    [
                        'vocabulary' => $parent_vocabulary,
                        'code' => $descriptor_code,
                        'text' => $descriptor_description,
                        'level' => $level,
                    ]
                );
            }

            foreach($data['selected_languages'] as $language) {
                if ($language == 'en') continue;
                $vocabulary_id = $stepData['descriptor_set-' . $count . '-vocabulary_id_' . $language];
                $descriptor_description = $stepData['descriptor_set-' . $count . '-vocabulary_item_' . $language];
//echo '<pre>'.var_export([
//        __FILE__ => __LINE__,
//         '$vocabulary_id' => $vocabulary_id,
//        '$item' => $item,
//        '$code' => $code,
//        '$text' => $text,
//    ],true).'</pre>';

                if ($vocabulary_id > 0) {
                    $descriptorTranslation = $descriptor->translations()->where('language', $language)->first();
                    $descriptorTranslation->update(
                        [
                            'text' => $descriptor_description,
                        ]
                    );
                } else {
                    $descriptorTranslation = new \Repository\DescriptorTranslation(
                        [
                            'object_id' => $descriptor_id,
                            'content_type_id' => 36,
                            'language' => $language,
                            'text' => $descriptor_description,
                        ]
                    );
                    $descriptor->translations()->save($descriptorTranslation);
                }
            }
        }
//            echo '<pre>'.var_export([
//                    __FILE__ => __LINE__,
//                    '$descriptor' => $descriptor->toArray(),
//                ],true).'</pre>';
    }

    public function GetStepData($id)
    {
        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);

        $data['health_condition_en'] = $clinicalTrial->hc_freetext;
        foreach ($clinicalTrial->translations as $translation) {
            $data['health_condition_'.$translation->language] = $translation->hc_freetext;
        }
        $data['count_hc'] = 0;
        foreach ($clinicalTrial->descriptors->where('aspect', 'HealthCondition')->where('_deleted', 0) as $descriptor) {
            ++$data['count_hc'];

            $data['descriptor_set-'.$data['count_hc'].'-id'] = $descriptor->id;

            if ($descriptor->level == 'general') $data['descriptor_set-'.$data['count_hc'].'-descriptor_type'] = 'G';
            if ($descriptor->level == 'specific') $data['descriptor_set-'.$data['count_hc'].'-descriptor_type'] = 'S';

            if ($descriptor->vocabulary == 'CAS') $data['descriptor_set-'.$data['count_hc'].'-parent_vocabulary'] = '';
            if ($descriptor->vocabulary == 'ICD-10') $data['descriptor_set-'.$data['count_hc'].'-parent_vocabulary'] = 'CID-10';
            if ($descriptor->vocabulary == 'DeCS') $data['descriptor_set-'.$data['count_hc'].'-parent_vocabulary'] = 'DeCS';

            $data['descriptor_set-'.$data['count_hc'].'-vocabulary_code'] = $descriptor->code;

            $data['descriptor_set-'.$data['count_hc'].'-vocabulary_id_en'] = $descriptor->id;
//            $data['descriptor_set-'.$data['count_hc'].'-vocabulary_item_en'] = $descriptor->code . (empty($descriptor->text) ? '' : ' - '.$descriptor->text);
            $data['descriptor_set-'.$data['count_hc'].'-vocabulary_item_en'] = $descriptor->text;
//
            foreach ($descriptor->translations as $translation) {
                $data['descriptor_set-'.$data['count_hc'].'-vocabulary_id_'.$translation->language] = $translation->id;
//                $data['descriptor_set-'.$data['count_hc'].'-vocabulary_item_'.$translation->language] = $descriptor->code . (empty($translation->text) ? '' : ' - '.$translation->text);
                $data['descriptor_set-'.$data['count_hc'].'-vocabulary_item_'.$translation->language] = $translation->text;
            }
        }
        {
            $context = 'health-conditions';
            $statuses = ['open', 'acknowledged'];
            if ($_SESSION['user']->isRevisor()) { $statuses[] = 'acknowledged'; $statuses[] = 'closed'; }
            $collection = 
                $clinicalTrial->submission()->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                    if (in_array($remark->status, $statuses)) {
                        return $remark;
                    }
                });
            $data['review_remark_lastest'] = 
                $collection->sortByDesc('id')->first();
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;
    }

}