<?php
namespace Steps;

defined('BASEPATH') OR exit('No direct script access allowed');

class Desfecho extends Base {

    private function web_date_to_iso($fieldvalue) {
        if (empty($fieldvalue)) return NULL;
        $date = explode('-', $fieldvalue);
        if (count($date) != 3) {
            $date = explode('/', preg_replace("([^\/0-9])", '', $fieldvalue));
            if (count($date) == 3) {
                $_date = $date;
                if (strlen($_date[2]) == 4) $date[0] = $_date[2];
                if (strlen($_date[2]) == 4) $date[2] = $_date[0];
            }
        }
        if (count($date) != 3) return NULL;
        if (checkdate($date[1], $date[2], $date[0]) == false) return NULL;
        return "{$date[0]}-{$date[1]}-{$date[2]} 12:00:00";
    }

    private function iso_to_web_date($fieldvalue) {
        if (empty($fieldvalue)) return '';
        $value = explode(' ', $fieldvalue);
        if (count($value) != 2) return '';
        $date = explode('-', $value[0]);
        if (count($date) != 3) return '';
        if (checkdate($date[1], $date[2], $date[0]) == false) return '';
        return "{$date[0]}-{$date[1]}-{$date[2]}";
    }

    public function SetStepData($id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $id,
            '$stepData' => $stepData,
        ]));
        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);

        {   // VALIDAÇÃO
            /**
             ** Todo estudo cujo status da Situação do estudo (item existente no campo 5) estiver como
             ** “Análise de dados completa”, precisa ter 2 campos de desfechos aberto (tanto primário quanto secundário
             ** (se houver desfecho secundário)).
             */
            if ($clinicalTrial->recruitment_status_id == 8) { // 8 =>  “Análise de dados completa”
                // precisa ter dois campos de desfechos abertos
                // tanto primário quanto secundário (se houver desfecho secundário)
                $primary_count = 0;
                $secondary_count = 0;
                for($index = 1; $index <= $stepData['count_outcomes']; $index++) {
                    $outcomer_id = $stepData['outcome_set-'.$index.'-id'];
                    $outcomer_type = '';
                    $outcomer_delete = $stepData['outcome_set-'.$index.'-delete'];
                    if ($outcomer_delete == 'on') continue;
//                    $outcome_description_id = $stepData['outcome_set-' . $index . '-description_id_en'];
                    $outcome_description = $stepData['outcome_set-' . $index . '-description_en'];
                    if (empty($outcome_description)) continue;
                    if ($stepData['outcome_set-'.$index.'-outcome_type'] == 'P') $primary_count++;
                    if ($stepData['outcome_set-'.$index.'-outcome_type'] == 'S') $secondary_count++;
                }
                if ($primary_count < 1)
                    $errors['messages']['id_outcome_set-1-outcome_type'] = 'rule_outcome_primary_less_one'; // rule_outcome_primary_less_two
                // if ($secondary_count > 0 && $secondary_count < 2)
                //     $errors['messages']['id_outcome_set-2-outcome_type'] = 'rule_outcome_secondary_less_two';
            }
            if (count($errors) > 0) return $errors;
        }
        {
            $this->pack_date($stepData, 'results_date_completed');
        }
        {
            $clinicalTrial = \Repository\ClinicalTrial::find($id);
            $clinicalTrial->update(
                [
                    'results_date_completed' => $stepData['results_date_completed'],
                ]
            );
        }
        for($index = 1; $index <= $stepData['count_outcomes']; $index++) {
            $outcome_id = $stepData['outcome_set-' . $index . '-id'];
            $outcome_type = '';
            if ($stepData['outcome_set-' . $index . '-outcome_type'] == 'P') $outcome_type = 'primary';
            if ($stepData['outcome_set-' . $index . '-outcome_type'] == 'S') $outcome_type = 'secondary';
            $outcome_delete = $stepData['outcome_set-' . $index . '-delete'];

            if (($outcome_id > 0) && ($outcome_delete == 'on')) {
                $outcome = \Repository\Outcome::find($outcome_id);
                if ($outcome) $outcome->update([
                    '_deleted' => TRUE,
                ]);
                continue;
            }

            $outcome = NULL;

            if ($outcome_id > 0) {
                $outcome = \Repository\Outcome::find($outcome_id);
                if ($outcome) {
                    $outcome->update([
                        'interest' => $outcome_type,
                    ]);
                }
            } else {
                $outcome = new \Repository\Outcome([
                    'interest' => $outcome_type,
                    '_order' => -1,
                    'description' => '',
                ]);
                $clinicalTrial->outcomes()->save($outcome);
            }

            foreach($data['selected_languages'] as $selected_language) {
                $outcome_description_id = $stepData['outcome_set-'.$index.'-description_id_'.$selected_language];
                $outcome_description = $stepData['outcome_set-'.$index.'-description_'.$selected_language];
                if ($selected_language == 'en')  {
                    $outcome->update([
                        'description' => $outcome_description,
                    ]);
                } else {
                    if ($outcome_description_id > 0) {
                        $outcomeTranslation = \Repository\OutcomeTranslation::find($outcome_description_id);
                        $outcomeTranslation->update([
                            'description' => $outcome_description,
                        ]);
                    } else {
                        $outcomeTranslation = new \Repository\OutcomeTranslation(
                            [
                                'content_type_id' => 34,
                                'language' => $selected_language,
                                'description' => $outcome_description,
                            ]
                        );
                        $outcome->translations()->save($outcomeTranslation);
                    }
                }
            }
        }
    }

    public function GetStepData($id)
    {
        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);

        $data['results_date_completed'] = $this->iso_to_web_date($clinicalTrial->results_date_completed);
        {
            $extracted_results_date_completed = $this->extract_date($clinicalTrial->results_date_completed);
            // echo '<pre>'.var_export([
            //     $clinicalTrial->results_date_completed,
            //     $extracted_results_date_completed,
            // ], TRUE).'<pre>';
            $data['results_date_completed-d'] = $extracted_results_date_completed['day'];
            $data['results_date_completed-m'] = $extracted_results_date_completed['month'];
            $data['results_date_completed-y'] = $extracted_results_date_completed['year'];
        }

        foreach ($clinicalTrial->outcomes as $outcome) {
            ++$data['count_outcomes'];
            $data['outcome_set-'.$data['count_outcomes'].'-id'] = $outcome->id;
            if ($outcome->interest == 'primary') $data['outcome_set-'.$data['count_outcomes'].'-outcome_type'] = 'P';
            if ($outcome->interest == 'secondary') $data['outcome_set-'.$data['count_outcomes'].'-outcome_type'] = 'S';
            $data['outcome_set-'.$data['count_outcomes'].'-description_en'] = $outcome->description;
            $data['outcome_set-'.$data['count_outcomes'].'-description_id_en'] = $outcome->id;
            foreach ($outcome->translations as $translation) {
                $data['outcome_set-'.$data['count_outcomes'].'-description_'.$translation->language] = $translation->description;
                $data['outcome_set-'.$data['count_outcomes'].'-description_id_'.$translation->language] = $translation->id;
            }
        }
        {
            $context = 'outcomes';
            $statuses = ['open', 'acknowledged'];
            if ($_SESSION['user']->isRevisor()) { $statuses[] = 'acknowledged'; $statuses[] = 'closed'; }
            $collection = 
                $clinicalTrial->submission()->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                    if (in_array($remark->status, $statuses)) {
                        return $remark;
                    }
                });
            $data['review_remark_lastest'] = 
                $collection->sortByDesc('id')->first();
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;
    }

}