<?php
namespace Steps;

use Helper\FieldHelpLanguage;

defined('BASEPATH') OR exit('No direct script access allowed');

class Anexos {

    public function SetStepData($id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $id,
            '$stepData' => $stepData,
        ]));

        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        for($ctattachment = 1; $ctattachment <= $stepData['count_attach']; $ctattachment++) {

            $attach_id = $stepData['ctattachment_set-'.$ctattachment.'-id'];
            $attach_type = $stepData['ctattachment_set-'.$ctattachment.'-attachment'];
            $attach_delete = $stepData['ctattachment_set-'.$ctattachment.'-delete'];
            $attach_public = ($stepData['ctattachment_set-'.$ctattachment.'-public'] == 'on' ? 1 : 0);
            $attach_description = $stepData['ctattachment_set-'.$ctattachment.'-description'];

// echo '<pre>'.var_export(
//        [
//            __FILE__ => __LINE__,
//            '$attach_id' => $attach_id,
//            '$attach_type' => $attach_type,
//            '$attach_delete' => $attach_delete,
//            '$attachment' => $attachment->toArray(),
//        ],true).'</pre>';

            if (empty($attach_id) || $attach_id == 0) { // insert
                $attachment = new \ReviewApp\Attachment(
                    [
                        'public' => $attach_public,
                        'description' => $attach_description,
                    ]
                );
                if ($attach_type == 1) {
                    $attachment->attach_url = '';
                    $attachment->file = $stepData['ctattachment_set-'.$ctattachment.'-info'];
                    if (empty($attachment->file)) continue;
                } else if ($attach_type == 2) {
                    $attachment->file = '';
                    $attachment->attach_url = $stepData['ctattachment_set-' . $ctattachment . '-link'];
                    if (empty($attachment->attach_url)) continue;
                } else {
                    continue;
                }
                $clinicalTrial->submission()->attachments()->save($attachment);
            } elseif (!empty($attach_delete)) { // delete
                $attachment = $clinicalTrial->submission()->attachments()->find($attach_id);
                $attachment->delete();
            } elseif (!empty($attach_description)) { // update
                $attachment = $clinicalTrial->submission()->attachments()->find($attach_id);
                $attachment->public = $attach_public;
                $attachment->description = $attach_description;
                $attachment->save();
            }
        }
    }

    public function GetStepData($id)
    {
        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $data['submission_id'] = $clinicalTrial->submission()->id;

        $data['count_attach'] = $clinicalTrial->submission()->attachments->count();

        $ctattachment = 0;
        foreach ($clinicalTrial->submission()->attachments as $attachment) {
            $ctattachment++;
            $data['ctattachment_set-'.$ctattachment.'-id'] = $attachment->id;
            $data['ctattachment_set-'.$ctattachment.'-public'] = $attachment->public == 1 ? 'on' : '';
            $data['ctattachment_set-'.$ctattachment.'-attachment'] = $attachment->file != '' ? 1 : 2;
            $data['ctattachment_set-'.$ctattachment.'-link'] = $attachment->attach_url;
            $data['ctattachment_set-'.$ctattachment.'-description'] = $attachment->description;

            if (substr($attachment->file, 0, 1) == '{') {
                $data['ctattachment_set-'.$ctattachment.'-info'] = json_decode($attachment->file, true);
            } else {
                $data['ctattachment_set-'.$ctattachment.'-file'] = $attachment->file;
                $local_filename = ROOT_DIRECTORY.'/'.$attachment->file;
                $eh_attachment = strpos($attachment->file, "attachments/") === 0;
                if ($eh_attachment and file_exists($local_filename)) {
                    $name = substr($attachment->file, strlen("attachments/"));
                    $attachment_info = [
                        'name' => $name,
                        'type' => 'application/octet-stream',
                        'size' => filesize($local_filename),
                        'file' => $attachment->file,
                        'home' => ROOT_DIRECTORY . '/',
                    ];
                    $data['ctattachment_set-'.$ctattachment.'-info'] = $attachment_info;
                }
            }
        }
        {
            $context = 'attachments';
            $statuses = ['open', 'acknowledged'];
            if ($_SESSION['user']->isRevisor()) { $statuses[] = 'acknowledged'; $statuses[] = 'closed'; }
            $collection = 
                $clinicalTrial->submission()->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                    if (in_array($remark->status, $statuses)) {
                        return $remark;
                    }
                });
            $data['review_remark_lastest'] = 
                $collection->sortByDesc('id')->first();
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
           'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;
    }

}