<?php
namespace Steps;

defined('BASEPATH') OR exit('No direct script access allowed');

class DadosIniciais {

    private $selected_languages = [];
    private $userId;

    public function SetUserData($userData)
    {
        $this->userId = $userData['userId'];
    }
    public function SetResultData($resultData)
    {
        $this->selected_languages = ['en'];
        foreach ($resultData['selected_languages'] as $language) $this->selected_languages[] = $language;
    }
    public function SetStepData($id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $id,
            '$stepData' => $stepData,
        ]));
        {   // VALIDAÇÃO
//            if (empty($stepData['utrn_number'])) $errors['messages']['utrn_number'] = 'rule_start_need_utn_number';
            if (empty($stepData['study_type_id'])) $errors['messages']['id_study_type'] = 'rule_start_need_study_type';
            if (empty($stepData['scientific_title_en'])) $errors['messages']['scientific_title'] = 'rule_start_need_scientific_title_en';
            if (count($errors) > 0) return $errors;
        }
        {
            foreach (['pt-br', 'es'] as $language) {
                if (empty($stepData['language_' . $language])) continue;
                $data['selected_languages'][] = $language;
            }
        }

        $data =
            [
                'scientific_title' => @$stepData['scientific_title_en'],
                'scientific_acronym' => '',
                'scientific_acronym_expansion' => '',
                'public_title' => '',
                'acronym' => '',
                'acronym_expansion' => '',
                'hc_freetext' => '',
                'i_freetext' => '',
                'inclusion_criteria' => '',
                'gender' => '',
                'agemin_unit' => '',
                'agemax_unit' => '',
                'exclusion_criteria' => '',
                'study_design' => '',
                'target_sample_size' => 0,
                'created' => date('Y-m-d H:i:s'),
                'status' => 'processing',
                'staff_note' => '',
                '_deleted' => 0,
                'language' => 'en',
                'is_observational' => ($stepData['study_type_id'] == 1) ? 0 : 1,
                'outdated' => 0,
                'study_type_id' => $stepData['study_type_id'],
            ];

        if (empty($stepData['primary_sponsor']) == false) {
            $institution_mix = $stepData['primary_sponsor'];
            $institution_id = $stepData['primary_sponsor'];
            if ($institution_mix and strpos($institution_mix, ':')) {
                $id_and_hash = explode(':', $institution_mix);
                $institution_id = $id_and_hash[0];
                $institution_hash = $id_and_hash[1];
            }
            $data['primary_sponsor_id'] = $institution_id;
        }

        $clinicalTrial = new \Repository\ClinicalTrial($data);

        $clinicalTrial->save();

        $data['id'] = $clinicalTrial->id;

        $clinicalTrial->submissions()->save(
            new \ReviewApp\Submission(
                [
                    'created' => date('Y-m-d H:i:s'),
                    'updated' => date('Y-m-d H:i:s'),
                    'creator_id' => $this->userId,
                    'language' => 'en',
                    'title' => '',
                    'status' => 'draft',
                    'staff_note' => '',
                    '_deleted' => 0,
                ]
            )
        );
        $clinicalTrial->numbers()->save(
            new \Repository\TrialNumber(
                [
                    'id_identifier' => 2,
                    'id_number' => $stepData['utrn_number'],
                    'issuing_authority' => '',
                    '_deleted' => 0,
                ]
            )
        );
        foreach (['pt-br', 'es'] as $language) {
            if (empty($stepData['language_'.$language])) continue;
            $clinicalTrial->translations()->save(
                new \Repository\TrialTranslation(
                    [
                        'language' => $language,
                        'content_type_id' => 24,
                        'scientific_title' => @$stepData['scientific_title_'.$language],
                        'scientific_acronym' => '',
                        'scientific_acronym_expansion' => '',
                        'public_title' => '',
                        'acronym' => '',
                        'acronym_expansion' => '',
                        'hc_freetext' => '',
                        'i_freetext' => '',
                        'inclusion_criteria' => '',
                        'exclusion_criteria' => '',
                        'study_design' => '',
                    ]
                )
            );
        }

        foreach ($stepData['recruitment_countries'] as $recruitment_country_id) {
            $countryCode = \Vocabulary\CountryCode::find($recruitment_country_id);
            $clinicalTrial->recruitmentCountries()->save($countryCode);
        }

        $clinicalTrial->push();

        $data['nextStep'] = STEP_SUMARIO;

        return $data;
    }
    public function GetStepData($id)
    {
        $data = ['trial_id' => $id];
        $data['selected_languages'] = $this->selected_languages;
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;
    }
}