<?php
namespace Steps;

defined('BASEPATH') OR exit('No direct script access allowed');

class Intervencao {


    public function SetStepData($id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $id,
            '$stepData' => $stepData,
        ]));

        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);

        {   // VALIDAÇÃO
            $messages = [];
            $validation = 0;
            {
                // deve ser marcado pelo menos uma categoria
                foreach(\Vocabulary\InterventionCode::all() as $interventionCode) {
                    if (@$stepData['intervention_codes_'.$interventionCode->id] > 0) $validation++;
                }
                if ($validation == 0) $messages['intervention_codes'] = 'rule_intervention_minimal_category';
            }
            {
                $errors = 0;
                // for($count = 1; $count <= $stepData['count_ic']; $count++) {
                //     if ($stepData['descriptor_set-' . $count . '-delete']) continue;
                //     if ($stepData['descriptor_set-' . $count . '-id']) continue;
                //     $descriptor_type = $stepData['descriptor_set-' . $count . '-descriptor_type'];
                //     $code = trim($stepData['descriptor_set-' . $count . '-vocabulary_code']);
                //     if ($descriptor_type == 'G') {
                //         if (str_contains($code, ['.']) == true) $errors++;
                //     } else if ($descriptor_type == 'S') {
                //         if (str_contains($code, ['.']) == false) $errors++;
                //     }
                // }
                if ($errors > 0) $messages['descriptor_set'] = 'descriptor_set_format';
            }
            if (sizeof($messages) > 0) return ['messages' => $messages];
        }
        foreach($data['selected_languages'] as $language)
        {
            if ($language == 'en') {
                $clinicalTrial->update([
                    'i_freetext' => $stepData['intervention_'.$language]
                ]);
            } else {
                $translation = $clinicalTrial->translations->where('language', $language)->first();
                $translation->update([
                    'i_freetext' => $stepData['intervention_'.$language]
                ]);
            }
        }

        foreach(\Vocabulary\InterventionCode::all() as $interventionCode) {
            $interventioncode_id = @$stepData['intervention_codes_'.$interventionCode->id.'_id'];
            $interventioncode_checked = @$stepData['intervention_codes_'.$interventionCode->id];
            if ($interventioncode_checked && $interventioncode_id == NULL) {
                $clinicalTrial->interventionCodes()->attach($interventionCode->id);
            } elseif ($interventioncode_checked == NULL && $interventioncode_id != NULL) {
                $clinicalTrial->interventionCodes()->detach([$interventionCode->id]);
            }
        }

        for($count = 1; $count <= $stepData['count_ic']; $count++) {

            $descriptor_id = $stepData['descriptor_set-' . $count . '-id'];
            $descriptor_delete = $stepData['descriptor_set-' . $count . '-delete'];

            $level = '';
            if ($stepData['descriptor_set-' . $count . '-descriptor_type'] == 'G') $level = 'general';
            if ($stepData['descriptor_set-' . $count . '-descriptor_type'] == 'S') $level = 'specific';

            $parent_vocabulary = '';
            if ($stepData['descriptor_set-' . $count . '-parent_vocabulary'] == 'CID-10') $parent_vocabulary = 'ICD-10';
            if ($stepData['descriptor_set-' . $count . '-parent_vocabulary'] == 'DeCS') $parent_vocabulary = 'DeCS';

            if (empty($level) or empty($parent_vocabulary)) continue;

            $descriptor_code = $stepData['descriptor_set-' . $count . '-vocabulary_code'];
            $descriptor_description = $stepData['descriptor_set-' . $count . '-vocabulary_item_en'];

            if (empty($descriptor_id)) { // insert
                $descriptor = new \Repository\Descriptor(
                    [
                        'aspect' => 'Intervention',
                        'vocabulary' => $parent_vocabulary,
                        'level' => $level,
                        'version' => '',
                        'code' => $descriptor_code,
                        'text' => $descriptor_description,
                        '_order' => 0,
                    ]
                );
                $clinicalTrial->descriptors()->save($descriptor);
            } elseif ($descriptor_delete && $descriptor_id > 0) { // delete
                $descriptor = $clinicalTrial->descriptors()->where('id', $descriptor_id)->first();
                $descriptor->update(
                    [
                        '_deleted' => 1,
                    ]
                );
                continue;

            } else { // update
                $descriptor = $clinicalTrial->descriptors()->where('id', $descriptor_id)->first();
                $descriptor->update(
                    [
                        'vocabulary' => $parent_vocabulary,
                        'code' => $descriptor_code,
                        'text' => $descriptor_description,
                        'level' => $level,
                    ]
                );
            }

            foreach($data['selected_languages'] as $language) {
                if ($language == 'en') continue;
                $vocabulary_id = $stepData['descriptor_set-' . $count . '-vocabulary_id_' . $language];
                $descriptor_description = $stepData['descriptor_set-' . $count . '-vocabulary_item_' . $language];

                if ($vocabulary_id > 0) {
                    $descriptorTranslation = $descriptor->translations()->where('language', $language)->first();
                    $descriptorTranslation->update(
                        [
                            'text' => $descriptor_description,
                        ]
                    );
                } else {
                    $descriptorTranslation = new \Repository\DescriptorTranslation(
                        [
                            'object_id' => $descriptor_id,
                            'content_type_id' => 36,
                            'language' => $language,
                            'text' => $descriptor_description,
                        ]
                    );
                    $descriptor->translations()->save($descriptorTranslation);
                }
            }
        }
    }

    public function GetStepData($id)
    {
        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $data['selected_languages'] = $clinicalTrial->selectedLanguages(2);

        $data['intervention_en'] = $clinicalTrial->i_freetext;
        foreach ($clinicalTrial->translations as $translation) {
            $data['intervention_'.$translation->language] = $translation->i_freetext;
        }

        foreach ($clinicalTrial->interventionCodes as $interventionCode) {
//            $data['intervention_codes_'.$interventionCode->id.'_pivot'] = $interventionCode->pivot;
            $data['intervention_codes_'.$interventionCode->id.'_id'] = $interventionCode->pivot->id;
            $data['intervention_codes_'.$interventionCode->id] = $interventionCode->id;
        }

        $data['count_ic'] = 0;
        foreach ($clinicalTrial->descriptors->where('aspect', 'Intervention') as $descriptor) {
            ++$data['count_ic'];

            $data['descriptor_set-'.$data['count_ic'].'-id'] = $descriptor->id;

            if ($descriptor->level == 'general') $data['descriptor_set-'.$data['count_ic'].'-descriptor_type'] = 'G';
            if ($descriptor->level == 'specific') $data['descriptor_set-'.$data['count_ic'].'-descriptor_type'] = 'S';

            if ($descriptor->vocabulary == 'CAS') $data['descriptor_set-'.$data['count_ic'].'-parent_vocabulary'] = '';
            if ($descriptor->vocabulary == 'ICD-10') $data['descriptor_set-'.$data['count_ic'].'-parent_vocabulary'] = 'CID-10';
            if ($descriptor->vocabulary == 'DeCS') $data['descriptor_set-'.$data['count_ic'].'-parent_vocabulary'] = 'DeCS';

            foreach ($data['selected_languages'] as $language) {
                $data['descriptor_set-'.$data['count_ic'].'-vocabulary_id_'.$language] = NULL;
                $data['descriptor_set-'.$data['count_ic'].'-vocabulary_item_'.$language] = '';
            }

            $data['descriptor_set-'.$data['count_ic'].'-vocabulary_id_en'] = $descriptor->id;
            $data['descriptor_set-'.$data['count_ic'].'-vocabulary_code'] = $descriptor->code;
//            $data['descriptor_set-'.$data['count_ic'].'-vocabulary_item_en'] = $descriptor->code . (empty($descriptor->text) ? '' : ' - '.$descriptor->text);
            $data['descriptor_set-'.$data['count_ic'].'-vocabulary_item_en'] = $descriptor->text;

            foreach ($descriptor->translations as $translation) {
                $data['descriptor_set-'.$data['count_ic'].'-vocabulary_id_'.$translation->language] = $translation->id;
//                $data['descriptor_set-'.$data['count_ic'].'-vocabulary_item_'.$translation->language] = $descriptor->code . (empty($translation->text) ? '' : ' - '.$translation->text);
                $data['descriptor_set-'.$data['count_ic'].'-vocabulary_item_'.$translation->language] = $translation->text;
            }
        }

        {
            $context = 'interventions';
            $statuses = ['open', 'acknowledged'];
            if ($_SESSION['user']->isRevisor()) { $statuses[] = 'acknowledged'; $statuses[] = 'closed'; }
            $collection = 
                $clinicalTrial->submission()->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                    if (in_array($remark->status, $statuses)) {
                        return $remark;
                    }
                });
            $data['review_remark_lastest'] = 
                $collection->sortByDesc('id')->first();
        }
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;

    }

}