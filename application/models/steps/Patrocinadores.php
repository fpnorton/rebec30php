<?php
namespace Steps;

defined('BASEPATH') OR exit('No direct script access allowed');

class Patrocinadores {

    public function SetStepData($id, $stepData)
    {
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$id' => $id,
            '$stepData' => $stepData,
        ]));
        $clinicalTrial = \Repository\ClinicalTrial::find($id);
        // echo '<pre>';
        // echo var_export([$stepData], TRUE);
        // echo '</pre>';
        // die();
/*
        {   // VALIDAÇÃO
            $validation = [
                'P' => 0,
                'S' => 0,
                'U' => 0
            ];
            $errors = [];
            for($sponsor = 1; $sponsor <= $stepData['count_sponsor']; $sponsor++) {
                $sponsor_id_and_type = explode(';', $stepData['id_sponsor_set-' . $sponsor . '-sponsor_type']);
                $sponsor_type = (sizeof($sponsor_id_and_type) == 1 ? $sponsor_id_and_type[0] : $sponsor_id_and_type[1]);
                if (empty($sponsor_type)) continue;
                $institution_id = $stepData['id_sponsor_set-'. $sponsor . '-institution'];
                $institution_hash = $stepData['id_sponsor_set-'. $sponsor . '-hash'];
                {
                    $institution_mix = $stepData['id_sponsor_set-'. $sponsor . '-mix'];
                    if ($institution_mix and strpos($institution_mix, ':')) {
                        $id_and_hash = explode(':', $institution_mix);
                        $institution_id = $id_and_hash[0];
                        $institution_hash = $id_and_hash[1];
                    }
                }
                if (empty($institution_id)) continue;
                $validation[$sponsor_type]++;
            }
            foreach ($validation as $key => $value) {
                if ($value == 0) $errors['messages'] = ['sponsor_set-0-institution' => 'rule_sponsor_per_type'];
            }
            if (count($errors) > 0) return $errors;
        }
// */
        for($sponsor = 1; $sponsor <= $stepData['count_sponsor']; $sponsor++) {
            $sponsor_id_and_type = explode(';', $stepData['id_sponsor_set-' . $sponsor . '-sponsor_type']);
            $sponsor_type = (sizeof($sponsor_id_and_type) == 1 ? $sponsor_id_and_type[0] : $sponsor_id_and_type[1]);
            $sponsor_id = ($sponsor_id_and_type[0] > 0 ? $sponsor_id_and_type[0] : '');
            $institution_mix = $stepData['id_sponsor_set-'. $sponsor . '-mix'];
            $institution_id = $stepData['id_sponsor_set-'. $sponsor . '-institution'];
            $institution_hash = $stepData['id_sponsor_set-'. $sponsor . '-hash'];
            if ($institution_mix and strpos($institution_mix, ':')) {
                $id_and_hash = explode(':', $institution_mix);
                $institution_id = $id_and_hash[0];
                $institution_hash = $id_and_hash[1];
            }
            $sponsor_delete = ($stepData['sponsor_set-'. $sponsor . '-delete'] == 'on');
            switch ($sponsor_type) {
                case 'P':
                    if ($sponsor_delete or ($institution_id == '')) {
                    
                        if ($clinicalTrial->primarySponsor != NULL) $clinicalTrial->primarySponsor()->update(['_deleted' => 1]);
                    
                    } elseif ($institution_id > 0) {
                    
                        $clinicalTrial->update([
                            'primary_sponsor_id' => $institution_id,
                        ]);
                    }
                    break;
                case 'S':
                    if ($sponsor_id != '') { // secondary sponsor existente
                        $secondarySponsor = \Repository\SecondarySponsor::find($sponsor_id);
                        if ($sponsor_delete or $institution_id == '') {
                            $secondarySponsor->update(['_deleted' => 1]);
                        } else {
                            $secondarySponsor->institution_id = $institution_id;
                            $clinicalTrial->secondarySponsors()->save($secondarySponsor);
                        }
                    } elseif ($institution_id != '') {
                        $secondarySponsor = new \Repository\SecondarySponsor(
                            [
                                'institution_id' => $institution_id,
                            ]
                        );
                        $clinicalTrial->secondarySponsors()->save($secondarySponsor);
                    }
                    break;
                case 'U':
                    if ($sponsor_id != '') { // support sponsor existente
                        $supportSource = \Repository\SupportSource::find($sponsor_id);
                        if ($sponsor_delete or $institution_id == '') {
                            $supportSource->update(['_deleted' => 1]);
                        } else {
                            $supportSource->institution_id = $institution_id;
                            $supportSource->save();
                        }
                    } elseif ($institution_id != '') {
                        $supportSource = new \Repository\SupportSource(
                            [
                                'institution_id' => $institution_id,
                            ]
                        );
                        $clinicalTrial->supportSources()->save($supportSource);
                    }
                    break;
            }
        }
        $clinicalTrial->push();
    }

    public function GetStepData($id)
    {
        $clinicalTrial = \Repository\ClinicalTrial::find($id);

        $hasSecondSponsors = ($clinicalTrial->secondarySponsors()->count() > 0);
        $hasSupportSources = ($clinicalTrial->supportSources()->count() > 0);

        $data['count_secondary_sponsor'] =  0;
        $data['count_support_source'] = 0;
        $count = 1;
        {
            $data['id_sponsor_map']['P'][] = $count;
            $data['id_sponsor_set-'.$count.'-sponsor_type'] = $id.';P';
            if ($clinicalTrial->primarySponsor == NULL) :
                $data['id_sponsor_set-'.$count.'-institution'] = '';
                $data['id_sponsor_set-'.$count.'-hash'] = '';
                $data['id_sponsor_set-'.$count.'-mix'] = '';
            else :
                $data['id_sponsor_set-'.$count.'-sponsor_type'] = $id.';P';
                $data['id_sponsor_set-'.$count.'-institution'] = $clinicalTrial->primarySponsor->id;
                $data['id_sponsor_set-'.$count.'-hash'] = $clinicalTrial->primarySponsor->hashCode();
                $data['id_sponsor_set-'.$count.'-mix'] =
                    implode(':',
                        [$clinicalTrial->primarySponsor->id, $clinicalTrial->primarySponsor->hashCode()]
                    );
            endif;
        }
        {
            foreach ($clinicalTrial->secondarySponsors as $secondarySponsor) {
                if ($secondarySponsor->institution == NULL) continue;
                // echo '<pre>';
                // echo var_export(['$secondarySponsor->institution->id' => $secondarySponsor->institution->id], TRUE);
                // echo '</pre>';
                $data['count_secondary_sponsor']++;
                $count++;
                // if ($data['count_secondary_sponsor'] == 1) {
                //     $count = 2;
                // } else if ($hasSupportSources) {
                //     $count = 3 + ($data['count_secondary_sponsor'] - 1);
                // } else {
                //     $count = $data['count_secondary_sponsor'];
                // }
                $data['id_sponsor_map']['S'][] = $count;
                $data['id_sponsor_set-'.$count.'-sponsor_type'] = $secondarySponsor->id.';S';
                $data['id_sponsor_set-'.$count.'-institution'] = $secondarySponsor->institution->id;
                $data['id_sponsor_set-'.$count.'-hash'] = $secondarySponsor->institution->hashCode();
                $data['id_sponsor_set-'.$count.'-mix'] =
                    implode(':',
                        [$secondarySponsor->institution->id, $secondarySponsor->institution->hashCode()]
                    );
            }
            if ($data['count_secondary_sponsor'] === 0) {
                $data['count_secondary_sponsor'] = 1;
                $data['id_sponsor_map']['S'][] = ++$count;
            }
        }
        {
            // echo '<pre>';
            // echo var_export(['$clinicalTrial->supportSources' => $clinicalTrial->supportSources->toArray()], TRUE);
            // echo '</pre>';
            foreach ($clinicalTrial->supportSources as $supportSource) {
                if ($supportSource->institution == NULL) continue;
                // echo '<pre>';
                // echo var_export(['$supportSource->institution->id' => $supportSource->institution->id], TRUE);
                // echo '</pre>';
                $data['count_support_source']++;
                $count++;
                // if ($data['count_support_source'] == 1) {
                //     $count = 3;
                // } else if ($hasSecondSponsors) {
                //     $count = 3 + ($data['count_secondary_sponsor'] - 1) + ($data['count_support_source'] - 1);
                // } else {
                //     $count = 3 + $data['count_support_source'];
                // }
                $data['id_sponsor_map']['U'][] = $count;
                $data['id_sponsor_set-'.$count.'-sponsor_type'] = $supportSource->id.';U';
                $data['id_sponsor_set-'.$count.'-institution'] = $supportSource->institution->id;
                $data['id_sponsor_set-'.$count.'-hash'] = $supportSource->institution->hashCode();
                $data['id_sponsor_set-'.$count.'-mix'] = implode(':',
                    [$supportSource->institution->id, $supportSource->institution->hashCode()]
                );
            }
            if ($data['count_support_source'] === 0) {
                $data['count_support_source'] = 1;
                $data['id_sponsor_map']['U'][] = ++$count;
            }
                
        }
        $data['count_sponsor'] = 1 + $data['count_secondary_sponsor'] + $data['count_support_source'];
        {
            $context = 'sponsors';
            $statuses = ['open', 'acknowledged'];
            if ($_SESSION['user']->isRevisor()) { $statuses[] = 'acknowledged'; $statuses[] = 'closed'; }
            $collection = 
                $clinicalTrial->submission()->remarks->where('context', $context)->filter(function($remark) use($statuses) { 
                    if (in_array($remark->status, $statuses)) {
                        return $remark;
                    }
                });
            $data['review_remark_lastest'] = 
                $collection->sortByDesc('id')->first();
        }
        // echo '<pre>';
        // echo var_export([$data], TRUE);
        // echo '</pre>';
        error_log(json_encode([
            __FILE__ => __LINE__,
            'class' => get_class($this),
            'REQUEST_URI' => '('.$_SERVER['REQUEST_URI'].')',
            '$data' => $data,
        ]));
        return $data;
    }
}