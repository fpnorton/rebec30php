<?php

namespace Sidebar;

class SidebarItem {

    private $_fa_icon;
    private $_title;
    private $_route;

    /**
     * SidebarItem constructor.
     * @param $_fa_icon
     * @param $_title
     * @param $_route
     */
    public function __construct($_fa_icon, $_title, $_route)
    {
        $this->_fa_icon = $_fa_icon;
        $this->_title = $_title;
        $this->_route = $_route;
    }
    /**
     * @return mixed
     */
    public function getFaIcon()
    {
        return $this->_fa_icon;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->_route;
    }

}