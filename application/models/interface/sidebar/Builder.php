<?php

namespace Sidebar;

class Builder {

    static function build($sidebar = []) { ?>
        <ul class="sidebar-menu">
            <li class="header"><?php lang('main'); ?></li>
            <li class="header"><?php echo (@$sidebar['title'] ?? 'MENU') ?></li>
            <?php foreach((@$sidebar['items'] ?? []) as $sidebaritem) : ?>
            <li class="treeview">
                <a class="anchor-menu-link" href="<?php echo base_url() . $sidebaritem->getRoute(); ?>">
                    <i class="<?php echo $sidebaritem->getFaIcon(); ?>" aria-hidden="true" style="padding: 2px;"></i>&nbsp;<span class="no-break" style="padding: 2px;"><?php echo $sidebaritem->getTitle(); ?></span>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
    <?php }

}