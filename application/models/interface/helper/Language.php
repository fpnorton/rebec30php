<?php

namespace Helper;

class Language
{
    static public $available = [
        'en' => [
            'name' =>'English',
            'flag' =>'flag-icon-us',
            'default' => true,
            'date_fmt_hrs' => 'm/d/Y H:i:s',
            'date_fmt' => 'm/d/Y',
            'date_sample' => 'mm/dd/yyyy',
        ],
        'pt-br' => [
            'name' =>'Português',
            'flag' =>'flag-icon-br',
            'default' => true,
            'date_fmt_hrs' => 'd/m/Y H:i:s',
            'date_fmt' => 'd/m/Y',
            'date_sample' => 'dd/mm/yyyy',
        ],
        'es' => [
            'name' =>'Español',
            'flag' =>'flag-icon-mx',
            'default' => false,
            'date_fmt_hrs' => 'd/m/Y H:i:s',
            'date_fmt' => 'd/m/Y',
            'date_sample' => 'dd/mm/yyyy',
        ],
    ];
}