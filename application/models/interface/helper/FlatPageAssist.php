<?php

namespace Helper;

use Django\FlatPage;
use Django\FlatPageTranslation;

class FlatPageAssist
{
    private $_url;
    private $_flatPage = NULL;
    private $_update = FALSE;

    public $_title;
    public $_content;

    public function __construct($_url)
    {
        $this->_url = $_url;
        $flatPages = \Django\FlatPage::where('url','LIKE', $_url)->get();
        if ($flatPages->count() > 0) {
            $this->_flatPage = $flatPages->first();
            $this->_title = $this->_flatPage->title;
            $this->_content = $this->_flatPage->content;
            if (linguagem_selecionada() != 'en') {
                foreach ($this->_flatPage->translations as $translation) {
                    if (linguagem_selecionada() == $translation->language) {
                        if (empty(trim($translation->title)) or empty(trim($translation->content))) break;
                        $this->_title = $translation->title;
                        $this->_content = $translation->content;
                    }
                }
            }
        } else {

            try {

                $FlatPage = new FlatPage([
                    'url' => $_url,
                    'title' => '',
                    'content' => '',
                    'enable_comments' => false,
                    'template_name' => '',
                    'registration_required' => false,
                ]);
                $FlatPage->save();
                foreach (Language::$available as $language_key => $language) {
                    if ('en' == $language_key) continue;
                    $FlatPageTranslation = new FlatPageTranslation([
                        'language' => $language_key,
                        'content_type_id' => 9,
                        'object_id' => $FlatPage->id,
                        'title' => '',
                        'content' => ''
                    ]);
                    $FlatPageTranslation->save();
                }
    
            } catch(Exception $e) {
                error_log("ERRO AO SALVAR FLATPAGE - VERIFICAR SE NECESSARIO REPARAR TABELA");
            }
            $this->_flatPage = \Django\FlatPage::where('url','=', $_url)->first();
            $this->_title = '';
            $this->_content = '';
        }
    }

}