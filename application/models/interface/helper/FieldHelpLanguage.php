<?php

namespace Helper;

use Assistance\FieldHelp;
use Assistance\FieldHelpTranslation;

class FieldHelpLanguage
{
    private $_form;
    private $_fields = [];
    private $_update = FALSE;

    private $_redis = NULL;

    public function __construct($_form)
    {
        {
            $this->_redis = new \Redis();
            $this->_redis->connect('redis', 6379);
            $this->_redis->auth('rebec');
        }
        $this->_form = $_form;
        $fields = FieldHelp::where('form','=', $_form)
            ->where('field','like', 'help_%')
            ->get();
        foreach ($fields as $field) {
            if (linguagem_selecionada() == 'en') {
                $this->add($field->field, $field->text, $field->example);
            }
            foreach ($field->translations as $translation) {
                if (linguagem_selecionada() == $translation->language) {
                    $this->add($field->field, $translation->text, $translation->example);
                }
            }
        }
//        {
//echo '<pre>'.var_export([
//        __FILE__ => __LINE__,
//        '$this->_fields' => $this->_fields,
//    ],true).'</pre>';
//        }
    }

    public function disableUpdate() {
        $this->_update = FALSE;
    }

    public function enableUpdate() {
        $this->_update = (ENVIRONMENT == 'development');
    }

    private function add($_field, $_text, $_example) {
        $this->_fields[$_field] = [
            'text' => $_text,
            'example' => $_example,
        ];
    }

    public function get($_field) {
        $_field = strpos($_field, 'help_') > -1 ? $_field : ('help_'.$_field);
//echo '<pre>'.var_export([
//        __FILE__ => __LINE__,
//        '$_field' => $_field,
//    ],true).'</pre>';
        if (array_key_exists($_field, $this->_fields)) {
            return @$this->_fields[$_field];
        }
        return [
            'text' => '',
            'example' => '',
        ];
    }

    public function get_or_new($_field) {
        $_field = strpos($_field, 'help_') > -1 ? $_field : ('help_'.$_field);
        $this->_redis->hIncrBy('fieldHelp:'.$this->_form, $_field, 1);
        $_text    = '';
        $_example = '';
        if (array_key_exists($_field, $this->_fields)) {
            $_text    = @$this->_fields[$_field]['text'];
            $_example = @$this->_fields[$_field]['example'];
        } else if ($this->_update) {
            $FieldHelp = FieldHelp::where('form','=', $this->_form)
                ->where('field','=', $_field)->get();
            if ($FieldHelp->count() == 1) {
                $FieldHelp = $FieldHelp->first();
                $_text    = trim($FieldHelp->text);
                $_example = trim($FieldHelp->example);
            } else if ($FieldHelp->count() == 0) {
                $FieldHelp = new FieldHelp([
                    'form' => $this->_form,
                    'field' => $_field,
                    'text' => '@'.$this->_form.'-'.$_field,
                    'example' => '@'
                ]);
                $FieldHelp->save();
                foreach (Language::$available as $language_key => $language) {
                    if ('en' == $language_key) continue;
                    $FieldHelpTranslation = new FieldHelpTranslation([
                        'language' => $language_key,
                        'content_type_id' => 51,
                        'object_id' => $FieldHelp->id,
                        'text' => '@'.$this->_form.'-'.$_field,
                        'example' => '@'
                    ]);
                    $FieldHelpTranslation->save();
                }
                $_text    = trim($FieldHelp->text);
                $_example = trim($FieldHelp->example);
            }
            $this->add($_field, $_text, $_example);
        } else {
            $this->_redis->hSet('warning:fieldHelpLanguage', date("Y-m-d H:i:s"), $this->_form.':'.$_field);
        }
        $_text    = trim($_text);
        $_example = trim($_example);
        if (strlen($_example) > 3) $_text =  $_text.'<br /><b>Ex: </b>'.$_example;
        {   // REPLACE CHARs
            $_transform = [
                '"' => '&#8221;',
            ];
            foreach ($_transform as $key => $value) {
                $_text = str_replace($key, $value, $_text);
            }
        }
        return $_text;
    }
}