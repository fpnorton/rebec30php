<?php
namespace Helper;

class Email 
{
    private $_from = NULL;
    private $_to = [];
    private $_cc = [];
    private $_bcc = [];
    private $_subject = '';
    private $_html_body = '';
    private $_text_body = '';

    public function __construct($from_email = NULL, $from_name = NULL)
    {
        if ($from_email == NULL) $from_email = ($_ENV['SMTP_USER'] ?? 'root@maildev.org');
        $this->_from = [
            'email' => $from_email, 
            'name' => ($from_name == NULL ? $from_email: $from_name),
        ];
    }

    public function addTO($email, $name = NULL) 
    {
        if (empty($email)) return $this;
        $this->_to[] = [
            'email' => $email, 
            'name' => ($name == NULL ? $email : $name),
        ];
        return $this;
    }

    public function addCC($email, $name = NULL) 
    {
        if (empty($email)) return $this;
        $this->_cc[] = [
            'email' => $email, 
            'name' => ($name == NULL ? $email : $name),
        ];
        return $this;
    }

    public function addBCC($email, $name = NULL) 
    {
        if (empty($email)) return $this;
        $this->_bcc[] = [
            'email' => $email, 
            'name' => ($name == NULL ? $email : $name),
        ];
        return $this;
    }

    public function addSUBJECT($subject) 
    {
        $this->_subject = mb_encode_mimeheader(html_entity_decode($subject, ENT_HTML5, 'UTF-8'));
        return $this;
    }

    public function addBODY($html) 
    {
        $this->_html_body = $html;
        $this->_text_body = strip_tags($html);
        return $this;
    }

    public function getFromEmail() {
        return $this->_from['email'];
    }

    public function getFromName() {
        return $this->_from['name'];
    }

    public function getTo() {
        return $this->_to;
    }

    public function getCarbonCopy() {
        return $this->_cc;
    }

    public function getBlindedCarbonCopy() {
        return $this->_bcc;
    }

    public function getSubject() {
        return $this->_subject;
    }

    public function getHtmlBody() {
        return $this->_html_body;
    }

    public function getTextBody() {
        return $this->_text_body;
    }

}