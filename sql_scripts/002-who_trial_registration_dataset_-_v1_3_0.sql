/*
Ethics Review
Completion date
Summary Results
IPD sharing statement

Parecer consubstanciado
Data de conclusão
Resumo dos resultados
Termo de compartilhamento de dados individuais do participante
 */
-- 17 - Sample Size
-- 17 - b
ALTER TABLE `repository_clinicaltrial`
      ADD COLUMN  `enrollment_actual_results` int(10) unsigned NULL
;

-- 21 - Ethics Review
CREATE TABLE `repository_ethicsreviewcontact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trial_id` int(11) NOT NULL,
  `contact_id` int(11) NULL,
  `ethicsreviewcontactstatus_id` int(11) NULL,
  `date_approval` varchar(10) NULL,
  `_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `trial_id` (`trial_id`,`contact_id`),
  KEY `repository_ethicsreviewcontact_1` (`trial_id`),
  KEY `repository_ethicsreviewcontact_2` (`contact_id`),
  KEY `repository_ethicsreviewcontact_3` (`_deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `vocabulary_ethicsreviewcontactstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `order` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ethicsreviewcontact_status_label` (`label`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO django_content_type (id, name, app_label, model) VALUES (66, 'ethics review contact status', 'vocabulary', 'ethicsreviewcontactstatus');

INSERT INTO vocabulary_ethicsreviewcontactstatus (id, label, description, `order`) VALUES (1, 'Approved', '', 10);
INSERT INTO vocabulary_ethicsreviewcontactstatus (id, label, description, `order`) VALUES (2, 'Not approved', '', 20);
INSERT INTO vocabulary_ethicsreviewcontactstatus (id, label, description, `order`) VALUES (3, 'Not available', '', 30);

INSERT INTO vocabulary_vocabularytranslation (language, content_type_id, object_id, label, description)
    VALUES ('pt-br', 66, 1, 'Aprovado', '');
INSERT INTO vocabulary_vocabularytranslation (language, content_type_id, object_id, label, description)
    VALUES ('pt-br', 66, 2, 'Não aprovado', '');
INSERT INTO vocabulary_vocabularytranslation (language, content_type_id, object_id, label, description)
    VALUES ('pt-br', 66, 3, 'Indisponivel', '');
INSERT INTO vocabulary_vocabularytranslation (language, content_type_id, object_id, label, description)
    VALUES ('es', 66, 1, 'Aprobado', '');
INSERT INTO vocabulary_vocabularytranslation (language, content_type_id, object_id, label, description)
    VALUES ('es', 66, 2, 'No aprobado', '');
INSERT INTO vocabulary_vocabularytranslation (language, content_type_id, object_id, label, description)
    VALUES ('es', 66, 3, 'Indisponible', '');

-- 22 - Completion date
ALTER TABLE `repository_clinicaltrial`
      ADD COLUMN  `results_date_completed` datetime NULL
;

-- 23 - Summary Results -- resumo dos resultados
ALTER TABLE `repository_clinicaltrial`
      ADD COLUMN  `results_date_posted` datetime NULL,
      ADD COLUMN  `results_date_first_publication` datetime NULL,
      ADD COLUMN  `results_url_link` varchar(255) NULL,
      ADD COLUMN  `results_baseline_char` longtext NULL,
      ADD COLUMN  `results_participant_flow` longtext NULL,
      ADD COLUMN  `results_adverse_events` longtext NULL,
      ADD COLUMN  `results_outcome_measure` longtext NULL,
      ADD COLUMN  `results_url_protocol` varchar(255) NULL,
      ADD COLUMN  `results_summary` varchar(512) NULL
;
ALTER TABLE `repository_clinicaltrialtranslation`
      ADD COLUMN  `results_baseline_char` longtext NULL,
      ADD COLUMN  `results_participant_flow` longtext NULL,
      ADD COLUMN  `results_adverse_events` longtext NULL,
      ADD COLUMN  `results_outcome_measure` longtext NULL,
      ADD COLUMN  `results_summary` varchar(512) NULL
;

-- 24 - IPD sharing statement -- termo de compartilhamento de dados individuais
ALTER TABLE `repository_clinicaltrial`
      ADD COLUMN  `results_IPD_plan_id` int(11) NULL,
      ADD COLUMN  `results_IPD_description` longtext NULL
;
ALTER TABLE `repository_clinicaltrialtranslation`
      ADD COLUMN  `results_IPD_description` longtext NULL
;

CREATE TABLE `vocabulary_ipd_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `order` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ethicsreviewcontact_status_label` (`label`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO django_content_type (id, name, app_label, model)
    VALUES (67, 'ethics review ipd plan', 'vocabulary', 'ethicsreviewipdplan');

INSERT INTO vocabulary_ipd_plan (id, label, description, `order`) VALUES (1, 'Yes', '', 10);
INSERT INTO vocabulary_ipd_plan (id, label, description, `order`) VALUES (2, 'No', '', 20);
INSERT INTO vocabulary_ipd_plan (id, label, description, `order`) VALUES (3, 'Undecided', '', 30);

INSERT INTO vocabulary_vocabularytranslation (language, content_type_id, object_id, label, description)
    VALUES ('pt-br', 67, 1, 'Sim', '');
INSERT INTO vocabulary_vocabularytranslation (language, content_type_id, object_id, label, description)
    VALUES ('pt-br', 67, 2, 'Não', '');
INSERT INTO vocabulary_vocabularytranslation (language, content_type_id, object_id, label, description)
    VALUES ('pt-br', 67, 3, 'Não decidido', '');
INSERT INTO vocabulary_vocabularytranslation (language, content_type_id, object_id, label, description)
    VALUES ('es', 67, 1, 'Si', '');
INSERT INTO vocabulary_vocabularytranslation (language, content_type_id, object_id, label, description)
    VALUES ('es', 67, 2, 'No', '');
INSERT INTO vocabulary_vocabularytranslation (language, content_type_id, object_id, label, description)
    VALUES ('es', 67, 3, 'No decidido', '');


-- A1 - ADDITIONAL DATA ITEMS
ALTER TABLE `repository_clinicaltrial`
      ADD COLUMN  `url` varchar(255) NULL
;
