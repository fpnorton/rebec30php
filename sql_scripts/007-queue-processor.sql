CREATE TABLE `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` longtext NOT NULL,
  `payload` longtext NOT NULL,
  `status` longtext NOT NULL,
  `created` datetime NULL,
  `closed` datetime NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `queue_sequence` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
