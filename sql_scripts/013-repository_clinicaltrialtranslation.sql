alter table repository_clinicaltrialtranslation 
    modify column scientific_acronym longtext NOT NULL,
    modify column scientific_acronym_expansion longtext NOT NULL,
    modify column acronym longtext NOT NULL,
    modify column acronym_expansion longtext NOT NULL
;
alter table repository_clinicaltrial 
    modify column scientific_acronym longtext NOT NULL,
    modify column scientific_acronym_expansion longtext NOT NULL,
    modify column acronym longtext NOT NULL,
    modify column acronym_expansion longtext NOT NULL
;
