ALTER TABLE `repository_clinicaltrial`
      MODIFY COLUMN  `results_url_link` longtext NULL
;
ALTER TABLE `repository_clinicaltrial`
      MODIFY COLUMN  `results_url_protocol` longtext NULL
;
ALTER TABLE `repository_clinicaltrial`
      MODIFY COLUMN  `results_summary` longtext NULL
;
ALTER TABLE `repository_clinicaltrialtranslation`
      MODIFY COLUMN  `results_summary` longtext NULL
;
