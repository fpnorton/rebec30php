CREATE TABLE `versioner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trial_id` int(10) unsigned NOT NULL,
  `sequence_number` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `snapshot` longtext NOT NULL,
  `creator_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `trial_id_and_sequence` (`trial_id`,`sequence_number`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE  assistance_category                          CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  assistance_categorytranslation               CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  assistance_field                             CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
-- ALTER TABLE  assistance_fieldhelp                         CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  assistance_fieldhelptranslation              CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  assistance_languages                         CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  assistance_languages_translations            CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  assistance_page                              CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  assistance_question                          CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  assistance_questiontranslation               CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  auth_group                                   CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  auth_group_permissions                       CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  auth_message                                 CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  auth_permission                              CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  auth_user                                    CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  auth_user_groups                             CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  auth_user_user_permissions                   CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  django_admin_log                             CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  django_content_type                          CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  django_flatpage                              CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  django_flatpage_sites                        CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  django_session                               CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  django_site                                  CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  flatpages_polyglot_flatpagetranslation       CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  fossil_fossil                                CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
-- ALTER TABLE  fossil_fossilindexer                         CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  maintenance_maintenancewindow                CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  picolo_approved                              CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  picolo_aprovacao                             CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  picolo_reports                               CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  picolo_resubmit                              CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  picolo_revisor_info                          CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  picolo_submit                                CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  registration_registrationprofile             CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_clinicaltrial                     CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_clinicaltrial_i_code              CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_clinicaltrial_recruitment_country CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_clinicaltrialtranslation          CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_contact                           CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_descriptor                        CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_descriptortranslation             CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_institution                       CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_outcome                           CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_outcometranslation                CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_publiccontact                     CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_scientificcontact                 CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_sitecontact                       CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_trialnumber                       CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_trialsecondarysponsor             CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  repository_trialsupportsource                CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  reviewapp_attachment                         CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  reviewapp_news                               CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  reviewapp_newstranslation                    CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  reviewapp_recruitmentcountry                 CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  reviewapp_remark                             CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  reviewapp_submission                         CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  reviewapp_userprofile                        CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  south_migrationhistory                       CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  tickets_followup                             CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  tickets_media                                CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  tickets_ticket                               CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  trialnumberissuingauthorityidentifier        CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  versioner                                    CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_attachmenttype                    CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_countrycode                       CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_decsdisease                       CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_icdchapter                        CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_institutiontype                   CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_interventionassigment             CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_interventioncode                  CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_mailmessage                       CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_observationalstudydesign          CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_recruitmentstatus                 CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_studyallocation                   CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_studymasking                      CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_studyphase                        CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_studypurpose                      CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_studytype                         CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_timeperspective                   CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_trialnumberissuingauthority       CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  vocabulary_vocabularytranslation             CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;

update vocabulary_vocabularytranslation
set label = 'Observacional'
where content_type_id = 14 and language = 'pt-br' and object_id = 2;

insert into vocabulary_timeperspective(`label`, `description`, `order`) values('Longitudinal', '', '52');

insert into vocabulary_vocabularytranslation(`language`, `content_type_id`, `label`, `description`, `object_id`)
       values('es', 62, 'Longitudinal', '', (select id from vocabulary_timeperspective where label = 'Longitudinal'));
insert into vocabulary_vocabularytranslation(`language`, `content_type_id`, `label`, `description`, `object_id`)
       values('pt-br', 62, 'Longitudinal', '', (select id from vocabulary_timeperspective where label = 'Longitudinal'));
