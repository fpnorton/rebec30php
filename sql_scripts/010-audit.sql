CREATE TABLE IF NOT EXISTS `audit_events` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `tag` LONGTEXT NOT NULL,
    PRIMARY KEY (id),
    KEY `audit_events_id` (`id`)
);
CREATE TABLE IF NOT EXISTS `audit_access` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `event_id` INT NOT NULL,
    `origin` LONGTEXT NOT NULL,
    `access` LONGTEXT NULL,
    `request` LONGTEXT NULL,
    PRIMARY KEY (id),
    KEY `audit_access_id` (`id`),
    FOREIGN KEY (event_id) REFERENCES audit_events(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS `audit_permission` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `event_id` INT NOT NULL,
    `username` LONGTEXT NOT NULL,
    `profile` LONGTEXT NOT NULL,
    PRIMARY KEY (id),
    KEY `audit_permission_id` (`id`),
    FOREIGN KEY (event_id) REFERENCES audit_events(id) ON UPDATE CASCADE ON DELETE CASCADE
);
