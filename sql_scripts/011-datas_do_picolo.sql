alter table picolo_approved modify column approved_date datetime;
alter table picolo_resubmit modify column resubmit_date datetime;
alter table picolo_submit modify column submit_date datetime;
