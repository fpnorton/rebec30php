CREATE TABLE assistance_languages (
     id INT NOT NULL AUTO_INCREMENT,
     language_symbol VARCHAR(10) NOT NULL,
     PRIMARY KEY (id)
);

INSERT INTO assistance_languages (language_symbol) VALUES ('pt-br'),('es'),('en');

CREATE TABLE assistance_languages_translations (
     id INT NOT NULL AUTO_INCREMENT,
     language_id INT NOT NULL,
     language_symbol_id INT NOT NULL,
     language_name VARCHAR(100) NOT NULL,
     PRIMARY KEY (id)
     -- FOREIGN KEY (language_id) REFERENCES assistance_languages(id) ON DELETE CASCADE
     -- FOREIGN KEY (language_symbol_id) REFERENCES assistance_languages(id) ON DELETE CASCADE
);

INSERT INTO assistance_languages_translations (language_id, language_symbol_id, language_name) VALUES
	(1, 1, 'Português'), (1, 2, 'Inglês'), (1, 3, 'Espanhol'),
    (2, 1, 'Portugués'), (2, 2, 'Inglés'), (2, 3, 'Español'), 
    (3, 1, 'Portuguese'), (3, 2, 'English'), (3, 3, 'Spanish');


CREATE TABLE assistance_page (
     id INT NOT NULL,
     page_name VARCHAR(100) NOT NULL,
     description VARCHAR(255),
     PRIMARY KEY (id)
);

INSERT INTO assistance_page (id, page_name) VALUES 
    (-5, 'templates_footer'), 
    (-4, 'templates_header'), 
    (-3, 'clinicaltrial_templates_header'),
    (-2, 'clinicaltrial_create_steps'),  
    (-1, 'clinicaltrial_create_termo'), 
    (0, 'clinicaltrial_create_dados_iniciais'), 
    (1, 'clinicaltrial_create_summary'), 
    (2,'clinicaltrial_create_identification'), 
    (3, 'clinicaltrial_create_attachments'), 
    (4, 'clinicaltrial_create_sponsors'), 
    (5, 'clinicaltrial_create_health_conditions'), 
    (6, 'clinicaltrial_create_interventions'), 
    (7, 'clinicaltrial_create_recruitment'), 
    (8, 'clinicaltrial_create_study_type'), 
    (9, 'clinicaltrial_create_outcomers'), 
    (10, 'clinicaltrial_create_contacts'),
    (11, 'clinicaltrial_observations'),
    (12, 'clinicaltrial_historico');


CREATE TABLE assistance_field (
     id INT NOT NULL AUTO_INCREMENT,
     language_id INT NOT NULL,
     field_name VARCHAR(100) NOT NULL,
     description VARCHAR(255),
     text LONGTEXT NOT NULL,
     PRIMARY KEY (id),
     FOREIGN KEY (language_id) REFERENCES assistance_languages(id) ON DELETE CASCADE
);

INSERT INTO assistance_field (language_id, field_name, text) VALUES
    (1, 'lbl_linguagem', 'linguagem'),
    (1, 'new_submission', 'Nova Submiss&atilde;o'),
    (1, 'nav_step', 'Ensaio Cl&iacute;nico'),
    (1, 'nav_step-1', 'Criar um novo Ensaio Cl&iacute;nico'),
    (1, 'nav_step0', 'Novo Ensaio Cl&iacute;nico'),
    (1, 'nav_step1', 'Resumo Dos Ensarios Cl&iacute;nicos'),
    (1, 'nav_step2', 'Identifica&ccedil;&atilde;o'),
    (1, 'nav_step3', 'Anexo'),
    (1, 'nav_step4', 'Patrocinadores'),
    (1, 'nav_step5', 'Condi&ccedil;&otilde;es De Sa&uacute;de'),
    (1, 'nav_step6', 'Interven&ccedil;&atilde;o'),
    (1, 'nav_step7', 'Recrutamento'),
    (1, 'nav_step8', 'Desenho Do Estudo'),
    (1, 'nav_step9', 'Desfechos'),
    (1, 'nav_step10', 'Contato'),
    (1, 'nav_step11', 'Enviar'),
    (1, 'nav_step11', 'Enviar'),
    (1, 'step-1', 'Criar um novo Ensaio Cl&iacute;nico'),
    (1, 'step0', 'Novo Ensaio Cl&iacute;nico'),
    (1, 'step1', 'Sum&aacute;rio'),
    (1, 'step2', 'Identifica&ccedil;&atilde;o'),
    (1, 'step3', 'Anexos'),
    (1, 'step4', 'Patrocinadores'),
    (1, 'step5', 'Condi&ccedil;&otilde;es de Sa&uacute;de'),
    (1, 'step6', 'Interven&ccedil;&atilde;o'),
    (1, 'step7', 'Recrutamento'),
    (1, 'step8', 'Desenho do Estudo'),
    (1, 'step9', 'Desfechos'),
    (1, 'step10', 'Contato'),
    (1, 'step11', 'Enviar'),
    (1, 'menu_step_0', 'Sum&aacute;rio'),
    (1, 'menu_step_1', 'Identifica&ccedil;&atilde;o'),
    (1, 'menu_step_2', 'Anexos'),
    (1, 'menu_step_3', 'Patrocinadores'),
    (1, 'menu_step_4', 'Cond. Sa&uacute;de'),
    (1, 'menu_step_5', 'Intervencional'),
    (1, 'menu_step_6', 'Recrutamento'),
    (1, 'menu_step_7', 'Desenho do Estudo'),
    (1, 'menu_step_8', 'Desfechos'),
    (1, 'menu_step_9', 'Contatos'),
    (1, 'menu_step_10', 'Enviar'),
    (1, 'text_accept', 'Prezado(a) registrante, voc&ecirc; est&aacute; prestes a criar um novo registro de estudo no ReBEC. Voc&ecirc; &eacute; o &uacute;nico respons&aacute;vel e habilitado para fornecer informa&ccedil;&otilde;es sobre o estudo. &Eacute; importante ressaltar que seu estudo passar&aacute; por um processo de revis&atilde;o com foco no correto e coerente preenchimento dos campos que implica, a cada nova submiss&atilde;o ou ressubmiss&atilde;o feita por voc&ecirc;, um prazo de at&eacute; 45 dias para avalia&ccedil;&atilde;o por parte do ReBEC. Em tempo algum ser&aacute; feita uma aprecia&ccedil;&atilde;o &eacute;tica ou metodol&oacute;gica &quot;per se&quot;. Assim, pedimos especial gentileza na observ&acirc;ncia das orienta&ccedil;&otilde;es feitas por nossa equipe de revisores, bem como daquelas que constam nos campos de ajuda. Elas possuem todas as informa&ccedil;&otilde;es que devem constar nos campos, incluindo exemplos. Isso garantir&aacute; mais agilidade para a aprova&ccedil;&atilde;o e obten&ccedil;&atilde;o do seu n&uacute;mero de registro.</p><p>Certifique-se, com base no protocolo, da correta informa&ccedil;&atilde;o sobre o tipo de estudo - observacional ou intervencional - pois a plataforma n&atilde;o permite altera&ccedil;&atilde;o posterior, o que implicaria a dele&ccedil;&atilde;o do registro atual e cria&ccedil;&atilde;o de um outro registro e novo preenchimento. Vale ressaltar que um estudo intervencional (ou ensaio cl&iacute;nico) envolve a realiza&ccedil;&atilde;o em pacientes volunt&aacute;rios de interven&ccedil;&otilde;es &ndash; uso de drogas, equipamentos, procedimentos, dietas... - e possui desenhos com especifica&ccedil;&atilde;o de enfoque do estudo, desenho da interven&ccedil;&atilde;o, n&uacute;mero de bra&ccedil;os, tipo de mascaramento, tipo de aloca&ccedil;&atilde;o e fase do estudo. J&aacute; no estudo observacional, os pesquisadores n&atilde;o realizam interven&ccedil;&otilde;es de acordo com o plano/protocolo de pesquisa, mas observam pacientes e desfechos de uma evolu&ccedil;&atilde;o na qual eles n&atilde;o intervieram. Os Estudos Observacionais aceitos pelo ReBEC s&atilde;o os anal&iacute;ticos (caso-controle, coorte e corte transversal). Em caso de estudos que possam ter uma etapa intervencional e outra observacional, o pesquisador deve definir qual &eacute; a etapa que melhor define o projeto - se a de interven&ccedil;&atilde;o ou de observa&ccedil;&atilde;o - e escolher em que formato (intervencional ou observacional) ir&aacute; registrar o estudo. As informa&ccedil;&otilde;es sobre a segunda etapa devem ser inseridas no campo &ldquo;Descri&ccedil;&atilde;o do estudo&rdquo;.'),
    (1, 'label_accept_terms', 'Eu aceito os termos de uso'),
    (1, 'error_message_terms', 'Voc&ecirc; deve aceitar os termos de uso para prosseguir.'),
    (1, 'create_language', 'Enviar'),
    (1, 'create_study_type', 'Enviar'),
    (1, 'menu_step_10', 'Enviar');
    

-- Identificadores - Já Criados
CREATE TABLE trialnumberissuingauthorityidentifier (
     id INT NOT NULL AUTO_INCREMENT,
     issuingidentifier VARCHAR(255) NOT NULL,
     mask VARCHAR(255),
     PRIMARY KEY (id)
);

ALTER TABLE repository_trialnumber
	ADD COLUMN id_identifier INT NOT NULL DEFAULT 4,
	ADD CONSTRAINT FOREIGN KEY (id_identifier) REFERENCES trialnumberissuingauthorityidentifier(id) ON DELETE CASCADE;

INSERT INTO trialnumberissuingauthorityidentifier (issuingidentifier, mask) VALUES
	('CAAE - Certificado De Apresentação Para Apreciação Ética (NNNNNNNN.A.NNNN.NNNN)', 'NNNNNNNN.A.NNNN.NNNN'),
    ('UTN - WHO International Clinical Trials Registry Platfor (UNNNN-NNNN-NNNN)', '(UNNNN-NNNN-NNNN)'),
    ('NÚMERO DO PARECER DO CEP - COMITÊS DE ÉTICA EM PESQUISAS', ''),
    ('OUTROS - OUTROS IDENTIFICADORES', '');

-- Inserir novos tooltips
INSERT INTO assistance_fieldhelp (form, field, text, example) VALUES
	('TrialIdentificationForm', 'Study_Final_Date', 'Data para o final deste estudo', ''),
    ('TrialIdentificationForm', 'secondary_identifier', 'Os identificadores secund&aacute;rios s&atilde;o fundamentalmente os n&uacute;meros de registro junto aos Comit&ecirc;s de &Eacute;tica em Pesquisa (CEP), Sistema Nacional de &Eacute;tica em Pesquisa (SISNEP), Plataforma Brasil (PB) e n&uacute;mero do registro do estudo em outros registros de ensaios cl&iacute;nicos (p.ex. clinicaltrials.gov, ANZCTR etc.)', ''),
    ('TrialIdentificationForm', 'sec_id_1_identifier', '&quot;1) PARA ESTUDOS APROVADOS A PARTIR DE 15 DE JANEIRO DE 2012 o CAAE (Certificado de Apresentação para Apreciação Ética) emitido pela PLATAFORMA BRASIL deverá  OBRIGATORIAMENTE constar nos identificadores secundários.<br/><br/>2) PARA ESTUDOS ANTERIORES A 15 DE JANEIRO DE 2012, se o CEP do seu estudo está cadastrado no SISNEP, o número do CAAE (Certificado de Apresentação para Apreciação Ética) deverá  OBRIGATÓRIAMENTE constar nos identificadores.<br/><br/>3)  Em caso de ESTUDOS MULTICÊNTRICOS os Identificadores informados deverão ser do CENTRO COORDENADOR DO ESTUDO.<br/><br/>6)TODAS AS INFORMAÇÕES DEVEM ESTAR EM CONFORMIDADE COM O DOCUMENTO (PARECER CONSUBSTANCIADOOU CARTA DE APROVAÇÃO DO CEP) QUE FOI ANEXADA AO ESTUDO.&quot;', ''),
    ('TrialIdentificationForm', 'sec_id_1_code', '1) Informar o número do CAAE.<br/>2)TODAS AS INFORMA&Ccedil;&Otilde;ES DEVEM ESTAR EM CONFORMIDADE COM O DOCUMENTO (PARECER CONSUBSTANCIADO OU CARTA DE APROVA&Ccedil;&Atilde;O DO CEP) QUE FOI ANEXADA AO ESTUDO.', ''),
    ('TrialIdentificationForm', 'sec_id_1_date', '1) No campo DATA deve ser informado a data de emiss&atilde;o do parecer que informa o n&uacute;mero do CAAE.<br/>2)TODAS AS INFORMA&Ccedil;&Otilde;ES DEVEM ESTAR EM CONFORMIDADE COM O DOCUMENTO (PARECER CONSUBSTANCIADO OU CARTA DE APROVA&Ccedil;&Atilde;O DO CEP) QUE FOI ANEXADA AO ESTUDO.', ''),
    ('TrialIdentificationForm', 'sec_id_1_description', '1) No campo DESCRI&Ccedil;&Atilde.O Informar o nome do &oacute;rg&atilde;o emissor do CAAE, que deve ser a Plataforma Brasil ou o Sisnep (Aten&ccedil;&atilde;o a explica&ccedil;&atilde;o do item 1 e 2 da ajuda de campo para CAAE).<br/>2)TODAS AS INFORMA&Ccedil;&Otilde;ES DEVEM ESTAR EM CONFORMIDADE COM O DOCUMENTO (PARECER CONSUBSTANCIADO OU CARTA DE APROVA&Ccedil;&AtildeO DO CEP) QUE FOI ANEXADA AO ESTUDO.','');
    

INSERT INTO assistance_fieldhelptranslation (language, content_type_id, object_id, text, example) VALUES
	('pt-br', 51, (select id from assistance_fieldhelp where form = 'TrialIdentificationForm' and field = 'Study_Final_Date'), 'Data para o final deste estudo', ''),
    ('pt-br', 51, (select id from assistance_fieldhelp where form = 'TrialIdentificationForm' and field = 'secondary_identifier'), 'Os identificadores secund&aacute;rios s&atilde;o fundamentalmente os n&uacute;meros de registro junto aos Comit&ecirc;s de &Eacute;tica em Pesquisa (CEP), Sistema Nacional de &Eacute;tica em Pesquisa (SISNEP), Plataforma Brasil (PB) e n&uacute;mero do registro do estudo em outros registros de ensaios cl&iacute;nicos (p.ex. clinicaltrials.gov, ANZCTR etc.)', ''),
    ('pt-br', 51, (select id from assistance_fieldhelp where form = 'TrialIdentificationForm' and field = 'sec_id_1_identifier'), '&quot;1) PARA ESTUDOS APROVADOS A PARTIR DE 15 DE JANEIRO DE 2012 o CAAE (Certificado de Apresentação para Apreciação Ética) emitido pela PLATAFORMA BRASIL deverá  OBRIGATORIAMENTE constar nos identificadores secundários.<br/><br/>2) PARA ESTUDOS ANTERIORES A 15 DE JANEIRO DE 2012, se o CEP do seu estudo está cadastrado no SISNEP, o número do CAAE (Certificado de Apresentação para Apreciação Ética) deverá  OBRIGATÓRIAMENTE constar nos identificadores.<br/><br/>3)  Em caso de ESTUDOS MULTICÊNTRICOS os Identificadores informados deverão ser do CENTRO COORDENADOR DO ESTUDO.<br/><br/>6)TODAS AS INFORMAÇÕES DEVEM ESTAR EM CONFORMIDADE COM O DOCUMENTO (PARECER CONSUBSTANCIADOOU CARTA DE APROVAÇÃO DO CEP) QUE FOI ANEXADA AO ESTUDO.&quot;', ''),
    ('pt-br', 51, (select id from assistance_fieldhelp where form = 'TrialIdentificationForm' and field = 'sec_id_1_code'), '1) Informar o número do CAAE.<br/>2)TODAS AS INFORMA&Ccedil;&Otilde;ES DEVEM ESTAR EM CONFORMIDADE COM O DOCUMENTO (PARECER CONSUBSTANCIADO OU CARTA DE APROVA&Ccedil;&Atilde;O DO CEP) QUE FOI ANEXADA AO ESTUDO.', ''),
    ('pt-br', 51, (select id from assistance_fieldhelp where form = 'TrialIdentificationForm' and field = 'sec_id_1_date'), '1) No campo DATA deve ser informado a data de emiss&atilde;o do parecer que informa o n&uacute;mero do CAAE.<br/>2)TODAS AS INFORMA&Ccedil;&Otilde;ES DEVEM ESTAR EM CONFORMIDADE COM O DOCUMENTO (PARECER CONSUBSTANCIADO OU CARTA DE APROVA&Ccedil;&Atilde;O DO CEP) QUE FOI ANEXADA AO ESTUDO.', ''),
    ('pt-br', 51, (select id from assistance_fieldhelp where form = 'TrialIdentificationForm' and field = 'sec_id_1_description'), '1) No campo DESCRI&Ccedil;&Atilde.O Informar o nome do &oacute;rg&atilde;o emissor do CAAE, que deve ser a Plataforma Brasil ou o Sisnep (Aten&ccedil;&atilde;o a explica&ccedil;&atilde;o do item 1 e 2 da ajuda de campo para CAAE).<br/>2)TODAS AS INFORMA&Ccedil;&Otilde;ES DEVEM ESTAR EM CONFORMIDADE COM O DOCUMENTO (PARECER CONSUBSTANCIADO OU CARTA DE APROVA&Ccedil;&AtildeO DO CEP) QUE FOI ANEXADA AO ESTUDO.','');;