*** Settings ***
Resource  config/common.robot
Resource  config/keyword.robot
Library  Selenium2Library
Library  Collections
Library  OperatingSystem
Library  Process
Library  DateTime
# Library  Dialogs
Library  String
Suite Setup     Abrir site
# Suite Teardown  Fechar site

*** Variables ***

*** Test Cases ***
Setup
    Logar ReBEC

    ${webelement}=  Get WebElement  xpath: //a[contains(@name, "link_to_summary")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]

    ${webelement}=  Get WebElement  xpath: //a[contains(@href, "corrigir/7")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //h1[@class="title_line"]

    Select From List By Label  id=id_expanded_access_program  Desconhecido

    Select From List By Label  id=id_study_purpose  Tratamento

    Select From List By Label  id=id_intervention_assignment  Grupo único

    Input Text  id=id_number_arms  5

    Select From List By Label  id=id_masking_type  Duplo-cego

    Select From List By Label  id=id_allocation_type  Braço único

    Select From List By Label  id=id_study_phase  N/A

    Sleep  2s

    ${webelement}=  Get WebElement  xpath: //input[contains(@type, "submit") and contains(@value, "Salvar")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]

    # Deslogar ReBEC

*** Keywords ***
Setup Root Name
    ${root}=  Get Value From User On Console  Usuario ROOT [${u_root}] >
    ${root}=  Replace String  ${root}  \n  ""
    Run Keyword If  '${root}'!=''  Set Global Variable  ${u_root}  ${root}
    Run Keyword If  '${root}'!=''  Setup Root Password
#
Setup Root Password
    Set Global Variable  ${p_root}
    ${password}=  Get Value From User On Console  Senha ROOT [${p_root}] >
    ${password}=  Replace String  ${password}  \n  ""
    Run Keyword If  '${password}'!=''  Set Global Variable  ${p_root}  ${password}
#
