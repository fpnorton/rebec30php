# -*- coding: utf-8 -*-
from datetime import date
from datetime import timedelta

GATEWAY="172.20.0.1"
HOMEPAGE = "http://172.20.0.10"
#HOMEPAGE = "http://149.28.107.240"
BROWSER = "chrome"
EMAILS = "/var/mail/new/"

USUARIO = "Allergan"
SENHA = "123456"

SEMANA_RETRASADA = (date.today() - timedelta(weeks=2))
SEMANA_PASSADA = (date.today() - timedelta(weeks=1))
ANTEONTEM = (date.today() - timedelta(days=2))
ONTEM = (date.today() - timedelta(days=1))
HOJE = date.today()
AMANHA = (date.today() + timedelta(days=1))
DEPOIS_DE_AMANHA = (date.today() + timedelta(days=2))
SEMANA_QUE_VEM = (date.today() + timedelta(weeks=1))

DIAS = [
    [
        (date.today() + timedelta(days=180)),
        (date.today() + timedelta(days=181)),
        (date.today() + timedelta(days=182))
    ],
    [
        (date.today() + timedelta(days=190)),
        (date.today() + timedelta(days=191)),
        (date.today() + timedelta(days=192))
    ],
    [
        (date.today() + timedelta(days=210)),
        (date.today() + timedelta(days=211))
    ],
]

CNPJS_INVALIDOS = [ "00000000000000",
                    "11111111111111",
                    "22222222222222",
                    "33333333333333",
                    "44444444444444",
                    "55555555555555",
                    "66666666666666",
                    "77777777777777",
                    "88888888888888",
                    "99999999999999" ]
# CPF
# O terceiro dígito da direita para a esquerda identifica a unidade federativa na
# qual a pessoa foi registrada, observando o dígito final antes do traço pode-se
# descobrir a origem da pessoa. Veja o código para cada Estado:
# No exemplo CPF nº 000.000.006-00
# 0 - Rio Grande do Sul
# 1 - Distrito Federal, Goiás, Mato Grosso do Sul e Tocantins
# 2 - Pará, Amazonas, Acre, Amapá, Rondônia e Roraima
# 3 - Ceará, Maranhão e Piauí
# 4 - Pernambuco, Rio Grande do Norte, Paraíba e Alagoas
# 5 - Bahia e Sergipe
# 6 - Minas Gerais
# 7 - Rio de Janeiro e Espírito Santo
# 8 - São Paulo
# 9 - Paraná e Santa Catarina
CPFS_INVALIDOS = [
                '00000000000',
                '11111111111',
                '22222222222',
                '33333333333',
                '44444444444',
                '55555555555',
                '66666666666',
                '77777777777',
                '88888888888',
                '99999999999' ]
