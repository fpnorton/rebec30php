*** Settings ***
Library  Selenium2Library
Library  Collections
Library  OperatingSystem
Library  Process
Library  DateTime
Library  Dialogs
Library  String
Variables  variables.py

*** Variables ***
*** Keywords ***
Logar ReBEC
    Wait Until Page Contains Element  xpath: //input[contains(@name, "email")]
    Input Text  name=email  ${USUARIO}
    Input Text  name=password  ${SENHA}
    Execute JavaScript  $("form").submit()
    Wait Until Page Contains Element  xpath: //span[contains(@class, "user_logged")]

Deslogar ReBEC
    Wait Until Page Contains Element  xpath: //span[contains(@class, "user_logged")]
    ${webelement}=  Get WebElement  xpath: //span[contains(@class, "user_logged")]/parent::*//img[contains(@src, "dropdown")]
    Click Element  ${webelement}
    ${webelement}=  Get WebElement  xpath: //a[contains(@href, "logout")]
    Click Element  ${webelement}
    Wait Until Page Contains Element  xpath: //input[contains(@name, "email")]

# Try Click Button
#     [Arguments]  ${locator}
#     ${webelement}=  Get WebElement  ${locator}
#     Set Focus To Element  ${webelement}
#     Click Element  ${webelement}

# Try Input Text
#     [Arguments]  ${locator}  ${text}
# #     Log To Console  \nTry Input Text
#     ${text}=  Remove String Using Regexp  ${text}  [\r\t\n\.-]
#     ${webelement}=  Get WebElement  ${locator}
#     # Log To Console  \nTry Input Text: [${webelement}]
#     ${retries}=  Set Variable  3
#     :FOR  ${counter}  IN RANGE  0  ${retries}
#     \  Input Text  ${locator}  ${text}
#     \  ${inserted_text}=  Get Value  ${webelement}
#     \  ${inserted_text}=  Remove String Using Regexp  ${inserted_text}  [\r\t\n\.-]
#     \  ${result}=  Evaluate  '${inserted_text}'=='${text}'
#     # \  Log To Console  \nTry Input Text: [${counter}] '${inserted_text}'=='${text}' => '${result}'
#     \  Exit For Loop If  ${result}==True
Ir Ao Link do Email
    ${arquivos}=  List Files In Directory  ${EMAILS}
    ${arquivo}=  Get Variable Value  ${arquivos[0]}
    Log To Console  ${arquivo}
    # Run Keyword If  ${arquivo}=False  Fail  msg=Nao foi enviado email
    # :FOR  ${arquivo}  IN  @{arquivos}
    ${mail}=  Get File  ${EMAILS}/${arquivo}  encoding=UTF-8
    Log To Console  ${mail}
    ${mail}=  Remove String  ${mail}  =\n
    ${link}=  Get Regexp Matches  ${mail}  "http://(.+)"
    ${link}=  Remove String  ${link[0]}  "  ${HOMEPAGE}/
    Log To Console  ${HOMEPAGE}/${link}
    # Confirma Execucao
    Go To  ${HOMEPAGE}/${link}

Remover Todos Os Emails
    @{arquivos}=  List Files In Directory  ${EMAILS}
    :FOR  ${arquivo}  IN  @{arquivos}
    \  Remove File  ${EMAILS}/${arquivo}

Check Problem 27
    # Acertar o formulário salvar no root/admin
    # A rota deve ser apontar apenas o nome
    [Arguments]  ${action}
    ${form}=  Get WebElement  xpath: //form
    ${form_action}=  Remove String  ${form.get_attribute('action')}  ${HOMEPAGE}
    # Log To Console  \n\t#27 Action Check ['${form_action}'=='${action}']
    Run Keyword And Continue On Failure	 Should Be Equal  ${form_action}  ${action}  #27 Falhou Para ${action}

Check Problem 28
    # Telas de cadastro de administrador/root
    # Não há necessidade de exibir os campos de nome e sobrenome
    Run Keyword And Continue On Failure	 Page Should Not Contain Element  xpath: //input[@id='nome']  message=\n#28 Falhou Para input nome
    Run Keyword And Continue On Failure	 Page Should Not Contain Element  xpath: //input[@id='email']  message=\n#28 Falhou Para input email

Editar Dias Do Evento
    [Arguments]  ${evento}
    Log To Console  \nEditar Dias Do Evento: ${evento['nome']}
    Wait Until Page Contains Element  xpath: //small[contains(text(), "Lista")]
    Log To Console  \n${evento['nome']}
    ${LINK}=  Get WebElement  xpath: //td[contains(text(), "${evento['nome']}")]/parent::*//a[contains(@href, '/edit')]
    Click Link  ${LINK}
    Wait Until Page Contains Element  xpath: //small[contains(text(), "Editar")]
    Execute JavaScript  window.scrollBy(0,1000)
    :FOR  ${DIA}  IN  @{evento['dia']}
    \  Log To Console  \nData: ${DIA.strftime("%d/%m/%Y")}
    \  Escolher data  ${DIA}
    Set Selenium Speed  1s
    Execute JavaScript  $("form").submit()
    # Click Button    xpath: //button[contains(text(), "Salvar")]
    Set Selenium Speed  0s
    Wait Until Page Contains Element  xpath: //div[contains(@class, "toast") and contains(text(), "sucesso")]
    Set Selenium Speed  1s
    ${LINK}=  Get WebElement  xpath: //td[contains(text(), "${evento['nome']}")]/parent::*//i[contains(text(), 'visibility')]
    Click Link  ${LINK}
    Sleep  5
    ${LINK}=  Get WebElement  xpath: //a[contains(@class, 'modal-close')]
    Click Link  ${LINK}
    Set Selenium Speed  0s

Cadastrar Evento
    [Arguments]  ${evento}
    Log To Console  \nCadastrar Evento: ${evento['nome']}
    Log To Console  Inscricoes: ${evento['inscricao']['inicio'].strftime('%d/%m/%Y')} - ${evento['inscricao']['final'].strftime('%d/%m/%Y')}
    Page Should Contain Link  xpath: //a[contains(text(), "Adicionar")]
    Click Link  xpath: //a[contains(text(), "Adicionar")]
    Wait Until Page Contains Element  xpath: //small[contains(text(), "Criar")]
    Input Text  id=nome  ${evento['nome']}
    # Set Selenium Speed  2s
    # SEMANA_RETRASADA = (date.today() - timedelta(weeks=2))
    # SEMANA_PASSADA = (date.today() - timedelta(weeks=1))
    # ANTEONTEM = (date.today() - timedelta(days=2))
    # ONTEM = (date.today() - timedelta(days=1))
    # HOJE = date.today()
    # AMANHA = (date.today() + timedelta(days=1))
    # DEPOIS_DE_AMANHA = (date.today() + timedelta(days=2))
    # SEMANA_QUE_VEM = (date.today() + timedelta(weeks=1))
    # Testar Data  inicio_inscricao  ${ONTEM.strftime('%d%m%Y')}
    # Press Key  id=inicio_inscricao  \r ${ANTEONTEM.strftime('%d%m%Y')}
    # Press Key  id=inicio_inscricao  \r ${ONTEM.strftime('%d%m%Y')}
    # Press Key  id=inicio_inscricao  \r 04112018
    Set Selenium Speed  0s
    Input Text  id=total_vagas  0
    Input Text  id=total_vagas  -1
    Input Text  id=total_vagas  2147483647
    # Set Selenium Speed  1s
    Press Key  id=inicio_inscricao  ${evento['inscricao']['inicio'].strftime('%d%m%Y')}
    Press Key  id=fim_inscricao  ${evento['inscricao']['final'].strftime('%d%m%Y')}
    Input Text  id=total_vagas  ${evento['total_por_dia']}
    Set Selenium Speed  0s
    # Escolher data  ${DIAS[0]}
    Run Keyword If  ${evento['dia_0']}!=None  Escolher data  ${evento['dia_0']}
    Execute JavaScript  $("form").submit()
    # Click Button    xpath: //button[contains(text(), "Salvar")]
    Wait Until Page Contains Element  xpath: //div[contains(@class, "toast") and contains(text(), "sucesso")]
    Set Selenium Speed  1s
    ${LINK}=  Get WebElement  xpath: //td[contains(text(), "${evento['nome']}")]/parent::*//i[contains(text(), 'visibility')]
    Click Link  ${LINK}
    Sleep  5
    ${LINK}=  Get WebElement  xpath: //a[contains(@class, 'modal-close')]
    Click Link  ${LINK}
    Set Selenium Speed  0s

Cadastrar Administrador
    Set Selenium Speed  0s
    [Arguments]  ${username}
    Selecionar Menu  Administradores
    Wait Until Page Contains Element  xpath: //small[contains(text(), "Lista")]
    Click Link  xpath: //a[contains(@href, "/create")]
    Wait Until Page Contains Element  xpath: //small[contains(text(), "Cadastro")]
#    Check Problem 27  /admin
#    Check Problem 28
    :FOR  ${CPF_INVALIDO}  IN  @{CPFS_INVALIDOS}
    # \  Click Button  xpath: //*[contains(text(), "Limpar")]
    \  Input Text  id=cpf  ${CPF_INVALIDO}\t
    \  Run Keyword And Continue On Failure  Page Should Contain Element  xpath: //input[@id='cpf' and (contains(@class, 'invalid') or contains(@class, 'red-text'))]  message=CPF '${CPF_INVALIDO}' foi considerado valido
    # \  Input Text  id=cpf  \r
    # \  Click Button  xpath: //*[contains(text(), "Limpar")]
    \  Try Clear Element Text  id=cpf
    Input Text    id=cpf     ${username}
    Execute JavaScript  $("form[action*='/admin']").submit()
    Wait Until Page Contains Element  xpath: //div[contains(@class, "toast") and contains(text(), "sucesso")]
    Log To Console  \nAdministrador ${username} cadastrado.

Adicionar Cadastro de Instituicao
    [Arguments]  ${instituicao}  ${dia}
    ${ie}=  Copy Dictionary  ${instituicao}
    Log Dictionary  ${ie}
    ${data}=  Convert Date  ${dia}  result_format=%d / %m / %Y
    Set To Dictionary  ${ie}  dia  ${data}
    Log Dictionary  ${ie}
    Append To List  ${IES}  ${ie}

Inscricao de Instituicao
    [Arguments]  ${ie}
    Remover Todos Os Emails
    ${dia}=  Convert Date  ${ie['dia'].isoformat()}  result_format=%d / %m / %Y
    Log To Console  \nInscricao de Instituicao: '${dia}' '${ie['nome']}'
    Wait Until Page Contains Element  xpath: //a[contains(@href, "/inscricao/create")]
    Click Link  xpath: //a[contains(@href, "/inscricao/create")]
    Wait Until Page Contains Element  xpath: //small[contains(text(), "Inscri")]
    Input Text  id=quantidade_alunos  ${ie['total']}
    Selecionar item ID pelo LABEL  tipo_inscricao  ${ie['tipo_inscricao']}
    Selecionar item ID pelo LABEL  tipo_de_gestao  ${ie['gestao']}
    :FOR  ${CNPJ_INVALIDO}  IN  @{CNPJS_INVALIDOS}
    \  Run Keyword And Continue On Failure  Testar CNPJ  cnpj  ${CNPJ_INVALIDO}
    :FOR  ${CPF_INVALIDO}  IN  @{CPFS_INVALIDOS}
    \  Run Keyword And Continue On Failure  Testar CPF  cpf_diretor  ${CPF_INVALIDO}
    Input Text  id=nome_instituicao  ${ie['nome']}
    Try Input Text  id=cnpj  ${ie['cnpj']}
    Input Text  id=email  ${ie['email']}
    Input Text  id=telefone_instituicao  ${ie['telefone']}

    Input Text  id=logradouro  ${ie['endereco']['logradouro']}
    Input Text  id=numero  ${ie['endereco']['numero']}
    Input Text  id=complemento  ${ie['endereco']['complemento']}
    Input Text  id=bairro  ${ie['endereco']['bairro']}
    Input Text  id=cidade  ${ie['endereco']['cidade']}
    Selecionar item ID pelo LABEL  uf  ${ie['endereco']['uf']}

    Input Text  id=nome_diretor  ${ie['diretor']['nome']}
    Try Input Text  id=cpf_diretor  ${ie['diretor']['cpf']}
    Input Text  id=email_diretor  ${ie['diretor']['email']}
    Input Text  id=telefone_diretor  ${ie['diretor']['telefone']}
    Set Selenium Speed  1s
    Selecionar item ID pelo LABEL  dia_de_evento  ${dia}
    Execute JavaScript  window.scrollBy(0,1000)
    Set Selenium Speed  0s

    ${index}=  Set Variable  0
    :FOR  ${responsavel}  IN  @{ie['responsavel']}
    \  Cadastrar Responsavel  ${index}  ${responsavel}
    \  ${index}=  Evaluate  ${index} + 1

    Set Selenium Speed  1s
    Execute JavaScript  $("form[action*='/create/']").submit()
    Wait Until Page Contains Element  xpath: //div[contains(@class, "toast") and contains(text(), "sucesso")]
    Set Selenium Speed  0s

    # ${LINK}=  Get WebElement  xpath: //i[contains(text(), "file_download")]
    # Click Link  ${LINK}

    ${contador}=  Count Files In Directory  ${EMAILS}
    Run Keyword If  ${contador} > 0  Ir Ao Link do Email
    Run Keyword If  ${contador} > 0  Resetar Senha  ${ie}
    Run Keyword If  ${contador} > 0  Deslogar usuário
    Set Selenium Speed  0s

    # Confirma Execucao

    # Log  DIA: ${dia} ${ie['nome']} +${ie['total']} alunos
    # Log To Console  \nDIA: ${dia} ${ie['nome']} +${ie['total']} alunos

Resetar Senha
    [Arguments]  ${ie}
    Wait Until Page Contains Element  xpath: //small[contains(text(), "criar")]

    Input Text  id=email  ${ie['email']}
    Input Text  id=password  ${ie['cnpj']}
    Input Text  id=password-confirm  ${ie['cnpj']}

    Log To Console  email: ${ie['email']} password: ${ie['cnpj']}

    Set Selenium Speed  1s
    Execute JavaScript  $("form[action*='/reset']").submit()

    Wait Until Page Contains Element  xpath: //div[contains(@class, "toast") and contains(text(), "senha")]
    # Wait Until Page Contains Element  xpath: //small[contains(text(), "Lista")]

Cadastrar Responsavel
    [Arguments]  ${index}  ${responsavel}
    Log To Console  \nResponsavel: ${responsavel['cpf']} - ${responsavel['nome']}
    ${LINK}=  Get WebElement  xpath: //i[contains(text(), 'add')]
    Click Link  ${LINK}
    Try Input Text  xpath: //input[contains(@name, '[${index}][nome]')]  ${responsavel['nome']}
    Try Input Text  xpath: //input[contains(@id, 'cpf-${index}')]  ${responsavel['cpf']}
    Input Text  xpath: //input[contains(@name, '[${index}][email]')]  ${responsavel['email']}
    Try Input Text  xpath: //input[contains(@id, 'telefone-${index}')]  ${responsavel['telefone']}
    Set Selenium Speed  1s
    Log To Console  \nresp-modal-${index}
    ${LINK}=  Get WebElement  xpath: //div[contains(@id, 'resp-modal-${index}')]//a[contains(text(), 'Salvar')]
    Click Link  ${LINK}
    Set Selenium Speed  0s
