*** Settings ***
Library  Selenium2Library
Library  Collections
Library  OperatingSystem
Library  Process
Library  DateTime
Library  String
Variables  variables.py

*** Variables ***
*** Keywords ***
Abrir site
    Iniciar variaveis globais
    Set Screenshot Directory  /tmp/
    # Execute Manual Step  Executar suite: ${SUITE NAME} ?  default_error=Nao
    Log To Console  Site: ${HOMEPAGE}
    Log To Console  Browser: ${BROWSER}
    #
    # ${continue}=  Get Environment Variable  CONFIRM_EXECUTION
    # Run Keyword If  '${continue}'=='None' or '${continue.upper()}'!='S'  Confirma Execucao
    #
    # Remove File  /tmp/conhecendo_a_ufrj.sqlite
    # Touch  /tmp/conhecendo_a_ufrj.sqlite
    # Run  php /var/www/NovoConhecendoUFRJ/artisan migrate:refresh --seed
    # Run  docker container exec -it conhecendoaufrj-phpfpm php artisan migrate:refresh --seed
    # Start Process  php  /var/www/NovoConhecendoUFRJ/artisan  serve
    # Pause Execution  message=${HOMEPAGE} foi iniciado...
    # Open Browser    ${HOMEPAGE}     ${BROWSER}
    Run Keyword If  '${BROWSER}'=='firefox'  Open Browser  ${HOMEPAGE}  ${BROWSER}  remote_url=http://${GATEWAY}:4444
    Run Keyword If  '${BROWSER}'=='chrome'  Open Browser  ${HOMEPAGE}  ${BROWSER}  remote_url=http://${GATEWAY}:9515
    Maximize Browser Window

Confirma Execucao
    ${continue}=  Get Value From User On Console  Executar suite: ${SUITE NAME}? [s/N]
    Run Keyword If  '${continue.upper()}'!='S'  Fail


Fechar site
    # Pause Execution  message=Finalizar testes...
    Terminate All Processes
    Run Keyword If All Tests Passed  Close All Browsers

Iniciar variaveis globais
    #
    # Log To Console  ${TEST_DATE.isoformat()}
    # Log To Console  ${TEST_DATE.strftime('%d/%m/%Y')}
    # ${TEST_DATE}=  Add Time To Date  ${TEST_DATE.isoformat()}  1 day  result_format=%Y-%m-%d
    # Log To Console  ${TEST_DATE}
    # Fatal Error

    # ${LINES}=  Grep File  /var/www/NovoConhecendoUFRJ/.env  CAS_HOST
    # ${LINES}=  Get Lines Matching Regexp  ${LINES}  ^\s*[^\s*#].*
    # ${LINES}=  Split String  ${LINES}  =
    # ${CAS_HOST}=  Set Variable  ${LINES[1]}

    # ${USERS}=  Get From Dictionary  ${CAS_CONFIG}  ${CAS_HOST}

    # Set Suite Variable  ${CAS_HOST}
    # Set Suite Variable  ${USERS}
    Set Suite Variable  @{DIAS}  @{EMPTY}
    Set Suite Variable  @{IES}  @{EMPTY}

    ${INSCRICAO_DIA_1}=  Get Current Date  result_format=%Y-%m-%d
    ${INSCRICAO_DIA_2}=  Add Time To Date  ${INSCRICAO_DIA_1}  2 days  result_format=%Y-%m-%d

    ${dia}=  Add Time To Date  ${INSCRICAO_DIA_1}  180 days  result_format=%Y-%m-%d
    Append To List  ${DIAS}  ${dia}

    ${dia}=  Add Time To Date  ${INSCRICAO_DIA_1}  181 days  result_format=%Y-%m-%d
    Append To List  ${DIAS}  ${dia}

    ${dia}=  Add Time To Date  ${INSCRICAO_DIA_1}  182 days  result_format=%Y-%m-%d
    Append To List  ${DIAS}  ${dia}

    Set Suite Variable  ${INSCRICAO_DIA_1}
    Set Suite Variable  ${INSCRICAO_DIA_2}

Escolher data
    [Arguments]  ${DIA_ESCOLHIDO}
    ${DIA_ESCOLHIDO}=  Add Time To Date  ${DIA_ESCOLHIDO.isoformat()}  1 day  result_format=%Y-%m-%d
    Execute JavaScript  $('.datepicker').click()
    Execute JavaScript  $('.datepicker').datepicker('gotoDate', '${DIA_ESCOLHIDO}')
    Execute JavaScript  $('.datepicker').datepicker('setDate', '${DIA_ESCOLHIDO}')
    Execute JavaScript  $('.datepicker').datepicker('setDate', '+1d')
    Execute JavaScript  $('.datepicker-done').click()

Selecionar item ID pelo LABEL
    [Arguments]  ${ID}  ${LABEL}
    Log To Console  \nSELECIONANDO: '${LABEL}'
    Execute JavaScript  $('#${ID}').parent().find('.dropdown-trigger').click()
    Execute JavaScript  $('#${ID}').parent().find(":contains('${LABEL}')").click()

Testar CNPJ
    [Arguments]  ${id}  ${cnpj}
    Try Clear Element Text  id=${id}
    Sleep  1
    Input Text  id=${id}  ${cnpj}\t
    Page Should Contain Element  xpath: //input[@id='${id}' and (contains(@class, 'invalid') or contains(@class, 'red-text'))]

Testar CPF
    [Arguments]  ${id}  ${cpf}
    Try Clear Element Text  id=${id}
    Sleep  1
    Input Text  id=${id}  ${cpf}\t
    Page Should Contain Element  xpath: //input[@id='${id}' and (contains(@class, 'invalid') or contains(@class, 'red-text'))]

Testar Data
    [Arguments]  ${id}  ${data}
    Press Key  id=${id}  ${data}
    # Page Should Contain Element  xpath: //input[@id='${id}' and (contains(@class, 'invalid') or contains(@class, 'red-text'))]

Preencher Input Date
    [Arguments]  ${id}  ${DATA}
    Log To Console  \nPreencher Input Date
    ${TECLAS}=  Convert Date  ${DATA}  result_format=%d%m%Y
    Log To Console  \n${TECLAS}
    Press Key  id=${id}  ${TECLAS}

Autenticar usuário
#    Selecionar Menu  Acessar
    ${LINK}=  Get WebElement  xpath: //a[contains(text(), "administrativo")]
    Click Link  ${LINK}
    Wait Until Page Contains Element  id=password
    [Arguments]  ${username}  ${password}
    Log To Console  \nAutenticando ${username}
    Input Text  id=username  ${username}
    Input Text  id=password  ${password}
    Click Button  xpath: //*[contains(text(), "Login")]

Deslogar usuário
    Log To Console  \nDeslogando
    Selecionar Menu  Sair

Selecionar Menu
    [Arguments]  ${menu}
    Wait Until Page Does Not Contain Element  xpath: //div[contains(@class, "toast")]
    Log To Console  \nSelecionar Menu: ${menu}
    ${SANDUICHE}=  Get WebElement  xpath: //i[contains(text(), "menu")]
    ${TARGET}=  Run Keyword If  ${SANDUICHE.is_displayed()}==True  Abrir Menu Sanduiche  ${menu}
    ...         ELSE  Trazer Menu Aberto  ${menu}
    Log To Console  \nClicando Elemento: (${TARGET.text} ${TARGET.get_attribute('href')})
    Click Element  ${TARGET}

Try Clear Element Text
    [Arguments]  ${locator}
    ${webelement}=  Get WebElement  ${locator}
    ${text}=  Get Value  ${webelement}
    ${retries}=  Get Length  ${text}
    :FOR  ${counter}  IN RANGE  0  ${retries}
    \  Press Key  ${locator}  \\8
    # Run Keyword If  '${text}'!=''  Try Clear Element Text  ${locator}

Try Input Text
    [Arguments]  ${locator}  ${text}
    Try Clear Element Text  ${locator}
#     Log To Console  \nTry Input Text
    ${text}=  Remove String Using Regexp  ${text}  [\r\t\n\.-]
    ${webelement}=  Get WebElement  ${locator}
    # Log To Console  \nTry Input Text: [${webelement}]
    ${retries}=  Set Variable  3
    :FOR  ${counter}  IN RANGE  0  ${retries}
    \  Input Text  ${locator}  ${text}
    \  ${inserted_text}=  Get Value  ${webelement}
    \  ${inserted_text}=  Remove String Using Regexp  ${inserted_text}  [\r\t\n\.-]
    \  ${result}=  Evaluate  '${inserted_text}'=='${text}'
    # \  Log To Console  \nTry Input Text: [${counter}] '${inserted_text}'=='${text}' => '${result}'
    \  Exit For Loop If  ${result}==True

Trazer Menu Aberto
    [Arguments]  ${menu}
    Log To Console  \nMenu Aberto (buscando ${menu})
    ${TARGET}=  Get WebElement  xpath: //ul[contains(@class, "hide-on-")]
    @{LINKS}=  Set Variable  ${TARGET.find_elements_by_tag_name('a')}
    ${menu}=  Replace String  ${menu}  \n  ""
    :FOR  ${LINK}  IN  @{LINKS}
    \  ${texto}=  Replace String  ${LINK.text}  \n  ""
    \  ${lines}=  Get Lines Containing String  ${texto}  ${menu}
    # \  Log To Console  [${lines} - ${texto}]
    \  Return From Keyword If  '${lines}'!=''  ${LINK}
    Fatal Error
    # Return From Keyword  None

Abrir Menu Sanduiche
    Log To Console  \nMenu Sanduiche
    [Arguments]  ${menu}
    ${SANDUICHE}=  Get WebElement  xpath: //i[contains(text(), "menu")]/parent::*
    # Log To Console  \n${MENU}
    ${TARGET}=  Set Variable  ${SANDUICHE.get_attribute('data-target')}
    # Log To Console  \n${TARGET}
    Click Element  ${SANDUICHE}
    ${ITEM}=  Get WebElement  xpath: //*[@id='${TARGET}']
    @{LINKS}=  Set Variable  ${ITEM.find_elements_by_tag_name('a')}
    ${menu}=  Replace String  ${menu}  \n  ""
    :FOR  ${LINK}  IN  @{LINKS}
    \  ${texto}=  Replace String  ${LINK.text}  \n  ""
    \  ${lines}=  Get Lines Containing String  ${texto}  ${menu}
    # \  Log To Console  'lines=${lines}'
    \  Return From Keyword If  '${lines}'!=''  ${LINK}
    Return From Keyword  None

Get Value From User On Console
    [Arguments]  ${prompt}
    Evaluate  sys.__stdout__.write("""\n${prompt}""")  sys
    ${input}=  Evaluate  unicode(raw_input())
    [Return]  ${input}

Pause Execution On Console
    ${continue}=  Get Value From User On Console  Pausa ${SUITE NAME}. Continua? [s/N]
    Run Keyword If  '${continue.upper()}'!='S'  Fail
