*** Settings ***
Resource  config/common.robot
Resource  config/keyword.robot
Library  Selenium2Library
Library  Collections
Library  OperatingSystem
Library  Process
Library  DateTime
# Library  Dialogs
Library  String
Suite Setup     Abrir site
# Suite Teardown  Fechar site

*** Variables ***

*** Test Cases ***
Setup
    Logar ReBEC

    ${webelement}=  Get WebElement  xpath: //a[contains(@name, "link_to_summary")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]

    ${webelement}=  Get WebElement  xpath: //a[contains(@href, "corrigir/3")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //h1[@class="title_line"]

    ${count}=  Get Element Attribute  xpath: //input[contains(@name, "count_sponsor")]  value

    :FOR  ${index}  IN RANGE  4  ${count}
    \     Sleep  1s
    \     Log To Console  index: ${index}
    \     Select Checkbox  id=id_sponsor_set-${index}-delete

    ${count}=  Evaluate  ${count}+1

    Sleep  2s
    ${webelement}=  Get WebElement  xpath: //a[contains(text(), "Adicionar outro")]
    Click Element  ${webelement}
    Sleep  2s
    Select From List By Label  id=id_sponsor_set-${count}-sponsor_type  Patrocinador Secundário
    Select From List By Label  id=id_sponsor_set-${count}-institution  Allergan

    # ${count}=  Evaluate  ${count}+1
    #
    # ${webelement}=  Get WebElement  xpath: //a[contains(text(), "Adicionar outro")]
    # Click Element  ${webelement}
    # Select From List By Label  id=id_sponsor_set-${count}-sponsor_type  Fonte de apoio monetário ou material
    # Select From List By Label  id=id_sponsor_set-${count}-institution  Allergan

    # ${webelement}=  Get WebElement  xpath: //a[contains(text(), "Adicionar outro")]
    # Click Element  ${webelement}
    # Select From List By Label  id=id_sponsor_set-3-institution  patrocinador

    # Select From List By Label  id=id_ctattachment_set-1-attachment  Link
    # Input Text  id=id_ctattachment_set-1-link  http://host.provider.net/path/file.ext
    # Input Text  id=id_ctattachment_set-1-description  A link
    # Select Checkbox  id=id_ctattachment_set-1-public
    #
    # Select From List By Label  id=id_ctattachment_set-2-attachment  Arquivo
    # Choose File  id=id_ctattachment_set-2-file  ${CURDIR}/upload
    # Input Text  id=id_ctattachment_set-2-description  A file
    # Select Checkbox  id=id_ctattachment_set-1-public

    ${webelement}=  Get WebElement  xpath: //input[contains(@type, "submit") and contains(@value, "Salvar")]
    Click Element  ${webelement}
    #
    Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]

    Deslogar ReBEC

*** Keywords ***
Setup Root Name
    ${root}=  Get Value From User On Console  Usuario ROOT [${u_root}] >
    ${root}=  Replace String  ${root}  \n  ""
    Run Keyword If  '${root}'!=''  Set Global Variable  ${u_root}  ${root}
    Run Keyword If  '${root}'!=''  Setup Root Password
#
Setup Root Password
    Set Global Variable  ${p_root}
    ${password}=  Get Value From User On Console  Senha ROOT [${p_root}] >
    ${password}=  Replace String  ${password}  \n  ""
    Run Keyword If  '${password}'!=''  Set Global Variable  ${p_root}  ${password}
#
