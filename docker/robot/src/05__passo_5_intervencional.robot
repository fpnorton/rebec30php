*** Settings ***
Resource  config/common.robot
Resource  config/keyword.robot
Library  Selenium2Library
Library  Collections
Library  OperatingSystem
Library  Process
Library  DateTime
# Library  Dialogs
Library  String
Suite Setup     Abrir site
# Suite Teardown  Fechar site

*** Variables ***

*** Test Cases ***
Setup
    Logar ReBEC

    ${webelement}=  Get WebElement  xpath: //a[contains(@name, "link_to_summary")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]

    ${webelement}=  Get WebElement  xpath: //a[contains(@href, "corrigir/5")]
    Click Element  ${webelement}

    # Set Selenium Speed  1s

    Wait Until Page Contains Element  xpath: //h1[@class="title_line"]

    Input Text  id=id_intervention_en  Intervention
    Input Text  id=id_intervention_pt-br  Intervenção

    Select Checkbox  id=id_intervention_codes_3

    Select Checkbox  id=id_intervention_codes_6

    Select From List By Label  id=id_descriptor_set-1-descriptor_type  Geral

    Select From List By Label  id=id_descriptor_set-1-parent_vocabulary  CID-10

    Input Text  id=id_descriptor_set-1-vocabulary_item_pt-br  H30-040 - Item do vocabulário

    ${webelement}=  Get WebElement  xpath: //input[contains(@type, "submit") and contains(@value, "Salvar")]
    Click Element  ${webelement}

    # Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]
    # Deslogar ReBEC

*** Keywords ***
Setup Root Name
    ${root}=  Get Value From User On Console  Usuario ROOT [${u_root}] >
    ${root}=  Replace String  ${root}  \n  ""
    Run Keyword If  '${root}'!=''  Set Global Variable  ${u_root}  ${root}
    Run Keyword If  '${root}'!=''  Setup Root Password
#
Setup Root Password
    Set Global Variable  ${p_root}
    ${password}=  Get Value From User On Console  Senha ROOT [${p_root}] >
    ${password}=  Replace String  ${password}  \n  ""
    Run Keyword If  '${password}'!=''  Set Global Variable  ${p_root}  ${password}
#
