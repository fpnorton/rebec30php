*** Settings ***
Resource  config/common.robot
Resource  config/keyword.robot
Library  Selenium2Library
Library  Collections
Library  OperatingSystem
Library  Process
Library  DateTime
# Library  Dialogs
Library  String

*** Variables ***
${HOMEPAGE}=  http://www.facebook.com
${BROWSER}=  chrome

*** Test Cases ***
Abrir Facebook
    # Iniciar variaveis globais
    Set Screenshot Directory  /tmp/
    # Execute Manual Step  Executar suite: ${SUITE NAME} ?  default_error=Nao
    Log To Console  Site: ${HOMEPAGE}
    # Log To Console  Browser: ${BROWSER}
    #
    # ${continue}=  Get Environment Variable  CONFIRM_EXECUTION
    # Run Keyword If  '${continue}'=='None' or '${continue.upper()}'!='S'  Confirma Execucao
    #
    # Remove File  /tmp/conhecendo_a_ufrj.sqlite
    # Touch  /tmp/conhecendo_a_ufrj.sqlite
    # Run  php /var/www/NovoConhecendoUFRJ/artisan migrate:refresh --seed
    # Run  docker container exec -it conhecendoaufrj-phpfpm php artisan migrate:refresh --seed
    # Start Process  php  /var/www/NovoConhecendoUFRJ/artisan  serve
    # Pause Execution  message=${HOMEPAGE} foi iniciado...
    # Open Browser    ${HOMEPAGE}     ${BROWSER}
    # Open Browser  ${HOMEPAGE}  ${BROWSER}  remote_url=http://${GATEWAY}:9515  desired_capabilities=profile.default_content_setting_values.notifications:0,profile.default_content_settings.popups:0
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Log To Console  ${chrome_options}

    ${prefs}    Create Dictionary   credentials_enable_service=${false}  profile.default_content_setting_values.notifications=2
    Call Method    ${chrome_options}    add_experimental_option    prefs    ${prefs}

    Call Method    ${chrome_options}    add_argument    --disable-infobars
    Call Method    ${chrome_options}    add_argument    --disable-notifications

    ${excluded}    Create List    disable-popup-blocking
    Call Method    ${chrome_options}    add_experimental_option    excludeSwitches    ${excluded}

    ${caps}    Call Method    ${chrome_options}    to_capabilities

    # profile.default_content_setting_values.notifications:0,profile.default_content_settings.popups:0
    Log To Console  ${caps}

    Open Browser  ${HOMEPAGE}  ${BROWSER}  remote_url=http://${GATEWAY}:9515  desired_capabilities=${caps}

Login
    Set Selenium Timeout  30s

    Wait Until Page Contains Element  xpath: //input[contains(@name, "email")]
    Input Text  name=email  josue.resende@gmail.com
    Input Text  name=pass  d3p3ch3m0d3

    ${webelement}=  Get WebElement  id=loginbutton
    Click Element  ${webelement}
    #
    Wait Until Page Contains Element  xpath: //*[contains(@class, "linkWrap")]  timeout=None
    #
    Go To  https://www.facebook.com/groups/1685635948159756

    # ${webelement}=  Get WebElement  xpath: //*[contains(@class, "linkWrap") and contains(text(), "aturista")]
    # Log To Console  ${webelement}
    # Click Element  ${webelement}
    #
    # Log To Console  grupo
    # Sleep  3s
    #
    # ${webelement}=  Get WebElement  xpath: //span[contains(text(), "Discussão")]
    # Click Element  ${webelement}
    #
    # Log To Console  discussao
    # Sleep  3s
    #
    # @{webelements}=  Get Webelements  xpath: //div[contains(@class, "userContent")]
    # @{webelements}=  Get Webelements  class=userContent
    @{userContentWrappers}=  Get Webelements  class=userContentWrapper
    # Log To Console  @{webelements}

    :FOR  ${userContentWrapper}  IN  @{userContentWrappers}
    \  Log To Console  ${userContentWrapper}
    \  ${timestampWebElement}=  Set Variable  ${userContentWrapper.find_element_by_xpath('class="timestamp"')}
    # \  Log To Console  title=${timestampWebElement.get_attribute('title')}
    \  Log To Console  ${timestampWebElement.text}
    # \  Log To Console  ${timestampWebElement.find_element_by_xpath('..//*[class=userContentWrapper]')}
    # \  ${postWebElement}=  Set Variable  ${timestampWebElement.find_element_by_xpath('.//ancestor::div[class=userContentWrapper]')}
    # \  Log To Console  text=${postWebElement.text}
    # title - data-utime

    # :FOR  ${loop}  IN RANGE  0  5
    # \  Log To Console  test
    # \  Sleep  2s
    # \  ${loop}=  Set Variable  1


# Setup
#     Set Global Variable  ${u_root}  ${USERS['u_root_seed']}
#     Set Global Variable  ${p_root}  ${USERS['p_root_seed']}
#     ${default}=  Get Environment Variable  DEFAULT_USER
#     Run Keyword If  '${default}'=='None' or '${default.upper()}'!='S'  Setup Root Name
    # Logar ReBEC
    #
    # ${webelement}=  Get WebElement  xpath: //a[contains(@name, "link_to_summary")]
    # Click Element  ${webelement}
    #
    # Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]
    #
    # ${webelement}=  Get WebElement  xpath: //a[contains(@href, "/6")]
    # Click Element  ${webelement}
    #
    # Wait Until Page Contains Element  xpath: //h1[@class="title_line"]

    # Input Text  id=id_health_condition_en  Health Condition
    # Input Text  id=id_health_condition_pt-br  Condições de Saúde
    #
    # Select From List By Label  id=id_descriptor_set-1-descriptor_type  Geral
    #
    # Select From List By Label  id=id_descriptor_set-1-parent_vocabulary  CID-10
    #
    # Input Text  id=id_descriptor_set-1-vocabulary_item_pt-br  H30-040 - Item do vocabulário

    # Select From List By Label  id=id_ctattachment_set-1-attachment  Link
    # Input Text  id=id_ctattachment_set-1-link  http://host.provider.net/path/file.ext
    # Input Text  id=id_ctattachment_set-1-description  A link
    # Select Checkbox  id=id_ctattachment_set-1-public
    #
    # Select From List By Label  id=id_ctattachment_set-2-attachment  Arquivo
    # Choose File  id=id_ctattachment_set-2-file  ${CURDIR}/upload
    # Input Text  id=id_ctattachment_set-2-description  A file
    # Select Checkbox  id=id_ctattachment_set-1-public

    # ${webelement}=  Get WebElement  xpath: //input[contains(@type, "submit") and contains(@value, "Salvar")]
    # Click Element  ${webelement}
    #
    # Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]
    # Deslogar ReBEC

*** Keywords ***
Setup Root Name
    ${root}=  Get Value From User On Console  Usuario ROOT [${u_root}] >
    ${root}=  Replace String  ${root}  \n  ""
    Run Keyword If  '${root}'!=''  Set Global Variable  ${u_root}  ${root}
    Run Keyword If  '${root}'!=''  Setup Root Password
#
Setup Root Password
    Set Global Variable  ${p_root}
    ${password}=  Get Value From User On Console  Senha ROOT [${p_root}] >
    ${password}=  Replace String  ${password}  \n  ""
    Run Keyword If  '${password}'!=''  Set Global Variable  ${p_root}  ${password}
#
