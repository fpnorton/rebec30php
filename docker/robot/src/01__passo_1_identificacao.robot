*** Settings ***
Resource  config/common.robot
Resource  config/keyword.robot
Library  Selenium2Library
Library  Collections
Library  OperatingSystem
Library  Process
Library  DateTime
# Library  Dialogs
Library  String
Suite Setup     Abrir site
# Suite Teardown  Fechar site

*** Variables ***

*** Test Cases ***
Setup
    Logar ReBEC

    ${webelement}=  Get WebElement  xpath: //a[contains(@href, "/submissao")]
    Click Element  ${webelement}

    ${webelement}=  Get WebElement  xpath: //a[contains(@name, "/sumario")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]

    ${webelement}=  Get WebElement  xpath: //a[contains(@href, "corrigir/1/")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //h1[@class="title_line"]

    Input Text    id=id_scientific_title_en  Scientific Title
    Input Text    id=id_scientific_title_pt-br  Titulo Científico

    Input Text    id=id_public_title_en  Public Title
    Input Text    id=id_public_title_pt-br  Título Público

    Input Text    id=id_scientific_acronym_en  Scientific Acronym
    Input Text    id=id_scientific_acronym_pt-br  Acrônimo Científico

    Input Text    id=id_scientific_acronym_expansion_en  Scientific Acronym Expansion
    Input Text    id=id_scientific_acronym_expansion_pt-br  Expansão Acrônimo Científico

    Input Text    id=id_acronym_en  Acronym
    Input Text    id=id_acronym_pt-br  Acrônimo

    Input Text    id=id_acronym_expansion_en  Acronym Expansion
    Input Text    id=id_acronym_expansion_pt-br  Expansão Acrônimo

    ${count}=  Get Element Attribute  xpath: //input[contains(@name, "count_secid")]  value
#    ${count}=  Evaluate  ${count}+1

    :FOR  ${index}  IN RANGE  4  ${count}
    \     Sleep  1s
    \     Log To Console  index: ${index}
    \     Select Checkbox  id=id_identifier_set-${index}-delete

#    Input Text    id=id_identifier_set-1-code  12345678.A.1234.1234
#    Input Text    id=id_identifier_set-1-description  Certificado De Apresentação Para Apreciação Ética
#
#    Input Text    id=id_identifier_set-2-code  U2345-1234-1234
#    Input Text    id=id_identifier_set-2-description  WHO International Clinical Trials Registry Platform
#
#    Input Text    id=id_identifier_set-3-code  1234
#    Input Text    id=id_identifier_set-3-description  COMITÊS DE ÉTICA EM PESQUISAS

    ${webelement}=  Get WebElement  xpath: //input[contains(@type, "submit") and contains(@value, "Salvar")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]

#    Deslogar ReBEC

#
# US07-0 Acesso Administrativo - Cadastrar Root (sendo Root)
#     Autenticar usuário  ${u_root}  ${p_root}
#     Selecionar Menu  Root
#     Wait Until Page Contains Element  xpath: //small[contains(text(), "Lista")]
#     Click Link  xpath: //a[contains(@href, "/root/create")]
#     Wait Until Page Contains Element  xpath: //small[contains(text(), "Cadastro")]
# #    Check Problem 27  /root
# #    Check Problem 28
#     # Set Selenium Speed  1s
#     :FOR  ${CPF_INVALIDO}  IN  @{CPFS_INVALIDOS}
#     \  Input Text  id=cpf  ${CPF_INVALIDO}\t
#     \  Run Keyword And Continue On Failure  Page Should Contain Element  xpath: //input[@id='cpf' and (contains(@class, 'invalid') or contains(@class, 'red-text'))]  message=CPF '${CPF_INVALIDO}' foi considerado valido
#     # \  Input Text  id=cpf  \r
#     # \  Click Button  xpath: //*[contains(text(), "Limpar")]
#     \  Try Clear Element Text  id=cpf
#     Set Selenium Speed  0s
#     Try Input Text  id=cpf  ${USERS['rootusername']}\t
#     Execute JavaScript  $("form").submit()
#     # Click Button  xpath: //button[@type='submit' and contains(text(), 'Salvar')]
#     Wait Until Page Contains Element  xpath: //div[contains(@class, "toast") and contains(text(), "sucesso")]
#     # Page Should Contain Element  xpath: //div[contains(@class, "toast") and contains(text(), "sucesso")]
#     Log To Console  \nRoot ${USERS['rootusername']} cadastrado
#     Deslogar usuário
#
# US07-1 Acesso Administrativo - Status Root (sendo Root)
#     Set Selenium Speed  0s
#     Autenticar usuário  ${u_root}  ${p_root}
#     Selecionar Menu  Root
#     Wait Until Page Contains Element  xpath: //small[contains(text(), "Lista")]
#     ${LINK}=  Get WebElement  xpath: //td[contains(text(), "${USERS['rootusername']}")]/parent::*//a[contains(@href, '/activate')]
#     Click Link  ${LINK}
#     Wait Until Page Contains Element  xpath: //div[contains(@class, "toast") and contains(text(), " desativado")]
#     Log To Console  \nRoot ${USERS['rootusername']} desativado
#     Deslogar usuário
#     Autenticar usuário  ${USERS['rootusername']}  ${USERS['rootpassword']}
#     Wait Until Page Contains Element  xpath: //h5[contains(text(), "401")]
#     Log To Console  \n'Exibir um erro melhor do que 401'
#     Set Selenium Speed  1s
#     Go To  ${HOMEPAGE}/admin/logout
#     Set Selenium Speed  0s
#     Autenticar usuário  ${u_root}  ${p_root}
#     Selecionar Menu  Root
#     Wait Until Page Contains Element  xpath: //small[contains(text(), "Lista")]
#     ${LINK}=  Get WebElement  xpath: //td[contains(text(), "${USERS['rootusername']}")]/parent::*//a[contains(@href, '/activate')]
#     Click Link  ${LINK}
#     Wait Until Page Contains Element  xpath: //div[contains(@class, "toast") and contains(text(), " ativado")]
#     Log To Console  \nRoot ${USERS['rootusername']} ativado
#     Deslogar usuário

*** Keywords ***
Setup Root Name
    ${root}=  Get Value From User On Console  Usuario ROOT [${u_root}] >
    ${root}=  Replace String  ${root}  \n  ""
    Run Keyword If  '${root}'!=''  Set Global Variable  ${u_root}  ${root}
    Run Keyword If  '${root}'!=''  Setup Root Password
#
Setup Root Password
    Set Global Variable  ${p_root}
    ${password}=  Get Value From User On Console  Senha ROOT [${p_root}] >
    ${password}=  Replace String  ${password}  \n  ""
    Run Keyword If  '${password}'!=''  Set Global Variable  ${p_root}  ${password}
#
