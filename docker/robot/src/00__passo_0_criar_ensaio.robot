*** Settings ***
Resource  config/common.robot
Resource  config/keyword.robot
Library  Selenium2Library
Library  Collections
Library  OperatingSystem
Library  Process
Library  DateTime
# Library  Dialogs
Library  String
Suite Setup     Abrir site
Suite Teardown  Fechar site

*** Variables ***

*** Test Cases ***
Setup
    Set Global Variable  ${u_root}  ${USERS['u_root_seed']}
    Set Global Variable  ${p_root}  ${USERS['p_root_seed']}
#     ${default}=  Get Environment Variable  DEFAULT_USER
#     Run Keyword If  '${default}'=='None' or '${default.upper()}'!='S'  Setup Root Name
    Logar ReBEC
    # Deslogar ReBEC
    Wait Until Page Contains Element  xpath: //a[contains(@href, "/prototype/submissao")]
    ${webelement}=  Get WebElement  xpath: //a[contains(@href, "/prototype/submissao")]
    Click Element  ${webelement}
    # Wait Until Page Contains Element  xpath: //button[contains(text(), "Nova Submissão")]
    # ${webelement}=  Get WebElement  xpath: //div[contains(@class, "tour")]/parent::*//div[contains(@class, "popover-navigation")]
    # ${webelement}=  Get WebElement  xpath: //div[contains(@class, "tour-backdrop")]
    # Wait Until Page Contains Element  xpath: //div[contains(@class, "tour-backdrop")]
    # Press Key  xpath: //div[contains(@class, "tour-backdrop")]/parent::*  \\27

    Select Checkbox  id=id_accept_terms
    Click Button  id=save_ct

    Wait Until Page Contains Element  xpath: //h1[contains(@class, "title_line") and contains(text(), "Novo")]

    Select From List By Label    id=id_study_type    Intervenção
    # Select From List By Label    id=id_study_type    Observação

    Input Text    id=id_scientific_title_en    Scientific title
    Input Text    id=id_scientific_title_pt-br    Texto científico
    Select From List By Label    name=recruitment_countries[]    Brasil
    Input Text    id=id_utn_code    123456
    Select From List By Label    id=id_primary_sponsor    patrocinador

    Sleep  3s

    ${webelement}=  Get WebElement  xpath: //input[contains(@type, "submit") and contains(@value, "Salvar")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]

    Sleep  15s

    :FOR  ${counter}  IN RANGE  2  11
    \  ${webelement}=  Get WebElement  xpath: //a[contains(@href, "Step/${counter}/")]
    \  Click Element  ${webelement}
    \  Wait Until Page Contains Element  xpath: //h1[@class="title_line"]

    ${webelement}=  Get WebElement  xpath: //a[contains(@href, "/sumario/")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]

    Deslogar ReBEC

#
# US07-0 Acesso Administrativo - Cadastrar Root (sendo Root)
#     Autenticar usuário  ${u_root}  ${p_root}
#     Selecionar Menu  Root
#     Wait Until Page Contains Element  xpath: //small[contains(text(), "Lista")]
#     Click Link  xpath: //a[contains(@href, "/root/create")]
#     Wait Until Page Contains Element  xpath: //small[contains(text(), "Cadastro")]
# #    Check Problem 27  /root
# #    Check Problem 28
#     # Set Selenium Speed  1s
#     :FOR  ${CPF_INVALIDO}  IN  @{CPFS_INVALIDOS}
#     \  Input Text  id=cpf  ${CPF_INVALIDO}\t
#     \  Run Keyword And Continue On Failure  Page Should Contain Element  xpath: //input[@id='cpf' and (contains(@class, 'invalid') or contains(@class, 'red-text'))]  message=CPF '${CPF_INVALIDO}' foi considerado valido
#     # \  Input Text  id=cpf  \r
#     # \  Click Button  xpath: //*[contains(text(), "Limpar")]
#     \  Try Clear Element Text  id=cpf
#     Set Selenium Speed  0s
#     Try Input Text  id=cpf  ${USERS['rootusername']}\t
#     Execute JavaScript  $("form").submit()
#     # Click Button  xpath: //button[@type='submit' and contains(text(), 'Salvar')]
#     Wait Until Page Contains Element  xpath: //div[contains(@class, "toast") and contains(text(), "sucesso")]
#     # Page Should Contain Element  xpath: //div[contains(@class, "toast") and contains(text(), "sucesso")]
#     Log To Console  \nRoot ${USERS['rootusername']} cadastrado
#     Deslogar usuário
#
# US07-1 Acesso Administrativo - Status Root (sendo Root)
#     Set Selenium Speed  0s
#     Autenticar usuário  ${u_root}  ${p_root}
#     Selecionar Menu  Root
#     Wait Until Page Contains Element  xpath: //small[contains(text(), "Lista")]
#     ${LINK}=  Get WebElement  xpath: //td[contains(text(), "${USERS['rootusername']}")]/parent::*//a[contains(@href, '/activate')]
#     Click Link  ${LINK}
#     Wait Until Page Contains Element  xpath: //div[contains(@class, "toast") and contains(text(), " desativado")]
#     Log To Console  \nRoot ${USERS['rootusername']} desativado
#     Deslogar usuário
#     Autenticar usuário  ${USERS['rootusername']}  ${USERS['rootpassword']}
#     Wait Until Page Contains Element  xpath: //h5[contains(text(), "401")]
#     Log To Console  \n'Exibir um erro melhor do que 401'
#     Set Selenium Speed  1s
#     Go To  ${HOMEPAGE}/admin/logout
#     Set Selenium Speed  0s
#     Autenticar usuário  ${u_root}  ${p_root}
#     Selecionar Menu  Root
#     Wait Until Page Contains Element  xpath: //small[contains(text(), "Lista")]
#     ${LINK}=  Get WebElement  xpath: //td[contains(text(), "${USERS['rootusername']}")]/parent::*//a[contains(@href, '/activate')]
#     Click Link  ${LINK}
#     Wait Until Page Contains Element  xpath: //div[contains(@class, "toast") and contains(text(), " ativado")]
#     Log To Console  \nRoot ${USERS['rootusername']} ativado
#     Deslogar usuário

*** Keywords ***
Setup Root Name
    ${root}=  Get Value From User On Console  Usuario ROOT [${u_root}] >
    ${root}=  Replace String  ${root}  \n  ""
    Run Keyword If  '${root}'!=''  Set Global Variable  ${u_root}  ${root}
    Run Keyword If  '${root}'!=''  Setup Root Password
#
Setup Root Password
    Set Global Variable  ${p_root}
    ${password}=  Get Value From User On Console  Senha ROOT [${p_root}] >
    ${password}=  Replace String  ${password}  \n  ""
    Run Keyword If  '${password}'!=''  Set Global Variable  ${p_root}  ${password}
#
