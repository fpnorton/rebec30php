*** Settings ***
Resource  config/common.robot
Resource  config/keyword.robot
Library  Selenium2Library
Library  Collections
Library  OperatingSystem
Library  Process
Library  DateTime
# Library  Dialogs
Library  String
Suite Setup     Abrir site
# Suite Teardown  Fechar site

*** Variables ***

*** Test Cases ***
Setup
    Logar ReBEC

    ${webelement}=  Get WebElement  xpath: //a[contains(@name, "link_to_summary")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]

    ${webelement}=  Get WebElement  xpath: //a[contains(@href, "corrigir/6")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //h1[@class="title_line"]

    Select From List By Label  id=id_study_status  Outros

    @{COUNTRIES}=  Get Selected List Labels  name=recruitment_countries[]
    :FOR  ${COUNTRY}  IN  @{COUNTRIES}
    \  Unselect From List By Label  name=recruitment_countries[]  ${COUNTRY}

    Select From List By Label  name=recruitment_countries[]  Brasil  Zimbabwe

    Press Key  id=id_date_first_enrollment  04/12/2017

    Press Key  id=id_date_last_enrollment  26/01/2018

    Select From List By Label  name=gender  Ambos

    Select From List By Label  id=id_minimum_age_no_limit  Sim

    Select From List By Label  id=id_maximum_age_no_limit  Não

    Input Text  id=id_inclusion_maximum_age  20

    Select From List By Label  id=id_maximum_age_unit  Meses

    Input Text  id=id_target_size  530

    Input Text  id=id_inclusion_criteria_en  Unique Inclusion Criterias

    Input Text  id=id_inclusion_criteria_pt-br  Critérios de Inclusão Únicos

    Input Text  id=id_exclusion_criteria_en  Unique Exclusion Criterias

    Input Text  id=id_exclusion_criteria_pt-br  Critérios de Exclusão Únicos

    #
    # Select Checkbox  id=id_intervention_codes_3
    #
    # Select Checkbox  id=id_intervention_codes_6
    #
    # Select From List By Label  id=id_descriptor_set-1-descriptor_type  Geral
    #
    # Select From List By Label  id=id_descriptor_set-1-parent_vocabulary  CID-10
    #
    # Input Text  id=id_descriptor_set-1-vocabulary_item_pt-br  H30-040 - Item do vocabulário
    #
    Sleep  3s

    ${webelement}=  Get WebElement  xpath: //input[contains(@type, "submit") and contains(@value, "Salvar")]
    Click Element  ${webelement}

    # Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]
    # Deslogar ReBEC

*** Keywords ***
Setup Root Name
    ${root}=  Get Value From User On Console  Usuario ROOT [${u_root}] >
    ${root}=  Replace String  ${root}  \n  ""
    Run Keyword If  '${root}'!=''  Set Global Variable  ${u_root}  ${root}
    Run Keyword If  '${root}'!=''  Setup Root Password
#
Setup Root Password
    Set Global Variable  ${p_root}
    ${password}=  Get Value From User On Console  Senha ROOT [${p_root}] >
    ${password}=  Replace String  ${password}  \n  ""
    Run Keyword If  '${password}'!=''  Set Global Variable  ${p_root}  ${password}
#
