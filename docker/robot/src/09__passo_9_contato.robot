*** Settings ***
Resource  config/common.robot
Resource  config/keyword.robot
Library  Selenium2Library
Library  Collections
Library  OperatingSystem
Library  Process
Library  DateTime
# Library  Dialogs
Library  String
Suite Setup     Abrir site
# Suite Teardown  Fechar site

*** Variables ***

*** Test Cases ***
Setup
    Logar ReBEC

    ${webelement}=  Get WebElement  xpath: //a[contains(@name, "link_to_summary")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]

    ${webelement}=  Get WebElement  xpath: //a[contains(@href, "corrigir/9")]
    Click Element  ${webelement}

    Wait Until Page Contains Element  xpath: //h1[@class="title_line"]

    Set Selenium Speed    0s

    Select From List By Label  id=id_ctcontact_set-1-contact_type  Consultas públicas

    ${webelement}=  Get WebElement  name=botao_cadastro_contato
    Click Element  ${webelement}

    Sleep  1s

    Input Text  id=id_first_name  John
    Input Text  id=id_last_name  Doe
    Input Text  id=id_email  johndoe@doe.com
    Input Text  id=id_address  Elm Street
    Input Text  id=id_city  City
    Input Text  id=id_state  State
    Select From List By Label  id=id_country  Brasil
    Input Text  id=id_postal_code  22000-000
    Input Text  id=id_phone  21987651234

    Sleep  1s

    ${webelement}=  Get WebElement  xpath: //form[contains(@action, "add_contact")]//input[contains(@value, "Salvar")]
    Click Element  ${webelement}

    Select From List By Label  id_ctcontact_set-1-contact  Paula Marques

    Sleep  2s

    ${webelement}=  Get WebElement  xpath: //form[contains(@action, "/10")]//input[contains(@value, "Salvar")]
    Click Element  ${webelement}

    # Execute JavaScript  $("form[action*='add_contact']").submit()
    #
    # Wait Until Page Contains Element  xpath: //th[contains(text(), "PASSO")]
    #
    # Deslogar ReBEC

*** Keywords ***
Setup Root Name
    ${root}=  Get Value From User On Console  Usuario ROOT [${u_root}] >
    ${root}=  Replace String  ${root}  \n  ""
    Run Keyword If  '${root}'!=''  Set Global Variable  ${u_root}  ${root}
    Run Keyword If  '${root}'!=''  Setup Root Password
#
Setup Root Password
    Set Global Variable  ${p_root}
    ${password}=  Get Value From User On Console  Senha ROOT [${p_root}] >
    ${password}=  Replace String  ${password}  \n  ""
    Run Keyword If  '${password}'!=''  Set Global Variable  ${p_root}  ${password}
#
