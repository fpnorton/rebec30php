#!/usr/bin/env bash

export SWAPSIZE=4G
export SWAPFILE=/memory-swap-file

#set -ex

# create a swap file
sudo /usr/bin/fallocate -l $SWAPSIZE $SWAPFILE
sudo /bin/chmod 600 $SWAPFILE

# now make this file a swap file
sudo /sbin/mkswap $SWAPFILE
# and activate it as swap
sudo /sbin/swapon $SWAPFILE

apache2 -D FOREGROUND