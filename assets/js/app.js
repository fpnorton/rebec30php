$.busyLoadSetup({
    animation: "fade",
    background: "rgb(44, 153, 228,0.86)", 
    text:"LOADING ...",
    textPosition: "bottom",
    fontSize: "2rem",
    textMargin: "3rem",
    spinner:'accordion',
});

function showLoader(element=null){ 
    if (element==null) {
        element="body";
    }
    $(element).busyLoad("show");
}


function hideLoader(element=null){ 
    if (element==null) {
        element="body";
    }
    $(element).busyLoad("hide");
}


function showAlert(type,message){
    const toast = swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            timer: 3000
        });

        toast({
            type: type,
            title: message
        })
  }

 //$('#dataTable').DataTable();