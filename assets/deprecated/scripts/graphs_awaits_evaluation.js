$.plot(
   $("#graphs_awaits_evaluation"),
   [
     {
       label: "Verde",
       data: porcentagem_green,
       color: "#006400",
     },
     {
       label: "Vermelho",
       data: porcentagem_red,
       color: "#FF0000",
     },
     {
       label: "Amarelo",
       data: porcentagem_yellow,
       color: "#FFFF00",
     }
   ],
   {
     series: {
       pie: {
         show: true,
         label: {
           show: true
         }
     }
    },
    legend: {
        show: true,
        labelBoxBorderColor: "#ffffff",
        position: "ne",
        color: "#000000",
    }
  }
);
