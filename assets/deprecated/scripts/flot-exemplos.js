// example 3 - basic pie chart
$.plot(
   $("#exemplo-3"),
   [
     {
       label: "Chrome",
       data: 47
     },
     {
       label: "Firefox",
       data: 40
     },
     {
       label: "Opera",
       data: 23
     }
   ],
   {
     series: {
       pie: {
         show: true,
         label: {
           show: true
         }
     }
    },
    legend: {
      show: true
    }
  }
);
