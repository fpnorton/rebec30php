 var host= $('body').attr('data-baseurl')+"api/";

 $('#queryForm').parsley();
 $('#queryPreviewForm').parsley();

  $(".queryAndOr").hide();
  $("#queryPreviewForm").hide();

  $("#queryHeader").change(function(event) {
    event.preventDefault();
    var type= $(this).find(':selected').attr('data-type');
    if (type==0 || type==2) {
      $("#searchText").hide();
      $("#isTrue").show();
    }
    if (type==1 ) {
      $("#isTrue").hide();
      $("#searchText").show();  
    }
  });

  $("#queryForm").submit(function(event) {
    event.preventDefault();
    var valid= $("#queryForm").parsley().validate();
    if (valid==true) {
       $(".queryAndOr").show();
       $("#queryPreviewForm").show();
       queryBuilder();

    }
    //console.log(valid);

  });

   $(".queryReset").click(function(event) {
      $(".queryAndOr").hide();
      $("#queryPreviewForm").trigger('reset');
      $("#isTrue").hide();
      $("#searchText").show();
      $("#queryPreviewForm").hide();
      getData(false);
   });

   $(".queryAndOr").click(function(event) {
      
     var options= $(this).attr('data-option');
     console.log(options);
     queryBuilder(2,options);
  
   });


   function queryBuilder(typeQuery=1,optionsQuery=null){
     var where = $("#queryHeader").val();
     var options = $("#queryOptions").val();
     var type= $('#queryHeader').find(':selected').attr('data-type');
     var optionType= $('#queryOptions').find(':selected').attr('data-type');
     var value="";

     if (type==1) {
       value= $("#queryText").val();
     }else if(type==2){
       var select= $("#queryForm input[name='typeRadios']:checked").val();
       if (select=="T") {
         value=1;
       }else{
         value=0;
       }
     }else{
       value = $("#queryForm input[name='typeRadios']:checked").val();
     }

     if (optionType==1) {
       var appendValue= " '"+ value +"'";
     }else{
       var appendValue= " ("+ value +")";
     }


     
     var query= where+" "+ options + appendValue;

     if (typeQuery==1) {
      $("#queryPreview").val(query);
     }

     if (typeQuery==2) {
        var currentVal = $('#queryPreview').val();
        console.log(currentVal);
        $('#queryPreview').val(currentVal+" "+optionsQuery+" " + query);
     }

     $("#queryForm").trigger('reset');
     $("#isTrue").hide();
     $("#searchText").show();

   }


   $("#queryPreviewForm").submit(function(event) {
     event.preventDefault();
     getData(true);
   });

   getData(false);

   function getData(query){
      // var element="#crudTable";
       var obj={
        query:query,
        form: $('#queryPreviewForm').serialize(),
       }

       $.ajax({
         url: host+'report/crud',
         type: 'post',
         dataType: 'json',
         data: obj, 
         beforeSend:function(){showLoader();},
         success:function(data){
            if (data.status==true) {
              $("#crudTable").empty();
              $("#crudTable").append(data.view);
            }
         },
         complete:function(){ hideLoader() }
       })
   }


  