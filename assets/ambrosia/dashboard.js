var host= $('body').attr('data-baseurl')+"api/";

var reviewer_icons = [
	'mdi-checkbox-blank-circle-outline',
	'mdi-checkbox-blank-circle',
	'mdi-checkbox-marked-circle'
];
var reviewer_colors = [
	'#001100',
	'#114477',
	'#117744'
];

var page=1;
var estudosList;
getList(page,false);
function getList(page,loaderFull=false,search=null){
	 var obj={
    	page: page,
    	tipo_de_relatorio:'relatorio_de_pendings',
    	search:search,
        field: $('#field').val(),
    	estudos: $("#usuario").val(),
    }
    var element="#dataTable";

    if (loaderFull==false) {
        element=null;
    }

    $.ajax({
    	url: host + "web/retornaj/page/"+page,
        type: "POST",
        data:obj,
        context: this,
        error: function() {},
        dataType: 'json',
        beforeSend:function(){
        	showLoader(element);
        },
        success:function(object){
        	$('#dataTable').dataTable().fnDestroy();
        	$("#dataTable tbody").empty();
           

        	if (object.status==true) {

        		//$('.preloader').hide();
        		var json= object.data;
        		estudosList= object.estudos;
	    		// var vermelho = 0;
	      //       var amarelo = 0;
	      //       var verde = 0;
	      //       var total;
	            var limited_date='';
	            var resubmission_date=''

	            if (page==1) {
	            	estudosList= object.estudos;
				    $("#usuario .drop").remove();
		    		$.each(estudosList, function(index, val) {
		    			 var html='<option class="drop" value="'+val.id+'">'+val.name+' </option> ';
		    			 $("#usuario").append(html);
		    		});
	            }

        		$.each(json, function(index, item) {

        			//var total = i + 1;
                    var days=  item.dias_para_revisao
	                var tempo_expira ='';
	                var nome_do_revisor;
	                var nome_do_revisor_puro;
	                var comando;
					var days_as_absolute_number = days < 0 ? (days * -1) : days;
                    var pad = "00000";
                    var to_pad_1 = "" + item.dias_para_ordenar;
                    var to_pad_2 = "" + days_as_absolute_number;

					tempo_expira = 
						'<span style="display:none;">'+
                        (pad.substring(0, pad.length - to_pad_1.length) + to_pad_1)+
                        (pad.substring(0, pad.length - to_pad_2.length) + to_pad_2)+
                        '</span>';
		            if (days <= 45 && days > 20) {
	                   tempo_expira += '<span class="badge badge-success badge-pill">' + days_as_absolute_number + '</span>';
	                   tempo_expira += '<span style="display:none;">Expira</span>';
	                   // verde = verde + 1;
	                } else if (days <= 20 && days > 0) {
	                   tempo_expira += '<span class="badge badge-warning badge-pill">' + days_as_absolute_number + '</span>';
	                   tempo_expira += '<span style="display:none;">Expira</span>';
	                   // amarelo = amarelo + 1;
	                } else {
	                   tempo_expira += '<span class="badge badge-danger badge-pill">' + days_as_absolute_number + '</span>';
                       tempo_expira += '<span style="display:none;">Expirou</span>';
	                   // vermelho = vermelho + 1;
	                }

	                    nome_do_revisor_puro = "";

		                if (typeof(item.revisor_name) == 'object') {
		                    nome_do_revisor = '<select id="nome-do-revisor" class="form-control form-control-sm" >';

		                    nome_do_revisor = nome_do_revisor + '<option value="">Select</option>';

		                    $.each(estudosList, function(index, list) {
						    	 nome_do_revisor = nome_do_revisor + '<option value="' + list.id + '">' + list.name + '</option>';
						    });

		                    nome_do_revisor = nome_do_revisor + '</select>';
		                    comando = '<span id="botao_' + item.trial_id + '"><button type="button" class="btn btn-warning btn-sm " id="salvar_' + item.trial_id + '" onclick="definirRevisor(\'' + item.trial_id + '\')"><span class="mdi mdi-cached"></span></button></span>';

		                } else {
		                    nome_do_revisor_puro = item.revisor_name;
		                    nome_do_revisor = '<span class="badge badge-success badge-pill">' + item.revisor_name + '</span>';
		                    comando = '<span id="botao_' + item.trial_id + '"><button type="button" class="btn btn-info btn-sm " id="editar_' + item.trial_id + '" onclick="editarRevisor(' + item.trial_id + ')"><span class="mdi mdi-border-color"></span></button></span>';
		                }

		                var link = 
							'<span style="display: none;">'+atob(item.title)+'</span>' +
							'<a href="' + item.link_review + '" class="btn btn-dark btn-rounded btn-fw btn-sm" target="_blank"  style="padding:3px;" ><i class="mdi mdi-link-variant"></i></a>';

						var favorite = '<span style="display: none;">'+(item.priority * -1)+' '+item.created+'</span>';
						{
							if (item.priority >= 0) {
								var icone = 'mdi-star-outline';
								if (item.priority > 0) icone = 'mdi-star';
								favorite += '<a '+
									'name="priority_' + item.trial_id + '" '+
									'onclick="mudarPrioridade('+item.trial_id+')" '+
									'href="#" '+
									'class="btn btn-red btn-fw btn-xs" '+
									'style="padding:5px;">'+
										'<i id="priority_button_'+item.trial_id+'" class="mdi '+icone+'" style="color: #ff8866;"></i> '+
									'</a> ';
							}
						}
						
						var reviewer = '';
						{
							reviewer += '<a '+
								'name="reviewer_' + item.trial_id + '" '+
								'onclick="mudarRevisor('+item.trial_id+')" '+
								'href="#" '+
								'class="btn btn-red btn-fw btn-xs" '+
								'style="padding:5px;">'+
									'<i id="reviewer_button_'+item.trial_id+'" class="mdi '+reviewer_icons[item.reviewer]+'" style="color: '+reviewer_colors[item.reviewer]+';"></i> '+
								'</a> ';
						}

						var trial_id = '<a href="'+($('body').attr('data-baseurl'))+'audit/download/'+item.trial_id+'" target="_blank" style="padding:5px;" >'+item.trial_id+'</a>';

		                var modal='<a data-toggle="modal" href="#myModal" class="btn btn-primary btn-sm" onclick="historicoDeRevisoes(' + item.submissionId + ')"><span class="mdi mdi-comment-text-outline"></span></a>';

		                if (item.resubmission !=null) {
		                	var resubmission= item.resubmission;
		                	limited_date= resubmission.limited_date;
		                	resubmission_date= resubmission.resubmission_date;
		                }

						var created = item.created;

        			var html='<tr  title="'+atob(item.title)+'" class="' + nome_do_revisor_puro + '">'+
						'<td>'+favorite+'</td>'+
						'<td>'+reviewer+'</td>'+
						'<td>'+created+'</td>'+
						'<td> '+trial_id+'</td>'+
						'<td> '+item.rbr+'</td>'+
						'<td>'+item.updated_submission+'</td>'+
						// '<td>'+ item.username +' </td>'+
						// '<td>'+item.email+'</td>'+
						'<td>'+
							item.username+
							'<br/>'+
							item.email+
						'</td>'+
						'<td>'+link+'</td>'+
						'<td>'+modal+'</td>'+
						'<td>'+item.submissionId+'</td>'+
						'<td>'+resubmission_date +'</td>'+
						'<td>'+limited_date +'</td>'+
						'<td>'+tempo_expira+'</td>'+
						'<td> <span id="registro_' + item.trial_id + '">' + nome_do_revisor + '</span> </td>'+
						'<td>'+comando+' </td>'+
					'</tr> ';
        			$('tbody').append(html);

        		});

        		// $("#toatlRecords").text(object.totalrecords);
        		// $("#pagination").append(object.links);
                
                $('#dataTable').DataTable({
                      "aLengthMenu": [
                        [5, 10, 15, -1],
                        [5, 10, 15, "All"]
                      ],
                      "iDisplayLength": 10,
                    });
        	}


        },
        complete:function(){
            hideLoader(element);
        }
    })


}


$("#pagination").on('click', 'a', function(event) {
   event.preventDefault();
   var page = $(this).attr('data-ci-pagination-page');
   getList(page,true); 
});


$(".filtrar-estudos").click(function(event) {
   event.preventDefault();
   getList(1,false);
});

$(".search-estudos").click(function(event){
    event.preventDefault();
    var string = $("#searchTable").val();
    getList(1,false,string);
})


 function historicoDeRevisoes(sub_id) {
    $.ajax({
        url: host + "web/remark",
        type: "GET",
        data: "sub_id=" + sub_id,
        context: this,
        error: function() {},
        dataType: 'json',
        success: function(object) {
        	$('.historico-de-revisoes').empty();
        	if (object.status==true) {
        		$.each(object.data, function(index, m) {
            		var html='<div class="card width-100 mb-3" ><div class="card-body"><h5 class="card-title mb-1">Revisor: '+m.revisor+' </h5><h6 class="card-subtitle mb-2 mt-1 text-muted">Data: '+m.date+'</h6><p class="card-text">Context: '+m.context+'</p></div></div>';
            		$('#myModal .historico-de-revisoes').append(html);

            	});
        	}else{

        		var html='<div class="alert alert-warning" style=""><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;&nbsp;Ensaio ainda não revisado.</div>';
        		$('#myModal .historico-de-revisoes').append(html);
        	}
        }
    });

}


function editarRevisor(trial_id) { // função responsável por editar o revisor responsável pelo ensaio. Recebe um trial_id referente ao ensaio em pending e a lista de revisores responsáveis.
    var revisores = estudosList;

    var nomeDoRevisor = '<select id="nome-do-revisor"  class="form-control form-control-sm">';

    $.each(revisores, function(index, list) {
    	 nomeDoRevisor = nomeDoRevisor + '<option value="' + list.id + '">' + list.name + '</option>';
    });
    // for (var x = 0; x < revisores.length; x++) {
    //     nomeDoRevisor = nomeDoRevisor + '<option value="' + revisores[x] + '">' + revisores[x] + '</option>';
    // }
    nomeDoRevisor = nomeDoRevisor + '</select>';
    var nomeDoRevisorNaDiv = $('#registro_' + trial_id + ' span').html();
    $('#registro_' + trial_id).html(nomeDoRevisor);
    $('#registro_' + trial_id + ' #nome-do-revisor option[value="' + nomeDoRevisorNaDiv + '"]').prop('selected', true);

    $('#botao_' + trial_id).html('<span id="botao_' + trial_id + '"><button type="button" class="btn btn-primary btn-sm" id="salvar_' + trial_id + '" onclick="definirRevisor(\'' + trial_id + '\')"><span class="mdi mdi-check-all"></span></button></span>');
}


function definirRevisor(trial_id) { // função responsável por definir o revisor responsável por um ensaio. Recebe um trial_id referente ao ensaio que está em pending
        var nomeDoRevisor = $("#registro_" + trial_id + " #nome-do-revisor option:selected").val();

        if (nomeDoRevisor != "") {
            $('#botao_' + trial_id).html('<span id="botao_' + trial_id + '"><button type="button" class="btn btn-warning btn-sm "><span class="mdi mdi-cached"></span></button></span>');

            $.getJSON(host + "web/gravarevisorname?trial_id=" + trial_id + "&revisor=" + nomeDoRevisor, function(json) {

                if (json.persisted == 1) {
                    $('#registro_' + trial_id).html('<span class="badge badge-success badge-pill">' + json.revisor_name + '</span>');
                    $('#botao_' + trial_id).html('<span id="botao_' + trial_id + '"><button type="button" class="btn btn-info btn-xs" id="editar_' + trial_id + '" onclick="editarRevisor(' + trial_id + ')"><span class="mdi mdi-border-color"></span></button></span>');
                } else if (json.persisted == 0) {
                    alert('Ocorreu um erro!');
                }

            });

        } else {
            $("#registro_" + trial_id + " #nome-do-revisor").css('box-shadow', '0px 0px 5px #FF0000');
        }

    }

function mudarPrioridade(trial_id) {
	var direcao = 0;
	if ($("#priority_button_" + trial_id).hasClass('mdi-star-outline')) direcao = 1;
	if ($("#priority_button_" + trial_id).hasClass('mdi-star')) direcao = -1;
    $.ajax({
        url: host + "web/priority",
        type: "GET",
        data: "trial_id=" + trial_id + '&direction='+direcao,
        context: this,
        error: function() {},
        dataType: 'json',
        success: function(object) {
			$("#priority_button_" + trial_id).removeClass('mdi-star-outline');
			$("#priority_button_" + trial_id).removeClass('mdi-star');
			if (object.priority == 0) $("#priority_button_" + trial_id).addClass('mdi-star-outline');
			if (object.priority == 1) $("#priority_button_" + trial_id).addClass('mdi-star');
			// getList(1,false);
        }
    });
}

function mudarRevisor(trial_id) {
    $.ajax({
        url: host + "web/next_reviewer",
        type: "GET",
        data: "trial_id=" + trial_id,
        context: this,
        error: function() {},
        dataType: 'json',
        success: function(object) {
			reviewer_icons.forEach(element => {
				$("#reviewer_button_" + trial_id).removeClass(element);
			});
			$("#reviewer_button_" + trial_id).addClass(reviewer_icons[object.reviewer]);
			$("#reviewer_button_" + trial_id).css('color', reviewer_colors[object.reviewer]);
			// getList(1,false);
        }
    });
}
